USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_volume_types_get_list]    Script Date: 4/6/2021 11:03:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table

/*EXEC [mgn_menu_parent_number_get]
	@action_name = 'TransactionDashboard'
	*/
-- =============================================
Create or ALTER       PROCEDURE [dbo].[mgn_menu_parent_number_get]
	@action_name nvarchar(50),
	@user nvarchar(50) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT menu_parent_number_1 as default_parent_number

FROM menu_items 

WHERE menu_action_result_name = @action_name
	
END
