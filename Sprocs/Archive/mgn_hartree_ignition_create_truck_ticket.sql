USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_hartree_ignition_create_truck_ticket]    Script Date: 4/23/2021 9:45:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/18/2021
-- Description:		Update selected business entities record with new user input details.

/*EXEC [mgn_hartree_ignition_create_truck_ticket] 
	@location = 'tl3',
	@nomination = 1001
	
	// 
	*/
-- =============================================
ALTER             PROCEDURE [dbo].[mgn_hartree_ignition_create_truck_ticket]

	@location nvarchar(100),
	@nomination bigint,
	@truck_name nvarchar(100) = null,
	@trailer_name nvarchar(100) = null,
	@lease nvarchar(100) = null,
	@driver_name nvarchar(100) = null,
	@unload_bbls nvarchar(100) = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

/* Set Location Type */
--declare @location nvarchar(100) = 'd1'
declare @location_type int = (select lt.lt_location_type
							  from   locations as l left outer join
							  	     location_types as lt on l.ln_location_type = lt.lt_location_type
							  where  l.ln_external_id = @location)

declare @ticket_type int
/* Set Ticket Type */
if (((select l.ln_load from locations as l where l.ln_external_id = @location) = 1) and
    @location_type = 1 /* Truck lane */)
begin
	Set @ticket_type = (select tt.typ_type
						from   ticket_type as tt
						where  tt.type_is_truck = 1 and
							   tt.type_is_shipment = 1)
end
else if (((select l.ln_offload from locations as l where l.ln_external_id = @location) = 1) and
		 @location_type = 1 /* Truck lane */)
begin
	Set @ticket_type = (select tt.typ_type
						from   ticket_type as tt
						where  tt.type_is_truck = 1 and
							   tt.type_is_receipt = 1)
end

--print @ticket_type

--declare @batch_id nvarchar(100) = (select e.short_name + '_' + isnull(@truck_name, 'truck') + '_' + CAST(YEAR(getdate()) as varchar) + CAST(MONTH(getdate()) as varchar) + CAST(DAY(getdate()) as varchar)
								   --from   (select * from nominations_general where nomination_number = @nomination) as n left outer join
										 -- entities as e on n.entity1_key = e._key)

insert into [dbo].[tickets]
			([tk_type],
			 [tk_status],
			 --[tk_batch_id],
			 [tk_nomination_number],
			 [tk_start_datetime],
			 [tk_eod_datetime],
			 [tk_product],
			 [tk_volume_uom],
			 [tk_scheduled_volume],
			 [tk_source_tank_key],
			 [tk_destination_tank_key],
			 [tk_truck_lane_key],
			 [tk_entity1_key],
			 [tk_entity2_key],
			 [tk_entity3_key],
			 [tk_entity4_key],
			 [tk_entity5_key],
			 [tk_entity6_key],
			 [tk_entity7_key])

Values	    (@ticket_type,
             2, --Active status
			 --@batch_id,
			 @nomination,
			 getdate(),
			 dbo.get_eod_datetime(getdate()),
             (select product_key from nominations_general where nomination_number = @nomination),
			 (select uom_default_volume from site_configuration where site_key = dbo.get_default_site()),
			 @unload_bbls,
			 (select ln_default_src_tank from locations where ln_external_id = @location),
			 (select ln_default_dest_tank from locations where ln_external_id = @location),
			 (select _key from locations where ln_external_id = @location),
			 (select entity1_key from nominations_general where nomination_number = @nomination),
			 (select entity2_key from nominations_general where nomination_number = @nomination),
			 (select entity3_key from nominations_general where nomination_number = @nomination),
			 (select entity4_key from nominations_general where nomination_number = @nomination),
			 (select entity5_key from nominations_general where nomination_number = @nomination),
			 (select entity6_key from nominations_general where nomination_number = @nomination),
			 (select entity7_key from nominations_general where nomination_number = @nomination)
			)

declare @new_ticket_id bigint = Cast(IDENT_CURRENT('tickets') as bigint)
declare @new_ticket_key uniqueidentifier = (select _key from tickets where tk_ticket_id = @new_ticket_id)

insert into [dbo].[tickets_misc]
			([tm_ticket_key],
			[tm_truck_name],
			[tm_trailer_name],
			[tm_lease],
			[tm_driver_name])

Values (@new_ticket_key,
		@truck_name,
		@trailer_name,
		@lease,
		@driver_name)

select @new_ticket_id

END

