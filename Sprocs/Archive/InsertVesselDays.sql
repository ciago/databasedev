
INSERT INTO vessel_projected_volumes (_key, synthesis_key, day_number, projected_volume)
select newid(), st._key, 1, syn_nominated_quantity /2
from synthesis_tickets st 
WHERE syn_vessel_lay_status <> 'REJECTED'


INSERT INTO vessel_projected_volumes (_key, synthesis_key, day_number, projected_volume)
select newid(), st._key, 2, syn_nominated_quantity /2
from synthesis_tickets st 
WHERE syn_vessel_lay_status <> 'REJECTED'


INSERT INTO vessel_projected_volumes (_key, synthesis_key, day_number, projected_volume)
select newid(), st._key, 3, 0
from synthesis_tickets st 
WHERE syn_vessel_lay_status <> 'REJECTED'