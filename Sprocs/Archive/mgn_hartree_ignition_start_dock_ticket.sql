USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_manual_tank_gauge_update]    Script Date: 4/12/2021 4:01:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/18/2021
-- Description:		Update selected business entities record with new user input details.

/*EXEC [mgn_ignition_start_dock_ticket] 
	@ticket = 1005
	
	// 
	*/
-- =============================================
Create or ALTER         PROCEDURE [dbo].[mgn_ignition_start_dock_ticket]

	@ticket bigint
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from entities
--Where _key = @row

Update tickets

set tk_status = 2

where tk_ticket_id = @ticket

--declare @ticket bigint = 1005
Select t.tk_ticket_id,
	   p.product_code,
	   m.meter_name

from tickets as t left outer join
	 products as p on t.tk_product = p._key left outer join
	 meter_products as mp on t.tk_product = mp.product_key left outer join
	 meters as m on mp.meter_key = m._key

where tk_ticket_id = @ticket

END

