
--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)
DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 05, 14, 7, 0, 0, 0)

delete from Moda_Final_Tickets WHERE eod_datetime = @eod

INSERT INTO [dbo].[Moda_Final_Tickets]
           ([_key]
           ,[movement_type]
           ,[batch_id]
           ,[from_location_id]
           ,[to_location_id]
           ,[product_id]
           ,[gsv]
           ,[start_datetime]
           ,[stop_datetime]
           ,[eod_datetime]
           ,[tank_open_gsv]
           ,[tank_open_level]
           ,[tank_close_gsv]
           ,[tank_close_level]
		   
        )
SELECT  t._key,
	   tt.type_external_code as [type],
	   t.tk_batch_id, 
	  ISNULL(ISNULL(out_pipe.external_id, out_tank.tank_external_id_location), out_berth.berth_external_code) as from_location_code,
	  ISNULL(ISNULL(in_pipe.external_id, in_tank.tank_external_id_location), in_berth.berth_external_code) as to_location_code,
	   p.product_external_id as product, 
	   t.tk_final_gsv, 
	   t.tk_start_datetime, 
	   t.tk_end_datetime, 
	   t.tk_eod_datetime, 
	   t.tk_tank_open_gsv, 
	   t.tk_tank_open_level, 
	   t.tk_tank_close_gsv, 
	   t.tk_tank_close_level
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 products p ON p._key = t.tk_product LEFT OUTER JOIN 
	 pipeline in_pipe ON in_pipe._key  = t.tk_to_key LEFT OUTER JOIN 
	 pipeline out_pipe ON out_pipe._key = t.tk_from_key LEFT OUTER JOIN 
	 tanks in_tank ON in_tank._key = t.tk_to_key LEFT OUTER JOIN 
	 tanks out_tank ON out_tank._key = t.tk_from_key LEFT OUTER JOIN 
	 berths in_berth ON in_berth._key = t.tk_to_key LEFT OUTER JOIN 
     berths out_berth ON out_berth._key = t.tk_from_key
WHERE t.tk_type IN (1,2) AND tk_eod_datetime  = @eod

UPDATE Moda_Final_Tickets 
SET batch_id = ticket_id 
WHERE eod_datetime = @eod AND movement_type = 'PipelineToTank'

INSERT INTO [dbo].[Moda_Final_Tickets]
           ([_key]
           ,[movement_type]
           ,[batch_id]
           ,[from_location_id]
           ,[to_location_id]
           ,[product_id]
           ,[gsv]
           ,[start_datetime]
           ,[stop_datetime]
           ,[eod_datetime]
           ,[tank_open_gsv]
           ,[tank_open_level]
           ,[tank_close_gsv]
           ,[tank_close_level]
        )
		
SELECT  t._key,
	   tt.type_external_code as [type],
	   t.tk_batch_id, 
	   out_tank.tank_external_id_location as from_location_code,
	   NULL as to_location_code,
	   p.product_external_id as product, 
	   t.tk_final_gsv, 
	   t.tk_start_datetime, 
	   t.tk_end_datetime, 
	   t.tk_eod_datetime, 
	   t.tk_tank_open_gsv, 
	   t.tk_tank_open_level, 
	   t.tk_tank_close_gsv, 
	   t.tk_tank_close_level
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 products p ON p._key = t.tk_product LEFT OUTER JOIN 
	 pipeline in_pipe ON in_pipe._key  = t.tk_to_key LEFT OUTER JOIN 
	 pipeline out_pipe ON out_pipe._key = t.tk_from_key LEFT OUTER JOIN 
	 tanks in_tank ON in_tank._key = t.tk_to_key LEFT OUTER JOIN 
	 tanks out_tank ON out_tank._key = t.tk_from_key LEFT OUTER JOIN 
	 berths in_berth ON in_berth._key = t.tk_to_key LEFT OUTER JOIN 
     berths out_berth ON out_berth._key = t.tk_from_key
WHERE t.tk_type  = 3 AND tk_eod_datetime  = @eod


INSERT INTO [dbo].[Moda_Final_Tickets]
           ([_key]
           ,[movement_type]
           ,[batch_id]
           ,[from_location_id]
           ,[to_location_id]
           ,[product_id]
           ,[gsv]
           ,[start_datetime]
           ,[stop_datetime]
           ,[eod_datetime]
           ,[tank_open_gsv]
           ,[tank_open_level]
           ,[tank_close_gsv]
           ,[tank_close_level]
        )
SELECT  newid(),
	   tt.type_external_code as [type],
	   t.tk_batch_id, 
	   NULL as from_location_code,
	   in_tank.tank_external_id_location as to_location_code,
	   p.product_external_id as product, 
	   t.tk_final_gsv, 
	   t.tk_start_datetime, 
	   t.tk_end_datetime, 
	   t.tk_eod_datetime, 
	   t.tk_transfer_to_tank_open_gsv, 
	   t.tk_transfer_to_tank_open_level, 
	   t.tk_transfer_to_tank_close_gsv, 
	   t.tk_transfer_to_tank_close_level
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 products p ON p._key = t.tk_product LEFT OUTER JOIN 
	 pipeline in_pipe ON in_pipe._key  = t.tk_to_key LEFT OUTER JOIN 
	 pipeline out_pipe ON out_pipe._key = t.tk_from_key LEFT OUTER JOIN 
	 tanks in_tank ON in_tank._key = t.tk_to_key LEFT OUTER JOIN 
	 tanks out_tank ON out_tank._key = t.tk_from_key LEFT OUTER JOIN 
	 berths in_berth ON in_berth._key = t.tk_to_key LEFT OUTER JOIN 
     berths out_berth ON out_berth._key = t.tk_from_key
WHERE t.tk_type  = 3 AND tk_eod_datetime  = @eod

--INSERT INTO EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets]
--           ([_key]
--           ,[movement_type]
--           ,[batch_id]
--		   ,ticket_id
--           ,[from_location_id]
--           ,[to_location_id]
--           ,[product_id]
--           ,[gsv]
--           ,[start_datetime]
--           ,[stop_datetime]
--           ,[eod_datetime]
--           ,[tank_open_gsv]
--           ,[tank_open_level]
--           ,[tank_close_gsv]
--           ,[tank_close_level]
--		   ,site_code)
--   SELECT[_key]
--           ,[movement_type]
--           ,[batch_id]
--		   ,ticket_id
--           ,[from_location_id]
--           ,[to_location_id]
--           ,[product_id]
--           ,[gsv]
--           ,[start_datetime]
--           ,[stop_datetime]
--           ,[eod_datetime]
--           ,[tank_open_gsv]
--           ,[tank_open_level]
--           ,[tank_close_gsv]
--           ,[tank_close_level]
--		   ,site_code
--	FROM [Moda_Final_Tickets]
--	WHERE eod_datetime = @eod
	
	