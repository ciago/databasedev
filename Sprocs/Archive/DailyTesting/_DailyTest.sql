-- 1.   transfers brandi data - last eod 5/2 which is 5/1 tab on brndi
-- 2.  run this sproc  

--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)
DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 05, 11, 7, 0, 0, 0)
DECLARE @re_import_brandi bit = 0



IF @re_import_brandi = 1 
BEGIN
		DELETE FROM tickets WHERE tk_eod_datetime = @eod

		INSERT INTO tickets(
			_key, 
			tk_type, 
			tk_status, 
			tk_shipper, 
			tk_product, 
			tk_batch_id, 
			tk_eod_datetime, 
			tk_from_key, 
			tk_to_key, 
			tk_scheduled_start_datetime, 
			tk_scheduled_end_datetime, 
			tk_start_datetime, 
			tk_end_datetime, 
			tk_day_sequence_number, 
			tk_volume_uom, 
			tk_scheduled_volume)


		SELECT  
			 newid(),--_key, 
			1 ,--tk_type, -- 1 pipeline, 2 vessel, 3 tank to tank
			CASE WHEN [calculated_meter_bbls_tov_based] IS NOT NULL THEN 4 ELSE 1 END,--tk_status, 
			o.tank_owner_key,--tk_shipper, 
			'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',--tk_product, 
			'',--tk_batch_id, 
			tt.eod_datetime,--tk_eod_datetime, 
			p._key,--tk_from_key, 
			t._key,--tk_to_key, 
			DATEADD(DAY, -1,tt.eod_datetime),--tk_scheduled_start_datetime, 
			tt.eod_datetime,--tk_scheduled_end_datetime, 
			DATEADD(DAY, -1,tt.eod_datetime),--tk_start_datetime, 
			tt.eod_datetime,--tk_end_datetime, 
			1,--tk_day_sequence_number, 
			1,--tk_volume_uom, 
			tt.brandis_value--tk_scheduled_volume, 
	
	
		FROM testing_pipeline_to_tank tt INNER JOIN 
			 tanks t ON t.tank_pi_name = tt.tank_name INNER JOIN 
			 tank_owner o ON o.tank_key = t._key INNER JOIN 
			 pipeline p ON p.short_code = tt.pipeline_name 
		WHERE tt.brandis_value <> 0 AND tt.eod_datetime = @eod



		----**********************************************VESSELS*********************************************


		INSERT INTO tickets(
			_key, 
			tk_type, 
			tk_status, 
			tk_shipper, 
			tk_product, 
			tk_batch_id, 
			tk_eod_datetime, 
			tk_from_key, 
			tk_to_key, 
			tk_scheduled_start_datetime, 
			tk_scheduled_end_datetime, 
			tk_start_datetime, 
			tk_end_datetime, 
			tk_day_sequence_number, 
			tk_volume_uom, 
			tk_scheduled_volume)


		SELECT  
			 newid(),--_key, 
			2 ,--tk_type, -- 1 pipeline, 2 vessel, 3 tank to tank
			CASE WHEN [calculated_meter_bbls_tov_based] IS NOT NULL THEN 4 ELSE 1 END,--tk_status, 
			o.tank_owner_key,--tk_shipper, 
			'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',--tk_product, 
			'',--tk_batch_id, 
			tt.eod_datetime,--tk_eod_datetime, 
			t._key,--tk_from_key, 
			b._key,--tk_to_key, 
			DATEADD(DAY, -1,tt.eod_datetime),--tk_scheduled_start_datetime, 
			tt.eod_datetime,--tk_scheduled_end_datetime, 
			NULL,--tk_start_datetime, 
			NULL,--tk_end_datetime, 
			1,--tk_day_sequence_number, 
			1,--tk_volume_uom, 
			(tt.brandis_value * -1)--tk_scheduled_volume, 
	
	
		FROM testing_tank_to_berth tt INNER JOIN 
			 tanks t ON t.tank_pi_name = tt.tank_name INNER JOIN 
			 tank_owner o ON o.tank_key = t._key INNER JOIN 
			 berths b ON tt.berth_name = b.berth_code 
		WHERE tt.brandis_value <> 0 AND tt.eod_datetime = @eod


		----**********************************************TRANSFERS*********************************************
		INSERT INTO tickets(
			_key, 
			tk_type, 
			tk_status, 
			tk_shipper, 
			tk_product, 
			tk_batch_id, 
			tk_eod_datetime, 
			tk_from_key, 
			tk_to_key, 
			tk_scheduled_start_datetime, 
			tk_scheduled_end_datetime, 
			tk_start_datetime, 
			tk_end_datetime, 
			tk_day_sequence_number, 
			tk_volume_uom, 
			tk_scheduled_volume)


		SELECT  
			 newid(),--_key, 
			3 ,--tk_type, -- 1 pipeline, 2 vessel, 3 tank to tank
			4, --CASE WHEN [calculated_meter_bbls_tov_based] IS NOT NULL THEN 4 ELSE 1 END,--tk_status, 
			o.tank_owner_key,--tk_shipper, 
			'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',--tk_product, 
			'',--tk_batch_id, 
			tt.eod_datetime,--tk_eod_datetime, 
			ft._key,--tk_from_key, 
			tot._key,--tk_to_key, 
			DATEADD(DAY, -1,tt.eod_datetime),--tk_scheduled_start_datetime, 
			tt.eod_datetime,--tk_scheduled_end_datetime, 
			DATEADD(DAY, -1,tt.eod_datetime),--tk_start_datetime, 
			tt.eod_datetime,--tk_end_datetime, 
			1,--tk_day_sequence_number, 
			1,--tk_volume_uom, 
			tt.brandis_value--tk_scheduled_volume, 
	
		FROM testing_tank_transfers tt INNER JOIN 
			 tanks ft ON ft.tank_pi_name = tt.from_tank_name INNER JOIN 
			 tanks tot ON tot.tank_pi_name = tt.to_tank_name INNER JOIN 
			 tank_owner o ON o.tank_key = ft._key
		WHERE tt.brandis_value <> 0 --AND pt.pipeline_key <> '00000000-0000-0000-0000-000000000000'
			 AND eod_datetime = @eod
END


---- ***********************************************************************************************************************   PIPELINE  *******************************************************************************
 -- remove any data in the final columns from the day
 UPDATE tickets 
 SET tk_tank_close_timestamp = NULL,
	 tk_tank_open_gsv = NULL,
	 tk_tank_close_gsv = NULL,
	 tk_tank_change_gsv = NULL,
	 tk_group_total_tank_diff = NULL,
	 tk_percent_tank_diff = NULL,
	 tk_total_meter_diff = NULL,
	 tk_meter_volume = NULL, 
	 tk_status = 1
 WHERE tk_eod_datetime = @eod  AND tk_type = 1

-- remove meter records to re-pull
DELETE FROM tickets_meter WHERE eod_datetime = @eod AND ticket_key IN (SELECT _key FROM tickets WHERE  tk_eod_datetime = @eod  AND tk_type = 1)

-- if the end time is not end of day at 7 am, add some time to the tank stop time so the tank can settle
-- settle time is set int he sites table 
UPDATE t
SET tk_tank_close_timestamp = CASE WHEN CONVERT(TIME, tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN tk_eod_datetime
							  ELSE DATEADD(HOUR, s.tank_settle_time_hours, tk_end_datetime) END
FROM tickets t INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod  AND tk_type = 1


-- update  tank readings 
UPDATE t
SET tk_tank_open_gsv = tr_open.tank_gsv,
tk_tank_open_level = tr_open.tank_level,
	tk_tank_close_gsv = tr_close.tank_gsv,
tk_tank_close_level = tr_close.tank_level
--select t.tk_to_key, tr_open.tank_gsv as start_gsv, tr_close.tank_gsv as close_gsv, tr_close.tank_gsv - tr_open.tank_gsv as tank_gsv_diff, tanks.tank_description
from tickets t INNER JOIN 
     tank_readings tr_open ON t.tk_to_key = tr_open.tank_key AND t.tk_start_datetime = tr_open.tank_timestamp INNER JOIN 
	 tank_readings tr_close ON tr_close.tank_key = t.tk_to_key AND  t.tk_tank_close_timestamp = tr_close.tank_timestamp INNER JOIN 
	 tanks on tanks._key = t.tk_to_key INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod AND tk_type = 1


-- update the tank diff 
UPDATE tickets 
SET tk_tank_change_gsv = tk_tank_close_gsv - tk_tank_open_gsv
WHERE tk_eod_datetime = @eod AND tk_type = 1


-- update the total tank change by pipeline or FROM key
UPDATE tk
SET tk_group_total_tank_diff = tk2.tank_change_gsv
--SELECT tk.eod_datetime, tk.tank_key, tk.berth_key, tk.tank_change_nsv, tk.total_tank_diff_nsv, tk2.tank_change_nsv
FROM tickets tk INNER JOIN 
	 (SELECT SUM(tk_tank_change_gsv) as tank_change_gsv , tk_from_key, tk_eod_datetime
	  FROM tickets 
	  WHERE tk_eod_datetime = @eod AND tk_type = 1
	  GROUP BY tk_eod_datetime, tk_from_key) tk2 ON tk.tk_from_key = tk2.tk_from_key AND tk.tk_eod_datetime = tk.tk_eod_datetime
WHERE tk.tk_eod_datetime = @eod AND tk_type = 1

-- update the percent where the group total is not 0 (avoid divide by 0 error)
UPDATE tickets
SET tk_percent_tank_diff = (tk_tank_change_gsv / tk_group_total_tank_diff)
WHERE tk_eod_datetime = @eod AND tk_type = 1 AND tk_group_total_tank_diff <> 0

-- update percent to 0 if the group total is 0
UPDATE tickets
SET tk_percent_tank_diff = 0
WHERE tk_eod_datetime = @eod AND tk_type = 1 AND tk_group_total_tank_diff = 0


 -- CALCS FOR NOW UNTIL WE GET ACTUAL PIPELINE TIMES**************************************************************************************************************************************

INSERT INTO dbo.tickets_meter
           (_key
           ,ticket_key
           ,eod_datetime
           ,meter_key
           ,skid_key
           ,open_totalizer
           ,close_totalizer
           ,meter_error)

---- pipeline transactions
select newid(), 
	   t._key, 
	   t.tk_eod_datetime, 
	   m._key, 
	   m.skid_key, 
	   mr_open.meter_totalizer as open_totalizer, 
	   mr_close.meter_totalizer as close_totalizer,
	   CASE WHEN mr_close.meter_totalizer < mr_open.meter_totalizer THEN 1 ELSE 0 END as meter_error
	   --mr_close.meter_totalizer - mr_open.meter_totalizer as meter_diff, 
	   --, p.description, t.tk_start_datetime, t.tk_end_datetime, m.meter_name, s.skid_description, 
FROM tickets t INNER JOIN 
	 pipeline p ON t.tk_from_key = p._key INNER JOIN 
	 meters m ON m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key INNER JOIN 
	 meter_readings mr_open ON mr_open.meter_key = m._key AND mr_open.meter_timestamp = DATEADD(day, -1, t.tk_eod_datetime)   INNER JOIN 
	 meter_readings mr_close ON mr_close.meter_key = m._key AND mr_close.meter_timestamp = t.tk_eod_datetime 
WHERE tk_eod_datetime = @eod AND tk_type = 1




UPDATE t
SET tk_total_meter_diff = t2.meter_amt
FROM (SELECT ticket_key, (SUM(tm.close_totalizer) -  SUM(tm.open_totalizer) ) as meter_amt
	  FROM tickets_meter tm INNER JOIN 
	  	 tickets t ON t._key = tm.ticket_key
	  WHERE t.tk_eod_datetime = @eod AND t.tk_type = 1
	  GROUP BY ticket_key) t2 INNER JOIN 
	  tickets t ON t._key = t2.ticket_key

UPDATE tickets
SET tk_meter_volume = tk_total_meter_diff * tk_percent_tank_diff
WHERE tk_eod_datetime = @eod and tk_type = 1

UPDATE t
SET tk_meter_error = 1
--SELECT tm.ticket_key
FROM tickets_meter tm INNER JOIN 
	  	 tickets t ON t._key = tm.ticket_key
WHERE t.tk_eod_datetime = @eod AND t.tk_type = 1 AND tm.meter_error = 1

UPDATE  tickets
SET tk_status = 2
WHERE tk_eod_datetime = @eod AND tk_type = 1

UPDATE  tickets
SET tk_final_gsv = tk_meter_volume
WHERE tk_eod_datetime = @eod AND tk_type = 1



 -- CALCS WHEN WE GET ACTUAL PIPELINE TIMES**************************************************************************************************************************************
-- INSERT INTO dbo.tickets_meter
--           (_key
--           ,ticket_key
--           ,eod_datetime
--           ,meter_key
--           ,skid_key
--           ,open_totalizer
--           ,close_totalizer
--           ,meter_error)

------ pipeline transactions
--select newid(), 
--	   t._key, 
--	   t.tk_eod_datetime, 
--	   m._key, 
--	   m.skid_key, 
--	   mr_open.meter_totalizer as open_totalizer, 
--	   mr_close.meter_totalizer as close_totalizer,
--	   CASE WHEN mr_close.meter_totalizer < mr_open.meter_totalizer THEN 1 ELSE 0 END as meter_error
--	   --mr_close.meter_totalizer - mr_open.meter_totalizer as meter_diff, 
--	   --, p.description, t.tk_start_datetime, t.tk_end_datetime, m.meter_name, s.skid_description, 
--FROM tickets t INNER JOIN 
--	 pipeline p ON t.tk_from_key = p._key INNER JOIN 
--	 meters m ON m.pipeline_key = p._key INNER JOIN 
--	 skids s ON s._key = m.skid_key INNER JOIN 
--	 meter_readings mr_open ON mr_open.meter_key = m._key AND mr_open.meter_timestamp = t.tk_start_datetime  INNER JOIN 
--	 meter_readings mr_close ON mr_close.meter_key = m._key AND mr_close.meter_timestamp = t.tk_end_datetime 
--WHERE tk_eod_datetime = @eod AND tk_type = 1


--UPDATE t
--SET tk_meter_volume = t2.meter_amt
--FROM (SELECT ticket_key, (SUM(tm.close_totalizer) -  SUM(tm.open_totalizer) ) as meter_amt
--	  FROM tickets_meter tm INNER JOIN 
--	  	 tickets t ON t._key = tm.ticket_key
--	  WHERE t.tk_eod_datetime = @eod AND t.tk_type = 1
--	  GROUP BY ticket_key) t2 INNER JOIN 
--	  tickets t ON t._key = t2.ticket_key

--UPDATE t
--SET tk_total_meter_diff = pa.gsv_bbls
----SELECT t._key, pa.gsv_bbls
--FROM tickets t INNER JOIN 
--	 pipeline_actuals pa ON t.tk_from_key = pa.pipeline_key AND t.tk_eod_datetime = pa.eod_datetime
--WHERE t.tk_eod_datetime = @eod AND t.tk_type = 1 

--UPDATE t
--SET tk_meter_error = 1
----SELECT tm.ticket_key
--FROM tickets_meter tm INNER JOIN 
--	  	 tickets t ON t._key = tm.ticket_key
--WHERE t.tk_eod_datetime = @eod AND t.tk_type = 1 AND tm.meter_error = 1

-- ********************************************************************************************************************************************************************************




---- ***********************************************************************************************************************   TRANSFERS  *******************************************************************************

 -- remove any data in the final columns from the day
 UPDATE tickets 
 SET tk_tank_close_timestamp = NULL,
	 tk_tank_open_gsv = NULL,
	 tk_tank_close_gsv = NULL,
	 tk_tank_change_gsv = NULL,
	 tk_group_total_tank_diff = NULL,
	 tk_percent_tank_diff = NULL,
	 tk_total_meter_diff = NULL,
	 tk_meter_volume = NULL, 
	 tk_status = 1
 WHERE tk_eod_datetime = @eod  AND tk_type = 3

-- remove meter records to re-pull
DELETE FROM tickets_meter WHERE eod_datetime = @eod AND ticket_key IN (SELECT _key FROM tickets WHERE  tk_eod_datetime = @eod  AND tk_type = 3)



-- if the end time is not end of day at 7 am, add some time to the tank stop time so the tank can settle
-- settle time is set int he sites table 
UPDATE t
SET tk_tank_close_timestamp = tk_end_datetime
--CASE WHEN CONVERT(TIME, tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN tk_eod_datetime
--							  ELSE DATEADD(HOUR, s.tank_settle_time_hours, tk_end_datetime) END
FROM tickets t INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod  AND tk_type = 3

-- check for another start using the same from tank so we can grab the correct close and not wait an hour
--UPDATE t
----SELECT t._key , t.tk_end_datetime, t.tk_tank_close_timestamp, t2.tk_start_datetime, 
--SET tk_tank_close_timestamp = CASE WHEN t.tk_tank_close_timestamp >t2.tk_start_datetime THEN t2.tk_start_datetime ELSE t.tk_tank_close_timestamp END 
--FROM tickets t INNER JOIN 
--	(SELECT * FROM tickets WHERE tk_eod_datetime = @eod  AND tk_type = 3 ) t2 ON t.tk_eod_datetime = t2.tk_eod_datetime AND t.tk_from_key = t2.tk_from_key AND t2.tk_day_sequence_number = t.tk_day_sequence_number + 1
--WHERE t.tk_eod_datetime = @eod  AND t.tk_type = 3 


-- update  tank readings ***************** USES THE SOURCE TANK AS TANK DIFF OF RECORD******************
UPDATE t
SET tk_tank_open_gsv = tr_open.tank_gsv,
	tk_tank_open_level = tr_open.tank_level,
	tk_tank_close_gsv = tr_close.tank_gsv,
    tk_tank_close_level = tr_close.tank_level
--select t.tk_to_key, tr_open.tank_gsv as start_gsv, tr_close.tank_gsv as close_gsv, tr_close.tank_gsv - tr_open.tank_gsv as tank_gsv_diff, tanks.tank_description
from tickets t INNER JOIN 
     tank_readings tr_open ON t.tk_from_key = tr_open.tank_key AND t.tk_start_datetime = tr_open.tank_timestamp INNER JOIN 
	 tank_readings tr_close ON tr_close.tank_key = t.tk_from_key AND  t.tk_tank_close_timestamp = tr_close.tank_timestamp INNER JOIN 
	 tanks on tanks._key = t.tk_to_key INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod AND tk_type = 3

UPDATE t
SET tk_transfer_to_tank_open_gsv = tr_open.tank_gsv,
	tk_transfer_to_tank_open_level = tr_open.tank_level,
	tk_transfer_to_tank_close_gsv = tr_close.tank_gsv,
    tk_transfer_to_tank_close_level = tr_close.tank_level
--select t.tk_to_key, tr_open.tank_gsv as start_gsv, tr_close.tank_gsv as close_gsv, tr_close.tank_gsv - tr_open.tank_gsv as tank_gsv_diff, tanks.tank_description
from tickets t INNER JOIN 
     tank_readings tr_open ON t.tk_to_key = tr_open.tank_key AND t.tk_start_datetime = tr_open.tank_timestamp INNER JOIN 
	 tank_readings tr_close ON tr_close.tank_key = t.tk_to_key AND  t.tk_tank_close_timestamp = tr_close.tank_timestamp INNER JOIN 
	 tanks on tanks._key = t.tk_to_key INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod AND tk_type = 3


-- update the tank diff 
UPDATE tickets 
SET tk_tank_change_gsv = -1 * (tk_tank_close_gsv - tk_tank_open_gsv)
WHERE tk_eod_datetime = @eod AND tk_type = 3


 -- CALCS FOR NOW UNTIL WE GET ACTUAL PIPELINE TIMES**************************************************************************************************************************************
 
UPDATE  tickets
SET tk_final_gsv = tk_tank_change_gsv
WHERE tk_eod_datetime = @eod AND tk_type = 3

UPDATE t
SET tk_batch_id = (out_tank.tank_pi_name) +'-'+ in_tank.tank_pi_name +'-'+ convert(varchar, t.tk_eod_datetime, 12)
FROM tickets t INNER JOIN 
	 tanks out_tank  ON t.tk_from_key = out_tank._key INNER JOIN 
	 tanks in_tank ON t.tk_to_key = in_tank._key
WHERE t.tk_eod_datetime = @eod AND t.tk_type = 3


UPDATE  tickets
SET tk_status = 2
WHERE tk_eod_datetime = @eod AND tk_type = 3

 -- CALCS WHEN WE GET ACTUAL PIPELINE TIMES**************************************************************************************************************************************

--UPDATE t
--SET tk_meter_volume = t2.meter_amt
--FROM (SELECT ticket_key, (SUM(tm.close_totalizer) -  SUM(tm.open_totalizer) ) as meter_amt
--	  FROM tickets_meter tm INNER JOIN 
--	  	 tickets t ON t._key = tm.ticket_key
--	  WHERE t.tk_eod_datetime = @eod AND t.tk_type = 3
--	  GROUP BY ticket_key) t2 INNER JOIN 
--	  tickets t ON t._key = t2.ticket_key

--UPDATE t
--SET tk_total_meter_diff = pa.gsv_bbls
----SELECT t._key, pa.gsv_bbls
--FROM tickets t INNER JOIN 
--	 pipeline_actuals pa ON t.tk_from_key = pa.pipeline_key AND t.tk_eod_datetime = pa.eod_datetime
--WHERE t.tk_eod_datetime = @eod AND t.tk_type = 3 

--UPDATE t
--SET tk_meter_error = 1
----SELECT tm.ticket_key
--FROM tickets_meter tm INNER JOIN 
--	  	 tickets t ON t._key = tm.ticket_key
--WHERE t.tk_eod_datetime = @eod AND t.tk_type = 3 AND tm.meter_error = 1


-- ********************************************************************************************************************************************************************************





---- ***********************************************************************************************************************   VESSEL  *******************************************************************************

 -- remove any data in the final columns from the day
 UPDATE tickets 
 SET tk_tank_close_timestamp = NULL,
	 tk_tank_open_gsv = NULL,
	 tk_tank_close_gsv = NULL,
	 tk_tank_change_gsv = NULL,
	 tk_group_total_tank_diff = NULL,
	 tk_percent_tank_diff = NULL,
	 tk_total_meter_diff = NULL,
	 tk_meter_volume = NULL, 
	 tk_status = 1
 WHERE tk_eod_datetime = @eod  AND tk_type = 2

-- remove meter records to re-pull
DELETE FROM tickets_meter WHERE eod_datetime = @eod AND ticket_key IN (SELECT _key FROM tickets WHERE  tk_eod_datetime = @eod  AND tk_type = 2)

-- update meter volume and dates from berth tracking 
UPDATE tk
SET tk_total_meter_diff = (pa.berth_meter_bbls ), 
	tk_start_datetime = pa.berth_start, 
	tk_end_datetime = pa.berth_stop, 
	tk_batch_id = berth_batch_id
--SELECT tk.total_tank_diff_nsv, pa.tov_bbls
FROM tickets tk INNER JOIN 	 
	(SELECT SUM(berth_meter_bbls) as berth_meter_bbls, eod_datetime	 , berth_key, berth_start, berth_stop, berth_batch_id
	FROM berth_activity 
	WHERE eod_datetime = @eod
	GROUP BY eod_datetime, berth_key,berth_start, berth_stop, berth_batch_id) pa ON tk.tk_eod_datetime = pa.eod_datetime AND tk.tk_to_key = pa.berth_key
WHERE tk.tk_eod_datetime = @eod  AND tk_type = 2


-- Updates the start and end time based on any other moveemnts involving that tank in teh same day 
--SELECT t.tk_start_datetime, t.tk_end_datetime, t.tk_from_key, t.tk_to_key, t2.tk_start_datetime, t2.tk_end_datetime, t2.tk_from_key, t2.tk_to_key, 
UPDATE t
SET tk_start_datetime = CASE WHEN t2.tk_end_datetime > t.tk_start_datetime AND t2.tk_end_datetime <  t.tk_end_datetime THEN t2.tk_end_datetime ELSE t.tk_start_datetime END ,
	tk_end_datetime = CASE WHEN t2.tk_start_datetime < t.tk_end_datetime AND t2.tk_start_datetime >  t.tk_start_datetime THEN t2.tk_start_datetime ELSE t.tk_end_datetime END 
	   --,   CASE WHEN t2.tk_start_datetime < t.tk_end_datetime THEN t2.tk_start_datetime ELSE t.tk_end_datetime END as new_start 
FROM 
tickets t INNER JOIN 
(SELECT tk_from_key, tk_to_key, tk_start_datetime, tk_end_datetime, tk_eod_datetime FROM tickets WHERE tk_eod_datetime = @eod  AND tk_type <> 2 AND 
			(tk_from_key IN ((SELECT tk_from_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ) OR 
			 tk_to_key IN ((SELECT tk_from_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ))) t2 ON t.tk_eod_datetime = t2.tk_eod_datetime AND (t.tk_from_key = t2.tk_to_key OR t.tk_from_key = t2.tk_from_key)
WHERE t.tk_eod_datetime = @eod  AND t.tk_type = 2


-- Update the tank close timestamp 
--SELECT t.tk_start_datetime, t.tk_end_datetime, t.tk_from_key, t.tk_to_key, t2.tk_start_datetime, t2.tk_end_datetime, t2.tk_from_key, t2.tk_to_key, 
UPDATE t
SET tk_tank_close_timestamp =  CASE WHEN t.tk_end_datetime = t2.tk_start_datetime THEN t.tk_end_datetime
								  WHEN CONVERT(TIME, t.tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN t.tk_eod_datetime
								  ELSE DATEADD(HOUR, s.tank_settle_time_hours, t.tk_end_datetime) END
FROM 
tickets t INNER JOIN 
(SELECT tk_from_key, tk_to_key, tk_start_datetime, tk_end_datetime, tk_eod_datetime FROM tickets WHERE tk_eod_datetime = @eod  AND tk_type <> 2 AND 
			(tk_from_key IN ((SELECT tk_from_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ) OR 
			 tk_to_key IN ((SELECT tk_from_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ))) t2 ON t.tk_eod_datetime = t2.tk_eod_datetime AND (t.tk_from_key = t2.tk_to_key OR t.tk_from_key = t2.tk_from_key) INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE t.tk_eod_datetime = @eod  AND t.tk_type = 2  AND tk_tank_close_timestamp IS NULL

UPDATE t
SET tk_tank_close_timestamp = CASE WHEN CONVERT(TIME, tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN tk_eod_datetime
							  ELSE DATEADD(HOUR, s.tank_settle_time_hours, tk_end_datetime) END
FROM tickets t INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod  AND tk_type = 2 AND tk_tank_close_timestamp IS NULL






-- update  tank readings 
UPDATE t
SET tk_tank_open_gsv = tr_open.tank_gsv,
tk_tank_open_level = tr_open.tank_level,
	tk_tank_close_gsv = tr_close.tank_gsv,
tk_tank_close_level = tr_close.tank_level
--select t.tk_to_key, tr_open.tank_gsv as start_gsv, tr_close.tank_gsv as close_gsv, tr_close.tank_gsv - tr_open.tank_gsv as tank_gsv_diff, tanks.tank_description
from tickets t INNER JOIN 
     tank_readings tr_open ON t.tk_from_key = tr_open.tank_key AND t.tk_start_datetime = tr_open.tank_timestamp INNER JOIN 
	 tank_readings tr_close ON tr_close.tank_key = t.tk_from_key AND  t.tk_tank_close_timestamp = tr_close.tank_timestamp INNER JOIN 
	 tanks on tanks._key = t.tk_from_key INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod AND tk_type = 2


-- update the tank diff 
UPDATE tickets 
SET tk_tank_change_gsv = (tk_tank_close_gsv - tk_tank_open_gsv) * -1
WHERE tk_eod_datetime = @eod AND tk_type = 2


-- update the total tank change by berth or TO key
UPDATE tk
SET tk_group_total_tank_diff = tk2.tank_change_gsv
--SELECT tk.eod_datetime, tk.tank_key, tk.berth_key, tk.tank_change_nsv, tk.total_tank_diff_nsv, tk2.tank_change_nsv
FROM tickets tk INNER JOIN 
	 (SELECT SUM(tk_tank_change_gsv) as tank_change_gsv , tk_to_key, tk_eod_datetime
	  FROM tickets 
	  WHERE tk_eod_datetime = @eod AND tk_type = 2
	  GROUP BY tk_eod_datetime, tk_to_key) tk2 ON tk.tk_to_key = tk2.tk_to_key AND tk.tk_eod_datetime = tk.tk_eod_datetime
WHERE tk.tk_eod_datetime = @eod AND tk_type = 2

-- update the percent where the group total is not 0 (avoid divide by 0 error)
UPDATE tickets
SET tk_percent_tank_diff = (tk_tank_change_gsv / tk_group_total_tank_diff)
WHERE tk_eod_datetime = @eod AND tk_type = 2 AND tk_group_total_tank_diff <> 0

-- update percent to 0 if the group total is 0
UPDATE tickets
SET tk_percent_tank_diff = 0
WHERE tk_eod_datetime = @eod AND tk_type = 2 AND tk_group_total_tank_diff = 0

-- insert meter records for the vessel transactions
INSERT INTO dbo.tickets_meter
           (_key
           ,ticket_key
           ,eod_datetime
           ,meter_key
           ,skid_key
           ,open_totalizer
           ,close_totalizer
           ,meter_error)
select newid(), 
	   t._key, 
	   t.tk_eod_datetime, 
	   vam.meter_key, 
	   vam.skid_key, 
	   vam.open_totalizer, 
	   vam.close_totalizer,
	   CASE WHEN vam.close_totalizer < vam.open_totalizer THEN 1 ELSE 0 END as meter_error
FROM tickets t INNER JOIN 
	 valve_activity va ON va.valve_skid_batch_id = t.tk_batch_id AND va.valve_eod_datetime = t.tk_eod_datetime INNER JOIN 
	 valve_activity_meter vam ON vam.valve_activity_key = va._key
WHERE tk_eod_datetime = @eod AND tk_type = 2


-- -- CALCS FOR Meter adjusted tank amounts    **************************************************************************************************************************************


UPDATE tickets
SET tk_meter_volume = tk_total_meter_diff * tk_percent_tank_diff
WHERE tk_eod_datetime = @eod and tk_type = 2

--UPDATE t
--SET tk_meter_error = 1
----SELECT tm.ticket_key
--FROM tickets_meter tm INNER JOIN 
--	  	 tickets t ON t._key = tm.ticket_key
--WHERE t.tk_eod_datetime = @eod AND t.tk_type = 2 AND tm.meter_error = 1

UPDATE  tickets
SET tk_status = 2
WHERE tk_eod_datetime = @eod AND tk_type = 2

UPDATE  tickets
SET tk_final_gsv = tk_meter_volume
WHERE tk_eod_datetime = @eod AND tk_type = 2




---- ***********************************************************************************************************************   TEST RESULTS  *******************************************************************************



-- test results pipeline
SELECT t.tk_eod_datetime as [Date], 
	   p.description as [Pipeline] , 
	   ta.tank_name as [Tank], 
	   t.tk_scheduled_volume as [Scheduled], 
	   ROUND(t.tk_tank_change_gsv,0) [Actual Tank Change(gsv)],  
	   ROUND(t.tk_final_gsv,0) [Actual Final (gsv)], 
	   ROUND(t.tk_scheduled_volume - t.tk_final_gsv,0) as [Diff],
	   ROUND((t.tk_scheduled_volume * .989) - t.tk_final_gsv,0) as [Diff est gsv]
FROM tickets t INNER JOIN 
	 tanks ta ON t.tk_to_key = ta._key INNER JOIN 
	 pipeline p ON p._key = t.tk_from_key
WHERE tk_eod_datetime = @eod AND tk_type = 1 -- pipeline


-- test vessel results 
SELECT t.tk_eod_datetime as [Date], 
	   b.berth_description as [Berth] , 
	   ta.tank_name as [Tank], 
	   t.tk_scheduled_volume as [Scheduled], 
	   ROUND(t.tk_tank_change_gsv,0) [Actual Tank Change(gsv)],  
	   ROUND(t.tk_final_gsv,0) [Actual Final (gsv)], 
	   ROUND(t.tk_scheduled_volume - t.tk_final_gsv,0) as [Diff],
	   ROUND((t.tk_scheduled_volume * .989) - t.tk_final_gsv,0) as [Diff est gsv]
FROM tickets t INNER JOIN 
	 tanks ta ON t.tk_from_key = ta._key INNER JOIN 
	 berths b ON b._key = t.tk_to_key
WHERE tk_eod_datetime = @eod AND tk_type = 2 -- vessel



-- test transfer results 
SELECT t.tk_eod_datetime as [Date], 
	   from_ta.tank_name as [From Tank], 
	   to_ta.tank_name as [To Tank], 
	   t.tk_scheduled_volume as [Scheduled], 
	   ROUND(t.tk_tank_change_gsv,0) [Actual Tank Change(gsv)],  
	   ROUND(t.tk_final_gsv,0) [Actual Final (gsv)], 
	   ROUND(t.tk_scheduled_volume - t.tk_final_gsv,0) as [Diff],
	   ROUND((t.tk_scheduled_volume * .989) - t.tk_final_gsv,0) as [Diff est gsv]
FROM tickets t INNER JOIN 
	 tanks from_ta ON t.tk_from_key = from_ta._key INNER JOIN 
	 tanks to_ta ON t.tk_to_key = to_ta._key
WHERE tk_eod_datetime = @eod AND tk_type = 3 -- transfer


-- Inventory Report
---- shipment by tank
--SELECT t.tank_name,  tt.tk_eod_datetime as eod_datetime, (SUM(tt.tk_final_gsv)*-1) as bbls, tt.tk_from_key as tank_key
--		FROM tickets tt INNER JOIN 
--			 tanks t ON tt.tk_from_key = t._key INNER JOIN 
--			 berths b ON tt.tk_to_key = b._key
--		WHERE tt.tk_eod_datetime = @eod  AND tk_type = 2
--		GROUP BY t.tank_name, b.berth_description, tt.tk_eod_datetime,  tt.tk_from_key, tt.tk_to_key 
---- receipt by tank
--		SELECT t.tank_name, tt.tk_eod_datetime as eod_datetime, SUM(tt.tk_final_gsv) as bbls, tt.tk_to_key as tank_key
--			FROM tickets tt INNER JOIN 
--				 pipeline p ON tt.tk_from_key = p._key INNER JOIN 
--				 tanks t ON t._key = tt.tk_to_key
--			WHERE tk_eod_datetime = @eod  AND tk_type = 1
--			GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_to_key

---- transfers
--SELECT tank_name , tk_eod_datetime, SUM(bbls) as bbls, tank_key
--FROM (
--SELECT t.tank_name, tt.tk_eod_datetime, (SUM(tt.tk_final_gsv)* -1) as bbls,  tt.tk_from_key as tank_key
--FROM tickets tt INNER JOIN 
--	 tanks t ON t._key = tt.tk_from_key
--WHERE tk_eod_datetime = @eod AND tt.tk_type = 3
--GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_from_key UNION
--SELECT t.tank_name, tt.tk_eod_datetime, SUM(tt.tk_final_gsv) as bbls,  tt.tk_to_key  as tank_key
--FROM tickets tt INNER JOIN 
--	 tanks t ON t._key = tt.tk_to_key
--WHERE tk_eod_datetime = @eod AND tt.tk_type = 3
--GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_to_key) transfers
--GROUP BY  tank_name , tk_eod_datetime,  tank_key



SELECT t.tank_name as Tank, ROUND(ISNULL(ta.open_gsv,0),0) as Beginning , 
	   (ROUND(ISNULL(shipments.Bbls,0),0)*-1) as Shipments, 
	   ROUND(ISNULL(receipts.bbls,0),0) as Receipts, 
	   ROUND(ISNULL(transfers.bbls,0),0) as Transfers,
	   ROUND(ISNULL(ta.open_gsv,0) + (ISNULL(shipments.Bbls,0)*-1) + ISNULL(receipts.bbls,0) + ISNULL(transfers.bbls,0),0)  as [Book Close], 
	   ROUND(ISNULL(ta.close_gsv,0),0) as [Physical Close], 
	   ROUND(ISNULL(ta.close_gsv,0)-(ISNULL(ta.open_gsv,0) + (ISNULL(shipments.Bbls,0)*-1) + ISNULL(receipts.bbls,0) + ISNULL(transfers.bbls,0)),0) as [Gain Loss]
FROM  tank_actuals ta INNER JOIN 
	  tanks t ON ta.tank_key = t._key LEFT OUTER JOIN 
	  (SELECT t.tank_name,  tt.tk_eod_datetime as eod_datetime, SUM(tt.tk_final_gsv) as bbls, tt.tk_from_key as tank_key
		FROM tickets tt INNER JOIN 
			 tanks t ON tt.tk_from_key = t._key INNER JOIN 
			 berths b ON tt.tk_to_key = b._key
		WHERE tt.tk_eod_datetime = @eod  AND tk_type = 2
		GROUP BY t.tank_name, b.berth_description, tt.tk_eod_datetime,  tt.tk_from_key, tt.tk_to_key)
    shipments ON ta.tank_key = shipments.tank_key AND ta.eod_datetime = shipments.eod_datetime LEFT OUTER JOIN 
		(SELECT t.tank_name, tt.tk_eod_datetime as eod_datetime, SUM(tt.tk_final_gsv) as bbls, tt.tk_to_key as tank_key
			FROM tickets tt INNER JOIN 
				 pipeline p ON tt.tk_from_key = p._key INNER JOIN 
				 tanks t ON t._key = tt.tk_to_key
			WHERE tk_eod_datetime = @eod  AND tk_type = 1
			GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_to_key) 
	receipts ON receipts.tank_key = ta.tank_key AND receipts.eod_datetime = ta.eod_datetime LEFT OUTER JOIN 
			(SELECT tank_name , tk_eod_datetime, SUM(bbls) as bbls, tank_key
FROM (
SELECT t.tank_name, tt.tk_eod_datetime, (SUM(tt.tk_final_gsv)* -1) as bbls,  tt.tk_from_key as tank_key
FROM tickets tt INNER JOIN 
	 tanks t ON t._key = tt.tk_from_key
WHERE tk_eod_datetime = @eod AND tt.tk_type = 3
GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_from_key UNION
SELECT t.tank_name, tt.tk_eod_datetime, SUM(tt.tk_final_gsv) as bbls,  tt.tk_to_key  as tank_key
FROM tickets tt INNER JOIN 
	 tanks t ON t._key = tt.tk_to_key
WHERE tk_eod_datetime = @eod AND tt.tk_type = 3
GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_to_key) transfers
GROUP BY  tank_name , tk_eod_datetime,  tank_key
) transfers ON ta.tank_key = transfers.tank_key AND ta.eod_datetime = transfers.tk_eod_datetime
WHERE ta.eod_datetime = @eod
ORDER BY Tank


