
DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)
--DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 04, 23, 7, 0, 0, 0)
-- TankToVessel 
-- PipelineToTank
--  TankToTankCharge
-- TankToTankNoCharge
DELETE FROM Moda_Final_Tickets WHERE eod_datetime = @eod

-- INSERT PIPELINE TICKETS 
INSERT INTO [dbo].[Moda_Final_Tickets]
           ([_key]
           ,[movement_type]
           ,[batch_id]
           ,[ticket_id]
           ,[from_location_id]
           ,[to_location_id]
           ,[product_id]
           ,[gsv]
           ,[start_datetime]
           ,[stop_datetime]
		   ,eod_datetime
           ,[synthesis_received])
SELECT     newid(), -- ([_key]
          'PipelineToTank', -- ,[movement_type]
           CONVERT(varchar(10), pt.eod_datetime, 110) + '-' + t.tank_pi_name,  -- ,[batch_id]
           CONVERT(varchar(10), pt.eod_datetime, 110) + '-' + t.tank_pi_name,  -- ,[ticket_id]
           p.external_id, -- ,[from_location_id]
           t.tank_external_id_location, -- ,[to_location_id]
           'WTI', -- ,[product_id]
           pt.tank_meter_bbls_using_tank_nsv, -- ,[nsv]
           DATEADD( day, -1, pt.eod_datetime),-- ,[start_datetime]
           pt.eod_datetime, -- ,[stop_datetime]
           pt.eod_datetime, -- ,[eod_datetime]
           NULL-- ,[synthesis_received])
FROM pipeline_to_tank_tickets pt INNER JOIN 
	 pipeline p ON p._key = pt.pipeline_key INNER JOIN 
	 tanks t ON t._key = pt.tank_key
WHERE eod_datetime = @eod AND pt.pipeline_key <> '00000000-0000-0000-0000-000000000000'
	  AND pt.tank_meter_bbls_using_tank_nsv <> 0


INSERT INTO [dbo].[Moda_Final_Tickets]
           ([_key]
           ,[movement_type]
           ,[batch_id]
           ,[ticket_id]
           ,[from_location_id]
           ,[to_location_id]
           ,[product_id]
           ,[gsv]
           ,[start_datetime]
           ,[stop_datetime]
		   ,eod_datetime
           ,[synthesis_received])
SELECT     newid(), -- ([_key]
          'TankToVessel', -- ,[movement_type]
           tb.batch_id,  -- ,[batch_id]
           CONVERT(varchar(10), tb.eod_datetime, 110) + '-' + t.tank_pi_name,  -- ,[ticket_id]
           t.tank_external_id_location, -- ,[from_location_id]
           b.berth_external_code, -- ,[to_location_id]
           'WTI', -- ,[product_id]
           (tb.berth_meter_bbls_using_tank_nsv) * -1, -- ,[nsv]
           tb.start_datetime,-- ,[start_datetime]
           tb.end_datetime, -- ,[stop_datetime]
		   tb.eod_datetime,  --eod_datetime
           NULL-- ,[synthesis_received])
FROM tank_to_berth_tickets tb INNER JOIN 
	 tanks t ON tb.tank_key = t._key INNER JOIN 
	 berths b ON tb.berth_key = b._key
WHERE eod_datetime = @eod AND tb.berth_meter_bbls_using_tank_nsv <> 0 AND tb.berth_key <> '00000000-0000-0000-0000-000000000000'
--GROUP BY t.tank_name, b.berth_description, tb.eod_datetime, tb.tank_key, tb.berth_key



--DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 04, 17, 7, 0, 0, 0)
DECLARE @transfer TABLE (tank_key uniqueidentifier, bbls float, eod_datetime datetime, compare_bbls float)


-- tank to tank receipts
INSERT INTO @transfer(tank_key, bbls, eod_datetime, compare_bbls)
SELECT t._key, tt.brandis_value, eod_datetime, tt.brandis_value
FROM testing_tank_transfers tt INNER JOIN 
	 tanks t ON tt.tank_key = t._key
WHERE brandis_value > 0 and tt.eod_datetime = @eod


-- tank to tank shipments
INSERT INTO @transfer(tank_key, bbls, eod_datetime, compare_bbls)
SELECT t._key, tt.brandis_value, eod_datetime, tt.brandis_value * -1
FROM testing_tank_transfers tt INNER JOIN 
	 tanks t ON tt.tank_key = t._key
WHERE brandis_value < 0 and tt.eod_datetime = @eod



INSERT INTO [dbo].[Moda_Final_Tickets]
           ([_key]
           ,[movement_type]
           ,[batch_id]
           ,[ticket_id]
           ,[from_location_id]
           ,[to_location_id]
           ,[product_id]
           ,[gsv]
           ,[start_datetime]
           ,[stop_datetime]
		   ,eod_datetime
           ,[synthesis_received])
SELECT     newid(), -- ([_key]
          'TankToTankNoCharge', -- ,[movement_type]
           CONVERT(varchar(10), rec.eod_datetime, 110) + '-' + rect.tank_pi_name,  -- ,[batch_id]
           CONVERT(varchar(10), rec.eod_datetime, 110) + '-' + rect.tank_pi_name,  -- ,[ticket_id]
           shipt.tank_external_id_location, -- ,[from_location_id]
           rect.tank_external_id_location, -- ,[to_location_id]
           'WTI', -- ,[product_id]
           rec.bbls, -- ,[nsv]
           DATEADD( day, -1, rec.eod_datetime),-- ,[start_datetime]
           rec.eod_datetime, -- ,[stop_datetime]
           rec.eod_datetime, -- ,[eod_datetime]
           NULL-- ,[synthesis_received])
--SELECT rec.eod_datetime, rec.bbls, rec.tank_key as ToTank, ship.tank_key as FromTank
FROM (SELECT *
		FROM @transfer
		WHERE bbls > 0)rec INNER JOIN 
	(SELECT *
		FROM @transfer
		WHERE bbls <0 )ship ON rec.eod_datetime = ship.eod_datetime AND ship.compare_bbls  = rec.compare_bbls INNER JOIN 
		tanks rect ON rec.tank_key = rect._key INNER JOIN 
		tanks shipt ON ship.tank_key = shipt._key

--UPDATE em 
--SET batch_id = m.batch_id
----select *
--FROM EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets] em INNER JOIN 
--Moda_Final_Tickets m on em._key = m._key

-- move to emerson 

INSERT INTO EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets]
           ([_key]
           ,[movement_type]
           ,[batch_id]
           ,[ticket_id]
           ,[from_location_id]
           ,[to_location_id]
           ,[product_id]
           ,[gsv]
           ,[start_datetime]
           ,[stop_datetime]
           ,[eod_datetime]
           ,[synthesis_received])
   SELECT [_key]
           ,[movement_type]
           ,[batch_id]
           ,[ticket_id]
           ,[from_location_id]
           ,[to_location_id]
           ,[product_id]
           ,[gsv]
           ,[start_datetime]
           ,[stop_datetime]
           ,[eod_datetime]
           ,[synthesis_received]
	FROM [Moda_Final_Tickets]
	WHERE eod_datetime = @eod



