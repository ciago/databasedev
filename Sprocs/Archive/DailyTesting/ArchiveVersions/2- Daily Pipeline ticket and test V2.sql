--***********************************************************************************************************
-- STEP 1:  copy Brandi's excel into the tables testing_pipeline_to_tank, testing_tank_to_berth, testing_tank_transfers
--***********************************************************************************************************
DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 05, 02, 7, 0, 0, 0)
--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)

--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, DATEADD(day, -1,getdate())), 
--											DATEPART(MONTH, DATEADD(day, -1,getdate())), 
--											DATEPART(DAY, DATEADD(day, -1,getdate())), 7, 0, 0, 0)
 
--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, DATEADD(day, -2,getdate())), 
--											DATEPART(MONTH, DATEADD(day, -2,getdate())), 
--											DATEPART(DAY, DATEADD(day, -2,getdate())), 7, 0, 0, 0)
 
--***********************************************************************************************************

--***********************************************************************************************************
-- STEP 3:  Run this to calculate and create pipeline_to_tank_tickets, 
--          fill the comparing columns in the testing_pipeline_to_tank
--***********************************************************************************************************

 -- reove anything there from the day
 DELETE FROM tickets WHERE tk_eod_datetime = @eod



-- insert a row into this table for any tanks not involved in a pipeline but have a transfer 
--INSERT INTO pipeline_to_tank_tickets (eod_datetime, tank_key, pipeline_key)
--SELECT DISTINCT tt.eod_datetime, tt.tank_key, '00000000-0000-0000-0000-000000000000'
--FROM testing_tank_transfers  tt LEFT OUTER JOIN 
--	  pipeline_to_tank_tickets pt ON pt.tank_key = tt.tank_key AND pt.eod_datetime = tt.eod_datetime
--WHERE tt.eod_datetime = @eod AND brandis_value <> 0 AND pt.tank_key IS NULL

--UPDATE tk
--SET tank_transfer_amt = transfers.brandis_value
----SELECT tk.tank_key, tt.brandis_value
--FROM pipeline_to_tank_tickets tk INNER JOIN 
--	(SELECT SUM(tt.brandis_value) as brandis_value, tk.tank_key, tk.eod_datetime
--	 FROM pipeline_to_tank_tickets tk INNER JOIN 
--	 	 testing_tank_transfers tt ON tk.eod_datetime = tt.eod_datetime AND tk.tank_key = tt.tank_key
--	 WHERE tk. eod_datetime = @eod  
--	 GROUP BY tk.tank_key, tk.eod_datetime)transfers ON tk.eod_datetime = transfers.eod_datetime AND tk.tank_key = transfers.tank_key
--WHERE tk. eod_datetime = @eod      

--UPDATE pipeline_to_tank_tickets
--SET tank_transfer_amt = 0 
--WHERE tank_transfer_amt IS NULL

 -- UPDATE tank changes ( taking out anything that was a transfer so we are left with only pipeline )
 -- TODO:  account for any barge changes too??


 --  if the actual start and stop times crossed 7 am, split into 2 records 


 -- update the eod for actual times
 UPDATE tickets 
SET tk_eod_datetime = CASE WHEN cast( tk_start_datetime as time) >= cast('07:00 AM' as TIME) 
	        THEN DATEADD(day, 1, SMALLDATETIMEFROMPARTS(YEAR(tk_start_datetime), MONTH(tk_start_datetime), DAY(tk_start_datetime), 07, 00)) 
			ELSE SMALLDATETIMEFROMPARTS(YEAR(tk_start_datetime), MONTH(tk_start_datetime), DAY(tk_start_datetime), 07, 00) END
WHERE tk_eod_datetime = @eod 



 UPDATE tk
 SET tk_tank_change_gsv = ISNULL(ta.tank_gsv_change,0)-- - tk.tank_transfer_amt)
 --SELECT * 
 FROM tickets tk INNER JOIN
	  tank_actuals ta ON tk.tk_to_key = ta.tank_key AND tk.tk_eod_datetime = ta.eod_datetime
WHERE tk.tk_eod_datetime = @eod

-- ADD WHEN WE HAVE TOV
 UPDATE tk
 SET tank_change_tov = (ISNULL(ta.tank_tov_change, 0)- tk.tank_transfer_amt)
 --SELECT * 
 FROM pipeline_to_tank_tickets tk INNER JOIN
	  tank_actuals ta ON tk.tank_key = ta.tank_key AND tk.eod_datetime = ta.eod_datetime
WHERE tk.eod_datetime = @eod

DECLARE @tank_check TABLE (tank_key uniqueidentifier, number int, brandi_total float)
DECLARE @tank_adjust TABLE ( row_key uniqueidentifier,  tank_key uniqueidentifier, new_tank_change_nsv float,  new_tank_change_tov float)

INSERT INTO @tank_check (tank_key, number, brandi_total)
SELECT tk.tank_key , COUNT(tk.tank_key), SUM(tp.brandis_value)--,  SUM(tk.tank_change_tov)  ,SUM(tk.total_tank_diff_nsv) 
FROM pipeline_to_tank_tickets tk INNER JOIN 
	 testing_pipeline_to_tank tp ON tk.pipeline_key = tp.pipeline_key AND tk.eod_datetime = tp.eod_datetime AND tk.tank_key = tp.tank_key
WHERE tk.eod_datetime = @eod 
GROUP BY tk.tank_key

DELETE FROM @tank_check WHERE number = 1

--SELECT * FROM @tank_check

UPDATE tk
	SET tank_change_nsv = tk.tank_change_nsv * tp.brandis_value/tc.brandi_total ,
		tank_change_tov = tk.tank_change_tov * tp.brandis_value/tc.brandi_total

--INSERT INTO @tank_adjust (row_key, tank_key, brandi, brandi_percent,  total_all_tanks_nsv, total_all_tanks_tov)
--SELECT tk._key, tk.tank_key,  
--tk.tank_change_nsv * tp.brandis_value/tc.brandi_total ,
--tk.tank_change_tov * tp.brandis_value/tc.brandi_total 
--tc.brandi_total, tk.tank_change_nsv, tk.tank_change_tov, tp.brandis_value
--, tp.brandis_value/tc.brandi_total  PERCENT 
FROM pipeline_to_tank_tickets tk INNER JOIN 
	 @tank_check tc ON tk.tank_key = tc.tank_key INNER JOIN 
	 testing_pipeline_to_tank tp ON tk.pipeline_key = tp.pipeline_key AND tk.eod_datetime = tp.eod_datetime AND tk.tank_key = tp.tank_key
WHERE tk.eod_datetime = @eod 





UPDATE tk
SET total_tank_diff_nsv = tk2.tank_change_nsv
--SELECT tk.eod_datetime, tk.tank_key, tk.berth_key, tk.tank_change_nsv, tk.total_tank_diff_nsv, tk2.tank_change_nsv
FROM pipeline_to_tank_tickets tk INNER JOIN 
	 (SELECT SUM(tank_change_nsv) as tank_change_nsv , pipeline_key, eod_datetime
	  FROM pipeline_to_tank_tickets 
	  WHERE eod_datetime = @eod 
	  GROUP BY eod_datetime, pipeline_key) tk2 ON tk.pipeline_key = tk2.pipeline_key AND tk.eod_datetime = tk.eod_datetime
WHERE tk.eod_datetime = @eod




UPDATE pipeline_to_tank_tickets 
SET percent_total_tank_diff_nsv = tank_change_nsv / total_tank_diff_nsv
WHERE eod_datetime = @eod AND total_tank_diff_nsv <> 0

-- ADD WHEN WE HAVE tov

UPDATE tk
SET total_tank_diff_tov = tk2.tank_change_tov
--SELECT tk.eod_datetime, tk.tank_key, tk.berth_key, tk.tank_change_nsv, tk.total_tank_diff_nsv, tk2.tank_change_nsv
FROM pipeline_to_tank_tickets tk INNER JOIN 
	 (SELECT SUM(tank_change_tov) as tank_change_tov , pipeline_key, eod_datetime
	  FROM pipeline_to_tank_tickets 
	  WHERE eod_datetime = @eod 
	  GROUP BY eod_datetime, pipeline_key) tk2 ON tk.pipeline_key = tk2.pipeline_key AND tk.eod_datetime = tk.eod_datetime
WHERE tk.eod_datetime = @eod



UPDATE pipeline_to_tank_tickets 
SET percent_total_tank_diff_tov = tank_change_tov / total_tank_diff_tov
WHERE eod_datetime = @eod AND total_tank_diff_tov <> 0


UPDATE tk
SET total_meter_diff = pa.tov_bbls
--SELECT tk.total_tank_diff_nsv, pa.tov_bbls
FROM pipeline_to_tank_tickets tk INNER JOIN 	 
	(SELECT SUM(gsv_bbls) as tov_bbls, eod_datetime	 , pipeline_key
	FROM pipeline_actuals 
	WHERE eod_datetime = @eod
	GROUP BY eod_datetime, pipeline_key) pa ON tk.eod_datetime = pa.eod_datetime AND tk.pipeline_key = pa.pipeline_key
WHERE tk.eod_datetime = @eod

UPDATE pipeline_to_tank_tickets
SET tank_meter_bbls_using_tank_nsv = total_meter_diff * percent_total_tank_diff_nsv, 
	tank_meter_bbls_using_tank_tov = total_meter_diff * percent_total_tank_diff_tov
WHERE eod_datetime = @eod

UPDATE test 
SET calculated_tank_nsv = tk.tank_change_nsv,
	calculated_tank_tov = tk.tank_change_tov,
	calculated_meter_bbls_nsv_based = tk.tank_meter_bbls_using_tank_nsv, 
	calculated_meter_bbls_tov_based = tk.tank_meter_bbls_using_tank_tov,
	diff_brandi_to_tank_nsv = test.brandis_value - ISNULL(tk.tank_change_nsv,0),
	diff_brandi_to_tank_tov = test.brandis_value - ISNULL(tk.tank_change_tov,0),
	diff_brandi_to_meter_nsv_based = test.brandis_value - ISNULL(tk.tank_meter_bbls_using_tank_nsv,0),
	diff_brandi_to_meter_tov_based = test.brandis_value - ISNULL(tk.tank_meter_bbls_using_tank_tov,0)
--SELECT test.pipeline_name, test.tank_name, test.brandis_value, tk.tank_meter_bbls_using_tank_nsv, tk.tank_meter_bbls_using_tank_nsv - test.brandis_value as meterDiff, tank_change_nsv as nsvTankChange, tank_change_nsv - test.brandis_value as tankNsvDiff
FROM testing_pipeline_to_tank test INNER JOIN 
	 pipeline_to_tank_tickets tk ON test.eod_datetime = tk.eod_datetime AND test.pipeline_key = tk.pipeline_key AND test.tank_key = tk.tank_key
WHERE test.eod_datetime = @eod






