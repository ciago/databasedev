--SELECT * 
--FROM  tank_actuals ta INNER JOIN 


DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)
--DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 04, 17, 7, 0, 0, 0)
--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, DATEADD(day, -2,getdate())), 
--											DATEPART(MONTH, DATEADD(day, -2,getdate())), 
--											DATEPART(DAY, DATEADD(day, -2,getdate())), 7, 0, 0, 0)


--SELECT t.tank_name, b.berth_description, tb.eod_datetime, SUM(tb.berth_meter_bbls_using_tank_nsv), tb.tank_key, tb.berth_key
--FROM tank_to_berth_tickets tb INNER JOIN 
--	 tanks t ON tb.tank_key = t._key INNER JOIN 
--	 berths b ON tb.berth_key = b._key
--WHERE eod_datetime = @eod AND tb.berth_key <> '00000000-0000-0000-0000-000000000000'
--GROUP BY t.tank_name, b.berth_description, tb.eod_datetime, tb.tank_key, tb.berth_key



--SELECT t.tank_name, p.[description], pt.eod_datetime, SUM(pt.tank_meter_bbls_using_tank_nsv) as bbls,  pt.tank_key, pt.pipeline_key
--FROM pipeline_to_tank_tickets pt INNER JOIN 
--	 pipeline p ON pt.pipeline_key = p._key INNER JOIN 
--	 tanks t ON t._key = pt.tank_key
--WHERE eod_datetime = @eod AND pt.pipeline_key <> '00000000-0000-0000-0000-000000000000'
--GROUP BY t.tank_name, p.[description], pt.eod_datetime,  pt.tank_key, pt.pipeline_key


--SELECT t.tank_name, pt.eod_datetime, SUM(pt.tank_transfer_amt) as bbls,  pt.tank_key, pt.pipeline_key
--FROM pipeline_to_tank_tickets pt INNER JOIN 
--	 --pipeline p ON pt.pipeline_key = p._key INNER JOIN 
--	 tanks t ON t._key = pt.tank_key
--WHERE eod_datetime = @eod AND pt.pipeline_key = '00000000-0000-0000-0000-000000000000'
--GROUP BY t.tank_name,  pt.eod_datetime,  pt.tank_key, pt.pipeline_key



SELECT t.tank_name as Tank, ROUND(ISNULL(ta.open_nsv,0),0) as Beginning , ROUND(ISNULL(shipments.Bbls,0),0) as Shipments, ROUND(ISNULL(receipts.bbls,0),0) as Receipts, 
	   ROUND(ISNULL(transfers.bbls,0),0) as Transfers,
	   ROUND(ISNULL(ta.open_nsv,0) + ISNULL(shipments.Bbls,0) + ISNULL(receipts.bbls,0) + ISNULL(transfers.bbls,0),0)  as [Book Close], 
	   ROUND(ISNULL(ta.close_nsv,0),0) as [Physical Close], 
	   ROUND(ISNULL(ta.close_nsv,0)-(ISNULL(ta.open_nsv,0) + ISNULL(shipments.Bbls,0) + ISNULL(receipts.bbls,0) + ISNULL(transfers.bbls,0)),0) as [Gain Loss]
FROM  tank_actuals ta INNER JOIN 
	  tanks t ON ta.tank_key = t._key LEFT OUTER JOIN 
	  (SELECT t.tank_name,  tb.eod_datetime, SUM(tb.berth_meter_bbls_using_tank_nsv) as bbls, tb.tank_key
		FROM tank_to_berth_tickets tb INNER JOIN 
			 tanks t ON tb.tank_key = t._key INNER JOIN 
			 berths b ON tb.berth_key = b._key
		WHERE eod_datetime = @eod AND tb.berth_key <> '00000000-0000-0000-0000-000000000000'
		GROUP BY t.tank_name, b.berth_description, tb.eod_datetime, tb.tank_key, tb.berth_key)shipments ON ta.tank_key = shipments.tank_key AND ta.eod_datetime = shipments.eod_datetime LEFT OUTER JOIN 
		(SELECT t.tank_name, pt.eod_datetime, SUM(pt.tank_meter_bbls_using_tank_nsv) as bbls,  pt.tank_key
			FROM pipeline_to_tank_tickets pt INNER JOIN 
				 pipeline p ON pt.pipeline_key = p._key INNER JOIN 
				 tanks t ON t._key = pt.tank_key
			WHERE eod_datetime = @eod AND pt.pipeline_key <> '00000000-0000-0000-0000-000000000000'
			GROUP BY t.tank_name,pt.eod_datetime,  pt.tank_key) receipts ON receipts.tank_key = ta.tank_key AND receipts.eod_datetime = ta.eod_datetime LEFT OUTER JOIN 
			(SELECT t.tank_name, pt.eod_datetime, SUM(pt.tank_transfer_amt) as bbls,  pt.tank_key
FROM pipeline_to_tank_tickets pt INNER JOIN 
	 tanks t ON t._key = pt.tank_key
WHERE eod_datetime = @eod AND pt.pipeline_key = '00000000-0000-0000-0000-000000000000'
GROUP BY t.tank_name,  pt.eod_datetime,  pt.tank_key) transfers ON ta.tank_key = transfers.tank_key AND ta.eod_datetime = transfers.eod_datetime
WHERE ta.eod_datetime = @eod



