--***********************************************************************************************************
-- STEP 1:  copy Brandi's excel into the tables testing_tank_to_berth, testing_tank_to_berth, testing_tank_transfers
--***********************************************************************************************************

--DECLARE @eod datetime  = DATETIMEFROMPARTS(DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)
DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 04, 30, 7, 0, 0, 0)
--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, DATEADD(day, -2,getdate())), 
--											DATEPART(MONTH, DATEADD(day, -2,getdate())), 
--											DATEPART(DAY, DATEADD(day, -2,getdate())), 7, 0, 0, 0)
 
--***********************************************************************************************************
-- STEP 2:  Update the keys in those tables 
--***********************************************************************************************************
-- update keys into Brandi's data 
--UPDATE pt
--SET tank_key = t._key,
--	pipeline_key = p._key
----SELECT t._key, t.tank_name, p._key, pt.pipeline_name
--FROM testing_pipeline_to_tank pt INNER JOIN 
--	 pipeline p ON p.short_code = pt.pipeline_name INNER JOIN 
--	 tanks t ON t.tank_pi_name = pt.tank_name
--WHERE tank_key IS NULL OR pipeline_key IS NULL

--UPDATE tb
--SET tank_key = t._key,
--	berth_key = b._key
--	--SELECT tb.tank_name,  tb.berth_name ,t._key,b._key
--FROM testing_tank_to_berth tb INNER JOIN 
--	 berths b ON b.berth_code = tb.berth_name INNER JOIN 
--	 tanks t ON t.tank_pi_name = tb.tank_name
--WHERE tank_key IS NULL OR berth_key IS NULL

--UPDATE tt
--SET tank_key = t._key
----SELECT tt.tank_name, t._key
--FROM testing_tank_transfers tt INNER JOIN 
--	 tanks t ON t.tank_pi_name = tt.tank_name
--WHERE tt.tank_key IS NULL





--***********************************************************************************************************
-- STEP 3:  Run this to calculate and create tank_to_berth_tickets, 
--          fill the comparing columns in the testing_tank_to_berth
--***********************************************************************************************************



 -- reset any previous calculations
 UPDATE tickets
SET
[calculated_tank_nsv] = NULL, 
[calculated_tank_tov] = NULL, 
[calculated_meter_bbls_nsv_based] = NULL, 
[calculated_meter_bbls_tov_based] = NULL, 
[diff_brandi_to_tank_nsv] = NULL, 
[diff_brandi_to_tank_tov] = NULL, 
[diff_brandi_to_meter_nsv_based] = NULL, 
[diff_brandi_to_meter_tov_based] = NULL
WHERE eod_datetime = @eod


 -- get the list of tanks for the day 
 INSERT INTO tank_to_berth_tickets (eod_datetime, tank_key, berth_key)
 SELECT @eod, tank_key, berth_key
 FROM  testing_tank_to_berth test
 WHERE eod_datetime = @eod AND brandis_value <> 0
 
 -- GET ANY TRANSFER DATA 
 -- get the list of tanks involved in transfers for the day 

 --INSERT INTO tank_to_berth_tickets  (eod_datetime, tank_key)
 --SELECT tt.tank_key, pt.tank_key
 --FROM testing_tank_transfers  tt LEFT OUTER JOIN 
	--  tank_to_berth_tickets pt ON pt.tank_key = tt.tank_key AND pt.eod_datetime = tt.eod_datetime
 --WHERE tt.eod_datetime = @eod AND brandis_value <> 0

-- insert a row into this table for any tanks not involved in a pipeline but have a transfer 
INSERT INTO tank_to_berth_tickets (eod_datetime, tank_key, berth_key)
SELECT DISTINCT tt.eod_datetime, tt.tank_key, '00000000-0000-0000-0000-000000000000'
FROM testing_tank_transfers  tt LEFT OUTER JOIN 
	  tank_to_berth_tickets pt ON pt.tank_key = tt.tank_key AND pt.eod_datetime = tt.eod_datetime
WHERE tt.eod_datetime = @eod AND brandis_value <> 0 AND pt.tank_key IS NULL

UPDATE tk
SET tank_transfer_amt = transfers.brandis_value
FROM tank_to_berth_tickets tk INNER JOIN 
	(SELECT SUM(tt.brandis_value) as brandis_value, tk.tank_key, tk.eod_datetime
	FROM tank_to_berth_tickets tk INNER JOIN 
		 testing_tank_transfers tt ON tk.eod_datetime = tt.eod_datetime AND tk.tank_key = tt.tank_key
	WHERE tk. eod_datetime = @eod  
	GROUP BY tk.tank_key, tk.eod_datetime)transfers ON tk.eod_datetime = transfers.eod_datetime AND tk.tank_key = transfers.tank_key
WHERE tk. eod_datetime = @eod    

 


UPDATE tank_to_berth_tickets
SET tank_transfer_amt = 0 
WHERE tank_transfer_amt IS NULL

 -- UPDATE tank changes ( taking out anything that was a transfer so we are left with only pipeline )
 -- TODO:  account for any barge changes too??
 UPDATE tk
 SET tank_change_nsv = (ISNULL(ta.tank_nsv_change,0) - tk.tank_transfer_amt)
 --SELECT * 
 FROM tank_to_berth_tickets tk INNER JOIN
	  tank_actuals ta ON tk.tank_key = ta.tank_key AND tk.eod_datetime = ta.eod_datetime
WHERE tk.eod_datetime = @eod

-- ADD WHEN WE HAVE tov
 UPDATE tk
 SET tank_change_tov = (ISNULL(ta.tank_tov_change, 0)- tk.tank_transfer_amt)
 --SELECT * 
 FROM tank_to_berth_tickets tk INNER JOIN
	  tank_actuals ta ON tk.tank_key = ta.tank_key AND tk.eod_datetime = ta.eod_datetime
WHERE tk.eod_datetime = @eod

----SELECT SUM(tank_change_nsv) FROM tank_to_berth_tickets WHERE eod_datetime = @eod GROUP BY eod_datetime
--UPDATE tank_to_berth_tickets
--SET total_tank_diff_nsv = (SELECT SUM(tank_change_nsv) FROM tank_to_berth_tickets WHERE eod_datetime = @eod GROUP BY eod_datetime)
--WHERE eod_datetime = @eod

UPDATE tb
SET total_tank_diff_nsv = tb2.tank_change_nsv
--SELECT tb.eod_datetime, tb.tank_key, tb.berth_key, tb.tank_change_nsv, tb.total_tank_diff_nsv, tb2.tank_change_nsv
FROM tank_to_berth_tickets tb INNER JOIN 
	 (SELECT SUM(tank_change_nsv) as tank_change_nsv , berth_key, eod_datetime
	  FROM tank_to_berth_tickets 
	  WHERE eod_datetime = @eod 
	  GROUP BY eod_datetime, berth_key) tb2 ON tb.berth_key = tb2.berth_key AND tb.eod_datetime = tb.eod_datetime
WHERE tb.eod_datetime = @eod



UPDATE tank_to_berth_tickets 
SET percent_total_tank_diff_nsv = tank_change_nsv / total_tank_diff_nsv
WHERE eod_datetime = @eod AND total_tank_diff_nsv <> 0

---- ADD WHEN WE HAVE tov
UPDATE tb
SET total_tank_diff_tov = tb2.tank_change_tov
--SELECT tb.eod_datetime, tb.tank_key, tb.berth_key, tb.tank_change_nsv, tb.total_tank_diff_nsv, tb2.tank_change_nsv
FROM tank_to_berth_tickets tb INNER JOIN 
	 (SELECT SUM(tank_change_tov) as tank_change_tov , berth_key, eod_datetime
	  FROM tank_to_berth_tickets 
	  WHERE eod_datetime = @eod 
	  GROUP BY eod_datetime, berth_key) tb2 ON tb.berth_key = tb2.berth_key AND tb.eod_datetime = tb.eod_datetime
WHERE tb.eod_datetime = @eod

UPDATE tank_to_berth_tickets 
SET percent_total_tank_diff_tov = tank_change_tov / total_tank_diff_tov
WHERE eod_datetime = @eod AND total_tank_diff_tov <> 0


UPDATE tk
SET total_meter_diff = (pa.berth_meter_bbls * -1), 
	start_datetime = pa.berth_start, 
	end_datetime = pa.berth_stop, 
	batch_id = berth_batch_id
--SELECT tk.total_tank_diff_nsv, pa.tov_bbls
FROM tank_to_berth_tickets tk INNER JOIN 	 
	(SELECT SUM(berth_meter_bbls) as berth_meter_bbls, eod_datetime	 , berth_key, berth_start, berth_stop, berth_batch_id
	FROM berth_activity 
	WHERE eod_datetime = @eod
	GROUP BY eod_datetime, berth_key,berth_start, berth_stop, berth_batch_id) pa ON tk.eod_datetime = pa.eod_datetime AND tk.berth_key = pa.berth_key
WHERE tk.eod_datetime = @eod

UPDATE tank_to_berth_tickets
SET berth_meter_bbls_using_tank_nsv = total_meter_diff * percent_total_tank_diff_nsv, 
	berth_meter_bbls_using_tank_tov = total_meter_diff * percent_total_tank_diff_tov
WHERE eod_datetime = @eod

UPDATE test 
SET calculated_tank_nsv = tk.tank_change_nsv,
	calculated_tank_tov = tk.tank_change_tov,
	calculated_meter_bbls_nsv_based = tk.berth_meter_bbls_using_tank_nsv, 
	calculated_meter_bbls_tov_based = tk.berth_meter_bbls_using_tank_tov,
	diff_brandi_to_tank_nsv = test.brandis_value - ISNULL(tk.tank_change_nsv,0),
	diff_brandi_to_tank_tov = test.brandis_value - ISNULL(tk.tank_change_tov,0),
	diff_brandi_to_meter_nsv_based = test.brandis_value - ISNULL(tk.berth_meter_bbls_using_tank_nsv,0),
	diff_brandi_to_meter_tov_based = test.brandis_value - ISNULL(tk.berth_meter_bbls_using_tank_tov,0)
--SELECT test.pipeline_name, test.tank_name, test.brandis_value, tk.berth_meter_bbls_using_tank_nsv, tk.berth_meter_bbls_using_tank_nsv - test.brandis_value as meterDiff, tank_change_nsv as nsvTankChange, tank_change_nsv - test.brandis_value as tankNsvDiff
FROM testing_tank_to_berth test INNER JOIN 
	 tank_to_berth_tickets tk ON test.eod_datetime = tk.eod_datetime AND test.berth_key = tk.berth_key AND test.tank_key = tk.tank_key
WHERE test.eod_datetime = @eod






