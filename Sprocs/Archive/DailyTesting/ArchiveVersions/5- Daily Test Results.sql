
--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)
DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 04, 30, 7, 0, 0, 0)

-- test results pipeline 
SELECT       tt.eod_datetime as [EOD], pipeline_name as Pipeline, tank_name as Tank, brandis_value,
			 ROUND(calculated_tank_tov,0) as [TOV Tankmaster Tank Change], 
			  ROUND(calculated_tank_tov,0)- brandis_value, 
			  '',
			 ROUND(pt.total_meter_diff,0) as [Meter Total]

FROM         testing_pipeline_to_tank tt LEFT OUTER JOIN 
			 pipeline_to_tank_tickets pt ON tt.eod_datetime = pt.eod_datetime AND tt.tank_key = pt.tank_key AND tt.pipeline_key = pt.pipeline_key
WHERE        (brandis_value <> 0) AND tt.eod_datetime = @eod

SELECT DISTINCT tt.eod_datetime as [EOD],pipeline_name as Pipeline, '',  ROUND(pt.total_meter_diff,0) as [Meter Total]
FROM         testing_pipeline_to_tank tt LEFT OUTER JOIN 
			 pipeline_to_tank_tickets pt ON tt.eod_datetime = pt.eod_datetime AND tt.tank_key = pt.tank_key AND tt.pipeline_key = pt.pipeline_key
WHERE        (brandis_value <> 0) AND tt.eod_datetime = @eod


-- test results pipeline 
SELECT       tt.eod_datetime as [EOD], pipeline_name as Pipeline, tank_name as Tank, brandis_value, 
			 ROUND(calculated_tank_nsv,0) as [NSV Tankmaster Tank Change], 
			 ROUND(calculated_meter_bbls_nsv_based,0) as [Calculated Meter Ticket NSVBased],
			 ROUND(diff_brandi_to_tank_nsv,0) as [Diff Tank to Brandi NSV], 
			 ROUND(diff_brandi_to_meter_nsv_based,0) as  [Diff Meter to Brandi NSV], 
			 ROUND(pt.total_meter_diff,0) as [Meter Total],
			 ROUND(calculated_tank_tov,0) as [TOV Tankmaster Tank Change], 
			 ROUND(calculated_meter_bbls_tov_based,0) as [Calculated Meter Ticket TOVBased],
			 ROUND(diff_brandi_to_tank_tov,0) as [Diff Tank to Brandi TOV], 
			 ROUND(diff_brandi_to_meter_tov_based,0) as  [Diff Meter to Brandi TOV], 
			 ROUND(pt.total_meter_diff,0) as [Meter Total]

FROM         testing_pipeline_to_tank tt LEFT OUTER JOIN 
			 pipeline_to_tank_tickets pt ON tt.eod_datetime = pt.eod_datetime AND tt.tank_key = pt.tank_key AND tt.pipeline_key = pt.pipeline_key
WHERE        (brandis_value <> 0) AND tt.eod_datetime = @eod

SELECT       tt.eod_datetime as [EOD], berth_name as Berth, tank_name as Tank, brandis_value, 
			 ROUND(calculated_tank_nsv,0) as [NSV Tankmaster Tank Change], 
			 ROUND(calculated_meter_bbls_nsv_based,0)as [Calculated Meter Ticket NSV],
			 ROUND(diff_brandi_to_tank_nsv,0) as [Diff Tank to Brandi NSV], 
			 ROUND(diff_brandi_to_meter_nsv_based,0) as  [Diff Meter to Brandi NSV], 
			 ROUND(tb.total_meter_diff,0) as [Meter Total], 
			 ROUND(calculated_tank_tov,0) as [tov Tankmaster Tank Change], 
			 ROUND(calculated_meter_bbls_tov_based,0)as [Calculated Meter Ticket TOV],
			 ROUND(diff_brandi_to_tank_tov,0) as [Diff Tank to Brandi TOV], 
			 ROUND(diff_brandi_to_meter_tov_based,0) as  [Diff Meter to Brandi TOV], 
			 ROUND(tb.total_meter_diff,0) as [Meter Total]
FROM            testing_tank_to_berth tt LEFT OUTER JOIN 
		      tank_to_berth_tickets tb ON tt.eod_datetime = tb.eod_datetime AND tt.tank_key = tb.tank_key AND tb.berth_key = tt.berth_key
WHERE        (brandis_value <> 0) AND tt.eod_datetime = @eod


SELECT t.tank_name as Tank, ROUND(ISNULL(ta.open_nsv,0),0) as Beginning , ROUND(ISNULL(shipments.Bbls,0),0) as Shipments, ROUND(ISNULL(receipts.bbls,0),0) as Receipts, 
	   ROUND(ISNULL(transfers.bbls,0),0) as Transfers,
	   ROUND(ISNULL(ta.open_nsv,0) + ISNULL(shipments.Bbls,0) + ISNULL(receipts.bbls,0) + ISNULL(transfers.bbls,0),0)  as [Book Close], 
	   ROUND(ISNULL(ta.close_nsv,0),0) as [Physical Close], 
	   ROUND(ISNULL(ta.close_nsv,0)-(ISNULL(ta.open_nsv,0) + ISNULL(shipments.Bbls,0) + ISNULL(receipts.bbls,0) + ISNULL(transfers.bbls,0)),0) as [Gain Loss]
FROM  tank_actuals ta INNER JOIN 
	  tanks t ON ta.tank_key = t._key LEFT OUTER JOIN 
	  (SELECT t.tank_name,  tb.eod_datetime, SUM(tb.berth_meter_bbls_using_tank_nsv) as bbls, tb.tank_key
		FROM tank_to_berth_tickets tb INNER JOIN 
			 tanks t ON tb.tank_key = t._key INNER JOIN 
			 berths b ON tb.berth_key = b._key
		WHERE eod_datetime = @eod AND tb.berth_key <> '00000000-0000-0000-0000-000000000000'
		GROUP BY t.tank_name, b.berth_description, tb.eod_datetime, tb.tank_key, tb.berth_key)shipments ON ta.tank_key = shipments.tank_key AND ta.eod_datetime = shipments.eod_datetime LEFT OUTER JOIN 
		(SELECT t.tank_name, pt.eod_datetime, SUM(pt.tank_meter_bbls_using_tank_nsv) as bbls,  pt.tank_key
			FROM pipeline_to_tank_tickets pt INNER JOIN 
				 pipeline p ON pt.pipeline_key = p._key INNER JOIN 
				 tanks t ON t._key = pt.tank_key
			WHERE eod_datetime = @eod AND pt.pipeline_key <> '00000000-0000-0000-0000-000000000000'
			GROUP BY t.tank_name,pt.eod_datetime,  pt.tank_key) receipts ON receipts.tank_key = ta.tank_key AND receipts.eod_datetime = ta.eod_datetime LEFT OUTER JOIN 
			(SELECT t.tank_name, pt.eod_datetime, SUM(pt.tank_transfer_amt) as bbls,  pt.tank_key
FROM pipeline_to_tank_tickets pt INNER JOIN 
	 tanks t ON t._key = pt.tank_key
WHERE eod_datetime = @eod AND pt.tank_transfer_amt <> 0
GROUP BY t.tank_name,  pt.eod_datetime,  pt.tank_key) transfers ON ta.tank_key = transfers.tank_key AND ta.eod_datetime = transfers.eod_datetime
WHERE ta.eod_datetime = @eod



SELECT t.tank_name as Tank, ROUND(ISNULL(ta.open_tov,0),0) as Beginning , ROUND(ISNULL(shipments.Bbls,0),0) as Shipments, ROUND(ISNULL(receipts.bbls,0),0) as Receipts, 
	   ROUND(ISNULL(transfers.bbls,0),0) as Transfers,
	   ROUND(ISNULL(ta.open_tov,0) + ISNULL(shipments.Bbls,0) + ISNULL(receipts.bbls,0) + ISNULL(transfers.bbls,0),0)  as [Book Close], 
	   ROUND(ISNULL(ta.close_tov,0),0) as [Physical Close], 
	   ROUND(ISNULL(ta.close_tov,0)-(ISNULL(ta.open_tov,0) + ISNULL(shipments.Bbls,0) + ISNULL(receipts.bbls,0) + ISNULL(transfers.bbls,0)),0) as [Gain Loss]
FROM  tank_actuals ta INNER JOIN 
	  tanks t ON ta.tank_key = t._key LEFT OUTER JOIN 
	  (SELECT t.tank_name,  tb.eod_datetime, SUM(tb.berth_meter_bbls_using_tank_tov) as bbls, tb.tank_key
		FROM tank_to_berth_tickets tb INNER JOIN 
			 tanks t ON tb.tank_key = t._key INNER JOIN 
			 berths b ON tb.berth_key = b._key
		WHERE eod_datetime = @eod AND tb.berth_key <> '00000000-0000-0000-0000-000000000000'
		GROUP BY t.tank_name, b.berth_description, tb.eod_datetime, tb.tank_key, tb.berth_key)
	shipments ON ta.tank_key = shipments.tank_key AND ta.eod_datetime = shipments.eod_datetime LEFT OUTER JOIN 
		(SELECT t.tank_name, pt.eod_datetime, SUM(pt.tank_meter_bbls_using_tank_tov) as bbls,  pt.tank_key
			FROM pipeline_to_tank_tickets pt INNER JOIN 
				 pipeline p ON pt.pipeline_key = p._key INNER JOIN 
				 tanks t ON t._key = pt.tank_key
			WHERE eod_datetime = @eod AND pt.pipeline_key <> '00000000-0000-0000-0000-000000000000'
			GROUP BY t.tank_name,pt.eod_datetime,  pt.tank_key) 
	receipts ON receipts.tank_key = ta.tank_key AND receipts.eod_datetime = ta.eod_datetime LEFT OUTER JOIN 
			(SELECT t.tank_name, pt.eod_datetime, SUM(pt.tank_transfer_amt) as bbls,  pt.tank_key
FROM pipeline_to_tank_tickets pt INNER JOIN 
	 tanks t ON t._key = pt.tank_key
WHERE eod_datetime = @eod AND pt.tank_transfer_amt <> 0
GROUP BY t.tank_name,  pt.eod_datetime,  pt.tank_key) transfers ON ta.tank_key = transfers.tank_key AND ta.eod_datetime = transfers.eod_datetime
WHERE ta.eod_datetime = @eod

