--***********************************************************************************************************
-- STEP 1:  copy Brandi's excel into the tables testing_pipeline_to_tank, testing_tank_to_berth, testing_tank_transfers
--***********************************************************************************************************
--DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 04, 24, 7, 0, 0, 0)
DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)

--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, DATEADD(day, -1,getdate())), 
--											DATEPART(MONTH, DATEADD(day, -1,getdate())), 
--											DATEPART(DAY, DATEADD(day, -1,getdate())), 7, 0, 0, 0)
 
--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, DATEADD(day, -2,getdate())), 
--											DATEPART(MONTH, DATEADD(day, -2,getdate())), 
--											DATEPART(DAY, DATEADD(day, -2,getdate())), 7, 0, 0, 0)
 
--***********************************************************************************************************
-- STEP 2:  Update the keys in those tables 
--***********************************************************************************************************
---- update keys into Brandi's data 
UPDATE pt
SET tank_key = t._key,
	pipeline_key = p._key
--SELECT t._key, t.tank_name, p._key, pt.pipeline_name
FROM testing_pipeline_to_tank pt INNER JOIN 
	 pipeline p ON p.short_code = pt.pipeline_name INNER JOIN 
	 tanks t ON t.tank_pi_name = pt.tank_name
WHERE tank_key IS NULL OR pipeline_key IS NULL

UPDATE tb
SET tank_key = t._key,
	berth_key = b._key
	--SELECT tb.tank_name,  tb.berth_name ,t._key,b._key
FROM testing_tank_to_berth tb INNER JOIN 
	 berths b ON b.berth_code = tb.berth_name INNER JOIN 
	 tanks t ON t.tank_pi_name = tb.tank_name
WHERE tank_key IS NULL OR berth_key IS NULL

UPDATE tt
SET tank_key = t._key
--SELECT tt.tank_name, t._key
FROM testing_tank_transfers tt INNER JOIN 
	 tanks t ON t.tank_pi_name = tt.tank_name
WHERE tt.tank_key IS NULL




--***********************************************************************************************************
-- STEP 3:  Run this to calculate and create pipeline_to_tank_tickets, 
--          fill the comparing columns in the testing_pipeline_to_tank
--***********************************************************************************************************

 -- reove anything there from the day
 DELETE FROM pipeline_to_tank_tickets WHERE eod_datetime = @eod

 -- get the list of tanks for the day 
 INSERT INTO pipeline_to_tank_tickets (eod_datetime, tank_key, pipeline_key)
 SELECT @eod, tank_key, pipeline_key
 FROM  testing_pipeline_to_tank test
 WHERE eod_datetime = @eod AND brandis_value <> 0
 
 -- GET ANY TRANSFER DATA 
 -- get the list of tanks involved in transfers for the day 

 --INSERT INTO pipeline_to_tank_tickets  (eod_datetime, tank_key)
 --SELECT tt.tank_key, pt.tank_key
 --FROM testing_tank_transfers  tt LEFT OUTER JOIN 
	--  pipeline_to_tank_tickets pt ON pt.tank_key = tt.tank_key AND pt.eod_datetime = tt.eod_datetime
 --WHERE tt.eod_datetime = @eod AND brandis_value <> 0

-- insert a row into this table for any tanks not involved in a pipeline but have a transfer 
INSERT INTO pipeline_to_tank_tickets (eod_datetime, tank_key, pipeline_key)
SELECT tt.eod_datetime, tt.tank_key, '00000000-0000-0000-0000-000000000000'
FROM testing_tank_transfers  tt LEFT OUTER JOIN 
	  pipeline_to_tank_tickets pt ON pt.tank_key = tt.tank_key AND pt.eod_datetime = tt.eod_datetime
WHERE tt.eod_datetime = @eod AND brandis_value <> 0 AND pt.tank_key IS NULL

UPDATE tk
SET tank_transfer_amt = tt.brandis_value
--SELECT tk.tank_key, tt.brandis_value
FROM pipeline_to_tank_tickets tk INNER JOIN 
     testing_tank_transfers tt ON tk.eod_datetime = tt.eod_datetime AND tk.tank_key = tt.tank_key
WHERE tk. eod_datetime = @eod     

UPDATE pipeline_to_tank_tickets
SET tank_transfer_amt = 0 
WHERE tank_transfer_amt IS NULL

 -- UPDATE tank changes ( taking out anything that was a transfer so we are left with only pipeline )
 -- TODO:  account for any barge changes too??
 UPDATE tk
 SET tank_change_nsv = (ISNULL(ta.tank_nsv_change,0) - tk.tank_transfer_amt)
 --SELECT * 
 FROM pipeline_to_tank_tickets tk INNER JOIN
	  tank_actuals ta ON tk.tank_key = ta.tank_key AND tk.eod_datetime = ta.eod_datetime
WHERE tk.eod_datetime = @eod

-- ADD WHEN WE HAVE TOV
 UPDATE tk
 SET tank_change_tov = (ISNULL(ta.tank_tov_change, 0)- tk.tank_transfer_amt)
 --SELECT * 
 FROM pipeline_to_tank_tickets tk INNER JOIN
	  tank_actuals ta ON tk.tank_key = ta.tank_key AND tk.eod_datetime = ta.eod_datetime
WHERE tk.eod_datetime = @eod

----SELECT SUM(tank_change_nsv) FROM pipeline_to_tank_tickets WHERE eod_datetime = @eod GROUP BY eod_datetime
--UPDATE pipeline_to_tank_tickets
--SET total_tank_diff_nsv = (SELECT SUM(tank_change_nsv) FROM pipeline_to_tank_tickets WHERE eod_datetime = @eod GROUP BY eod_datetime)
--WHERE eod_datetime = @eod

UPDATE tk
SET total_tank_diff_nsv = tk2.tank_change_nsv
--SELECT tk.eod_datetime, tk.tank_key, tk.berth_key, tk.tank_change_nsv, tk.total_tank_diff_nsv, tk2.tank_change_nsv
FROM pipeline_to_tank_tickets tk INNER JOIN 
	 (SELECT SUM(tank_change_nsv) as tank_change_nsv , pipeline_key, eod_datetime
	  FROM pipeline_to_tank_tickets 
	  WHERE eod_datetime = @eod 
	  GROUP BY eod_datetime, pipeline_key) tk2 ON tk.pipeline_key = tk2.pipeline_key AND tk.eod_datetime = tk.eod_datetime
WHERE tk.eod_datetime = @eod




UPDATE pipeline_to_tank_tickets 
SET percent_total_tank_diff_nsv = tank_change_nsv / total_tank_diff_nsv
WHERE eod_datetime = @eod AND total_tank_diff_nsv <> 0

---- ADD WHEN WE HAVE tov
--UPDATE pipeline_to_tank_tickets
--SET total_tank_diff_tov = (SELECT SUM(tank_change_tov) FROM pipeline_to_tank_tickets WHERE eod_datetime = @eod GROUP BY eod_datetime)
--WHERE eod_datetime = @eod
UPDATE tk
SET total_tank_diff_tov = tk2.tank_change_tov
--SELECT tk.eod_datetime, tk.tank_key, tk.berth_key, tk.tank_change_nsv, tk.total_tank_diff_nsv, tk2.tank_change_nsv
FROM pipeline_to_tank_tickets tk INNER JOIN 
	 (SELECT SUM(tank_change_tov) as tank_change_tov , pipeline_key, eod_datetime
	  FROM pipeline_to_tank_tickets 
	  WHERE eod_datetime = @eod 
	  GROUP BY eod_datetime, pipeline_key) tk2 ON tk.pipeline_key = tk2.pipeline_key AND tk.eod_datetime = tk.eod_datetime
WHERE tk.eod_datetime = @eod


UPDATE pipeline_to_tank_tickets 
SET percent_total_tank_diff_tov = tank_change_tov / total_tank_diff_tov
WHERE eod_datetime = @eod AND total_tank_diff_tov <> 0


UPDATE tk
SET total_meter_diff = pa.tov_bbls
--SELECT tk.total_tank_diff_nsv, pa.tov_bbls
FROM pipeline_to_tank_tickets tk INNER JOIN 	 
	(SELECT SUM(gsv_bbls) as tov_bbls, eod_datetime	 , pipeline_key
	FROM pipeline_actuals 
	WHERE eod_datetime = @eod
	GROUP BY eod_datetime, pipeline_key) pa ON tk.eod_datetime = pa.eod_datetime AND tk.pipeline_key = pa.pipeline_key
WHERE tk.eod_datetime = @eod

UPDATE pipeline_to_tank_tickets
SET tank_meter_bbls_using_tank_nsv = total_meter_diff * percent_total_tank_diff_nsv, 
	tank_meter_bbls_using_tank_tov = total_meter_diff * percent_total_tank_diff_tov
WHERE eod_datetime = @eod

UPDATE test 
SET calculated_tank_nsv = tk.tank_change_nsv,
	calculated_tank_tov = tk.tank_change_tov,
	calculated_meter_bbls_nsv_based = tk.tank_meter_bbls_using_tank_nsv, 
	calculated_meter_bbls_tov_based = tk.tank_meter_bbls_using_tank_tov,
	diff_brandi_to_tank_nsv = test.brandis_value - ISNULL(tk.tank_change_nsv,0),
	diff_brandi_to_tank_tov = test.brandis_value - ISNULL(tk.tank_change_tov,0),
	diff_brandi_to_meter_nsv_based = test.brandis_value - ISNULL(tk.tank_meter_bbls_using_tank_nsv,0),
	diff_brandi_to_meter_tov_based = test.brandis_value - ISNULL(tk.tank_meter_bbls_using_tank_tov,0)
--SELECT test.pipeline_name, test.tank_name, test.brandis_value, tk.tank_meter_bbls_using_tank_nsv, tk.tank_meter_bbls_using_tank_nsv - test.brandis_value as meterDiff, tank_change_nsv as nsvTankChange, tank_change_nsv - test.brandis_value as tankNsvDiff
FROM testing_pipeline_to_tank test INNER JOIN 
	 pipeline_to_tank_tickets tk ON test.eod_datetime = tk.eod_datetime AND test.pipeline_key = tk.pipeline_key AND test.tank_key = tk.tank_key
WHERE test.eod_datetime = @eod






