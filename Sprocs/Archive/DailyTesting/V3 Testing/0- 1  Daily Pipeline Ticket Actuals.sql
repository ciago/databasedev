-- ***********************************
--  Daily Pipeline Ticket Actuals 
--  Candice Iago 
--  5/12/20
--************************************


DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 05, 02, 7, 0, 0, 0)
--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)

 -- remove any data in the final columns from the day
 UPDATE tickets 
 SET tk_tank_close_timestamp = NULL,
	 tk_tank_open_gsv = NULL,
	 tk_tank_close_gsv = NULL,
	 tk_tank_change_gsv = NULL,
	 tk_group_total_tank_diff = NULL,
	 tk_percent_tank_diff = NULL,
	 tk_total_meter_diff = NULL,
	 tk_meter_volume = NULL, 
	 tk_status = 1
 WHERE tk_eod_datetime = @eod  AND tk_type = 1

-- remove meter records to re-pull
DELETE FROM tickets_meter WHERE eod_datetime = @eod AND ticket_key IN (SELECT _key FROM tickets WHERE  tk_eod_datetime = @eod  AND tk_type = 1)

-- if the end time is not end of day at 7 am, add some time to the tank stop time so the tank can settle
-- settle time is set int he sites table 
UPDATE t
SET tk_tank_close_timestamp = CASE WHEN CONVERT(TIME, tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN tk_eod_datetime
							  ELSE DATEADD(HOUR, s.tank_settle_time_hours, tk_end_datetime) END
FROM tickets t INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod  AND tk_type = 1


-- update  tank readings 
UPDATE t
SET tk_tank_open_gsv = tr_open.tank_gsv,
tk_tank_open_level = tr_open.tank_level,
	tk_tank_close_gsv = tr_close.tank_gsv,
tk_tank_close_level = tr_close.tank_level
--select t.tk_to_key, tr_open.tank_gsv as start_gsv, tr_close.tank_gsv as close_gsv, tr_close.tank_gsv - tr_open.tank_gsv as tank_gsv_diff, tanks.tank_description
from tickets t INNER JOIN 
     tank_readings tr_open ON t.tk_to_key = tr_open.tank_key AND t.tk_start_datetime = tr_open.tank_timestamp INNER JOIN 
	 tank_readings tr_close ON tr_close.tank_key = t.tk_to_key AND  t.tk_tank_close_timestamp = tr_close.tank_timestamp INNER JOIN 
	 tanks on tanks._key = t.tk_to_key INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod AND tk_type = 1


-- update the tank diff 
UPDATE tickets 
SET tk_tank_change_gsv = tk_tank_close_gsv - tk_tank_open_gsv
WHERE tk_eod_datetime = @eod AND tk_type = 1


-- update the total tank change by pipeline or FROM key
UPDATE tk
SET tk_group_total_tank_diff = tk2.tank_change_gsv
--SELECT tk.eod_datetime, tk.tank_key, tk.berth_key, tk.tank_change_nsv, tk.total_tank_diff_nsv, tk2.tank_change_nsv
FROM tickets tk INNER JOIN 
	 (SELECT SUM(tk_tank_change_gsv) as tank_change_gsv , tk_from_key, tk_eod_datetime
	  FROM tickets 
	  WHERE tk_eod_datetime = @eod AND tk_type = 1
	  GROUP BY tk_eod_datetime, tk_from_key) tk2 ON tk.tk_from_key = tk2.tk_from_key AND tk.tk_eod_datetime = tk.tk_eod_datetime
WHERE tk.tk_eod_datetime = @eod AND tk_type = 1

-- update the percent where the group total is not 0 (avoid divide by 0 error)
UPDATE tickets
SET tk_percent_tank_diff = (tk_tank_change_gsv / tk_group_total_tank_diff)
WHERE tk_eod_datetime = @eod AND tk_type = 1 AND tk_group_total_tank_diff <> 0

-- update percent to 0 if the group total is 0
UPDATE tickets
SET tk_percent_tank_diff = 0
WHERE tk_eod_datetime = @eod AND tk_type = 1 AND tk_group_total_tank_diff = 0

INSERT INTO dbo.tickets_meter
           (_key
           ,ticket_key
           ,eod_datetime
           ,meter_key
           ,skid_key
           ,open_totalizer
           ,close_totalizer
           ,meter_error)

---- pipeline transactions
select newid(), 
	   t._key, 
	   t.tk_eod_datetime, 
	   m._key, 
	   m.skid_key, 
	   mr_open.meter_totalizer as open_totalizer, 
	   mr_close.meter_totalizer as close_totalizer,
	   CASE WHEN mr_close.meter_totalizer < mr_open.meter_totalizer THEN 1 ELSE 0 END as meter_error
	   --mr_close.meter_totalizer - mr_open.meter_totalizer as meter_diff, 
	   --, p.description, t.tk_start_datetime, t.tk_end_datetime, m.meter_name, s.skid_description, 
FROM tickets t INNER JOIN 
	 pipeline p ON t.tk_from_key = p._key INNER JOIN 
	 meters m ON m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key INNER JOIN 
	 meter_readings mr_open ON mr_open.meter_key = m._key AND mr_open.meter_timestamp = t.tk_start_datetime  INNER JOIN 
	 meter_readings mr_close ON mr_close.meter_key = m._key AND mr_close.meter_timestamp = t.tk_end_datetime 
WHERE tk_eod_datetime = @eod AND tk_type = 1

 -- CALCS FOR NOW UNTIL WE GET ACTUAL PIPELINE TIMES**************************************************************************************************************************************
UPDATE t
SET tk_total_meter_diff = t2.meter_amt
FROM (SELECT ticket_key, (SUM(tm.close_totalizer) -  SUM(tm.open_totalizer) ) as meter_amt
	  FROM tickets_meter tm INNER JOIN 
	  	 tickets t ON t._key = tm.ticket_key
	  WHERE t.tk_eod_datetime = @eod AND t.tk_type = 1
	  GROUP BY ticket_key) t2 INNER JOIN 
	  tickets t ON t._key = t2.ticket_key

UPDATE tickets
SET tk_meter_volume = tk_total_meter_diff * tk_percent_tank_diff
WHERE tk_eod_datetime = @eod and tk_type = 1

UPDATE t
SET tk_meter_error = 1
--SELECT tm.ticket_key
FROM tickets_meter tm INNER JOIN 
	  	 tickets t ON t._key = tm.ticket_key
WHERE t.tk_eod_datetime = @eod AND t.tk_type = 1 AND tm.meter_error = 1

UPDATE  tickets
SET tk_status = 2
WHERE tk_eod_datetime = @eod AND tk_type = 1

UPDATE  tickets
SET tk_final_gsv = tk_meter_volume
WHERE tk_eod_datetime = @eod AND tk_type = 1



 -- CALCS WHEN WE GET ACTUAL PIPELINE TIMES**************************************************************************************************************************************

--UPDATE t
--SET tk_meter_volume = t2.meter_amt
--FROM (SELECT ticket_key, (SUM(tm.close_totalizer) -  SUM(tm.open_totalizer) ) as meter_amt
--	  FROM tickets_meter tm INNER JOIN 
--	  	 tickets t ON t._key = tm.ticket_key
--	  WHERE t.tk_eod_datetime = @eod AND t.tk_type = 1
--	  GROUP BY ticket_key) t2 INNER JOIN 
--	  tickets t ON t._key = t2.ticket_key

--UPDATE t
--SET tk_total_meter_diff = pa.gsv_bbls
----SELECT t._key, pa.gsv_bbls
--FROM tickets t INNER JOIN 
--	 pipeline_actuals pa ON t.tk_from_key = pa.pipeline_key AND t.tk_eod_datetime = pa.eod_datetime
--WHERE t.tk_eod_datetime = @eod AND t.tk_type = 1 

--UPDATE t
--SET tk_meter_error = 1
----SELECT tm.ticket_key
--FROM tickets_meter tm INNER JOIN 
--	  	 tickets t ON t._key = tm.ticket_key
--WHERE t.tk_eod_datetime = @eod AND t.tk_type = 1 AND tm.meter_error = 1


-- ********************************************************************************************************************************************************************************



