-- ***********************************
--  Daily Vessel Ticket Actuals 
--  Candice Iago 
--  5/12/20
--************************************


DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 05, 03, 7, 0, 0, 0)
--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)

 -- remove any data in the final columns from the day
 UPDATE tickets 
 SET tk_tank_close_timestamp = NULL,
	 tk_tank_open_gsv = NULL,
	 tk_tank_close_gsv = NULL,
	 tk_tank_change_gsv = NULL,
	 tk_group_total_tank_diff = NULL,
	 tk_percent_tank_diff = NULL,
	 tk_total_meter_diff = NULL,
	 tk_meter_volume = NULL, 
	 tk_status = 1
 WHERE tk_eod_datetime = @eod  AND tk_type = 2

-- remove meter records to re-pull
DELETE FROM tickets_meter WHERE eod_datetime = @eod AND ticket_key IN (SELECT _key FROM tickets WHERE  tk_eod_datetime = @eod  AND tk_type = 2)

-- update meter volume and dates from berth tracking 
UPDATE tk
SET tk_total_meter_diff = (pa.berth_meter_bbls ), 
	tk_start_datetime = pa.berth_start, 
	tk_end_datetime = pa.berth_stop, 
	tk_batch_id = berth_batch_id
--SELECT tk.total_tank_diff_nsv, pa.tov_bbls
FROM tickets tk INNER JOIN 	 
	(SELECT SUM(berth_meter_bbls) as berth_meter_bbls, eod_datetime	 , berth_key, berth_start, berth_stop, berth_batch_id
	FROM berth_activity 
	WHERE eod_datetime = @eod
	GROUP BY eod_datetime, berth_key,berth_start, berth_stop, berth_batch_id) pa ON tk.tk_eod_datetime = pa.eod_datetime AND tk.tk_to_key = pa.berth_key
WHERE tk.tk_eod_datetime = @eod  AND tk_type = 2


-- Updates the start and end time based on any other moveemnts involving that tank in teh same day 
--SELECT t.tk_start_datetime, t.tk_end_datetime, t.tk_from_key, t.tk_to_key, t2.tk_start_datetime, t2.tk_end_datetime, t2.tk_from_key, t2.tk_to_key, 
UPDATE t
SET tk_start_datetime = CASE WHEN t2.tk_end_datetime > t.tk_start_datetime AND t2.tk_end_datetime <  t.tk_end_datetime THEN t2.tk_end_datetime ELSE t.tk_start_datetime END ,
	tk_end_datetime = CASE WHEN t2.tk_start_datetime < t.tk_end_datetime AND t2.tk_start_datetime >  t.tk_start_datetime THEN t2.tk_start_datetime ELSE t.tk_end_datetime END 
	   --,   CASE WHEN t2.tk_start_datetime < t.tk_end_datetime THEN t2.tk_start_datetime ELSE t.tk_end_datetime END as new_start 
FROM 
tickets t INNER JOIN 
(SELECT tk_from_key, tk_to_key, tk_start_datetime, tk_end_datetime, tk_eod_datetime FROM tickets WHERE tk_eod_datetime = @eod  AND tk_type <> 2 AND 
			(tk_from_key IN ((SELECT tk_from_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ) OR 
			 tk_to_key IN ((SELECT tk_from_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ))) t2 ON t.tk_eod_datetime = t2.tk_eod_datetime AND (t.tk_from_key = t2.tk_to_key OR t.tk_from_key = t2.tk_from_key)
WHERE t.tk_eod_datetime = @eod  AND t.tk_type = 2


-- Update the tank close timestamp 
--SELECT t.tk_start_datetime, t.tk_end_datetime, t.tk_from_key, t.tk_to_key, t2.tk_start_datetime, t2.tk_end_datetime, t2.tk_from_key, t2.tk_to_key, 
UPDATE t
SET tk_tank_close_timestamp =  CASE WHEN t.tk_end_datetime = t2.tk_start_datetime THEN t.tk_end_datetime
								  WHEN CONVERT(TIME, t.tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN t.tk_eod_datetime
								  ELSE DATEADD(HOUR, s.tank_settle_time_hours, t.tk_end_datetime) END
FROM 
tickets t INNER JOIN 
(SELECT tk_from_key, tk_to_key, tk_start_datetime, tk_end_datetime, tk_eod_datetime FROM tickets WHERE tk_eod_datetime = @eod  AND tk_type <> 2 AND 
			(tk_from_key IN ((SELECT tk_from_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ) OR 
			 tk_to_key IN ((SELECT tk_from_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ))) t2 ON t.tk_eod_datetime = t2.tk_eod_datetime AND (t.tk_from_key = t2.tk_to_key OR t.tk_from_key = t2.tk_from_key) INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE t.tk_eod_datetime = @eod  AND t.tk_type = 2  AND tk_tank_close_timestamp IS NULL

UPDATE t
SET tk_tank_close_timestamp = CASE WHEN CONVERT(TIME, tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN tk_eod_datetime
							  ELSE DATEADD(HOUR, s.tank_settle_time_hours, tk_end_datetime) END
FROM tickets t INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod  AND tk_type = 2 AND tk_tank_close_timestamp IS NULL





-- update  tank readings 
UPDATE t
SET tk_tank_open_gsv = tr_open.tank_gsv,
tk_tank_open_level = tr_open.tank_level,
	tk_tank_close_gsv = tr_close.tank_gsv,
tk_tank_close_level = tr_close.tank_level
--select t.tk_to_key, tr_open.tank_gsv as start_gsv, tr_close.tank_gsv as close_gsv, tr_close.tank_gsv - tr_open.tank_gsv as tank_gsv_diff, tanks.tank_description
from tickets t INNER JOIN 
     tank_readings tr_open ON t.tk_from_key = tr_open.tank_key AND t.tk_start_datetime = tr_open.tank_timestamp INNER JOIN 
	 tank_readings tr_close ON tr_close.tank_key = t.tk_from_key AND  t.tk_tank_close_timestamp = tr_close.tank_timestamp INNER JOIN 
	 tanks on tanks._key = t.tk_from_key INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod AND tk_type = 2


-- update the tank diff 
UPDATE tickets 
SET tk_tank_change_gsv = (tk_tank_close_gsv - tk_tank_open_gsv) * -1
WHERE tk_eod_datetime = @eod AND tk_type = 2


-- update the total tank change by berth or TO key
UPDATE tk
SET tk_group_total_tank_diff = tk2.tank_change_gsv
--SELECT tk.eod_datetime, tk.tank_key, tk.berth_key, tk.tank_change_nsv, tk.total_tank_diff_nsv, tk2.tank_change_nsv
FROM tickets tk INNER JOIN 
	 (SELECT SUM(tk_tank_change_gsv) as tank_change_gsv , tk_to_key, tk_eod_datetime
	  FROM tickets 
	  WHERE tk_eod_datetime = @eod AND tk_type = 2
	  GROUP BY tk_eod_datetime, tk_to_key) tk2 ON tk.tk_to_key = tk2.tk_to_key AND tk.tk_eod_datetime = tk.tk_eod_datetime
WHERE tk.tk_eod_datetime = @eod AND tk_type = 2

-- update the percent where the group total is not 0 (avoid divide by 0 error)
UPDATE tickets
SET tk_percent_tank_diff = (tk_tank_change_gsv / tk_group_total_tank_diff)
WHERE tk_eod_datetime = @eod AND tk_type = 2 AND tk_group_total_tank_diff <> 0

-- update percent to 0 if the group total is 0
UPDATE tickets
SET tk_percent_tank_diff = 0
WHERE tk_eod_datetime = @eod AND tk_type = 2 AND tk_group_total_tank_diff = 0

-- insert meter records for the vessel transactions
INSERT INTO dbo.tickets_meter
           (_key
           ,ticket_key
           ,eod_datetime
           ,meter_key
           ,skid_key
           ,open_totalizer
           ,close_totalizer
           ,meter_error)
select newid(), 
	   t._key, 
	   t.tk_eod_datetime, 
	   vam.meter_key, 
	   vam.skid_key, 
	   vam.open_totalizer, 
	   vam.close_totalizer,
	   CASE WHEN vam.close_totalizer < vam.open_totalizer THEN 1 ELSE 0 END as meter_error
FROM tickets t INNER JOIN 
	 valve_activity va ON va.valve_skid_batch_id = t.tk_batch_id AND va.valve_eod_datetime = t.tk_eod_datetime INNER JOIN 
	 valve_activity_meter vam ON vam.valve_activity_key = va._key
WHERE tk_eod_datetime = @eod AND tk_type = 2


-- -- CALCS FOR Meter adjusted tank amounts    **************************************************************************************************************************************


UPDATE tickets
SET tk_meter_volume = tk_total_meter_diff * tk_percent_tank_diff
WHERE tk_eod_datetime = @eod and tk_type = 2

--UPDATE t
--SET tk_meter_error = 1
----SELECT tm.ticket_key
--FROM tickets_meter tm INNER JOIN 
--	  	 tickets t ON t._key = tm.ticket_key
--WHERE t.tk_eod_datetime = @eod AND t.tk_type = 2 AND tm.meter_error = 1

UPDATE  tickets
SET tk_status = 2
WHERE tk_eod_datetime = @eod AND tk_type = 2

UPDATE  tickets
SET tk_final_gsv = tk_meter_volume
WHERE tk_eod_datetime = @eod AND tk_type = 2


