-- 1.   transfers brandi data - last eod 5/2 which is 5/1 tab on brndi
-- 2.  run this sproc to add tickets 


----INSERT INTO tickets(
----	_key, 
----	tk_type, 
----	tk_status, 
----	tk_shipper, 
----	tk_product, 
----	tk_batch_id, 
----	tk_eod_datetime, 
----	tk_from_key, 
----	tk_to_key, 
----	tk_scheduled_start_datetime, 
----	tk_scheduled_end_datetime, 
----	tk_start_datetime, 
----	tk_end_datetime, 
----	tk_day_sequence_number, 
----	tk_volume_uom, 
----	tk_scheduled_volume, 
----	tk_tank_change_tov, 
----	tk_group_total_tank_diff, 
----	tk_percent_tank_diff, 
----	tk_total_meter_diff, 
----	tk_meter_volume)


----SELECT  
----	 newid(),--_key, 
----	1 ,--tk_type, -- 1 pipeline, 2 vessel, 3 tank to tank
----	CASE WHEN [calculated_meter_bbls_tov_based] IS NOT NULL THEN 4 ELSE 1 END,--tk_status, 
----	o.tank_owner_key,--tk_shipper, 
----	'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',--tk_product, 
----	'',--tk_batch_id, 
----	tt.eod_datetime,--tk_eod_datetime, 
----	p._key,--tk_from_key, 
----	t._key,--tk_to_key, 
----	DATEADD(DAY, -1,tt.eod_datetime),--tk_scheduled_start_datetime, 
----	tt.eod_datetime,--tk_scheduled_end_datetime, 
----	DATEADD(DAY, -1,tt.eod_datetime),--tk_start_datetime, 
----	tt.eod_datetime,--tk_end_datetime, 
----	1,--tk_day_sequence_number, 
----	1,--tk_volume_uom, 
----	tt.brandis_value,--tk_scheduled_volume, 
----	pt.tank_change_tov,--tk_tank_change_tov, 
----	pt.total_tank_diff_tov,--tk_group_total_tank_diff, 
----	pt.percent_total_tank_diff_tov,--tk_percent_tank_diff, 
----	pt.total_meter_diff,--tk_total_meter_diff, 
----	pt.tank_meter_bbls_using_tank_tov--tk_meter_volume, 
	
----FROM testing_pipeline_to_tank tt INNER JOIN 
----	 tanks t ON t.tank_pi_name = tt.tank_name INNER JOIN 
----	 tank_owner o ON o.tank_key = t._key INNER JOIN 
----	 pipeline p ON p.short_code = tt.pipeline_name LEFT OUTER JOIN 
----	 pipeline_to_tank_tickets pt ON pt.eod_datetime = tt.eod_datetime AND pt.tank_key = t._key AND pt.pipeline_key = p._key
----WHERE tt.brandis_value <> 0 AND pt.pipeline_key <> '00000000-0000-0000-0000-000000000000'
--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)
DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 05, 02, 7, 0, 0, 0)

DELETE FROM tickets WHERE tk_eod_datetime = @eod

INSERT INTO tickets(
	_key, 
	tk_type, 
	tk_status, 
	tk_shipper, 
	tk_product, 
	tk_batch_id, 
	tk_eod_datetime, 
	tk_from_key, 
	tk_to_key, 
	tk_scheduled_start_datetime, 
	tk_scheduled_end_datetime, 
	tk_start_datetime, 
	tk_end_datetime, 
	tk_day_sequence_number, 
	tk_volume_uom, 
	tk_scheduled_volume)


SELECT  
	 newid(),--_key, 
	1 ,--tk_type, -- 1 pipeline, 2 vessel, 3 tank to tank
	CASE WHEN [calculated_meter_bbls_tov_based] IS NOT NULL THEN 4 ELSE 1 END,--tk_status, 
	o.tank_owner_key,--tk_shipper, 
	'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',--tk_product, 
	'',--tk_batch_id, 
	tt.eod_datetime,--tk_eod_datetime, 
	p._key,--tk_from_key, 
	t._key,--tk_to_key, 
	DATEADD(DAY, -1,tt.eod_datetime),--tk_scheduled_start_datetime, 
	tt.eod_datetime,--tk_scheduled_end_datetime, 
	DATEADD(DAY, -1,tt.eod_datetime),--tk_start_datetime, 
	tt.eod_datetime,--tk_end_datetime, 
	1,--tk_day_sequence_number, 
	1,--tk_volume_uom, 
	tt.brandis_value--tk_scheduled_volume, 
	
	
FROM testing_pipeline_to_tank tt INNER JOIN 
	 tanks t ON t.tank_pi_name = tt.tank_name INNER JOIN 
	 tank_owner o ON o.tank_key = t._key INNER JOIN 
	 pipeline p ON p.short_code = tt.pipeline_name 
WHERE tt.brandis_value <> 0 AND tt.eod_datetime = @eod



----**********************************************VESSELS*********************************************
--INSERT INTO tickets(
--	_key, 
--	tk_type, 
--	tk_status, 
--	tk_shipper, 
--	tk_product, 
--	tk_batch_id, 
--	tk_eod_datetime, 
--	tk_from_key, 
--	tk_to_key, 
--	tk_scheduled_start_datetime, 
--	tk_scheduled_end_datetime, 
--	tk_start_datetime, 
--	tk_end_datetime, 
--	tk_day_sequence_number, 
--	tk_volume_uom, 
--	tk_scheduled_volume, 
--	tk_tank_change_tov, 
--	tk_group_total_tank_diff, 
--	tk_percent_tank_diff, 
--	tk_total_meter_diff, 
--	tk_meter_volume)


--SELECT  
--	 newid(),--_key, 
--	1 ,--tk_type, -- 1 pipeline, 2 vessel, 3 tank to tank
--	CASE WHEN [calculated_meter_bbls_tov_based] IS NOT NULL THEN 4 ELSE 1 END,--tk_status, 
--	o.tank_owner_key,--tk_shipper, 
--	'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',--tk_product, 
--	'',--tk_batch_id, 
--	tt.eod_datetime,--tk_eod_datetime, 
--	t._key,--tk_from_key, 
--	b._key,--tk_to_key, 
--	DATEADD(DAY, -1,tt.eod_datetime),--tk_scheduled_start_datetime, 
--	tt.eod_datetime,--tk_scheduled_end_datetime, 
--	tb.start_datetime,--tk_start_datetime, 
--	tb.end_datetime,--tk_end_datetime, 
--	1,--tk_day_sequence_number, 
--	1,--tk_volume_uom, 
--	tt.brandis_value,--tk_scheduled_volume, 
--	tb.tank_change_tov,--tk_tank_change_tov, 
--	tb.total_tank_diff_tov,--tk_group_total_tank_diff, 
--	tb.percent_total_tank_diff_tov,--tk_percent_tank_diff, 
--	tb.total_meter_diff,--tk_total_meter_diff, 
--	tb.berth_meter_bbls_using_tank_tov--tk_meter_volume, 
	
--FROM testing_tank_to_berth tt INNER JOIN 
--	 tanks t ON t.tank_pi_name = tt.tank_name INNER JOIN 
--	 tank_owner o ON o.tank_key = t._key INNER JOIN 
--	 berths b ON tt.berth_name = b.berth_code LEFT OUTER JOIN 
--	 --pipeline p ON p.short_code = tt.pipeline_name LEFT OUTER JOIN 
--	 tank_to_berth_tickets tb ON tb.eod_datetime = tt.eod_datetime AND tb.tank_key = t._key AND tb.berth_key = b._key--pt.pipeline_key = p._key
--WHERE tt.brandis_value <> 0 --AND pt.pipeline_key <> '00000000-0000-0000-0000-000000000000'


INSERT INTO tickets(
	_key, 
	tk_type, 
	tk_status, 
	tk_shipper, 
	tk_product, 
	tk_batch_id, 
	tk_eod_datetime, 
	tk_from_key, 
	tk_to_key, 
	tk_scheduled_start_datetime, 
	tk_scheduled_end_datetime, 
	tk_start_datetime, 
	tk_end_datetime, 
	tk_day_sequence_number, 
	tk_volume_uom, 
	tk_scheduled_volume)


SELECT  
	 newid(),--_key, 
	2 ,--tk_type, -- 1 pipeline, 2 vessel, 3 tank to tank
	CASE WHEN [calculated_meter_bbls_tov_based] IS NOT NULL THEN 4 ELSE 1 END,--tk_status, 
	o.tank_owner_key,--tk_shipper, 
	'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',--tk_product, 
	'',--tk_batch_id, 
	tt.eod_datetime,--tk_eod_datetime, 
	t._key,--tk_from_key, 
	b._key,--tk_to_key, 
	DATEADD(DAY, -1,tt.eod_datetime),--tk_scheduled_start_datetime, 
	tt.eod_datetime,--tk_scheduled_end_datetime, 
	NULL,--tk_start_datetime, 
	NULL,--tk_end_datetime, 
	1,--tk_day_sequence_number, 
	1,--tk_volume_uom, 
	tt.brandis_value--tk_scheduled_volume, 
	
	
FROM testing_tank_to_berth tt INNER JOIN 
	 tanks t ON t.tank_pi_name = tt.tank_name INNER JOIN 
	 tank_owner o ON o.tank_key = t._key INNER JOIN 
	 berths b ON tt.berth_name = b.berth_code 
WHERE tt.brandis_value <> 0 AND tt.eod_datetime = @eod


----**********************************************TRANSFERS*********************************************
INSERT INTO tickets(
	_key, 
	tk_type, 
	tk_status, 
	tk_shipper, 
	tk_product, 
	tk_batch_id, 
	tk_eod_datetime, 
	tk_from_key, 
	tk_to_key, 
	tk_scheduled_start_datetime, 
	tk_scheduled_end_datetime, 
	tk_start_datetime, 
	tk_end_datetime, 
	tk_day_sequence_number, 
	tk_volume_uom, 
	tk_scheduled_volume)


SELECT  
	 newid(),--_key, 
	3 ,--tk_type, -- 1 pipeline, 2 vessel, 3 tank to tank
	4, --CASE WHEN [calculated_meter_bbls_tov_based] IS NOT NULL THEN 4 ELSE 1 END,--tk_status, 
	o.tank_owner_key,--tk_shipper, 
	'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',--tk_product, 
	'',--tk_batch_id, 
	tt.eod_datetime,--tk_eod_datetime, 
	ft._key,--tk_from_key, 
	tot._key,--tk_to_key, 
	DATEADD(DAY, -1,tt.eod_datetime),--tk_scheduled_start_datetime, 
	tt.eod_datetime,--tk_scheduled_end_datetime, 
	DATEADD(DAY, -1,tt.eod_datetime),--tk_start_datetime, 
	tt.eod_datetime,--tk_end_datetime, 
	1,--tk_day_sequence_number, 
	1,--tk_volume_uom, 
	tt.brandis_value--tk_scheduled_volume, 
	
FROM testing_tank_transfers tt INNER JOIN 
	 tanks ft ON ft.tank_pi_name = tt.from_tank_name INNER JOIN 
	 tanks tot ON tot.tank_pi_name = tt.to_tank_name INNER JOIN 
	 tank_owner o ON o.tank_key = ft._key
WHERE tt.brandis_value <> 0 --AND pt.pipeline_key <> '00000000-0000-0000-0000-000000000000'
	 AND eod_datetime = @eod

