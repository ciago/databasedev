
--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)
DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 05, 03, 7, 0, 0, 0)

-- test results pipeline
SELECT t.tk_eod_datetime as [Date], 
	   p.description as [Pipeline] , 
	   ta.tank_name as [Tank], 
	   t.tk_scheduled_volume as [Scheduled], 
	   ROUND(t.tk_tank_change_gsv,0) [Actual Tank Change(gsv)],  
	   ROUND(t.tk_final_gsv,0) [Actual Final (gsv)], 
	   ROUND(t.tk_scheduled_volume - t.tk_final_gsv,0) as [Diff],
	   ROUND((t.tk_scheduled_volume * .989) - t.tk_final_gsv,0) as [Diff est gsv]
FROM tickets t INNER JOIN 
	 tanks ta ON t.tk_to_key = ta._key INNER JOIN 
	 pipeline p ON p._key = t.tk_from_key
WHERE tk_eod_datetime = @eod AND tk_type = 1 -- pipeline


-- test vessel results 
SELECT t.tk_eod_datetime as [Date], 
	   b.berth_description as [Berth] , 
	   ta.tank_name as [Tank], 
	   t.tk_scheduled_volume as [Scheduled], 
	   ROUND(t.tk_tank_change_gsv,0) [Actual Tank Change(gsv)],  
	   ROUND(t.tk_final_gsv,0) [Actual Final (gsv)], 
	   ROUND(t.tk_scheduled_volume - t.tk_final_gsv,0) as [Diff],
	   ROUND((t.tk_scheduled_volume * .989) - t.tk_final_gsv,0) as [Diff est gsv]
FROM tickets t INNER JOIN 
	 tanks ta ON t.tk_from_key = ta._key INNER JOIN 
	 berths b ON b._key = t.tk_to_key
WHERE tk_eod_datetime = @eod AND tk_type = 2 -- vessel



-- test transfer results 
SELECT t.tk_eod_datetime as [Date], 
	   from_ta.tank_name as [From Tank], 
	   to_ta.tank_name as [To Tank], 
	   t.tk_scheduled_volume as [Scheduled], 
	   ROUND(t.tk_tank_change_gsv,0) [Actual Tank Change(gsv)],  
	   ROUND(t.tk_final_gsv,0) [Actual Final (gsv)], 
	   ROUND(t.tk_scheduled_volume - t.tk_final_gsv,0) as [Diff],
	   ROUND((t.tk_scheduled_volume * .989) - t.tk_final_gsv,0) as [Diff est gsv]
FROM tickets t INNER JOIN 
	 tanks from_ta ON t.tk_from_key = from_ta._key INNER JOIN 
	 tanks to_ta ON t.tk_to_key = to_ta._key
WHERE tk_eod_datetime = @eod AND tk_type = 3 -- transfer


-- Inventory Report
---- shipment by tank
--SELECT t.tank_name,  tt.tk_eod_datetime as eod_datetime, (SUM(tt.tk_final_gsv)*-1) as bbls, tt.tk_from_key as tank_key
--		FROM tickets tt INNER JOIN 
--			 tanks t ON tt.tk_from_key = t._key INNER JOIN 
--			 berths b ON tt.tk_to_key = b._key
--		WHERE tt.tk_eod_datetime = @eod  AND tk_type = 2
--		GROUP BY t.tank_name, b.berth_description, tt.tk_eod_datetime,  tt.tk_from_key, tt.tk_to_key 
---- receipt by tank
--		SELECT t.tank_name, tt.tk_eod_datetime as eod_datetime, SUM(tt.tk_final_gsv) as bbls, tt.tk_to_key as tank_key
--			FROM tickets tt INNER JOIN 
--				 pipeline p ON tt.tk_from_key = p._key INNER JOIN 
--				 tanks t ON t._key = tt.tk_to_key
--			WHERE tk_eod_datetime = @eod  AND tk_type = 1
--			GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_to_key

---- transfers
--SELECT tank_name , tk_eod_datetime, SUM(bbls) as bbls, tank_key
--FROM (
--SELECT t.tank_name, tt.tk_eod_datetime, (SUM(tt.tk_final_gsv)* -1) as bbls,  tt.tk_from_key as tank_key
--FROM tickets tt INNER JOIN 
--	 tanks t ON t._key = tt.tk_from_key
--WHERE tk_eod_datetime = @eod AND tt.tk_type = 3
--GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_from_key UNION
--SELECT t.tank_name, tt.tk_eod_datetime, SUM(tt.tk_final_gsv) as bbls,  tt.tk_to_key  as tank_key
--FROM tickets tt INNER JOIN 
--	 tanks t ON t._key = tt.tk_to_key
--WHERE tk_eod_datetime = @eod AND tt.tk_type = 3
--GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_to_key) transfers
--GROUP BY  tank_name , tk_eod_datetime,  tank_key



SELECT t.tank_name as Tank, ROUND(ISNULL(ta.open_gsv,0),0) as Beginning , 
	   (ROUND(ISNULL(shipments.Bbls,0),0)*-1) as Shipments, 
	   ROUND(ISNULL(receipts.bbls,0),0) as Receipts, 
	   ROUND(ISNULL(transfers.bbls,0),0) as Transfers,
	   ROUND(ISNULL(ta.open_gsv,0) + (ISNULL(shipments.Bbls,0)*-1) + ISNULL(receipts.bbls,0) + ISNULL(transfers.bbls,0),0)  as [Book Close], 
	   ROUND(ISNULL(ta.close_gsv,0),0) as [Physical Close], 
	   ROUND(ISNULL(ta.close_gsv,0)-(ISNULL(ta.open_gsv,0) + (ISNULL(shipments.Bbls,0)*-1) + ISNULL(receipts.bbls,0) + ISNULL(transfers.bbls,0)),0) as [Gain Loss]
FROM  tank_actuals ta INNER JOIN 
	  tanks t ON ta.tank_key = t._key LEFT OUTER JOIN 
	  (SELECT t.tank_name,  tt.tk_eod_datetime as eod_datetime, SUM(tt.tk_final_gsv) as bbls, tt.tk_from_key as tank_key
		FROM tickets tt INNER JOIN 
			 tanks t ON tt.tk_from_key = t._key INNER JOIN 
			 berths b ON tt.tk_to_key = b._key
		WHERE tt.tk_eod_datetime = @eod  AND tk_type = 2
		GROUP BY t.tank_name, b.berth_description, tt.tk_eod_datetime,  tt.tk_from_key, tt.tk_to_key)
    shipments ON ta.tank_key = shipments.tank_key AND ta.eod_datetime = shipments.eod_datetime LEFT OUTER JOIN 
		(SELECT t.tank_name, tt.tk_eod_datetime as eod_datetime, SUM(tt.tk_final_gsv) as bbls, tt.tk_to_key as tank_key
			FROM tickets tt INNER JOIN 
				 pipeline p ON tt.tk_from_key = p._key INNER JOIN 
				 tanks t ON t._key = tt.tk_to_key
			WHERE tk_eod_datetime = @eod  AND tk_type = 1
			GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_to_key) 
	receipts ON receipts.tank_key = ta.tank_key AND receipts.eod_datetime = ta.eod_datetime LEFT OUTER JOIN 
			(SELECT tank_name , tk_eod_datetime, SUM(bbls) as bbls, tank_key
FROM (
SELECT t.tank_name, tt.tk_eod_datetime, (SUM(tt.tk_final_gsv)* -1) as bbls,  tt.tk_from_key as tank_key
FROM tickets tt INNER JOIN 
	 tanks t ON t._key = tt.tk_from_key
WHERE tk_eod_datetime = @eod AND tt.tk_type = 3
GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_from_key UNION
SELECT t.tank_name, tt.tk_eod_datetime, SUM(tt.tk_final_gsv) as bbls,  tt.tk_to_key  as tank_key
FROM tickets tt INNER JOIN 
	 tanks t ON t._key = tt.tk_to_key
WHERE tk_eod_datetime = @eod AND tt.tk_type = 3
GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_to_key) transfers
GROUP BY  tank_name , tk_eod_datetime,  tank_key
) transfers ON ta.tank_key = transfers.tank_key AND ta.eod_datetime = transfers.tk_eod_datetime
WHERE ta.eod_datetime = @eod
ORDER BY Tank
