-- ***********************************
--  Daily Transfer Ticket Actuals 
--  Candice Iago 
--  5/12/20
--************************************

DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 05, 02, 7, 0, 0, 0)
--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)

 -- remove any data in the final columns from the day
 UPDATE tickets 
 SET tk_tank_close_timestamp = NULL,
	 tk_tank_open_gsv = NULL,
	 tk_tank_close_gsv = NULL,
	 tk_tank_change_gsv = NULL,
	 tk_group_total_tank_diff = NULL,
	 tk_percent_tank_diff = NULL,
	 tk_total_meter_diff = NULL,
	 tk_meter_volume = NULL, 
	 tk_status = 1
 WHERE tk_eod_datetime = @eod  AND tk_type = 3

-- remove meter records to re-pull
DELETE FROM tickets_meter WHERE eod_datetime = @eod AND ticket_key IN (SELECT _key FROM tickets WHERE  tk_eod_datetime = @eod  AND tk_type = 3)



-- if the end time is not end of day at 7 am, add some time to the tank stop time so the tank can settle
-- settle time is set int he sites table 
UPDATE t
SET tk_tank_close_timestamp = CASE WHEN CONVERT(TIME, tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN tk_eod_datetime
							  ELSE DATEADD(HOUR, s.tank_settle_time_hours, tk_end_datetime) END
FROM tickets t INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod  AND tk_type = 3

-- check for another start using the same from tank so we can grab the correct close and not wait an hour
UPDATE t
--SELECT t._key , t.tk_end_datetime, t.tk_tank_close_timestamp, t2.tk_start_datetime, 
SET tk_tank_close_timestamp = CASE WHEN t.tk_tank_close_timestamp >t2.tk_start_datetime THEN t2.tk_start_datetime ELSE t.tk_tank_close_timestamp END 
FROM tickets t INNER JOIN 
	(SELECT * FROM tickets WHERE tk_eod_datetime = @eod  AND tk_type = 3 ) t2 ON t.tk_eod_datetime = t2.tk_eod_datetime AND t.tk_from_key = t2.tk_from_key AND t2.tk_day_sequence_number = t.tk_day_sequence_number + 1
WHERE t.tk_eod_datetime = @eod  AND t.tk_type = 3 


-- update  tank readings ***************** USES THE SOURCE TANK AS TANK DIFF OF RECORD******************
UPDATE t
SET tk_tank_open_gsv = tr_open.tank_gsv,
	tk_tank_open_level = tr_open.tank_level,
	tk_tank_close_gsv = tr_close.tank_gsv,
    tk_tank_close_level = tr_close.tank_level
--select t.tk_to_key, tr_open.tank_gsv as start_gsv, tr_close.tank_gsv as close_gsv, tr_close.tank_gsv - tr_open.tank_gsv as tank_gsv_diff, tanks.tank_description
from tickets t INNER JOIN 
     tank_readings tr_open ON t.tk_from_key = tr_open.tank_key AND t.tk_start_datetime = tr_open.tank_timestamp INNER JOIN 
	 tank_readings tr_close ON tr_close.tank_key = t.tk_from_key AND  t.tk_tank_close_timestamp = tr_close.tank_timestamp INNER JOIN 
	 tanks on tanks._key = t.tk_to_key INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod AND tk_type = 3

UPDATE t
SET tk_transfer_to_tank_open_gsv = tr_open.tank_gsv,
	tk_transfer_to_tank_open_level = tr_open.tank_level,
	tk_transfer_to_tank_close_gsv = tr_close.tank_gsv,
    tk_transfer_to_tank_close_level = tr_close.tank_level
--select t.tk_to_key, tr_open.tank_gsv as start_gsv, tr_close.tank_gsv as close_gsv, tr_close.tank_gsv - tr_open.tank_gsv as tank_gsv_diff, tanks.tank_description
from tickets t INNER JOIN 
     tank_readings tr_open ON t.tk_to_key = tr_open.tank_key AND t.tk_start_datetime = tr_open.tank_timestamp INNER JOIN 
	 tank_readings tr_close ON tr_close.tank_key = t.tk_to_key AND  t.tk_tank_close_timestamp = tr_close.tank_timestamp INNER JOIN 
	 tanks on tanks._key = t.tk_to_key INNER JOIN 
	 sites s ON s._key = t.site_key
WHERE tk_eod_datetime = @eod AND tk_type = 3


-- update the tank diff 
UPDATE tickets 
SET tk_tank_change_gsv = -1 * (tk_tank_close_gsv - tk_tank_open_gsv)
WHERE tk_eod_datetime = @eod AND tk_type = 3


 -- CALCS FOR NOW UNTIL WE GET ACTUAL PIPELINE TIMES**************************************************************************************************************************************
 
UPDATE  tickets
SET tk_final_gsv = tk_tank_change_gsv
WHERE tk_eod_datetime = @eod AND tk_type = 3

UPDATE t
SET tk_batch_id = (out_tank.tank_pi_name) +'-'+ in_tank.tank_pi_name +'-'+ convert(varchar, t.tk_eod_datetime, 12)
FROM tickets t INNER JOIN 
	 tanks out_tank  ON t.tk_from_key = out_tank._key INNER JOIN 
	 tanks in_tank ON t.tk_to_key = in_tank._key
WHERE t.tk_eod_datetime = @eod AND t.tk_type = 3


UPDATE  tickets
SET tk_status = 2
WHERE tk_eod_datetime = @eod AND tk_type = 3

 -- CALCS WHEN WE GET ACTUAL PIPELINE TIMES**************************************************************************************************************************************

--UPDATE t
--SET tk_meter_volume = t2.meter_amt
--FROM (SELECT ticket_key, (SUM(tm.close_totalizer) -  SUM(tm.open_totalizer) ) as meter_amt
--	  FROM tickets_meter tm INNER JOIN 
--	  	 tickets t ON t._key = tm.ticket_key
--	  WHERE t.tk_eod_datetime = @eod AND t.tk_type = 3
--	  GROUP BY ticket_key) t2 INNER JOIN 
--	  tickets t ON t._key = t2.ticket_key

--UPDATE t
--SET tk_total_meter_diff = pa.gsv_bbls
----SELECT t._key, pa.gsv_bbls
--FROM tickets t INNER JOIN 
--	 pipeline_actuals pa ON t.tk_from_key = pa.pipeline_key AND t.tk_eod_datetime = pa.eod_datetime
--WHERE t.tk_eod_datetime = @eod AND t.tk_type = 3 

--UPDATE t
--SET tk_meter_error = 1
----SELECT tm.ticket_key
--FROM tickets_meter tm INNER JOIN 
--	  	 tickets t ON t._key = tm.ticket_key
--WHERE t.tk_eod_datetime = @eod AND t.tk_type = 3 AND tm.meter_error = 1


-- ********************************************************************************************************************************************************************************




