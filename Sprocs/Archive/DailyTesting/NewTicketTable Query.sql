SELECT  CASE WHEN tt.type_is_shipment = 1  THEN 'shipment' 
		     WHEN tt.type_is_receipt = 1  THEN 'receipt'
			 WHEN tt.type_is_transfer = 1 THEN 'transfer' END as [transaction_type],
	   tt.type_description as [type],
	   e.name as [shipper], 
	   p.product_name as product, 
	  ISNULL(ISNULL(out_pipe.[description], out_tank.tank_description), out_berth.berth_description) as from_location,
	  ISNULL(ISNULL(in_pipe.[description], in_tank.tank_description), in_berth.berth_description) as to_location,
	   t.tk_scheduled_volume as scheduled_volume,
	   t.tk_scheduled_start_datetime as scheduled_start, 
	   t.tk_scheduled_end_datetime as scheduled_end, 
	   t.tk_start_datetime as actual_start, 
	   t.tk_end_datetime as actual_end, 
	   t.tk_eod_datetime as eod_date 
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 entities e ON e._key = t.tk_shipper INNER JOIN 
	 products p ON p._key = t.tk_product LEFT OUTER JOIN 
	 pipeline in_pipe ON in_pipe._key  = t.tk_to_key LEFT OUTER JOIN 
	 pipeline out_pipe ON out_pipe._key = t.tk_from_key LEFT OUTER JOIN 
	 tanks in_tank ON in_tank._key = t.tk_to_key LEFT OUTER JOIN 
	 tanks out_tank ON out_tank._key = t.tk_from_key LEFT OUTER JOIN 
	 berths in_berth ON in_berth._key = t.tk_to_key LEFT OUTER JOIN 
     berths out_berth ON out_berth._key = t.tk_from_key



	