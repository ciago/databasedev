USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_invoice_status_get_list]    Script Date: 3/26/2021 11:16:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_invoice_status_get_list
 
	*/

create or ALTER         PROCEDURE [dbo].[mgn_invoice_status_accounting_get_list] 
    @site_key uniqueidentifier = null, 
	@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();

	SELECT '00000000-0000-0000-0000-000000000000' as [key], 'Select Invoice Status' as [stat_description], 0 as type
	UNION
    SELECT _key as [key],
		   stat_description as [stat_description],
		   stat_type as type
    FROM vessel_invoice_status
	WHERE site_key = @site_key and
		  stat_type = 4 or 
		  stat_type = 6 or 
		  stat_type = 7
	ORDER BY type
    
END
