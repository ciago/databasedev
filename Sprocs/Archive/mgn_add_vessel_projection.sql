USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_vessel_projections_update]    Script Date: 4/22/2021 11:26:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected vessel projections with new user input details.

/*EXEC mgn_vessel_projections_update 
	@row = '2cd79062-5c11-4420-91d7-67c13c412cf2',
	@projected_volume = 462375,
	@add_proj = 1
	   	
		462375

	// 
	*/
-- =============================================
create or ALTER           PROCEDURE [dbo].[mgn_add_vessel_projection]

	@order uniqueidentifier
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from vessel_projections
--Where _key = @row

declare @new_day_number int = (select Max(day_number) from vessel_projected_volumes where synthesis_key = @order) + 1
		Insert Into  vessel_projected_volumes
					([synthesis_key],
					 [day_number],
					 [projected_volume])
		Values		(@order,
					 @new_day_number,
					 0)

END
