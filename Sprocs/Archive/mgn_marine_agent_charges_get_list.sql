USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_marine_agent_charges_get_list]    Script Date: 3/23/2021 1:12:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		10/21/2020
-- Description:		Obtains a list of marine agent charges from a selected invoice record.

/*EXEC mgn_marine_agent_charges_get_list
	@row  = '0592dabb-6795-449e-b929-4ab4fd850992'

    */	
-- =============================================
ALTER		     PROCEDURE [dbo].[mgn_marine_agent_charges_get_list]

	@row uniqueidentifier = null
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

--Select * from 
--WHere _key = @row 

		-- charge description, charge boolean, unit amount, unit description,
		-- unite rate, charge total, charge override

SELECT        ic._key,
			  ic.ic_vessel_invoice_key as [vessel_invoice_key],
			  ic.ic_charges_key as [charges_key],
			  c.charge_description as [charge_descrip],
			  ic.ic_charge_bool as [charge_bool],
			  ISNULL(ic.ic_unit_amount, 0) as [unit_amount],
			  ISNULL(cut.charge_unit_description, '') as [unit_descrip],
			  ISNULL(c.charge_amount, 0) as [unit_rate],
			  ISNULL(ic.ic_charge_total, 0) as [charge_total],
			  ISNULL(ic.ic_charge_override, 0) as [charge_override]
			  
FROM          invoice_charge AS ic INNER JOIN
              charges AS c ON ic.ic_charges_key = c._key INNER JOIN
	          charge_unit_type AS cut ON c.charge_unit_key = cut._key

WHERE		  ic.ic_vessel_invoice_key = @row
	  
END
