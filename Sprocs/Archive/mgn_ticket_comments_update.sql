USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_manual_tank_gauge_update]    Script Date: 4/12/2021 4:01:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/18/2021
-- Description:		Update selected business entities record with new user input details.

/*EXEC [mgn_ticket_comments_update] 
	@ticket = '5d55d4da-d27e-4a0a-a9af-27c4ace3a141',
	@comment = 'Test'
	
	// 
	*/
-- =============================================
Create or ALTER         PROCEDURE [dbo].[mgn_ticket_comments_update]

	@ticket_key uniqueidentifier = null,
	@ticket_id bigint = null,
	@comment nvarchar(max)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from entities
--Where _key = @row


INSERT INTO [dbo].[ticket_comments]
	       ([tc_ticket_key]
		   ,[tc_ticket_id]
		   ,[tc_comment]
		   ,[tc_date])
VALUES	   
           (isnull(@ticket_key, (select _key from tickets where tk_ticket_id = @ticket_id))
		   ,isnull(@ticket_id, (select tk_ticket_id from tickets where _key = @ticket_key))
		   ,@comment
		   ,getdate())

END

