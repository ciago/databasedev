USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_marine_agent_invoices_get_row]    Script Date: 3/23/2021 12:56:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Obtains a single row of marine agent invoices data from a specified guid.

/*EXEC [mgn_marine_agent_generated_invoices_get_row]
	@row  = 'F7D2CBAD-D34C-49CE-8147-3A1F86252849'
	*/
-- =============================================
Create or ALTER		     PROCEDURE [dbo].[mgn_marine_agent_generated_invoices_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from 
--WHere _key = @row   

   -- ETA Date (descending order), Berth, Vessel Name, Agent, Arrived Date,
   -- Status, Volume, Type

SELECT        vi._key,--st._key,
			  vi.vi_invoice_number as [invoice_number],
			  isnull(st.syn_vessel_name,'') as [vessel_name],
			  ISNULL(st.syn_vessel_description, '') as [vessel_type],
			  isnull(e.name,'') as [marine_agent],
			  ISNULL(CONVERT(varchar, vi.vi_accounting_date, 22), '') as [arrived_date],
			  ISNULL(CONVERT(varchar, st.syn_ticket_end_datetime, 22), '') as [departed_date],
			  isnull(vi.vi_time_in_berth,0) as [time_in_berth],
			  isnull(vis.stat_description,'') as [invoice_status],
			  ISNULL(CONVERT(varchar, vi.vi_invoice_date, 22), '') as [invoice_date],
			  isnull(pt.pt_description, '') as [payment_terms],
			  ISNULL(CONVERT(varchar, vi.vi_payment_date, 22), '') as [payment_date],
			  ISNULL(CONVERT(float, vi.vi_invoice_total), 0) as [invoice_total],
			  isnull([first_name],'') as [first_name],
			  isnull([last_name],'') as [last_name],
			  isnull(c.[company_name],'') as [company_name],
			  isnull(c.[address_1],'') as [address1],
			  isnull(c.[address_2],'') as [address2],
			  isnull(c.[city],'') as [city],
			  isnull(s.[state_abbreviations],'') as [state],
			  isnull(cnt.country,'') as [country],
			  isnull(c.[zip_code],'') as [zip_code],
			  isnull(c.[phone_1],'') as [phone1],
			  isnull(c.[phone_2],'') as [phone2],
			  isnull(c.[email_address],'') as [email],
			  isnull(ct.[contact_type_description],'') as [contact_type],
			  isnull(vi.vi_comments,'') as [comments]
			  
FROM		  vessel_invoice vi left outer JOIN 
			  synthesis_tickets AS st ON vi.vi_synthesis_ticket_key = st._key left outer JOIN
			  vessel_invoice_status AS vis ON vi.vi_vessel_invoice_status_key = vis._key left outer join
			  entities as e on vi.vi_marine_agent_key = e._key left outer join
			  contacts as c on vi.vi_marine_agent_key = c.entity_key left outer join
			  contact_types as ct on c.contact_type_key = ct._key left outer join
			  states as s on c.state_key = s._key left outer join
			  countries as cnt on c.country_key = cnt._key left outer join
			  payment_terms as pt on e.payment_terms = pt.pt_terms

WHERE @row = vi._key
	  
END
