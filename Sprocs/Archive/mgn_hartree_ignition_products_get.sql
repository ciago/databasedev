USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_hartree_ignition_products_get]    Script Date: 4/20/2021 11:20:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_ignition_products_get
	
	*/

ALTER                     PROCEDURE [dbo].[mgn_hartree_ignition_products_get] 

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

Select  tp._key,
		tp.tank_key,
		t.tank_name,
		tp.product_key,
		e.product_name as [product_name],
		tp.tk_start_datetime,
		tp.tk_end_datetime,
		tp.external_row_id
from tank_products as tp left join
	 tanks as t on tp.tank_key = t._key left join
	 products as e on tp.product_key = e._key
where tp.deleted = 0 and
	  tp.tk_start_datetime <= GETDATE() and
	  (tp.tk_end_datetime >= GetDate() or tp.tk_end_datetime is null)

END
