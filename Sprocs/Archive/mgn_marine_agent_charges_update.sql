USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_marine_agent_charges_update]    Script Date: 4/22/2021 2:14:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected marine agent charge record with new user input details.

/*EXEC mgn_marine_agent_charges_update
	@row = 'afa80b51-0a22-40d0-ae8c-1391561b6d8f',
	@charge_bool = 'False',
	@unit_amount = '3',
	@charge_total = '',
	@charge_override = 'False',
	@user = 'candice'
	
	// 
	*/
-- =============================================
ALTER		     PROCEDURE [dbo].[mgn_marine_agent_charges_update]

	@row uniqueidentifier,
	@charge_bool bit,
	@unit_amount decimal(18,5),
	@charge_total float,
	@charge_override bit,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from invoice_charge
--Where _key = @row

IF(@row IN (SELECT _key from invoice_charge))
BEGIN
-- it exists already so update it 

DECLARE @unit_rate float

SET @unit_rate = (SELECT charge_amount FROM invoice_charge as ic INNER JOIN
						 charges as c ON ic.ic_charges_key = c._key
				   WHERE ic._key = @row)

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT ic_record_info FROM invoice_charge WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())	

SET			@charge_total = (@unit_amount*@unit_rate)

UPDATE invoice_charge

SET			ic_charge_bool = @charge_bool,
			ic_unit_amount = @unit_amount,
			ic_charge_total = @charge_total,
			ic_charge_override = @charge_override,
			ic_record_info = @json

WHERE _key = @row

END

END
