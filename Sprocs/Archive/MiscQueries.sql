-- valve state last 1000 rows 
SELECT  TOP (50) v.valve_name, b.berth_description,  state, timestamp, v._key
FROM   valve_state vs INNER JOIN 
	   valves v ON vs.valve_key = v._key INNER JOIN 
	   valve_to_berth vb ON vb.valve_key = v._key INNER JOIN 
	   berths b ON b._key = vb.berth_key
WHERE        (state = 1)
ORDER BY timestamp DESC, b.berth_description



-- valve activity info
Select va.valve_start, va.valve_stop, v.valve_name, s.skid_description, b.berth_description, va.valve_bbl, va.valve_skid_batch_id
from valve_activity va INNER JOIN 
	 valves v ON va.valve_key = v._key INNER JOIN 
		meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
	 skids s ON s._key = v.valve_skid_key LEFT OUTER  JOIN 
	 berths b ON b._key = va.valve_berth_key
WHERE valve_skid_batch_id <> 'seed row'
ORDER BY berth_description, valve_start

-- get meter readingds for a certain skid onlu after a certain time 
SELECT        meter_readings._key, meter_readings.meter_key, meter_readings.meter_timestamp, meter_readings.meter_totalizer, skids.skid_description, meters.meter_name, 
                         skids._key AS Expr1, skid_batch_ids.skid_batch_id
FROM            meter_readings INNER JOIN
                         meters ON meter_readings.meter_key = meters._key INNER JOIN
                         skids ON meters.skid_key = skids._key INNER JOIN
                         skid_batch_ids ON skids._key = skid_batch_ids.skid_key AND meter_readings.meter_timestamp = skid_batch_ids.timestamp
WHERE        (skids._key = '3c789c2d-6710-4dc3-99bb-b31571409090') AND (meter_readings.meter_timestamp BETWEEN '2020-04-23 23:15' AND '2020-04-25 10:20')
ORDER BY meter_name, meter_timestamp desc


-- meter, skid, pipelines 
SELECT m.meter_name, s.skid_description, p.description
FROM meters m INNER JOIN 
	 pipeline p on m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key


	 	 select va._key,  SUM(mr.meter_totalizer) as start_totalizer --, m.meter_name, va.valve_key, va.valve_start, va.valve_stop
		 from valve_activity va  INNER JOIN 
			  valves v ON va.valve_key = v._key INNER JOIN 
			  meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
			  meter_readings mr ON mr.meter_key = m._key
		 WHERE --va._key = 'F2018298-A7F2-4022-8015-FAB65C9774A4'	AND	   
		    mr.meter_timestamp = va.valve_start
		 GROUP BY va._key

		 -- 52 rows meters with min and max readings in meter readings table 
		 SELECT DISTINCT m.meter_name, m.skid_name, MIN(meter_totalizer), MAX(meter_totalizer)
		 FROM meter_readings mr INNER JOIN 
		 meters m on m._key = mr.meter_key
		 GROUP BY m.meter_name, m.skid_name

		 -- 52 rows 
		 select * from meters

		 
		 -- 52 rows tanks with min and max readings in tank readings table 
		 SELECT DISTINCT t.tank_name, MIN(tank_nsv), MAX(tank_nsv)
		 FROM tank_readings tr INNER JOIN 
		 tanks t on t._key = tr.tank_key
		 GROUP BY t.tank_name

-- meter opening reading for a certain date 
		 SELECT DATEADD(day, -1, pa.eod_datetime) as timestamp,  pa.eod_datetime as eod ,mr.meter_totalizer, m._key, m.meter_name, p.description, m._key-- SUM( ISNULL(mr.meter_totalizer,0)) as OpenTotalizer
FROM meters m INNER JOIN 
	 pipeline p on m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key INNER JOIN 
	 pipeline_actuals pa ON pa.pipeline_key = p._key
	 LEFT OUTER JOIN 
	 meter_readings mr ON mr.meter_key = m._key AND mr.meter_timestamp = DATEADD(day, -1, pa.eod_datetime)
WHERE pa.eod_datetime BETWEEN '2020-04-14' AND '2020-04-15'
--GROUP BY s._key, p._key, pa.eod_datetime


-- TANKs from yesterday 
SELECT t.tank_name,    ta.eod_datetime, ta.open_nsv, ta.close_nsv, ta.tank_nsv_change
FROM   tank_actuals AS ta INNER JOIN
       tanks AS t ON ta.tank_key = t._key
WHERE ta.eod_datetime =  DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)

-- pipeline from yesterday 

SELECT p.description, pa.eod_datetime, pa.open_totalizer, pa.close_totalizer, pa.gsv_bbls
FROM pipeline_actuals pa INNER JOIN 
	 pipeline p ON p._key = pa.pipeline_key
	 WHERE pa.eod_datetime BETWEEN  '4/14/2020' AND GETDATE()


-- get the totalizer totals by skid for a time period 

SELECT s.skid_description , mr.meter_timestamp, SUM(mr.meter_totalizer)
FROM meter_readings mr INNER JOIN 
	 meters m ON mr.meter_key = m._key INNER JOIN 
	 skids s ON m.skid_key = s._key
WHERE mr.meter_timestamp > '4/14/2020' AND s.skid_description = 'SK-1340A'
GROUP BY s.skid_description , mr.meter_timestamp
ORDER BY mr.meter_timestamp

--  *************** TEST RESULTS************************************************************************
-- test results pipeline 
SELECT       eod_datetime, pipeline_name, tank_name, brandis_value, calculated_tank_nsv, calculated_meter_bbls_nsv_based, diff_brandi_to_tank_nsv, diff_brandi_to_meter_nsv_based
FROM            testing_pipeline_to_tank
WHERE        (brandis_value <> 0) AND eod_datetime = '4/19/2020 07:00'

SELECT       eod_datetime, berth_name, tank_name, brandis_value, calculated_tank_nsv, calculated_meter_bbls_nsv_based, diff_brandi_to_tank_nsv, diff_brandi_to_meter_nsv_based
FROM            testing_tank_to_berth
WHERE        (brandis_value <> 0) AND eod_datetime = '4/20/2020 07:00'







-- skid batch id check 
select distinct s.skid_description, sb.skid_batch_id
from skid_batch_ids sb INNER JOIN 
	 skids s ON sb.skid_key = s._key
WHERE sb.skid_batch_id <> ''
ORDER BY skid_description	  


-- TANKs from yesterday 
SELECT t.tank_name,    ta.eod_datetime, ta.open_nsv, ta.close_nsv, ta.tank_nsv_change
FROM   tank_actuals AS ta INNER JOIN
       tanks AS t ON ta.tank_key = t._key
WHERE ta.eod_datetime =  DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)


-- Berth activity from yesterday 
Select va.valve_start, va.valve_stop, v.valve_name, s.skid_description, b.berth_description, va.valve_bbl, va.valve_skid_batch_id
from valve_activity va INNER JOIN 
	 valves v ON va.valve_key = v._key INNER JOIN 
		meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
	 skids s ON s._key = v.valve_skid_key LEFT OUTER  JOIN 
	 berths b ON b._key = va.valve_berth_key
WHERE valve_skid_batch_id <> 'seed row'
GROUP BY  va.valve_start, va.valve_stop, v.valve_name, s.skid_description, b.berth_description, va.valve_bbl, va.valve_skid_batch_id
ORDER BY berth_description, valve_start

-- pipeline activity 
SELECT p.description, pa.movement_date, pa.open_totalizer, pa.close_totalizer, pa.gsv_bbls
FROM pipeline_actuals pa INNER JOIN 
	 pipeline p ON p._key = pa.pipeline_key
	 WHERE pa.movement_date BETWEEN  '4/14/2020' AND GETDATE()

	 -- total berth activity 

	 SELECT b.berth_description, ba.berth_start, ba.berth_stop, ba.berth_nsv
	 FROM berth_activity ba INNER JOIN 
		  berths b ON b._key = ba.berth_key



SELECT va.valve_start, va.valve_stop, s.skid_description, va.valve_bbl, va.valve_skid_batch_id, b.berth_description
FROM valve_activity va INNER JOIN 
	 skids s ON va.valve_skid_key = s._key INNER JOIN 
	 berths b ON b._key = va.valve_berth_key
ORDER BY b.berth_description, va.valve_start




