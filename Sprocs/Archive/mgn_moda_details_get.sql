USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_moda_details_get]    Script Date: 3/25/2021 3:39:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_moda_details_get
@row = 'f1d7731a-117d-4377-b06a-377f0284e671'
 
	*/

ALTER         PROCEDURE [dbo].[mgn_moda_details_get] 

@row uniqueidentifier

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from addresses

SELECT        vi._key as [_key],
			  a.company_name as [moda_name],
			  a.company_address as [moda_address],
			  vi.vi_invoice_number as [moda_invoice_number],
			  vi.vi_accounting_date as [accounting_date]


			  
FROM		  vessel_invoice AS vi INNER JOIN
			  addresses AS a ON vi.parent_company_address_key = a._key

WHERE		  @row = vi._key
    
END
