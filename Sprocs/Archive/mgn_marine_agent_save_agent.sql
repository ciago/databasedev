USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_marine_agent_save_agent]    Script Date: 3/23/2021 12:32:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Save the selected marine agent to the invoice record.

/*EXEC mgn_marine_agent_save_agent
	@row = 'acb965d5-53c4-48de-afe3-1e6f8cd47358',
	@marine_agent_key ='79c4533f-5e5a-4ecb-ab8a-f58fbf10b85a'
	
	// 
	*/
-- =============================================
ALTER		     PROCEDURE [dbo].[mgn_marine_agent_save_agent]

	@row uniqueidentifier,
	@marine_agent_key uniqueidentifier,
	@vessel_invoice_status_key uniqueidentifier

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from vessel_invoice
--Where _key = @row


UPDATE vessel_invoice

SET			vi_marine_agent_key = @marine_agent_key,
			vi_vessel_invoice_status_key = @vessel_invoice_status_key

WHERE _key = @row

END

