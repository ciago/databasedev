USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_daily_tank_overview]    Script Date: 4/5/2021 9:11:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--delete from pipeline_allocations
--USE [Moda]
--GO
--/****** Object:  StoredProcedure [dbo].[mgn_tank_overview_get]    Script Date: 5/1/2020 7:35:46 AM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		Candice Iago	
---- Create date: 6/26/20
---- Description:	Gets all of the tank overview records for the month

/*EXEC mgn_daily_tank_overview 
	@date = '6/18/2020'
	,@entity_key = 'd25051f3-4888-4bfb-89fd-4e1449ac0fba'
	--'86c0cacb-b542-49c8-b352-8be35f276541'  'dd85132f-4039-4fda-8fcc-bbd2dd9b7656'(OEMI) 'd25051f3-4888-4bfb-89fd-4e1449ac0fba'(Vitol)
	,@site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
	 ,@sql = ''

	 EXEC mgn_daily_tank_overview 
	 @date = '6/18/2020',
	 @site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	 @sql = ''
	*/
---- =============================================

ALTER       PROCEDURE [dbo].[mgn_daily_tank_overview]
-- 	@pipeline_key uniqueidentifier , 
--DECLARE 
	@date  datetime = null
	--,@product_key uniqueidentifier = NULL -- 
	--'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0' -- WTI, 'aaf2e572-0a5a-483a-8bb0-41573e832ab7' -- WTL, 'ec6741ed-4534-4260-9a42-6f168e2d4fbb'  EFCR
	,@entity_key uniqueidentifier = null --'86c0cacb-b542-49c8-b352-8be35f276541'  'dd85132f-4039-4fda-8fcc-bbd2dd9b7656'(OEMI) 'd25051f3-4888-4bfb-89fd-4e1449ac0fba'(Vitol)
	,@site_key uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
	 ,@sql     NVARCHAR(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @eod datetime = DATETIMEFROMPARTS(Year(@date),MONTH(@date),DAY(@DATE)+1,7,0,0,0)
--DECLARE @eod datetime = [dbo].[get_eod_datetime](@date)
print @eod
	
DECLARE 
    @columns NVARCHAR(MAX) = 'Beginning,',
    @columns2 NVARCHAR(MAX) = 'ISNULL(Beginning,0) as Beginning,';

SELECT 
    @columns += QUOTENAME( p.short_code + '-' + prod.product_code ) + ',',
	@columns2 += 'ISNULL(' + QUOTENAME( p.short_code + '-' + prod.product_code ) + ',0) as '+ QUOTENAME( p.short_code + '-' + prod.product_code ) +','
FROM 
    pipeline p INNER JOIN 
	pipeline_product_assignment ppa ON p._key = ppa.pipeline_key INNER JOIN 
	products prod ON ppa.product_key = prod._key INNER JOIN 
	pipeline_entity_assignment pep ON pep.entity_key = @entity_key AND pep.pipeline_key = ppa.pipeline_key
WHERE p.active = 1 and ppa.start_datetime <= @eod AND (ppa.end_datetime >= @eod OR ppa.end_datetime IS NULL)
ORDER BY 
    p.description;

print @columns2	

SET @columns += 'Receipts,';
SET @columns2 += 'ISNULL(Receipts,0) as Receipts,';

SELECT 
    @columns += QUOTENAME( b.berth_description) + ',',
    @columns2 += 'ISNULL(' +  QUOTENAME( b.berth_description) + ',0) as '+ QUOTENAME( b.berth_short_name) +','
FROM 
    berths b 
WHERE b.berth_active = 1
ORDER BY 
    b.berth_description;
	
SET @columns += 'Deliveries,';
SET @columns2 += 'ISNULL(Deliveries,0) as Deliveries,';

SET @columns += 'Transfers,';
SET @columns2 += 'ISNULL(Transfers,0) as Transfers,';

SET @columns += 'Ending,[Tank Bottom],[Tank Top],';
SET @columns2 += 'ISNULL(Ending,0) as Ending,ISNULL([Tank Bottom],0) as [Tank Bottom],ISNULL([Tank Top],0) as [Tank Top],'

SET @columns = LEFT(@columns, LEN(@columns) - 1);
SET @columns2 = LEFT(@columns2, LEN(@columns2) - 1);

--PRINT @columns;
CREATE TABLE #data  (tk_scheduled_volume int, location_name nvarchar(50), tank nvarchar(50))

-- insert 0 rows to remove all nulls from pivot table 

INSERT INTO #data (tk_scheduled_volume, location_name, tank )
SELECT ISNULL(t.tank_min_operational_limit, 25000), 
	   'Tank Bottom',
	   t.tank_name + ' - ' + ISNULL(te.short_name, '')
FROM tanks t  INNER JOIN 
	 (SELECT * 
	  FROM tank_owner tow 
	  WHERE tow.tk_start_datetime <= DATEADD(DAY, 1, @eod) 
		    AND (tow.tk_end_datetime >= @eod OR tow.tk_end_datetime IS NULL)
			AND (tow.tank_owner_key = @entity_key OR @entity_key IS NULL)) tow ON tow.tank_key = t._key INNER JOIN 
	 entities te ON te._key = tow.tank_owner_key
WHERE t.active = 1


INSERT INTO #data (tk_scheduled_volume, location_name, tank )
SELECT ISNULL(t.tank_max_operational_limit, 335000),
	   'Tank Top',
	   t.tank_name + ' - ' + ISNULL(te.short_name, '')
FROM tanks t  INNER JOIN 
	 (SELECT * 
	  FROM tank_owner tow 
	  WHERE tow.tk_start_datetime <= DATEADD(DAY, 1, @eod) 
		    AND (tow.tk_end_datetime >= @eod OR tow.tk_end_datetime IS NULL)
			AND (tow.tank_owner_key = @entity_key OR @entity_key IS NULL)) tow ON tow.tank_key = t._key INNER JOIN 
	 entities te ON te._key = tow.tank_owner_key
WHERE t.active = 1


DECLARE @inventory_projections TABLE (eod_datetime datetime, tank_key uniqueidentifier, beginning float, receipts float, 
									  shipments float, transfers float, ending_book float) 


--*****************************************************************************

DECLARE @have_actual_inventory bit = (SELECT CASE WHEN (SELECT TOP(1) open_gsv FROM tank_actuals WHERE eod_datetime = @eod AND open_gsv IS NOT NULL) IS NOT NULL THEN 1 ELSE 0 END)
print @have_actual_inventory

IF (@have_actual_inventory = 1) 
BEGIN 
	-- insert BEGINNING into #data 
	INSERT INTO #data (tk_scheduled_volume, location_name, tank )
	SELECT ta.open_gsv, 
		   'Beginning',
		   t.tank_name + ' - ' + ISNULL(te.short_name, '')
	FROM tank_actuals ta INNER JOIN 
		 tanks t ON ta.tank_key = t._key INNER JOIN 
		 (SELECT * 
		  FROM tank_owner tow 
		  WHERE tow.tk_start_datetime <= DATEADD(DAY, 1, @eod) 
				AND (tow.tk_end_datetime >= @eod OR tow.tk_end_datetime IS NULL)
				AND (tow.tank_owner_key = @entity_key OR @entity_key IS NULL)) tow ON tow.tank_key = t._key INNER JOIN 
		 entities te ON te._key = tow.tank_owner_key
	WHERE ta.eod_datetime = @eod AND t.active = 1 


	-- insert ENDING into #data 
	INSERT INTO #data (tk_scheduled_volume, location_name, tank )
	SELECT ta.close_gsv, 
		   'Ending',
		   t.tank_name + ' - ' + ISNULL(te.short_name, '')
	FROM tank_actuals ta INNER JOIN 
		 tanks t ON ta.tank_key = t._key INNER JOIN 
		 (SELECT * 
		  FROM tank_owner tow 
		  WHERE tow.tk_start_datetime <= DATEADD(DAY, 1, @eod) 
				AND (tow.tk_end_datetime >= @eod OR tow.tk_end_datetime IS NULL)
				AND (tow.tank_owner_key = @entity_key OR @entity_key IS NULL)) tow ON tow.tank_key = t._key INNER JOIN 
		 entities te ON te._key = tow.tank_owner_key
	WHERE ta.eod_datetime = @eod AND t.active = 1 
END
ELSE 
BEGIN 
	-- insert BEGINNING into #data 
	INSERT INTO #data (tk_scheduled_volume, location_name, tank )
	SELECT ta.beginning, 
		   'Beginning',
		   t.tank_name + ' - ' + ISNULL(te.short_name, '')
	FROM (SELECT * FROM get_inventory_projections(@eod)) ta INNER JOIN 
		 tanks t ON ta.tank_key = t._key INNER JOIN 
		 (SELECT * 
		  FROM tank_owner tow 
		  WHERE tow.tk_start_datetime <= DATEADD(DAY, 1, @eod) 
				AND (tow.tk_end_datetime >= @eod OR tow.tk_end_datetime IS NULL)
				AND (tow.tank_owner_key = @entity_key OR @entity_key IS NULL)) tow ON tow.tank_key = t._key INNER JOIN 
		 entities te ON te._key = tow.tank_owner_key
	WHERE ta.eod_datetime = @eod AND t.active = 1 


	-- insert ENDING into #data 
	INSERT INTO #data (tk_scheduled_volume, location_name, tank )
	SELECT ta.ending_book, 
		   'Ending',
		   t.tank_name + ' - ' + ISNULL(te.short_name, '')
	FROM (SELECT * FROM get_inventory_projections(@eod)) ta INNER JOIN 
		 tanks t ON ta.tank_key = t._key INNER JOIN 
		 (SELECT * 
		  FROM tank_owner tow 
		  WHERE tow.tk_start_datetime <= DATEADD(DAY, 1, @eod) 
				AND (tow.tk_end_datetime >= @eod OR tow.tk_end_datetime IS NULL)
				AND (tow.tank_owner_key = @entity_key OR @entity_key IS NULL)) tow ON tow.tank_key = t._key INNER JOIN 
		 entities te ON te._key = tow.tank_owner_key
	WHERE ta.eod_datetime = @eod AND t.active = 1 

END 

-- insert receipt and shipment TICKETS into #data 
INSERT INTO #data (tk_scheduled_volume, location_name, tank )
SELECT CASE WHEN tt.type_is_receipt = 1 THEN t.tk_scheduled_volume ELSE (t.tk_scheduled_volume * -1) END as tk_scheduled_volume,
       ISNULL(pl.short_code + '-' + p.product_code, b.berth_description) as location_name, 
	   ISNULL(from_t.tank_name, to_t.tank_name)  + ' - ' + ISNULL(te.short_name, '') as tank
FROM tickets t INNER JOIN 
     ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
     ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN 
     entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN -- entity1 is always going to be the site default entity type
     products p On t.tk_product = p._key LEFT OUTER JOIN 
     ticket_type_fields ttf on ttf.type_fie_ticket_type = tt._key left outer join
     uom on uom.uom_uom = t.tk_volume_uom LEFT OUTER JOIN 
     entities e1 ON e1._key = t.tk_entity1_key LEFT OUTER JOIN
     tanks from_t ON from_t._key = tk_source_tank_key LEFT OUTER JOIN 
     pipeline pl ON pl._key = t.tk_pipeline_key LEFT OUTER JOIN 
     berths b ON b._key = t.tk_berth_key LEFT OUTER JOIN 
     tanks to_t ON to_t._key = tk_destination_tank_key  LEFT OUTER JOIN 
	 (SELECT * 
	  FROM tank_owner tow 
	  WHERE tow.tk_start_datetime <= DATEADD(DAY, 1, @eod) 
		    AND (tow.tk_end_datetime >= @eod OR tow.tk_end_datetime IS NULL)
			AND (tow.tank_owner_key = @entity_key OR @entity_key IS NULL)) tow ON (tow.tank_key = to_t._key OR tow.tank_key = from_t._key) LEFT OUTER JOIN 
	 entities te ON te._key = tow.tank_owner_key
WHERE tk_eod_datetime = @eod AND t.tk_site_key = @site_key
      AND (@entity_key = t.tk_entity1_key OR @entity_key IS NULL)
      --AND (@product_key = t.tk_product OR @product_key IS NULL)
      AND (t.tk_deleted = 0)
	  AND t.tk_status <> 7 -- not voided

-- insert TRANSFER receipt  TICKETS into #data 
INSERT INTO #data (tk_scheduled_volume, location_name, tank )
SELECT t.tk_scheduled_volume as tk_scheduled_volume,
       'Transfers' as location_name, 
	   to_t.tank_name  + ' - ' + ISNULL(te.short_name, '') as tank
FROM tickets t INNER JOIN 
     ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
     ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN 
     entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN -- entity1 is always going to be the site default entity type
     products p On t.tk_product = p._key LEFT OUTER JOIN 
     ticket_type_fields ttf on ttf.type_fie_ticket_type = tt._key left outer join
     uom on uom.uom_uom = t.tk_volume_uom LEFT OUTER JOIN 
     entities e1 ON e1._key = t.tk_entity1_key LEFT OUTER JOIN
     pipeline pl ON pl._key = t.tk_pipeline_key LEFT OUTER JOIN 
     berths b ON b._key = t.tk_berth_key LEFT OUTER JOIN 
     tanks to_t ON to_t._key = tk_destination_tank_key  LEFT OUTER JOIN 
	 (SELECT * 
	  FROM tank_owner tow 
	  WHERE tow.tk_start_datetime <= DATEADD(DAY, 1, @eod) 
		    AND (tow.tk_end_datetime >= @eod OR tow.tk_end_datetime IS NULL)
			AND (tow.tank_owner_key = @entity_key OR @entity_key IS NULL)) tow ON (tow.tank_key = to_t._key) LEFT OUTER JOIN 
	 entities te ON te._key = tow.tank_owner_key
WHERE tk_eod_datetime = @eod AND t.tk_site_key = @site_key
      AND (@entity_key = t.tk_entity1_key OR @entity_key IS NULL)
      --AND (@product_key = t.tk_product OR @product_key IS NULL)
      AND (t.tk_deleted = 0)
	  AND t.tk_status <> 7 -- not voided
	  AND tt.type_is_transfer = 1 

-- insert TRANSFER shipment  TICKETS into #data 
INSERT INTO #data (tk_scheduled_volume, location_name, tank )
SELECT (t.tk_scheduled_volume * -1) as tk_scheduled_volume,
       'Transfers' as location_name, 
	   from_t.tank_name  + ' - ' + ISNULL(te.short_name, '') as tank
FROM tickets t INNER JOIN 
     ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
     ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN 
     entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN -- entity1 is always going to be the site default entity type
     products p On t.tk_product = p._key LEFT OUTER JOIN 
     ticket_type_fields ttf on ttf.type_fie_ticket_type = tt._key left outer join
     uom on uom.uom_uom = t.tk_volume_uom LEFT OUTER JOIN 
     entities e1 ON e1._key = t.tk_entity1_key LEFT OUTER JOIN
     pipeline pl ON pl._key = t.tk_pipeline_key LEFT OUTER JOIN 
     berths b ON b._key = t.tk_berth_key LEFT OUTER JOIN 
     tanks from_t ON from_t._key = tk_source_tank_key  LEFT OUTER JOIN 
	 (SELECT * 
	  FROM tank_owner tow 
	  WHERE tow.tk_start_datetime <= DATEADD(DAY, 1, @eod) 
		    AND (tow.tk_end_datetime >= @eod OR tow.tk_end_datetime IS NULL)
			AND (tow.tank_owner_key = @entity_key OR @entity_key IS NULL)) tow ON (tow.tank_key = from_t._key) LEFT OUTER JOIN 
	 entities te ON te._key = tow.tank_owner_key
WHERE tk_eod_datetime = @eod AND t.tk_site_key = @site_key
      AND (@entity_key = t.tk_entity1_key OR @entity_key IS NULL)
      --AND (@product_key = t.tk_product OR @product_key IS NULL)
      AND (t.tk_deleted = 0)
	  AND t.tk_status <> 7 -- not voided
	  AND tt.type_is_transfer = 1 

	  
-- insert total receipts into #data 
INSERT INTO #data (tk_scheduled_volume, location_name, tank )
SELECT SUM(t.tk_scheduled_volume) as tk_scheduled_volume,
       'Receipts' as location_name, 
	   to_t.tank_name  + ' - ' + ISNULL(te.short_name, '') as tank
FROM tickets t INNER JOIN 
     ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
     ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN 
     entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN -- entity1 is always going to be the site default entity type
     products p On t.tk_product = p._key LEFT OUTER JOIN 
     ticket_type_fields ttf on ttf.type_fie_ticket_type = tt._key left outer join
     uom on uom.uom_uom = t.tk_volume_uom LEFT OUTER JOIN 
     entities e1 ON e1._key = t.tk_entity1_key LEFT OUTER JOIN
     pipeline pl ON pl._key = t.tk_pipeline_key LEFT OUTER JOIN 
     tanks to_t ON to_t._key = tk_destination_tank_key  LEFT OUTER JOIN 
	 (SELECT * 
	  FROM tank_owner tow 
	  WHERE tow.tk_start_datetime <= DATEADD(DAY, 1, @eod) 
		    AND (tow.tk_end_datetime >= @eod OR tow.tk_end_datetime IS NULL)
			AND (tow.tank_owner_key = @entity_key OR @entity_key IS NULL)) tow ON (tow.tank_key = to_t._key) LEFT OUTER JOIN 
	 entities te ON te._key = tow.tank_owner_key
WHERE tk_eod_datetime = @eod AND t.tk_site_key = @site_key
      AND (@entity_key = t.tk_entity1_key OR @entity_key IS NULL)
      --AND (@product_key = t.tk_product OR @product_key IS NULL)
      AND (t.tk_deleted = 0)
	  AND t.tk_status <> 7 -- not voided
	  AND tt.type_is_receipt = 1
GROUP BY to_t.tank_name, te.short_name	


-- insert total SHIPMENTS into #data 
INSERT INTO #data (tk_scheduled_volume, location_name, tank )
SELECT (SUM(t.tk_scheduled_volume)* -1) as tk_scheduled_volume,
       'Deliveries' as location_name, 
	   from_t.tank_name  + ' - ' + ISNULL(te.short_name, '') as tank
FROM tickets t INNER JOIN 
     ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
     ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN 
     entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN -- entity1 is always going to be the site default entity type
     products p On t.tk_product = p._key LEFT OUTER JOIN 
     ticket_type_fields ttf on ttf.type_fie_ticket_type = tt._key left outer join
     uom on uom.uom_uom = t.tk_volume_uom LEFT OUTER JOIN 
     entities e1 ON e1._key = t.tk_entity1_key LEFT OUTER JOIN
     pipeline pl ON pl._key = t.tk_pipeline_key LEFT OUTER JOIN 
     tanks from_t ON from_t._key = tk_source_tank_key  LEFT OUTER JOIN 
	 (SELECT * 
	  FROM tank_owner tow 
	  WHERE tow.tk_start_datetime <= DATEADD(DAY, 1, @eod) 
		    AND (tow.tk_end_datetime >= @eod OR tow.tk_end_datetime IS NULL)
			AND (tow.tank_owner_key = @entity_key OR @entity_key IS NULL)) tow ON (tow.tank_key = from_t._key) LEFT OUTER JOIN 
	 entities te ON te._key = tow.tank_owner_key
WHERE tk_eod_datetime = @eod AND t.tk_site_key = @site_key
      AND (@entity_key = t.tk_entity1_key OR @entity_key IS NULL)
      --AND (@product_key = t.tk_product OR @product_key IS NULL)
      AND (t.tk_deleted = 0)
	  AND t.tk_status <> 7 -- not voided
	  AND tt.type_is_shipment = 1
GROUP BY from_t.tank_name, te.short_name
--SELECT * from #data

	-- scheduled pipeline amounts 
INSERT INTO #data (tk_scheduled_volume, location_name, tank )
SELECT (SUM(allocated_amount) * -1) as allocated_amount, pl.short_code + '-' + p.product_code, '  Nominated'
FROM pipeline_allocations pa INNER JOIN 
	 products p ON p._key = pa.product_key INNER JOIN 
	 pipeline pl ON pl._key = pa.pipeline_key
WHERE pa.eod_datetime = @eod AND pa.entity_key = @entity_key
GROUP BY eod_datetime, product_key, pipeline_key , pl.short_code , p.product_code

-- scheduled berth amounts 
INSERT INTO #data (tk_scheduled_volume, location_name, tank )
SELECT SUM(projected_volume), ISNULL(b.berth_description, 'no berth'), '  Nominated'
FROM
(select vp.projected_volume as projected_volume,  
	   dbo.get_eod_datetime( DATEADD(day, vp.day_number - 1, st.syn_vessel_eta)) as eod --DATEADD(day, vp.day_number - 1, st.syn_vessel_eta) as eta,
	   ,b._key as berth_key
FROM vessel_projected_volumes vp INNER JOIN 
	synthesis_tickets st ON vp.synthesis_key = st._key INNER JOIN 
	 entities e ON st.syn_shipper_id = e.external_id INNER JOIN 
	 products p ON p.product_external_id = st.syn_product_code left outer JOIN 
	 berths b ON st.syn_dock_name = b.berth_external_code
WHERE syn_vessel_lay_status <> 'REJECTED' 
	  and (e._key= @entity_key OR @entity_key IS NULL)
	  --AND (p._key = @product_key OR @product_key IS NULL)
	  AND st.syn_vessel_eta BETWEEN DATEADD(DAY, -7, @eod) AND DATEADD(DAY, 7, @eod)) as t  left outer JOIN 
	 berths b ON t.berth_key = b._key
WHERE t.eod = @eod
GROUP BY b.berth_description

-- GET THE DIFFERENCE 
INSERT INTO #data (tk_scheduled_volume, location_name, tank )
SELECT SUM(tk_scheduled_volume), location_name, ' Difference'
FROM #data
WHERE location_name NOT IN ('Beginning','Ending','[Tank Bottom]','[Tank Top]')
GROUP BY location_name

UPDATE #data
SET tk_scheduled_volume = tk_scheduled_volume * -1
WHERE tk_scheduled_volume < 0

SET @sql ='
SELECT tank as [Description], '+ @columns2 +' FROM   
(
SELECT tk_scheduled_volume,
	   location_name, 
	   tank
FROM #data 
) t 
PIVOT(
    SUM(tk_scheduled_volume) 
    FOR location_name IN ('+ @columns +')
) AS pivot_table;';

--print @sql 
-- execute the dynamic SQL
EXECUTE sp_executesql @sql;

drop table #data
END

