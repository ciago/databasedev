USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_volume_types_get_list]    Script Date: 4/6/2021 11:03:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table

/*EXEC [mgn_menu_parents_get_list]
	*/
-- =============================================
Create or ALTER       PROCEDURE [dbo].[mgn_menu_parents_get_list]
	@user nvarchar(50) = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Declare @company_name nvarchar(100) = (select an_company_name from [dbo].[app_name])
Declare @company_name_key uniqueidentifier = (select _key from [dbo].[app_name])

SELECT mi.menu_label_name as label_text,
	   default_child.menu_action_result_name as action_result, 
	   default_child.menu_controller as controller,
	   mi.menu_number as parent_number,
	   @company_name as company_name,
	   @company_name_key as company_key

FROM menu_items mi INNER JOIN 
     menu_items default_child ON mi.menu_default_child_number = default_child.menu_number

WHERE mi.menu_active = 1 
      AND mi.menu_is_parent = 1

ORDER BY mi.menu_sort
	
END
