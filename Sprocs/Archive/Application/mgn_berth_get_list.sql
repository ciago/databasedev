USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_berth_get_list]    Script Date: 6/1/2020 3:10:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
CREATE OR ALTER   PROCEDURE [dbo].[mgn_berth_get_list] 
    @site_key uniqueidentifier = null, 
	@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();


	SELECT '00000000-0000-0000-0000-000000000000', 'Select Berth'
	UNION
    SELECT _key, berth_short_name
    FROM berths
	WHERE berth_site = @site_key
    
END