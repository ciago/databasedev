USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_volume_accounts_delete]    Script Date: 12/11/2020 12:05:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Deletes a single row of volume accounts data based on the selected row/record.

/*EXEC mgn_berth_fees_delete
	@row  = '5dbdc516-110e-4f20-ab7a-b768078125dd'
	*/
-- =============================================
create or ALTER           PROCEDURE [dbo].[mgn_volume_accounts_delete]
	@row uniqueidentifier = null,
	@user varchar(50) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from schedule
--WHere _key = @row
Delete From volume_accounts

Where _key = @row

END