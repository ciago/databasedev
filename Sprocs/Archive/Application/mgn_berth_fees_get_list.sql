USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_berth_fees_get_list]    Script Date: 9/28/2020 9:21:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Brandon Morey    
-- Create date: 5/4/2020
-- Description:    Gets a list of berth fees from a selected day.

/*EXEC mgn_berth_fees_get_list  
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
	
    */
	
-- =============================================
CREATE or ALTER      PROCEDURE [dbo].[mgn_berth_fees_get_list]

	@site uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from berth_fees

IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END

SELECT        bf._key,
			  bf.site_key as [site_key],
			  bf.start_loa as [start_loa],
			  bf.end_loa as [end_loa],
			  bf.loa_fee as [loa_fee]
			  
FROM		  berth_fees AS bf INNER JOIN
			  sites AS s ON bf.site_key = s._key

WHERE		  bf.site_key = @site
	  
END