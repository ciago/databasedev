--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_user_groups_test_get_list]    Script Date: 9/28/2020 9:21:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:    Candice Iago   
-- Create date: 4/28/21
-- Description:   gets user group list

/*EXEC mgn_user_groups_test_get_list  

	
    */
	
-- =============================================
CREATE or ALTER      PROCEDURE [dbo].mgn_user_groups_test_get_list
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


-- THERE IS NO SITE ON HERE BECAUSE AT THIS POINT WE DO NOT KNOW THE SITE, we only know the system


SELECT  [_key], 
	    [user_group_name],
		[user_group_hierarchy]
FROM user_groups_test
WHERE [user_group_active] = 1
	  
END