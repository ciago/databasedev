USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_site_config_get_list]    Script Date: 3/9/2021 2:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table

/*EXEC [mgn_site_config_get_list]
	@site  = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@user  = 'candice'
	*/
-- =============================================
ALTER         PROCEDURE [dbo].[mgn_site_config_get_list]
	/*Defining the variable that I will eventually be filtering the table by*/
	@site uniqueidentifier = null,
	@user nvarchar(50) = 'candice'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--Where _key = @volume_type

--DECLARE @eod datetime = SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date), 07, 00)

Select sc._key,
	   sc.site_key,
	   sc.save_pipeline_import_csv,
	   sc.tank_settle_time_hours,
	   sc.uom_default_volume,
	   sc.uom_default_mass,
	   sc.uom_default_temperature,
	   sc.uom_default_pressure,
	   sc.uom_default_density,
	   sc.uom_default_length,
	   sc.default_entity_type_1,
	   et.type_description,
	   sc.default_location_type,
	   lt.lt_type_desc

from site_configuration as sc inner join
	 entity_types et on sc.default_entity_type_1 = et.type_code left outer join
	 location_types lt on sc.default_location_type = lt.lt_location_type

where sc.site_key = @site
	
END
