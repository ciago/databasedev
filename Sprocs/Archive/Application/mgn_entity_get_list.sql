--USE [Hartree_TMS]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_entity_get_list]    Script Date: 4/9/2021 1:58:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER    PROCEDURE [dbo].[mgn_entity_get_list] 
	@type int = null
	--@terminal uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT '00000000-0000-0000-0000-000000000000' as _key, ' Select entity' as name
	UNION
    SELECT _key, name
	FROM entities
	WHERE (type = @type or @type is null)
	AND active = 1
	ORDER BY name
	
END
