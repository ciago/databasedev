USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_vessel_activity_get]    Script Date: 9/11/2020 8:23:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets one row from the vessel activity table

/*EXEC mgn_vessel_activity_get
	@row  = 'ad052978-c6df-4947-b0e4-cc5ffc4c3056'
	*/
-- =============================================
ALTER     PROCEDURE [dbo].[mgn_vessel_activity_get]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row

--DECLARE @eod datetime = SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date), 07, 00)


Select va._key, 
	   va.valve_start, 
	   va.valve_stop, 
	   s.skid_description, 
	   va.valve_skid_key as skid_key,
	   b.berth_description, 
	   va.valve_berth_key as berth_key, 
	   va.valve_bbl, 
	   ISNULL(va.valve_skid_batch_id, '') as valve_skid_batch_id, 
	   va.valve_ignore
from valve_activity va LEFT OUTER JOIN 
	 valves v ON va.valve_key = v._key LEFT OUTER JOIN 
		meters m ON v.valve_skid_key = m.skid_key LEFT OUTER JOIN
	 skids s ON s._key = va.valve_skid_key LEFT OUTER  JOIN 
	 berths b ON b._key = va.valve_berth_key
WHERE va._key = @row
GROUP BY va._key, va.valve_start, va.valve_stop, v.valve_name, s.skid_description, va.valve_skid_key, b.berth_description, va.valve_bbl, va.valve_skid_batch_id, va.valve_ignore, valve_berth_key
--ORDER BY berth_description, valve_start
	
END
