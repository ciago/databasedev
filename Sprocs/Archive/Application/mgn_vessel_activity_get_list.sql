--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_vessel_activity_get_list]    Script Date: 4/28/2021 9:13:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 5/15/2020
-- Description:	gets a list of vessel activity records for the selected date

/*EXEC mgn_vessel_activity_get_list
	@date  = '4/27/2021'
	*/
-- =============================================
ALTER     PROCEDURE [dbo].[mgn_vessel_activity_get_list]
	@date datetime = null, 
	@site uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row

DECLARE @eod datetime = DATEADD(DAY, 1, SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date)  , 07, 00))

IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END


Select DISTINCT va._key, 
	    va.valve_start, 
		va.valve_stop, 
		s.skid_description, 
		b.berth_description, 
		ISNULL(va.valve_bbl,0) as valve_bbl, 
		ISNULL(va.valve_skid_batch_id, '') as valve_skid_batch_id, 
		va.valve_ignore, 
		va.valve_eod_datetime, 
		ISNULL(syn.syn_customer_ref,'') as syn_customer_ref --, syn_vessel_eta 
from valve_activity va LEFT OUTER JOIN 
	 valves v ON va.valve_key = v._key LEFT OUTER JOIN 
		meters m ON v.valve_skid_key = m.skid_key LEFT OUTER JOIN
	 skids s ON s._key = va.valve_skid_key LEFT OUTER  JOIN 
	 berths b ON b._key = va.valve_berth_key LEFT OUTER JOIN
	 (SELECT syn_customer_ref,  syn_vessel_lay_status, syn_vessel_eta FROM synthesis_tickets WHERE syn_vessel_lay_status <> 'REJECTED' and syn_customer_ref IS NOT NULL) syn ON syn.syn_customer_ref = va.valve_skid_batch_id
WHERE (valve_skid_batch_id <> 'seed row' OR valve_skid_batch_id IS NULL)
      AND valve_eod_datetime = @eod AND va.site_key = @site
	  AND valve_is_bunker = 0
	 -- AND (valve_start > DATEADD(DAY, -7, syn_vessel_eta) OR syn_vessel_eta IS NULL)
GROUP BY va._key, va.valve_start, va.valve_stop, v.valve_name, s.skid_description, b.berth_description, va.valve_bbl, va.valve_skid_batch_id, va.valve_ignore, 
		 va.valve_eod_datetime, syn_customer_ref, syn_vessel_eta
ORDER BY berth_description, valve_start
	
END
