--USE [Moda_Matt]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tickets_update]    Script Date: 3/9/2021 3:22:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 5/15/2020
-- Description:	gets a list of vessel activity records for the selected date

/*EXEC mgn_tickets_update
	@row  = '6b9989b5-ea25-4ea5-8959-35eb067acc31', @volume =   106137.85458626457   
	*/
-- =============================================
ALTER     PROCEDURE [dbo].[mgn_tickets_update]
	@row uniqueidentifier = null, 
	@volume float, 
	@start datetime , 
	@end datetime , 
	@batch nvarchar(250),
	@user varchar(50) = 'default', 
	@machine varchar(50) = 'default'
	--, @type (pipeline, vessel, tank to tank transfer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Update record info 
DECLARE @record_info varchar(max) = (SELECT tk_record_info FROM tickets WHERE _key = @row)
SET @record_info = dbo.fn_json_record_info(@record_info, 'updated', @user, @machine, getdate());



UPDATE tickets
	SET tk_final_gsv = @volume,
		tk_record_info = @record_info, 
		tk_start_datetime = @start, 
		tk_end_datetime = @end, 
		tk_batch_id = @batch
WHERE _key = @row


-- UPDATE dates for ones that did not send properly 
UPDATE tickets
SET tk_start_datetime = DATEADD(DAY, -1, tk_eod_datetime)
WHERE tk_start_datetime = '2001-01-01 00:00'
AND _key = @row

UPDATE tickets
SET tk_end_datetime =  tk_eod_datetime
WHERE tk_end_datetime = '2001-01-01 00:00'
AND _key = @row


declare @site uniqueidentifier 
SET @site = (SELECT tk_site_key FROM tickets WHERE  _key = @row)
DECLARE @external_system_update bit 
SET @external_system_update = (SELECT site_update_ticket_external FROM site_configuration WHERE site_key = @site)

IF @external_system_update = 1 
BEGIN 
	UPDATE mft
	SET mft.batch_id = t.tk_batch_id
	FROM Moda_Final_Tickets mft INNER JOIN 
		 tickets t ON mft._key = t._key
	WHERE mft.batch_id <> t.tk_batch_id
	AND mft.synthesis_received IS NULL
	AND t._key = @row


	
	--UPDATE mft
	--SET mft.batch_id = t.tk_batch_id
	--FROM  EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets]  mft INNER JOIN 
	--	 tickets t ON mft._key = t._key
	--WHERE mft.batch_id <> t.tk_batch_id
	--AND mft.synthesis_received IS NULL
	--AND t._key = @row


	
	--SELECT * 
	--FROM Moda_Final_Tickets mft 
	UPDATE Moda_Final_Tickets
	SET start_datetime = DATEADD(DAY, -1, eod_datetime)
	WHERE start_datetime = '2001-01-01 00:00'
	AND _key = @row
	

	--UPDATE EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets]
	--SET start_datetime = DATEADD(DAY, -1, eod_datetime)
	--WHERE  start_datetime = '2001-01-01 00:00'
	--AND _key = @row
	
	--SELECT * 
	--FROM Moda_Final_Tickets mft 
	UPDATE Moda_Final_Tickets
	SET stop_datetime = eod_datetime
	WHERE stop_datetime = '2001-01-01 00:00'
	AND _key = @row

	
	--UPDATE EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets]
	--SET stop_datetime = eod_datetime
	--WHERE  stop_datetime = '2001-01-01 00:00'
	--AND _key = @row


END


END
