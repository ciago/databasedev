USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_ticket_status_get_dropdown]    Script Date: 4/12/2021 2:37:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
ALTER             PROCEDURE [dbo].[mgn_ticket_status_get_dropdown] 
	@site_key uniqueidentifier = null, 
	@user varchar(50) = 'default'

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


SELECT 0 AS RowKey, 'Select Type' As description
UNION
SELECT stat_type as RowKey, [stat_description] as description
FROM [dbo].[ticket_status]
WHERE active = 1
ORDER BY RowKey

END


-- EXEC [mgn_accounting_volume_types_get_list]





