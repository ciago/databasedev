USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_schedule_hourly_get]    Script Date: 5/1/2020 7:35:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/20
-- Description:	Gets all of the allocations for all days in a certain month for a certain pipeline

/*EXEC mgn_pipeline_schedule_hourly_get
	@pipeline_key  = 'fc536541-5a92-466a-bcd1-db7fedcd15e7', 
	@month = 6,
	@year = 2020 , @product_key = '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0'
	*/
-- =============================================
CREATE OR ALTER   PROCEDURE [dbo].[mgn_pipeline_schedule_hourly_get]
	@pipeline_key uniqueidentifier = null, 
	@month int, 
	@year int, 
	@product_key uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-- fill the tmp table with all pipeline schedules by product by hour
EXEC mgn_pipeline_schedule_fill_hourly
	@month = @month,
	@year  = @year

--IF(@product_key IS NULL)
--BEGIN
--		SELECT *
--		FROM   
--		(SELECT DATEADD(day, -1,eod_datetime) as [Start], 
--				--eod_datetime as [End], 
--				DATEPART(HH, start_datetime)  as start_time, SUM(volume) as volume
--				FROM tmp_pipeline_by_hours
--				WHERE (@pipeline_key IS NULL OR pipeline_key = @pipeline_key)
--				GROUP BY  start_datetime, eod_datetime
--		) as pivotData
--		PIVOT (
--		SUM(volume)
--		FOR start_time  IN 
--		([7] , [8] ,  [9],[10], [11],[12],[13],[14],[15], [16], [17], [18], [19],[20],[21],[22],[23],[0],[1],[2],[3],[4],[5],[6])
--		) as pivotTable
--		ORDER BY [Start];
--END
--ELSE
--BEGIN

declare @products TABLE (prod_key uniqueidentifier, cell_color varchar(30))
INSERT INTO @products(prod_key, cell_color)
SELECT DISTINCT p._key, p.product_color
FROM  tmp_pipeline_by_hours tmp INNER JOIN 
	  products p ON tmp.product_key = p._key
		  WHERE (@pipeline_key IS NULL OR pipeline_key = @pipeline_key) AND (product_key = @product_key OR @product_key IS NULL) AND volume <> 0

--select * from @products


		--SELECT *, ([7] + [8] +  [9]+[10]+ [11]+[12]+[13]+[14]+[15]+ [16]+ [17]+ [18]+ [19]+[20]+[21]+[22]+[23]+[0]+[1]+[2]+[3]+[4]+[5]+[6]) as [Total BD]
		--FROM   
		--  (SELECT DATEADD(day, -1,eod_datetime) as [Start], 
		--		--eod_datetime as [End], 
		--		DATEPART(HH, start_datetime)  as start_time, 
		--		SUM(volume) as volume, 
		--		COUNT(product_key) as number_prods
		--  FROM tmp_pipeline_by_hours
		--  WHERE (@pipeline_key IS NULL OR pipeline_key = @pipeline_key) AND (product_key = @product_key OR @product_key IS NULL)
		--  GROUP BY  start_datetime, eod_datetime
		--  ) as pivotData
		--PIVOT (
		--SUM(volume)
		--FOR start_time  IN 
		--([7] , [8] ,  [9],[10], [11],[12],[13],[14],[15], [16], [17], [18], [19],[20],[21],[22],[23],[0],[1],[2],[3],[4],[5],[6])
		--) as pivotTable
		--ORDER BY [Start];
--END
--SELECT DATEADD(day, -1,eod_datetime) as [Start], 
--				--eod_datetime as [End], 
--				DATEPART(HH, start_datetime)  as start_time, 
--				SUM(volume) as volume, 
--				CASE WHEN COUNT(DISTINCT product_key) = 1 THEN p.product_color ELSE 'khaki' END as prod_color
--		  FROM tmp_pipeline_by_hours tmp LEFT OUTER JOIN 
--			   products p ON p._key = tmp.product_key
--		  WHERE (@pipeline_key IS NULL OR pipeline_key = @pipeline_key) AND (product_key = @product_key OR @product_key IS NULL) AND volume <> 0
--		  GROUP BY  start_datetime, eod_datetime, product_color


--SELECT [Start],
--	   prod_color as [7 color], [7],
--	   prod_color as [8 color], [8],
--	   prod_color as [9 color], [9],
--	   prod_color as [10 color], [10],
--	   prod_color as [11 color], [11],
--	   prod_color as [12 color], [12],
--	   prod_color as [13 color], [13],
--	   prod_color as [14 color], [14],
--	   prod_color as [15 color], [15],
--	   prod_color as [16 color], [16],
--	   prod_color as [17 color], [17],
--	   prod_color as [18 color], [18],
--	   prod_color as [19 color], [19],
--	   prod_color as [20 color], [20],
--	   prod_color as [21 color], [21],
--	   prod_color as [22 color], [22],
--	   prod_color as [23 color], [23],
--	   prod_color as [0 color], [0],
--	   prod_color as [1 color], [1],
--	   prod_color as [2 color], [2],
--	   prod_color as [3 color], [3],
--	   prod_color as [4 color], [4],
--	   prod_color as [5 color], [5],
--	   prod_color as [6 color], [6],
--	   [Total BD]
--FROM


declare @records TABLE( [Start] datetime,
	   [7 color] nvarchar(30), [7] int,
	   [8 color] nvarchar(30), [8] int,
	   [9 color] nvarchar(30), [9] int,
	   [10 color] nvarchar(30), [10] int,
	   [11 color] nvarchar(30), [11] int,
	   [12 color] nvarchar(30), [12] int,
	   [13 color] nvarchar(30), [13] int,
	   [14 color] nvarchar(30), [14] int,
	   [15 color] nvarchar(30), [15] int,
	   [16 color] nvarchar(30), [16] int,
	   [17 color] nvarchar(30), [17] int,
	   [18 color] nvarchar(30), [18] int,
	   [19 color] nvarchar(30), [19] int,
	   [20 color] nvarchar(30), [20] int,
	   [21 color] nvarchar(30), [21] int,
	   [22 color] nvarchar(30), [22] int,
	   [23 color] nvarchar(30), [23] int,
	   [0 color] nvarchar(30), [0] int,
	   [1 color] nvarchar(30), [1] int,
	   [2 color] nvarchar(30), [2] int,
	   [3 color] nvarchar(30), [3] int,
	   [4 color] nvarchar(30), [4] int,
	   [5 color] nvarchar(30), [5] int,
	   [6 color] nvarchar(30), [6] int,
	   [Total BD] int )

INSERT INTO @records ( [Start] ,
	   [7 color] , [7] ,
	   [8 color] , [8] ,
	   [9 color] , [9] ,
	   [10 color] , [10] ,
	   [11 color] , [11] ,
	   [12 color] , [12] ,
	   [13 color] , [13] ,
	   [14 color] , [14] ,
	   [15 color] , [15] ,
	   [16 color] , [16] ,
	   [17 color] , [17] ,
	   [18 color] , [18] ,
	   [19 color] , [19] ,
	   [20 color] , [20] ,
	   [21 color] , [21] ,
	   [22 color] , [22] ,
	   [23 color] , [23] ,
	   [0 color] , [0] ,
	   [1 color] , [1] ,
	   [2 color] , [2] ,
	   [3 color] , [3] ,
	   [4 color] , [4] ,
	   [5 color] , [5] ,
	   [6 color] , [6] ,
	   [Total BD])
		SELECT [Start],
	   prod_color as [7 color], [7],
	   prod_color as [8 color], [8],
	   prod_color as [9 color], [9],
	   prod_color as [10 color], [10],
	   prod_color as [11 color], [11],
	   prod_color as [12 color], [12],
	   prod_color as [13 color], [13],
	   prod_color as [14 color], [14],
	   prod_color as [15 color], [15],
	   prod_color as [16 color], [16],
	   prod_color as [17 color], [17],
	   prod_color as [18 color], [18],
	   prod_color as [19 color], [19],
	   prod_color as [20 color], [20],
	   prod_color as [21 color], [21],
	   prod_color as [22 color], [22],
	   prod_color as [23 color], [23],
	   prod_color as [0 color], [0],
	   prod_color as [1 color], [1],
	   prod_color as [2 color], [2],
	   prod_color as [3 color], [3],
	   prod_color as [4 color], [4],
	   prod_color as [5 color], [5],
	   prod_color as [6 color], [6],
 ([7] + [8] +  [9]+[10]+ [11]+[12]+[13]+[14]+[15]+ [16]+ [17]+ [18]+ [19]+[20]+[21]+[22]+[23]+[0]+[1]+[2]+[3]+[4]+[5]+[6]) as [Total BD]
		FROM   
		  (SELECT DATEADD(day, -1,eod_datetime) as [Start], 
				--eod_datetime as [End], 
				DATEPART(HH, start_datetime)  as start_time, 
				SUM(volume) as volume, 
				CASE WHEN COUNT(DISTINCT product_key) = 1 THEN p.cell_color ELSE 'khaki' END as prod_color
		  FROM tmp_pipeline_by_hours tmp LEFT OUTER JOIN 
			   @products p ON p.prod_key = tmp.product_key
		  WHERE (@pipeline_key IS NULL OR pipeline_key = @pipeline_key) AND (product_key = @product_key OR @product_key IS NULL)
		  GROUP BY  start_datetime, eod_datetime, cell_color
		  ) as pivotData
		PIVOT (
		SUM(volume)
		FOR start_time  IN 
		([7] , [8] ,  [9],[10], [11],[12],[13],[14],[15], [16], [17], [18], [19],[20],[21],[22],[23],[0],[1],[2],[3],[4],[5],[6])
		) as pivotTable
		--ORDER BY [Start];
	


	select * from @records
	order by Start

END

GO 


EXEC mgn_pipeline_schedule_hourly_get
	@pipeline_key  = '8c87deaf-24fb-4b8f-bf28-8eff618532b6', 
	@month = 6,
	@year = 2020 , @product_key = null--'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0'


	GO