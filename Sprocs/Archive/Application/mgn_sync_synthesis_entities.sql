
CREATE OR ALTER PROCEDURE mgn_sync_synthesis_entities
AS
BEGIN

-- insert new entities 
INSERT INTO [dbo].[entities]
           ([_key]
           ,[name]
           ,[short_name]
           ,[type]
           ,[order_number]
           ,[external_id]
           ,[external_type]
           ,[external_code]
		   ,[active])
SELECT newid(), 
		LONG_NAME as name, 
		SHORT_NAME as short_name, 
		et.[converted_type_id] as new_type , 
		NULL,
		MASTER_BE_ID as external_id, 
		MASTER_ENTITY_TYPE_ID as external_type ,
		[MASTER_BE_CODE],
		[ACTIVE_IND] 
FROM EMERSON.Synthesis_Test_02242020.dbo.MASTER_BUSINESS_ENTITY be LEFT OUTER JOIN 
[dbo].[synthesis_entity_types] et on et.[syn_type_id] = be.MASTER_ENTITY_TYPE_ID
WHERE MASTER_BE_ID NOT IN (SELECT [external_id] FROM entities) AND [ACTIVE_IND] = 1
--MASTER_ENTITY_TYPE_ID = 64

UPDATE e
SET[active] =  be.[ACTIVE_IND],
   [external_code] =  be.[MASTER_BE_CODE],
   [name] = be.[LONG_NAME],
   [short_name] =  be.[SHORT_NAME]
FROM entities e INNER JOIN 
  EMERSON.Synthesis_Test_02242020.dbo.MASTER_BUSINESS_ENTITY be ON e.[external_id] = be.MASTER_BE_ID


END