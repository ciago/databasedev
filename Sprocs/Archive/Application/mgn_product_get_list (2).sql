USE [Moda]
GO
/****** Object: StoredProcedure [dbo].[mgn_product_get_list] Script Date: 2/16/2021 10:45:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[mgn_product_get_list]
@site_key uniqueidentifier = null,
@user varchar(50) = null
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

IF @site_key IS NULL
SET @site_key = dbo.get_default_site();
SELECT '00000000-0000-0000-0000-000000000000', 'Select Product'
UNION
SELECT _key, product_code
FROM products
WHERE site_key = @site_key
END
