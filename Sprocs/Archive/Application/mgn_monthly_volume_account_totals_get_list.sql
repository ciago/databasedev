USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_monthly_volume_account_totals_get_list]    Script Date: 2/11/2021 12:32:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Gets a list of nominations from a selected month and year for a variety of pipelines and entities.

/*EXEC mgn_monthly_volume_account_totals_get_list  @month = 12, @year = 2021 , @site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', @volume_type = 'f315101e-b284-447f-976f-f88f6d06df63'

  EXEC mgn_monthly_volume_account_totals_get_list  @month = 1, @year = 2021 , @site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', @volume_type = '00000000-0000-0000-0000-000000000000'
	*/
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_monthly_volume_account_totals_get_list]
	@month int, 
	@year int, 
	@site uniqueidentifier,
	@volume_type uniqueidentifier

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_nominations

-- check to see if we need to add any seed rows for this time period
DECLARE @month_end_datetime DATETIME
if (@month = 12)
begin
SET @month_end_datetime = DATETIMEFROMPARTS(@year, (1) , 1, 7 , 0, 0, 0)
end
else
begin
SET @month_end_datetime = DATETIMEFROMPARTS(@year, (@month + 1) , 1, 7 , 0, 0, 0)
end

IF @site IS NULL 
	SET @site = (SELECT dbo.get_default_site())

DECLARE @possible_records TABLE ([month] int, [year] int, volume_account_key uniqueidentifier)


INSERT INTO @possible_records ([month], [year], volume_account_key)
select @month as [month], 
	   @year as [year], 
	   va._key as volume_account_key
FROM volume_accounts as va
where DATEPART(year,va.start) <= @year and DATEPART(month,va.start) <= @month
	  and (va.stop is NULL or (DATEPART(year,va.stop) >= @year and DATEPART(month,va.stop) >= @month))


INSERT INTO monthly_volume_account_totals(_key, month, year, volume_account_key)
		SELECT newid(),pr.month, pr.year, pr.volume_account_key
		FROM @possible_records pr left JOIN 
			 monthly_volume_account_totals mvat ON pr.[month] = mvat.[month] AND pr.[year] = mvat.[year] AND pr.volume_account_key = mvat.volume_account_key left outer join
			 volume_accounts as va on pr.volume_account_key = va._key 
		WHERE mvat._key IS NULL
--**********************************************************************

UPDATE monthly_volume_account_totals
set monthly_account_total = isnull(dbo.get_vessel_by_type(va.entity_key,va.vessel_types_key,@month,@year),0)
from monthly_volume_account_totals mvat inner join
	 volume_accounts va on mvat.volume_account_key = va._key left outer join
	 accounting_volume_types avt on va.accounting_volume_types_key = avt._key
where avt._key = 'f315101e-b284-447f-976f-f88f6d06df63' --vessel_by_type key
	  and mvat.month = @month
	  and mvat.year = @year

UPDATE monthly_volume_account_totals
set monthly_account_total = isnull(dbo.get_throughput_inbound(va.entity_key,@month,@year),0)
from monthly_volume_account_totals mvat inner join
	 volume_accounts va on mvat.volume_account_key = va._key left outer join
	 accounting_volume_types avt on va.accounting_volume_types_key = avt._key
where avt._key = '2bb23464-5f62-4ad4-b57b-0718abb90cd1' --Throughput-inbound key
	  and mvat.month = @month
	  and mvat.year = @year

UPDATE monthly_volume_account_totals
set monthly_account_total = isnull(dbo.get_throughput_outbound(va.entity_key,@month,@year),0)
from monthly_volume_account_totals mvat inner join
	 volume_accounts va on mvat.volume_account_key = va._key left outer join
	 accounting_volume_types avt on va.accounting_volume_types_key = avt._key
where avt._key = '4275a108-aa8d-4ab7-924e-51f8ac48e3d3' --Throughput-outbound key
	  and mvat.month = @month
	  and mvat.year = @year

UPDATE monthly_volume_account_totals
set monthly_account_total = isnull(dbo.get_tank_capacity(va.entity_key,@month,@year),0)
from monthly_volume_account_totals mvat inner join
	 volume_accounts va on mvat.volume_account_key = va._key left outer join
	 accounting_volume_types avt on va.accounting_volume_types_key = avt._key
where avt._key = '6fdc5c66-6dcc-4aeb-b68c-f020e40da497' --TankCapacity key
	  and mvat.month = @month
	  and mvat.year = @year

	 

DECLARE @empty_guid UNIQUEIDENTIFIER = 0x0
if (@volume_type = @empty_guid)
	Begin
		SELECT        mvat._key,
					  mvat.month,
					  mvat.year, 
					  mvat.volume_account_key, 
					  va.accounts, 
					  va.unit_description,
					  va.accounting_volume_types_key,
					  avt.type_description,
					  ISNULL(va.entity_key, '00000000-0000-0000-0000-000000000000') as [entity_key],
					  ISNULL(ent.short_name, '') as [short_name], 
					  ISNULL(va.vessel_types_key, '00000000-0000-0000-0000-000000000000') as [vessel_types_key],
					  ISNULL(vt.vessel_type, '') as [vessel_type],
					  ISNULL(va.gl_account, '') as [gl_account],
					   va.[start],
					   va.[stop],
					   avt.auto_data,
					   mvat.monthly_account_total


		FROM          monthly_volume_account_totals AS mvat INNER JOIN
					  volume_accounts AS va on mvat.volume_account_key = va._key left join
					  accounting_volume_types avt ON va.accounting_volume_types_key = avt._key LEFT JOIN
					  entities ent ON va.entity_key = ent._key LEFT JOIN 
					  vessel_types vt ON va.vessel_types_key = vt._key 

		WHERE mvat.month = @month and
			  mvat.year = @year and
			  (va.site = @site) and 
			  va.active = 1

		Group by mvat._key,
				 mvat.month,
				 mvat.year, 
				 mvat.volume_account_key, 
				 va.accounts, 
				 va.unit_description,
				 va.accounting_volume_types_key,
				 avt.type_description,
				 [entity_key],
				 [short_name], 
				 [vessel_types_key],
				 [vessel_type],
				 [gl_account],
				 va.[start],
				 va.[stop],
				 avt.auto_data,
				 mvat.monthly_account_total
		order by va.accounting_volume_types_key
	END
else
	Begin
		SELECT        mvat._key,
					  mvat.month,
					  mvat.year, 
					  mvat.volume_account_key, 
					  va.accounts, 
					  va.unit_description,
					  va.accounting_volume_types_key,
					  avt.type_description,
					  ISNULL(va.entity_key, '00000000-0000-0000-0000-000000000000') as [entity_key],
					  ISNULL(ent.short_name, '') as [short_name], 
					  ISNULL(va.vessel_types_key, '00000000-0000-0000-0000-000000000000') as [vessel_types_key],
					  ISNULL(vt.vessel_type, '') as [vessel_type],
					  ISNULL(va.gl_account, '') as [gl_account],
					   va.[start],
					   va.[stop],
					   avt.auto_data,
					   mvat.monthly_account_total


		FROM          monthly_volume_account_totals AS mvat INNER JOIN
					  volume_accounts AS va on mvat.volume_account_key = va._key left join
					  accounting_volume_types avt ON va.accounting_volume_types_key = avt._key LEFT JOIN
					  entities ent ON va.entity_key = ent._key LEFT JOIN 
					  vessel_types vt ON va.vessel_types_key = vt._key 

		WHERE mvat.month = @month and
			  mvat.year = @year and
			  (va.site = @site) and 
			  va.active = 1 and
			  va.accounting_volume_types_key = @volume_type

		Group by mvat._key,
				 mvat.month,
				 mvat.year, 
				 mvat.volume_account_key, 
				 va.accounts, 
				 va.unit_description,
				 va.accounting_volume_types_key,
				 avt.type_description,
				 [entity_key],
				 [short_name], 
				 [vessel_types_key],
				 [vessel_type],
				 [gl_account],
				 va.[start],
				 va.[stop],
				 avt.auto_data,
				 mvat.monthly_account_total
		order by va.accounting_volume_types_key
	END

end