--USE [Moda_Matt]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tickets_get]    Script Date: 3/9/2021 3:45:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Candice Iago    
-- Create date: 5/15/2020
-- Description:    gets a list of vessel activity records for the selected date

 

/*EXEC mgn_location_types_get_row
    @row  = 1
    */
-- =============================================
Create or ALTER       PROCEDURE [dbo].[mgn_location_types_get_row]
    @row int = null
    --, @type (pipeline, vessel, tank to tank transfer
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row

 

SELECT lt._key,
	   lt.lt_location_type as [location_type],
	   isnull(lt.lt_e1_type,0) as [e1_type],
	   isnull(lt.lt_e2_type,0) as [e2_type],
	   isnull(lt.lt_e3_type,0) as [e3_type],
	   isnull(et1.type_description,'') as [e1_desc],
	   isnull(et2.type_description,'') as [e2_desc],
	   isnull(et3.type_description,'') as [e3_desc]
      
FROM location_types as lt left outer join
	 entity_types as et1 on et1.type_code = lt.lt_e1_type left outer join
	 entity_types as et2 on et2.type_code = lt.lt_e2_type left outer join
	 entity_types as et3 on et3.type_code = lt.lt_e3_type

WHERE lt.[lt_location_type] = @row
    

 

END
 