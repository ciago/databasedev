--USE [Hartree_TMS]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_contacts_update]    Script Date: 4/9/2021 2:20:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/24/2021
-- Description:		Update selected contacts record with new user input details.

/*EXEC mgn_contacts_update 
	@row = '8650AB82-B903-4C55-B507-CD94B1677987',
	@entity_key = '86C0CACB-B542-49C8-B352-8BE35F276541',
	@contact_type_key = '7CA005B6-E39E-4716-9812-C123E08B1993',
	@first_name = 'TestUpdateInsert',
	@last_name = 'TestUpdateInsert',
	@company_name = 'TestUpdateInsert',
	@address_1 = 'TestUpdateInsert',
	@address_2 = 'TestUpdateInsert',
	@city = 'TestUpdateInsert',
	@state_key = '4F7AE123-48B6-4F25-B823-4B6978D7138B',
	@country_key = '4D55BD06-1E5F-4EB7-8782-5E4965E045E9',
	@zip_code = '99999',
	@phone_1 = '123-456-1234',
	@phone_2 = '312-543-9786',
	@email_address = 'testinsert@outlook.com',
	@user = 'candice'

	// 
	*/
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_contacts_update]

	@row uniqueidentifier,
	@entity_key uniqueidentifier,
	@contact_type_key uniqueidentifier,
	@first_name nvarchar(50) = '',
	@last_name nvarchar(50) = '',
	@company_name nvarchar(100) = '',
	@address_1 nvarchar(100) = '',
	@address_2 nvarchar(100) = '',
	@city nvarchar(50) ='',
	@state_key uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@country_key uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@zip_code nvarchar(10) = '',
	@phone_1 nvarchar(20) = '',
	@phone_2 nvarchar(20) = '',
	@email_address nvarchar(100) = '',
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from contacts
--Where _key = @row

IF @row = '00000000-0000-0000-0000-000000000000'
BEGIN
	SET @row = newid()
END



IF(@row IN (SELECT _key from contacts))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM contacts WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE contacts

SET		entity_key = @entity_key,
		contact_type_key = @contact_type_key,
		first_name = @first_name,
		last_name = @last_name,
		company_name = @company_name,
		address_1 = @address_1,
		address_2 = @address_2,
		city = @city,
		state_key = @state_key,
		country_key = @country_key,
		zip_code = @zip_code,
		phone_1 = @phone_1,
		phone_2 = @phone_2,
		email_address = @email_address,
		record_info = @json

WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[contacts]
           ([_key]
		   ,[entity_key]
		   ,[contact_type_key]
		   ,[first_name]
		   ,[last_name]
		   ,[company_name]
		   ,[address_1]
		   ,[address_2]
		   ,[city]
		   ,[state_key]
		   ,[country_key]
		   ,[zip_code]
		   ,[phone_1]
		   ,[phone_2]
		   ,[email_address]
           ,[record_info])
     VALUES
           (@row
		   ,@entity_key
		   ,@contact_type_key
		   ,@first_name
		   ,@last_name
		   ,@company_name
		   ,@address_1
		   ,@address_2
		   ,@city
		   ,@state_key
		   ,@country_key
		   ,@zip_code
		   ,@phone_1
		   ,@phone_2
		   ,@email_address
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
