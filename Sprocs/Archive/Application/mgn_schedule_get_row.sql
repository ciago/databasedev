USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_schedule_get_row]    Script Date: 6/1/2020 2:29:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Obtains a single row of schedule data based on the user clicking 'edit' within the browser application.

/*EXEC mgn_schedule_get_row
	@row  = 'a8e70d71-1e94-4d0d-b086-f7de02cb34f9'
	*/
-- =============================================
CREATE OR ALTER           PROCEDURE [dbo].[mgn_schedule_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from schedule
--WHere _key = @row

Select		  i._key as _key,
			  i.start_datetime,
			  i.stop_datetime, 
			  CONVERT(int, Round(i.evt_volume,0)) as [evt_volume],
              CONVERT(int, Round(i.rate_bbls_hour,0)) as [rate_bbls_hour],
              pl.description as [pipeline_desc],             
              i.pipeline_key as pipeline_key,
              i.product_key as [product_key],
              p.product_name as [product_name],
              i.site_key as [site_key],
			  ISNULL(e.name,'') AS [entity_name],
			  ISNULL(e._key, '00000000-0000-0000-0000-000000000000') as [entity_key],
              s.short_name as [site_name]
	   
FROM          import_schedule AS i INNER JOIN
			  products AS p ON p._key = i.product_key INNER JOIN
			  sites AS s ON s._key = i.site_key INNER JOIN
              pipeline AS pl ON i.pipeline_key = pl._key LEFT OUTER JOIN
			  entities AS e ON i.shipper_key = e._key

WHere  @row = i._key
	
END