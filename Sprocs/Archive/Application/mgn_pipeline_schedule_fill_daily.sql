USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_schedule_fill_daily]    Script Date: 6/22/2020 6:16:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago
-- Create date: 2/26/2020
-- Description:	Fills the tmp_pipeline_by_hours table  with the given month and year

/*EXEC mgn_pipeline_schedule_fill_daily
	@month = '5',
	@year  = '2020'
	*/
-- =============================================
ALTER     PROCEDURE [dbo].[mgn_pipeline_schedule_fill_daily]
	@month int = 5,
    @year int = 2020
AS

delete from tmp_pipeline_by_day

--declare @AllHourlyVolume TABLE (start_datetime datetime, end_datetime datetime, datetime_slot datetime, rate float, volume float, pipeline_key uniqueidentifier, product_key uniqueidentifier, eod_datetime datetime)
DECLARE @month_start_datetime DATETIME = DATETIMEFROMPARTS(@year, @month, 1, 7 , 0, 0, 0)
DECLARE @month_end_datetime DATETIME = DATETIMEFROMPARTS(@year, (@month + 1) , 1, 7 , 0, 0, 0)
DECLARE @dates TABLE (start_datetime datetime, end_datetime datetime)


;
WITH Dates_CTE
     AS (SELECT @month_start_datetime AS Dates
         UNION ALL
         SELECT Dateadd(DD, 1, Dates)
         FROM   Dates_CTE
         WHERE  Dates < DATEADD(hh, -1, @month_end_datetime))
INSERT INTO @dates (start_datetime, end_datetime)
SELECT Dates as startdate, DATEADD(day, 1, Dates) as enddate
FROM   Dates_CTE 
OPTION (MAXRECURSION 0) 
--select * from @dates

INSERT INTO tmp_pipeline_by_day (start_datetime, end_datetime, pipeline_key, product_key)
select d.start_datetime, d.end_datetime, pa.pipeline_key, pa.product_key
FROM @dates d CROSS JOIN 
	 pipeline_product_assignment pa 
WHERE pa.start_datetime <= @month_start_datetime 
	  AND (pa.end_datetime >= @month_start_datetime OR pa.end_datetime IS NULL)


UPDATE d
SET rate = x.rate,
	volume = x.volume
FROM (SELECT  d.pipeline_key, d.product_key, d.start_datetime, d.end_datetime  , AVG(h.rate) as rate, SUM(h.volume) as volume
	  FROM tmp_pipeline_by_day d LEFT OUTER JOIN 
	  	 tmp_pipeline_by_hours h ON  h.eod_datetime = d.end_datetime AND h.pipeline_key = d.pipeline_key AND h.product_key = d.product_key
	  GROUP BY  d.pipeline_key, d.product_key, d.start_datetime, d.end_datetime  ) x INNER JOIN 
	  tmp_pipeline_by_day d ON d.start_datetime = x.start_datetime AND d.pipeline_key = x.pipeline_key AND d.product_key = x.product_key


UPDATE tmp_pipeline_by_day
SET rate = 0
WHERE rate IS NULL


UPDATE tmp_pipeline_by_day
SET volume = 0
WHERE volume IS NULL


UPDATE tmp_pipeline_by_day
SET volume = ROUND(volume, 0)