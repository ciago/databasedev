USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_tanks_update]    Script Date: 4/8/2021 2:45:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Update selected berth fees record with new user input details.


-- =============================================
ALTER            PROCEDURE [dbo].[mgn_tanks_update]

	@key uniqueidentifier,
    @tank_name nvarchar(100),
	@tank_description nvarchar(200),
	@tank_pi_name nvarchar(100) = null,
	@tank_external_id_inventory nvarchar(30) = null,
	@tank_external_id_location nvarchar(30) = null,
	@tank_min_operational_limit float = null,
	@tank_max_operational_limit float = null,
	@tank_lolo float = null,
	@tank_lo float = null,
	@tank_hi float = null,
	@tank_hihi float = null,
	@min_operating_temp float = null,
	@max_operating_temp float = null,
	@min_operating_press float = null,
	@max_operating_press float = null,
	@active bit = 1,
	@site uniqueidentifier,
	@user nvarchar(50) = null,
	@year_constructed int = null,
	@last_api float = null,
	@comments nvarchar(max) = null,
	@floating_roof bit = null,
	@blending bit = null,
	@heating bit = null,
	@capacity float = 0
	
	/*
	EXEC mgn_tanks_update
	@key = 'EA50538A-0730-47D1-A3B4-1606E9BBF0D6',
    @tank_name = 'test',
	@tank_description = 'test',
	@tank_pi_name = 'test',
	@tank_external_id_inventory = 'test',
	@tank_external_id_location = 'test',
	@tank_min_operational_limit = 123,
	@tank_max_operational_limit = 123,
	@tank_lolo = 10,
	@tank_lo = 10,
	@tank_hi = 10,
	@tank_hihi = 0,
	@min_operating_temp = 0,
	@max_operating_temp  = 0,
	@min_operating_press = 0,
	@max_operating_press = 0,
	@active = 1,
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@user = 'candice',
	@year_constructed = 10,
	@last_api = 10,
	@comments = 'test',
	@floating_roof = 1,
	@blending = 1,
	@heating = 1

	*/

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

IF(@key IN (SELECT _key from tanks))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM tanks WHERE _key = @key)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE tanks

SET		tank_name = @tank_name,
		tank_description = @tank_description,
		tank_pi_name = @tank_pi_name,
		tank_external_id_inventory = @tank_external_id_inventory,
		tank_external_id_location = @tank_external_id_location,
		tank_min_operational_limit = @tank_min_operational_limit,
		tank_max_operational_limit = @tank_max_operational_limit,
		tank_lolo = @tank_lolo,
		tank_lo = @tank_lo,
		tank_hi = @tank_hi,
		tank_hihi = @tank_hihi,
		min_operating_temp = @min_operating_temp,
		max_operating_temp = @max_operating_temp,
		min_operating_press = @min_operating_press,
		max_operating_press = @max_operating_press,
		active = @active,
		site_key = @site,
		tank_constructed = @year_constructed,
		tank_last_api = @last_api,
		tank_comments = @comments,
		tank_floating_roof = @floating_roof,
		tank_blending = @blending,
		tank_heating = @heating,
		tank_capacity = @capacity

WHERE _key = @key

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[tanks]
           ([_key]
           ,[tank_name]
           ,[tank_description]
           ,[tank_pi_name]
           ,[tank_external_id_inventory]
		   ,[tank_external_id_location]
		   ,[tank_min_operational_limit]
           ,[tank_max_operational_limit]
		   ,[tank_lolo]
		   ,[tank_lo]
		   ,[tank_hi]
		   ,[tank_hihi]
		   ,[min_operating_temp]
		   ,[max_operating_temp]
		   ,[min_operating_press]
		   ,[max_operating_press]
		   ,[active]
		   ,[site_key]
		   ,[tank_constructed]
		   ,[tank_last_api]
		   ,[tank_comments]
		   ,[tank_floating_roof]
		   ,[tank_blending]
		   ,[tank_heating]
		   ,[record_info]
		   ,tank_capacity)
     VALUES
           (@key
		   ,@tank_name
           ,@tank_description
           ,@tank_pi_name
           ,@tank_external_id_inventory
           ,@tank_external_id_location
		   ,@tank_min_operational_limit
		   ,@tank_max_operational_limit
		   ,@tank_lolo
		   ,@tank_lo
		   ,@tank_hi
		   ,@tank_hihi
		   ,@min_operating_temp
		   ,@max_operating_temp
		   ,@min_operating_press
		   ,@max_operating_press
		   ,@active
		   ,@site
		   ,@year_constructed
		   ,@last_api
		   ,@comments
		   ,@floating_roof
		   ,@blending
		   ,@heating
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   ,@capacity)

END

END
