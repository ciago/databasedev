USE [Moda]
GO
/****** Object:  Table [dbo].[berth_fees]    Script Date: 10/13/2020 9:44:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[berth_fees](
	[_key] [uniqueidentifier] NOT NULL,
	[site_key] [uniqueidentifier] NOT NULL,
	[start_loa] [int] NULL,
	[end_loa] [int] NULL,
	[loa_fee] [float] NOT NULL,
	[record_info] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[berth_fees] ([_key], [site_key], [start_loa], [end_loa], [loa_fee], [record_info]) VALUES (N'e0d30167-034c-497b-8656-48666e9d3feb', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 0, 199, 5.02, N'{ 
    "created_by":"", 
    "created_date":"", 
    "created_machine":"", 
    "modified_by":"candice", 
    "modified_date":"Oct 13 2020  9:34AM", 
    "modified_machine":"MANGANATL-L-055", 
    "deleted_by":"", 
    "deleted_date":"", 
    "deleted_machine":"", 
    "archived_by":"", 
    "archived_date":"", 
    "archived_machine":"", 
    "restored_by":"", 
    "restored_date":"", 
    "restored_machine":"" 
  }')
GO
INSERT [dbo].[berth_fees] ([_key], [site_key], [start_loa], [end_loa], [loa_fee], [record_info]) VALUES (N'8cd9c1e4-fc2b-41e3-b00c-ff7954903193', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 200, 399, 7.5, NULL)
GO
INSERT [dbo].[berth_fees] ([_key], [site_key], [start_loa], [end_loa], [loa_fee], [record_info]) VALUES (N'905b9098-9ba6-4d70-8e1e-d5f17529ad69', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 400, 499, 7.6, NULL)
GO
INSERT [dbo].[berth_fees] ([_key], [site_key], [start_loa], [end_loa], [loa_fee], [record_info]) VALUES (N'51f68da5-da5a-4e3f-846f-50eef83951c7', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 500, 599, 8.15, NULL)
GO
INSERT [dbo].[berth_fees] ([_key], [site_key], [start_loa], [end_loa], [loa_fee], [record_info]) VALUES (N'c0902b97-47de-4fcb-be44-7d8551c8849b', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 600, 699, 9.15, NULL)
GO
INSERT [dbo].[berth_fees] ([_key], [site_key], [start_loa], [end_loa], [loa_fee], [record_info]) VALUES (N'4fc5ad90-b7dc-4a60-bdf2-885544d33fda', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 700, 799, 11.85, NULL)
GO
INSERT [dbo].[berth_fees] ([_key], [site_key], [start_loa], [end_loa], [loa_fee], [record_info]) VALUES (N'5448b0ba-bf9a-4044-8d5b-992afcc617b8', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 800, 899, 13.32, NULL)
GO
INSERT [dbo].[berth_fees] ([_key], [site_key], [start_loa], [end_loa], [loa_fee], [record_info]) VALUES (N'22b58ac8-ffb0-450b-90c4-1eb7667a2781', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 900, 999, 15.71, NULL)
GO
INSERT [dbo].[berth_fees] ([_key], [site_key], [start_loa], [end_loa], [loa_fee], [record_info]) VALUES (N'cb47ec25-eed6-4cbe-9bbf-88e44ea4ca11', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 1000, 1099, 19.71, NULL)
GO
INSERT [dbo].[berth_fees] ([_key], [site_key], [start_loa], [end_loa], [loa_fee], [record_info]) VALUES (N'7a99c546-21ee-424a-acef-bc19c6009ddc', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 1100, 10000000, 21.71, NULL)
GO
