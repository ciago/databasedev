USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_tanks_get_row]    Script Date: 4/8/2021 2:39:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_tanks_get_row
	
	@rowkey = '3a305eae-a136-47a3-b663-9d9230d522cf'
	
	*/

ALTER              PROCEDURE [dbo].[mgn_tanks_get_row] 
    @rowkey uniqueidentifier = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

Declare @today datetime = getdate()
Declare @tank_tank_owner table ([tank_key] uniqueidentifier,[tank_owner_key] uniqueidentifier)
insert into @tank_tank_owner ([tank_key] ,[tank_owner_key] )
Select t._key as [tank_key],
	   isnull(town.tank_owner_key,'00000000-0000-0000-0000-000000000000') as[tank_owner_key]
from tanks as t left join
	 tank_owner as town on t._key = town.tank_key
where (town.tk_start_datetime <= @today or -- grabs the owner of the tank that became owner before today and will stop being owner after today
	  town.tk_start_datetime is null) and -- grabs _key from tanks that don't have an associated owner yet
	  (town.tk_end_datetime >= @today OR -- grabs the owner of the tank that became owner before today and will stop being owner after today
	  town.tk_end_datetime is null) 

Declare @tank_tank_product table ([tank_key] uniqueidentifier,[tank_product_key] uniqueidentifier)
insert into @tank_tank_product ([tank_key] ,[tank_product_key] )
Select t._key as [tank_key],
	   ISNULL(tp.product_key,'00000000-0000-0000-0000-000000000000') as [tank_product_key]
from tanks as t left join
	 tank_products as tp on t._key = tp.tank_key
where (tp.tk_start_datetime <= @today or -- grabs the owner of the tank that became owner before today and will stop being owner after today
	  tp.tk_start_datetime is null) and -- grabs _key from tanks that don't have an associated owner yet
	  (tp.tk_end_datetime >= @today OR -- grabs the owner of the tank that became owner before today and will stop being owner after today
	  tp.tk_end_datetime is null) 
	  
Select  t._key,
		t.tank_name,
		t.tank_description,
		ISNULL(t.tank_pi_name, '')as [tank_pi_name],
		ISNULL(t.tank_external_id_inventory, '') as tank_external_id_inventory,
		ISNULL(t.tank_external_id_location, '') as tank_external_id_location,
		ISNULL(t.tank_min_operational_limit,0) as tank_min_operational_limit,
		ISNULL(t.tank_max_operational_limit,0) as tank_max_operational_limit,
		ISNULL(t.tank_lolo, 0) as [tank_lolo],
		ISNULL(t.tank_lo, 0) as [tank_lo],
		ISNULL(t.tank_hihi, 0) as [tank_hihi],
		ISNULL(t.tank_hi, 0) as [tank_hi],	
		ISNULL(t.min_operating_temp, 0) as [min_operating_temp],
		ISNULL(t.max_operating_temp, 0) as [max_operating_temp],
		ISNULL(t.min_operating_press, 0) as [min_operating_press],
		ISNULL(t.max_operating_press, 0) as [max_operating_press],
		ISNULL(e._key, '00000000-0000-0000-0000-000000000000') as [owner_key],
		ISNULL(e.short_name, '')  as [owner_name],
		ISNULL(p._key, '00000000-0000-0000-0000-000000000000') as [product_key],
		ISNULL(p.product_name, '') as product_name,
		t.site_key,
		t.active,
		ISNULL(t.tank_constructed, 0)as [year_constructed],
		ISNULL(t.tank_last_api, 0)as [last_api],
		ISNULL(t.tank_comments, '')as [comments],
		ISNULL(t.tank_floating_roof, 0) as [tank_floating_roof],
		ISNULL(t.tank_blending,0) as [tank_blending],
		ISNULL(t.tank_heating,0) as [tank_heating],
		ISNULL(t.tank_capacity,0) as tank_capacity

from tanks as t left join
	 @tank_tank_owner as tto on t._key = tto.tank_key left join
	 entities as e on tto.tank_owner_key = e._key left join
	 @tank_tank_product as ttp on t._key = ttp.tank_key left join
	 products as p on ttp.tank_product_key = p._key
where @rowkey = t._key

END
