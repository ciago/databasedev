USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_tanks_get]    Script Date: 2/19/2021 4:42:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_tank_owners_get_row
	@rowKey = '650710de-2dfd-4b93-baec-e68b7162e7ee'
	
	*/

create or ALTER           PROCEDURE [dbo].[mgn_tank_owners_get_row] 
    @rowKey uniqueidentifier
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

Select  town._key,
		town.tank_key,
		t.tank_name,
		town.tank_owner_key,
		e.short_name as [owner_name],
		town.tk_start_datetime,
		town.tk_end_datetime,
		town.capacity_for_billing,
		town.external_row_id
from tank_owner as town left join
	 tanks as t on town.tank_key = t._key left join
	 entities as e on town.tank_owner_key = e._key
where town._key = @rowKey

END
