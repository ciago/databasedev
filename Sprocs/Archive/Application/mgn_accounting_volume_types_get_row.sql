USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_volume_types_get_row]    Script Date: 12/14/2020 2:56:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Obtains a single row of volume accounts data by searching for a specified guid.

/*EXEC mgn_accounting_volume_types_get_row
	@row  = '2bb23464-5f62-4ad4-b57b-0718abb90cd1'
	*/
-- =============================================
create or ALTER         PROCEDURE [dbo].[mgn_accounting_volume_types_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from 
--WHere _key = @row
   
SELECT avt._key,
	   avt.type_description as volume_type, 
	   avt.site_key,
	   sites.short_name
			  
FROM accounting_volume_types avt left join
	 sites ON avt.site_key = sites._key 

WHERE @row = avt._key
GROUP BY avt._key, avt.type_description, avt.site_key, sites.short_name
	  
END