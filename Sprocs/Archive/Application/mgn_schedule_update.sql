USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_schedule_update]    Script Date: 5/27/2020 12:34:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected schedule with new user input details.

/*EXEC mgn_schedule_update @row = 'a8e70d71-1e94-4d0d-b086-f7de02cb34f9',
						   @pipeline_key = 'fc536541-5a92-466a-bcd1-db7fedcd15e7',
						   @startDatetime = '2020-05-01T07:00:00.000',
						   @stopDatetime = '2020-05-02T04:27:00.000',
						   @evtVolume = 514942,
						   @rateBPH = 24000,
						   @site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
                           @batch_code = ' IN-WTI-0212',
                           @notes = null,
                           @product_key = '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',
						   @user = 'test'
	// 514942
	*/
-- =============================================
CREATE or ALTER       PROCEDURE [dbo].[mgn_schedule_update]
	@row uniqueidentifier,
	@startDatetime datetime,
	@stopDatetime datetime,
	@evtVolume float,
	@rateBPH float,
	@product_key uniqueidentifier,
	@shipper_key uniqueidentifier = null,
	@user varchar(50) = ''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from schedule
--Where _key = @row

--record info update 
DECLARE @json varchar(max)
SET @json = (SELECT record_info FROM import_schedule WHERE _key = @row)
SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())

UPDATE import_schedule

	SET start_datetime = @startDatetime,
		stop_datetime = @stopDatetime,
		evt_volume = @evtVolume,
		rate_bbls_hour= @rateBPH,
		product_key = @product_key,
		shipper_key = @shipper_key,
		record_info = @json

WHERE _key = @row

END