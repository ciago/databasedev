USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_nominations_update]    Script Date: 3/8/2021 12:02:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Update selected berth fees record with new user input details.


-- =============================================
Create or ALTER             PROCEDURE [dbo].[mgn_locations_update]

	@_key uniqueidentifier,
	@location_name nvarchar(50),
    @location_type int, 
	@e1_key uniqueidentifier = null,
	@e2_key uniqueidentifier = null,
	@e3_key uniqueidentifier = null,
	@external_id nvarchar(100) = null,
	@offload bit = 0,
	@load bit = 0,
	@site_key uniqueidentifier,
	@active bit = 1,
	@user nvarchar(50)
	
	
	/*EXEC [mgn_nominations_update] 
	@_key = '9f0b321f-72ba-40fc-8cbe-59b720e9dd5a',
	@vessel_name = 'oranges',
	@vessel_type_key = '43b2fb58-ddd3-429b-a396-a39d611fb7c6',
	@vessel_description = 'oranges',
	@vessel_external_id = 50,
	@vessel_loa = 3,
	@user = 'candice',
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@deleted = 0
	
	// 
	*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row



IF(@_key IN (SELECT ln._key from locations as ln))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT ln.ln_records_info FROM locations as ln WHERE _key = @_key)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE locations

SET		ln_location_name = @location_name,
		ln_location_type = @location_type,
		ln_e1_key = @e1_key,
		ln_e2_key = @e2_key,
		ln_e3_key = @e3_key,
		ln_external_id = @external_id,
		ln_offload = @offload,
		ln_load = @load,
		[ln_site_key] = @site_key,
		[ln_active] = @active,
		ln_records_info = @json

WHERE _key = @_key

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[locations]
           ([_key]
           ,[ln_location_name]
		   ,[ln_location_type]
		   ,[ln_e1_key]
		   ,[ln_e2_key]
		   ,[ln_e3_key]
		   ,[ln_external_id]
		   ,[ln_offload]
		   ,[ln_load]
		   ,[ln_site_key]
		   ,[ln_active]
		   ,[ln_records_info])

     VALUES
           (@_key
		   ,@location_name
		   ,@location_type
		   ,@e1_key
		   ,@e2_key
		   ,@e3_key
		   ,@external_id
		   ,@offload
		   ,@load
		   ,@site_key
		   ,@active
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE()))

END

END
