USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_actuals_update]    Script Date: 5/27/2020 12:34:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected pipeline_actuals with new user input details.

/*EXEC mgn_pipeline_actuals_update 
	@row = 'F77C4D35-9B71-48B0-924F-9621435339A2',
	@gsv_bbls = 189834.25,
	@open_totalizer = 6224389.75,
	@close_totalizer = 6414224,
	@pipeline_error = 0

	189834.25
	6224389.75
	6414224
	0

	// 
	*/
-- =============================================
CREATE or ALTER       PROCEDURE [dbo].[mgn_pipeline_actuals_update]

	@row uniqueidentifier,
    @site_key uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 
	@gsv_bbls float,
	@open_totalizer float,
	@close_totalizer float,
	@pipeline_error bit
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_actuals
--Where _key = @row


UPDATE pipeline_actuals

	SET gsv_bbls = @gsv_bbls,
		open_totalizer = @open_totalizer,
		close_totalizer = @close_totalizer,
		pipeline_error = @pipeline_error

WHERE _key = @row

END