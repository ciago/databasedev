USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_volume_accounts_get_list]    Script Date: 2/9/2021 12:01:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table

/*EXEC [mgn_volume_accounts_get_list]
	@volume_type  = 'f315101e-b284-447f-976f-f88f6d06df63',
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
  EXEC [mgn_volume_accounts_get_list]
	@volume_type  = '00000000-0000-0000-0000-000000000000'
	*/
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_volume_accounts_get_list]
	/*Defining the variable that I will eventually be filtering the table by*/
	@volume_type uniqueidentifier = null,
	@site uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--Where _key = @volume_type

--DECLARE @eod datetime = SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date), 07, 00)
DECLARE @empty_guid UNIQUEIDENTIFIER = 0x0
if (@volume_type = @empty_guid)
	Begin
		Select va._key, 
			   va.accounts, 
			   va.unit_description,
			   va.accounting_volume_types_key,
			   avt.type_description, 
			   ISNULL(va.entity_key, '00000000-0000-0000-0000-000000000000') as [entity_key],
			   ISNULL(ent.short_name, '') as [short_name], 
			   ISNULL(va.vessel_types_key, '00000000-0000-0000-0000-000000000000') as [vessel_types_key],
			   ISNULL(vt.vessel_type, '') as [vessel_type],
			   ISNULL(va.gl_account, '') as [gl_account],
			   va.start,
			   va.stop
		from volume_accounts as va INNER JOIN 
			 accounting_volume_types avt ON va.accounting_volume_types_key = avt._key LEFT JOIN 
			 entities ent ON va.entity_key = ent._key LEFT JOIN 
			 vessel_types vt ON va.vessel_types_key = vt._key 
		Where ((va.[site] = @site) and (va.active = 1))
		GROUP BY va._key, va.accounts, va.unit_description, va.accounting_volume_types_key, avt.type_description, va.entity_key, ent.short_name, va.vessel_types_key, vt.vessel_type, va.gl_account, va.start,va.stop
		ORDER BY va._key
	End
Else
	BEGIN
		Select va._key, 
			   va.accounts, 
			   va.unit_description,
			   va.accounting_volume_types_key,
			   avt.type_description, 
			   ISNULL(va.entity_key, '00000000-0000-0000-0000-000000000000') as [entity_key],
			   ISNULL(ent.short_name, '') as [short_name], 
			   ISNULL(va.vessel_types_key, '00000000-0000-0000-0000-000000000000') as [vessel_types_key],
			   ISNULL(vt.vessel_type, '') as [vessel_type],
			   ISNULL(va.gl_account, '') as [gl_account],
			   va.start,
			   va.stop
		from volume_accounts as va INNER JOIN 
			 accounting_volume_types avt ON va.accounting_volume_types_key = avt._key LEFT JOIN 
			 entities ent ON va.entity_key = ent._key LEFT JOIN 
			 vessel_types vt ON va.vessel_types_key = vt._key 
		WHERE ((va.accounting_volume_types_key = @volume_type) and (va.[site] = @site) and (va.active = 1))
		GROUP BY va._key, va.accounts, va.unit_description, va.accounting_volume_types_key, avt.type_description, va.entity_key, ent.short_name, va.vessel_types_key, vt.vessel_type, va.gl_account, va.start,va.stop
		ORDER BY va._key
	END
	
END
