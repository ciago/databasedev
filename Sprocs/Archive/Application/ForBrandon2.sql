USE [Moda]
GO

INSERT [dbo].[products] ([_key], [product_name], [product_code], [product_external_id], [site_key], [deleted], [record_info], [active]) VALUES (N'c30c4aea-d0cc-4c8a-86c0-45c681783cf1', N'Low Sulfur Fuel Oil', N'LSFO', N'LSFO', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 0, N'{ 
    "created_by":"", 
    "created_date":"", 
    "created_machine":"", 
    "modified_by":"", 
    "modified_date":"", 
    "modified_machine":"", 
    "deleted_by":"", 
    "deleted_date":"", 
    "deleted_machine":"", 
    "archived_by":"", 
    "archived_date":"", 
    "archived_machine":"", 
    "restored_by":"", 
    "restored_date":"", 
    "restored_machine":"" 
  }', 1)
GO
INSERT [dbo].[products] ([_key], [product_name], [product_code], [product_external_id], [site_key], [deleted], [record_info], [active]) VALUES (N'8e52211a-40d0-4722-b423-663781b8bd9f', N'Marine Gasoil', N'MGO', N'MGO', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 0, N'{ 
    "created_by":"", 
    "created_date":"", 
    "created_machine":"", 
    "modified_by":"", 
    "modified_date":"", 
    "modified_machine":"", 
    "deleted_by":"", 
    "deleted_date":"", 
    "deleted_machine":"", 
    "archived_by":"", 
    "archived_date":"", 
    "archived_machine":"", 
    "restored_by":"", 
    "restored_date":"", 
    "restored_machine":"" 
  }', 1)
GO
INSERT [dbo].[products] ([_key], [product_name], [product_code], [product_external_id], [site_key], [deleted], [record_info], [active]) VALUES (N'ec6741ed-4534-4260-9a42-6f168e2d4fbb', N'Eagleford Condensate', N'EFCN', N'EFCN', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 0, N'{ 
    "created_by":"", 
    "created_date":"", 
    "created_machine":"", 
    "modified_by":"", 
    "modified_date":"", 
    "modified_machine":"", 
    "deleted_by":"", 
    "deleted_date":"", 
    "deleted_machine":"", 
    "archived_by":"", 
    "archived_date":"", 
    "archived_machine":"", 
    "restored_by":"", 
    "restored_date":"", 
    "restored_machine":"" 
  }', 1)
GO
INSERT [dbo].[products] ([_key], [product_name], [product_code], [product_external_id], [site_key], [deleted], [record_info], [active]) VALUES (N'86e1baa4-142e-4d36-9d2b-7e9e3a3530d2', N'Eagleford Crude', N'EFCR', N'EFCR', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 0, N'{ 
    "created_by":"", 
    "created_date":"", 
    "created_machine":"", 
    "modified_by":"", 
    "modified_date":"", 
    "modified_machine":"", 
    "deleted_by":"", 
    "deleted_date":"", 
    "deleted_machine":"", 
    "archived_by":"", 
    "archived_date":"", 
    "archived_machine":"", 
    "restored_by":"", 
    "restored_date":"", 
    "restored_machine":"" 
  }', 1)
GO
INSERT [dbo].[products] ([_key], [product_name], [product_code], [product_external_id], [site_key], [deleted], [record_info], [active]) VALUES (N'f4d6b3e8-44fc-4329-ba9e-a5536b584f8f', N'High Sulfur Fuel Oil', N'HSFO', N'HSFO', N'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 0, N'{ 
    "created_by":"", 
    "created_date":"", 
    "created_machine":"", 
    "modified_by":"", 
    "modified_date":"", 
    "modified_machine":"", 
    "deleted_by":"", 
    "deleted_date":"", 
    "deleted_machine":"", 
    "archived_by":"", 
    "archived_date":"", 
    "archived_machine":"", 
    "restored_by":"", 
    "restored_date":"", 
    "restored_machine":"" 
  }', 1)
GO
