DECLARE @key uniqueidentifier = '0ce7711e-f267-4b6c-971d-5ca920da3fd5'
DECLARE @rate float = (SELECT rate_bbls_hour FROM pipeline_schedule WHERE _key = @key)
DECLARE @vol float = 514942


select start_datetime, --DATEADD(HOUR, 1, start_datetime),
	   DATETIMEFROMPARTS( DATEPART(YEAR, start_datetime), DATEPART(MONTH, start_datetime), DATEPART(DAY, start_datetime), DATEPART(HOUR, DATEADD(HOUR, 1, start_datetime)), 0, 0, 0),
	   stop_datetime, 
	   DATEDIFF(MINUTE, start_datetime, stop_datetime)
from pipeline_schedule

DECLARE @start_datetime DATETIME =  (select start_datetime from pipeline_schedule WHERE _key = @key);
DECLARE @end_datetime DATETIME =  (select stop_datetime from pipeline_schedule WHERE _key = @key);
--select DATETIMEFROMPARTS( DATEPART(YEAR, start_datetime), DATEPART(MONTH, start_datetime), DATEPART(YEAR, start_datetime), DATEPART(HOUR, DATEADD(HOUR, 1, start_datetime)), 0, 0, 0)

declare @HourlyVolume TABLE (start_datetime datetime, end_datetime datetime, datetime_slot datetime, rate float, volume float)
DECLARE @minDateTime AS DATETIME;
DECLARE @maxDateTime AS DATETIME;
DECLARE @totalvol float;

SET @minDateTime = (select DATETIMEFROMPARTS( DATEPART(YEAR, start_datetime), DATEPART(MONTH, start_datetime), DATEPART(DAY, start_datetime), DATEPART(HOUR, start_datetime), 0, 0, 0) from pipeline_schedule);
SET @maxDateTime = (select DATEADD(hour, -1, stop_datetime) from pipeline_schedule);

;
WITH Dates_CTE
     AS (SELECT @minDateTime AS Dates
         UNION ALL
         SELECT Dateadd(hh, 1, Dates)
         FROM   Dates_CTE
         WHERE  Dates < @maxDateTime)
INSERT INTO @HourlyVolume (datetime_slot)
SELECT *
FROM   Dates_CTE
OPTION (MAXRECURSION 0) 


UPDATE @HourlyVolume
SET start_datetime = @start_datetime
WHERE datetime_slot =  DATETIMEFROMPARTS( DATEPART(YEAR, @start_datetime), DATEPART(MONTH, @start_datetime), DATEPART(DAY, @start_datetime), DATEPART(HOUR, @start_datetime), 0, 0, 0)

UPDATE @HourlyVolume
SET end_datetime = @end_datetime
WHERE datetime_slot =  DATETIMEFROMPARTS( DATEPART(YEAR, @end_datetime), DATEPART(MONTH, @end_datetime), DATEPART(DAY, @end_datetime), DATEPART(HOUR, @end_datetime), 0, 0, 0)

UPDATE @HourlyVolume
SET start_datetime = datetime_slot
WHERE start_datetime IS NULL

UPDATE @HourlyVolume
SET end_datetime = DATEADD(HOUR, 1, datetime_slot)
WHERE end_datetime IS NULL


UPDATE @HourlyVolume
SET rate = @rate

UPDATE @HourlyVolume
SET volume = (rate/60) * DATEDIFF(MINUTE, start_datetime, end_datetime)

SET @totalvol = (SELECT SUM(volume) FROM @HourlyVolume)


print @totalvol


UPDATE @HourlyVolume
SET volume = @vol - @totalvol
WHERE datetime_slot =  DATETIMEFROMPARTS( DATEPART(YEAR, @end_datetime), DATEPART(MONTH, @end_datetime), DATEPART(DAY, @end_datetime), DATEPART(HOUR, @end_datetime), 0, 0, 0)


select *, DATEDIFF(MINUTE, start_datetime, end_datetime) as minutesdiff
from @HourlyVolume
