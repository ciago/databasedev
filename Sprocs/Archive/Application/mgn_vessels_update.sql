USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_vessels_update]    Script Date: 3/3/2021 5:00:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Update selected berth fees record with new user input details.

/*EXEC mgn_volume_accounts_update 
	@va_key = '9f0b321f-72ba-40fc-8cbe-59b720e9dd5a',
	@va_desc = 'oranges',
	@unit = '123',
	@avt_key = 'f315101e-b284-447f-976f-f88f6d06df63',
	@ent_key = '86c0cacb-b542-49c8-b352-8be35f276541',
	@vt_key = '4eec6d8d-8c3f-4c9f-896b-6ba19865131a',
	@gl_code = 123456,
	@user = 'candice',
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@start = '2/9/2021 12:00:00 AM',
	@stop = NULL
	
	// 
	*/
-- =============================================
ALTER           PROCEDURE [dbo].[mgn_vessels_update]

	@_key uniqueidentifier,
    @vessel_name nvarchar(100),
	@vessel_type_key uniqueidentifier,
	@vessel_description nvarchar(100) = '',
	@vessel_external_id bigint = 0,
	@vessel_loa float,
	@von bigint = null,
	@user nvarchar(50),
	@site uniqueidentifier,
	@active bit = 0
	
	/*EXEC mgn_volume_accounts_update 
	@_key = '9f0b321f-72ba-40fc-8cbe-59b720e9dd5a',
	@vessel_name = 'oranges',
	@vessel_type_key = '43b2fb58-ddd3-429b-a396-a39d611fb7c6',
	@vessel_description = 'oranges',
	@vessel_external_id = 50,
	@vessel_loa = 3,
	@user = 'candice',
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@deleted = 0
	
	// 
	*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

IF(@_key IN (SELECT v._key from vessels as v))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT v.record_info FROM vessels as v WHERE _key = @_key)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE vessels

SET		vessel_name = @vessel_name,
		vessel_type_key = @vessel_type_key,
		vessel_description = @vessel_description,
		vessel_external_id = @vessel_external_id,
		vessel_loa = @vessel_loa,
		von = @von,
		[site] = @site,
		active = @active,
		record_info = @json

WHERE _key = @_key

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[vessels]
           ([_key]
           ,[vessel_name]
           ,[vessel_type_key]
           ,[vessel_description]
           ,[vessel_external_id]
		   ,[vessel_loa]
		   ,[von]
		   ,[site]
           ,[active]
		   ,[record_info])
     VALUES
           (@_key
           ,@vessel_name
           ,@vessel_type_key
           ,@vessel_description
           ,@vessel_external_id
		   ,@vessel_loa
		   ,@von
		   ,@site
		   ,@active
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE()))

END

END
