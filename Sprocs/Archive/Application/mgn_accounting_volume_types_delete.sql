USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_volume_types_delete]    Script Date: 12/14/2020 4:14:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Deletes a single row of volume accounts data based on the selected row/record.

/*EXEC mgn_accounting_volume_types_delete
	@row  = 'fd323b20-3399-4241-9433-c83b06fe6cd2'
	*/
-- =============================================
create or ALTER             PROCEDURE [dbo].[mgn_accounting_volume_types_delete]
	@row uniqueidentifier = null,
	@user varchar(50) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from schedule
--WHere _key = @row
Delete From accounting_volume_types

Where _key = @row

END