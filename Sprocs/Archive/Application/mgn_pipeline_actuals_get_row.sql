USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_actuals_get_row]    Script Date: 6/4/2020 8:21:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Obtains a single row of pipeline actuals data based on the user clicking 'edit' within the browser application.

/*EXEC mgn_pipeline_actuals_get_row
	@row  = 'F77C4D35-9B71-48B0-924F-9621435339A2'
	*/
-- =============================================
CREATE or ALTER           PROCEDURE [dbo].[mgn_pipeline_actuals_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_actuals
--WHere _key = @row

Select		  pa._key as [_key],
			  pa.pipeline_key as [pipeline_key],
			  pl.description as [pipeline_desc],
              DATEADD(day, -1, pa.eod_datetime) as [date_datetime],
              ISNULL(pa.gsv_bbls,'') as [gsv_bbls],
			  ISNULL(pa.open_totalizer,'') as [open_totalizer],
			  ISNULL(pa.close_totalizer,'') as [close_totalizer],
			  pa.pipeline_error as [pipeline_error],
			  ISNULL(pa.pipeline_error_message,'') as [pipeline_error_message],
			  pa.site_key as [site_key],
              s.short_name as [site_name]

	   
FROM          pipeline_actuals AS pa INNER JOIN
			  sites AS s ON s._key = pa.site_key INNER JOIN
              pipeline AS pl ON pa.pipeline_key = pl._key 

WHERE  @row = pa._key
	
END