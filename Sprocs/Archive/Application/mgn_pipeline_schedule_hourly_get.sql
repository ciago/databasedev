USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_schedule_hourly_get]    Script Date: 5/1/2020 7:35:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/20
-- Description:	Gets all of the allocations for all days in a certain month for a certain pipeline

/*EXEC mgn_pipeline_schedule_hourly_get
	@pipeline_key  = 'fc536541-5a92-466a-bcd1-db7fedcd15e7', 
	@month = 6,
	@year = 2020 , @product_key = '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0'
	*/
-- =============================================
CREATE OR ALTER   PROCEDURE [dbo].[mgn_pipeline_schedule_hourly_get]
	@pipeline_key uniqueidentifier = null, 
	@month int, 
	@year int, 
	@product_key uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	-- fill the tmp table with all pipeline schedules by product by hour
EXEC mgn_pipeline_schedule_fill_hourly
	@month = @month,
	@year  = @year

--IF(@product_key IS NULL)
--BEGIN
--		SELECT *
--		FROM   
--		(SELECT DATEADD(day, -1,eod_datetime) as [Start], 
--				--eod_datetime as [End], 
--				DATEPART(HH, start_datetime)  as start_time, SUM(volume) as volume
--				FROM tmp_pipeline_by_hours
--				WHERE (@pipeline_key IS NULL OR pipeline_key = @pipeline_key)
--				GROUP BY  start_datetime, eod_datetime
--		) as pivotData
--		PIVOT (
--		SUM(volume)
--		FOR start_time  IN 
--		([7] , [8] ,  [9],[10], [11],[12],[13],[14],[15], [16], [17], [18], [19],[20],[21],[22],[23],[0],[1],[2],[3],[4],[5],[6])
--		) as pivotTable
--		ORDER BY [Start];
--END
--ELSE
--BEGIN
		SELECT *
		FROM   
		(SELECT DATEADD(day, -1,eod_datetime) as [Start], 
				--eod_datetime as [End], 
				DATEPART(HH, start_datetime)  as start_time, 
				SUM(volume) as volume
				FROM tmp_pipeline_by_hours
				WHERE (@pipeline_key IS NULL OR pipeline_key = @pipeline_key) AND (product_key = @product_key OR @product_key IS NULL)
				GROUP BY  start_datetime, eod_datetime
		) as pivotData
		PIVOT (
		SUM(volume)
		FOR start_time  IN 
		([7] , [8] ,  [9],[10], [11],[12],[13],[14],[15], [16], [17], [18], [19],[20],[21],[22],[23],[0],[1],[2],[3],[4],[5],[6])
		) as pivotTable
		ORDER BY [Start];
--END

END
