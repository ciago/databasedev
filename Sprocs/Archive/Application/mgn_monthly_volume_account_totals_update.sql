USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_volume_accounts_update]    Script Date: 2/10/2021 10:57:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Update selected berth fees record with new user input details.

/*EXEC mgn_monthly_volume_accounts_update 
	@row_key = '5eec1137-bcd5-4d85-8361-78551a0dc29a',
	@mvat_total = 1000,
	@user = 'Candice'
	
	// 
	*/
-- =============================================
create or ALTER         PROCEDURE [dbo].[mgn_monthly_volume_accounts_update]

	@row_key uniqueidentifier,
    @mvat_total float,
	@user nvarchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row


	--record info update 
DECLARE @json varchar(max)
SET @json = (SELECT record_info FROM volume_accounts WHERE _key = @row_key)
SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE monthly_volume_account_totals

SET		[monthly_account_total] = @mvat_total,
		record_info = @json

WHERE _key = @row_key

END
