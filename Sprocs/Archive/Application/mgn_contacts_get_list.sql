USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_contacts_get_list]    Script Date: 4/9/2021 2:24:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/24/2021
-- Description:		Gets a list of contacts from a selected entity type, entity, and active status.

/*EXEC mgn_contacts_get_list  
	@type = 0,
	@entity_key = '00000000-0000-0000-0000-000000000000',
	@contact_type_key = '00000000-0000-0000-0000-000000000000'
	
    */	
-- =============================================
ALTER        PROCEDURE [dbo].[mgn_contacts_get_list]

	@type int = 0,
	@entity_key uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@contact_type_key uniqueidentifier = '00000000-0000-0000-0000-000000000000'
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from contacts

SELECT			c._key,
				ISNULL(c.first_name,'') + ' ' + ISNULL(c.last_name,'')  as [contact_name],
				ISNULL(e.name,'') as [entity_name],
				ISNULL(c.entity_key, '00000000-0000-0000-0000-000000000000') as [entity_key],
				ISNULL(et.type_description,'') as [entity_type_desc],
				e.type as [type],
				ISNULL(ct.contact_type_description,'') as [contact_type_desc],
				ISNULL(c.contact_type_key, '00000000-0000-0000-0000-000000000000') as [contact_type_key],
				ISNULL(c.first_name,'') as [first_name],
				ISNULL(c.last_name,'') as [last_name],
				ISNULL(c.company_name,'') as [company_name],
				ISNULL(c.address_1,'') as [address_1],
				ISNULL(c.address_2,'') as [address_2],
				ISNULL(c.city,'') as [city],
				ISNULL(s.state,'') as [state],
				ISNULL(c.state_key, '00000000-0000-0000-0000-000000000000') as [state_key],
				ISNULL(c.zip_code,'') as [zip_code],
				ISNULL(c.phone_1,'') as [phone_1],
				ISNULL(c.phone_2,'') as [phone_2],
				ISNULL(c.email_address,'') as [email_address]	

FROM			contacts AS c INNER JOIN
				entities AS e ON e._key = c.entity_key LEFT OUTER JOIN
				entity_types AS et ON et.type_code = e.type LEFT OUTER JOIN
				contact_types AS ct ON ct._key = c.contact_type_key LEFT OUTER JOIN
				states AS s ON s._key = c.state_key

WHERE			(e.type = @type OR @type = 0)
				AND (c.entity_key = @entity_key OR @entity_key = '00000000-0000-0000-0000-000000000000')
				AND	(c.contact_type_key = @contact_type_key OR @contact_type_key = '00000000-0000-0000-0000-000000000000')

ORDER BY		name
	  
END
