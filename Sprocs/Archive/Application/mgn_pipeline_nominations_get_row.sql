USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_nominations_get_row]    Script Date: 5/21/2020 12:02:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Obtains a single row of nomination data based on where the user clicked 'edit' within the Moda browser application.

/*EXEC mgn_pipeline_nominations_get_row
	@row  = '30772599-501e-46ba-a7fd-456a5a89ca7d'
	*/
-- =============================================
ALTER     PROCEDURE [dbo].[mgn_pipeline_nominations_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_nominations
--WHere _key = @row

Select n._key as row_key,
	   n.month as [month],
	   n.year as [year],
	   pl.description as [pipeline_desc],
	   entities.name AS entity_name,
	   n.bbls_per_day as [bbls_per_day],
	   n.bbls_per_month as [bbls_per_month],
	   n.pipeline_key as pipeline_key,
	   n.entity_key as [entity_key],
	   n.product_key as [product_key],
	   p.product_name as [product_name],
	   n.site_key as [site_key],
	   s.short_name as [site_name],
	   n.record_info as [record_info]
	   
FROM          nominations AS n INNER JOIN
			  products AS p ON p._key = n.product_key INNER JOIN
			  sites AS s ON s._key = n.site_key INNER JOIN
              pipeline AS pl ON n.pipeline_key = pl._key INNER JOIN
              entities ON n.entity_key = entities._key

WHere  @row = n._key
	
END