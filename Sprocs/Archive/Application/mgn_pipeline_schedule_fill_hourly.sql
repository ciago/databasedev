
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_schedule_fill_hourly]    Script Date: 5/1/2020 7:34:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago
-- Create date: 2/26/2020
-- Description:	Fills the tmp_pipeline_by_hours table  with the given month and year

/*EXEC mgn_pipeline_schedule_fill_hourly
	@month = '6',
	@year  = '2020'
	*/
-- =============================================
CREATE OR ALTER   PROCEDURE [dbo].[mgn_pipeline_schedule_fill_hourly]
	@month int = 6,
    @year int = 2020
AS


declare @AllHourlyVolume TABLE (start_datetime datetime, end_datetime datetime, datetime_slot datetime, rate float, volume float, pipeline_key uniqueidentifier, product_key uniqueidentifier, eod_datetime datetime)
DECLARE @month_start_datetime DATETIME = DATETIMEFROMPARTS(@year, @month, 1, 7 , 0, 0, 0)
DECLARE @month_end_datetime DATETIME = DATETIMEFROMPARTS(@year, (@month + 1) , 1, 7 , 0, 0, 0)
DECLARE @hours TABLE (start_datetime datetime, end_datetime datetime)
delete from tmp_pipeline_by_hours
-- fill @hours table with an hour for every day for every pipeline for every product
;
WITH Dates_CTE
     AS (SELECT @month_start_datetime AS Dates
         UNION ALL
         SELECT Dateadd(HH, 1, Dates)
         FROM   Dates_CTE
         WHERE  Dates <  (DATEADD(HOUR, -1, @month_end_datetime)))
INSERT INTO @hours (start_datetime, end_datetime)
SELECT Dates as startdate, DATEADD(HH, 1, Dates) as enddate
FROM   Dates_CTE 
OPTION (MAXRECURSION 0) 

INSERT INTO tmp_pipeline_by_hours(start_datetime , end_datetime ,   pipeline_key , product_key)
SELECT  d.start_datetime, d.end_datetime, pa.pipeline_key, pa.product_key
FROM   @hours d CROSS JOIN 
	 pipeline_product_assignment pa 
WHERE pa.start_datetime <= @month_start_datetime 
	  AND (pa.end_datetime >= @month_start_datetime OR pa.end_datetime IS NULL)

-- Cursor to run through every schedule during the month 
--************************************************** Start Cursor*****************************************************
-- declare cursor vars
DECLARE @key uniqueidentifier ,
		@rate float ,
		@vol float, 
		@pipe uniqueidentifier, 
		@prod uniqueidentifier, 
		@start datetime, 
		@stop datetime 
 
DECLARE schedule_cursor CURSOR
FOR SELECT _key, rate_bbls_hour, evt_volume, pipeline_key, product_key, start_datetime, stop_datetime
    FROM import_schedule
	WHERE start_datetime >= @month_start_datetime AND stop_datetime <= @month_end_datetime;
 
OPEN schedule_cursor;
 
FETCH NEXT FROM schedule_cursor INTO 
   @key ,
	@rate , 
	@vol , 
	@pipe, 
	@prod, 
	@start, 
	@stop
 
WHILE @@FETCH_STATUS = 0
    BEGIN


--DECLARE @start_datetime DATETIME =  (select start_datetime from import_schedule WHERE _key = @key);
--DECLARE @end_datetime DATETIME =  (select stop_datetime from import_schedule WHERE _key = @key);
--select DATETIMEFROMPARTS( DATEPART(YEAR, start_datetime), DATEPART(MONTH, start_datetime), DATEPART(YEAR, start_datetime), DATEPART(HOUR, DATEADD(HOUR, 1, start_datetime)), 0, 0, 0)

declare @HourlyVolume TABLE (start_datetime datetime, end_datetime datetime, datetime_slot datetime, rate float, volume float, pipeline_key uniqueidentifier, product_key uniqueidentifier)
DECLARE @minDateTime AS DATETIME;
DECLARE @maxDateTime AS DATETIME;
DECLARE @totalvol float;


delete from @HourlyVolume
SET @minDateTime = (select DATETIMEFROMPARTS( DATEPART(YEAR, @start), DATEPART(MONTH, @start), DATEPART(DAY, @start), DATEPART(HOUR, @start), 0, 0, 0));
SET @maxDateTime = (select DATEADD(hour, -1, @stop));

;
WITH Dates_CTE
     AS (SELECT @minDateTime AS Dates
         UNION ALL
         SELECT Dateadd(hh, 1, Dates)
         FROM   Dates_CTE
         WHERE  Dates < @maxDateTime)
INSERT INTO @HourlyVolume (datetime_slot)
SELECT *
FROM   Dates_CTE
OPTION (MAXRECURSION 0) 


UPDATE @HourlyVolume
SET start_datetime = @start
WHERE datetime_slot =  DATETIMEFROMPARTS( DATEPART(YEAR, @start), DATEPART(MONTH, @start), DATEPART(DAY, @start), DATEPART(HOUR, @start), 0, 0, 0)

UPDATE @HourlyVolume
SET end_datetime = @stop
WHERE datetime_slot =  DATETIMEFROMPARTS( DATEPART(YEAR, @stop), DATEPART(MONTH, @stop), DATEPART(DAY, @stop), DATEPART(HOUR, @stop), 0, 0, 0)

UPDATE @HourlyVolume
SET start_datetime = datetime_slot
WHERE start_datetime IS NULL

UPDATE @HourlyVolume
SET end_datetime = DATEADD(HOUR, 1, datetime_slot)
WHERE end_datetime IS NULL


UPDATE @HourlyVolume
SET rate = @rate

UPDATE @HourlyVolume
SET volume = (rate/60) * DATEDIFF(MINUTE, start_datetime, end_datetime)

SET @totalvol = (SELECT SUM(volume) FROM @HourlyVolume)

UPDATE @HourlyVolume
SET pipeline_key = @pipe

UPDATE @HourlyVolume
SET product_key = @prod

--print @totalvol


UPDATE @HourlyVolume
SET volume =  @vol - ( @totalvol - volume)
WHERE datetime_slot =  DATETIMEFROMPARTS( DATEPART(YEAR, @stop), DATEPART(MONTH, @stop), DATEPART(DAY, @stop), DATEPART(HOUR, @stop), 0, 0, 0)


DECLARE @lastVolumeRecord int = (Select volume FROM @HourlyVolume WHERE datetime_slot =  DATETIMEFROMPARTS( DATEPART(YEAR, @stop), DATEPART(MONTH, @stop), DATEPART(DAY, @stop), DATEPART(HOUR, @stop), 0, 0, 0))
-- if the last record is negative - subtract it from the previus 
if (@lastVolumeRecord < 0 )
BEGIN

UPDATE @HourlyVolume
SET volume = volume + @lastVolumeRecord
WHERE datetime_slot =  DATETIMEFROMPARTS( DATEPART(YEAR, @stop), DATEPART(MONTH, @stop), DATEPART(DAY, @stop), DATEPART(HOUR, @stop) - 1, 0, 0, 0)


UPDATE @HourlyVolume
SET volume = 0
WHERE datetime_slot =  DATETIMEFROMPARTS( DATEPART(YEAR, @stop), DATEPART(MONTH, @stop), DATEPART(DAY, @stop), DATEPART(HOUR, @stop), 0, 0, 0)



	
END 


INSERT INTO @AllHourlyVolume(start_datetime , end_datetime , datetime_slot , rate , volume, pipeline_key, product_key) 
select *--, DATEDIFF(MINUTE, start_datetime, end_datetime) as minutesdiff
from @HourlyVolume

--select * from @HourlyVolume


        FETCH NEXT FROM schedule_cursor INTO 
   @key ,
	@rate , 
	@vol  , 
	@pipe, 
	@prod, 
	@start, 
	@stop;
    END;
 
CLOSE schedule_cursor;
 
DEALLOCATE schedule_cursor;

--**************************************************END Cursor*****************************************************

-- update the eod date 
UPDATE @AllHourlyVolume
SET eod_datetime = CASE WHEN cast( start_datetime as time) >= cast('07:00 AM' as TIME) 
	        THEN DATEADD(day, 1, SMALLDATETIMEFROMPARTS(YEAR(start_datetime), MONTH(start_datetime), DAY(start_datetime), 07, 00)) 
			ELSE SMALLDATETIMEFROMPARTS(YEAR(start_datetime), MONTH(start_datetime), DAY(start_datetime), 07, 00) END


--select start_datetime, end_datetime, datetime_slot, AVG(rate) as rate ,SUM(volume) as volume,  pipeline_key, product_key, eod_datetime
--		from @AllHourlyVolume
--		GROUP BY start_datetime, end_datetime, datetime_slot, pipeline_key, product_key, eod_datetime

UPDATE hd
SET rate = ISNULL(x.rate,0),
	volume = ISNULL(x.volume ,0)
--SELECT hd.start_datetime, hd.end_datetime, ISNULL(x.rate,0) as rate, ISNULL(x.volume ,0) as volume , hd.pipeline_key, hd.product_key
		FROM tmp_pipeline_by_hours hd LEFT OUTER JOIN 
		(select start_datetime, end_datetime, datetime_slot, AVG(rate) as rate ,SUM(volume) as volume,  pipeline_key, product_key, eod_datetime
		from @AllHourlyVolume
		GROUP BY start_datetime, end_datetime, datetime_slot, pipeline_key, product_key, eod_datetime) x 
		ON hd.start_datetime = x.datetime_slot AND hd.pipeline_key = x.pipeline_key AND hd.product_key = x.product_key;

UPDATE tmp_pipeline_by_hours
SET eod_datetime = CASE WHEN cast( start_datetime as time) >= cast('07:00 AM' as TIME) 
	        THEN DATEADD(day, 1, SMALLDATETIMEFROMPARTS(YEAR(start_datetime), MONTH(start_datetime), DAY(start_datetime), 07, 00)) 
			ELSE SMALLDATETIMEFROMPARTS(YEAR(start_datetime), MONTH(start_datetime), DAY(start_datetime), 07, 00) END
