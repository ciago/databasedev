USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_product_get_drop_down]    Script Date: 4/8/2021 9:04:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>


/*EXEC mgn_product_get_drop_down

	*/
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_product_get_drop_down] 
    @site_key uniqueidentifier = null, 
	@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();
	SELECT '00000000-0000-0000-0000-000000000000', ' Select Product'
	UNION
    SELECT _key, product_code
    FROM products
	WHERE site_key = @site_key
    
END
