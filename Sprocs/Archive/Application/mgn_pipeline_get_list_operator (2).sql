USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_get_list]    Script Date: 1/11/2021 9:28:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create or ALTER   PROCEDURE [dbo].[mgn_pipeline_get_list_operator] 
	--@terminal uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT _key, description
	FROM pipeline
	WHERE sort_order is NOT NULL
	ORDER BY sort_order

END
