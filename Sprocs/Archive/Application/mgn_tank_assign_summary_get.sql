declare @eod datetime = '6/18/2020 7:00'
		,@entity_key uniqueidentifier 


DECLARE @results TABLE (eod_datetime datetime,
					    tank_name nvarchar(100), 
				        tank_key uniqueidentifier, 
						tank_product nvarchar(100),
						tank_open float, 
						miop float, 
						cactus2 float, 
						gray_oak float, 
						epic float, 
						total_receipts float, 
						transfers float, 
						berth2 float, 
						berth4 float,
						berth5 float,
						berth_unknown float,
						total_shipments float, 
						tank_close float, 
						entity_short_name nvarchar(20), 
						entity_key uniqueidentifier)


DECLARE @miop uniqueidentifier = 'fc536541-5a92-466a-bcd1-db7fedcd15e7'
	    ,@cactus2 uniqueidentifier = '8c87deaf-24fb-4b8f-bf28-8eff618532b6'
		,@gray_oak uniqueidentifier = '263486ba-8550-49e2-81d2-055bbb61b0ae'
		,@epic uniqueidentifier = 'edf94ab4-9845-4c15-8873-096d6afd92a3'
		,@berth2  uniqueidentifier = '67502856-5746-45b9-96c7-dab439768624'
		,@berth4  uniqueidentifier = '0d791513-7146-481d-80fe-e0642c06d744'
		,@berth5 uniqueidentifier = '75796520-53bc-459a-a1f1-ed8e8623f0bf'
		,@unassigned_berth uniqueidentifier = 0x0


insert into @results (eod_datetime, tank_name, tank_key, tank_product, entity_short_name, entity_key, miop, cactus2, gray_oak, epic, total_receipts, transfers, 
					  berth2, berth4, berth5, berth_unknown, total_shipments, tank_close)
SELECT @eod, t.tank_description, t._key, p.product_code, e.short_name, e._key, 0,0,0,0,0,0,0,0,0,0,0,0
FROM tanks t INNER JOIN 
	 tank_owner tow ON t._key = tow.tank_key INNER JOIN 
	 entities e ON tow.tank_owner_key  = e._key LEFT OUTER JOIN 
	 products p ON p._key = t.tank_product
WHERE tow.tk_start_datetime < DATEADD(DAY, -1, @eod) AND (tow.tk_end_datetime > @eod OR tow.tk_end_datetime IS NULL)
	  AND (@entity_key IS NULL OR tow.tank_owner_key = @entity_key)

UPDATE r
SET miop = miop.volume
FROM @results r INNER JOIN 
(SELECT SUM(ISNULL(tk_final_gsv, tk_scheduled_volume)) as volume, tk_to_key
FROM tickets 
WHERE tk_eod_datetime = @eod
	  AND tk_from_key = @miop
GROUP BY tk_from_key, tk_to_key) miop ON r.tank_key = miop.tk_to_key


UPDATE r
SET cactus2 = cactus2.volume
FROM @results r INNER JOIN 
(SELECT SUM(ISNULL(tk_final_gsv, tk_scheduled_volume)) as volume, tk_to_key
FROM tickets 
WHERE tk_eod_datetime = @eod
	  AND tk_from_key = @cactus2
GROUP BY tk_from_key, tk_to_key) cactus2 ON r.tank_key = cactus2.tk_to_key



UPDATE r
SET gray_oak = gray_oak.volume
FROM @results r INNER JOIN 
(SELECT SUM(ISNULL(tk_final_gsv, tk_scheduled_volume)) as volume, tk_to_key
FROM tickets 
WHERE tk_eod_datetime = @eod
	  AND tk_from_key = @gray_oak
GROUP BY tk_from_key, tk_to_key) gray_oak ON r.tank_key = gray_oak.tk_to_key



UPDATE r
SET epic = epic.volume
FROM @results r INNER JOIN 
(SELECT SUM(ISNULL(tk_final_gsv, tk_scheduled_volume)) as volume, tk_to_key
FROM tickets 
WHERE tk_eod_datetime = @eod
	  AND tk_from_key = @epic
GROUP BY tk_from_key, tk_to_key) epic ON r.tank_key = epic.tk_to_key


UPDATE r
SET berth2 = berth2.volume
FROM @results r INNER JOIN 
(SELECT SUM(ISNULL(tk_final_gsv, tk_scheduled_volume)) as volume, tk_from_key
FROM tickets 
WHERE tk_eod_datetime = @eod
	  AND tk_to_key = @berth2
GROUP BY tk_from_key, tk_to_key) berth2 ON r.tank_key = berth2.tk_from_key


UPDATE r
SET berth4 = berth4.volume
FROM @results r INNER JOIN 
(SELECT SUM(ISNULL(tk_final_gsv, tk_scheduled_volume)) as volume, tk_from_key
FROM tickets 
WHERE tk_eod_datetime = @eod
	  AND tk_to_key = @berth4
GROUP BY tk_from_key, tk_to_key) berth4 ON r.tank_key = berth4.tk_from_key


UPDATE r
SET berth5 = berth5.volume
FROM @results r INNER JOIN 
(SELECT SUM(ISNULL(tk_final_gsv, tk_scheduled_volume)) as volume, tk_from_key
FROM tickets 
WHERE tk_eod_datetime = @eod
	  AND tk_to_key = @berth5
GROUP BY tk_from_key, tk_to_key) berth5 ON r.tank_key = berth5.tk_from_key



declare @last_tank_close datetime 

select TOP(1) @last_tank_close = MAX(eod_datetime) 
from tank_actuals 
WHERE close_gsv IS NOT NULL
GROUP BY eod_datetime
ORDER BY eod_datetime DESC

--select tank_description, 
--	   ISNULL(close_gsv,0) as open_amt, 
--	   ISNULL(receipts.rec_volume,0), 
--	   ISNULL(shipments.ship_volume,0), 
--	   ISNULL(close_gsv,0) + ISNULL(rec_volume,0) - ISNULL(ship_volume,0) as close_amt
UPDATE r
	SET tank_open = ISNULL(close_gsv,0) + ISNULL(rec_volume,0) - ISNULL(ship_volume,0)
from @results r INNER JOIN 
(select close_gsv, tank_key, tank_description
from tank_actuals INNER JOIN 
	 tanks ON tanks._key = tank_actuals.tank_key
WHERE eod_datetime = @last_tank_close) begin_tank ON r.tank_key = begin_tank.tank_key LEFT OUTER JOIN 
-- all receipts since last tank 
(SELECT ISNULL(SUM(ISNULL(tk_final_gsv, tk_scheduled_volume)),0) as rec_volume, tk_to_key
FROM tickets 
WHERE tk_type = 1 
AND tk_eod_datetime > @last_tank_close AND tk_eod_datetime < @eod
GROUP BY tk_to_key) receipts ON begin_tank.tank_key = receipts.tk_to_key LEFT OUTER JOIN 

-- all shipments since last tank 
(SELECT SUM(ISNULL(tk_final_gsv, tk_scheduled_volume)) as ship_volume, tk_from_key
FROM tickets 
WHERE tk_type = 2 
AND tk_eod_datetime > @last_tank_close AND tk_eod_datetime < @eod
GROUP BY tk_from_key)shipments ON shipments.tk_from_key = begin_tank.tank_key

UPDATE @results
SET total_receipts = miop + cactus2 + gray_oak + epic

UPDATE @results
SET total_shipments = berth2 + berth4 + berth5 + berth_unknown

UPDATE @results
SET tank_close = tank_open + total_receipts + transfers - total_shipments


insert into @results (eod_datetime, tank_name, tank_key, tank_product, entity_short_name, entity_key, tank_open,
					  miop, cactus2, gray_oak, epic, total_receipts, transfers, 
					  berth2, berth4, berth5, berth_unknown, total_shipments, tank_close)
SELECT @eod, ' Total', CAST(0x0 as uniqueidentifier), 'ALL', 'ALL', CAST(0x0 as uniqueidentifier) ,
	   SUM(tank_open), SUM(miop), SUM(cactus2), SUM(gray_oak), SUM(epic), SUM(total_receipts), SUM(transfers), 
					  SUM(berth2), SUM(berth4), SUM(berth5), SUM(berth_unknown), SUM(total_shipments), SUM(tank_close)
FROM @results
GROUP BY eod_datetime
	
----update r
----SET  r.total_scheduled = ROUND(d.volume,0)
--		-- return data 
--SELECT d.[date] as [Date], 
--		ROUND(d.volume,0) as [Scheduled Volume],
--		ROUND(ISNULL(total_allocated, 0),0) as [Total Allocated],  
--		ROUND(d.volume - total_allocated,0) as Diff
		
--FROM (
--	SELECT CONVERT(Date, start_datetime) as [date], ISNULL(AVG(NULLIF(rate,0)),0) as rate, SUM(volume) as volume,  end_datetime
--	FROM tmp_pipeline_by_day d 
--	WHERE  (pipeline_key = @miop )
--	GROUP BY start_datetime,  end_datetime)d INNER JOIN 
--	@results r ON d.end_datetime = r.eod_datetime 





select * from @results ORDER BY tank_name



DECLARE @month int  = DATEPART(month, @eod), 
		@year int  = DATEPART(YEAR, @eod)


EXEC mgn_pipeline_schedule_fill_daily
	@month = @month,
	@year  = @year



---- product miop, cactus 2, gray oak, epic, total rec, transfers, berth 2 , berth 4, berth 5, unassigned, total shipments 
--select tmp.volume as volume, p.description as pipeline, pr.product_code as product_code
--FROM tmp_pipeline_by_day tmp INNER JOIN 
--	 pipeline p on tmp.pipeline_key = p._key INNER JOIN 
--	 products pr ON  tmp.product_key = pr._key
--WHERE tmp.end_datetime = @eod
DECLARE @Totals TABLE (product nvarchar(100), miop float, epic float, gray float, cactus float, eod datetime)

INSERT INTO @Totals(product, miop , epic , gray , cactus)
SELECT * 
FROM 
(
select ISNULL(tmp.volume,0) as volume, p.description as pipeline, pr.product_code as product_code
FROM tmp_pipeline_by_day tmp INNER JOIN 
	 pipeline p on tmp.pipeline_key = p._key INNER JOIN 
	 products pr ON  tmp.product_key = pr._key
WHERE tmp.end_datetime = @eod
) as pivotData
PIVOT (
SUM(volume)
FOR pipeline IN ([MIOP 20" Pipeline], [EPIC 20" Pipeline ], [Gray Oak 30" Pipeline], [Cactus II 26" Pipeline])
) as pivotTable
ORDER BY product_code

UPDATE @Totals
SET miop = ISNULL(miop, 0),
	epic = ISNULL(epic, 0),
	gray = ISNULL(gray, 0),
	cactus = ISNULL(cactus, 0), 
	eod = @eod

INSERT INTO @Totals(product, miop , epic , gray , cactus, eod)
SELECT ' Total Scheduled', SUM(miop) , SUM(epic) , SUM(gray) , SUM(cactus), @eod
FROm @Totals
GROUP BY eod


INSERT INTO @Totals(product, miop , epic , gray , cactus, eod)
SELECT ' Total Assigned', miop , epic , gray_oak , cactus2, @eod
FROm @results
WHERE tank_name = ' Total'


select * from @Totals
ORDER BY product desc


