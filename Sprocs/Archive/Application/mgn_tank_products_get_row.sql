USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_owners_get_row]    Script Date: 2/19/2021 5:32:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_tank_products_get_row
	@rowKey = '9104424c-fd7a-440c-b8ee-aede2fe89aea'
	
	*/

Create or ALTER             PROCEDURE [dbo].[mgn_tank_products_get_row] 
    @rowKey uniqueidentifier
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

Select  tp._key,
		tp.tank_key,
		t.tank_name,
		tp.product_key,
		p.product_name as [product_name],
		tp.tk_start_datetime,
		tp.tk_end_datetime,
		tp.external_row_id
from tank_products as tp left join
	 tanks as t on tp.tank_key = t._key left join
	 products as p on tp.product_key = p._key
where tp._key = @rowKey

END
