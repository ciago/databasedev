USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_vessel_types_update]    Script Date: 12/14/2020 3:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Update selected berth fees record with new user input details.

/*EXEC mgn_vessel_types_update 
	@vt_key = '64b80ca9-adba-492f-8692-876c343c9c57',
	@vt_desc = 'Suez',
	@vt_vol = 1234,
	@site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@active = 1,
	@user = 'candice'
	
	// 
	*/
-- =============================================
create or ALTER         PROCEDURE [dbo].mgn_vessel_types_update

	@vt_key uniqueidentifier,
    @vt_desc varchar(50),
	@vt_vol int,
	@site_key uniqueidentifier,
	@active bit,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

IF(@vt_key IN (SELECT _key from vessel_types))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM vessel_types WHERE _key = @vt_key)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE vessel_types

SET		vessel_type = @vt_desc,
		vessel_volume = @vt_vol,
		site_key = @site_key,
		active = @active,
		record_info = @json

WHERE _key = @vt_key

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].vessel_types
           ([_key]
           ,vessel_type
           ,vessel_volume
		   ,site_key
		   ,active
           ,[record_info])
     VALUES
           (@vt_key
           ,@vt_desc
		   ,@vt_vol
           ,@site_key
		   ,@active
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
