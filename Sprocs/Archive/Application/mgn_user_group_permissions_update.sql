--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_user_group_permissions_update]    Script Date: 1/14/2021 3:29:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 4/29/21
-- Description:	

/*

EXEC mgn_user_group_permissions_update  @row  = '207D4768-2D6E-4BC8-AEF3-B5C9D906601D'

*/
-- =============================================
Create or ALTER       PROCEDURE [dbo].mgn_user_group_permissions_update
	@row uniqueidentifier = NULL,
	@view bit, 
	@update bit, 
	@delete bit, 
	@special bit
AS
BEGIN

SET NOCOUNT ON;

UPDATE user_groups_permissions
SET gp_view_permission = @view,
	gp_update_permission = @update,
	gp_delete_inactivate_permisssion = @delete,
	gp_special_permission = @special
WHERE _key = @row

-- UPDATE Parent view status based on the view of all children 
declare  @group_key uniqueidentifier = (SELECT gp_user_group_key FROM user_groups_permissions WHERE _key = @row)
DECLARE @menu_key uniqueidentifier = (SELECT gp_menu_item_key FROM user_groups_permissions WHERE _key = @row)

--select * from menu_items
declare @parent1 int = (SELECT menu_parent_number_1 FROM menu_items WHERE _key = @menu_key)
       ,@parent2 int = (SELECT menu_parent_number_1 FROM menu_items WHERE _key = @menu_key)
       ,@parent3 int = (SELECT menu_parent_number_1 FROM menu_items WHERE _key = @menu_key)
       ,@parent4 int = (SELECT menu_parent_number_1 FROM menu_items WHERE _key = @menu_key)

-- if a menu item that is a child , has a parent1234 that is @parent1, and a view = 1 , then parent1 view = 1,
-- if a menu item that is a child , has a parent1234 that is @parent1, and all view = 0 , then parent1 view = 0,

--SELECT * 
--		  FROM menu_items mi INNER JOIN 
--		  	 user_groups_permissions ugp  ON ugp.gp_menu_item_key = mi._key 
--		  WHERE menu_is_child = 1 
--		  	  AND (menu_parent_number_1 = @parent1
--		  		   OR menu_parent_number_2 = @parent1
--		  		   OR menu_parent_number_3 = @parent1
--		  		   OR menu_parent_number_4 = @parent1)
--		  	  AND ugp.gp_view_permission = 1
--			  AND ugp.gp_user_group_key = @group_key


--if a child who is visible exists, then make sure parent 1 view = true

--***************************Parent 1 

IF EXISTS(SELECT * 
		  FROM menu_items mi INNER JOIN 
		  	 user_groups_permissions ugp  ON ugp.gp_menu_item_key = mi._key 
		  WHERE menu_is_child = 1 
		  	  AND (menu_parent_number_1 = @parent1
		  		   OR menu_parent_number_2 = @parent1
		  		   OR menu_parent_number_3 = @parent1
		  		   OR menu_parent_number_4 = @parent1)
		  	  AND ugp.gp_view_permission = 1
			  AND ugp.gp_user_group_key = @group_key)
BEGIN
	UPDATE ugp
	SET gp_view_permission = 1 
	FROM user_groups_permissions ugp INNER JOIN 
	 menu_items mi ON ugp.gp_menu_item_key = mi._key 
	WHERE mi.menu_active = 1 
	  AND ugp.gp_view_permission = 0
	  AND mi.menu_number = @parent1
	  AND ugp.gp_user_group_key = @group_key

END
ELSE --else make sure parent view = false
BEGIN
	UPDATE ugp
	SET gp_view_permission = 0
	FROM user_groups_permissions ugp INNER JOIN 
	 menu_items mi ON ugp.gp_menu_item_key = mi._key 
	WHERE mi.menu_active = 1 
	  AND ugp.gp_view_permission = 1
	  AND mi.menu_number = @parent1
	  AND ugp.gp_user_group_key = @group_key
END

--******************Parent 2

--if a child who is visible exists, then make sure parent 1 view = true
IF EXISTS(SELECT * 
		  FROM menu_items mi INNER JOIN 
		  	 user_groups_permissions ugp  ON ugp.gp_menu_item_key = mi._key 
		  WHERE menu_is_child = 1 
		  	  AND (menu_parent_number_1 = @parent2
		  		   OR menu_parent_number_2 = @parent2
		  		   OR menu_parent_number_3 = @parent2
		  		   OR menu_parent_number_4 = @parent2)
		  	  AND ugp.gp_view_permission = 1
			  AND ugp.gp_user_group_key = @group_key)
BEGIN
	UPDATE ugp
	SET gp_view_permission = 1 
	FROM user_groups_permissions ugp INNER JOIN 
	 menu_items mi ON ugp.gp_menu_item_key = mi._key 
	WHERE mi.menu_active = 1 
	  AND ugp.gp_view_permission = 0
	  AND mi.menu_number = @parent2
	  AND ugp.gp_user_group_key = @group_key

END
ELSE --else make sure parent view = false
BEGIN
	UPDATE ugp
	SET gp_view_permission = 0
	FROM user_groups_permissions ugp INNER JOIN 
	 menu_items mi ON ugp.gp_menu_item_key = mi._key 
	WHERE mi.menu_active = 1 
	  AND ugp.gp_view_permission = 1
	  AND mi.menu_number = @parent2
	  AND ugp.gp_user_group_key = @group_key
END

--**************************parent 3 

--if a child who is visible exists, then make sure parent 1 view = true
IF EXISTS(SELECT * 
		  FROM menu_items mi INNER JOIN 
		  	 user_groups_permissions ugp  ON ugp.gp_menu_item_key = mi._key 
		  WHERE menu_is_child = 1 
		  	  AND (menu_parent_number_1 = @parent3
		  		   OR menu_parent_number_2 = @parent3
		  		   OR menu_parent_number_3 = @parent3
		  		   OR menu_parent_number_4 = @parent3)
		  	  AND ugp.gp_view_permission = 1
			  AND ugp.gp_user_group_key = @group_key)
BEGIN
	UPDATE ugp
	SET gp_view_permission = 1 
	FROM user_groups_permissions ugp INNER JOIN 
	 menu_items mi ON ugp.gp_menu_item_key = mi._key 
	WHERE mi.menu_active = 1 
	  AND ugp.gp_view_permission = 0
	  AND mi.menu_number = @parent3
	  AND ugp.gp_user_group_key = @group_key

END
ELSE --else make sure parent view = false
BEGIN
	UPDATE ugp
	SET gp_view_permission = 0
	FROM user_groups_permissions ugp INNER JOIN 
	 menu_items mi ON ugp.gp_menu_item_key = mi._key 
	WHERE mi.menu_active = 1 
	  AND ugp.gp_view_permission = 1
	  AND mi.menu_number = @parent3
	  AND ugp.gp_user_group_key = @group_key
END

--*****************************parent 4 

--if a child who is visible exists, then make sure parent 1 view = true
IF EXISTS(SELECT * 
		  FROM menu_items mi INNER JOIN 
		  	 user_groups_permissions ugp  ON ugp.gp_menu_item_key = mi._key 
		  WHERE menu_is_child = 1 
		  	  AND (menu_parent_number_1 = @parent4
		  		   OR menu_parent_number_2 = @parent4
		  		   OR menu_parent_number_3 = @parent4
		  		   OR menu_parent_number_4 = @parent4)
		  	  AND ugp.gp_view_permission = 1
			  AND ugp.gp_user_group_key = @group_key)
BEGIN
	UPDATE ugp
	SET gp_view_permission = 1 
	FROM user_groups_permissions ugp INNER JOIN 
	 menu_items mi ON ugp.gp_menu_item_key = mi._key 
	WHERE mi.menu_active = 1 
	  AND ugp.gp_view_permission = 0
	  AND mi.menu_number = @parent4
	  AND ugp.gp_user_group_key = @group_key

END
ELSE --else make sure parent view = false
BEGIN
	UPDATE ugp
	SET gp_view_permission = 0
	FROM user_groups_permissions ugp INNER JOIN 
	 menu_items mi ON ugp.gp_menu_item_key = mi._key 
	WHERE mi.menu_active = 1 
	  AND ugp.gp_view_permission = 1
	  AND mi.menu_number = @parent4
	  AND ugp.gp_user_group_key = @group_key
END

END
