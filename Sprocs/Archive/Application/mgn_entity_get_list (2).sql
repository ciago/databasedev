USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_entity_get_list]    Script Date: 2/18/2021 6:23:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER    PROCEDURE [dbo].[mgn_entity_get_list] 
	@type int = null

	/*EXEC mgn_entity_get_list
	@type  = 1

	*/
	--@terminal uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT '00000000-0000-0000-0000-000000000000' as _key, 'Select entity' as name
	UNION
    SELECT _key, name
	FROM entities
	WHERE type = @type or @type is null
	ORDER BY name
	
END
