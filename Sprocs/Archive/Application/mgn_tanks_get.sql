USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_tanks_get]    Script Date: 2/19/2021 10:24:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_tanks_get
	@site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@active = 0
	@product = '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',
	@owner = '86c0cacb-b542-49c8-b352-8be35f276541',
	
	*/

ALTER           PROCEDURE [dbo].[mgn_tanks_get] 
    @site_key uniqueidentifier = null, 
	@user varchar(50) = null,
	@owner uniqueidentifier = null,
	@product uniqueidentifier = null,
	@active bit = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	Begin
		SET @site_key = dbo.get_default_site();
	End

Declare @today datetime = getdate()
Declare @tank_tank_owner table ([tank_key] uniqueidentifier,[tank_owner_key] uniqueidentifier)
insert into @tank_tank_owner ([tank_key] ,[tank_owner_key] )
Select t._key as [tank_key],
	   isnull(town.tank_owner_key,'00000000-0000-0000-0000-000000000000') as[tank_owner_key]
from tanks as t left join
	 tank_owner as town on t._key = town.tank_key
where (town.tk_start_datetime <= @today or -- grabs the owner of the tank that became owner before today and will stop being owner after today
	  town.tk_start_datetime is null) and -- grabs _key from tanks that don't have an associated owner yet
	  (town.tk_end_datetime >= @today OR -- grabs the owner of the tank that became owner before today and will stop being owner after today
	  town.tk_end_datetime is null) and -- if the end date is null, that owner has the tank indefinitely
	  town.site_key = @site_key

Declare @tank_tank_product table ([tank_key] uniqueidentifier,[tank_product_key] uniqueidentifier)
insert into @tank_tank_product ([tank_key] ,[tank_product_key] )
Select t._key as [tank_key],
	   isnull(tp.product_key,'00000000-0000-0000-0000-000000000000') as [tank_product_key]
from tanks as t left join
	 tank_products as tp on t._key = tp.tank_key
where (tp.tk_start_datetime <= @today or -- grabs the owner of the tank that became owner before today and will stop being owner after today
	  tp.tk_start_datetime is null) and -- grabs _key from tanks that don't have an associated owner yet
	  (tp.tk_end_datetime >= @today OR -- grabs the owner of the tank that became owner before today and will stop being owner after today
	  tp.tk_end_datetime is null) and -- if the end date is null, that owner has the tank indefinitely
	  tp.site_key = @site_key

Select  t._key,
		t.tank_name,
		t.tank_description,
		e._key [owner_key],
		e.short_name as [owner_name],
		p._key [product_key],
		p.product_name,
		t.tank_pi_name,
		t.tank_external_id_inventory,
		t.tank_external_id_location,
		t.tank_min_operational_limit,
		t.tank_max_operational_limit,
		t.tank_lolo,
		t.tank_lo,
		t.tank_hi,
		t.tank_hihi,
		t.min_operating_temp,
		t.max_operating_temp,
		t.min_operating_press,
		t.max_operating_press,
		t.site_key,
		t.active,
		isnull(t.tank_constructed,1950) as [year_constructed],
		isnull(t.tank_last_api,0) as [last_api],
		isnull(t.tank_comments,'') as [comments],
		t.tank_floating_roof,
		t.tank_blending,
		t.tank_heating
from tanks as t left join
	 @tank_tank_owner as tto on t._key = tto.tank_key left join
	 entities as e on tto.tank_owner_key = e._key left join
	 @tank_tank_product as ttp on t._key = ttp.tank_key left join
	 products as p on ttp.tank_product_key = p._key
where t.site_key = @site_key
	  and (e._key = @owner or @owner is null)
	  and (p._key = @product or @product is null)
	  and (t.active = @active or @active is null)

END
