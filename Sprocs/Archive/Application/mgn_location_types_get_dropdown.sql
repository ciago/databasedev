USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_volume_types_get_dropdown]    Script Date: 1/11/2021 4:18:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
create or ALTER           PROCEDURE [dbo].[mgn_location_types_get_dropdown] 
	@site_key uniqueidentifier = null, 
	@user varchar(50) = 'default'

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


SELECT 0 AS RowKey, 'Select Type' As description
UNION
SELECT lt_location_type as RowKey, [lt_type_desc] as description
FROM [dbo].[location_types]
WHERE active = 1
ORDER BY RowKey

END


-- EXEC [mgn_accounting_volume_types_get_list]





