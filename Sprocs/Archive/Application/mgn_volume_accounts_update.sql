USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_volume_accounts_update]    Script Date: 2/9/2021 12:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Update selected berth fees record with new user input details.

/*EXEC mgn_volume_accounts_update 
	@va_key = '9f0b221f-72ba-40fc-8cbe-59b720e9dd5a',
	@va_desc = 'oranges',
	@unit = '123',
	@avt_key = 'f315101e-b284-447f-976f-f88f6d06df63',
	@ent_key = '86c0cacb-b542-49c8-b352-8be35f276541',
	@vt_key = '4eec6d8d-8c3f-4c9f-896b-6ba19865131a',
	@gl_code = 123456,
	@user = 'candice',
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
	
	// 
	*/
-- =============================================
ALTER         PROCEDURE [dbo].[mgn_volume_accounts_update]

	@va_key uniqueidentifier,
    @va_desc varchar(50),
	@unit varchar(50),
	@avt_key uniqueidentifier,
	@ent_key uniqueidentifier,
	@vt_key uniqueidentifier,
	@gl_code varchar(50),
	@user varchar(50),
	@site uniqueidentifier,
	@active bit = 1,
	@start datetime,
	@stop datetime
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

IF(@va_key IN (SELECT _key from volume_accounts))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM volume_accounts WHERE _key = @va_key)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE volume_accounts

SET		accounts = @va_desc,
		unit_description = @unit,
		accounting_volume_types_key = @avt_key,
		entity_key = @ent_key,
		vessel_types_key = @vt_key,
		gl_account = @gl_code,
		record_info = @json,
		[site] = @site,
		[active] = @active,
		[start] = @start,
		[stop] = @stop

WHERE _key = @va_key

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[volume_accounts]
           ([_key]
           ,[accounts]
           ,[unit_description]
           ,[accounting_volume_types_key]
           ,[entity_key]
		   ,[vessel_types_key]
		   ,[gl_account]
           ,[record_info]
		   ,[site]
		   ,[active]
		   ,[start]
		   ,[stop])
     VALUES
           (@va_key
           ,@va_desc
           ,@unit
           ,@avt_key
           ,@ent_key
		   ,@vt_key
		   ,@gl_code
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   ,@site
		   ,@active
		   ,@start
		   ,@stop)

END

END
