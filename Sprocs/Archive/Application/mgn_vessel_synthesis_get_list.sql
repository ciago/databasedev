USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_vessel_synthesis_get_list]    Script Date: 2/15/2021 10:20:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 5/15/2020
-- Description:	gets a list of vessel activity records for the selected date

/*EXEC mgn_vessel_synthesis_get_list
	@date  = '6/8/2020'
	*/
-- =============================================
ALTER     PROCEDURE [dbo].[mgn_vessel_synthesis_get_list]
	@date datetime = null, 
	@site uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row

DECLARE @eod datetime =  DATEADD(DAY,1,SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date)  , 07, 00))

IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END

IF(@date IS NULL)
BEGIN
	SET @date = DATEADD(DAY, -30, getdate())
END

Select syn_customer_ref,  syn_vessel_lay_status, syn_vessel_eta, _key
from synthesis_tickets
WHERE syn_vessel_lay_status <> 'REJECTED' and syn_customer_ref IS NOT NULL
	 AND syn_vessel_eta > DATEADD(DAY, -2, @date)
	 ORDER BY syn_vessel_eta

	
END
