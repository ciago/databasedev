USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_volume_types_get_list]    Script Date: 12/14/2020 2:31:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
create or ALTER         PROCEDURE [dbo].[mgn_accounting_volume_types_get_dropdown] 
	@site_key uniqueidentifier = null, 
	@user varchar(50) = 'default'

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


SELECT '00000000-0000-0000-0000-000000000000' AS RowKey, 'Select Type' As description
UNION
SELECT _key as RowKey, [type_description] as description
FROM [dbo].[accounting_volume_types]
--WHERE active = 1
ORDER BY RowKey

END


-- EXEC [mgn_accounting_volume_types_get_list]





