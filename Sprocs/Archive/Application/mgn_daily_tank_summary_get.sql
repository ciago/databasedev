--USE [Hartree_TMS]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_daily_tank_summary_get]    Script Date: 4/13/2021 9:19:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER           PROCEDURE [dbo].[mgn_daily_tank_summary_get]
 @eod datetime  , 
 @site uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
 
	
AS	

/* 
declare @date datetime = DATETIMEFROMPARTS(2021, 01, 2, 7, 0, 0, 0)
EXEC mgn_daily_tank_summary_get  @eod = @date , @site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
								
	
*/
BEGIN



declare @total_by_entity bit = (SELECT balance_sheet_total_by_entity FROM site_configuration WHERE site_key = @site)
declare @total_by_product bit = (SELECT balance_sheet_total_by_product FROM site_configuration WHERE site_key = @site)

DECLARE @TankInv TABLE (tank varchar(30), beginning int, shipments int, receipts int, transfers int, 
					    book_close int, physical_close int, gain_loss int, shipper uniqueidentifier, row_key uniqueidentifier,
						product_name nvarchar(100), tank_api float, product_key uniqueidentifier) 


SET @eod  = DATEADD(DAY, 1, SMALLDATETIMEFROMPARTS(YEAR(@eod), MONTH(@eod), (DAY(@eod)), 07, 00))

INSERT INTO @TankInv  (tank , beginning , shipments , receipts , transfers ,
book_close , physical_close , gain_loss, shipper , row_key, product_name, tank_api, product_key) 

SELECT t.tank_pi_name as Tank, 
	   CONVERT(int,ROUND(ISNULL(ta.open_gsv,0),0)) as Beginning , 
	   CONVERT(int, (ROUND(ISNULL(shipments.Bbls,0),0)*-1)) as Shipments, 
	   CONVERT(int,ROUND(ISNULL(receipts.bbls,0),0)) as Receipts, 
	   CONVERT(int,ROUND(ISNULL(transfers.bbls,0),0)) as Transfers,
	   CONVERT(int,ROUND(ISNULL(ta.open_gsv,0) + (ISNULL(shipments.Bbls,0)*-1) + ISNULL(receipts.bbls,0) + ISNULL(transfers.bbls,0),0))  as [Book_Close], 
	   CONVERT(int,ROUND(ISNULL(ta.close_gsv,0),0)) as [Physical_Close], 
	   CONVERT(int,ROUND(ISNULL(ta.close_gsv,0)-(ISNULL(ta.open_gsv,0) + (ISNULL(shipments.Bbls,0)*-1) + ISNULL(receipts.bbls,0) + ISNULL(transfers.bbls,0)),0)) as [Gain_Loss],
	   town.tank_owner_key,
	   ta._key as row_key,
	   ISNULL(p.product_name, 'No Product Assigned') as [tank_product],
	   ISNULL(ta.tank_api, 0) as [tank_api],
	   p._key
FROM  tank_actuals ta INNER JOIN 
	  tanks t ON ta.tank_key = t._key LEFT OUTER JOIN
	  (SELECT *    ---tp TABLE start
	   FROM tank_products 
	   WHERE (tk_start_datetime <= @eod)
			  AND (tk_end_datetime >= @eod or tk_end_datetime IS null)) AS tp ON ta.tank_key = tp.tank_key LEFT OUTER JOIN
	  products AS p ON tp.product_key = p._key LEFT OUTER JOIN 
	  (SELECT *   --town table start
	   FROM tank_owner 
	   WHERE tk_start_datetime < @eod 
		     AND (tk_end_datetime > @eod OR tk_end_datetime IS NULL)) town ON t._key = town.tank_key LEFT OUTER JOIN
	  (SELECT t.tank_name,  tt.tk_eod_datetime as eod_datetime, SUM(tt.tk_final_gsv) as bbls, tt.tk_source_tank_key as tank_key  --shipments table start
		FROM tickets tt INNER JOIN 
			 tanks t ON tt.tk_source_tank_key = t._key INNER JOIN 
			 ticket_type ty ON tt.tk_type = ty.typ_type
		WHERE tt.tk_eod_datetime = @eod  AND ty.type_is_shipment = 1 --tk_type = 2
		GROUP BY t.tank_name,  tt.tk_eod_datetime,  tt.tk_source_tank_key) shipments ON ta.tank_key = shipments.tank_key AND ta.eod_datetime = shipments.eod_datetime LEFT OUTER JOIN 
	  (SELECT t.tank_name, tt.tk_eod_datetime as eod_datetime, SUM(tt.tk_final_gsv) as bbls, tt.tk_destination_tank_key as tank_key  -- receipts table start 
	  	FROM tickets tt INNER JOIN 
	  		 tanks t ON t._key = tt.tk_destination_tank_key LEFT OUTER JOIN 
			 ticket_type ty ON tt.tk_type = ty.typ_type
	  	WHERE tk_eod_datetime = @eod  AND ty.type_is_receipt = 1
	  	GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_destination_tank_key) receipts ON receipts.tank_key = ta.tank_key AND receipts.eod_datetime = ta.eod_datetime LEFT OUTER JOIN 
	  (SELECT tank_name , tk_eod_datetime, SUM(bbls) as bbls, tank_key
	   FROM (SELECT t.tank_name, tt.tk_eod_datetime, (SUM(tt.tk_final_gsv)* -1) as bbls,  tt.tk_source_tank_key as tank_key
			 FROM tickets tt INNER JOIN 
			 	 tanks t ON t._key = tt.tk_source_tank_key LEFT OUTER JOIN 
			     ticket_type ty ON tt.tk_type = ty.typ_type
			 WHERE tk_eod_datetime = @eod AND ty.type_is_transfer = 1
			 GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_source_tank_key UNION
			 SELECT t.tank_name, tt.tk_eod_datetime, SUM(tt.tk_final_gsv) as bbls,  tt.tk_destination_tank_key  as tank_key
			 FROM tickets tt INNER JOIN 
			 	 tanks t ON t._key = tt.tk_destination_tank_key LEFT OUTER JOIN 
			     ticket_type ty ON tt.tk_type = ty.typ_type
			 WHERE tk_eod_datetime = @eod AND ty.type_is_transfer = 1
			 GROUP BY t.tank_name, tt.tk_eod_datetime,  tt.tk_destination_tank_key) transfers
	   GROUP BY  tank_name , tk_eod_datetime,  tank_key ) transfers ON ta.tank_key = transfers.tank_key AND ta.eod_datetime = transfers.tk_eod_datetime
WHERE ta.eod_datetime = @eod AND t.active = 1
ORDER BY Tank


INSERT INTO @TankInv  (tank , beginning , shipments , receipts , transfers , book_close , physical_close , gain_loss ) 
select ' Total', SUM(beginning) , SUM(shipments) , SUM(receipts) , SUM(transfers) , SUM(book_close) , SUM(physical_close) , SUM(gain_loss) 
from @TankInv

IF (@total_by_entity = 1) 
BEGIN
	INSERT INTO @TankInv  (tank , beginning , shipments , receipts , transfers , book_close , physical_close , gain_loss ) 
	select ' Total Entity: ' + e.short_name, SUM(beginning) , SUM(shipments) , SUM(receipts) , SUM(transfers) , SUM(book_close) , SUM(physical_close) , SUM(gain_loss) 
	from @TankInv inv INNER JOIN 
		  entities e ON inv.shipper = e._key
		  GROUP BY inv.shipper, e.short_name
END

IF (@total_by_product = 1) 
BEGIN
	INSERT INTO @TankInv  (tank , beginning , shipments , receipts , transfers , book_close , physical_close , gain_loss ) 
	select ' Total Product: ' + p.product_code, SUM(beginning) , SUM(shipments) , SUM(receipts) , SUM(transfers) , SUM(book_close) , SUM(physical_close) , SUM(gain_loss) 
	from @TankInv inv INNER JOIN 
		  products p ON inv.product_key = p._key
	GROUP BY inv.product_key, p.product_code
END

SELECT tank as Tank, 
	   ISNULL(beginning, 0)  as Beginning , 
	   ISNULL(shipments, 0) as Shipments , 
	   ISNULL(receipts, 0) as Receipts , 
	   ISNULL(transfers, 0)  as Transfers, 
	   ISNULL(book_close, 0)  as [Book_Close], 
	   ISNULL(physical_close, 0)  as [Physical_Close], 
	   ISNULL(gain_loss, 0) as [Gain_Loss],
	   ISNULL(product_name, '') as [Product_Name],
	   ISNULL(tank_api, 0) as [Tank_API],
	   ISNULL(row_key, '00000000-0000-0000-0000-000000000000')  as TankActualRowKey
FROM @TankInv
ORDER BY tank



END
