---- filter sections 
--site 
--tank 
--date 

--GRID view 

USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_readings_get]    Script Date: 7/20/2020 6:59:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER   PROCEDURE [dbo].[mgn_tank_readings_get]
 @site uniqueidentifier = NULL, 
	    @tank_key uniqueidentifier, 
		@date datetime
	
AS	

/* 

EXEC mgn_tank_readings_get  @date = '07-15-2020 08:10' 
	                        , @tank_key = '50bb0b35-4c41-4155-b8cf-ccfb725ca6c2'
								
	
*/
BEGIN


declare @eod datetime 
SET @eod  = DATEADD(DAY, 1, SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date) , 07, 00))


IF @site IS NULL
BEGIN 
	SET @site = dbo.get_default_site();
END

SELECT t.tank_name, 
	   tr.tank_timestamp,
	   ROUND(tr.tank_gsv,2) as tank_gsv , 
	   ROUND(tr.tank_tov,2) as tank_tov
FROM tank_readings tr INNER JOIN 
	 tanks t ON tr.tank_key = t._key INNER JOIN 
	 sites s ON t.site_key = s._key 
WHERE t._key = @tank_key AND s._key = @site 
	  AND tank_timestamp BETWEEN DATEADD(DAY, -1, @eod) AND @eod
ORDER BY tank_timestamp

END





