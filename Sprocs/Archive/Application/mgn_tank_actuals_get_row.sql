USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_actuals_get_row]    Script Date: 6/4/2020 8:21:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Obtains a single row of tank actuals data from a specified guid.

/*EXEC mgn_tank_actuals_get_row
	@row  = 'F6DA37F0-689D-47A9-84AD-B4EAC7CC7889'
	*/
-- =============================================
CREATE or ALTER           PROCEDURE [dbo].[mgn_tank_actuals_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from tank_actuals
--WHere _key = @row

SELECT        ta._key,
			  ta.tank_key as [tank_key],
			  t.tank_name as [tank_name],
              DATEADD( day, -1,ta.eod_datetime) as [date_datetime],
              ROUND(ISNULL(ta.tank_gsv_change,0), 2) as [tank_gsv_change],
			  ROUND(ISNULL(ta.tank_tov_change,0), 2) as [tank_tov_change],
			  ROUND(ISNULL(ta.open_gsv, 0), 2) as [open_gsv],
			  ROUND(ISNULL(ta.close_gsv,0), 2) as [close_gsv],
			  ta.site_key as [site_key],
              s.short_name as [site_name]

	   
FROM          tank_actuals AS ta INNER JOIN
              sites AS s ON s._key = ta.site_key INNER JOIN
              tanks AS t ON ta.tank_key = t._key

WHERE  @row = ta._key
	
END