USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_meter_products_get_row]    Script Date: 4/9/2021 10:36:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/9/2021
-- Description:		Obtains a single row of meter product data from a specified guid.

/*EXEC mgn_meter_products_get_row
	@row  = '2567c667-0e9b-4a5c-a937-035117907b7f'

	*/
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_meter_products_get_row]
	@row uniqueidentifier = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from meters
--WHere _key = @row
   
SELECT			mp._key,
				mp.meter_key as [meter_key],
				m.skid_name + ' ' + m.meter_name as [meter_name],
				mp.product_key as [product_key],
				p.product_name as [product_name],
				mp.mtr_start_datetime as [mtr_start_datetime],
				mp.mtr_end_datetime as [mtr_end_datetime]

FROM			meter_products AS mp INNER JOIN
				meters AS m ON m._key = mp.meter_key INNER JOIN
				products AS p ON p._key = mp.product_key

WHERE			@row = mp._key
	  
END
