USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_product_get_list]    Script Date: 2/3/2021 1:31:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>

/*EXEC mgn_product_verification 
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@product = 'WTI'*/

-- =============================================
CREATE OR ALTER   PROCEDURE [dbo].[mgn_product_verification] 
	@site uniqueidentifier = null,
	@product nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

/* Declaring a variable that will tell whether the product exists in the products table or not */
DECLARE @prod_exist bit

if EXISTS 
	(
		SELECT _key
		 from products 
		 WHERE (@site =  [site_key]) AND
			   ([deleted] = 0) AND
			   ((@product = product_code) OR
				(@product = product_name))
	)
	BEGIN
		/* if the product given to SQL exists in the products table at the same site that was also passed to SQL, then set the main variable to 1 (True) */
		Set @prod_exist = 1
	END
Else
	Begin
		/* if the product given to SQL does not exist in the products table at the same site that was also passed to SQL, then set the main variable to 0 (False) */
		Set @prod_exist = 0
	End

/* Returns a table with 1 column and 1 row, whose only value is the value of the main variable, @prod_exist */
Select @prod_exist

END
