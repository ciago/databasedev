USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_allocations_entity_totals_get]    Script Date: 5/1/2020 7:35:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago
-- Create date: 2/26/2020
-- Description:	Gets the totals for all entities for a given month of pipeline allocations
/*EXEC mgn_pipeline_allocations_entity_totals_get  @month = 6, @year = 2020 , @pipeline_key = 'fc536541-5a92-466a-bcd1-db7fedcd15e7'
   , @product_key = '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0'
	*/
-- =============================================
CREATE OR ALTER   PROCEDURE [dbo].mgn_pipeline_allocations_entity_totals_get
--declare
	@pipeline_key uniqueidentifier, 
	@month int , 
	@year int ,
	@product_key uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- the time the month starts 
DECLARE @month_start_datetime DATETIME = DATETIMEFROMPARTS(@year, @month, 1, 7 , 0, 0, 0)
-- the end of day for the first day of the month 
DECLARE @month_eod_start_datetime DATETIME = DATETIMEFROMPARTS(@year, @month, 2, 7 , 0, 0, 0)
-- the end of day for the last day of the month 
DECLARE @month_end_datetime DATETIME = DATETIMEFROMPARTS(@year, (@month + 1) , 1, 7 , 0, 0, 0)
--select @month_eod_start_datetime, @month_end_datetime

DECLARE @results TABLE ([month] int, [year] int, entity_key uniqueidentifier, [entity_name] nvarchar(100), allocated_amt int,
			            bpm int, allocated_percent decimal(6,4), is_balancing bit, allocated_diff int, pipeline_key uniqueidentifier, product_key uniqueidentifier) 


IF(@product_key IS NULL)
BEGIN
	INSERT INTO @results ([month], [year] , entity_key, pipeline_key, product_key)
	SELECT DISTINCT  @month, @year, al.entity_key,  al.pipeline_key, NULL
	FROM pipeline_allocations al
	WHERE eod_datetime BETWEEN @month_start_datetime AND @month_end_datetime
		  AND  al.pipeline_key = @pipeline_key
END
ELSE
BEGIN
	INSERT INTO @results ([month], [year], entity_key,  pipeline_key, product_key)
	SELECT DISTINCT  @month, @year, al.entity_key, al.pipeline_key, product_key
	FROM pipeline_allocations al
	WHERE eod_datetime BETWEEN @month_start_datetime AND @month_end_datetime
			AND product_key = @product_key   AND  al.pipeline_key = @pipeline_key
END


--select *
--from @results

UPDATE @results 
SET entity_name = (SELECT name FROM entities WHERE _key = entity_key)


--SELECT *
--FROM @results r INNER JOIN 
IF @product_key IS NULL
BEGIN
	--SELECT r.eod_datetime, nom.bbls_per_month, nom.allocation_percent, nom.is_balancing, nom.pipeline_key, nom.entity_key, nom.product_key 
	UPDATE r
	SET bpm = nom.bbls_per_month, 
		allocated_percent = ISNULL(nom.allocation_percent,0), 
		is_balancing = nom.is_balancing
	FROM @results r INNER JOIN 
		(SELECT SUM(n.bbls_per_month) as bbls_per_month, 
	  		   AVG(NULLIF(n.allocation_percent,0)) as allocation_percent, 
	  		   0 as is_balancing,
	  		   n.pipeline_key,
	  		   n.month,
	  		   n.year,
	  		   n.entity_key,
	  		   NULL as product_key
		  FROM nominations n 
		  WHERE n.month = @month AND n.year = @year AND n.pipeline_key= @pipeline_key
		  GROUP BY n.month, n.year, n.pipeline_key, n.entity_key) nom ON  r.entity_key = nom.entity_key AND r.pipeline_key = r.pipeline_key 

UPDATE r 
SET allocated_amt = nom.allocated_amount
FROM @results r INNER JOIN 
  (SELECT SUM(allocated_amount)as allocated_amount, entity_key, pipeline_key, NULL as product_key
  FROM pipeline_allocations
  WHERE (product_key = @product_key OR @product_key IS NULL) AND pipeline_key = @pipeline_key 
		AND eod_datetime BETWEEN @month_eod_start_datetime AND @month_end_datetime
  GROUP BY entity_key, pipeline_key) nom ON r.entity_key = nom.entity_key AND r.pipeline_key = r.pipeline_key 

END
ELSE
BEGIN
	--SELECT r.eod_datetime, nom.bbls_per_month, nom.allocation_percent, nom.is_balancing, nom.pipeline_key, nom.entity_key, nom.product_key 
	UPDATE r
	SET bpm = nom.bbls_per_month, 
		allocated_percent = ISNULL(nom.allocation_percent,0), 
		is_balancing = nom.is_balancing
	FROM @results r INNER JOIN 
		(SELECT SUM(n.bbls_per_month) as bbls_per_month, 
	 		   AVG(NULLIF(n.allocation_percent,0)) as allocation_percent, 
	 		   n.is_balancing_shipper as is_balancing,
	 		   n.pipeline_key,
	 		   n.month,
	 		   n.year,
	 		   n.entity_key,
	 		   n.product_key as product_key
		 FROM nominations n 
		 WHERE n.month = @month AND n.year = @year AND n.pipeline_key= @pipeline_key
	 		  AND (@product_key = n.product_key)
		 GROUP BY n.month, n.year, n.pipeline_key,n.entity_key, n.product_key, n.is_balancing_shipper) nom ON  r.entity_key = nom.entity_key AND r.pipeline_key = r.pipeline_key AND nom.product_key = nom.product_key

UPDATE r 
SET allocated_amt = nom.allocated_amount
FROM @results r INNER JOIN 
  (SELECT SUM(allocated_amount)as allocated_amount, entity_key, pipeline_key, product_key as product_key
  FROM pipeline_allocations
  WHERE (product_key = @product_key OR @product_key IS NULL) AND pipeline_key = @pipeline_key 
		AND eod_datetime BETWEEN @month_eod_start_datetime AND @month_end_datetime
  GROUP BY entity_key, pipeline_key, product_key) nom ON r.entity_key = nom.entity_key AND r.pipeline_key = r.pipeline_key 

END


UPDATE @results
SET allocated_diff = ISNULL(allocated_amt, 0) - ISNULL(bpm, 0) 


select *
from @results



END
