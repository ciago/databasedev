USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_nominations_get_list]    Script Date: 5/20/2020 11:27:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Gets a list of nominations from a selected month and year for a variety of pipelines and entities.

/*EXEC mgn_pipeline_nominations_get_list  @month = 5, @year = 2020 , @entity_key = 'D25051F3-4888-4BFB-89FD-4E1449AC0FBA'
	*/
-- =============================================
CREATE OR ALTER   PROCEDURE [dbo].[mgn_pipeline_nominations_get_list]
	@month int, 
	@year int, 
	@entity_key uniqueidentifier = NULL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_nominations

-- check to see if we need to add any seed rows for this time period
DECLARE @possible_records TABLE ([month] int, [year] int, pipeline_key uniqueidentifier, entity_key uniqueidentifier, product_key uniqueidentifier)

INSERT INTO @possible_records ([month], [year], entity_key, product_key, pipeline_key)
SELECT x.month, x.year, x.entity_key, x.product_key, pl._key as pipeline_key
FROM 
		(select @month as [month], 
			   @year as [year], 
			   e._key as entity_key,
			   p._key as product_key
		FROM (SELECT * FROM entities WHERE [type] = 1) e CROSS JOIN 
			 (SELECT * FROM products p WHERE p.active = 1)p) x CROSS JOIN 
	    (SELECT * FROM pipeline where active = 1)pl



		--select * from @possible_records

		INSERT INTO nominations(_key, month, year, pipeline_key, entity_key, product_key, bbls_per_day, bbls_per_month, allocation_percent, is_balancing_shipper)
		SELECT newid(),pr.month, pr.year, pr.pipeline_key,  pr.entity_key,  pr.product_key, 0, 0, 0, 0
		FROM @possible_records pr LEFT OUTER JOIN 
			 nominations n ON pr.[month] = n.[month] AND pr.[year] = n.[year] AND pr.pipeline_key = n.pipeline_key AND pr.entity_key = n.entity_key AND pr.product_key = n.product_key
		WHERE n._key IS NULL
--**********************************************************************



SELECT        n._key,
			  n.month,
			  n.year, 
			  pl.description as [pipeline_desc], 
			  e.name AS entity_name, 
			  n.bbls_per_day,
			  n.bbls_per_month,
			  n.pipeline_key, 
			  n.entity_key, 
			  n.site_key, n.deleted, 
			  n.record_info, 
			  sites.name AS site_name

FROM          nominations AS n INNER JOIN
              pipeline AS pl ON n.pipeline_key = pl._key INNER JOIN
              entities e ON n.entity_key = e._key INNER JOIN
              sites ON n.site_key = sites._key

WHERE n.month = @month
  AND n.year = @year
  AND (e._key = @entity_key OR @entity_key IS NULL)

END
