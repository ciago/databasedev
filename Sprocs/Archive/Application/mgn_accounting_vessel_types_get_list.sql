USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_vessel_types_get_list]    Script Date: 12/15/2020 9:46:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table

/*EXEC [mgn_accounting_vessel_types_get_list]
	@site  = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
	@user  = 'candice'
	*/
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_accounting_vessel_types_get_list]
	/*Defining the variable that I will eventually be filtering the table by*/
	@site uniqueidentifier = null,
	@user nvarchar(50) = 'candice'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--Where _key = @volume_type

--DECLARE @eod datetime = SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date), 07, 00)

Select vt._key, 
	   vt.vessel_type, 
	   vt.vessel_volume,
	   vt.site_key,
	   sites.short_name,
	   vt.active
from vessel_types as vt Inner JOIN 
	 sites on vt.site_key = sites._key

WHERE vt.site_key = @site and vt.active = 'True'
GROUP BY vt._key, vt.vessel_type, vt.vessel_volume, vt.site_key, sites.short_name, vt.active
--ORDER BY berth_description, valve_start
	
END
