USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_allocations_day_totals_get]    Script Date: 5/1/2020 7:34:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago
-- Create date: 2/26/2020
-- Description:	Gets the allocations for a single day on a single pipeline 

/*EXEC mgn_pipeline_allocations_day_totals_get
	@pipeline_key = 'fc536541-5a92-466a-bcd1-db7fedcd15e7',
	@date  = '6/01/2020'
	*/
-- =============================================
CREATE OR ALTER   PROCEDURE [dbo].[mgn_pipeline_allocations_day_totals_get]
 	@pipeline_key uniqueidentifier = 'fc536541-5a92-466a-bcd1-db7fedcd15e7', 
		@date date = '5/01/2020'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

declare @datetime DATETIME = CONVERT(DATETIME, @date)

DECLARE @eod DATETIME  =  DATEADD(day, 1, SMALLDATETIMEFROMPARTS(YEAR(@datetime), MONTH(@datetime), DAY(@datetime), 07, 00)) 
DECLARE @month int =  (SELECT MONTH(@date)), 
	    @year int =  (SELECT YEAR(@date))


	-- fill the tmp table with all pipeline schedules by product by hour
EXEC mgn_pipeline_schedule_fill_hourly
	@month = @month,
	@year  = @year


	-- fill the tmp table with all pipeline schedules by product by hour
EXEC mgn_pipeline_schedule_fill_daily
	@month = @month,
	@year  = @year

	-- return data 
SELECT p.short_code as pipeline_name, pr.product_code as product_name,
	   CONVERT(int, ROUND(d.volume,0)) as scheduled_volume , 
	   CONVERT(int, ROUND(total_allocated,0)) as total_allocated, 
	   CONVERT(int, ROUND(d.volume - total_allocated,0)) as diff  

FROM
 (SELECT CONVERT(Date, start_datetime) as [date], ISNULL(AVG(NULLIF(rate,0)),0) as rate, SUM(volume) as volume, pipeline_key, end_datetime, product_key
  FROM tmp_pipeline_by_day d 
  WHERE (pipeline_key = @pipeline_key)
  GROUP BY start_datetime, pipeline_key, end_datetime, product_key)d  LEFT OUTER JOIN 
 (SELECT sum(allocated_amount) as total_allocated, pipeline_key, product_key
  FROM pipeline_allocations pa 
  WHERE eod_datetime = @eod AND pa.pipeline_key = @pipeline_key
  GROUP BY pipeline_key, product_key
  )act ON act.pipeline_key = d.pipeline_key LEFT OUTER JOIN 
  pipeline p ON p._key = d.pipeline_key LEFT OUTER JOIN 
  products pr ON pr._key = d.product_key
WHERE d.pipeline_key = @pipeline_key AND   d.end_datetime = @eod  AND d.[date] = @date and d.product_key = act.product_key

END