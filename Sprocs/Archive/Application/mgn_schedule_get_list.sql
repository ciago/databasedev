USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_schedule_get_list]    Script Date: 6/1/2020 2:59:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Brandon Morey    
-- Create date: 5/4/2020
-- Description:    Gets a list of schedules from a selected month, year, site, and pipeline.

/*EXEC mgn_schedule_get_list  @site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', @pipeline_key = 'fc536541-5a92-466a-bcd1-db7fedcd15e7', @month = 5, @year = 2020

    */
	
-- =============================================
CREATE OR ALTER           PROCEDURE [dbo].[mgn_schedule_get_list]

    @site uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 
    @pipeline_key uniqueidentifier = 'fc536541-5a92-466a-bcd1-db7fedcd15e7', 
    @month int = 5, 
    @year int  = 2020
	
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from import_scedule

--6/1/2020 02:00 - 6/1/2020 07:00
--5/1/2020 07:00 am - 6/1/2020 07:00 month 
DECLARE @month_start_datetime DATETIME = DATETIMEFROMPARTS(@year, @month, 1, 7 , 0, 0, 0)
DECLARE @month_end_datetime DATETIME = DATETIMEFROMPARTS(@year, (@month + 1) , 1, 7 , 0, 0, 0)

SELECT        i._key,
              i.start_datetime as [start_datetime],
              i.stop_datetime as [stop_datetime], 
              CONVERT(int, Round(i.evt_volume,0)) as [evt_volume],
              CONVERT(int, Round(i.rate_bbls_hour,0)) as [rate_bbls_hour],
              i.pipeline_key as [pipeline_key],
              pl.description as [pipeline_desc], 
              p.product_name as [product_name],
              ISNULL(e.name,'') as [entity_name],
              s.short_name as [site_name]

FROM          import_schedule AS i INNER JOIN
              products AS p ON p._key = i.product_key INNER JOIN
              sites AS s ON s._key = i.site_key INNER JOIN
              pipeline AS pl ON i.pipeline_key = pl._key LEFT OUTER JOIN 
              entities AS e ON i.shipper_key = e._key
			  
WHERE		  i.start_datetime >= @month_start_datetime
          AND i.start_datetime < @month_end_datetime
          AND s._key = @site
		  AND i.pipeline_key = @pipeline_key

ORDER BY start_datetime
		  
END
 