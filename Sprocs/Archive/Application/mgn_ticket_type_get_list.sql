--USE [Hartree_TMS]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_ticket_type_get_list]    Script Date: 4/12/2021 11:40:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_ticket_type_get_list] 
	@site uniqueidentifier = null, 
	@user varchar(50) = 'default'

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


SELECT 0, 'Select Ticket Type'
UNION
SELECT typ_type, type_description 
FROM ticket_type
WHERE type_active = 1
    
END





