USE [Moda]
GO

/****** Object:  Table [dbo].[nominations]    Script Date: 5/21/2020 10:19:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[nominations](
	[_key] [uniqueidentifier] NOT NULL,
	[month] [int] NULL,
	[year] [int] NULL,
	[pipeline_key] [uniqueidentifier] NULL,
	[entity_key] [uniqueidentifier] NULL,
	[bbls_per_day] [int] NULL,
	[bbls_per_month] [int] NULL,
	[site_key] [uniqueidentifier] NOT NULL,
	[deleted] [bit] NOT NULL,
	[record_info] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[nominations] ADD  CONSTRAINT [DF_nominations__key]  DEFAULT (newid()) FOR [_key]
GO

ALTER TABLE [dbo].[nominations] ADD  CONSTRAINT [DF_nominations_site_key]  DEFAULT ([dbo].[get_default_site]()) FOR [site_key]
GO

ALTER TABLE [dbo].[nominations] ADD  CONSTRAINT [DF_nominations_deleted]  DEFAULT ((0)) FOR [deleted]
GO

ALTER TABLE [dbo].[nominations] ADD  CONSTRAINT [DF_nominations_record_info]  DEFAULT ([dbo].[default_record_info]()) FOR [record_info]
GO


