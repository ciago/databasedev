USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_meter_factor_update]    Script Date: 4/9/2021 11:04:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/9/2021
-- Description:		Update selected meter factor record with new user input details.

/*EXEC mgn_meter_factor_update 
	@row = '8B7BF623-D438-4CAC-8BC8-BB36216D9371',
    @meter_key = 'dc6794e1-ee3d-478f-bdd5-13352fc58512',
	@meter_factor = 123,
	@mtr_start_datetime = '2020-01-01 10:01',
	@mtr_end_datetime = '2020-01-02 10:02',
	@deleted = 0,
	@user = 'candice'

	// 
	*/
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_meter_factor_update]

	@row uniqueidentifier,	
	@meter_key uniqueidentifier,
	@meter_factor float,
	@mtr_start_datetime datetime,
	@mtr_end_datetime datetime = NULL,
	@deleted bit,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from meter_factor
--Where _key = @row

IF @row = '00000000-0000-0000-0000-000000000000'
BEGIN
	SET @row = newid()
END



IF(@row IN (SELECT _key from meter_factor))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM meter_factor WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE meter_factor

SET		meter_key = @meter_key,
		meter_factor = @meter_factor,
		mtr_start_datetime = @mtr_start_datetime,
		mtr_end_datetime = @mtr_end_datetime,
		deleted = @deleted,
		record_info = @json

WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[meter_factor]
           ([_key]
			,[meter_key]
			,[meter_factor]
			,[mtr_start_datetime]
			,[mtr_end_datetime]
			,[deleted]
			,[record_info])
     VALUES
           (@row
		   	,@meter_key
			,@meter_factor
			,@mtr_start_datetime
			,@mtr_end_datetime
			,@deleted
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
