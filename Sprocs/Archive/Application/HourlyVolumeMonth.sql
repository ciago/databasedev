
Declare @month int = 5
declare @year int = 2020
DECLARE @return_hourly bit = 0, 
		@get_hourly_view bit = 0,
	    @return_monthly bit = 0,
		@pipeline_key uniqueidentifier = 'FC536541-5A92-466A-BCD1-DB7FEDCD15E7',
		@product_key uniqueidentifier = NULL, 
		@all_pipes bit = 0,
		@all_products bit = 1 ,
		@return_allocation_page_view bit = 1
-- to get the hourly view by pipeline all products pass @get_hourly_view = 1, @pipeline_key = 'FC536541-5A92-466A-BCD1-DB7FEDCD15E7', @month = 5, @year = 2020

DECLARE @month_start_datetime DATETIME = DATETIMEFROMPARTS(@year, @month, 1, 7 , 0, 0, 0)
DECLARE @month_end_datetime DATETIME = DATETIMEFROMPARTS(@year, (@month + 1) , 1, 7 , 0, 0, 0)

-- fill the tmp table with all pipeline schedules by product by hour
EXEC mgn_pipeline_schedule_fill_hourly
	@month = @month,
	@year  = @year



IF (@return_hourly = 1 )
BEGIN

	IF(@all_products = 0)
	BEGIN
		SELECT start_datetime, end_datetime, rate, volume, pipeline_key, product_key
		FROM tmp_pipeline_by_hours
		WHERE (@product_key IS NULL OR product_key = @product_key)
			  AND (@pipeline_key IS NULL OR pipeline_key = @pipeline_key)
		ORDER BY start_datetime, pipeline_key
	END

	IF (@all_products = 1)
	BEGIN		
		SELECT start_datetime, end_datetime, ISNULL(AVG(NULLIF(rate,0)),0) as rate , SUM(volume) as volume, pipeline_key
		FROM tmp_pipeline_by_hours
		WHERE (@pipeline_key IS NULL OR pipeline_key = @pipeline_key)
			   --AND rate <> 0
		GROUP BY start_datetime, end_datetime, pipeline_key
		ORDER BY start_datetime, pipeline_key;
	END
END




----   Data by Day *********************************************************************************************



IF (@return_monthly = 1 )
BEGIN
  IF (@all_products = 1 ) 
  BEGIN
	SELECT CONVERT(Date, start_datetime) as [date], ISNULL(AVG(NULLIF(rate,0)),0) as rate, SUM(volume) as volume, pipeline_key
	FROM @dailyData d 
	WHERE (pipeline_key = @pipeline_key)
	GROUP BY start_datetime, pipeline_key
  END
  ELSE
  BEGIN
	select * 
	from @dailyData
	WHERE (@product_key IS NULL OR product_key = @product_key)
			AND (@pipeline_key IS NULL OR pipeline_key = @pipeline_key)
  END

END




--select * 
--from @AllHourlyVolume
--GROUP BY 