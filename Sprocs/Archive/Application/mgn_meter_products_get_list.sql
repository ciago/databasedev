USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_meter_products_get_list]    Script Date: 4/9/2021 10:32:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/9/2021
-- Description:		Gets a list of meter products from a selected site and meter.

/*EXEC mgn_meter_products_get_list  
	@site_key = '00000000-0000-0000-0000-000000000000',
	@meter_key = 'cd720764-4264-4b31-ae1c-6f5c47666437'

    */	
-- =============================================
ALTER        PROCEDURE [dbo].[mgn_meter_products_get_list]

	@site_key uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@meter_key uniqueidentifier = '00000000-0000-0000-0000-000000000000'
	  
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from meter_products

SELECT			mp._key,
				mp.meter_key as [meter_key],
				ISNULL(m.skid_name, '') + ' ' + m.meter_name as [meter_name],
				mp.product_key as [product_key],
				ISNULL(p.product_name,'') as [product_name],
				mp.mtr_start_datetime as [mtr_start_datetime],
				mp.mtr_end_datetime as [mtr_end_datetime]

FROM			meter_products AS mp INNER JOIN
				meters AS m ON m._key = mp.meter_key INNER JOIN
				products AS p ON p._key = mp.product_key			

WHERE			(mp.site_key = @site_key OR @site_key = '00000000-0000-0000-0000-000000000000')
				AND	(mp.meter_key = @meter_key OR @meter_key = '00000000-0000-0000-0000-000000000000')
				AND (mp.deleted = 0)
				and (m.active = 1)

ORDER by meter_name
	  
END
