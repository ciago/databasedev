--USE [Hartree_TMS]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_menu_children_get_list]    Script Date: 4/28/2021 12:30:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table

/*EXEC [mgn_menu_children_get_list]
	@parent_number  = 6, @user = 'ciago'
	*/
-- =============================================
ALTER         PROCEDURE [dbo].[mgn_menu_children_get_list]
	@parent_number int,
	@user nvarchar(50) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC mgn_user_group_permissions_sync

SELECT menu_label_name as label_text, 
       menu_action_result_name as action_result, 
	   menu_controller as controller,
	   @parent_number as parent_number
FROM menu_items r  INNER JOIN 
		 users u ON u.user_login_name = @user INNER JOIN
		 user_groups ug ON ug.user_group_hierarchy = u.user_highest_group_number INNER JOIN 
		 user_groups_permissions ugp ON ug._key = ugp.gp_user_group_key AND r._key = ugp.gp_menu_item_key
WHERE menu_active = 1 
      AND (menu_parent_number_1 = @parent_number or menu_parent_number_2 = @parent_number or menu_parent_number_3 = @parent_number or menu_parent_number_4 = @parent_number)
      AND menu_is_child = 1 
		  AND ugp.gp_view_permission = 1
ORDER BY --try to make the first links be those whose first digits match those of the parent_number variable
		 menu_sort
	
END
