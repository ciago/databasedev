--USE [Hartree_TMS]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_product_update]    Script Date: 4/8/2021 8:09:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/26/2021
-- Description:		Update selected product recipes record with new user input details.

/*EXEC mgn_product_update 
	@row = '713FFF12-5DCD-46DC-83D9-FBAD54498F61',
	@product_name = 'TestInsert123',
	@product_class_key = '3BDCB8C4-B681-4F6F-B2F2-1BC2E6E9CBDC',
	@product_api = '12.12',
	@safe_operating_temperature = '123.12',
	@product_external_id = 'WTI',
	@product_code = 'WTI',
	@product_irs_code  = 'XXX',
	@site_key = 'B8AB8843-02D4-4E7D-96CB-E3648327C0DD',
	@active = 1,
	@user = 'candice'

	// 
	*/
-- =============================================
CREATE OR ALTER  PROCEDURE [dbo].[mgn_product_update]

	@row uniqueidentifier,
	@product_name nvarchar(100),
	@product_class_key uniqueidentifier,
	@product_api float,
	@safe_operating_temperature float,
	@product_external_id nvarchar(50),
	@product_code nvarchar(8),
	@product_irs_code nvarchar(3) = 'XXX',
	@site_key uniqueidentifier,
	@active bit,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from products
--Where _key = @row




IF(@row IN (SELECT _key from products))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM products WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE products

SET		product_name = @product_name,		
		product_class_key = @product_class_key,
		product_api = @product_api,
		safe_operating_temperature = @safe_operating_temperature,
		product_external_id = @product_external_id,
		product_code = @product_code,
		site_key = @site_key,
		active = @active,
		record_info = @json, 
		product_irs_code = @product_irs_code

WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[products]
           ([_key]
		   ,[product_name]
		   ,[product_class_key]
		   ,[product_api]
		   ,[safe_operating_temperature]
		   ,[product_external_id]
		   ,[product_code]
		   ,product_irs_code
		   ,[site_key]
		   ,[active]
           ,[record_info])
     VALUES
           (@row
		   ,@product_name
		   ,@product_class_key
		   ,@product_api
		   ,@safe_operating_temperature
		   ,@product_external_id
		   ,@product_code
		   ,@product_irs_code
		   ,@site_key
		   ,@active
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
