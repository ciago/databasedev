USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_vessels_update]    Script Date: 3/2/2021 4:58:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Update selected berth fees record with new user input details.


-- =============================================
create or ALTER           PROCEDURE [dbo].[mgn_nominations_update]

	@_key uniqueidentifier,
    @site_key uniqueidentifier, 
	@start_date datetime,
	@end_date datetime = null,
	@product uniqueidentifier,
	@order_type int,
	@deleted bit = 0,
	@nominated_volume bigint,
	@user nvarchar(50),
	@entity1_key uniqueidentifier=null,
	@entity2_key uniqueidentifier=null,
	@entity3_key uniqueidentifier=null,
	@entity4_key uniqueidentifier=null,
	@entity5_key uniqueidentifier=null,
	@entity6_key uniqueidentifier=null,
	@entity7_key uniqueidentifier=null

	
	/*EXEC [mgn_nominations_update] 
	@_key = '9f0b321f-72ba-40fc-8cbe-59b720e9dd5a',
	@vessel_name = 'oranges',
	@vessel_type_key = '43b2fb58-ddd3-429b-a396-a39d611fb7c6',
	@vessel_description = 'oranges',
	@vessel_external_id = 50,
	@vessel_loa = 3,
	@user = 'candice',
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@deleted = 0
	
	// 
	*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

IF(@_key IN (SELECT n._key from nominations_general as n))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT n.record_info FROM nominations_general as n WHERE _key = @_key)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE nominations_general

SET		[start_date] = @start_date,
		end_date = @end_date,
		[order_type] = @order_type,
		product_key = @product,
		entity1_key = @entity1_key,
		entity2_key = @entity2_key,
		entity3_key = @entity3_key,
		entity4_key = @entity4_key,
		entity5_key = @entity5_key,
		entity6_key = @entity6_key,
		entity7_key = @entity7_key,
		[nom_volume] = @nominated_volume,
		[site] = @site_key,
		[deleted] = @deleted,
		record_info = @json

WHERE _key = @_key

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[nominations_general]
           ([_key]
           ,[start_date]
           ,[end_date]
           ,[order_type]
           ,[product_key]
		   ,[entity1_key]
		   ,[entity2_key]
		   ,[entity3_key]
		   ,[entity4_key]
		   ,[entity5_key]
		   ,[entity6_key]
		   ,[entity7_key]
		   ,[nom_volume]
		   ,[site]
           ,[deleted]
		   ,[record_info])
     VALUES
           (@_key
           ,@start_date
           ,@end_date
           ,@order_type
           ,@product
		   ,@entity1_key
		   ,@entity2_key
		   ,@entity3_key
		   ,@entity4_key
		   ,@entity5_key
		   ,@entity6_key
		   ,@entity7_key
		   ,@nominated_volume
		   ,@site_key
		   ,@deleted
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE()))

END

END
