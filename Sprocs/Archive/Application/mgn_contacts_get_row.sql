USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_contacts_get_row]    Script Date: 4/9/2021 2:31:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/24/2021
-- Description:		Obtains a single row of contacts data from a specified guid.

/*EXEC mgn_contacts_get_row
	@row  = 'd95c9ea3-022f-4424-bd95-e28ede5c900d'

	*/
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_contacts_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from contacts
--WHere _key = @row
   
SELECT			c._key,
				ISNULL(c.first_name,'') + ' ' + ISNULL(c.last_name,'')  as [contact_name],
				ISNULL(e.name,'') as [entity_name],
				ISNULL(c.entity_key, '00000000-0000-0000-0000-000000000000') as [entity_key],
				ISNULL(et.type_description,'') as [entity_type_desc],
				e.type as [type],
				ISNULL(ct.contact_type_description,'') as [contact_type_desc],
				ISNULL(c.contact_type_key, '00000000-0000-0000-0000-000000000000') as [contact_type_key],
				ISNULL(c.first_name,'') as [first_name],
				ISNULL(c.last_name,'') as [last_name],
				ISNULL(c.company_name,'') as [company_name],
				ISNULL(c.address_1,'') as [address_1],
				ISNULL(c.address_2,'') as [address_2],
				ISNULL(c.city,'') as [city],
				ISNULL(s.state,'') as [state],
				ISNULL(c.state_key, '00000000-0000-0000-0000-000000000000') as [state_key],
				ISNULL(co.country,'') as [country],
				ISNULL(s.country_key, '00000000-0000-0000-0000-000000000000') as [country_key],
				ISNULL(c.zip_code,'') as [zip_code],
				ISNULL(c.phone_1,'') as [phone_1],
				ISNULL(c.phone_2,'') as [phone_2],
				ISNULL(c.email_address,'') as [email_address]	

FROM			contacts AS c INNER JOIN
				entities AS e ON e._key = c.entity_key LEFT OUTER JOIN
				entity_types AS et ON et.type_code = e.type LEFT OUTER JOIN
				contact_types AS ct ON ct._key = c.contact_type_key LEFT OUTER JOIN
				states AS s ON s._key = c.state_key LEFT OUTER JOIN
				countries AS co ON co._key = s.country_key

WHERE			@row = c._key
	  
END
