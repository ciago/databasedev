USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_allocations_row_update]    Script Date: 5/1/2020 7:36:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets one row from the allocation for a single day for a single pipeline for a single entity

/*EXEC mgn_pipeline_allocations_row_update
	@row  = '7b978f4f-9d88-4aee-b272-25302934e51a'
	@amount = 30000
	*/
-- =============================================
CREATE OR ALTER     PROCEDURE [dbo].[mgn_pipeline_allocations_row_update]
	@row uniqueidentifier = null, 
	@amount int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



UPDATE pipeline_allocations
SET allocated_amount = @amount
WHERE _key = @row


	
END
