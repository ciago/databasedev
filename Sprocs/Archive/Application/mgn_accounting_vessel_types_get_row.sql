USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_vessel_types_get_row]    Script Date: 12/14/2020 5:33:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Obtains a single row of volume accounts data by searching for a specified guid.

/*EXEC mgn_accounting_vessel_types_get_row
	@row  = '4eec6d8d-8c3f-4c9f-896b-6ba19865131a'
	*/
-- =============================================
ALTER           PROCEDURE [dbo].[mgn_accounting_vessel_types_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from 
--WHere _key = @row
   
SELECT vt._key,
	   vt.vessel_type, 
	   vt.vessel_volume,
	   vt.site_key,
	   sites.short_name,
	   vt.active
			  
FROM vessel_types vt left join
	 sites ON vt.site_key = sites._key 

WHERE @row = vt._key
GROUP BY vt._key,  vt.vessel_type, vt.vessel_volume, vt.site_key, sites.short_name,vt.active
	  
END