USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_location_get_list]    Script Date: 3/4/2021 6:22:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/4/2021
-- Description:		Select list of locations for dropdown usage.


/*EXEC mgn_location_get_list

	*/
-- =============================================
CREATE or ALTER     PROCEDURE [dbo].[mgn_location_get_list] 
    @site_key uniqueidentifier = null, 
	@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();
	SELECT '00000000-0000-0000-0000-000000000000', ' Select Location' as [name]
	UNION
    SELECT _key, ln_location_name
    FROM locations
	WHERE ln_site_key = @site_key

	ORDER BY name
    
END