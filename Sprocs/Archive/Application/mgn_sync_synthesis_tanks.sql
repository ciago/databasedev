USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_sync_synthesis_tanks]    Script Date: 7/20/2020 3:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER   PROCEDURE [dbo].[mgn_sync_synthesis_tanks]
 
	
AS
/* 
EXEC mgn_sync_synthesis_tanks
	
*/
BEGIN

UPDATE t1
SET tank_max_operational_limit = ts.MAX_VOLUME,
	tank_min_operational_limit =ts.HEEL_VOLUME, 
	active =  md.ACTIVE_IND, 
	tank_product = p._key
--SELECT t1.tank_name, md.MASTER_LOCATION_ID, ts.MAX_VOLUME, ts.HEEL_VOLUME, md.DESCRIPTION, md.CODE, md.ACTIVE_IND -- our table external_inventory-id
FROM tanks t1 INNER JOIN 
	EMERSON.Synthesis_Test_02242020.dbo.MASTER_DEVICE md ON md.CODE = t1.tank_external_id_inventory INNER JOIN 
	EMERSON.Synthesis_Test_02242020.dbo.TANK_STRAPPING ts ON ts.MASTER_DEVICE_ID = md.MASTER_DEVICE_ID INNER JOIN 
	EMERSON.Synthesis_Test_02242020.dbo.TANK t ON  md.MASTER_DEVICE_ID = t.MASTER_DEVICE_ID  LEFT OUTER JOIN 
	EMERSON.Synthesis_Test_02242020.dbo.MASTER_PRODUCT mp ON mp.MASTER_PRODUCT_ID = t.MASTER_PRODUCT_ID LEFT OUTER JOIN 
	products p ON p.product_external_id = mp.PRODUCT_CODE
 WHERE ts.ACTIVE_STRAPPING = 1 


INSERT INTO [dbo].[tanks]
           ([_key]
           ,[tank_name]
           ,[tank_description]
           ,[tank_pi_name]
           ,[tank_external_id_inventory]
           ,[tank_external_id_location]
           ,[tank_max_operational_limit]
           ,[tank_min_operational_limit]
           ,[active]
		   ,tank_product)
SELECT newid(), md.CODE,  md.DESCRIPTION, REPLACE(REPLACE(REPLACE(md.CODE, 'tk', ''), 'TK', ''), ' ', '' ), md.CODE, loc.LOCATION_CODE,  ts.MAX_VOLUME, ts.HEEL_VOLUME, md.ACTIVE_IND, p._key as product_key -- our table external_inventory-id
FROM EMERSON.Synthesis_Test_02242020.dbo.TANK t INNER JOIN 
 	EMERSON.Synthesis_Test_02242020.dbo.MASTER_DEVICE md ON md.MASTER_DEVICE_ID = t.MASTER_DEVICE_ID INNER JOIN 
	(SELECT * FROM OPENQUERY(EMERSON, 'select * FROM Synthesis_Test_02242020.dbo.MASTER_LOCATION')) loc ON md.MASTER_LOCATION_ID = loc.MASTER_LOCATION_ID LEFT OUTER JOIN 
	(SELECT * FROM EMERSON.Synthesis_Test_02242020.dbo.TANK_STRAPPING WHERE ACTIVE_STRAPPING = 1)ts ON ts.MASTER_DEVICE_ID = md.MASTER_DEVICE_ID LEFT OUTER JOIN 
	EMERSON.Synthesis_Test_02242020.dbo.MASTER_PRODUCT mp ON mp.MASTER_PRODUCT_ID = t.MASTER_PRODUCT_ID LEFT OUTER JOIN 
	products p ON p.product_external_id = mp.PRODUCT_CODE
   WHERE md.CODE NOT IN (SELECT tank_external_id_inventory FROM tanks)
   --AND md.DESCRIPTION NOT LIKE '%Virtual%'

END