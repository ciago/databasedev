USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_actuals_update]    Script Date: 2/1/2021 5:25:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected pipeline_actuals with new user input details.

/*EXEC mgn_import_schedule_update 
	@pipeline = 'fc536541-5a92-466a-bcd1-db7fedcd15e7',
	@start = '2020-07-01 7:00:00.000',
    @stop = '2020-07-01 16:00:00.000', 
	@product_grade = 'EFCR',
	@evt_vol = 87010,
	@rate_bbls_hr = 10000,
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@account_number = 'test',
	@batch_code = 'test',
	@shipper_name = 'test',
	@nom_vol = 87010,
	@notes = '',
	@is_shutdown = 0,
	@user = 'Candice'

	// 
	*/
-- =============================================
CREATE or ALTER         PROCEDURE [dbo].[mgn_import_schedule_update]

	@pipeline uniqueidentifier,
	@start datetime,
    @stop datetime, 
	@product_grade nvarchar(50) = '',
	@evt_vol int,
	@rate_bbls_hr int = 0,
	@site uniqueidentifier,
	@account_number nvarchar(50) = '',
	@batch_code nvarchar(50) = '',
	@shipper_name nvarchar(50) = '',
	@nom_vol int,
	@notes nvarchar(100) = '',
	@is_shutdown bit = 0,
	@user nvarchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_actuals
--Where _key = @row

Declare @product_key uniqueidentifier

if (@product_grade != '')
	BEGIN
		SET @product_key = (select _key from [dbo].[products] where @product_grade = [dbo].[products].product_code)
	END

Declare @shipper_key uniqueidentifier

if (@shipper_name != '')
	BEGIN
		SET @shipper_key = (select _key from [dbo].[entities] where @shipper_name = [dbo].[entities].short_name)
	END

Delete from import_schedule
where ((@start <= import_schedule.start_datetime) AND (import_schedule.start_datetime < @stop) AND (@stop <= import_schedule.stop_datetime)) OR
	  ((import_schedule.start_datetime <= @start) AND (@start < import_schedule.stop_datetime) AND (@stop >= import_schedule.stop_datetime)) OR
	  ((@start <= import_schedule.start_datetime) AND (@stop >= import_schedule.stop_datetime))

INSERT INTO [import_schedule] (pipeline_key,
							   start_datetime,
							   stop_datetime,
							   product_grade,
							   evt_volume,
							   rate_bbls_hour,
							   site_key,
							   account_number,
							   batch_code,
							   shipper,
							   nom_volume,
							   notes,
							   product_key,
							   shipper_key,
							   [record_info],
							   is_shutdown)
VALUES (@pipeline,
		@start,
		@stop,
		@product_grade,
		@evt_vol,
		@rate_bbls_hr,
		@site,
		@account_number,
		@batch_code,
		@shipper_name,
		@nom_vol,
		@notes,
		@product_key,
		@shipper_key,
		dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE()),
		@is_shutdown);

END
