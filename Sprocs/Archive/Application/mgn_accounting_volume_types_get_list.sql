USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_volume_types_get_list]    Script Date: 6/12/2020 10:05:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE OR ALTER PROCEDURE [dbo].[mgn_accounting_volume_types_get_list] 
	@site_key uniqueidentifier = null,
	@user nvarchar(50) = ''
	--@terminal uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT '00000000-0000-0000-0000-000000000000' AS RowKey, 'Select Type' AS description
	UNION
    SELECT _key as RowKey, [type_description] AS description
	FROM [dbo].[accounting_volume_types]
	--WHERE active = 1
	ORDER BY RowKey

END



-- EXEC [mgn_accounting_volume_types_get_list]