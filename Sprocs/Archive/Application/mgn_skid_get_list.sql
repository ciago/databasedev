USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_skid_get_list]    Script Date: 9/10/2020 9:47:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_skid_get_list
 
	*/

CREATE OR ALTER         PROCEDURE [dbo].[mgn_skid_get_list] 
    @site_key uniqueidentifier = null, 
	@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();

	SELECT '00000000-0000-0000-0000-000000000000' as [key], 'Select Vessel Skid' as [skid_description]
	UNION
    SELECT _key as [key],
		   skid_description as [skid_description]
    FROM skids
	WHERE site_key = @site_key
	ORDER BY skid_description
    
END