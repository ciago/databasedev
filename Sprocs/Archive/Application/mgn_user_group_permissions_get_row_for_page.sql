--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_user_group_permissions_get_row_for_page]    Script Date: 1/14/2021 3:29:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 4/29/21
-- Description:	

/*

EXEC mgn_user_group_permissions_get_row_for_page  @user  = 'ciago', @action_result = 'TicketScheduling'

*/
-- =============================================
Create or ALTER       PROCEDURE [dbo].mgn_user_group_permissions_get_row_for_page
	@user nvarchar(60) = NULL,
	@action_result nvarchar(50)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @group_key uniqueidentifier = (SELECT ug._key FROM user_groups ug INNER JOIN users u ON u.user_highest_group_number = ug.user_group_hierarchy)

SELECT ugp._key as _key,
	   ug.user_group_name as group_name,
	   mi.menu_label_name as menu_name, 
	   mi.menu_is_parent,
	   mi.menu_is_child,
	   ugp.gp_view_permission, 
	   ugp.gp_update_permission,
	   ugp.gp_delete_inactivate_permisssion,
	   ugp.gp_special_permission,
	   ISNULL(mi.menu_special_permission_description, '') as menu_special_permission_description
FROM user_groups_permissions ugp INNER JOIN  
	 menu_items mi  ON mi._key = ugp.gp_menu_item_key  INNER JOIN
	 user_groups ug ON ug._key = ugp.gp_user_group_key  INNER JOIN 
	 menu_items parent ON parent.menu_number = mi.menu_parent_number_1
WHERE ugp.gp_user_group_key = @group_key
	  AND mi.menu_action_result_name = @action_result
	
END
