USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_tickets_calculate]    Script Date: 3/12/2021 11:12:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER   PROCEDURE [dbo].mgn_tickets_calculate
 @eod datetime , 
 @re_import_brandi bit = 0
	
AS	

/* 
declare @date datetime = DATETIMEFROMPARTS(2020, 05, 29, 7, 0, 0, 0)
EXEC mgn_tickets_calculate  @eod = @date  
								,@re_import_brandi  = 1
	
*/
BEGIN
declare @return_tables bit = 1 


SET @eod  = DATEADD(DAY, 1, SMALLDATETIMEFROMPARTS(YEAR(@eod), MONTH(@eod), DAY(@eod) , 07, 00))


EXEC daily_berth_activity_update @eod = @eod

IF @re_import_brandi = 1 
BEGIN
		DELETE FROM tickets WHERE tk_eod_datetime = @eod

		INSERT INTO tickets(
			_key, 
			tk_type, 
			tk_status, 
			tk_entity1_key, 
			tk_product, 
			tk_batch_id, 
			tk_eod_datetime, 
			tk_pipeline_key, 
			tk_destination_tank_key, 
			tk_scheduled_start_datetime, 
			tk_scheduled_end_datetime, 
			tk_start_datetime, 
			tk_end_datetime, 
			tk_day_sequence_number, 
			tk_volume_uom, 
			tk_scheduled_volume)


		SELECT  
			 newid(),--_key, 
			1 ,--tk_type, -- 1 pipeline, 2 vessel, 3 tank to tank
			CASE WHEN [calculated_meter_bbls_tov_based] IS NOT NULL THEN 4 ELSE 1 END,--tk_status, 
			o.tank_owner_key,--tk_shipper, 
			CASE WHEN o.tank_owner_key IN ('d25051f3-4888-4bfb-89fd-4e1449ac0fba', 'dd85132f-4039-4fda-8fcc-bbd2dd9b7656') 
					THEN '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0' ELSE 'aaf2e572-0a5a-483a-8bb0-41573e832ab7' END,--tk_product, 
			'',--tk_batch_id, 
			tt.eod_datetime,--tk_eod_datetime, 
			p._key,--tk_from_key, 
			t._key,--tk_to_key, 
			DATEADD(DAY, -1,tt.eod_datetime),--tk_scheduled_start_datetime, 
			tt.eod_datetime,--tk_scheduled_end_datetime, 
			DATEADD(DAY, -1,tt.eod_datetime),--tk_start_datetime, 
			tt.eod_datetime,--tk_end_datetime, 
			1,--tk_day_sequence_number, 
			1,--tk_volume_uom, 
			tt.brandis_value--tk_scheduled_volume, 
	
	
		FROM testing_pipeline_to_tank tt INNER JOIN 
			 tanks t ON t.tank_pi_name = tt.tank_name INNER JOIN 
			 tank_owner o ON o.tank_key = t._key INNER JOIN 
			 pipeline p ON p.short_code = tt.pipeline_name 
		WHERE tt.brandis_value <> 0 AND tt.eod_datetime = @eod



		----**********************************************VESSELS*********************************************


		INSERT INTO tickets(
			_key, 
			tk_type, 
			tk_status, 
			tk_entity1_key, 
			tk_product, 
			tk_batch_id, 
			tk_eod_datetime, 
			tk_source_tank_key, 
			tk_berth_key, 
			tk_scheduled_start_datetime, 
			tk_scheduled_end_datetime, 
			tk_start_datetime, 
			tk_end_datetime, 
			tk_day_sequence_number, 
			tk_volume_uom, 
			tk_scheduled_volume)


		SELECT  
			 newid(),--_key, 
			2 ,--tk_type, -- 1 pipeline, 2 vessel, 3 tank to tank
			CASE WHEN [calculated_meter_bbls_tov_based] IS NOT NULL THEN 4 ELSE 1 END,--tk_status, 
			o.tank_owner_key,--tk_shipper, 
			'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',--tk_product, 
			'',--tk_batch_id, 
			tt.eod_datetime,--tk_eod_datetime, 
			t._key,--tk_from_key, 
			b._key,--tk_to_key, 
			DATEADD(DAY, -1,tt.eod_datetime),--tk_scheduled_start_datetime, 
			tt.eod_datetime,--tk_scheduled_end_datetime, 
			NULL,--tk_start_datetime, 
			NULL,--tk_end_datetime, 
			1,--tk_day_sequence_number, 
			1,--tk_volume_uom, 
			(tt.brandis_value * -1)--tk_scheduled_volume, 
	
	
		FROM testing_tank_to_berth tt INNER JOIN 
			 tanks t ON t.tank_pi_name = tt.tank_name INNER JOIN 
			 tank_owner o ON o.tank_key = t._key INNER JOIN 
			 berths b ON tt.berth_name = b.berth_code 
		WHERE tt.brandis_value <> 0 AND tt.eod_datetime = @eod


		----**********************************************TRANSFERS*********************************************
		INSERT INTO tickets(
			_key, 
			tk_type, 
			tk_status, 
			tk_entity1_key, 
			tk_product, 
			tk_batch_id, 
			tk_eod_datetime, 
			tk_source_tank_key, 
			tk_destination_tank_key, 
			tk_scheduled_start_datetime, 
			tk_scheduled_end_datetime, 
			tk_start_datetime, 
			tk_end_datetime, 
			tk_day_sequence_number, 
			tk_volume_uom, 
			tk_scheduled_volume)


		SELECT  
			 newid(),--_key, 
			3 ,--tk_type, -- 1 pipeline, 2 vessel, 3 tank to tank
			4, --CASE WHEN [calculated_meter_bbls_tov_based] IS NOT NULL THEN 4 ELSE 1 END,--tk_status, 
			o.tank_owner_key,--tk_shipper, 
			'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',--tk_product, 
			'',--tk_batch_id, 
			tt.eod_datetime,--tk_eod_datetime, 
			ft._key,--tk_from_key, 
			tot._key,--tk_to_key, 
			DATEADD(DAY, -1,tt.eod_datetime),--tk_scheduled_start_datetime, 
			tt.eod_datetime,--tk_scheduled_end_datetime, 
			DATEADD(DAY, -1,tt.eod_datetime),--tk_start_datetime, 
			tt.eod_datetime,--tk_end_datetime, 
			1,--tk_day_sequence_number, 
			1,--tk_volume_uom, 
			tt.brandis_value--tk_scheduled_volume, 
	
		FROM testing_tank_transfers tt INNER JOIN 
			 tanks ft ON ft.tank_pi_name = tt.from_tank_name INNER JOIN 
			 tanks tot ON tot.tank_pi_name = tt.to_tank_name INNER JOIN 
			 tank_owner o ON o.tank_key = ft._key
		WHERE tt.brandis_value <> 0 --AND pt.pipeline_key <> '00000000-0000-0000-0000-000000000000'
			 AND eod_datetime = @eod
END

--********************************Ticket tables *****************************



-- remove old data from linked tables 
DELETE FROM tickets_meter WHERE tk_ticket_key IN (SELECT _key FROM tickets WHERE tk_eod_datetime = @eod)
DELETE FROM tickets_tank_gauge WHERE tk_ticket_key IN (SELECT _key FROM tickets WHERE tk_eod_datetime = @eod)
DELETE FROM tickets_tank_meter_tieback WHERE tk_ticket_key IN (SELECT _key FROM tickets WHERE tk_eod_datetime = @eod)

-- insert a row for tank gauge and tank meter tie back 


INSERT INTO tickets_tank_gauge
           (_key
           ,tk_ticket_key)
SELECT newid(), 
	  _key 
FROM tickets 
WHERE tk_eod_datetime = @eod

INSERT INTO tickets_tank_meter_tieback
	(_key, 
	 tk_ticket_key)
SELECT newid(), 
	  _key 
FROM tickets 
WHERE tk_eod_datetime = @eod


--*************************************************************************************
--*****************************PIPELINE************************************************
--*************************************************************************************


-- UPDATE TANK destination CLOSING TIMESTAMP
UPDATE tg 
SET [tk_tank_closing_timestamp_destination] = CASE WHEN CONVERT(TIME, tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN tk_eod_datetime
							  ELSE DATEADD(HOUR, sc.tank_settle_time_hours, tk_end_datetime) END
--SELECT t._key, tk_end_datetime, sc.tank_settle_time_hours,
--	   CASE WHEN CONVERT(TIME, tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN tk_eod_datetime
--							  ELSE DATEADD(HOUR, sc.tank_settle_time_hours, tk_end_datetime) END
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_gauge tg ON t._key = tg.tk_ticket_key INNER JOIN 
	 site_configuration sc ON sc.site_key = t.tk_site_key
WHERE tk_eod_datetime = @eod AND tt.type_use_tank_gauge = 1 AND t.tk_destination_tank_key IS NOT NULL 

-- update  tank readings  DEST TANK 
UPDATE tg
SET [tk_tank_opening_gross_destination] = ISNULL(tr_open.tank_gsv,0),
[tk_tank_opening_net_destination] = ISNULL(tr_open.tank_gsv,0),
[tk_tank_opening_level_destination] = ISNULL(tr_open.tank_level,0),
[tk_tank_closing_gross_destination] = ISNULL(tr_close.tank_gsv ,t.tk_scheduled_volume),
[tk_tank_closing_net_destination] = ISNULL(tr_close.tank_gsv ,t.tk_scheduled_volume),
[tk_tank_closing_level_destination] = ISNULL(tr_close.tank_level,0),
[tk_tank_source_key] = [tk_source_tank_key],
[tk_tank_destination_key] = [tk_destination_tank_key],
[tk_tank_ticket_id] = [tk_ticket_id],
[tk_tank_batch_start_date] = CONVERT(varchar, CONVERT(DATE, t.tk_start_datetime)),
[tk_tank_batch_start_time] = CONVERT(varchar, CONVERT(TIME, t.tk_start_datetime)),
[tk_tank_batch_end_date] = CONVERT(varchar, CONVERT(DATE, t.[tk_end_datetime])),
[tk_tank_batch_end_time] = CONVERT(varchar, CONVERT(TIME, t.[tk_end_datetime])),
[tk_tank_product_name] = '',
[tk_tank_batch_id] = t.tk_batch_id
--select t.tk_destination_tank_key, tr_open.tank_gsv as start_gsv, tr_close.tank_gsv as close_gsv, tr_close.tank_gsv - tr_open.tank_gsv as tank_gsv_diff, tanks.tank_description
from tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_gauge tg ON t._key = tg.tk_ticket_key LEFT OUTER JOIN
     tank_readings tr_open ON t.tk_destination_tank_key = tr_open.tank_key AND t.tk_start_datetime = tr_open.tank_timestamp LEFT OUTER JOIN
	 tank_readings tr_close ON tr_close.tank_key = t.tk_destination_tank_key AND  tg.tk_tank_closing_timestamp_destination = tr_close.tank_timestamp LEFT OUTER JOIN
	 tanks on tanks._key = t.tk_destination_tank_key INNER JOIN 
	 sites s ON s._key = t.tk_site_key --LEFT OUTER JOIN 
	-- products p on t.tk_product = p._key
WHERE tk_eod_datetime = @eod AND tt.type_use_tank_gauge = 1 AND t.tk_destination_tank_key IS NOT NULL


-- update the tank diff 
UPDATE tmt 
SET tk_tmt_tank_change_gsv = [tk_tank_closing_gross_destination] - [tk_tank_opening_gross_destination],
	tk_tmt_tank_destination_key = t.tk_destination_tank_key
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_gauge tg ON t._key = tg.tk_ticket_key INNER JOIN 
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod AND tt.[type_tank_meter_tieback] = 1 AND t.tk_destination_tank_key IS NOT NULL


-- get the sum of all tanks by pipeline 
UPDATE tmt 
SET tmt.tk_tmt_group_total_tank_diff = tmt2.tank_change_gsv
--SELECT t.tk_eod_datetime, t.tk_destination_tank_key,  tmt.tk_tmt_group_total_tank_diff, tmt2.tank_change_gsv
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key INNER JOIN 
	 ( SELECT SUM(tmt.tk_tmt_tank_change_gsv) as tank_change_gsv , t.tk_pipeline_key, tk_eod_datetime
	  FROM tickets t INNER JOIN 
		   ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
		   tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key 
	  WHERE t.tk_eod_datetime = @eod AND tt.[type_tank_meter_tieback] = 1 AND t.tk_pipeline_key IS NOT NULL
	  GROUP BY tk_eod_datetime, tk_pipeline_key
	  ) tmt2 ON tmt2.tk_pipeline_key = t.tk_pipeline_key AND tmt2.tk_eod_datetime = t.tk_eod_datetime



-- update the percent where the group total is not 0 (avoid divide by 0 error)
UPDATE tmt 
SET tmt.[tk_tmt_percent_tank_diff] = [tk_tmt_tank_change_gsv]/[tk_tmt_group_total_tank_diff]
--SELECT t.tk_eod_datetime, t.tk_destination_tank_key,  tmt.tk_tmt_group_total_tank_diff, tmt2.tank_change_gsv
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_tank_meter_tieback] = 1 
	  AND t.tk_pipeline_key IS NOT NULL 
	  AND [tk_tmt_group_total_tank_diff] <> 0


-- update percent to 0 if the group total is 0
UPDATE tmt 
SET tmt.[tk_tmt_percent_tank_diff] = 0
--SELECT t.tk_eod_datetime, t.tk_destination_tank_key,  tmt.tk_tmt_group_total_tank_diff, tmt2.tank_change_gsv
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_tank_meter_tieback] = 1 
	  AND t.tk_pipeline_key IS NOT NULL 
	  AND [tk_tmt_group_total_tank_diff] = 0


INSERT INTO [dbo].[tickets_meter]
		([_key],
		[tk_ticket_key],
		[tk_meter_key],
		[tk_meter_ticket_id],
		[tk_meter_id],
		[tk_meter_batch_start_date],
		[tk_meter_batch_start_time],
		[tk_meter_batch_end_date],
		[tk_meter_batch_end_time],
		[tk_meter_product_name],
		[tk_meter_opening_gross],
		[tk_meter_opening_net],
		[tk_meter_opening_nsv],
		[tk_meter_closing_gross],
		[tk_meter_closing_net],
		[tk_meter_closing_nsv])
select newid(), --[_key]
	   t._key, --[tk_ticket_key]
	   m._key, --[tk_meter_key]
	   t.tk_ticket_id, --[tk_meter_ticket_id]
	   m.meter_name, --[tk_meter_id]
	   [tk_tank_batch_start_date] = CONVERT(varchar, CONVERT(DATE, t.tk_start_datetime)), --[tk_meter_batch_start_date]
	   [tk_tank_batch_start_time] = CONVERT(varchar, CONVERT(TIME, t.tk_start_datetime)), -- [tk_meter_batch_start_time]
	   [tk_tank_batch_end_date] = CONVERT(varchar, CONVERT(DATE, t.[tk_end_datetime])), -- [tk_meter_batch_end_date]
	   [tk_tank_batch_end_time] = CONVERT(varchar, CONVERT(TIME, t.[tk_end_datetime])), -- [tk_meter_batch_end_time]
	   [tk_tank_product_name] = '', -- [tk_meter_product_name]
	   mr_open.meter_totalizer, --[tk_meter_opening_gross]
	   mr_open.meter_totalizer, --[tk_meter_opening_net]
	   mr_open.meter_totalizer, --[tk_meter_opening_nsv]
	   mr_close.meter_totalizer, --[tk_meter_closing_gross]
	   mr_close.meter_totalizer, --[tk_meter_closing_net]
	   mr_close.meter_totalizer --[tk_meter_closing_nsv]
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 pipeline p ON t.tk_pipeline_key = p._key INNER JOIN 
	 meters m ON m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key INNER JOIN 
	 meter_readings mr_open ON mr_open.meter_key = m._key AND mr_open.meter_timestamp = DATEADD(day, -1, t.tk_eod_datetime)   INNER JOIN 
	 meter_readings mr_close ON mr_close.meter_key = m._key AND mr_close.meter_timestamp = t.tk_eod_datetime
	 --LEFT OUTER JOIN 
	 --products pr on t.tk_product = pr._key
WHERE tk_eod_datetime = @eod AND tt.[type_use_meter] = 1

UPDATE tmt 
SET [tk_tmt_total_meter_diff] = pa_meter.meter_amt
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key INNER JOIN 
(SELECT DISTINCT t.tk_pipeline_key, pa.gsv_bbls as meter_amt
	  FROM pipeline_actuals pa INNER JOIN 
	  	 tickets t ON t.tk_pipeline_key = pa.pipeline_key
	  WHERE pa.eod_datetime = @eod AND t.tk_type = 1)pa_meter ON t.tk_pipeline_key = pa_meter.tk_pipeline_key
WHERE t.tk_eod_datetime = @eod
	AND tt.[type_tank_meter_tieback] = 1 
	AND t.tk_pipeline_key IS NOT NULL 


UPDATE tmt 
SET tmt.[tk_tmt_meter_volume] = [tk_tmt_total_meter_diff] * [tk_tmt_percent_tank_diff]
--SELECT t.tk_eod_datetime, t.tk_destination_tank_key,  tmt.tk_tmt_group_total_tank_diff, tmt2.tank_change_gsv
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_tank_meter_tieback] = 1 
	  AND t.tk_pipeline_key IS NOT NULL 


-- update final if using the tank gauge only 
UPDATE t 
SET [tk_final_gsv] = [tk_tank_closing_gross_destination] - [tk_tank_opening_gross_destination], 
	[tk_status] = 2 
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_gauge ttt ON t._key = ttt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_use_tank_gauge] = 1 AND tt.[type_tank_meter_tieback] = 0
	  AND t.tk_pipeline_key IS NOT NULL 
	  
-- update final if using the meter only 
UPDATE t
SET [tk_final_gsv] = tm.gsv_bbls , 
	[tk_status] = 2 
--SELECT t.tk_scheduled_volume, t.tk_pipeline_key, tm.gsv_bbls 
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 (SELECT t._key as ticket_key, SUM([tk_meter_closing_gross]) - SUM([tk_meter_opening_gross]) as gsv_bbls
	  FROM tickets t INNER JOIN 
	       tickets_meter tm ON t._key = tm.tk_ticket_key
	  WHERE t.tk_eod_datetime = @eod AND t.tk_pipeline_key IS NOT NULL
	  GROUP BY t._key ) as tm ON tm.ticket_key = t._key
WHERE tk_eod_datetime = @eod 
	  AND tt.type_use_meter = 1 AND tt.[type_tank_meter_tieback] = 0
	  AND t.tk_pipeline_key IS NOT NULL



-- update final if using the tank meter tieback 
UPDATE t 
SET [tk_final_gsv] = [tk_tmt_meter_volume], 
	[tk_status] = 2 
--SELECT t.tk_eod_datetime, t.tk_destination_tank_key,  tmt.tk_tmt_group_total_tank_diff, tmt2.tank_change_gsv
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_tank_meter_tieback] = 1 
	  AND t.tk_pipeline_key IS NOT NULL 




--*************************************************************************************
--***************************** TANK TO TANK TRANSFERS*********************************
--*************************************************************************************








-- UPDATE TANK SOURCE CLOSING TIMESTAMP
UPDATE tg 
SET [tk_tank_closing_timestamp_source] = CASE WHEN CONVERT(TIME, tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN tk_eod_datetime
							  ELSE DATEADD(HOUR, sc.tank_settle_time_hours, tk_end_datetime) END
--SELECT t._key, tk_end_datetime, sc.tank_settle_time_hours,
--	   CASE WHEN CONVERT(TIME, tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN tk_eod_datetime
--							  ELSE DATEADD(HOUR, sc.tank_settle_time_hours, tk_end_datetime) END
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_gauge tg ON t._key = tg.tk_ticket_key INNER JOIN 
	 site_configuration sc ON sc.site_key = t.tk_site_key
WHERE tk_eod_datetime = @eod AND tt.type_use_tank_gauge = 1 AND t.tk_source_tank_key IS NOT NULL 




-- update  tank readings  SOURCE TANK 
UPDATE tg
SET [tk_tank_opening_gross_source] = ISNULL(tr_open.tank_gsv,t.tk_scheduled_volume),
[tk_tank_opening_net_source] = ISNULL(tr_open.tank_gsv,t.tk_scheduled_volume),
[tk_tank_opening_level_source] = ISNULL(tr_open.tank_level,0),
[tk_tank_closing_gross_source] = ISNULL(tr_close.tank_gsv,0),
[tk_tank_closing_net_source] = ISNULL(tr_close.tank_gsv,0),
[tk_tank_closing_level_source] = ISNULL(tr_close.tank_level,0),
[tk_tank_source_key] = [tk_source_tank_key],
[tk_tank_destination_key] = [tk_destination_tank_key],
[tk_tank_ticket_id] = [tk_ticket_id],
[tk_tank_batch_start_date] = CONVERT(varchar, CONVERT(DATE, t.tk_start_datetime)),
[tk_tank_batch_start_time] = CONVERT(varchar, CONVERT(TIME, t.tk_start_datetime)),
[tk_tank_batch_end_date] = CONVERT(varchar, CONVERT(DATE, t.[tk_end_datetime])),
[tk_tank_batch_end_time] = CONVERT(varchar, CONVERT(TIME, t.[tk_end_datetime])),
[tk_tank_product_name] = '',
[tk_tank_batch_id] = t.tk_batch_id
--select t.tk_source_tank_key, tr_open.tank_gsv as start_gsv, tr_close.tank_gsv as close_gsv, tr_close.tank_gsv - tr_open.tank_gsv as tank_gsv_diff, tanks.tank_description
from tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_gauge tg ON t._key = tg.tk_ticket_key LEFT OUTER JOIN 
     tank_readings tr_open ON t.tk_source_tank_key = tr_open.tank_key AND t.tk_start_datetime = tr_open.tank_timestamp LEFT OUTER JOIN
	 tank_readings tr_close ON tr_close.tank_key = t.tk_source_tank_key AND  tg.tk_tank_closing_timestamp_source = tr_close.tank_timestamp LEFT OUTER JOIN 
	 tanks on tanks._key = t.tk_source_tank_key INNER JOIN 
	 sites s ON s._key = t.tk_site_key
	 --LEFT OUTER JOIN 
	 --products p on t.tk_product = p._key
WHERE tk_eod_datetime = @eod AND tt.type_use_tank_gauge = 1 AND t.tk_source_tank_key IS NOT NULL


-- update the tank diff -- SOURCE TANK 
UPDATE tmt 
SET tk_tmt_tank_change_gsv = ([tk_tank_closing_gross_source] - [tk_tank_opening_gross_source]) * -1 , 
	tk_tmt_tank_source_key = t.tk_source_tank_key, 
	tk_tmt_tank_destination_key = t.tk_destination_tank_key
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_gauge tg ON t._key = tg.tk_ticket_key INNER JOIN 
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_tank_meter_tieback] = 1 
	  AND t.tk_source_tank_key IS NOT NULL
	  AND tt.type_transfers_use_source_tank = 1


-- update the tank diff  -- DEST TANK 
UPDATE tmt 
SET tk_tmt_tank_change_gsv = ([tk_tank_closing_gross_destination] - [tk_tank_opening_gross_destination]), 
	tk_tmt_tank_source_key = t.tk_source_tank_key, 
	tk_tmt_tank_destination_key = t.tk_destination_tank_key
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_gauge tg ON t._key = tg.tk_ticket_key INNER JOIN 
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_tank_meter_tieback] = 1 
	  AND t.tk_source_tank_key IS NOT NULL
	  AND tt.type_transfers_use_dest_tank = 1

-- update the tank diff -- AVERAGE 
UPDATE tmt 
SET tk_tmt_tank_change_gsv = ((([tk_tank_closing_gross_source] - [tk_tank_opening_gross_source]) * -1)  + ([tk_tank_closing_gross_destination] - [tk_tank_opening_gross_destination])) /2, 
	tk_tmt_tank_source_key = t.tk_source_tank_key, 
	tk_tmt_tank_destination_key = t.tk_destination_tank_key
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_gauge tg ON t._key = tg.tk_ticket_key INNER JOIN 
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_tank_meter_tieback] = 1 
	  AND t.tk_source_tank_key IS NOT NULL
	  AND tt.type_transfers_use_avg_tank = 1


-- update the ticket gsv  
UPDATE t 
SET tk_final_gsv =  tk_tmt_tank_change_gsv, 
	tk_status = 2
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_gauge tg ON t._key = tg.tk_ticket_key INNER JOIN 
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.type_is_transfer = 1

--*************************************************************************************
--***************************** BARGES ************************************************
--*************************************************************************************
--SELECT SUM(berth_meter_bbls) as berth_meter_bbls, eod_datetime	 , berth_key, berth_start, berth_stop, berth_batch_id
--	FROM berth_activity 
--	WHERE eod_datetime = @eod
--	GROUP BY eod_datetime, berth_key,berth_start, berth_stop, berth_batch_id

-- update the total meter for the berth by batch id 
UPDATE tmt
SET tk_tmt_total_meter_diff = (pa.berth_meter_bbls )
--SELECT tk.total_tank_diff_nsv, pa.tov_bbls
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key INNER JOIN
	(SELECT SUM(berth_meter_bbls) as berth_meter_bbls, eod_datetime	 , berth_key, berth_start, berth_stop, berth_batch_id
	FROM berth_activity 
	WHERE eod_datetime = @eod
	GROUP BY eod_datetime, berth_key,berth_start, berth_stop, berth_batch_id) pa ON t.tk_eod_datetime = pa.eod_datetime AND t.tk_berth_key = pa.berth_key AND tk_batch_id = pa.berth_batch_id
WHERE t.tk_eod_datetime = @eod  AND tt.type_is_vessel = 1


-- update the start and end times based on the berth activity record 
UPDATE t
SET tk_start_datetime = pa.berth_start, 
	tk_end_datetime = pa.berth_stop
--SELECT tk.total_tank_diff_nsv, pa.tov_bbls
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key INNER JOIN
	(SELECT SUM(berth_meter_bbls) as berth_meter_bbls, eod_datetime	 , berth_key, berth_start, berth_stop, berth_batch_id
	FROM berth_activity 
	WHERE eod_datetime = @eod
	GROUP BY eod_datetime, berth_key,berth_start, berth_stop, berth_batch_id) pa ON t.tk_eod_datetime = pa.eod_datetime AND t.tk_berth_key = pa.berth_key AND tk_batch_id = pa.berth_batch_id
WHERE t.tk_eod_datetime = @eod  AND tt.type_is_vessel = 1

--SELECT tk_source_tank_key, tk_destination_tank_key, tk_start_datetime, tk_end_datetime, tk_eod_datetime FROM tickets WHERE tk_eod_datetime = @eod  AND tk_type <> 2 AND 
--			(tk_source_tank_key IN ((SELECT tk_source_tank_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ) OR 
--			 tk_destination_tank_key IN ((SELECT tk_source_tank_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ))



-- Updates the start and end time based on any other movements involving that tank in the same day 
--SELECT t.tk_start_datetime, t.tk_end_datetime, t.tk_from_key, t.tk_to_key, t2.tk_start_datetime, t2.tk_end_datetime, t2.tk_from_key, t2.tk_to_key, 
UPDATE t
SET tk_start_datetime = CASE WHEN t2.tk_end_datetime > t.tk_start_datetime AND t2.tk_end_datetime <  t.tk_end_datetime THEN t2.tk_end_datetime ELSE t.tk_start_datetime END ,
	tk_end_datetime = CASE WHEN t2.tk_start_datetime < t.tk_end_datetime AND t2.tk_start_datetime >  t.tk_start_datetime THEN t2.tk_start_datetime ELSE t.tk_end_datetime END 
	   --,   CASE WHEN t2.tk_start_datetime < t.tk_end_datetime THEN t2.tk_start_datetime ELSE t.tk_end_datetime END as new_start 
FROM 
tickets t INNER JOIN 
(SELECT tk_source_tank_key, tk_destination_tank_key, tk_start_datetime, tk_end_datetime, tk_eod_datetime FROM tickets WHERE tk_eod_datetime = @eod  AND tk_type <> 2 AND 
			(tk_source_tank_key IN ((SELECT tk_source_tank_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ) OR 
			 tk_destination_tank_key IN ((SELECT tk_source_tank_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ))) t2 ON t.tk_eod_datetime = t2.tk_eod_datetime AND (t.tk_source_tank_key = t2.tk_source_tank_key OR t.tk_source_tank_key = t2.tk_destination_tank_key)
WHERE t.tk_eod_datetime = @eod  AND t.tk_type = 2


-- Update the tank close timestamp 
--SELECT t.tk_start_datetime, t.tk_end_datetime, t.tk_from_key, t.tk_to_key, t2.tk_start_datetime, t2.tk_end_datetime, t2.tk_from_key, t2.tk_to_key, 
UPDATE ttg
SET tk_tank_closing_timestamp_source =  CASE WHEN t.tk_end_datetime = t2.tk_start_datetime THEN t.tk_end_datetime
								  WHEN CONVERT(TIME, t.tk_end_datetime) BETWEEN '06:00' AND '07:00' THEN t.tk_eod_datetime
								  ELSE DATEADD(HOUR, sc.tank_settle_time_hours, t.tk_end_datetime) END
FROM 
tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key INNER JOIN
	 tickets_tank_gauge ttg ON t._key = ttg.tk_ticket_key INNER JOIN 
	 site_configuration sc ON t.[tk_site_key] = sc.[site_key] INNER JOIN 
(SELECT tk_source_tank_key, tk_destination_tank_key, tk_start_datetime, tk_end_datetime, tk_eod_datetime FROM tickets WHERE tk_eod_datetime = @eod  AND tk_type <> 2 AND 
			(tk_source_tank_key IN ((SELECT tk_source_tank_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ) OR 
			 tk_destination_tank_key IN ((SELECT tk_source_tank_key FROM tickets WHERE tk_eod_datetime = @eod AND tk_type = 2) ))) t2 ON t.tk_eod_datetime = t2.tk_eod_datetime AND (t.tk_source_tank_key = t2.tk_source_tank_key OR t.tk_source_tank_key = t2.tk_destination_tank_key)
WHERE t.tk_eod_datetime = @eod  AND t.tk_type = 2  



-- update  tank readings  SOURCE TANK 
UPDATE tg
SET [tk_tank_opening_gross_source] = ISNULL(tr_open.tank_gsv,t.tk_scheduled_volume),
[tk_tank_opening_net_source] = ISNULL(tr_open.tank_gsv,t.tk_scheduled_volume),
[tk_tank_opening_level_source] = ISNULL(tr_open.tank_level,0),
[tk_tank_closing_gross_source] = ISNULL(tr_close.tank_gsv,0),
[tk_tank_closing_net_source] = ISNULL(tr_close.tank_gsv,0),
[tk_tank_closing_level_source] = ISNULL(tr_close.tank_level,0),
[tk_tank_source_key] = [tk_source_tank_key],
[tk_tank_destination_key] = [tk_destination_tank_key],
[tk_tank_ticket_id] = [tk_ticket_id],
[tk_tank_batch_start_date] = CONVERT(varchar, CONVERT(DATE, t.tk_start_datetime)),
[tk_tank_batch_start_time] = CONVERT(varchar, CONVERT(TIME, t.tk_start_datetime)),
[tk_tank_batch_end_date] = CONVERT(varchar, CONVERT(DATE, t.[tk_end_datetime])),
[tk_tank_batch_end_time] = CONVERT(varchar, CONVERT(TIME, t.[tk_end_datetime])),
[tk_tank_product_name] = '',
[tk_tank_batch_id] = t.tk_batch_id
--select t.tk_source_tank_key, tr_open.tank_gsv as start_gsv, tr_close.tank_gsv as close_gsv, tr_close.tank_gsv - tr_open.tank_gsv as tank_gsv_diff, tanks.tank_description
from tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_gauge tg ON t._key = tg.tk_ticket_key LEFT OUTER JOIN
     tank_readings tr_open ON t.tk_source_tank_key = tr_open.tank_key AND t.tk_start_datetime = tr_open.tank_timestamp LEFT OUTER JOIN
	 tank_readings tr_close ON tr_close.tank_key = t.tk_source_tank_key AND  tg.tk_tank_closing_timestamp_source = tr_close.tank_timestamp LEFT OUTER JOIN 
	 tanks on tanks._key = t.tk_source_tank_key INNER JOIN 
	 sites s ON s._key = t.tk_site_key --LEFT OUTER JOIN 
	 --products p on t.tk_product = p._key
WHERE tk_eod_datetime = @eod AND tt.type_use_tank_gauge = 1 AND t.tk_source_tank_key IS NOT NULL 
	  AND   t.tk_type = 2  






-- update the tank diff 
UPDATE tmt 
SET tk_tmt_tank_change_gsv =   [tk_tank_opening_gross_source] - [tk_tank_closing_gross_source],
	tk_tmt_tank_source_key = t.tk_source_tank_key
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 tickets_tank_gauge tg ON t._key = tg.tk_ticket_key INNER JOIN 
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod AND tt.[type_tank_meter_tieback] = 1 AND t.tk_source_tank_key IS NOT NULL
       AND   t.tk_type = 2  


-- get the sum of all tanks by berth and batch id  
UPDATE tmt 
SET tmt.tk_tmt_group_total_tank_diff = tmt2.tank_change_gsv
--SELECT t.tk_eod_datetime, t.tk_destination_tank_key,  tmt.tk_tmt_group_total_tank_diff, tmt2.tank_change_gsv
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key INNER JOIN 
	 ( SELECT SUM(tmt.tk_tmt_tank_change_gsv) as tank_change_gsv , t.tk_berth_key, tk_eod_datetime, tk_batch_id
	  FROM tickets t INNER JOIN 
		   ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
		   tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key 
	  WHERE t.tk_eod_datetime = @eod AND tt.[type_tank_meter_tieback] = 1 AND t.tk_berth_key IS NOT NULL
	  GROUP BY tk_eod_datetime, tk_berth_key, tk_batch_id
	  ) tmt2 ON tmt2.tk_berth_key = t.tk_berth_key AND tmt2.tk_eod_datetime = t.tk_eod_datetime AND tmt2.tk_batch_id = t.tk_batch_id



-- update the percent where the group total is not 0 (avoid divide by 0 error)
UPDATE tmt 
SET tmt.[tk_tmt_percent_tank_diff] = [tk_tmt_tank_change_gsv]/[tk_tmt_group_total_tank_diff]
--SELECT t.tk_eod_datetime, t.tk_destination_tank_key,  tmt.tk_tmt_group_total_tank_diff, tmt2.tank_change_gsv
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_tank_meter_tieback] = 1 
	  AND t.tk_berth_key IS NOT NULL 
	  AND [tk_tmt_group_total_tank_diff] <> 0


-- update percent to 0 if the group total is 0
UPDATE tmt 
SET tmt.[tk_tmt_percent_tank_diff] = 0
--SELECT t.tk_eod_datetime, t.tk_destination_tank_key,  tmt.tk_tmt_group_total_tank_diff, tmt2.tank_change_gsv
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_tank_meter_tieback] = 1 
	  AND t.tk_berth_key IS NOT NULL 
	  AND [tk_tmt_group_total_tank_diff] = 0


INSERT INTO [dbo].[tickets_meter]
		([_key],
		[tk_ticket_key],
		[tk_meter_key],
		[tk_meter_ticket_id],
		[tk_meter_id],
		[tk_meter_batch_start_date],
		[tk_meter_batch_start_time],
		[tk_meter_batch_end_date],
		[tk_meter_batch_end_time],
		[tk_meter_product_name],
		[tk_meter_opening_gross],
		[tk_meter_opening_net],
		[tk_meter_opening_nsv],
		[tk_meter_closing_gross],
		[tk_meter_closing_net],
		[tk_meter_closing_nsv])
select newid(), --[_key]
	   t._key, --[tk_ticket_key]
	   vam.meter_key, --[tk_meter_key]
	   t.tk_ticket_id, --[tk_meter_ticket_id]
	   m.meter_name, --[tk_meter_id]
	   [tk_tank_batch_start_date] = CONVERT(varchar, CONVERT(DATE, t.tk_start_datetime)), --[tk_meter_batch_start_date]
	   [tk_tank_batch_start_time] = CONVERT(varchar, CONVERT(TIME, t.tk_start_datetime)), -- [tk_meter_batch_start_time]
	   [tk_tank_batch_end_date] = CONVERT(varchar, CONVERT(DATE, t.[tk_end_datetime])), -- [tk_meter_batch_end_date]
	   [tk_tank_batch_end_time] = CONVERT(varchar, CONVERT(TIME, t.[tk_end_datetime])), -- [tk_meter_batch_end_time]
	   [tk_tank_product_name] = '', -- [tk_meter_product_name]
	   vam.open_totalizer, --[tk_meter_opening_gross]
	   vam.open_totalizer, --[tk_meter_opening_net]
	   vam.open_totalizer, --[tk_meter_opening_nsv]
	   vam.close_totalizer, --[tk_meter_closing_gross]
	   vam.close_totalizer, --[tk_meter_closing_net]
	   vam.close_totalizer --[tk_meter_closing_nsv]
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 valve_activity va ON va.valve_skid_batch_id = t.tk_batch_id AND va.valve_eod_datetime = t.tk_eod_datetime INNER JOIN 
	 valve_activity_meter vam ON vam.valve_activity_key = va._key INNER JOIN 
	-- products pr on t.tk_product = pr._key INNER JOIN 
	 meters m ON m._key = vam.meter_key
WHERE tk_eod_datetime = @eod AND tt.[type_use_meter] = 1 
	  AND t.tk_berth_key IS NOT NULL 




UPDATE tmt 
SET tmt.[tk_tmt_meter_volume] = [tk_tmt_total_meter_diff] * [tk_tmt_percent_tank_diff]
--SELECT t.tk_eod_datetime, t.tk_destination_tank_key,  tmt.tk_tmt_group_total_tank_diff, tmt2.tank_change_gsv
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_tank_meter_tieback] = 1 
	  AND t.tk_berth_key IS NOT NULL 


-- update final if using the tank gauge only 
UPDATE t 
SET [tk_final_gsv] =  [tk_tank_opening_gross_source] - [tk_tank_closing_gross_source] , 
	[tk_status] = 2 
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_gauge ttt ON t._key = ttt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_use_tank_gauge] = 1 AND tt.[type_tank_meter_tieback] = 0
	  AND t.tk_berth_key IS NOT NULL 
	  
-- update final if using the meter only 
UPDATE t
SET [tk_final_gsv] = tm.gsv_bbls , 
	[tk_status] = 2 
--SELECT t.tk_scheduled_volume, t.tk_pipeline_key, tm.gsv_bbls 
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 (SELECT t._key as ticket_key, SUM([tk_meter_closing_gross]) - SUM([tk_meter_opening_gross]) as gsv_bbls
	  FROM tickets t INNER JOIN 
	       tickets_meter tm ON t._key = tm.tk_ticket_key
	  WHERE t.tk_eod_datetime = @eod AND t.tk_berth_key IS NOT NULL
	  GROUP BY t._key ) as tm ON tm.ticket_key = t._key
WHERE tk_eod_datetime = @eod 
	  AND tt.type_use_meter = 1 AND tt.[type_tank_meter_tieback] = 0
	  AND t.tk_berth_key IS NOT NULL



-- update final if using the tank meter tieback 
UPDATE t 
SET [tk_final_gsv] = [tk_tmt_meter_volume], 
	[tk_status] = 2 
--SELECT t.tk_eod_datetime, t.tk_destination_tank_key,  tmt.tk_tmt_group_total_tank_diff, tmt2.tank_change_gsv
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN  
	 tickets_tank_meter_tieback tmt ON t._key = tmt.tk_ticket_key
WHERE tk_eod_datetime = @eod 
	  AND tt.[type_tank_meter_tieback] = 1 
	  AND t.tk_berth_key IS NOT NULL 




END