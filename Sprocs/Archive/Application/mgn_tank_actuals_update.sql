USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_actuals_update]    Script Date: 11/30/2020 12:14:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected tank_actuals with new user input details.

/*EXEC mgn_tank_actuals_update 
	@row = 'F6DA37F0-689D-47A9-84AD-B4EAC7CC7889',
	@gsv_bbls = 185115.328125,
	@open_totalizer = 145797.515625,
	@close_totalizer = 330912.84375

	185115.328125
	145797.515625
	330912.84375
	
	// 
	*/
-- =============================================
ALTER         PROCEDURE [dbo].[mgn_tank_actuals_update]

	@row uniqueidentifier,
    @site_key uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 
	@tank_gsv_change float,
	@open_gsv float,
	@close_gsv float
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from tank_actuals
--Where _key = @row


UPDATE tank_actuals

	SET --tank_gsv_change = @tank_gsv_change,
		open_gsv = @open_gsv,
		close_gsv = @close_gsv
		
WHERE _key = @row


UPDATE tank_actuals
SET tank_gsv_change = open_gsv - close_gsv
WHERE _key = @row

END
