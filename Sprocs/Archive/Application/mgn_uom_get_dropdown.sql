USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_volume_types_get_dropdown]    Script Date: 1/11/2021 4:18:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
create or ALTER           PROCEDURE [dbo].[mgn_uom_get_dropdown] 
	@site_key uniqueidentifier = null, 
	@user varchar(50) = 'default',
	@uom_type int -- The integers received from VS correspond to the position of the columns in the uom table

/*
Execute [mgn_uom_get_dropdown] @uom_type = 1 -- volume
Execute [mgn_uom_get_dropdown] @uom_type = 2 -- mass
Execute [mgn_uom_get_dropdown] @uom_type = 3 -- temp
Execute [mgn_uom_get_dropdown] @uom_type = 4 -- desnity
Execute [mgn_uom_get_dropdown] @uom_type = 5 -- gravity
Execute [mgn_uom_get_dropdown] @uom_type = 6 -- pressure
Execute [mgn_uom_get_dropdown] @uom_type = 7 -- currency
*/

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
IF(@site_key IS NULL) 
BEGIN 
    SET @site_key = dbo.get_default_site();
END
if (@uom_type = 1) -- user wants volume units
begin
	SELECT 0 AS RowKey, 'Select Units' As description
	UNION
	SELECT uom_uom as RowKey, [uom_abbreviation] as description
	FROM [dbo].[uom]
	WHERE uom.uom_is_volume = 1 and
		  uom.site_key = @site_key and
		  uom.deleted = 0
	ORDER BY RowKey
end

if (@uom_type = 2) -- user wants mass units
begin
	SELECT 0 AS RowKey, 'Select Units' As description
	UNION
	SELECT uom_uom as RowKey, [uom_abbreviation] as description
	FROM [dbo].[uom]
	WHERE uom.uom_is_mass = 1 and
		  uom.site_key = @site_key and
		  uom.deleted = 0
	ORDER BY RowKey
end

if (@uom_type = 3) -- user wants temperature units
begin
	SELECT 0 AS RowKey, 'Select Units' As description
	UNION
	SELECT uom_uom as RowKey, [uom_abbreviation] as description
	FROM [dbo].[uom]
	WHERE uom.uom_is_temperature = 1 and
		  uom.site_key = @site_key and
		  uom.deleted = 0
	ORDER BY RowKey
end

if (@uom_type = 4) -- user wants density units
begin
	SELECT 0 AS RowKey, 'Select Units' As description
	UNION
	SELECT uom_uom as RowKey, [uom_abbreviation] as description
	FROM [dbo].[uom]
	WHERE uom.uom_is_density = 1 and
		  uom.site_key = @site_key and
		  uom.deleted = 0
	ORDER BY RowKey
end

if (@uom_type = 5) -- user wants gravity units
begin
	SELECT 0 AS RowKey, 'Select Units' As description
	UNION
	SELECT uom_uom as RowKey, [uom_abbreviation] as description
	FROM [dbo].[uom]
	WHERE uom.uom_is_gravity = 1 and
		  uom.site_key = @site_key and
		  uom.deleted = 0
	ORDER BY RowKey
end

if (@uom_type = 6) -- user wants pressure units
begin
	SELECT 0 AS RowKey, 'Select Units' As description
	UNION
	SELECT uom_uom as RowKey, [uom_abbreviation] as description
	FROM [dbo].[uom]
	WHERE uom.uom_is_pressure = 1 and
		  uom.site_key = @site_key and
		  uom.deleted = 0
	ORDER BY RowKey
end

if (@uom_type = 7) -- user wants currency units
begin
	SELECT 0 AS RowKey, 'Select Units' As description
	UNION
	SELECT uom_uom as RowKey, [uom_abbreviation] as description
	FROM [dbo].[uom]
	WHERE uom.uom_is_currency = 1 and
		  uom.site_key = @site_key and
		  uom.deleted = 0
	ORDER BY RowKey
end

END


-- EXEC [mgn_accounting_volume_types_get_list]





