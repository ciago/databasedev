--delete from pipeline_allocations
--USE [Moda]
--GO
--/****** Object:  StoredProcedure [dbo].[mgn_tank_overview_get]    Script Date: 5/1/2020 7:35:46 AM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		Candice Iago	
---- Create date: 6/26/20
---- Description:	Gets all of the tank overview records for the month

/*EXEC mgn_tank_overview_get 
	@month = 1,
	@year = 2021 
	,@product_key  =null -- '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0'
	,@product_key = null --'86c0cacb-b542-49c8-b352-8be35f276541'
	*/
---- =============================================

CREATE OR ALTER   PROCEDURE [dbo].mgn_tank_overview_get
-- 	@pipeline_key uniqueidentifier , 
--DECLARE 
	@month int , 
	@year int , 
	@product_key uniqueidentifier =null -- '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0'  --,
	,@entity_key uniqueidentifier = null --'86c0cacb-b542-49c8-b352-8be35f276541'
	,@site_key uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @month_start_datetime DATETIME = DATETIMEFROMPARTS(@year, @month, 1, 7 , 0, 0, 0)
DECLARE @month_end_datetime DATETIME = DATETIMEFROMPARTS(@year, (@month + 1) , 1, 7 , 0, 0, 0)

	-- fill the tmp table with all pipeline schedules by product by hour
EXEC mgn_pipeline_schedule_fill_hourly
	@month = @month,
	@year  = @year


	-- fill the tmp table with all pipeline schedules by product by hour
EXEC mgn_pipeline_schedule_fill_daily
	@month = @month,
	@year  = @year


-- create temp table for return data 
DECLARE @results TABLE ( [eod_datetime] datetime,  total_pipeline_nominated int, total_pipeline_allocated int,
							total_pipeline_assigned int, total_pipeline_actual int, total_berth_nominated int, total_berth_assigned int, total_berth_actual int,
							product_key uniqueidentifier, entity_key uniqueidentifier)

-- insert a row for every day of the month 
IF(@product_key IS NULL)
BEGIN
	INSERT INTO @results ([eod_datetime],  product_key)
	SELECT DISTINCT  eod_datetime,  NULL
	FROM pipeline_allocations 
	WHERE eod_datetime BETWEEN @month_start_datetime AND @month_end_datetime
END
ELSE
BEGIN
	INSERT INTO @results ([eod_datetime],  product_key)
	SELECT DISTINCT  eod_datetime,  product_key
	FROM pipeline_allocations 
	WHERE eod_datetime BETWEEN @month_start_datetime AND @month_end_datetime
			AND product_key = @product_key 
END
			

-- UPDATE THE ALLOCATED AMOUNT TO Equal the total allocated for each day 
IF(@entity_key IS NULL)
BEGIN
	--SELECT total_pipeline_allocated.allocated_amount, re.eod_datetime
	UPDATE re 
	SET total_pipeline_allocated = ISNULL(total_pipeline_allocated.allocated_amount, 0)
	FROM 
	(SELECT SUM(allocated_amount)as allocated_amount, eod_datetime
	 FROM pipeline_allocations
	 WHERE (product_key = @product_key OR @product_key IS NULL) 
			AND  eod_datetime BETWEEN @month_start_datetime AND @month_end_datetime
	 GROUP BY eod_datetime) total_pipeline_allocated INNER JOIN 
	 @results re ON re.eod_datetime = total_pipeline_allocated.eod_datetime
END
ELSE
BEGIN
	UPDATE re 
	SET total_pipeline_allocated = ISNULL(total.allocated_amount, 0)
	FROM 
	(SELECT SUM(allocated_amount)as allocated_amount, entity_key, eod_datetime
	 FROM pipeline_allocations
	 WHERE (product_key = @product_key OR @product_key IS NULL) AND entity_key = @entity_key
			AND  eod_datetime BETWEEN @month_start_datetime AND @month_end_datetime
	 GROUP BY entity_key, eod_datetime) total INNER JOIN 
	 @results re ON re.eod_datetime = total.eod_datetime
END

-- this is the daily pipeline nominated amount (not allocated by shipper yet, only distinct by poduct ) 
update r
SET  r.total_pipeline_nominated = ISNULL(ROUND(d.volume,0),0)
FROM (
	SELECT CONVERT(Date, start_datetime) as [date], ISNULL(AVG(NULLIF(rate,0)),0) as rate, SUM(volume) as volume,  end_datetime
	FROM tmp_pipeline_by_day d 
	WHERE  (product_key = @product_key OR @product_key IS NULL)
	GROUP BY start_datetime,  end_datetime)d INNER JOIN 
	@results r ON d.end_datetime = r.eod_datetime 


--pipeline tickets - update the assigned based on the scheduled volume total in the tickets table 
UPDATE r 
SET total_pipeline_assigned = ISNULL(pipeline_assigned,0)
FROM 
	@results r INNER JOIN 
	(SELECT tk_eod_datetime, SUM(tk_scheduled_volume) as pipeline_assigned FROM tickets 
	 WHERE tk_status <> 7 -- not voided
	  AND tk_type = 1 --pipeline
	  AND (tk_entity1_key = @entity_key OR @entity_key IS NULL)
	  AND (tk_product = @product_key OR @product_key IS NULL)
	  AND tk_eod_datetime BETWEEN @month_start_datetime AND @month_end_datetime
	 GROUP BY tk_eod_datetime) t ON t.tk_eod_datetime = r.eod_datetime
--get the total_pipeline_allocated pipeline assigned per day


--pipeline tickets -  update the final  based on the final gsv volume total in the tickets table 
UPDATE r 
SET total_pipeline_actual = ISNULL(pipeline_final_gsv,0)
FROM 
	@results r INNER JOIN 
	(SELECT tk_eod_datetime, SUM(ISNULL(tk_final_gsv,0)) as pipeline_final_gsv FROM tickets 
	 WHERE tk_status <> 7 -- not voided
	  AND tk_type = 1 --pipeline
	  AND (tk_entity1_key = @entity_key OR @entity_key IS NULL)
	  AND (tk_product = @product_key OR @product_key IS NULL)
	  AND tk_eod_datetime BETWEEN @month_start_datetime AND @month_end_datetime
	 GROUP BY tk_eod_datetime) t ON t.tk_eod_datetime = r.eod_datetime
--get the total_pipeline_allocated pipeline assigned per day

--berth tickets
UPDATE r 
SET total_berth_assigned = ISNULL(vessel_assigned,0)
--SELECT SUM(tk_scheduled_volume)
FROM 
	@results r INNER JOIN 
	(SELECT tk_eod_datetime, SUM(tk_scheduled_volume) as vessel_assigned FROM tickets 
	 WHERE tk_status <> 7 -- not voided
	  AND tk_type = 2 --vessel
	  AND (tk_entity1_key = @entity_key OR @entity_key IS NULL)
	  AND (tk_product = @product_key OR @product_key IS NULL)
	  AND tk_eod_datetime BETWEEN @month_start_datetime AND @month_end_datetime
	 GROUP BY tk_eod_datetime) t ON t.tk_eod_datetime = r.eod_datetime


--UPDATE r
--SET total_berth_nominated = syn_qty
--FROM @results r INNER JOIN 
--(select SUM(syn_nominated_quantity)  as syn_qty, dbo.get_eod_datetime(syn_vessel_eta) as eod_datetime, syn_vessel_eta
--FROM synthesis_tickets s INNER JOIN 
--	 entities e ON s.syn_shipper_id = e.external_id INNER JOIN 
--	 products p ON p.product_external_id = s.syn_product_code
--WHERE syn_vessel_lay_status <> 'REJECTED' 
--	  and (e._key= @entity_key OR @entity_key IS NULL)
--	  AND (p._key = @product_key OR @product_key IS NULL)
--GROUP BY dbo.get_eod_datetime(syn_vessel_eta), syn_vessel_eta) syn ON r.eod_datetime = syn.eod_datetime 


UPDATE r
SET total_berth_nominated = ISNULL(projected_volume,0)
FROM @results r INNER JOIN 
-- qty, eod, (grouping on eod ) 
(select SUM(projected_volume) as projected_volume, eod
FROM 
(select vp.projected_volume as projected_volume,  dbo.get_eod_datetime( DATEADD(day, vp.day_number - 1, st.syn_vessel_eta)) as eod --DATEADD(day, vp.day_number - 1, st.syn_vessel_eta) as eta,
FROM vessel_projected_volumes vp INNER JOIN 
	synthesis_tickets st ON vp.synthesis_key = st._key INNER JOIN 
	 entities e ON st.syn_shipper_id = e.external_id INNER JOIN 
	 products p ON p.product_external_id = st.syn_product_code
WHERE syn_vessel_lay_status <> 'REJECTED' 
	  and (e._key= @entity_key OR @entity_key IS NULL)
	  AND (p._key = @product_key OR @product_key IS NULL)
--GROUP BY dbo.get_eod_datetime(syn_vessel_eta)
) daily
GROUP BY eod) syn ON r.eod_datetime = syn.eod



--pipeline tickets -  update the final  based on the final gsv volume total in the tickets table 
UPDATE r 
SET total_berth_actual = ISNULL(berth_final_gsv,0)
FROM 
	@results r INNER JOIN 
	(SELECT tk_eod_datetime, SUM(ISNULL(tk_final_gsv,0)) as berth_final_gsv FROM tickets 
	 WHERE tk_status <> 7 -- not voided
	  AND tk_type = 2 --pipeline
	  AND (tk_entity1_key = @entity_key OR @entity_key IS NULL)
	  AND (tk_product = @product_key OR @product_key IS NULL)
	  AND tk_eod_datetime BETWEEN @month_start_datetime AND @month_end_datetime
	 GROUP BY tk_eod_datetime) t ON t.tk_eod_datetime = r.eod_datetime
--get the total_pipeline_allocated pipeline assigned per day


UPDATE @results
SET total_pipeline_nominated = 0 
WHERE total_pipeline_nominated IS NULL

UPDATE @results
SET total_pipeline_allocated = 0 
WHERE total_pipeline_allocated IS NULL

UPDATE @results
SET total_pipeline_assigned = 0 
WHERE total_pipeline_assigned IS NULL

UPDATE @results
SET total_pipeline_actual = 0 
WHERE total_pipeline_actual IS NULL

UPDATE @results
SET total_berth_nominated = 0 
WHERE total_berth_nominated IS NULL

UPDATE @results
SET total_berth_assigned = 0 
WHERE total_berth_assigned IS NULL

UPDATE @results
SET total_berth_actual = 0 
WHERE total_berth_actual IS NULL



select  DATEADD(DAY, -1, eod_datetime) as [Date],
	    ISNULL(total_pipeline_nominated,0) as [Pipeline Nominated],
		ISNULL(total_pipeline_allocated,0) as [Pipeline Allocated],
		ISNULL(total_pipeline_assigned,0) as [Pipeline Assigned],
		ISNULL(total_pipeline_actual,0) as [Pipeline Actual],
		ISNULL(total_berth_nominated,0) as [Berth Nominated],
		ISNULL(total_berth_assigned,0) as [Berth Assigned], 
		ISNULL(total_berth_actual,0) as [Berth Actual], 
		eod_datetime as [Edit]

from @results

END

Go 

EXEC mgn_tank_overview_get 
	@month = 6,
	@year = 2020 
	,@product_key  =  '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0'-- WTI
	,@entity_key = 'd25051f3-4888-4bfb-89fd-4e1449ac0fba' -- vitol
	,@site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'

GO