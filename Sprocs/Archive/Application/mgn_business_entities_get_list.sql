--USE [Hartree_TMS]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_business_entities_get_list]    Script Date: 4/9/2021 1:58:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/18/2021
-- Description:		Gets a list of business entities from a selected entity type and active status.

/*EXEC mgn_business_entities_get_list  
	@type = 0,
	@active = 1
	
    */	
-- =============================================
ALTER        PROCEDURE [dbo].[mgn_business_entities_get_list]

	@type int = 0,
	@active bit
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from entities


SELECT			e._key,
				e.name as [name],
				e.short_name as [short_name],
				et.type_description as [entity_type_desc],
				e.type as [type],
				e.active as [active],
				isnull(pt.[pt_description],'') as [payment_terms],
				ISNULL(e.fein,'') as [fein]
FROM			entities AS e INNER JOIN
				entity_types AS et ON et.type_code = e.type left outer join
				payment_terms as pt on e.payment_terms = pt.pt_terms
WHERE			(e.type = @type OR @type = 0)
				AND	e.active = @active

ORDER BY		name
	  
END
