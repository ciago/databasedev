USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_sync_synthesis_tank_owner]    Script Date: 1/20/2021 4:12:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER   PROCEDURE [dbo].[mgn_sync_synthesis_tank_owner]
 
	
AS
/* 
EXEC mgn_sync_synthesis_tanks
	
*/
BEGIN

-- insert new records 

INSERT INTO [dbo].[tank_owner]
           ([_key]
           ,[tank_key]
           ,[tank_owner_key]
           ,[tk_start_datetime]
           ,[tk_end_datetime]
           ,[capacity_for_billing]
           ,[external_row_id])
SELECT newid(), 
	   mt._key as moda_tank_key, 
	   e._key as entity_key,
	   tc.[EFFECTIVE_DATE],
	   NULL,
	   tc.[SPECIFIC_CAPACITY],
	    tc.[TANK_CONTRACT_ASSIGN_ID] as external_row_id
FROM EMERSON.Synthesis_Test_02242020.dbo.TANK t INNER JOIN 
 	 EMERSON.Synthesis_Test_02242020.dbo.MASTER_DEVICE md ON md.MASTER_DEVICE_ID = t.MASTER_DEVICE_ID  INNER JOIN 
	 EMERSON.Synthesis_Test_02242020.dbo.TANK_CONTRACT_ASSIGN tc ON md.MASTER_DEVICE_ID = tc.MASTER_DEVICE_ID LEFT OUTER JOIN
	 EMERSON.Synthesis_Test_02242020.dbo.CONTRACT c ON c.PETRO_CONTRACT_ID = tc.CONTRACT_ID LEFT OUTER JOIN
	 EMERSON.Synthesis_Test_02242020.dbo.MASTER_BUSINESS_ENTITY be ON c.CONTRACT_PARTY_ID = be.MASTER_BE_ID LEFT OUTER JOIN 
	 tanks mt ON mt.tank_external_id_inventory = md.CODE LEFT OUTER JOIN 
	 tank_owner tow ON tc.[TANK_CONTRACT_ASSIGN_ID] = tow.[external_row_id] LEFT OUTER JOIN 
	 entities e ON e.external_id =  be.MASTER_BE_ID
WHERE  tow._key IS NULL AND tc.ACTIVE_IND = 1

-- update existing records 
UPDATE tow
SET [tk_start_datetime] = tc.[EFFECTIVE_DATE]
   ,[capacity_for_billing] = tc.[SPECIFIC_CAPACITY]
  -- SELECT tc.[EFFECTIVE_DATE],  tc.[SPECIFIC_CAPACITY],  mt.tank_description
FROM EMERSON.Synthesis_Test_02242020.dbo.TANK t INNER JOIN 
 	 EMERSON.Synthesis_Test_02242020.dbo.MASTER_DEVICE md ON md.MASTER_DEVICE_ID = t.MASTER_DEVICE_ID  INNER JOIN 
	 EMERSON.Synthesis_Test_02242020.dbo.TANK_CONTRACT_ASSIGN tc ON md.MASTER_DEVICE_ID = tc.MASTER_DEVICE_ID LEFT OUTER JOIN
	 EMERSON.Synthesis_Test_02242020.dbo.CONTRACT c ON c.PETRO_CONTRACT_ID = tc.CONTRACT_ID LEFT OUTER JOIN
	 EMERSON.Synthesis_Test_02242020.dbo.MASTER_BUSINESS_ENTITY be ON c.CONTRACT_PARTY_ID = be.MASTER_BE_ID LEFT OUTER JOIN 
	 tanks mt ON mt.tank_external_id_inventory = md.CODE LEFT OUTER JOIN 
	 tank_owner tow ON tc.[TANK_CONTRACT_ASSIGN_ID] = tow.[external_row_id] LEFT OUTER JOIN 
	 entities e ON e.external_id =  be.MASTER_BE_ID
WHERE  tow._key IS NOT NULL


-- end date deleted rows 
UPDATE tow 
SET [tk_end_datetime] = getdate()
--SELECT tow.tk_start_datetime, mt.tank_description, tc.[SPECIFIC_CAPACITY], tc.ACTIVE_IND, tow.[external_row_id]
FROM tank_owner tow INNER JOIN 
     tanks mt ON tow.tank_key = mt._key LEFT OUTER JOIN  
	 entities e ON tow.tank_owner_key =  e._key LEFT OUTER JOIN 
	 EMERSON.Synthesis_Test_02242020.dbo.TANK_CONTRACT_ASSIGN tc on tow.external_row_id = tc.[TANK_CONTRACT_ASSIGN_ID]
WHERE tc.ACTIVE_IND = 0 OR tc.ACTIVE_IND  IS NULL


END