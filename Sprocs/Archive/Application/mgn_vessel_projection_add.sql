Create  OR ALTER             PROCEDURE [dbo].[mgn_vessel_projection_add]

	@order uniqueidentifier
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from vessel_projections
--Where _key = @row

declare @new_day_number int = (select Max(day_number) from vessel_projected_volumes where synthesis_key = @order) + 1
		Insert Into  vessel_projected_volumes
					([synthesis_key],
					 [day_number],
					 [projected_volume])
		Values		(@order,
					 @new_day_number,
					 0)

END
GO

