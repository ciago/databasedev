USE [Moda]
GO
/****** Object:  UserDefinedFunction [dbo].[get_days_in_month]    Script Date: 5/26/2020 11:48:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE OR ALTER FUNCTION [dbo].[get_days_in_month]
( @date DATETIME
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
DECLARE @days int = day(DATEADD(DD, -1, DATEADD(mm, DATEDIFF(mm, 0 ,@date)+ 1,0)))


	-- Return the result of the function
	RETURN @days

END