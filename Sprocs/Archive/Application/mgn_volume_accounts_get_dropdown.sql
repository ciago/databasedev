USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_volume_accounts_get_dropdown]    Script Date: 2/10/2021 3:16:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
ALTER             PROCEDURE [dbo].[mgn_volume_accounts_get_dropdown] 
	@site_key uniqueidentifier = null, 
	@user varchar(50) = 'default'

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


SELECT '00000000-0000-0000-0000-000000000000' AS RowKey, 'Select Account' As description
UNION
SELECT _key as RowKey, [accounts] as description
FROM [dbo].[volume_accounts]
WHERE active = 1 and @site_key = volume_accounts.site
ORDER BY RowKey

END


-- EXEC [mgn_accounting_volume_types_get_list]





