USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_vessel_activity_update]    Script Date: 5/1/2020 7:36:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Updates one row of the vessel acitvity table 

/*EXEC mgn_vessel_activity_update
	@row  = 'BF033E1C-7057-4927-A452-E302DF3B61B3', 
	@start_date = '2020-05-13 07:03',
	@end_date = '2020-05-13 10:58', 
	@amount = 2018, 
	@ignore = 0

	*/
-- =============================================
CREATE OR ALTER   PROCEDURE [dbo].mgn_vessel_activity_update
	@row uniqueidentifier = null, 
	@start_date datetime = null, 
	@end_date datetime = null, 
	@amount float = null, 
	@ignore bit = 0, 
	@batch_id nvarchar(200), 
	@berth_key uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


UPDATE valve_activity 
SET valve_start = @start_date, 
	valve_stop = @end_date, 
	valve_bbl = @amount, 
	valve_ignore = @ignore, 
	valve_skid_batch_id  = @batch_id, 
	valve_berth_key = @berth_key
WHERE _key = @row

	
END
