USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_get_list]    Script Date: 4/5/2021 12:24:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER   PROCEDURE [dbo].[mgn_pipeline_get_list] 
	--@terminal uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT '00000000-0000-0000-0000-000000000000' AS RowKey, 'Select Pipeline' AS description
	UNION
    SELECT _key as RowKey, description AS description
	FROM pipeline
	WHERE active = 1 

	ORDER BY RowKey

END
