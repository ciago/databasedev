USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_get_list]    Script Date: 2/18/2021 3:20:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_tank_get_list
 
	*/

ALTER         PROCEDURE [dbo].[mgn_tank_get_list] 
    @site_key uniqueidentifier = null, 
	@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();


	SELECT '00000000-0000-0000-0000-000000000000' as [key], 'Select Tank' as [tank_name]
	UNION
    SELECT _key as [key],
		   tank_name as [tank_name]
    FROM tanks
	WHERE site_key = @site_key
	ORDER BY tank_name
    
END
