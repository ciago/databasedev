USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_berth_fees_get_row]    Script Date: 9/28/2020 8:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Obtains a single row of berth fees data from a specified guid.

/*EXEC mgn_berth_fees_get_row
	@row  = '905b9098-9ba6-4d70-8e1e-d5f17529ad69'
	*/
-- =============================================
CREATE or ALTER     PROCEDURE [dbo].[mgn_berth_fees_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from 
--WHere _key = @row
   
SELECT        bf._key,
			  bf.site_key as [site_key],
			  bf.start_loa as [start_loa],
			  bf.end_loa as [end_loa],
			  ROUND(bf.loa_fee, 2) as [loa_fee]
			  
FROM berth_fees AS bf

WHERE @row = bf._key
	  
END