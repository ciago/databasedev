USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[import_adjusted_finals]    Script Date: 7/21/2020 3:25:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER   PROCEDURE [dbo].[import_adjusted_finals]

	
AS
/* 
EXEC daily_berth_activity_update  @eod = DATETIMEFROMPARTS( 2020, 05, 29, 7, 0, 0, 0)
	
*/
BEGIN


-- UPDATE pipeline actuals 

--SELECT t._key,
--	   et.LS_TICKET_ID,
--	   t.tk_start_datetime, 
--	   t.tk_final_gsv, 
--	   et.TICKET_GSV_AMT,
--	   et.NET_VOL
UPDATE t
SET tk_final_gsv = et.TICKET_GSV_AMT, 
	tk_final_nsv = et.NET_VOL
FROM tickets t INNER JOIN 
	 tanks ta ON t.tk_to_key = ta._key LEFT OUTER JOIN 
	 pipeline p ON p._key = t.tk_from_key LEFT OUTER JOIN 
	 Moda_Final_Tickets mft ON mft._key = t._key LEFT OUTER JOIN 
	 (SELECT  LS_TICKET_ID, MAX(TICKET_REV_NUM) as TICKET_REV_NUM FROM EMERSON.Synthesis_Test_02242020.dbo.TICKET WHERE MEASUREMENT_TICKET_TYPE_ID = 1002  GROUP BY LS_TICKET_ID) newtic ON newtic.LS_TICKET_ID = mft.ticket_id LEFT OUTER JOIN 
	 EMERSON.Synthesis_Test_02242020.dbo.TICKET et ON et.LS_TICKET_ID = newtic.LS_TICKET_ID AND et.TICKET_REV_NUM = newtic.TICKET_REV_NUM 
WHERE et.MEASUREMENT_TICKET_TYPE_ID = 1002 --AND ROUND( t.tk_final_gsv ,2) <> et.TICKET_GSV_AMT
AND mft.movement_type = 'PipelineToTank'
AND t.tk_start_datetime > DATEADD(MONTH, -2, GETDATE())



--SELECT t.tk_start_datetime, t.tk_final_gsv, et.TICKET_GSV_AMT,
--		ROUND(t.tk_final_gsv- et.TICKET_GSV_AMT,0) as diff_final_syn,
--		ROUND(t.tk_scheduled_volume- et.TICKET_GSV_AMT,0) as diff_final_sch, t.tk_scheduled_volume,
--		tr_open.READING_GROSS_VOL as open_reading, tr_close.READING_GROSS_VOL as close_reading, mft.ticket_id, 
--		 tr_close.READING_GROSS_VOL  - t.tk_scheduled_volume as new_open 
----UPDATE t 
----SET tk_final_gsv = et.TICKET_GSV_AMT
----UPDATE tr_open
----SET READING_GROSS_VOL = tr_close.READING_GROSS_VOL  - t.tk_scheduled_volume
--FROM tickets t INNER JOIN 
--	 tanks ta ON t.tk_to_key = ta._key LEFT OUTER JOIN 
--	 pipeline p ON p._key = t.tk_from_key LEFT OUTER JOIN 
--	 Moda_Final_Tickets mft ON mft._key = t._key LEFT OUTER JOIN 
--	 EMERSON.Synthesis_Test_02242020.dbo.TICKET et ON et.LS_TICKET_ID = mft.ticket_id INNER JOIN 
-- (SELECT MAX(SA_TICKET_ID) as SA_TICKET_ID, LS_TICKET_ID
--  FROM EMERSON.Synthesis_Test_02242020.dbo.TICKET
--  GROUP BY LS_TICKET_ID) unique_tickets ON et.SA_TICKET_ID = unique_tickets.SA_TICKET_ID
-- LEFT OUTER JOIN 
--	 (SELECT et.SA_TICKET_ID,  MIN(tr.SA_TICKET_READING_ID) as open_ticket_id
--		FROM EMERSON.Synthesis_Test_02242020.dbo.TICKET et INNER JOIN 
--			 EMERSON.Synthesis_Test_02242020.dbo.TICKET_READING tr ON et.SA_TICKET_ID = tr.SA_TICKET_ID
--		--WHERE et.SA_TICKET_ID = 15169
--		GROUP BY et.SA_TICKET_ID) open_read ON et.SA_TICKET_ID = open_read.SA_TICKET_ID INNER JOIN 
--		EMERSON.Synthesis_Test_02242020.dbo.TICKET_READING tr_open ON tr_open.SA_TICKET_READING_ID = open_read.open_ticket_id LEFT OUTER JOIN 
--	 (SELECT et.SA_TICKET_ID,  MAX(tr.SA_TICKET_READING_ID) as close_ticket_id
--		FROM EMERSON.Synthesis_Test_02242020.dbo.TICKET et INNER JOIN 
--			 EMERSON.Synthesis_Test_02242020.dbo.TICKET_READING tr ON et.SA_TICKET_ID = tr.SA_TICKET_ID
--		--WHERE et.SA_TICKET_ID = 15169
--		GROUP BY et.SA_TICKET_ID) close_read ON et.SA_TICKET_ID = close_read.SA_TICKET_ID INNER JOIN 
--		EMERSON.Synthesis_Test_02242020.dbo.TICKET_READING tr_close ON tr_close.SA_TICKET_READING_ID = close_read.close_ticket_id

--WHERE et.MEASUREMENT_TICKET_TYPE_ID = 1002 
--AND ROUND( t.tk_final_gsv ,2) <> et.TICKET_GSV_AMT
----AND (ROUND(t.tk_scheduled_volume- et.TICKET_GSV_AMT,0) < -5000 )--OR ROUND(t.tk_scheduled_volume- et.TICKET_GSV_AMT,0) < 5000)
----AND  mft.ticket_id = 10211
----AND tk_scheduled_start_datetime >= '6/1/2020 07:00' AND tk_scheduled_start_datetime <= '7/1/2020 07:00'
----AND ROUND(t.tk_final_gsv- et.TICKET_GSV_AMT,0) <> 0



END