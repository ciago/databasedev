USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_actuals_get_list]    Script Date: 3/4/2021 3:38:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Brandon Morey    
-- Create date: 5/4/2020
-- Description:    Gets a list of tank actuals from a selected day.

/*EXEC mgn_tank_actuals_get_list  
	@date  = '5/31/2020'
    */
	
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_tank_actuals_get_list]

    @site uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 
	@date datetime = null



AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from tank_actuals

DECLARE @eod datetime = DATEADD(DAY, 1, SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date), 07, 00))

IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END


SELECT        ta._key,
			  ta.tank_key as [tank_key],
			  t.tank_name as [tank_name],
              DATEADD(DAY, -1, ta.eod_datetime) as [date_datetime],
              ROUND(ISNULL(ta.tank_gsv_change,0), 2) as [tank_gsv_change],
              ROUND(ISNULL(ta.tank_tov_change,0), 2) as [tank_tov_change],
			  ROUND(ISNULL(ta.open_gsv, 0), 2) as [open_gsv],
			  ROUND(ISNULL(ta.close_gsv,0), 2) as [close_gsv],
			  ta.site_key as [site_key],
              s.short_name as [site_name]

FROM          tank_actuals AS ta INNER JOIN
              sites AS s ON s._key = ta.site_key INNER JOIN
              tanks AS t ON ta.tank_key = t._key
			  
WHERE     s._key = @site
	  AND eod_datetime = @eod
	  AND   t.active = 1

ORDER BY eod_datetime
		  
END
 