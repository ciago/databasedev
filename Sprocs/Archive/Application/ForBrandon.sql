USE [Moda]
GO
/****** Object:  Table [dbo].[pipeline_product_assignment]    Script Date: 6/16/2020 9:00:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pipeline_product_assignment](
	[_key] [uniqueidentifier] NOT NULL,
	[pipeline_key] [uniqueidentifier] NOT NULL,
	[product_key] [uniqueidentifier] NOT NULL,
	[start_datetime] [datetime] NOT NULL,
	[end_datetime] [datetime] NULL,
	[record_info] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[pipeline_product_assignment] ([_key], [pipeline_key], [product_key], [start_datetime], [end_datetime], [record_info]) VALUES (N'd5ac78b1-6677-45b0-8412-b84881591f08', N'fc536541-5a92-466a-bcd1-db7fedcd15e7', N'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0', CAST(N'2020-01-01T00:00:00.000' AS DateTime), NULL, N'{ 
    "created_by":"candice", 
    "created_date":"Jun 16 2020  8:50AM", 
    "created_machine":"MANGANATL-L-099", 
    "modified_by":"", 
    "modified_date":"", 
    "modified_machine":"", 
    "deleted_by":"", 
    "deleted_date":"", 
    "deleted_machine":"", 
    "archived_by":"", 
    "archived_date":"", 
    "archived_machine":"", 
    "restored_by":"", 
    "restored_date":"", 
    "restored_machine":"" 
  }')
GO
INSERT [dbo].[pipeline_product_assignment] ([_key], [pipeline_key], [product_key], [start_datetime], [end_datetime], [record_info]) VALUES (N'2fc5f8f4-e8e5-43c9-b614-29e48862cf9e', N'fc536541-5a92-466a-bcd1-db7fedcd15e7', N'86e1baa4-142e-4d36-9d2b-7e9e3a3530d2', CAST(N'2020-07-01T00:00:00.000' AS DateTime), NULL, N'{ 
    "created_by":"candice", 
    "created_date":"Jun 16 2020  8:51AM", 
    "created_machine":"MANGANATL-L-099", 
    "modified_by":"", 
    "modified_date":"", 
    "modified_machine":"", 
    "deleted_by":"", 
    "deleted_date":"", 
    "deleted_machine":"", 
    "archived_by":"", 
    "archived_date":"", 
    "archived_machine":"", 
    "restored_by":"", 
    "restored_date":"", 
    "restored_machine":"" 
  }')
GO
INSERT [dbo].[pipeline_product_assignment] ([_key], [pipeline_key], [product_key], [start_datetime], [end_datetime], [record_info]) VALUES (N'b5685029-976a-48c5-8bd7-ddb5f62bb5dd', N'fc536541-5a92-466a-bcd1-db7fedcd15e7', N'8e52211a-40d0-4722-b423-663781b8bd9f', CAST(N'2020-01-01T00:00:00.000' AS DateTime), CAST(N'2020-02-28T00:00:00.000' AS DateTime), N'{ 
    "created_by":"candice", 
    "created_date":"Jun 16 2020  8:52AM", 
    "created_machine":"MANGANATL-L-099", 
    "modified_by":"", 
    "modified_date":"", 
    "modified_machine":"", 
    "deleted_by":"", 
    "deleted_date":"", 
    "deleted_machine":"", 
    "archived_by":"", 
    "archived_date":"", 
    "archived_machine":"", 
    "restored_by":"", 
    "restored_date":"", 
    "restored_machine":"" 
  }')
GO
ALTER TABLE [dbo].[pipeline_product_assignment] ADD  CONSTRAINT [DF_pipeline_product_assignment__key]  DEFAULT (newid()) FOR [_key]
GO
ALTER TABLE [dbo].[pipeline_product_assignment] ADD  CONSTRAINT [DF_pipeline_product_assignment_record_info]  DEFAULT ([dbo].[default_record_info]()) FOR [record_info]
GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_product_assign_update]    Script Date: 6/16/2020 9:00:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected pipeline_actuals with new user input details.

/*EXEC [mgn_pipeline_product_assign_update] 
	@row = newid(),
	@pipeline_key = 'fc536541-5a92-466a-bcd1-db7fedcd15e7',
	@product_key = '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',
	@start_datetime = '1/1/2020 00:00',
	@user = 'candice'


	// 
	*/
-- =============================================
CREATE         PROCEDURE [dbo].[mgn_pipeline_product_assign_update]
	@row uniqueidentifier,
    @pipeline_key uniqueidentifier ,
	@product_key uniqueidentifier,
	@start_datetime datetime,
	@end_datetime datetime =  NULL,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_actuals
--Where _key = @row

IF(@row IN (SELECT _key from pipeline_product_assignment))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM nominations WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())


	UPDATE pipeline_product_assignment
		SET end_datetime = @end_datetime, 
			record_info = @json
	WHERE _key = @row

END
ELSE
BEGIN
-- it does not exist so insert a new row 


	INSERT INTO [dbo].[pipeline_product_assignment]
           ([_key]
           ,[pipeline_key]
           ,[product_key]
           ,[start_datetime]
           ,[end_datetime]
           ,[record_info])
     VALUES
           (@row
           ,@pipeline_key
           ,@product_key
           ,@start_datetime
           ,@end_datetime
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )


END

END
GO
