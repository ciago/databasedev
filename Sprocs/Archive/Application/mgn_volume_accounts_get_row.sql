USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_volume_accounts_get_row]    Script Date: 2/9/2021 12:03:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Obtains a single row of volume accounts data by searching for a specified guid.

/*EXEC mgn_volume_accounts_get_row
	@row  = '6f90dfed-30c9-45d6-b59b-5746650ba683'
	*/
-- =============================================
ALTER         PROCEDURE [dbo].[mgn_volume_accounts_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from 
--WHere _key = @row
   
SELECT va._key, 
	   va.accounts, 
	   va.unit_description,
	   va.accounting_volume_types_key,
	   avt.type_description, 
	   ISNULL(va.entity_key, '00000000-0000-0000-0000-000000000000') as [entity_key],
	   ISNULL(ent.short_name, '') as [short_name], 
	   ISNULL(va.vessel_types_key, '00000000-0000-0000-0000-000000000000') as [vessel_types_key],
	   ISNULL(vt.vessel_type, '') as [vessel_type],
	   ISNULL(va.gl_account, '') as [gl_account],
	   avt.show_shipper,
	   avt.show_vessel_type,
	   va.start,
	   va.stop
			  
FROM volume_accounts as va INNER JOIN 
	 accounting_volume_types avt ON va.accounting_volume_types_key = avt._key LEFT JOIN 
	 entities ent ON va.entity_key = ent._key LEFT JOIN 
	 vessel_types vt ON va.vessel_types_key = vt._key

WHERE @row = va._key
GROUP BY va._key, va.accounts, va.unit_description, va.accounting_volume_types_key, avt.type_description, va.entity_key, ent.short_name, va.vessel_types_key, vt.vessel_type, va.gl_account, avt.show_shipper, avt.show_vessel_type,va.start,va.stop

	  
END
