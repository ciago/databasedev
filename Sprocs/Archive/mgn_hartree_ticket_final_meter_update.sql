USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_hartree_ticket_final_meter_update]    Script Date: 4/20/2021 11:13:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/18/2021
-- Description:		Update selected business entities record with new user input details.

/*EXEC [mgn_ticket_final_meter_update] 
	@location = 'tl3',
	@nomination = 1001
	
	// 
	*/
-- =============================================
ALTER             PROCEDURE [dbo].[mgn_hartree_ticket_final_meter_update]

	@ticket_id bigint,
	@meter_id varchar(50),
	@batch_start_date varchar(50),
	@batch_start_time varchar(50),
	@batch_end_date varchar(50),
	@batch_end_time varchar(50),
	@product_name varchar(50),
	@batch_id varchar(50),
	@batch_gross_totalizer float,
	@batch_net_totalizer float,
	@batch_mass_totalizer float,
	@batch_nsv_totalizer float,
	@opening_gross float,
	@opening_net float,
	@opening_mass float,
	@opening_nsv float,
	@days_opening_gross float,
	@closing_gross float,
	@closing_net float,
	@closing_mass float,
	@closing_nsv float,
	@batch_report_number int,
	@batch_average_temperature float,
	@batch_average_pressure float,
	@batch_average_density float,
	@batch_average_vcf float,
	@batch_average_cpl float,
	@batch_average_meter_factor float,
	@batch_average_sw float,
	@batch_average_equilibrium_pressure float,
	@batch_average_api_60f float,
	@batch_ctpl float
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

/* Set Location Type */
--declare @location nvarchar(100) = 'd1'

declare @volume_uom float = (select uom_default_volume from site_configuration where site_key = dbo.get_default_site())
declare @mass_uom float = (select uom_default_mass from site_configuration where site_key = dbo.get_default_site())
declare @temperature_uom float = (select uom_default_temperature from site_configuration where site_key = dbo.get_default_site())
declare @density_uom float = (select uom_default_density from site_configuration where site_key = dbo.get_default_site())
declare @pressure_uom float = (select uom_default_pressure from site_configuration where site_key = dbo.get_default_site())
declare @ticket_key uniqueidentifier = (select _key from tickets where tk_ticket_id = @ticket_id)
declare @meter_key uniqueidentifier = (select [_key] from meters where meter_id = @meter_id)

DELETE from tickets_meter where [tk_meter_ticket_id] = @ticket_id

insert into [dbo].[tickets_meter]
			([tk_ticket_key],
			[tk_meter_key],
			[tk_meter_ticket_id],
			[tk_meter_id],
			[tk_meter_batch_start_date],
			[tk_meter_batch_start_time],
			[tk_meter_batch_end_date],
			[tk_meter_batch_end_time],
			[tk_meter_product_name],
			[tk_meter_batch_id],
			[tk_meter_batch_gross_totalizer],
			[tk_meter_batch_net_totalizer],
			[tk_meter_batch_mass_totalizer],
			[tk_meter_batch_nsv_totalizer],
			[tk_meter_opening_gross],
			[tk_meter_opening_net],
			[tk_meter_opening_mass],
			[tk_meter_opening_nsv],
			[tk_meter_days_opening_gross],
			[tk_meter_closing_gross],
			[tk_meter_closing_net],
			[tk_meter_closing_mass],
			[tk_meter_closing_nsv],
			[tk_meter_batch_report_number],
			[tk_meter_batch_average_temperature],
			[tk_meter_batch_average_pressure],
			[tk_meter_batch_average_density],
			[tk_meter_batch_average_vcf],
			[tk_meter_batch_average_cpl],
			[tk_meter_batch_average_meter_factor],
			[tk_meter_batch_average_sw],
			[tk_meter_batch_average_equilibrium_pressure],
			[tk_meter_batch_average_api_60f],
			[tk_meter_batch_ctpl],
			[tk_meter_volume_uom],
			[tk_meter_mass_uom],
			[tk_meter_temperature_uom],
			[tk_meter_pressure_uom],
			[tk_meter_density_uom])

Values	    (@ticket_key,
			@meter_key,
			@ticket_id,
			@meter_id,
			@batch_start_date,
			@batch_start_time,
			@batch_end_date,
			@batch_end_time,
			@product_name,
			@batch_id,
			@batch_gross_totalizer,
			@batch_net_totalizer,
			@batch_mass_totalizer,
			@batch_nsv_totalizer,
			@opening_gross,
			@opening_net,
			@opening_mass,
			@opening_nsv,
			@days_opening_gross,
			@closing_gross,
			@closing_net,
			@closing_mass,
			@closing_nsv,
			@batch_report_number,
			@batch_average_temperature,
			@batch_average_pressure,
			@batch_average_density,
			@batch_average_vcf,
			@batch_average_cpl,
			@batch_average_meter_factor,
			@batch_average_sw,
			@batch_average_equilibrium_pressure,
			@batch_average_api_60f,
			@batch_ctpl,
			@volume_uom,
			@mass_uom,
			@temperature_uom,
			@pressure_uom,
			@density_uom)

Update [dbo].[tickets]

Set		tk_final_nsv = @batch_nsv_totalizer,
		tk_final_gsv = @batch_gross_totalizer,
		tk_start_datetime = Convert(datetime, @batch_start_date + ' ' + @batch_start_time),
		tk_end_datetime = Convert(datetime, @batch_end_date + ' ' + @batch_end_time),
		tk_status = 3 -- Completed

where	tk_ticket_id = @ticket_id

END

