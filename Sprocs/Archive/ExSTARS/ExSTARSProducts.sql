delete FROM product_irs_codes
GO

INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0e629ca9-4f45-4cf5-a936-b00075e30ec4', N'001', N'Crude', 0, 1, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9c9382ad-180c-415d-8c32-4f303065ca9e', N'049', N'Condensate (not Crude)', 0, 1, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'56a7cf32-86f7-4220-a51f-a8a0137fed2d', N'052', N'Ethane', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a86c45d3-f36e-43c8-b515-2202fd94440a', N'054', N'Propane', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'fe4aac35-9a97-4213-89a2-8dbc7b7d9769', N'055', N'Butane, Including Butane Propane Mix', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1eda07d6-04cc-4988-953e-927db3fe013f', N'058', N'Isobutane', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'77120acc-28f9-4d1d-806f-fb309cf25e54', N'059', N'Pentanes, Including Isopentane', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9e9679f0-ac39-46d6-9e17-3f58eb82fd1a', N'065', N'Gasoline', 1, 0, 0, N'065', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5ff4f56c-f847-455b-9033-2238ff81563c', N'073', N'Kerosene Low Sulphur Dyed', 1, 0, 0, N'072', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'295e7525-f7f4-452d-9b12-e68f7867dd77', N'074', N'Kerosene High Sulphur Dyed ', 1, 0, 0, N'072', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'fe8280b6-bd36-4be1-9e64-90168f624cea', N'075', N'Propylene', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'71f0276e-9453-4af6-b02b-53f08835ffb9', N'076', N'Xylene', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'58ee54d0-b7f2-4f75-a99b-9c607c496732', N'077', N'Excluded Liquid (Mineral Oil)', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'29198fce-94ee-4baf-98c8-0220367618d7', N'090', N'Additive – Miscellaneous', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c632c208-2714-4e81-82e2-b36b9d0e351b', N'091', N'Waste Oil', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'111a7eab-0dc5-470f-8129-d2f7e9760bee', N'092', N'Undefined (Other) Product', 0, 1, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'06e992c9-a015-4291-9a7a-3e5a2cc399a9', N'093', N'MTBE', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'00ebab09-aaeb-4019-a4f2-ce62d38d2b03', N'100', N'Transmix ', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f846460b-854a-4832-8333-3bfeec99a5fd', N'121', N'TAME', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd66eaec7-49ab-45da-b194-4026b0dace58', N'122', N'Blending Components Other', 1, 1, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'08e0dd7d-6a5e-4747-9b8f-6eb1da3c4642', N'125', N'Aviation Gasoline', 0, 0, 0, N'125', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'33b70933-f320-4fb3-9bd8-89748a580d18', N'126', N'Napthas', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'686ad3ef-58f7-40d7-ac25-73948ef47676', N'130', N'Jet Fuel', 1, 0, 0, N'130', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd685bd77-2f29-4543-a0fb-aef414976522', N'145', N'Kerosene Low Sulphur Undyed', 1, 0, 0, N'142', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b10006f0-d0c3-4a3e-9bde-9d579ab5d65f', N'147', N'Kerosene High Sulphur Undyed', 1, 0, 0, N'142', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e25f628e-d0b4-4402-93d2-33bc9b862f61', N'150', N'Fuel Oil #1 Undyed', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'157f033a-19d2-459b-8bc8-81f5c535da28', N'153', N'Diesel Fuel #4 Dyed', 1, 0, 0, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7110fe04-72b7-4a72-af66-d64f671b17f4', N'154', N'Diesel Fuel #4 Undyed', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5da4a4f4-ebdd-445c-b6b0-f8ed5e143fb3', N'161', N'Diesel Fuel #1 Low Sulphur Undyed', 1, 0, 0, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'49a198d6-065f-45c8-8c79-fa71ada1d3d0', N'167', N'Diesel Fuel #2 Low Sulphur Undyed', 1, 0, 0, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7442b883-0ce9-4c8b-b9de-144a2359e843', N'188', N'Asphalt', 0, 1, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'203008e1-faf2-44ba-827b-20ddb2d9d2bb', N'196', N'Ethylene', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b1c5dc1b-90c2-4bd6-9f0d-1d8ed8886dfa', N'198', N'Butylene', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'80ae9614-afec-45b0-9914-653d352b18e8', N'199', N'Toluene', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e561db29-6157-46ff-976e-8284edeb948d', N'223', N'Raffinates', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a6b6221d-3383-487a-8ad1-4b11f1ad7e2c', N'224', N'Compressed Natural Gas', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3aa45e7f-55bf-4a7a-b969-b479d10eb476', N'225', N'Liquified Natural Gas', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1a6053f4-2488-42f4-b052-96624dbe4807', N'226', N'Diesel Fuel High Sulphur Dyed ', 1, 0, 0, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f5e56d19-75e2-4e94-9f9a-becef23700dc', N'227', N'Diesel Fuel Low Sulphur Dyed', 1, 0, 0, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8ff80d49-5b11-402e-b1cd-a6eb1be00c6b', N'231', N'Diesel Fuel #1 Dyed', 1, 0, 0, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7e6adaeb-26d9-4073-92a0-fe6756d65923', N'248', N'Benzene', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'805dbf08-0792-4caf-99c4-af3cb368ffff', N'249', N'ETBE', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9b7248f1-b5eb-40d5-a53e-e91f778b5609', N'265', N'Methane', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'56e220f5-abb9-4e8a-8d23-05d7f75637bc', N'279', N'Marine Diesel Oil', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9e97372e-382f-41c4-b0f4-e87b3dd8c528', N'280', N'Marine Gas Oil', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'99231d29-c025-4193-a590-be9dac08fedf', N'281', N'Mineral Oils', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f1098850-7192-4407-9c2a-88b654ff3653', N'282', N'Diesel Fuel High Sulphur #1 Undyed ', 1, 0, 0, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8d7b4d54-55a2-4153-a470-6cb430e896b3', N'283', N'Diesel Fuel High Sulphur #2 Undyed', 1, 0, 0, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7c238cf2-1d71-49aa-a27a-fb83af703545', N'285', N'Soy Oil', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'96bffc51-a319-425f-97e4-11d7762794d7', N'960', N'Food', 0, 1, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7e486450-755e-4e7a-961d-c09aa86c7c0e', N'B00', N'BioDiesel  100%,  Diesel  0%', 0, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f5fd023f-63e0-4455-8978-d1b70209d4c4', N'B01', N'BioDiesel  1%,  Diesel  99%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'49d30eae-ca6f-437f-b7d0-faf8af3d89d0', N'B02', N'BioDiesel  2%,  Diesel  98%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9eb7c9ab-a0a5-480c-beb3-e11cc7413ec0', N'B03', N'BioDiesel  3%,  Diesel  97%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1d2bc8e3-ec3a-4c7f-8be5-17c6248dbbe4', N'B04', N'BioDiesel  4%,  Diesel  96%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'88b8fc12-b4c2-4188-97fe-6e5514c49e9f', N'B05', N'BioDiesel  5%,  Diesel  95%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd7ad49f1-4d95-4ea5-a38b-30b88f80e85e', N'B06', N'BioDiesel  6%,  Diesel  94%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e1bcac6b-6a6c-45db-a687-d34c6335646b', N'B07', N'BioDiesel  7%,  Diesel  93%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1fe861cc-d859-455a-ab26-1744cba6753b', N'B08', N'BioDiesel  8%,  Diesel  92%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c9505251-c6d5-49a4-9767-47ddd78e24a6', N'B09', N'BioDiesel  9%,  Diesel  91%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'bc194884-5640-4e1b-85b0-76e44d84ba60', N'B10', N'BioDiesel  10%,  Diesel  90%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'97674af4-d040-46f7-99aa-02aad837472d', N'B11', N'BioDiesel  11%,  Diesel  89%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3ca83ed7-823f-4038-b1c2-25408dc73b7a', N'B12', N'BioDiesel  12%,  Diesel  88%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7bfb8bfa-7910-4f09-ad77-a4ab79ae4c6c', N'B13', N'BioDiesel  13%,  Diesel  87%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'835d063c-7161-4445-85f2-c1dd7383f22e', N'B14', N'BioDiesel  14%,  Diesel  86%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3f3d8ac1-17a6-4d82-abcc-3dda03026924', N'B15', N'BioDiesel  15%,  Diesel  85%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'39768b6d-d8d2-4b48-994b-59dbde7fcf86', N'B16', N'BioDiesel  16%,  Diesel  84%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b4908bf4-6a1a-49e3-ad1c-51e3bfc9ad5f', N'B17', N'BioDiesel  17%,  Diesel  83%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'04cf1d22-1484-4246-b633-202bfb1ce94b', N'B18', N'BioDiesel  18%,  Diesel  82%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'263bc01c-1465-4e7c-9402-4bfd4508c9b9', N'B19', N'BioDiesel  19%,  Diesel  81%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'aae376f6-3088-46cc-9961-adefcfadfc25', N'B20', N'BioDiesel  20%,  Diesel  80%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'221e7115-0f20-4462-a4bc-907fc9bd32c6', N'B21', N'BioDiesel  21%,  Diesel  79%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e9f3c5b8-4514-4e10-bfdd-a3bd0e494956', N'B22', N'BioDiesel  22%,  Diesel  78%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8c8fd975-7122-4b94-a130-54d07225e597', N'B23', N'BioDiesel  23%,  Diesel  77%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6e251e27-33e6-4289-a4cb-1db1bf9a2bd4', N'B24', N'BioDiesel  24%,  Diesel  76%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'691e0e54-8025-44e8-bebd-846c79ffbe9d', N'B25', N'BioDiesel  25%,  Diesel  75%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'fc29631b-c5f8-4410-95f2-3f19ea4b7d10', N'B26', N'BioDiesel  26%,  Diesel  74%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'076928e3-a542-441a-be86-a4bfb5d87066', N'B27', N'BioDiesel  27%,  Diesel  73%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'285423f6-cc94-4049-9984-3b4b5ee83c6b', N'B28', N'BioDiesel  28%,  Diesel  72%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e65e7686-4cf9-487d-bd20-107d14bacacf', N'B29', N'BioDiesel  29%,  Diesel  71%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'915c5447-d9ef-426f-9a28-fff2e821c776', N'B30', N'BioDiesel  30%,  Diesel  70%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0dafe2c1-40f7-4e2c-9b1c-81aaf6d3f42b', N'B31', N'BioDiesel  31%,  Diesel  69%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9e5ee939-472a-4d20-beea-3fc309bd2d1a', N'B32', N'BioDiesel  32%,  Diesel  68%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'86abf657-e08d-4248-b1b1-f98c4c4627a1', N'B33', N'BioDiesel  33%,  Diesel  67%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'12aff62a-64fe-4197-b100-c9053b72055e', N'B34', N'BioDiesel  34%,  Diesel  66%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c60f61fc-bff3-4bc1-9520-8e7fa2ec3f1a', N'B35', N'BioDiesel  35%,  Diesel  65%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c3f7c35c-0215-438e-a1d5-28467f4749d4', N'B36', N'BioDiesel  36%,  Diesel  64%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'87055b16-c3cf-4e1d-8d33-46b6363cb200', N'B37', N'BioDiesel  37%,  Diesel  63%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8960c967-9e20-468d-9484-41885d69778c', N'B38', N'BioDiesel  38%,  Diesel  62%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'67144076-1568-4abe-9b58-5851d304b63a', N'B39', N'BioDiesel  39%,  Diesel  61%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7446116c-e483-49f5-bb04-3a966b28aec2', N'B40', N'BioDiesel  40%,  Diesel  60%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'363865bc-fb96-4fe0-a3bf-7069c33028ff', N'B41', N'BioDiesel  41%,  Diesel  59%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd0aed152-30b8-49af-9905-8d9801480c25', N'B42', N'BioDiesel  42%,  Diesel  58%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'307725a7-2879-4c04-b1b0-34bc5b6bdc6f', N'B43', N'BioDiesel  43%,  Diesel  57%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f00f84ca-a698-41ca-b3c5-64dcc0f6f774', N'B44', N'BioDiesel  44%,  Diesel  56%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6d476786-136b-4b4d-a571-cc6293514ef7', N'B45', N'BioDiesel  45%,  Diesel  55%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e1b6e5b2-e088-4aa7-947e-bda04d45e66d', N'B46', N'BioDiesel  46%,  Diesel  54%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'cf6b5efb-7dbe-49c5-89d8-311823f398cb', N'B47', N'BioDiesel  47%,  Diesel  53%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'acd5a356-692e-41ee-90b7-6654ba850951', N'B48', N'BioDiesel  48%,  Diesel  52%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'44ce96a8-0b76-48e5-b96c-4bcf0122dcb3', N'B49', N'BioDiesel  49%,  Diesel  51%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'eae37642-2ff2-4e2d-a1a0-e475008e6fe1', N'B50', N'BioDiesel  50%,  Diesel  50%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c9cc9aab-af6a-417b-af99-2370a0f8d54e', N'B51', N'BioDiesel  51%,  Diesel  49%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a5317951-56cb-44a7-89e8-32a14a2d44f3', N'B52', N'BioDiesel  52%,  Diesel  48%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'64517c36-ebba-45d3-82b0-01b84f52a496', N'B53', N'BioDiesel  53%,  Diesel  47%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'99caafe7-c35c-4543-8121-1b6ddf49c9fd', N'B54', N'BioDiesel  54%,  Diesel  46%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'10c1a993-948a-45cb-ae9f-b99d259494ea', N'B55', N'BioDiesel  55%,  Diesel  45%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3cca467b-e2fa-4486-84a9-974fd89d2cc1', N'B56', N'BioDiesel  56%,  Diesel  44%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7d01fbe1-4163-4d98-83a4-8d087b3edb59', N'B57', N'BioDiesel  57%,  Diesel  43%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6f85ba19-fe60-459c-bbde-5d406963f186', N'B58', N'BioDiesel  58%,  Diesel  42%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd57d5b1e-76c6-466f-b237-d46c8095e9db', N'B59', N'BioDiesel  59%,  Diesel  41%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'afcc291e-b995-4b6b-83bc-5ad28ae73f6b', N'B60', N'BioDiesel  60%,  Diesel  40%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'880e0f91-1b7d-4900-a3d3-4bd69b721207', N'B61', N'BioDiesel  61%,  Diesel  39%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e6b78b11-dfc8-4317-8638-4f3985e779b0', N'B62', N'BioDiesel  62%,  Diesel  38%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1856d50c-3047-4889-94af-b1d2a324133f', N'B63', N'BioDiesel  63%,  Diesel  37%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'199fda14-0bf4-4d3f-9577-469bedac448e', N'B64', N'BioDiesel  64%,  Diesel  36%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4cbef0d1-30f6-4591-bd2a-c9e2454a80e9', N'B65', N'BioDiesel  65%,  Diesel  35%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a47f669e-782f-4c4d-968a-cdce9ea531f8', N'B66', N'BioDiesel  66%,  Diesel  34%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8b8a0858-d6d9-45cf-9dba-9d833cfe7455', N'B67', N'BioDiesel  67%,  Diesel  33%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'714b16fe-ca43-454e-a7e5-838463bfdc01', N'B68', N'BioDiesel  68%,  Diesel  32%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7b2edf08-4c20-4d0f-9263-e162428c52d4', N'B69', N'BioDiesel  69%,  Diesel  31%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ffa39ea2-1d42-4f5a-82f5-b8606f6594bf', N'B70', N'BioDiesel  70%,  Diesel  30%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'328724ec-2338-4283-910c-cbd5f36a323c', N'B71', N'BioDiesel  71%,  Diesel  29%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0470472f-b012-48e2-90b4-a0ecd0184040', N'B72', N'BioDiesel  72%,  Diesel  28%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'01abca41-2796-425e-b4ec-286cf7eada21', N'B73', N'BioDiesel  73%,  Diesel  27%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1fad79f6-7365-44e5-b8c0-328ace551a6a', N'B74', N'BioDiesel  74%,  Diesel  26%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3bf1773f-08fa-4ab6-b80e-bd190c15667f', N'B75', N'BioDiesel  75%,  Diesel  25%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'11c66e6b-1c49-4d00-a66f-3b4ba25ff3d2', N'B76', N'BioDiesel  76%,  Diesel  24%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'63a30682-4104-4b43-9543-66ec0c41b358', N'B77', N'BioDiesel  77%,  Diesel  23%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b92cbd0c-ff86-458a-b5a2-819b715378db', N'B78', N'BioDiesel  78%,  Diesel  22%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e4a25533-4a6d-4ae9-8c51-64f5d17a9e44', N'B79', N'BioDiesel  79%,  Diesel  21%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f02233b2-8b18-468a-b91f-c4bf347c527b', N'B80', N'BioDiesel  80%,  Diesel  20%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'79d4b619-b16e-47c3-b949-875d3258105f', N'B81', N'BioDiesel  81%,  Diesel  19%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'af4fa517-3fac-472c-bec7-af0e68d7adbc', N'B82', N'BioDiesel  82%,  Diesel  18%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'71ec734d-7ffa-4958-b398-04cb44d5f491', N'B83', N'BioDiesel  83%,  Diesel  17%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3d374354-6ed1-4f54-b9ee-7b788603c473', N'B84', N'BioDiesel  84%,  Diesel  16%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9dabf264-fd49-4681-b211-6ecad3a1d819', N'B85', N'BioDiesel  85%,  Diesel  15%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f0d36494-0bcc-4263-a187-f2fb317702b0', N'B86', N'BioDiesel  86%,  Diesel  14%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'41397e68-4dab-46f6-a36e-bb27b5bc09e5', N'B87', N'BioDiesel  87%,  Diesel  13%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'025bcb05-d474-423c-a450-da9b59e65bb2', N'B88', N'BioDiesel  88%,  Diesel  12%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'95b2eaf1-d216-4ed8-972c-b0f7f5a54bbd', N'B89', N'BioDiesel  89%,  Diesel  11%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ce13df59-8b37-4636-9d93-fb17281add23', N'B90', N'BioDiesel  90%,  Diesel  10%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2436e67e-7e82-442c-9899-27a799a67bdc', N'B91', N'BioDiesel  91%,  Diesel  9%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c8d08eed-c801-4063-b883-21983a8dda85', N'B92', N'BioDiesel  92%,  Diesel  8%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0158f146-0695-4bd7-8af3-40cc5e2ef542', N'B93', N'BioDiesel  93%,  Diesel  7%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'87ecf1e7-a744-47e3-a617-7e064d47f2c4', N'B94', N'BioDiesel  94%,  Diesel  6%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'58b47e6e-a56a-4c08-bd07-889d61def8e7', N'B95', N'BioDiesel  95%,  Diesel  5%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6f6eb93a-dc23-4921-9803-baa576689d01', N'B96', N'BioDiesel  96%,  Diesel  4%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7a8ca385-c95b-4b31-8c95-13b1c54d385e', N'B97', N'BioDiesel  97%,  Diesel  3%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7088432e-b270-4db7-8ca2-3a4294bc5962', N'B98', N'BioDiesel  98%,  Diesel  2%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'06abd6f5-327d-4211-a5a6-ca7c9908b1e3', N'B99', N'BioDiesel  99%,  Diesel  1%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a4bd635a-9043-4788-80ac-38bff6e7d39a', N'D00', N' Dyed BioDiesel  100%,  Diesel  0%', 0, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3da6f512-92ea-4a77-855c-f46cc64c8e58', N'D01', N' Dyed BioDiesel  1%,  Diesel  99%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b8c49dfc-f740-410b-b014-275b3596ae66', N'D02', N' Dyed BioDiesel  2%,  Diesel  98%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'81c81561-4624-4cdc-85a2-c90648ba3539', N'D03', N' Dyed BioDiesel  3%,  Diesel  97%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b5c136be-0d43-4104-a4d4-3a78ee1571a4', N'D04', N' Dyed BioDiesel  4%,  Diesel  96%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ee3dd590-3e17-4c64-97ad-79ca183b6a97', N'D05', N' Dyed BioDiesel  5%,  Diesel  95%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'56b39434-c8d2-4483-acd9-07f5f1d5bc1f', N'D06', N' Dyed BioDiesel  6%,  Diesel  94%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c2d70ea1-a3f5-4488-a7f9-748b7a81c75f', N'D07', N' Dyed BioDiesel  7%,  Diesel  93%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ddc2ca80-911e-4cfd-b406-e032678c3cc8', N'D08', N' Dyed BioDiesel  8%,  Diesel  92%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'440f451e-4d74-4c6a-a0a8-34015827a120', N'D09', N' Dyed BioDiesel  9%,  Diesel  91%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2af969af-a353-4b7e-8c84-1de8388c797d', N'D10', N' Dyed BioDiesel  10%,  Diesel  90%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3b4bbaed-0764-4efe-a636-188e30fe8ebc', N'D11', N' Dyed BioDiesel  11%,  Diesel  89%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'06105f40-2c07-42d7-bbf5-8c6cb2739741', N'D12', N' Dyed BioDiesel  12%,  Diesel  88%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'04421e5b-308a-4379-bd28-acfffe492220', N'D13', N' Dyed BioDiesel  13%,  Diesel  87%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'bc42994f-aa0b-4c1c-b0fd-fd2b1c73fdea', N'D14', N' Dyed BioDiesel  14%,  Diesel  86%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f9dbd983-06a7-4975-a81c-fce09eba2cf6', N'D15', N' Dyed BioDiesel  15%,  Diesel  85%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'48c4cbd2-3872-4572-bbc9-7e9a14b62aa0', N'D16', N' Dyed BioDiesel  16%,  Diesel  84%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0696ee00-d9e1-49da-aa1d-3c577585e8ed', N'D17', N' Dyed BioDiesel  17%,  Diesel  83%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2f6ed268-7341-4327-8caf-9a7b10e4b8d1', N'D18', N' Dyed BioDiesel  18%,  Diesel  82%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'88b07382-e0e0-40f1-b54e-60e8c48cb060', N'D19', N' Dyed BioDiesel  19%,  Diesel  81%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'da21f682-5449-4245-959d-3446f2ac6e6b', N'D20', N' Dyed BioDiesel  20%,  Diesel  80%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'76efad87-e7a8-4675-932b-881e7ab207d0', N'D21', N' Dyed BioDiesel  21%,  Diesel  79%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4f0f2a4e-de19-413b-9ec4-d35683bd97a2', N'D22', N' Dyed BioDiesel  22%,  Diesel  78%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'59d9c3f9-5018-422d-ae38-ae1be8ab48a4', N'D23', N' Dyed BioDiesel  23%,  Diesel  77%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'20aaa853-a02f-4acb-965b-dd3f61e7fc2d', N'D24', N' Dyed BioDiesel  24%,  Diesel  76%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'38a2ccde-baa0-4241-9fd5-a60ebc73b9f9', N'D25', N' Dyed BioDiesel  25%,  Diesel  75%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ce73b4f7-bfa5-4634-9b8a-ca1e74c78d4c', N'D26', N' Dyed BioDiesel  26%,  Diesel  74%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c2a7527f-88fd-4dbf-b75d-373650a8ef71', N'D27', N' Dyed BioDiesel  27%,  Diesel  73%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6a47c2c2-1df5-4de0-acd5-935f6e694d7e', N'D28', N' Dyed BioDiesel  28%,  Diesel  72%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9b9dd517-d8f8-4e0e-932b-69d7da702234', N'D29', N' Dyed BioDiesel  29%,  Diesel  71%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'873447e2-aac9-47b1-b92f-bbd2f8f40c96', N'D30', N' Dyed BioDiesel  30%,  Diesel  70%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'343ec00d-5972-4da2-bffd-041a8a4febbf', N'D31', N' Dyed BioDiesel  31%,  Diesel  69%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'035023d1-257c-40fa-bdd4-bfb642384f7e', N'D32', N' Dyed BioDiesel  32%,  Diesel  68%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f3f78fc6-9526-420e-bef7-69267f98b19f', N'D33', N' Dyed BioDiesel  33%,  Diesel  67%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'426e21b9-d7fe-4c1c-8902-efe4f72f0794', N'D34', N' Dyed BioDiesel  34%,  Diesel  66%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'89781f7a-a3b7-4308-9b30-853ebeb02ea5', N'D35', N' Dyed BioDiesel  35%,  Diesel  65%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1d6e77b5-e480-416c-8122-cf573495c7b3', N'D36', N' Dyed BioDiesel  36%,  Diesel  64%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6ae82019-6e03-4dfe-b4cd-37a3596a88f7', N'D37', N' Dyed BioDiesel  37%,  Diesel  63%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'84c9b9be-239d-4519-a5f0-8304122433c3', N'D38', N' Dyed BioDiesel  38%,  Diesel  62%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e0c61152-74e7-488c-b76e-3aa60a088d48', N'D39', N' Dyed BioDiesel  39%,  Diesel  61%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ef1a14ba-b19f-4ca5-809a-3d0fce791597', N'D40', N' Dyed BioDiesel  40%,  Diesel  60%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'af25cbf0-d972-4cc9-aeca-58b2c3eb46e4', N'D41', N' Dyed BioDiesel  41%,  Diesel  59%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'adaff455-927f-4a18-bb65-219534c7a609', N'D42', N' Dyed BioDiesel  42%,  Diesel  58%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'aa7efdfa-5b8b-420c-8d72-629c8a00f315', N'D43', N' Dyed BioDiesel  43%,  Diesel  57%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'92f083e2-ff34-477b-bb0a-5ed6982c7c78', N'D44', N' Dyed BioDiesel  44%,  Diesel  56%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd94e944c-386b-4812-a827-d45dccab12e6', N'D45', N' Dyed BioDiesel  45%,  Diesel  55%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'afcc2ef9-3513-4db9-9a28-f1369f02472e', N'D46', N' Dyed BioDiesel  46%,  Diesel  54%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9bbd925d-2c14-4c46-a462-7f1defa2bcea', N'D47', N' Dyed BioDiesel  47%,  Diesel  53%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'350609f9-f612-46e6-b92d-8130cc359780', N'D48', N' Dyed BioDiesel  48%,  Diesel  52%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0bc735b8-8bd8-4155-84d6-988835bc0067', N'D49', N' Dyed BioDiesel  49%,  Diesel  51%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5d4da167-8b35-4e5d-8242-3f056a726302', N'D50', N' Dyed BioDiesel  50%,  Diesel  50%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'aa2292a6-377c-4ead-a046-d26c4ca3f614', N'D51', N' Dyed BioDiesel  51%,  Diesel  49%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8baf54cf-1d5f-4d0d-be08-d38142067724', N'D52', N' Dyed BioDiesel  52%,  Diesel  48%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0e4ed2f8-59d1-4b23-9322-a13c7c6b7c9c', N'D53', N' Dyed BioDiesel  53%,  Diesel  47%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3b8a77e7-d2b7-4907-8e44-2dc2e9524a2f', N'D54', N' Dyed BioDiesel  54%,  Diesel  46%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'34b77693-592f-4b60-bc36-36ea0669ab6f', N'D55', N' Dyed BioDiesel  55%,  Diesel  45%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f6da7f76-bab8-4a30-b726-60df7b755392', N'D56', N' Dyed BioDiesel  56%,  Diesel  44%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'31389aec-55a9-4357-ba12-c403e62343b5', N'D57', N' Dyed BioDiesel  57%,  Diesel  43%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9b5e6c04-0806-46f4-873c-3d8c43f84f7a', N'D58', N' Dyed BioDiesel  58%,  Diesel  42%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b9a4d027-49da-4678-8608-c412a55d6368', N'D59', N' Dyed BioDiesel  59%,  Diesel  41%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f0966284-22ac-417c-87da-69cbd8ba4ea3', N'D60', N' Dyed BioDiesel  60%,  Diesel  40%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'98120fa0-2afb-485f-a848-1593fd114e90', N'D61', N' Dyed BioDiesel  61%,  Diesel  39%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'19b01b7c-8621-4e27-96d9-0fda3e85f7aa', N'D62', N' Dyed BioDiesel  62%,  Diesel  38%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4ab03f66-3310-46cd-a82a-f479aa29b8ee', N'D63', N' Dyed BioDiesel  63%,  Diesel  37%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3ac8970f-adf7-491c-b55c-035fc0a57bb5', N'D64', N' Dyed BioDiesel  64%,  Diesel  36%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'40861a74-5653-4ce0-8ebc-fe219d3d2f97', N'D65', N' Dyed BioDiesel  65%,  Diesel  35%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'372628a5-4feb-4d29-9eef-cf550001dbe6', N'D66', N' Dyed BioDiesel  66%,  Diesel  34%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'dfd6923c-35eb-452a-8b2b-d3f47bc5a070', N'D67', N' Dyed BioDiesel  67%,  Diesel  33%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'340f732b-7151-4445-a06c-e5a556fa9624', N'D68', N' Dyed BioDiesel  68%,  Diesel  32%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c02562f9-f894-47d5-9a78-89527d524361', N'D69', N' Dyed BioDiesel  69%,  Diesel  31%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'35a7d5cb-52a1-4368-8f2b-6484a2b7113c', N'D70', N' Dyed BioDiesel  70%,  Diesel  30%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8a033d77-7749-419e-ac9c-6375fb6ff58b', N'D71', N' Dyed BioDiesel  71%,  Diesel  29%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'df6d61eb-cca5-4542-a206-63248597cc8a', N'D72', N' Dyed BioDiesel  72%,  Diesel  28%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'80226184-87da-4333-a5af-a45b1abb6e16', N'D73', N' Dyed BioDiesel  73%,  Diesel  27%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'663ed835-8838-4864-ba2c-e5d837d84272', N'D74', N' Dyed BioDiesel  74%,  Diesel  26%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'21cef55a-ec63-4703-b99e-fb1c4f53c89f', N'D75', N' Dyed BioDiesel  75%,  Diesel  25%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ddf7c74c-b3e7-4b47-8e74-fe6a4b3df486', N'D76', N' Dyed BioDiesel  76%,  Diesel  24%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6aadebe9-4efa-4a65-bf45-d3b09bce2362', N'D77', N' Dyed BioDiesel  77%,  Diesel  23%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f2762cc5-377a-4821-87f0-6ccd95cb839f', N'D78', N' Dyed BioDiesel  78%,  Diesel  22%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'392163ab-652b-497b-bf03-a3e6deb8dc6a', N'D79', N' Dyed BioDiesel  79%,  Diesel  21%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'70eb3300-d54f-46b6-90e1-91e2fa23252a', N'D80', N' Dyed BioDiesel  80%,  Diesel  20%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'12009087-6db3-4a02-9504-0e15183ef70b', N'D81', N' Dyed BioDiesel  81%,  Diesel  19%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'07aab024-56d8-42c7-9180-efd568c37e73', N'D82', N' Dyed BioDiesel  82%,  Diesel  18%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'704faf2a-73bb-48c9-a284-6237dd755cf1', N'D83', N' Dyed BioDiesel  83%,  Diesel  17%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3b0334ac-4246-44ea-8898-835d19099963', N'D84', N' Dyed BioDiesel  84%,  Diesel  16%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3d65c831-1938-449d-a8c4-6c9a4815ad39', N'D85', N' Dyed BioDiesel  85%,  Diesel  15%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8c6057c2-c8dc-49f7-84d4-36ec86e37c31', N'D86', N' Dyed BioDiesel  86%,  Diesel  14%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7d0d18d5-6249-454e-b5a3-94efec792dba', N'D87', N' Dyed BioDiesel  87%,  Diesel  13%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f80b612d-83a8-4542-98d7-034dc891ad05', N'D88', N' Dyed BioDiesel  88%,  Diesel  12%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5dcd1fcc-59cc-4047-80c8-c29c2033cdb7', N'D89', N' Dyed BioDiesel  89%,  Diesel  11%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3f3472d5-e29f-4dcc-b95e-dfd2746e3a90', N'D90', N' Dyed BioDiesel  90%,  Diesel  10%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'eee2840e-20ec-4da9-b483-f0976556f50d', N'D91', N' Dyed BioDiesel  91%,  Diesel  9%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'02e11777-048d-49d8-9e84-13600be0fde7', N'D92', N' Dyed BioDiesel  92%,  Diesel  8%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e2b5a6e5-d557-4e70-9607-f4f559de2029', N'D93', N' Dyed BioDiesel  93%,  Diesel  7%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3c0f7951-475c-4be2-8491-b5abd507cfa1', N'D94', N' Dyed BioDiesel  94%,  Diesel  6%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e7e97581-7575-4d03-994f-ec9138fd3954', N'D95', N' Dyed BioDiesel  95%,  Diesel  5%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'bb293639-e0ea-4087-b369-ff570360bf39', N'D96', N' Dyed BioDiesel  96%,  Diesel  4%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'cb9bc184-b311-4970-870c-701ad7d8cf7a', N'D97', N' Dyed BioDiesel  97%,  Diesel  3%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1ee37fb1-55f9-4fc8-8b7f-7624ebe9bd06', N'D98', N' Dyed BioDiesel  98%,  Diesel  2%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'535f8bd6-2ddb-4a13-a61a-6ccdb6f1d9c2', N'D99', N' Dyed BioDiesel  99%,  Diesel  1%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9aa7a12c-6cb7-4cd1-b5a7-a14ccd61c422', N'E00', N'Ethanol  100%,   Gasoline   0%', 0, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9e15f491-5106-4a4a-8a6b-0b8f2c8daa7a', N'E01', N'Ethanol  1%,   Gasoline   99%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7fb1d641-d721-4bbb-a9de-1f221343b79c', N'E02', N'Ethanol  2%,   Gasoline   98%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2259d725-1560-481c-b876-1e6fb04e2d08', N'E03', N'Ethanol  3%,   Gasoline   97%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9ec48d9a-1c8e-4021-a1da-1052c172a6b4', N'E04', N'Ethanol  4%,   Gasoline   96%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ca29ed6d-426c-4f51-a0d5-3898b0dcfe1b', N'E05', N'Ethanol  5%,   Gasoline   95%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'14497cc5-e892-47b9-b59d-c811fd0930d9', N'E06', N'Ethanol  6%,   Gasoline   94%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'25739b1d-f1c9-4e3e-adaa-4f6bd8d472b1', N'E07', N'Ethanol  7%,   Gasoline   93%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'84e33431-0395-41e9-be9f-627e9f97de62', N'E08', N'Ethanol  8%,   Gasoline   92%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c6e7091f-6716-49d3-bc8f-74d81a6ac9a5', N'E09', N'Ethanol  9%,   Gasoline   91%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9d63974b-256a-422f-882a-d906d64619fa', N'E10', N'Ethanol  10%,   Gasoline   90%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a8a1ff9a-436a-44be-871d-4a1026cdc27d', N'E11', N'Ethanol  11%,   Gasoline   89%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3ee6b5eb-7afe-4ec1-bbaa-83830d837c70', N'E12', N'Ethanol  12%,   Gasoline   88%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'709ccb19-3a56-4ad3-936e-6bface13357a', N'E13', N'Ethanol  13%,   Gasoline   87%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0bce68e3-5886-44ad-9868-37323755c670', N'E14', N'Ethanol  14%,   Gasoline   86%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f63bd1dc-8ca3-43c0-a9fa-3d75ba230eef', N'E15', N'Ethanol  15%,   Gasoline   85%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'41f4fee3-b877-43e7-9623-47654acb9135', N'E16', N'Ethanol  16%,   Gasoline   84%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3cc695e0-879e-42a9-85b9-f6fbdf5cbbfe', N'E17', N'Ethanol  17%,   Gasoline   83%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'13933f78-e99a-4b4c-9340-2a6c90edfab2', N'E18', N'Ethanol  18%,   Gasoline   82%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ea25192d-6ae1-47be-b8b5-4aac97fb56f0', N'E19', N'Ethanol  19%,   Gasoline   81%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'75d0d269-0a67-4851-8d8f-d7e491df68b8', N'E20', N'Ethanol  20%,   Gasoline   80%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd6bc4b50-ab1a-4da2-b00e-297d2bb2291b', N'E21', N'Ethanol  21%,   Gasoline   79%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0196fbb5-54b9-466e-ae6c-9b7ee83e8a93', N'E22', N'Ethanol  22%,   Gasoline   78%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'79689efa-3afa-41d6-9c7e-65286107df88', N'E23', N'Ethanol  23%,   Gasoline   77%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f01bb01f-af9c-4e35-a781-35b1b449df34', N'E24', N'Ethanol  24%,   Gasoline   76%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'54ffbe44-6e48-4a56-a0a4-5835f8d6f608', N'E25', N'Ethanol  25%,   Gasoline   75%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f72eae70-dbf7-4d23-80a3-95fcb8219fb2', N'E26', N'Ethanol  26%,   Gasoline   74%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e253309f-980d-4377-9b15-885cec9189ce', N'E27', N'Ethanol  27%,   Gasoline   73%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1bfcfc05-9e97-4508-9838-a230c8ae5602', N'E28', N'Ethanol  28%,   Gasoline   72%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8b910515-bf53-4b15-b01b-c3e7808cee54', N'E29', N'Ethanol  29%,   Gasoline   71%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'92d585c8-013f-47bb-a33b-1ca878f24b76', N'E30', N'Ethanol  30%,   Gasoline   70%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ca437a47-6bc4-4c0d-ad2c-34f510adb215', N'E31', N'Ethanol  31%,   Gasoline   69%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'db8d973b-b0a3-4ad0-bf41-39d0c2c9ca14', N'E32', N'Ethanol  32%,   Gasoline   68%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0b9b9ccf-3698-47c3-9c0b-84b7b2bbbf92', N'E33', N'Ethanol  33%,   Gasoline   67%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'99f3a4f7-ba7b-4708-9e2c-27599e8ae1a4', N'E34', N'Ethanol  34%,   Gasoline   66%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0201e6c5-4a4a-47b2-a56d-b1f7d77d60ef', N'E35', N'Ethanol  35%,   Gasoline   65%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2c8ebb46-304a-466e-8b4a-0c95d66fac94', N'E36', N'Ethanol  36%,   Gasoline   64%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0bc0a307-deb9-4bbc-9f30-a215049f8799', N'E37', N'Ethanol  37%,   Gasoline   63%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'aeab6f65-0e6f-4f4d-ae7b-0474564b9c41', N'E38', N'Ethanol  38%,   Gasoline   62%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1b765fdc-1948-4aca-9efd-e9fec4fa2b9c', N'E39', N'Ethanol  39%,   Gasoline   61%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b6609b42-34bc-4dd1-87b6-42fc33837888', N'E40', N'Ethanol  40%,   Gasoline   60%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'33920254-442a-45ca-bab4-c284d8d4ef77', N'E41', N'Ethanol  41%,   Gasoline   59%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1e6df8b7-6efd-4e91-9b9f-b796ceee17e2', N'E42', N'Ethanol  42%,   Gasoline   58%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7d78b6c1-4210-4457-88b6-c7d0af260049', N'E43', N'Ethanol  43%,   Gasoline   57%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9a39b05a-1eef-494e-8238-78b77de463a5', N'E44', N'Ethanol  44%,   Gasoline   56%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5744a692-aa69-47be-bd05-05afe4c1c85b', N'E45', N'Ethanol  45%,   Gasoline   55%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6b846e49-93b1-4145-babc-8dabaa6890b7', N'E46', N'Ethanol  46%,   Gasoline   54%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'feff3ed3-0866-425e-a3d9-0f68d00a24bc', N'E47', N'Ethanol  47%,   Gasoline   53%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'cf57ff76-a3e1-4488-a26f-00324217fddb', N'E48', N'Ethanol  48%,   Gasoline   52%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'628c06a3-8510-405b-b477-a4189b5f045c', N'E49', N'Ethanol  49%,   Gasoline   51%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8419443b-7d27-441d-ac9e-66d1b01f752b', N'E50', N'Ethanol  50%,   Gasoline   50%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0c62dab7-d3aa-45a7-9425-46c79504f94e', N'E51', N'Ethanol  51%,   Gasoline   49%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd43825b3-c006-41cf-8b89-fd0b13076062', N'E52', N'Ethanol  52%,   Gasoline   48%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd88a141d-8cca-4845-b2f4-9d7485bd0b2a', N'E53', N'Ethanol  53%,   Gasoline   47%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9f1dba84-93a0-4f83-99eb-aa2c71de0fbc', N'E54', N'Ethanol  54%,   Gasoline   46%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd2f219c5-be7b-4736-9c10-d50bc34b6204', N'E55', N'Ethanol  55%,   Gasoline   45%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9564fb13-4181-44fa-a369-54a68907947e', N'E56', N'Ethanol  56%,   Gasoline   44%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4bb76b09-b1f4-40be-9cae-ccba32c9934f', N'E57', N'Ethanol  57%,   Gasoline   43%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'99e12182-1813-4d4b-86b7-0e098639012d', N'E58', N'Ethanol  58%,   Gasoline   42%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'de2c3f43-1b5d-4958-bcfc-d5392a4c7bef', N'E59', N'Ethanol  59%,   Gasoline   41%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'52e832fa-069e-42f5-9ddc-1b6886915279', N'E60', N'Ethanol  60%,   Gasoline   40%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'10c1f47c-a953-4927-99ce-1f78e1f5e1ac', N'E61', N'Ethanol  61%,   Gasoline   39%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4c22a76f-ea3d-41a3-a924-92067adbcb2e', N'E62', N'Ethanol  62%,   Gasoline   38%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8e37de50-4a4f-4604-abc3-ca1542f4b5a1', N'E63', N'Ethanol  63%,   Gasoline   37%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e5abf99f-4d29-4a1c-be94-c37826888f82', N'E64', N'Ethanol  64%,   Gasoline   36%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6482bd56-0a5e-41d7-b785-0d807fd7e07e', N'E65', N'Ethanol  65%,   Gasoline   35%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7e9093d9-d592-4b11-bfbf-aa2566759107', N'E66', N'Ethanol  66%,   Gasoline   34%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c098d3e8-c774-419d-838f-a9d631c1558a', N'E67', N'Ethanol  67%,   Gasoline   33%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'50317a18-549e-42f9-b235-bcc842519a2f', N'E68', N'Ethanol  68%,   Gasoline   32%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'cb25bfa6-c14e-41ad-9f70-0569402bdc54', N'E69', N'Ethanol  69%,   Gasoline   31%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'adac505c-26b4-404d-9990-c1dc00ba37ae', N'E70', N'Ethanol  70%,   Gasoline   30%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'241a2601-d7ab-4bc6-9d15-854c527dc903', N'E71', N'Ethanol  71%,   Gasoline   29%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f475b3f9-8a71-49fa-8562-e5b961927c02', N'E72', N'Ethanol  72%,   Gasoline   28%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f2bb82f9-47bd-4f8a-aca5-b6149f452fcf', N'E73', N'Ethanol  73%,   Gasoline   27%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'325927f2-6189-492a-95a8-4d705608c404', N'E74', N'Ethanol  74%,   Gasoline   26%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'408584d6-d872-433d-b45f-2929c37560cf', N'E75', N'Ethanol  75%,   Gasoline   25%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'355b11f2-eb64-47f9-8105-e585dd004e62', N'E76', N'Ethanol  76%,   Gasoline   24%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'51e5cb2f-782e-4b86-ae43-41ff0bf85990', N'E77', N'Ethanol  77%,   Gasoline   23%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2fc32ffe-5896-4ce7-8f96-d5e221a9b0eb', N'E78', N'Ethanol  78%,   Gasoline   22%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5366f6ae-76c8-4de8-acb6-df69b53320b0', N'E79', N'Ethanol  79%,   Gasoline   21%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'acf65b69-9bed-4b72-b687-db6035621fa8', N'E80', N'Ethanol  80%,   Gasoline   20%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5dcbb146-a1ab-4cb7-80c6-15fb6114983b', N'E81', N'Ethanol  81%,   Gasoline   19%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b9c42a4e-07e7-4d3c-99e5-39e9d3043ea3', N'E82', N'Ethanol  82%,   Gasoline   18%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'42ccc792-5211-467a-9bda-42e20d62b760', N'E83', N'Ethanol  83%,   Gasoline   17%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b35d1325-7ed1-44f4-bd53-e2792379b171', N'E84', N'Ethanol  84%,   Gasoline   16%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'88707abd-ef7a-4010-bb1e-9b3941570a9c', N'E85', N'Ethanol  85%,   Gasoline   15%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b49dd561-9915-4aac-9905-398714b9f7c4', N'E86', N'Ethanol  86%,   Gasoline   14%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'17ce0107-20e6-4f1a-a47a-00859c575649', N'E87', N'Ethanol  87%,   Gasoline   13%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ffe77067-8bb7-46b6-83b0-91b557bb1c98', N'E88', N'Ethanol  88%,   Gasoline   12%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8fae0e65-fc8a-4783-85e4-1a8a8f95e2aa', N'E89', N'Ethanol  89%,   Gasoline   11%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2ae27c4a-c01e-4e03-883d-db7ee0f54f90', N'E90', N'Ethanol  90%,   Gasoline   10%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2c46ae86-79e6-44ca-9e3c-637a9c15236c', N'E91', N'Ethanol  91%,   Gasoline   9%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b826b18d-43ae-464a-9ac1-ab14392aa756', N'E92', N'Ethanol  92%,   Gasoline   8%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1f800336-0b45-4a59-af68-978656334a84', N'E93', N'Ethanol  93%,   Gasoline   7%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7972db9e-3480-4dce-890d-d9ab72622164', N'E94', N'Ethanol  94%,   Gasoline   6%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'55a43518-0388-43a2-9031-2379d019d278', N'E95', N'Ethanol  95%,   Gasoline   5%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a7778fda-6e1b-4c73-b380-dfdc8dbe4e13', N'E96', N'Ethanol  96%,   Gasoline   4%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'71742f73-ca02-4420-bb5c-d8156f46a4bf', N'E97', N'Ethanol  97%,   Gasoline   3%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b68f43f1-71d2-45b2-aabb-66c0301ccb9e', N'E98', N'Ethanol  98%,   Gasoline   2%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'dcf12358-9a2e-4ce7-84a3-0e351f59e779', N'E99', N'Ethanol  99%,   Gasoline   1%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'26604f02-95d4-4712-97ad-5500a368f87e', N'M00', N'Methanol  100%,   Gasoline   0%', 0, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5552d66c-a1ee-40ca-974a-9d9602af7801', N'M01', N'Methanol  1%,   Gasoline   99%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'480f4f2d-d206-489e-8d24-edd62d51714b', N'M02', N'Methanol  2%,   Gasoline   98%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'33896d0f-8f00-429d-8d0c-a6155292cbb1', N'M03', N'Methanol  3%,   Gasoline   97%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6f266580-67ef-4f8d-b830-7526e2fcb303', N'M04', N'Methanol  4%,   Gasoline   96%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f8ca7409-97e4-4aeb-9b42-495fe43754de', N'M05', N'Methanol  5%,   Gasoline   95%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'95d8c137-6b75-4e6b-a7d0-f2cce4366043', N'M06', N'Methanol  6%,   Gasoline   94%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'44514b4c-c5c4-4c68-8012-57cdb1a75ca0', N'M07', N'Methanol  7%,   Gasoline   93%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b54f38b1-ebe4-44bf-acd8-6dcc70472e3b', N'M08', N'Methanol  8%,   Gasoline   92%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6e821ecc-8bf2-44ab-b2f5-752c0ff5b736', N'M09', N'Methanol  9%,   Gasoline   91%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'aafeec6a-dee7-4e30-b8f5-169f768277c6', N'M10', N'Methanol  10%,   Gasoline   90%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4cbef56e-22c3-4b7e-a057-b4dfc7274a2a', N'M11', N'Methanol  11%,   Gasoline   89%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6ccea31b-c0c5-4781-b978-035d3bc13761', N'M12', N'Methanol  12%,   Gasoline   88%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f1d9ce96-b638-433d-bb5a-23bb308cd9cb', N'M13', N'Methanol  13%,   Gasoline   87%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'af10b6ef-1c67-43fe-9f75-ee812dbc75ce', N'M14', N'Methanol  14%,   Gasoline   86%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1d17b24a-a0d3-4103-82c0-1b8ff72c83d2', N'M15', N'Methanol  15%,   Gasoline   85%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'194f87e3-6024-4e02-aec1-ec90ce2fdaec', N'M16', N'Methanol  16%,   Gasoline   84%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ad84acba-5897-4a31-afdc-bb177d366fd3', N'M17', N'Methanol  17%,   Gasoline   83%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4961cb08-50c4-4978-9592-96c67ead07ea', N'M18', N'Methanol  18%,   Gasoline   82%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ba7d42ef-400b-4695-b473-8cb10ba948fa', N'M19', N'Methanol  19%,   Gasoline   81%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2a576d00-adc8-463e-b744-de80b1506ede', N'M20', N'Methanol  20%,   Gasoline   80%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1880d0f3-6f2c-4e12-a7e4-40b8d3dbd960', N'M21', N'Methanol  21%,   Gasoline   79%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd67256c1-4703-43de-bee4-0c43d1e2669a', N'M22', N'Methanol  22%,   Gasoline   78%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'478650cf-7c9e-4edd-b043-ec987bf42777', N'M23', N'Methanol  23%,   Gasoline   77%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b07a6c2f-a6c5-473d-9d71-11eb2d08a09e', N'M24', N'Methanol  24%,   Gasoline   76%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c3f104e2-01fd-4add-993a-0f09a4cc1f79', N'M25', N'Methanol  25%,   Gasoline   75%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'56c6ac8b-0a4c-4936-ad24-4b47b5cf2859', N'M26', N'Methanol  26%,   Gasoline   74%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b8596be3-ff16-4f71-9ca1-90d4aad433dc', N'M27', N'Methanol  27%,   Gasoline   73%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e289af47-eb57-46d9-a848-5936a870812d', N'M28', N'Methanol  28%,   Gasoline   72%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4a1862f9-786c-4beb-92a1-8c96a33d82dd', N'M29', N'Methanol  29%,   Gasoline   71%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2c93619d-242f-48fc-a7f8-14dadc9cdc3f', N'M30', N'Methanol  30%,   Gasoline   70%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8f142349-0e97-42be-8b47-e2056b602f99', N'M31', N'Methanol  31%,   Gasoline   69%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9159ee4c-0c76-4adf-8656-1ec0e6d468d4', N'M32', N'Methanol  32%,   Gasoline   68%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd6e5e233-5578-404d-944e-f50f558b6363', N'M33', N'Methanol  33%,   Gasoline   67%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'16508fbb-d1c0-4910-97d8-48c3484ee4c0', N'M34', N'Methanol  34%,   Gasoline   66%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'83e18bc6-7441-4c9d-8f72-fe7cd238af58', N'M35', N'Methanol  35%,   Gasoline   65%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'286775d0-6666-4ea6-a855-24aca6ebbbda', N'M36', N'Methanol  36%,   Gasoline   64%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'27ad590c-66a3-4d32-aa20-3c5b56ea39b1', N'M37', N'Methanol  37%,   Gasoline   63%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b9f85e36-1a01-4b3e-ba2a-f8059bc9ff96', N'M38', N'Methanol  38%,   Gasoline   62%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'10083cb1-8523-4028-95cc-a424d29c739d', N'M39', N'Methanol  39%,   Gasoline   61%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2a6d6d03-f002-410c-9ad2-0a38240034ff', N'M40', N'Methanol  40%,   Gasoline   60%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3312d54e-5f2e-47f4-b054-925ec89287bc', N'M41', N'Methanol  41%,   Gasoline   59%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'54df061f-fea8-4f91-810a-9e017150685e', N'M42', N'Methanol  42%,   Gasoline   58%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'432219ee-7189-4cb9-9979-76a854b315bc', N'M43', N'Methanol  43%,   Gasoline   57%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'34bb4776-1739-4a14-9a18-5d4790caa6bf', N'M44', N'Methanol  44%,   Gasoline   56%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'07f881f8-6c5c-4a54-8e13-08487da17caa', N'M45', N'Methanol  45%,   Gasoline   55%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7a5749a3-d7d7-474e-938d-f01830151bb6', N'M46', N'Methanol  46%,   Gasoline   54%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'fe58b957-f439-44dc-acf1-69a17e506b9d', N'M47', N'Methanol  47%,   Gasoline   53%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd591bac8-3d86-4263-ab38-ec2b483fc0c2', N'M48', N'Methanol  48%,   Gasoline   52%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'78c9548d-b959-4721-b5bb-3cc57b6d8384', N'M49', N'Methanol  49%,   Gasoline   51%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'79c593cc-4162-4bc4-b651-23de486ab9d9', N'M50', N'Methanol  50%,   Gasoline   50%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e6c02c42-1366-439d-81a1-4bc07a28f974', N'M51', N'Methanol  51%,   Gasoline   49%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'bc4b8754-3a0d-444c-b6c4-1085ea38b91d', N'M52', N'Methanol  52%,   Gasoline   48%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5c7e0fa3-ea33-4f9b-bd8c-493197be4df1', N'M53', N'Methanol  53%,   Gasoline   47%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ee52f38b-6a8b-4ab4-aab1-437a25daae26', N'M54', N'Methanol  54%,   Gasoline   46%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0c499587-d8b0-409b-8fb1-817df5cd6be5', N'M55', N'Methanol  55%,   Gasoline   45%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7aaca300-431e-478f-a7ad-3e076774ee5f', N'M56', N'Methanol  56%,   Gasoline   44%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9ab9b218-bbfd-4f7e-8352-a88044d677b2', N'M57', N'Methanol  57%,   Gasoline   43%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ee02521b-9e48-435c-87dc-7d6e0ffe47d9', N'M58', N'Methanol  58%,   Gasoline   42%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'aeac0e21-cbd1-430a-b063-9c54bfcd2d23', N'M59', N'Methanol  59%,   Gasoline   41%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4c82ef33-d38e-4113-99c6-79cd2a677170', N'M60', N'Methanol  60%,   Gasoline   40%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4931d392-3385-44f1-9575-3159fa73ef37', N'M61', N'Methanol  61%,   Gasoline   39%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a2da1d5c-a84d-4d76-9914-01969980938e', N'M62', N'Methanol  62%,   Gasoline   38%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'836af5bb-1df9-45dd-bc7f-14e755f737a8', N'M63', N'Methanol  63%,   Gasoline   37%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3889760d-9597-4f6b-9616-c3b1cae79c0e', N'M64', N'Methanol  64%,   Gasoline   36%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9e89a6b0-0140-423d-9c39-b314bc6bb498', N'M65', N'Methanol  65%,   Gasoline   35%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'734b7aea-e980-43b4-8dda-70c52d37f194', N'M66', N'Methanol  66%,   Gasoline   34%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'248173b0-79a1-451b-96ce-966b42d85810', N'M67', N'Methanol  67%,   Gasoline   33%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6a6a7b1b-5d96-441a-adf4-c3926a1e484a', N'M68', N'Methanol  68%,   Gasoline   32%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'811c83b2-420c-4ded-a742-09df05aefba2', N'M69', N'Methanol  69%,   Gasoline   31%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2e80a65a-c754-40b6-b2ae-59c4c72bb6a5', N'M70', N'Methanol  70%,   Gasoline   30%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5b9dc1cc-8c5a-4c69-a142-7426ea62d4dc', N'M71', N'Methanol  71%,   Gasoline   29%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9726fc80-9d35-44bc-b16c-31867e05f4bf', N'M72', N'Methanol  72%,   Gasoline   28%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'072d2e0b-03f6-4b4e-b356-c0f59f64c2b3', N'M73', N'Methanol  73%,   Gasoline   27%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'95f6f2fb-0253-46ca-b71e-dce3580d51f9', N'M74', N'Methanol  74%,   Gasoline   26%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a8351049-3302-4488-953a-b5f2c94326e5', N'M75', N'Methanol  75%,   Gasoline   25%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'df43b473-b0a3-4aa0-becf-4ef486891458', N'M76', N'Methanol  76%,   Gasoline   24%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7de8a96b-5b72-42cc-9fa1-479f6db797e3', N'M77', N'Methanol  77%,   Gasoline   23%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4587b31e-2da4-44d3-a72b-d330a04465ef', N'M78', N'Methanol  78%,   Gasoline   22%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'66131d93-d6bc-410a-bf28-bc4ed3ac5b8a', N'M79', N'Methanol  79%,   Gasoline   21%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e60adbc4-4977-42c2-a0b0-efa5e5d46125', N'M80', N'Methanol  80%,   Gasoline   20%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ee2e94ca-9058-4d3b-a2d1-be57226d4140', N'M81', N'Methanol  81%,   Gasoline   19%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ddaa02b1-1d92-4fe7-9152-8dc9d85b4213', N'M82', N'Methanol  82%,   Gasoline   18%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd95abc9a-9c3b-4742-b5b0-7fd189c43eed', N'M83', N'Methanol  83%,   Gasoline   17%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5de73738-d852-4b99-8eb3-5de33d382153', N'M84', N'Methanol  84%,   Gasoline   16%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'93a5c45c-c322-465a-9b5c-1a51c8c6579d', N'M85', N'Methanol  85%,   Gasoline   15%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'52bf2735-ad68-4247-8ec1-a32898ac1d8c', N'M86', N'Methanol  86%,   Gasoline   14%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7fa5d762-2ce5-494d-80a3-8b536792d9dc', N'M87', N'Methanol  87%,   Gasoline   13%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6b8d725d-5b51-4df9-ae6f-cd710151c105', N'M88', N'Methanol  88%,   Gasoline   12%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'644a6319-b00f-4885-9f43-0e1c44cad98d', N'M89', N'Methanol  89%,   Gasoline   11%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'456afeb8-3602-42b0-b063-1c47cc47e146', N'M90', N'Methanol  90%,   Gasoline   10%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f9065afb-ab8d-4190-98df-44e33c5ac2d3', N'M91', N'Methanol  91%,   Gasoline   9%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9bc567ea-8a54-462e-b6ff-5f6355b65209', N'M92', N'Methanol  92%,   Gasoline   8%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'08ee89c1-53f1-4df8-b8bc-b21d56505d30', N'M93', N'Methanol  93%,   Gasoline   7%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'66eddb39-80c0-4c52-8a19-5e9ae63d40bf', N'M94', N'Methanol  94%,   Gasoline   6%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a1e613c0-ee61-469a-bbe2-e6421e3ec717', N'M95', N'Methanol  95%,   Gasoline   5%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'857bf3a9-51d1-4d52-a558-312541a48efa', N'M96', N'Methanol  96%,   Gasoline   4%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'99c2e195-5030-4b09-91da-55d9e0466063', N'M97', N'Methanol  97%,   Gasoline   3%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4bd77454-3294-467c-9e77-c16491a11dcc', N'M98', N'Methanol  98%,   Gasoline   2%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'95aa3fde-48ba-4d37-af82-6895a4ab660b', N'M99', N'Methanol  99%,   Gasoline   1%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'91cfa627-f980-490d-b644-303ca115eff1', N'S00', N'Substitute Ethanol', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6e421b4d-31de-4d1c-9292-bd436586c840', N'S01', N'Substitute Crude (any)', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'efaf93cf-6c4f-4a8c-9a5b-eba3d8691406', N'S21', N'Substitute TAME', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4e8670c9-bbab-4a6d-861a-0a0747d2c21c', N'S23', N'Substitute Raffinates', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'84adcc18-e1d3-4d4f-8eac-5033a77de808', N'S25', N'Substitute Aviation Gasoline', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6017c8d7-c44b-41ef-bae0-5f3101a249e2', N'S26', N'Substitute Liquified Natural Gas', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1b28a1b5-b936-4518-aee2-b18dd0784576', N'S48', N'Substitute Benzene', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c770c789-bc58-4e1d-83ce-9c60e1a8072d', N'S49', N'Substitute Condensate (not Crude)', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3f2d297a-13fd-46f8-a4db-70a4ffefad9d', N'S50', N'Substitute ETBE', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1ed8e276-59e2-4ad5-a542-b28fb666d7cb', N'S52', N'Substitute Ethane', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5d0baf53-91ab-4fd7-b4a1-1e0c862e845b', N'S54', N'Substitute Propane', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ff2864b6-1455-453e-8183-69da0ca515d6', N'S65', N'Substitute Methane', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9cc4ffe8-d3da-4391-ad0a-7ae857a41a13', N'S75', N'Substitute Propylene', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd0dab1bf-7041-4ef3-a18e-ee6b9d67626a', N'S76', N'Substitute Xylene', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a38c181c-3ae9-4414-a740-3a802f192a22', N'S77', N'Substitute Excluded Liquid (Mineral Oil)', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ba867c9f-05e3-43cb-80fc-20c2a19bb2f4', N'S79', N'Substitute Marine Diesel Oil', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'960fd260-2df5-420a-9177-f37545395800', N'S80', N'Substitute Marine Gas Oil', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'eb3e6cd2-d94d-4102-a37c-ec5d6ac9f3c3', N'S81', N'Substitute Mineral Oils', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd26c4592-84ad-4db3-8067-727cbd9750a9', N'S88', N'Substitute Asphalt', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'53381b2c-4f1a-4c62-ac24-92dabc185e6e', N'S90', N'Substitute Additive – Miscellaneous', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b51c3c46-e386-45c4-9f9f-6a2966ae760e', N'S92', N'Substitute Undefined (Other) Product', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'cc00e6bc-a66a-4ef4-8ca6-0d91e501392d', N'S93', N'Substitute MTBE', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b6d116ed-c78a-4603-83c5-42bce92adeaf', N'S96', N'Substitute Ethylene', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'59617479-b4a6-4b20-aba8-f11c3c9ad5af', N'S98', N'Substitute Butylene', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5a933d81-ecff-4135-ba83-a01def257d98', N'S99', N'Substitute Toluene', 1, 0, 0, N'XXX', 1)
GO
