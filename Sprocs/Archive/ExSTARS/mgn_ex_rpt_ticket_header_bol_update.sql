--USE [Mgn_ExSTARS]
--GO

-- mgn_ex_rpt_ticket_header_bol_update

declare @document_key uniqueidentifier  = '6599E51C-922F-469A-A376-7863E5DB15C6'
--update tmp_import_data set document_key = '1b649df2-c7c3-406a-99b1-e4b98b2fb27c'


delete from rpt_ticket_header where th_document_key = @document_key;

INSERT INTO [dbo].[rpt_ticket_header]
           (--[_key]
           [th_document_key]
           ,[th_tranaction_type_code]
           ,[th_product_code]
		   ,[th_tx_product_code]
           ,[th_carrier_name]
           ,[th_carrier_fein]
		   ,th_carrier_tx_taxpayer
           ,[th_position_holder_name]
           ,[th_position_holder_fein]
		   ,th_position_holder_tx_taxpayer
           ,[th_origin_facility]
           ,[th_destination_facility]
           ,[th_destination_state]
           ,[th_consignor_name]
           ,[th_consignor_fein]
           ,[th_exchanger_name]
           ,[th_exchanger_fein])
    
SELECT DISTINCT
--newid(),		    --<_key, uniqueidentifier,>
@document_key,      --     ,<th_document_key, uniqueidentifier,>
tt.tt_tranaction_type_code,      --     ,<th_tranaction_type_code, varchar(10),>
tmp.product_code,     --     ,<th_product_code, varchar(3),>
tmp.tx_product_code,
tmp.carrier_name,      --     ,<th_carrier_name, varchar(35),>
tmp.carrier_fein,      --     ,<th_carrier_fein, varchar(9),>
tmp.carrier_tx_taxpayer,
tmp.position_holder_name,      --     ,<th_position_holder_name, varchar(35),>
tmp.position_holder_fein,      --     ,<th_position_holder_fein, varchar(9),>
tmp.position_holder_tx_taxpayer,
tmp.origin_facility, --     ,<th_origin_facility, varchar(9),>
tmp.destintion_facility,      --     ,<th_destination_facility, varchar(9),>
tmp.destination_state,      --     ,<th_destination_state, varchar(3),>
tmp.consignor_name,      --     ,<th_consignor_name, varchar(35),>
tmp.consignor_fein,      --     ,<th_consignor_fein, varchar(9),>
tmp.exchanger_name,      --     ,<th_exchanger_name, varchar(35),>
tmp.exchanger_fein      --     ,<th_exchanger_fein, varchar(9),>)
FROM rpt_tmp_import_data tmp LEFT OUTER JOIN 
	 rpt_transaction_types tt ON tmp.transaction_type = tt.tt_transaction_type 
	 AND tmp.transaction_mode = tt.tt_transaction_mode LEFT OUTER JOIN 
	 product_irs_codes p ON tmp.product_code = p.prd_irs_code
WHERE transaction_type IN ('TD','TR') AND tx_product_code <> 'XXX'


delete from rpt_ticket_bol where tb_document_key = @document_key;


INSERT INTO [dbo].[rpt_ticket_bol]
           ([_key]
           ,[tb_ticket_header_key]
           ,[tb_document_key]
           ,[tb_net_gallons]
           ,[tb_gross_gallons]
           ,[tb_bol]
           ,[tb_document_date]
           ,[tb_vessel_name]
           ,[tb_von_imo])
  
SELECT 
	newid(),		    --<_key, uniqueidentifier,>
	 th._key,     --     ,<tb_ticket_header_key, uniqueidentifier,>
	th.th_document_key,      --     ,<tb_document_key, uniqueidentifier,>
	tmp.net_gallons,--     ,<tb_net_gallons, bigint,>
	tmp.gross_gallons,      --     ,<tb_gross_gallons, bigint,>
	tmp.bol,      --     ,<tb_bol, varchar(15),>
	tmp.document_date,      --     ,<tb_document_date, date,>
	tmp.vessel_name,      --     ,<tb_vessel_name, varchar(35),>
	tmp.von_imo      --     ,<tb_von_imo, varchar(9),>
FROM rpt_tmp_import_data tmp  INNER JOIN 
     rpt_ticket_header th ON tmp.document_key = th.th_document_key
				         AND tmp.transaction_type_code = th.[th_tranaction_type_code]
				         AND tmp.product_code = th.[th_product_code]
				         AND tmp.tx_product_code = th.[th_tx_product_code]
				         AND tmp.carrier_name = th.[th_carrier_name]
				         AND tmp.carrier_fein = th.[th_carrier_fein]
				         AND tmp.position_holder_name = th.[th_position_holder_name]
				         AND tmp.position_holder_fein = th.[th_position_holder_fein]
				         AND tmp.origin_facility = th.[th_origin_facility]
				         AND tmp.destintion_facility = th.[th_destination_facility]
				         AND tmp.destination_state = th.[th_destination_state]
				         AND tmp.consignor_name = th.[th_consignor_name]
				         AND tmp.consignor_fein = th.[th_consignor_fein]
				         AND tmp.exchanger_name = th.[th_exchanger_name]
				         AND tmp.exchanger_fein = th.[th_exchanger_fein]
