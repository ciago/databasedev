/****** Object:  Table [dbo].[rpt_balance_sheet]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_balance_sheet](
	[_key] [uniqueidentifier] NOT NULL,
	[bs_product_code] [varchar](3) NOT NULL,
	[bs_irs_end_date] [date] NOT NULL,
	[bs_beginning_inventory] [bigint] NULL,
	[bs_receipts] [bigint] NULL,
	[bs_book_receipts] [bigint] NULL,
	[bs_book_disbursements] [bigint] NULL,
	[bs_disbursements] [bigint] NULL,
	[bs_ending_inventory] [bigint] NULL,
	[bs_gain_loss] [bigint] NULL,
	[bs_document_key] [uniqueidentifier] NOT NULL,
	[bs_ref_55] [int] IDENTITY(100000001,1) NOT NULL,
	[bs_tcn] [varchar](9) NULL,
	[bs_group_key] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_document_status]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_document_status](
	[_key] [uniqueidentifier] NOT NULL,
	[ds_status] [int] NOT NULL,
	[ds_status_description] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_documents]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_documents](
	[_key] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[doc_report_entity_key] [uniqueidentifier] NOT NULL,
	[doc_irs_end_date] [date] NOT NULL,
	[doc_report_type_key] [uniqueidentifier] NOT NULL,
	[doc_report_version_key] [uniqueidentifier] NOT NULL,
	[doc_creation_datetime] [datetime] NOT NULL,
	[doc_production] [bit] NOT NULL,
	[doc_isa13_iea02] [int] IDENTITY(100000001,1) NOT NULL,
	[doc_gs06_gee02] [int] NULL,
	[doc_st02_se02] [int] NULL,
	[doc_tia5001_tia5004_total] [bigint] NULL,
	[doc_terminal_sold_aquired] [bigint] NOT NULL,
	[doc_terminal_sold_aquired_date] [date] NULL,
	[doc_no_business_activity] [bit] NOT NULL,
	[doc_report_bi] [bit] NOT NULL,
	[doc_replacement_ref_fj] [int] NULL,
	[doc_status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_documents_group_reports]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_documents_group_reports](
	[_key] [uniqueidentifier] NOT NULL,
	[gdoc_document_key] [uniqueidentifier] NOT NULL,
	[gdoc_report_entity_key] [uniqueidentifier] NOT NULL,
	[gdoc_tia_5001_5004] [bigint] NULL,
	[gdoc_st_se_number] [varchar](9) NULL,
	[gdoc_ref_fj] [varchar](9) NULL,
	[gdoc_no_business_activity] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_error_codes]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_error_codes](
	[error_code] [tinyint] NOT NULL,
	[error_message] [varchar](125) NOT NULL,
	[must_fix] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_errors]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_errors](
	[error_code] [tinyint] NOT NULL,
	[error_field] [varchar](50) NOT NULL,
	[error_data] [varchar](50) NULL,
	[tmp_import_row_key] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[rpt_report_entity]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_report_entity](
	[_key] [uniqueidentifier] NOT NULL,
	[re_name] [varchar](35) NOT NULL,
	[re_additional_name] [varchar](35) NULL,
	[re_fein] [varchar](18) NOT NULL,
	[re_tx_taxpayer_number] [varchar](11) NULL,
	[re_tcn] [varchar](9) NULL,
	[re_637_registration] [varchar](18) NULL,
	[re_physical_address1] [varchar](35) NOT NULL,
	[re_physical_address2] [varchar](35) SPARSE  NULL,
	[re_physical_city] [varchar](30) NOT NULL,
	[re_physical_state] [varchar](2) NOT NULL,
	[re_physical_zip] [varchar](11) NOT NULL,
	[re_physical_country] [varchar](3) NOT NULL,
	[re_mailing_address1] [varchar](35) NULL,
	[re_mailing_address2] [varchar](35) SPARSE  NULL,
	[re_mailing_city] [varchar](30) NULL,
	[re_mailing_state] [varchar](2) NULL,
	[re_mailing_zip] [varchar](11) NULL,
	[re_mailing_country] [varchar](3) NULL,
	[re_general_contact_name] [varchar](35) NOT NULL,
	[re_general_contact_phone] [varchar](14) NOT NULL,
	[re_general_contact_fax] [varchar](10) NULL,
	[re_general_contact_email] [varchar](80) NOT NULL,
	[re_edi_contact_name] [varchar](35) NULL,
	[re_edi_contact_phone] [varchar](14) NULL,
	[re_edi_contact_fax] [varchar](10) NULL,
	[re_edi_contact_email] [varchar](80) NULL,
	[re_fed_isa02] [varchar](10) NULL,
	[re_fed_isa04] [varchar](10) NOT NULL,
	[re_fed_isa05] [varchar](2) NOT NULL,
	[re_fed_isa06] [varchar](15) NOT NULL,
	[re_fed_gs02] [varchar](15) NOT NULL,
	[re_is_group] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_report_entity_group_members]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_report_entity_group_members](
	[rptg_key] [uniqueidentifier] NOT NULL,
	[rptg_report_entity_key] [uniqueidentifier] NOT NULL,
	[rptg_name] [varchar](50) NULL,
	[rptg_terminal] [bit] NOT NULL,
	[rptg_carrier] [bit] NOT NULL,
	[rptg_facility_number] [varchar](9) NULL,
	[rptg_637_registration] [varchar](18) NULL,
	[rptg_physical_address1] [varchar](35) NULL,
	[rptg_city] [varchar](30) NULL,
	[rptg_state] [varchar](2) NULL,
	[rptg_zip] [varchar](11) NULL,
	[rptg_country] [varchar](3) NULL,
	[rptg_delete_flg] [bit] NULL,
	[rptg_number] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_report_versions]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_report_versions](
	[_key] [uniqueidentifier] NOT NULL,
	[rpt_key] [uniqueidentifier] NULL,
	[rpt_version] [nvarchar](2) NULL,
	[rpt_version_description] [nvarchar](20) NULL,
	[rpt_is_original] [bit] NOT NULL,
	[rpt_is_correction] [bit] NOT NULL,
	[rpt_is_replacement] [bit] NOT NULL,
	[rpt_is_supplemental] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_reports]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_reports](
	[_key] [uniqueidentifier] NOT NULL,
	[rpt_name] [varchar](50) NOT NULL,
	[rpt_type] [varchar](50) NOT NULL,
	[rpt_description] [varchar](400) NOT NULL,
	[rpt_is_federal] [bit] NOT NULL,
	[rpt_is_state] [bit] NOT NULL,
	[rpt_active] [bit] NOT NULL,
 CONSTRAINT [PK_reports] PRIMARY KEY CLUSTERED 
(
	[_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_state_codes]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_state_codes](
	[_key] [uniqueidentifier] NOT NULL,
	[st_abbreviation] [varchar](3) NOT NULL,
	[st_description] [varchar](50) NOT NULL,
	[st_external_id_key] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_ticket_bol]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_ticket_bol](
	[_key] [uniqueidentifier] NOT NULL,
	[tb_ticket_header_key] [uniqueidentifier] NOT NULL,
	[tb_document_key] [uniqueidentifier] NOT NULL,
	[tb_net_gallons] [bigint] NULL,
	[tb_gross_gallons] [bigint] NULL,
	[tb_bol] [varchar](15) NULL,
	[tb_document_date] [date] NULL,
	[tb_vessel_name] [varchar](35) NULL,
	[tb_von_imo] [varchar](9) NULL,
	[tb_ref_55] [int] IDENTITY(500000001,1) NOT NULL,
 CONSTRAINT [PK_ticket_bol] PRIMARY KEY CLUSTERED 
(
	[_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_ticket_header]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_ticket_header](
	[_key] [uniqueidentifier] NOT NULL,
	[th_document_key] [uniqueidentifier] NOT NULL,
	[th_tranaction_type_code] [varchar](10) NOT NULL,
	[th_product_code] [varchar](3) NOT NULL,
	[th_tx_product_code] [varchar](3) NULL,
	[th_carrier_name] [varchar](35) NULL,
	[th_carrier_fein] [varchar](9) NULL,
	[th_carrier_tx_taxpayer] [varchar](11) NULL,
	[th_position_holder_name] [varchar](35) NULL,
	[th_position_holder_fein] [varchar](9) NULL,
	[th_position_holder_tx_taxpayer] [varchar](11) NULL,
	[th_origin_facility] [varchar](9) NULL,
	[th_destination_facility] [varchar](9) NULL,
	[th_destination_state] [varchar](3) NULL,
	[th_consignor_name] [varchar](35) NULL,
	[th_consignor_fein] [varchar](9) NULL,
	[th_exchanger_name] [varchar](35) NULL,
	[th_exchanger_fein] [varchar](9) NULL,
	[th_ref_55] [int] IDENTITY(400000001,1) NOT NULL,
 CONSTRAINT [PK_ticket_header] PRIMARY KEY CLUSTERED 
(
	[_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_tmp_import_data]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_tmp_import_data](
	[transaction_type] [varchar](3) NULL,
	[transaction_type_code] [varchar](10) NULL,
	[product_code] [varchar](3) NULL,
	[tx_product_code] [varchar](3) NULL,
	[transaction_mode] [varchar](3) NULL,
	[net_gallons] [bigint] NULL,
	[gross_gallons] [bigint] NULL,
	[carrier_name] [varchar](35) NULL,
	[carrier_fein] [varchar](9) NULL,
	[carrier_tx_taxpayer] [varchar](20) NULL,
	[position_holder_name] [varchar](35) NULL,
	[position_holder_fein] [varchar](9) NULL,
	[position_holder_tx_taxpayer] [varchar](20) NULL,
	[bol] [varchar](15) NULL,
	[document_date] [datetime] NULL,
	[origin_facility] [varchar](9) NULL,
	[destintion_facility] [varchar](9) NULL,
	[destination_state] [varchar](3) NULL,
	[vessel_name] [varchar](35) NULL,
	[von_imo] [varchar](9) NULL,
	[consignor_name] [varchar](35) NULL,
	[consignor_fein] [varchar](9) NULL,
	[exchanger_name] [varchar](35) NULL,
	[exchanger_fein] [varchar](9) NULL,
	[_key] [uniqueidentifier] NULL,
	[document_key] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rpt_transaction_types]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rpt_transaction_types](
	[tt_tranaction_type_code] [varchar](10) NOT NULL,
	[tt_transaction_type] [varchar](3) NOT NULL,
	[tt_transaction_mode] [varchar](2) NOT NULL,
	[tt_report_type] [varchar](5) NOT NULL,
	[tt_schedule_code] [varchar](10) NULL,
	[tt_description] [nvarchar](100) NULL,
	[tt_require_carrier] [bit] NULL,
	[tt_allow_carrier_override] [bit] NULL,
	[tt_allow_carrier_9s] [bit] NULL,
	[tt_allow_carrier_4s] [bit] NULL,
	[tt_require_consignor] [bit] NULL,
	[tt_require_destination] [bit] NULL,
	[tt_require_position_holder] [bit] NULL,
	[tt_require_origin] [bit] NULL,
	[tt_require_ship_to] [bit] NULL,
	[tt_require_exchanger] [bit] NULL,
	[tt_require_bol] [bit] NULL,
	[tt_require_document_date] [bit] NULL,
	[tt_require_vessel] [bit] NULL,
	[tt_require_net_gallon] [bit] NULL,
	[tt_require_gross_gallon] [bit] NULL,
	[tt_is_bulk] [bit] NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[rpt_balance_sheet] ON 
GO
INSERT [dbo].[rpt_balance_sheet] ([_key], [bs_product_code], [bs_irs_end_date], [bs_beginning_inventory], [bs_receipts], [bs_book_receipts], [bs_book_disbursements], [bs_disbursements], [bs_ending_inventory], [bs_gain_loss], [bs_document_key], [bs_ref_55], [bs_tcn], [bs_group_key]) VALUES (N'e9b70ece-781d-462e-9313-23555a50ba29', N'142', CAST(N'2021-02-28' AS Date), 249472, 0, 0, 0, 0, 249472, 6098, N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 100000019, N'T76TX2850', NULL)
GO
INSERT [dbo].[rpt_balance_sheet] ([_key], [bs_product_code], [bs_irs_end_date], [bs_beginning_inventory], [bs_receipts], [bs_book_receipts], [bs_book_disbursements], [bs_disbursements], [bs_ending_inventory], [bs_gain_loss], [bs_document_key], [bs_ref_55], [bs_tcn], [bs_group_key]) VALUES (N'c58488c2-bf2c-4826-9297-576e075b9641', N'228', CAST(N'2021-02-28' AS Date), 683762, 1050364, 587717, 587717, 1693028, 683762, 2813, N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 100000020, N'T76TX2850', NULL)
GO
INSERT [dbo].[rpt_balance_sheet] ([_key], [bs_product_code], [bs_irs_end_date], [bs_beginning_inventory], [bs_receipts], [bs_book_receipts], [bs_book_disbursements], [bs_disbursements], [bs_ending_inventory], [bs_gain_loss], [bs_document_key], [bs_ref_55], [bs_tcn], [bs_group_key]) VALUES (N'af11e11a-0323-4c44-b5fa-199c6a8dca57', N'142', CAST(N'2021-03-31' AS Date), 249472, 0, 0, 0, 0, 250404, 932, N'32f71106-2405-4d16-8317-bac93dae0334', 100000021, N'T76TX2850', NULL)
GO
INSERT [dbo].[rpt_balance_sheet] ([_key], [bs_product_code], [bs_irs_end_date], [bs_beginning_inventory], [bs_receipts], [bs_book_receipts], [bs_book_disbursements], [bs_disbursements], [bs_ending_inventory], [bs_gain_loss], [bs_document_key], [bs_ref_55], [bs_tcn], [bs_group_key]) VALUES (N'b0d022f2-7dd9-4ed5-8cfe-0bbee9808e1e', N'228', CAST(N'2021-03-31' AS Date), 683762, 849344, 222935, 222935, 917990, 622227, 7111, N'32f71106-2405-4d16-8317-bac93dae0334', 100000022, N'T76TX2850', NULL)
GO
SET IDENTITY_INSERT [dbo].[rpt_balance_sheet] OFF
GO
INSERT [dbo].[rpt_document_status] ([_key], [ds_status], [ds_status_description]) VALUES (N'168a6d27-6374-4531-b8e8-cb6bb3eb7f08', 1, N'Created')
GO
INSERT [dbo].[rpt_document_status] ([_key], [ds_status], [ds_status_description]) VALUES (N'9dd9fb66-99fb-4f9b-ab50-bcbcf3edb8a7', 2, N'Imported')
GO
INSERT [dbo].[rpt_document_status] ([_key], [ds_status], [ds_status_description]) VALUES (N'170093dd-d5d2-4f3f-a482-e32a5f1cb166', 3, N'Approved')
GO
INSERT [dbo].[rpt_document_status] ([_key], [ds_status], [ds_status_description]) VALUES (N'c98a31bf-360d-4873-8899-3a3a88b43ca2', 4, N'Edi Created ')
GO
INSERT [dbo].[rpt_document_status] ([_key], [ds_status], [ds_status_description]) VALUES (N'e160f3b8-21a7-4343-831f-cf30974fb5f1', 5, N'Submitted')
GO
SET IDENTITY_INSERT [dbo].[rpt_documents] ON 
GO
INSERT [dbo].[rpt_documents] ([_key], [doc_report_entity_key], [doc_irs_end_date], [doc_report_type_key], [doc_report_version_key], [doc_creation_datetime], [doc_production], [doc_isa13_iea02], [doc_gs06_gee02], [doc_st02_se02], [doc_tia5001_tia5004_total], [doc_terminal_sold_aquired], [doc_terminal_sold_aquired_date], [doc_no_business_activity], [doc_report_bi], [doc_replacement_ref_fj], [doc_status]) VALUES (N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', N'1a796fdd-b065-4b49-80db-67f3ba5f1c65', CAST(N'2021-02-28' AS Date), N'0de727bc-045f-49a9-98b4-ee62975c614a', N'bba0de85-4e6e-4702-8c8c-ba1a41084158', CAST(N'2021-03-22T19:11:25.163' AS DateTime), 1, 100000001, 100000001, 100000001, NULL, 0, NULL, 0, 0, NULL, 1)
GO
INSERT [dbo].[rpt_documents] ([_key], [doc_report_entity_key], [doc_irs_end_date], [doc_report_type_key], [doc_report_version_key], [doc_creation_datetime], [doc_production], [doc_isa13_iea02], [doc_gs06_gee02], [doc_st02_se02], [doc_tia5001_tia5004_total], [doc_terminal_sold_aquired], [doc_terminal_sold_aquired_date], [doc_no_business_activity], [doc_report_bi], [doc_replacement_ref_fj], [doc_status]) VALUES (N'32f71106-2405-4d16-8317-bac93dae0334', N'1a796fdd-b065-4b49-80db-67f3ba5f1c65', CAST(N'2021-03-31' AS Date), N'0de727bc-045f-49a9-98b4-ee62975c614a', N'bba0de85-4e6e-4702-8c8c-ba1a41084158', CAST(N'2021-04-21T06:36:16.793' AS DateTime), 1, 100000002, 100000002, 100000002, NULL, 0, NULL, 0, 0, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[rpt_documents] OFF
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (1, N'Info: Removed field not required', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (2, N'Must Fix: Required field is missing   ', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (3, N'Info: Removed special characters', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (4, N'Must Fix: Non-numeric data in numeric field', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (5, N'Info: Gallon amount rounded ', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (6, N'Info:  Name is too long, removed characters from the end of the name', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (7, N'Must Fix:  FEIN is more than 9 digits', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (8, N'Info:  Added 0s to front of FEIN to make it the required length', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (9, N'Must Fix:  Facility ID is not the correct length', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (10, N'Must Fix:  Facility ID is not in the correct format', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (11, N'Info:  Set Facility ID to Filing Facility ID', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (12, N'Must Fix: Incorrect  or missing transaction type', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (13, N'Must Fix: Incorrect or missing transaction mode', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (14, N'Info:  Added 0"s to the front of the product code to make it the required length', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (15, N'Must Fix:  Invalid IRS Product Code', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (16, N'Must Fix:  Book Adjustment not allowed for this product code', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (17, N'Must Fix: Can not use the transaction mode CE for this product code', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (18, N'Info: Beginning Inventory changed to reflect Ending Inventory from prior month', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (19, N'Must Fix: Beginning and Ending Inventories cannot be less than 0', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (20, N'Must Fix: More than one transaction per product code is not allowed', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (21, N'Info:  Carrier information changed to reporting entity information per IRS rules', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (22, N'Must Fix:  FEIN reported but missing corresponding name', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (23, N'Must Fix: Name reported but missing corresponding FEIN', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (24, N'Must Fix: FEIN values- 777777777 and 888888888 are no longer valid', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (25, N'Must Fix:  FEIN value of 444444444 not allowed for this transaction type', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (26, N'Must Fix: FEIN value of 999999999 not allowed for this transaction type', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (27, N'Must Fix: This product code requires position holder information', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (28, N'Must Fix: Not a valid State or Country Code', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (29, N'Info:  BOL made to "Summary" per IRS rules', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (30, N'Info:  BOL made to "RECLASS" per IRS rules', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (31, N'Must Fix:  Not a valid date', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (32, N'Must Fix: Document date not allowed in this report', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (33, N'Must Fix: Vessel Number is more than 9 characters long', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (34, N'Info: Removed transaction with 0 gallon amount', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (35, N'Info:  Gross gallon amount of 0 is not valid', 0)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (36, N'Must Fix:  Book Adjustment totals must add up to 0', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (37, N'Invalid Transaction Mode and Transaction Type combination', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (38, N'Invalid VON', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (39, N'Must Fix: Invalid Alt Type', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (40, N'MustFix: Missing Destination State', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (41, N'Must Fix: Destination Alt ID missing', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (42, N'Must Fix: Missing Origin State', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (43, N'Must Fix: TCN is not valid for the current Filing Entity setup', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (44, N'Must Fix: No transactions for Entity and not a no business activity report', 1)
GO
INSERT [dbo].[rpt_error_codes] ([error_code], [error_message], [must_fix]) VALUES (45, N'Must Fix: Transactions exist for a no business activity entity', 1)
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (30, N'BOL', N'RECLASS', N'f654036e-44a4-43dd-b7f1-631aa1cd5dba')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (30, N'BOL', N'RECLASS', N'4ce0569f-6778-4dc3-9d75-6033d8bb3634')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'PositionHolder Data', N'ALLIED PETROCHEMICAL LLC', N'4191e9ad-f9ae-4c72-92a8-a9f96b118af0')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'PositionHolder Data', N'ORIGIN TEXAS RECYCLING LLC', N'9ab57815-ff6e-431a-bcfb-8f481b8f7b81')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'PositionHolder Data', N'O''ROURKE MARINE SERVICES, LLC', N'90e3a8fa-92cf-480b-8022-b9e8b0dd3491')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'PositionHolder Data', N'O''ROURKE MARINE SERVICES, LLC', N'5b30926e-789d-4c26-9650-5bcb0eaa3776')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'PositionHolder Data', N'O''ROURKE MARINE SERVICES, LLC', N'f4dcbde2-c74b-4ebb-b049-10301d254f20')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'PositionHolder Data', N'O''ROURKE MARINE SERVICES, LLC', N'f654036e-44a4-43dd-b7f1-631aa1cd5dba')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'Vessel Name and/or von_imo', N'OMS460-1291327', N'90e3a8fa-92cf-480b-8022-b9e8b0dd3491')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'Vessel Name and/or von_imo', N'OMS460-1291327', N'5b30926e-789d-4c26-9650-5bcb0eaa3776')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'Vessel Name and/or von_imo', N'OMS460-1291327', N'f4dcbde2-c74b-4ebb-b049-10301d254f20')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'Vessel Name and/or von_imo', N'K301-1123005', N'e606b77b-38f1-4d45-b7f8-dd754c5886fb')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'Vessel Name and/or von_imo', N'G115-1073577', N'50a2f078-ef8f-4a78-ab57-e2029b2d1f40')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'Vessel Name and/or von_imo', N'G115-1073577', N'f21eb504-191e-4eba-9810-a3b3b2996c28')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'Vessel Name and/or von_imo', N'JAM440-946314', N'8622f4ce-90ed-4859-a859-e344a745c387')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'5313', N'4191e9ad-f9ae-4c72-92a8-a9f96b118af0')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'993482', N'9ab57815-ff6e-431a-bcfb-8f481b8f7b81')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'576525', N'90e3a8fa-92cf-480b-8022-b9e8b0dd3491')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'249613', N'5b30926e-789d-4c26-9650-5bcb0eaa3776')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'23206', N'f4dcbde2-c74b-4ebb-b049-10301d254f20')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'587717', N'f654036e-44a4-43dd-b7f1-631aa1cd5dba')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'19593', N'c874b746-d215-452d-9ae9-5d0a57dea576')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'378270', N'e606b77b-38f1-4d45-b7f8-dd754c5886fb')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'45000', N'c36b6620-0d15-456d-a367-592cbe304d15')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'252074', N'50a2f078-ef8f-4a78-ab57-e2029b2d1f40')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'352602', N'8622f4ce-90ed-4859-a859-e344a745c387')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'587717', N'4ce0569f-6778-4dc3-9d75-6033d8bb3634')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'19275', N'e97e1b49-0953-4716-bff0-0694eccdaad2')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'37471', N'255dcd00-9712-4ec5-ab30-b081cd946250')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'16917', N'dfcb5e5b-45c7-47d2-9f96-f3ee805c65e3')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'21267', N'23c90da2-0519-45aa-9355-b7cec921f543')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'26600', N'd6029f75-dbbb-4f71-91e9-105fa34dc48d')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'9965', N'c3f0e66a-9a28-47af-b817-9830fffc780b')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'275', N'0d844592-d7ae-4d19-8415-2ad586c70345')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'5211', N'3458269a-b0fc-4274-9d06-6769951586ba')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'15309', N'05988fee-a282-47bd-8a99-4734a26cfa5f')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'24596', N'277f6bef-96f9-4072-913a-d24c7f7a40a0')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'4003', N'51f116ac-f9b3-494d-b398-f06c139d72c3')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'33350', N'0c1bb545-7fe9-40cd-975d-cd89b795c2c3')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'31861', N'95f3dc43-5697-491a-aa17-9ca3a6598c88')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'10007', N'a3a468f4-8437-4a13-b52d-adb57c554a7e')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'5736', N'92a44966-916c-43d7-aead-115deb09b92a')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'14267', N'49801f1b-dbcd-4f23-a1cf-be6843406512')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'22897', N'816dc8b2-07b6-4365-ac39-a90722a07c37')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'29748', N'147681c9-bcf4-433f-9fc3-0e80ce43f4ac')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'21087', N'27e96cac-f068-4f9a-978f-63444fbe4124')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'26374', N'cacbed76-3f56-4e93-b30d-a2d4ace1e8cd')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'3296', N'5e591b24-1260-4e84-90a3-2c4be2ffe931')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'12267', N'74f903d5-1787-4179-9e8d-bd91442a678e')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'5343', N'11ef4da0-ed8c-451c-a685-93c6a32e0bad')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'32120', N'7ffada7c-5ada-42ef-a326-b56d284727fb')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'25774', N'489d41ac-67b0-4134-a882-21d5a66f5bd7')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'21111', N'6c9da035-ab23-4081-88f6-d5325d5e1965')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'35239', N'f9724152-9702-4704-8b39-df3016b26435')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'10880', N'ceff32bb-4feb-44ff-ae19-07c0b35ac251')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'23084', N'2193f01f-dadf-4b67-8486-05c830f98936')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (1, N'gross_gallons', N'20058', N'2ce39240-c3aa-4f53-9bab-918d03c89ee3')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (2, N'net_gallons', N'0', N'f21eb504-191e-4eba-9810-a3b3b2996c28')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (8, N'carrier_tx_taxpayer', N'741074014', N'e606b77b-38f1-4d45-b7f8-dd754c5886fb')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (34, N'net_gallons', N'0', N'f21eb504-191e-4eba-9810-a3b3b2996c28')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'e3656cc0-3799-4490-9fc9-0a0e5a462070')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'32b7789f-7e54-4fca-9a01-3210047fbfcf')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'38a3c72c-984b-41d3-95b4-77357dd9fc1f')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'983fef2c-1e36-4b03-bc10-9d73c8877a28')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'42961697-a16b-4b37-9e17-4f0a717a5163')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'b1eefbef-63b4-4c47-835a-4995c2a2930f')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'4f285139-caa9-46fe-b5b3-9f64c8481bc5')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'cd9fdf7b-140a-4b3b-abf2-4a12bc61d625')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'04f478ea-c05e-462a-850c-61e7847e4859')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'7971800e-8e88-432b-8f00-4f5f8db9c4ab')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'5d47a6cf-29f3-446e-86f6-11ddc5b9cb4e')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'ba9d96b6-0b0d-4a6d-9af6-a38d11ae28b7')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'90bcd501-3f00-4425-8222-72b509ce80f6')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'1649b3df-f188-48e6-b5e9-ff9fb3a63095')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'a0537537-49b8-421e-a339-a56948e415ce')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'af28c7ab-9b4d-4c62-aa90-6917355a4603')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'4191e9ad-f9ae-4c72-92a8-a9f96b118af0')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'9ab57815-ff6e-431a-bcfb-8f481b8f7b81')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'90e3a8fa-92cf-480b-8022-b9e8b0dd3491')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'5b30926e-789d-4c26-9650-5bcb0eaa3776')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'f4dcbde2-c74b-4ebb-b049-10301d254f20')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'f654036e-44a4-43dd-b7f1-631aa1cd5dba')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'c874b746-d215-452d-9ae9-5d0a57dea576')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'e606b77b-38f1-4d45-b7f8-dd754c5886fb')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'c36b6620-0d15-456d-a367-592cbe304d15')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'50a2f078-ef8f-4a78-ab57-e2029b2d1f40')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'8622f4ce-90ed-4859-a859-e344a745c387')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'4ce0569f-6778-4dc3-9d75-6033d8bb3634')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'e97e1b49-0953-4716-bff0-0694eccdaad2')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'255dcd00-9712-4ec5-ab30-b081cd946250')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'dfcb5e5b-45c7-47d2-9f96-f3ee805c65e3')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'23c90da2-0519-45aa-9355-b7cec921f543')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'd6029f75-dbbb-4f71-91e9-105fa34dc48d')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'c3f0e66a-9a28-47af-b817-9830fffc780b')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'0d844592-d7ae-4d19-8415-2ad586c70345')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'3458269a-b0fc-4274-9d06-6769951586ba')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'05988fee-a282-47bd-8a99-4734a26cfa5f')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'277f6bef-96f9-4072-913a-d24c7f7a40a0')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'51f116ac-f9b3-494d-b398-f06c139d72c3')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'0c1bb545-7fe9-40cd-975d-cd89b795c2c3')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'95f3dc43-5697-491a-aa17-9ca3a6598c88')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'a3a468f4-8437-4a13-b52d-adb57c554a7e')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'92a44966-916c-43d7-aead-115deb09b92a')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'49801f1b-dbcd-4f23-a1cf-be6843406512')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'816dc8b2-07b6-4365-ac39-a90722a07c37')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'147681c9-bcf4-433f-9fc3-0e80ce43f4ac')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'27e96cac-f068-4f9a-978f-63444fbe4124')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'cacbed76-3f56-4e93-b30d-a2d4ace1e8cd')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'5e591b24-1260-4e84-90a3-2c4be2ffe931')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'74f903d5-1787-4179-9e8d-bd91442a678e')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'11ef4da0-ed8c-451c-a685-93c6a32e0bad')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'7ffada7c-5ada-42ef-a326-b56d284727fb')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'489d41ac-67b0-4134-a882-21d5a66f5bd7')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'6c9da035-ab23-4081-88f6-d5325d5e1965')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'f9724152-9702-4704-8b39-df3016b26435')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'ceff32bb-4feb-44ff-ae19-07c0b35ac251')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'2193f01f-dadf-4b67-8486-05c830f98936')
GO
INSERT [dbo].[rpt_errors] ([error_code], [error_field], [error_data], [tmp_import_row_key]) VALUES (35, N'gross_gallons', N'0', N'2ce39240-c3aa-4f53-9bab-918d03c89ee3')
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0e629ca9-4f45-4cf5-a936-b00075e30ec4', N'001', N'Crude', 0, 1, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9c9382ad-180c-415d-8c32-4f303065ca9e', N'049', N'Condensate (not Crude)', 0, 1, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'56a7cf32-86f7-4220-a51f-a8a0137fed2d', N'052', N'Ethane', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a86c45d3-f36e-43c8-b515-2202fd94440a', N'054', N'Propane', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'fe4aac35-9a97-4213-89a2-8dbc7b7d9769', N'055', N'Butane, Including Butane Propane Mix', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1eda07d6-04cc-4988-953e-927db3fe013f', N'058', N'Isobutane', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'77120acc-28f9-4d1d-806f-fb309cf25e54', N'059', N'Pentanes, Including Isopentane', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9e9679f0-ac39-46d6-9e17-3f58eb82fd1a', N'065', N'Gasoline', 1, 0, 0, N'065', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5ff4f56c-f847-455b-9033-2238ff81563c', N'073', N'Kerosene Low Sulphur Dyed', 1, 0, 0, N'072', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'295e7525-f7f4-452d-9b12-e68f7867dd77', N'074', N'Kerosene High Sulphur Dyed ', 1, 0, 0, N'072', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'fe8280b6-bd36-4be1-9e64-90168f624cea', N'075', N'Propylene', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'71f0276e-9453-4af6-b02b-53f08835ffb9', N'076', N'Xylene', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'58ee54d0-b7f2-4f75-a99b-9c607c496732', N'077', N'Excluded Liquid (Mineral Oil)', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'29198fce-94ee-4baf-98c8-0220367618d7', N'090', N'Additive – Miscellaneous', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c632c208-2714-4e81-82e2-b36b9d0e351b', N'091', N'Waste Oil', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'111a7eab-0dc5-470f-8129-d2f7e9760bee', N'092', N'Undefined (Other) Product', 0, 1, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'06e992c9-a015-4291-9a7a-3e5a2cc399a9', N'093', N'MTBE', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'00ebab09-aaeb-4019-a4f2-ce62d38d2b03', N'100', N'Transmix ', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f846460b-854a-4832-8333-3bfeec99a5fd', N'121', N'TAME', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd66eaec7-49ab-45da-b194-4026b0dace58', N'122', N'Blending Components Other', 1, 1, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'08e0dd7d-6a5e-4747-9b8f-6eb1da3c4642', N'125', N'Aviation Gasoline', 0, 0, 0, N'125', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'33b70933-f320-4fb3-9bd8-89748a580d18', N'126', N'Napthas', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'686ad3ef-58f7-40d7-ac25-73948ef47676', N'130', N'Jet Fuel', 1, 0, 0, N'130', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd685bd77-2f29-4543-a0fb-aef414976522', N'145', N'Kerosene Low Sulphur Undyed', 1, 0, 0, N'142', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b10006f0-d0c3-4a3e-9bde-9d579ab5d65f', N'147', N'Kerosene High Sulphur Undyed', 1, 0, 0, N'142', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e25f628e-d0b4-4402-93d2-33bc9b862f61', N'150', N'Fuel Oil #1 Undyed', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'157f033a-19d2-459b-8bc8-81f5c535da28', N'153', N'Diesel Fuel #4 Dyed', 1, 0, 0, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7110fe04-72b7-4a72-af66-d64f671b17f4', N'154', N'Diesel Fuel #4 Undyed', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5da4a4f4-ebdd-445c-b6b0-f8ed5e143fb3', N'161', N'Diesel Fuel #1 Low Sulphur Undyed', 1, 0, 0, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'49a198d6-065f-45c8-8c79-fa71ada1d3d0', N'167', N'Diesel Fuel #2 Low Sulphur Undyed', 1, 0, 0, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7442b883-0ce9-4c8b-b9de-144a2359e843', N'188', N'Asphalt', 0, 1, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'203008e1-faf2-44ba-827b-20ddb2d9d2bb', N'196', N'Ethylene', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b1c5dc1b-90c2-4bd6-9f0d-1d8ed8886dfa', N'198', N'Butylene', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'80ae9614-afec-45b0-9914-653d352b18e8', N'199', N'Toluene', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e561db29-6157-46ff-976e-8284edeb948d', N'223', N'Raffinates', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a6b6221d-3383-487a-8ad1-4b11f1ad7e2c', N'224', N'Compressed Natural Gas', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3aa45e7f-55bf-4a7a-b969-b479d10eb476', N'225', N'Liquified Natural Gas', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1a6053f4-2488-42f4-b052-96624dbe4807', N'226', N'Diesel Fuel High Sulphur Dyed ', 1, 0, 0, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f5e56d19-75e2-4e94-9f9a-becef23700dc', N'227', N'Diesel Fuel Low Sulphur Dyed', 1, 0, 0, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8ff80d49-5b11-402e-b1cd-a6eb1be00c6b', N'231', N'Diesel Fuel #1 Dyed', 1, 0, 0, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7e6adaeb-26d9-4073-92a0-fe6756d65923', N'248', N'Benzene', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'805dbf08-0792-4caf-99c4-af3cb368ffff', N'249', N'ETBE', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9b7248f1-b5eb-40d5-a53e-e91f778b5609', N'265', N'Methane', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'56e220f5-abb9-4e8a-8d23-05d7f75637bc', N'279', N'Marine Diesel Oil', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9e97372e-382f-41c4-b0f4-e87b3dd8c528', N'280', N'Marine Gas Oil', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'99231d29-c025-4193-a590-be9dac08fedf', N'281', N'Mineral Oils', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f1098850-7192-4407-9c2a-88b654ff3653', N'282', N'Diesel Fuel High Sulphur #1 Undyed ', 1, 0, 0, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8d7b4d54-55a2-4153-a470-6cb430e896b3', N'283', N'Diesel Fuel High Sulphur #2 Undyed', 1, 0, 0, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7c238cf2-1d71-49aa-a27a-fb83af703545', N'285', N'Soy Oil', 0, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'96bffc51-a319-425f-97e4-11d7762794d7', N'960', N'Food', 0, 1, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7e486450-755e-4e7a-961d-c09aa86c7c0e', N'B00', N'BioDiesel  100%,  Diesel  0%', 0, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f5fd023f-63e0-4455-8978-d1b70209d4c4', N'B01', N'BioDiesel  1%,  Diesel  99%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'49d30eae-ca6f-437f-b7d0-faf8af3d89d0', N'B02', N'BioDiesel  2%,  Diesel  98%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9eb7c9ab-a0a5-480c-beb3-e11cc7413ec0', N'B03', N'BioDiesel  3%,  Diesel  97%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1d2bc8e3-ec3a-4c7f-8be5-17c6248dbbe4', N'B04', N'BioDiesel  4%,  Diesel  96%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'88b8fc12-b4c2-4188-97fe-6e5514c49e9f', N'B05', N'BioDiesel  5%,  Diesel  95%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd7ad49f1-4d95-4ea5-a38b-30b88f80e85e', N'B06', N'BioDiesel  6%,  Diesel  94%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e1bcac6b-6a6c-45db-a687-d34c6335646b', N'B07', N'BioDiesel  7%,  Diesel  93%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1fe861cc-d859-455a-ab26-1744cba6753b', N'B08', N'BioDiesel  8%,  Diesel  92%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c9505251-c6d5-49a4-9767-47ddd78e24a6', N'B09', N'BioDiesel  9%,  Diesel  91%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'bc194884-5640-4e1b-85b0-76e44d84ba60', N'B10', N'BioDiesel  10%,  Diesel  90%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'97674af4-d040-46f7-99aa-02aad837472d', N'B11', N'BioDiesel  11%,  Diesel  89%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3ca83ed7-823f-4038-b1c2-25408dc73b7a', N'B12', N'BioDiesel  12%,  Diesel  88%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7bfb8bfa-7910-4f09-ad77-a4ab79ae4c6c', N'B13', N'BioDiesel  13%,  Diesel  87%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'835d063c-7161-4445-85f2-c1dd7383f22e', N'B14', N'BioDiesel  14%,  Diesel  86%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3f3d8ac1-17a6-4d82-abcc-3dda03026924', N'B15', N'BioDiesel  15%,  Diesel  85%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'39768b6d-d8d2-4b48-994b-59dbde7fcf86', N'B16', N'BioDiesel  16%,  Diesel  84%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b4908bf4-6a1a-49e3-ad1c-51e3bfc9ad5f', N'B17', N'BioDiesel  17%,  Diesel  83%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'04cf1d22-1484-4246-b633-202bfb1ce94b', N'B18', N'BioDiesel  18%,  Diesel  82%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'263bc01c-1465-4e7c-9402-4bfd4508c9b9', N'B19', N'BioDiesel  19%,  Diesel  81%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'aae376f6-3088-46cc-9961-adefcfadfc25', N'B20', N'BioDiesel  20%,  Diesel  80%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'221e7115-0f20-4462-a4bc-907fc9bd32c6', N'B21', N'BioDiesel  21%,  Diesel  79%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e9f3c5b8-4514-4e10-bfdd-a3bd0e494956', N'B22', N'BioDiesel  22%,  Diesel  78%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8c8fd975-7122-4b94-a130-54d07225e597', N'B23', N'BioDiesel  23%,  Diesel  77%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6e251e27-33e6-4289-a4cb-1db1bf9a2bd4', N'B24', N'BioDiesel  24%,  Diesel  76%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'691e0e54-8025-44e8-bebd-846c79ffbe9d', N'B25', N'BioDiesel  25%,  Diesel  75%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'fc29631b-c5f8-4410-95f2-3f19ea4b7d10', N'B26', N'BioDiesel  26%,  Diesel  74%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'076928e3-a542-441a-be86-a4bfb5d87066', N'B27', N'BioDiesel  27%,  Diesel  73%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'285423f6-cc94-4049-9984-3b4b5ee83c6b', N'B28', N'BioDiesel  28%,  Diesel  72%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e65e7686-4cf9-487d-bd20-107d14bacacf', N'B29', N'BioDiesel  29%,  Diesel  71%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'915c5447-d9ef-426f-9a28-fff2e821c776', N'B30', N'BioDiesel  30%,  Diesel  70%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0dafe2c1-40f7-4e2c-9b1c-81aaf6d3f42b', N'B31', N'BioDiesel  31%,  Diesel  69%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9e5ee939-472a-4d20-beea-3fc309bd2d1a', N'B32', N'BioDiesel  32%,  Diesel  68%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'86abf657-e08d-4248-b1b1-f98c4c4627a1', N'B33', N'BioDiesel  33%,  Diesel  67%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'12aff62a-64fe-4197-b100-c9053b72055e', N'B34', N'BioDiesel  34%,  Diesel  66%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c60f61fc-bff3-4bc1-9520-8e7fa2ec3f1a', N'B35', N'BioDiesel  35%,  Diesel  65%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c3f7c35c-0215-438e-a1d5-28467f4749d4', N'B36', N'BioDiesel  36%,  Diesel  64%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'87055b16-c3cf-4e1d-8d33-46b6363cb200', N'B37', N'BioDiesel  37%,  Diesel  63%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8960c967-9e20-468d-9484-41885d69778c', N'B38', N'BioDiesel  38%,  Diesel  62%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'67144076-1568-4abe-9b58-5851d304b63a', N'B39', N'BioDiesel  39%,  Diesel  61%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7446116c-e483-49f5-bb04-3a966b28aec2', N'B40', N'BioDiesel  40%,  Diesel  60%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'363865bc-fb96-4fe0-a3bf-7069c33028ff', N'B41', N'BioDiesel  41%,  Diesel  59%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd0aed152-30b8-49af-9905-8d9801480c25', N'B42', N'BioDiesel  42%,  Diesel  58%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'307725a7-2879-4c04-b1b0-34bc5b6bdc6f', N'B43', N'BioDiesel  43%,  Diesel  57%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f00f84ca-a698-41ca-b3c5-64dcc0f6f774', N'B44', N'BioDiesel  44%,  Diesel  56%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6d476786-136b-4b4d-a571-cc6293514ef7', N'B45', N'BioDiesel  45%,  Diesel  55%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e1b6e5b2-e088-4aa7-947e-bda04d45e66d', N'B46', N'BioDiesel  46%,  Diesel  54%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'cf6b5efb-7dbe-49c5-89d8-311823f398cb', N'B47', N'BioDiesel  47%,  Diesel  53%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'acd5a356-692e-41ee-90b7-6654ba850951', N'B48', N'BioDiesel  48%,  Diesel  52%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'44ce96a8-0b76-48e5-b96c-4bcf0122dcb3', N'B49', N'BioDiesel  49%,  Diesel  51%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'eae37642-2ff2-4e2d-a1a0-e475008e6fe1', N'B50', N'BioDiesel  50%,  Diesel  50%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c9cc9aab-af6a-417b-af99-2370a0f8d54e', N'B51', N'BioDiesel  51%,  Diesel  49%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a5317951-56cb-44a7-89e8-32a14a2d44f3', N'B52', N'BioDiesel  52%,  Diesel  48%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'64517c36-ebba-45d3-82b0-01b84f52a496', N'B53', N'BioDiesel  53%,  Diesel  47%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'99caafe7-c35c-4543-8121-1b6ddf49c9fd', N'B54', N'BioDiesel  54%,  Diesel  46%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'10c1a993-948a-45cb-ae9f-b99d259494ea', N'B55', N'BioDiesel  55%,  Diesel  45%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3cca467b-e2fa-4486-84a9-974fd89d2cc1', N'B56', N'BioDiesel  56%,  Diesel  44%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7d01fbe1-4163-4d98-83a4-8d087b3edb59', N'B57', N'BioDiesel  57%,  Diesel  43%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6f85ba19-fe60-459c-bbde-5d406963f186', N'B58', N'BioDiesel  58%,  Diesel  42%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd57d5b1e-76c6-466f-b237-d46c8095e9db', N'B59', N'BioDiesel  59%,  Diesel  41%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'afcc291e-b995-4b6b-83bc-5ad28ae73f6b', N'B60', N'BioDiesel  60%,  Diesel  40%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'880e0f91-1b7d-4900-a3d3-4bd69b721207', N'B61', N'BioDiesel  61%,  Diesel  39%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e6b78b11-dfc8-4317-8638-4f3985e779b0', N'B62', N'BioDiesel  62%,  Diesel  38%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1856d50c-3047-4889-94af-b1d2a324133f', N'B63', N'BioDiesel  63%,  Diesel  37%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'199fda14-0bf4-4d3f-9577-469bedac448e', N'B64', N'BioDiesel  64%,  Diesel  36%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4cbef0d1-30f6-4591-bd2a-c9e2454a80e9', N'B65', N'BioDiesel  65%,  Diesel  35%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a47f669e-782f-4c4d-968a-cdce9ea531f8', N'B66', N'BioDiesel  66%,  Diesel  34%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8b8a0858-d6d9-45cf-9dba-9d833cfe7455', N'B67', N'BioDiesel  67%,  Diesel  33%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'714b16fe-ca43-454e-a7e5-838463bfdc01', N'B68', N'BioDiesel  68%,  Diesel  32%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7b2edf08-4c20-4d0f-9263-e162428c52d4', N'B69', N'BioDiesel  69%,  Diesel  31%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ffa39ea2-1d42-4f5a-82f5-b8606f6594bf', N'B70', N'BioDiesel  70%,  Diesel  30%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'328724ec-2338-4283-910c-cbd5f36a323c', N'B71', N'BioDiesel  71%,  Diesel  29%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0470472f-b012-48e2-90b4-a0ecd0184040', N'B72', N'BioDiesel  72%,  Diesel  28%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'01abca41-2796-425e-b4ec-286cf7eada21', N'B73', N'BioDiesel  73%,  Diesel  27%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1fad79f6-7365-44e5-b8c0-328ace551a6a', N'B74', N'BioDiesel  74%,  Diesel  26%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3bf1773f-08fa-4ab6-b80e-bd190c15667f', N'B75', N'BioDiesel  75%,  Diesel  25%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'11c66e6b-1c49-4d00-a66f-3b4ba25ff3d2', N'B76', N'BioDiesel  76%,  Diesel  24%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'63a30682-4104-4b43-9543-66ec0c41b358', N'B77', N'BioDiesel  77%,  Diesel  23%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b92cbd0c-ff86-458a-b5a2-819b715378db', N'B78', N'BioDiesel  78%,  Diesel  22%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e4a25533-4a6d-4ae9-8c51-64f5d17a9e44', N'B79', N'BioDiesel  79%,  Diesel  21%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f02233b2-8b18-468a-b91f-c4bf347c527b', N'B80', N'BioDiesel  80%,  Diesel  20%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'79d4b619-b16e-47c3-b949-875d3258105f', N'B81', N'BioDiesel  81%,  Diesel  19%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'af4fa517-3fac-472c-bec7-af0e68d7adbc', N'B82', N'BioDiesel  82%,  Diesel  18%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'71ec734d-7ffa-4958-b398-04cb44d5f491', N'B83', N'BioDiesel  83%,  Diesel  17%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3d374354-6ed1-4f54-b9ee-7b788603c473', N'B84', N'BioDiesel  84%,  Diesel  16%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9dabf264-fd49-4681-b211-6ecad3a1d819', N'B85', N'BioDiesel  85%,  Diesel  15%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f0d36494-0bcc-4263-a187-f2fb317702b0', N'B86', N'BioDiesel  86%,  Diesel  14%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'41397e68-4dab-46f6-a36e-bb27b5bc09e5', N'B87', N'BioDiesel  87%,  Diesel  13%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'025bcb05-d474-423c-a450-da9b59e65bb2', N'B88', N'BioDiesel  88%,  Diesel  12%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'95b2eaf1-d216-4ed8-972c-b0f7f5a54bbd', N'B89', N'BioDiesel  89%,  Diesel  11%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ce13df59-8b37-4636-9d93-fb17281add23', N'B90', N'BioDiesel  90%,  Diesel  10%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2436e67e-7e82-442c-9899-27a799a67bdc', N'B91', N'BioDiesel  91%,  Diesel  9%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c8d08eed-c801-4063-b883-21983a8dda85', N'B92', N'BioDiesel  92%,  Diesel  8%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0158f146-0695-4bd7-8af3-40cc5e2ef542', N'B93', N'BioDiesel  93%,  Diesel  7%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'87ecf1e7-a744-47e3-a617-7e064d47f2c4', N'B94', N'BioDiesel  94%,  Diesel  6%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'58b47e6e-a56a-4c08-bd07-889d61def8e7', N'B95', N'BioDiesel  95%,  Diesel  5%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6f6eb93a-dc23-4921-9803-baa576689d01', N'B96', N'BioDiesel  96%,  Diesel  4%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7a8ca385-c95b-4b31-8c95-13b1c54d385e', N'B97', N'BioDiesel  97%,  Diesel  3%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7088432e-b270-4db7-8ca2-3a4294bc5962', N'B98', N'BioDiesel  98%,  Diesel  2%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'06abd6f5-327d-4211-a5a6-ca7c9908b1e3', N'B99', N'BioDiesel  99%,  Diesel  1%', 1, 0, 1, N'160', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a4bd635a-9043-4788-80ac-38bff6e7d39a', N'D00', N' Dyed BioDiesel  100%,  Diesel  0%', 0, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3da6f512-92ea-4a77-855c-f46cc64c8e58', N'D01', N' Dyed BioDiesel  1%,  Diesel  99%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b8c49dfc-f740-410b-b014-275b3596ae66', N'D02', N' Dyed BioDiesel  2%,  Diesel  98%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'81c81561-4624-4cdc-85a2-c90648ba3539', N'D03', N' Dyed BioDiesel  3%,  Diesel  97%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b5c136be-0d43-4104-a4d4-3a78ee1571a4', N'D04', N' Dyed BioDiesel  4%,  Diesel  96%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ee3dd590-3e17-4c64-97ad-79ca183b6a97', N'D05', N' Dyed BioDiesel  5%,  Diesel  95%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'56b39434-c8d2-4483-acd9-07f5f1d5bc1f', N'D06', N' Dyed BioDiesel  6%,  Diesel  94%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c2d70ea1-a3f5-4488-a7f9-748b7a81c75f', N'D07', N' Dyed BioDiesel  7%,  Diesel  93%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ddc2ca80-911e-4cfd-b406-e032678c3cc8', N'D08', N' Dyed BioDiesel  8%,  Diesel  92%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'440f451e-4d74-4c6a-a0a8-34015827a120', N'D09', N' Dyed BioDiesel  9%,  Diesel  91%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2af969af-a353-4b7e-8c84-1de8388c797d', N'D10', N' Dyed BioDiesel  10%,  Diesel  90%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3b4bbaed-0764-4efe-a636-188e30fe8ebc', N'D11', N' Dyed BioDiesel  11%,  Diesel  89%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'06105f40-2c07-42d7-bbf5-8c6cb2739741', N'D12', N' Dyed BioDiesel  12%,  Diesel  88%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'04421e5b-308a-4379-bd28-acfffe492220', N'D13', N' Dyed BioDiesel  13%,  Diesel  87%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'bc42994f-aa0b-4c1c-b0fd-fd2b1c73fdea', N'D14', N' Dyed BioDiesel  14%,  Diesel  86%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f9dbd983-06a7-4975-a81c-fce09eba2cf6', N'D15', N' Dyed BioDiesel  15%,  Diesel  85%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'48c4cbd2-3872-4572-bbc9-7e9a14b62aa0', N'D16', N' Dyed BioDiesel  16%,  Diesel  84%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0696ee00-d9e1-49da-aa1d-3c577585e8ed', N'D17', N' Dyed BioDiesel  17%,  Diesel  83%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2f6ed268-7341-4327-8caf-9a7b10e4b8d1', N'D18', N' Dyed BioDiesel  18%,  Diesel  82%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'88b07382-e0e0-40f1-b54e-60e8c48cb060', N'D19', N' Dyed BioDiesel  19%,  Diesel  81%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'da21f682-5449-4245-959d-3446f2ac6e6b', N'D20', N' Dyed BioDiesel  20%,  Diesel  80%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'76efad87-e7a8-4675-932b-881e7ab207d0', N'D21', N' Dyed BioDiesel  21%,  Diesel  79%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4f0f2a4e-de19-413b-9ec4-d35683bd97a2', N'D22', N' Dyed BioDiesel  22%,  Diesel  78%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'59d9c3f9-5018-422d-ae38-ae1be8ab48a4', N'D23', N' Dyed BioDiesel  23%,  Diesel  77%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'20aaa853-a02f-4acb-965b-dd3f61e7fc2d', N'D24', N' Dyed BioDiesel  24%,  Diesel  76%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'38a2ccde-baa0-4241-9fd5-a60ebc73b9f9', N'D25', N' Dyed BioDiesel  25%,  Diesel  75%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ce73b4f7-bfa5-4634-9b8a-ca1e74c78d4c', N'D26', N' Dyed BioDiesel  26%,  Diesel  74%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c2a7527f-88fd-4dbf-b75d-373650a8ef71', N'D27', N' Dyed BioDiesel  27%,  Diesel  73%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6a47c2c2-1df5-4de0-acd5-935f6e694d7e', N'D28', N' Dyed BioDiesel  28%,  Diesel  72%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9b9dd517-d8f8-4e0e-932b-69d7da702234', N'D29', N' Dyed BioDiesel  29%,  Diesel  71%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'873447e2-aac9-47b1-b92f-bbd2f8f40c96', N'D30', N' Dyed BioDiesel  30%,  Diesel  70%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'343ec00d-5972-4da2-bffd-041a8a4febbf', N'D31', N' Dyed BioDiesel  31%,  Diesel  69%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'035023d1-257c-40fa-bdd4-bfb642384f7e', N'D32', N' Dyed BioDiesel  32%,  Diesel  68%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f3f78fc6-9526-420e-bef7-69267f98b19f', N'D33', N' Dyed BioDiesel  33%,  Diesel  67%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'426e21b9-d7fe-4c1c-8902-efe4f72f0794', N'D34', N' Dyed BioDiesel  34%,  Diesel  66%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'89781f7a-a3b7-4308-9b30-853ebeb02ea5', N'D35', N' Dyed BioDiesel  35%,  Diesel  65%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1d6e77b5-e480-416c-8122-cf573495c7b3', N'D36', N' Dyed BioDiesel  36%,  Diesel  64%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6ae82019-6e03-4dfe-b4cd-37a3596a88f7', N'D37', N' Dyed BioDiesel  37%,  Diesel  63%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'84c9b9be-239d-4519-a5f0-8304122433c3', N'D38', N' Dyed BioDiesel  38%,  Diesel  62%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e0c61152-74e7-488c-b76e-3aa60a088d48', N'D39', N' Dyed BioDiesel  39%,  Diesel  61%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ef1a14ba-b19f-4ca5-809a-3d0fce791597', N'D40', N' Dyed BioDiesel  40%,  Diesel  60%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'af25cbf0-d972-4cc9-aeca-58b2c3eb46e4', N'D41', N' Dyed BioDiesel  41%,  Diesel  59%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'adaff455-927f-4a18-bb65-219534c7a609', N'D42', N' Dyed BioDiesel  42%,  Diesel  58%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'aa7efdfa-5b8b-420c-8d72-629c8a00f315', N'D43', N' Dyed BioDiesel  43%,  Diesel  57%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'92f083e2-ff34-477b-bb0a-5ed6982c7c78', N'D44', N' Dyed BioDiesel  44%,  Diesel  56%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd94e944c-386b-4812-a827-d45dccab12e6', N'D45', N' Dyed BioDiesel  45%,  Diesel  55%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'afcc2ef9-3513-4db9-9a28-f1369f02472e', N'D46', N' Dyed BioDiesel  46%,  Diesel  54%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9bbd925d-2c14-4c46-a462-7f1defa2bcea', N'D47', N' Dyed BioDiesel  47%,  Diesel  53%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'350609f9-f612-46e6-b92d-8130cc359780', N'D48', N' Dyed BioDiesel  48%,  Diesel  52%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0bc735b8-8bd8-4155-84d6-988835bc0067', N'D49', N' Dyed BioDiesel  49%,  Diesel  51%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5d4da167-8b35-4e5d-8242-3f056a726302', N'D50', N' Dyed BioDiesel  50%,  Diesel  50%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'aa2292a6-377c-4ead-a046-d26c4ca3f614', N'D51', N' Dyed BioDiesel  51%,  Diesel  49%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8baf54cf-1d5f-4d0d-be08-d38142067724', N'D52', N' Dyed BioDiesel  52%,  Diesel  48%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0e4ed2f8-59d1-4b23-9322-a13c7c6b7c9c', N'D53', N' Dyed BioDiesel  53%,  Diesel  47%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3b8a77e7-d2b7-4907-8e44-2dc2e9524a2f', N'D54', N' Dyed BioDiesel  54%,  Diesel  46%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'34b77693-592f-4b60-bc36-36ea0669ab6f', N'D55', N' Dyed BioDiesel  55%,  Diesel  45%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f6da7f76-bab8-4a30-b726-60df7b755392', N'D56', N' Dyed BioDiesel  56%,  Diesel  44%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'31389aec-55a9-4357-ba12-c403e62343b5', N'D57', N' Dyed BioDiesel  57%,  Diesel  43%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9b5e6c04-0806-46f4-873c-3d8c43f84f7a', N'D58', N' Dyed BioDiesel  58%,  Diesel  42%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b9a4d027-49da-4678-8608-c412a55d6368', N'D59', N' Dyed BioDiesel  59%,  Diesel  41%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f0966284-22ac-417c-87da-69cbd8ba4ea3', N'D60', N' Dyed BioDiesel  60%,  Diesel  40%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'98120fa0-2afb-485f-a848-1593fd114e90', N'D61', N' Dyed BioDiesel  61%,  Diesel  39%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'19b01b7c-8621-4e27-96d9-0fda3e85f7aa', N'D62', N' Dyed BioDiesel  62%,  Diesel  38%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4ab03f66-3310-46cd-a82a-f479aa29b8ee', N'D63', N' Dyed BioDiesel  63%,  Diesel  37%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3ac8970f-adf7-491c-b55c-035fc0a57bb5', N'D64', N' Dyed BioDiesel  64%,  Diesel  36%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'40861a74-5653-4ce0-8ebc-fe219d3d2f97', N'D65', N' Dyed BioDiesel  65%,  Diesel  35%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'372628a5-4feb-4d29-9eef-cf550001dbe6', N'D66', N' Dyed BioDiesel  66%,  Diesel  34%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'dfd6923c-35eb-452a-8b2b-d3f47bc5a070', N'D67', N' Dyed BioDiesel  67%,  Diesel  33%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'340f732b-7151-4445-a06c-e5a556fa9624', N'D68', N' Dyed BioDiesel  68%,  Diesel  32%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c02562f9-f894-47d5-9a78-89527d524361', N'D69', N' Dyed BioDiesel  69%,  Diesel  31%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'35a7d5cb-52a1-4368-8f2b-6484a2b7113c', N'D70', N' Dyed BioDiesel  70%,  Diesel  30%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8a033d77-7749-419e-ac9c-6375fb6ff58b', N'D71', N' Dyed BioDiesel  71%,  Diesel  29%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'df6d61eb-cca5-4542-a206-63248597cc8a', N'D72', N' Dyed BioDiesel  72%,  Diesel  28%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'80226184-87da-4333-a5af-a45b1abb6e16', N'D73', N' Dyed BioDiesel  73%,  Diesel  27%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'663ed835-8838-4864-ba2c-e5d837d84272', N'D74', N' Dyed BioDiesel  74%,  Diesel  26%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'21cef55a-ec63-4703-b99e-fb1c4f53c89f', N'D75', N' Dyed BioDiesel  75%,  Diesel  25%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ddf7c74c-b3e7-4b47-8e74-fe6a4b3df486', N'D76', N' Dyed BioDiesel  76%,  Diesel  24%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6aadebe9-4efa-4a65-bf45-d3b09bce2362', N'D77', N' Dyed BioDiesel  77%,  Diesel  23%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f2762cc5-377a-4821-87f0-6ccd95cb839f', N'D78', N' Dyed BioDiesel  78%,  Diesel  22%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'392163ab-652b-497b-bf03-a3e6deb8dc6a', N'D79', N' Dyed BioDiesel  79%,  Diesel  21%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'70eb3300-d54f-46b6-90e1-91e2fa23252a', N'D80', N' Dyed BioDiesel  80%,  Diesel  20%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'12009087-6db3-4a02-9504-0e15183ef70b', N'D81', N' Dyed BioDiesel  81%,  Diesel  19%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'07aab024-56d8-42c7-9180-efd568c37e73', N'D82', N' Dyed BioDiesel  82%,  Diesel  18%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'704faf2a-73bb-48c9-a284-6237dd755cf1', N'D83', N' Dyed BioDiesel  83%,  Diesel  17%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3b0334ac-4246-44ea-8898-835d19099963', N'D84', N' Dyed BioDiesel  84%,  Diesel  16%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3d65c831-1938-449d-a8c4-6c9a4815ad39', N'D85', N' Dyed BioDiesel  85%,  Diesel  15%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8c6057c2-c8dc-49f7-84d4-36ec86e37c31', N'D86', N' Dyed BioDiesel  86%,  Diesel  14%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7d0d18d5-6249-454e-b5a3-94efec792dba', N'D87', N' Dyed BioDiesel  87%,  Diesel  13%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f80b612d-83a8-4542-98d7-034dc891ad05', N'D88', N' Dyed BioDiesel  88%,  Diesel  12%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5dcd1fcc-59cc-4047-80c8-c29c2033cdb7', N'D89', N' Dyed BioDiesel  89%,  Diesel  11%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3f3472d5-e29f-4dcc-b95e-dfd2746e3a90', N'D90', N' Dyed BioDiesel  90%,  Diesel  10%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'eee2840e-20ec-4da9-b483-f0976556f50d', N'D91', N' Dyed BioDiesel  91%,  Diesel  9%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'02e11777-048d-49d8-9e84-13600be0fde7', N'D92', N' Dyed BioDiesel  92%,  Diesel  8%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e2b5a6e5-d557-4e70-9607-f4f559de2029', N'D93', N' Dyed BioDiesel  93%,  Diesel  7%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3c0f7951-475c-4be2-8491-b5abd507cfa1', N'D94', N' Dyed BioDiesel  94%,  Diesel  6%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e7e97581-7575-4d03-994f-ec9138fd3954', N'D95', N' Dyed BioDiesel  95%,  Diesel  5%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'bb293639-e0ea-4087-b369-ff570360bf39', N'D96', N' Dyed BioDiesel  96%,  Diesel  4%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'cb9bc184-b311-4970-870c-701ad7d8cf7a', N'D97', N' Dyed BioDiesel  97%,  Diesel  3%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1ee37fb1-55f9-4fc8-8b7f-7624ebe9bd06', N'D98', N' Dyed BioDiesel  98%,  Diesel  2%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'535f8bd6-2ddb-4a13-a61a-6ccdb6f1d9c2', N'D99', N' Dyed BioDiesel  99%,  Diesel  1%', 1, 0, 1, N'228', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9aa7a12c-6cb7-4cd1-b5a7-a14ccd61c422', N'E00', N'Ethanol  100%,   Gasoline   0%', 0, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9e15f491-5106-4a4a-8a6b-0b8f2c8daa7a', N'E01', N'Ethanol  1%,   Gasoline   99%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7fb1d641-d721-4bbb-a9de-1f221343b79c', N'E02', N'Ethanol  2%,   Gasoline   98%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2259d725-1560-481c-b876-1e6fb04e2d08', N'E03', N'Ethanol  3%,   Gasoline   97%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9ec48d9a-1c8e-4021-a1da-1052c172a6b4', N'E04', N'Ethanol  4%,   Gasoline   96%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ca29ed6d-426c-4f51-a0d5-3898b0dcfe1b', N'E05', N'Ethanol  5%,   Gasoline   95%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'14497cc5-e892-47b9-b59d-c811fd0930d9', N'E06', N'Ethanol  6%,   Gasoline   94%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'25739b1d-f1c9-4e3e-adaa-4f6bd8d472b1', N'E07', N'Ethanol  7%,   Gasoline   93%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'84e33431-0395-41e9-be9f-627e9f97de62', N'E08', N'Ethanol  8%,   Gasoline   92%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c6e7091f-6716-49d3-bc8f-74d81a6ac9a5', N'E09', N'Ethanol  9%,   Gasoline   91%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9d63974b-256a-422f-882a-d906d64619fa', N'E10', N'Ethanol  10%,   Gasoline   90%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a8a1ff9a-436a-44be-871d-4a1026cdc27d', N'E11', N'Ethanol  11%,   Gasoline   89%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3ee6b5eb-7afe-4ec1-bbaa-83830d837c70', N'E12', N'Ethanol  12%,   Gasoline   88%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'709ccb19-3a56-4ad3-936e-6bface13357a', N'E13', N'Ethanol  13%,   Gasoline   87%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0bce68e3-5886-44ad-9868-37323755c670', N'E14', N'Ethanol  14%,   Gasoline   86%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f63bd1dc-8ca3-43c0-a9fa-3d75ba230eef', N'E15', N'Ethanol  15%,   Gasoline   85%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'41f4fee3-b877-43e7-9623-47654acb9135', N'E16', N'Ethanol  16%,   Gasoline   84%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3cc695e0-879e-42a9-85b9-f6fbdf5cbbfe', N'E17', N'Ethanol  17%,   Gasoline   83%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'13933f78-e99a-4b4c-9340-2a6c90edfab2', N'E18', N'Ethanol  18%,   Gasoline   82%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ea25192d-6ae1-47be-b8b5-4aac97fb56f0', N'E19', N'Ethanol  19%,   Gasoline   81%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'75d0d269-0a67-4851-8d8f-d7e491df68b8', N'E20', N'Ethanol  20%,   Gasoline   80%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd6bc4b50-ab1a-4da2-b00e-297d2bb2291b', N'E21', N'Ethanol  21%,   Gasoline   79%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0196fbb5-54b9-466e-ae6c-9b7ee83e8a93', N'E22', N'Ethanol  22%,   Gasoline   78%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'79689efa-3afa-41d6-9c7e-65286107df88', N'E23', N'Ethanol  23%,   Gasoline   77%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f01bb01f-af9c-4e35-a781-35b1b449df34', N'E24', N'Ethanol  24%,   Gasoline   76%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'54ffbe44-6e48-4a56-a0a4-5835f8d6f608', N'E25', N'Ethanol  25%,   Gasoline   75%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f72eae70-dbf7-4d23-80a3-95fcb8219fb2', N'E26', N'Ethanol  26%,   Gasoline   74%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e253309f-980d-4377-9b15-885cec9189ce', N'E27', N'Ethanol  27%,   Gasoline   73%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1bfcfc05-9e97-4508-9838-a230c8ae5602', N'E28', N'Ethanol  28%,   Gasoline   72%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8b910515-bf53-4b15-b01b-c3e7808cee54', N'E29', N'Ethanol  29%,   Gasoline   71%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'92d585c8-013f-47bb-a33b-1ca878f24b76', N'E30', N'Ethanol  30%,   Gasoline   70%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ca437a47-6bc4-4c0d-ad2c-34f510adb215', N'E31', N'Ethanol  31%,   Gasoline   69%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'db8d973b-b0a3-4ad0-bf41-39d0c2c9ca14', N'E32', N'Ethanol  32%,   Gasoline   68%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0b9b9ccf-3698-47c3-9c0b-84b7b2bbbf92', N'E33', N'Ethanol  33%,   Gasoline   67%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'99f3a4f7-ba7b-4708-9e2c-27599e8ae1a4', N'E34', N'Ethanol  34%,   Gasoline   66%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0201e6c5-4a4a-47b2-a56d-b1f7d77d60ef', N'E35', N'Ethanol  35%,   Gasoline   65%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2c8ebb46-304a-466e-8b4a-0c95d66fac94', N'E36', N'Ethanol  36%,   Gasoline   64%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0bc0a307-deb9-4bbc-9f30-a215049f8799', N'E37', N'Ethanol  37%,   Gasoline   63%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'aeab6f65-0e6f-4f4d-ae7b-0474564b9c41', N'E38', N'Ethanol  38%,   Gasoline   62%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1b765fdc-1948-4aca-9efd-e9fec4fa2b9c', N'E39', N'Ethanol  39%,   Gasoline   61%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b6609b42-34bc-4dd1-87b6-42fc33837888', N'E40', N'Ethanol  40%,   Gasoline   60%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'33920254-442a-45ca-bab4-c284d8d4ef77', N'E41', N'Ethanol  41%,   Gasoline   59%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1e6df8b7-6efd-4e91-9b9f-b796ceee17e2', N'E42', N'Ethanol  42%,   Gasoline   58%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7d78b6c1-4210-4457-88b6-c7d0af260049', N'E43', N'Ethanol  43%,   Gasoline   57%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9a39b05a-1eef-494e-8238-78b77de463a5', N'E44', N'Ethanol  44%,   Gasoline   56%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5744a692-aa69-47be-bd05-05afe4c1c85b', N'E45', N'Ethanol  45%,   Gasoline   55%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6b846e49-93b1-4145-babc-8dabaa6890b7', N'E46', N'Ethanol  46%,   Gasoline   54%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'feff3ed3-0866-425e-a3d9-0f68d00a24bc', N'E47', N'Ethanol  47%,   Gasoline   53%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'cf57ff76-a3e1-4488-a26f-00324217fddb', N'E48', N'Ethanol  48%,   Gasoline   52%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'628c06a3-8510-405b-b477-a4189b5f045c', N'E49', N'Ethanol  49%,   Gasoline   51%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8419443b-7d27-441d-ac9e-66d1b01f752b', N'E50', N'Ethanol  50%,   Gasoline   50%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0c62dab7-d3aa-45a7-9425-46c79504f94e', N'E51', N'Ethanol  51%,   Gasoline   49%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd43825b3-c006-41cf-8b89-fd0b13076062', N'E52', N'Ethanol  52%,   Gasoline   48%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd88a141d-8cca-4845-b2f4-9d7485bd0b2a', N'E53', N'Ethanol  53%,   Gasoline   47%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9f1dba84-93a0-4f83-99eb-aa2c71de0fbc', N'E54', N'Ethanol  54%,   Gasoline   46%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd2f219c5-be7b-4736-9c10-d50bc34b6204', N'E55', N'Ethanol  55%,   Gasoline   45%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9564fb13-4181-44fa-a369-54a68907947e', N'E56', N'Ethanol  56%,   Gasoline   44%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4bb76b09-b1f4-40be-9cae-ccba32c9934f', N'E57', N'Ethanol  57%,   Gasoline   43%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'99e12182-1813-4d4b-86b7-0e098639012d', N'E58', N'Ethanol  58%,   Gasoline   42%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'de2c3f43-1b5d-4958-bcfc-d5392a4c7bef', N'E59', N'Ethanol  59%,   Gasoline   41%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'52e832fa-069e-42f5-9ddc-1b6886915279', N'E60', N'Ethanol  60%,   Gasoline   40%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'10c1f47c-a953-4927-99ce-1f78e1f5e1ac', N'E61', N'Ethanol  61%,   Gasoline   39%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4c22a76f-ea3d-41a3-a924-92067adbcb2e', N'E62', N'Ethanol  62%,   Gasoline   38%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8e37de50-4a4f-4604-abc3-ca1542f4b5a1', N'E63', N'Ethanol  63%,   Gasoline   37%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e5abf99f-4d29-4a1c-be94-c37826888f82', N'E64', N'Ethanol  64%,   Gasoline   36%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6482bd56-0a5e-41d7-b785-0d807fd7e07e', N'E65', N'Ethanol  65%,   Gasoline   35%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7e9093d9-d592-4b11-bfbf-aa2566759107', N'E66', N'Ethanol  66%,   Gasoline   34%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c098d3e8-c774-419d-838f-a9d631c1558a', N'E67', N'Ethanol  67%,   Gasoline   33%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'50317a18-549e-42f9-b235-bcc842519a2f', N'E68', N'Ethanol  68%,   Gasoline   32%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'cb25bfa6-c14e-41ad-9f70-0569402bdc54', N'E69', N'Ethanol  69%,   Gasoline   31%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'adac505c-26b4-404d-9990-c1dc00ba37ae', N'E70', N'Ethanol  70%,   Gasoline   30%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'241a2601-d7ab-4bc6-9d15-854c527dc903', N'E71', N'Ethanol  71%,   Gasoline   29%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f475b3f9-8a71-49fa-8562-e5b961927c02', N'E72', N'Ethanol  72%,   Gasoline   28%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f2bb82f9-47bd-4f8a-aca5-b6149f452fcf', N'E73', N'Ethanol  73%,   Gasoline   27%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'325927f2-6189-492a-95a8-4d705608c404', N'E74', N'Ethanol  74%,   Gasoline   26%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'408584d6-d872-433d-b45f-2929c37560cf', N'E75', N'Ethanol  75%,   Gasoline   25%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'355b11f2-eb64-47f9-8105-e585dd004e62', N'E76', N'Ethanol  76%,   Gasoline   24%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'51e5cb2f-782e-4b86-ae43-41ff0bf85990', N'E77', N'Ethanol  77%,   Gasoline   23%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2fc32ffe-5896-4ce7-8f96-d5e221a9b0eb', N'E78', N'Ethanol  78%,   Gasoline   22%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5366f6ae-76c8-4de8-acb6-df69b53320b0', N'E79', N'Ethanol  79%,   Gasoline   21%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'acf65b69-9bed-4b72-b687-db6035621fa8', N'E80', N'Ethanol  80%,   Gasoline   20%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5dcbb146-a1ab-4cb7-80c6-15fb6114983b', N'E81', N'Ethanol  81%,   Gasoline   19%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b9c42a4e-07e7-4d3c-99e5-39e9d3043ea3', N'E82', N'Ethanol  82%,   Gasoline   18%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'42ccc792-5211-467a-9bda-42e20d62b760', N'E83', N'Ethanol  83%,   Gasoline   17%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b35d1325-7ed1-44f4-bd53-e2792379b171', N'E84', N'Ethanol  84%,   Gasoline   16%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'88707abd-ef7a-4010-bb1e-9b3941570a9c', N'E85', N'Ethanol  85%,   Gasoline   15%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b49dd561-9915-4aac-9905-398714b9f7c4', N'E86', N'Ethanol  86%,   Gasoline   14%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'17ce0107-20e6-4f1a-a47a-00859c575649', N'E87', N'Ethanol  87%,   Gasoline   13%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ffe77067-8bb7-46b6-83b0-91b557bb1c98', N'E88', N'Ethanol  88%,   Gasoline   12%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8fae0e65-fc8a-4783-85e4-1a8a8f95e2aa', N'E89', N'Ethanol  89%,   Gasoline   11%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2ae27c4a-c01e-4e03-883d-db7ee0f54f90', N'E90', N'Ethanol  90%,   Gasoline   10%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2c46ae86-79e6-44ca-9e3c-637a9c15236c', N'E91', N'Ethanol  91%,   Gasoline   9%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b826b18d-43ae-464a-9ac1-ab14392aa756', N'E92', N'Ethanol  92%,   Gasoline   8%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1f800336-0b45-4a59-af68-978656334a84', N'E93', N'Ethanol  93%,   Gasoline   7%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7972db9e-3480-4dce-890d-d9ab72622164', N'E94', N'Ethanol  94%,   Gasoline   6%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'55a43518-0388-43a2-9031-2379d019d278', N'E95', N'Ethanol  95%,   Gasoline   5%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a7778fda-6e1b-4c73-b380-dfdc8dbe4e13', N'E96', N'Ethanol  96%,   Gasoline   4%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'71742f73-ca02-4420-bb5c-d8156f46a4bf', N'E97', N'Ethanol  97%,   Gasoline   3%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b68f43f1-71d2-45b2-aabb-66c0301ccb9e', N'E98', N'Ethanol  98%,   Gasoline   2%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'dcf12358-9a2e-4ce7-84a3-0e351f59e779', N'E99', N'Ethanol  99%,   Gasoline   1%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'26604f02-95d4-4712-97ad-5500a368f87e', N'M00', N'Methanol  100%,   Gasoline   0%', 0, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5552d66c-a1ee-40ca-974a-9d9602af7801', N'M01', N'Methanol  1%,   Gasoline   99%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'480f4f2d-d206-489e-8d24-edd62d51714b', N'M02', N'Methanol  2%,   Gasoline   98%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'33896d0f-8f00-429d-8d0c-a6155292cbb1', N'M03', N'Methanol  3%,   Gasoline   97%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6f266580-67ef-4f8d-b830-7526e2fcb303', N'M04', N'Methanol  4%,   Gasoline   96%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f8ca7409-97e4-4aeb-9b42-495fe43754de', N'M05', N'Methanol  5%,   Gasoline   95%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'95d8c137-6b75-4e6b-a7d0-f2cce4366043', N'M06', N'Methanol  6%,   Gasoline   94%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'44514b4c-c5c4-4c68-8012-57cdb1a75ca0', N'M07', N'Methanol  7%,   Gasoline   93%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b54f38b1-ebe4-44bf-acd8-6dcc70472e3b', N'M08', N'Methanol  8%,   Gasoline   92%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6e821ecc-8bf2-44ab-b2f5-752c0ff5b736', N'M09', N'Methanol  9%,   Gasoline   91%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'aafeec6a-dee7-4e30-b8f5-169f768277c6', N'M10', N'Methanol  10%,   Gasoline   90%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4cbef56e-22c3-4b7e-a057-b4dfc7274a2a', N'M11', N'Methanol  11%,   Gasoline   89%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6ccea31b-c0c5-4781-b978-035d3bc13761', N'M12', N'Methanol  12%,   Gasoline   88%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f1d9ce96-b638-433d-bb5a-23bb308cd9cb', N'M13', N'Methanol  13%,   Gasoline   87%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'af10b6ef-1c67-43fe-9f75-ee812dbc75ce', N'M14', N'Methanol  14%,   Gasoline   86%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1d17b24a-a0d3-4103-82c0-1b8ff72c83d2', N'M15', N'Methanol  15%,   Gasoline   85%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'194f87e3-6024-4e02-aec1-ec90ce2fdaec', N'M16', N'Methanol  16%,   Gasoline   84%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ad84acba-5897-4a31-afdc-bb177d366fd3', N'M17', N'Methanol  17%,   Gasoline   83%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4961cb08-50c4-4978-9592-96c67ead07ea', N'M18', N'Methanol  18%,   Gasoline   82%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ba7d42ef-400b-4695-b473-8cb10ba948fa', N'M19', N'Methanol  19%,   Gasoline   81%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2a576d00-adc8-463e-b744-de80b1506ede', N'M20', N'Methanol  20%,   Gasoline   80%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1880d0f3-6f2c-4e12-a7e4-40b8d3dbd960', N'M21', N'Methanol  21%,   Gasoline   79%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd67256c1-4703-43de-bee4-0c43d1e2669a', N'M22', N'Methanol  22%,   Gasoline   78%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'478650cf-7c9e-4edd-b043-ec987bf42777', N'M23', N'Methanol  23%,   Gasoline   77%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b07a6c2f-a6c5-473d-9d71-11eb2d08a09e', N'M24', N'Methanol  24%,   Gasoline   76%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c3f104e2-01fd-4add-993a-0f09a4cc1f79', N'M25', N'Methanol  25%,   Gasoline   75%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'56c6ac8b-0a4c-4936-ad24-4b47b5cf2859', N'M26', N'Methanol  26%,   Gasoline   74%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b8596be3-ff16-4f71-9ca1-90d4aad433dc', N'M27', N'Methanol  27%,   Gasoline   73%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e289af47-eb57-46d9-a848-5936a870812d', N'M28', N'Methanol  28%,   Gasoline   72%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4a1862f9-786c-4beb-92a1-8c96a33d82dd', N'M29', N'Methanol  29%,   Gasoline   71%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2c93619d-242f-48fc-a7f8-14dadc9cdc3f', N'M30', N'Methanol  30%,   Gasoline   70%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'8f142349-0e97-42be-8b47-e2056b602f99', N'M31', N'Methanol  31%,   Gasoline   69%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9159ee4c-0c76-4adf-8656-1ec0e6d468d4', N'M32', N'Methanol  32%,   Gasoline   68%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd6e5e233-5578-404d-944e-f50f558b6363', N'M33', N'Methanol  33%,   Gasoline   67%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'16508fbb-d1c0-4910-97d8-48c3484ee4c0', N'M34', N'Methanol  34%,   Gasoline   66%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'83e18bc6-7441-4c9d-8f72-fe7cd238af58', N'M35', N'Methanol  35%,   Gasoline   65%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'286775d0-6666-4ea6-a855-24aca6ebbbda', N'M36', N'Methanol  36%,   Gasoline   64%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'27ad590c-66a3-4d32-aa20-3c5b56ea39b1', N'M37', N'Methanol  37%,   Gasoline   63%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b9f85e36-1a01-4b3e-ba2a-f8059bc9ff96', N'M38', N'Methanol  38%,   Gasoline   62%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'10083cb1-8523-4028-95cc-a424d29c739d', N'M39', N'Methanol  39%,   Gasoline   61%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2a6d6d03-f002-410c-9ad2-0a38240034ff', N'M40', N'Methanol  40%,   Gasoline   60%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3312d54e-5f2e-47f4-b054-925ec89287bc', N'M41', N'Methanol  41%,   Gasoline   59%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'54df061f-fea8-4f91-810a-9e017150685e', N'M42', N'Methanol  42%,   Gasoline   58%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'432219ee-7189-4cb9-9979-76a854b315bc', N'M43', N'Methanol  43%,   Gasoline   57%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'34bb4776-1739-4a14-9a18-5d4790caa6bf', N'M44', N'Methanol  44%,   Gasoline   56%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'07f881f8-6c5c-4a54-8e13-08487da17caa', N'M45', N'Methanol  45%,   Gasoline   55%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7a5749a3-d7d7-474e-938d-f01830151bb6', N'M46', N'Methanol  46%,   Gasoline   54%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'fe58b957-f439-44dc-acf1-69a17e506b9d', N'M47', N'Methanol  47%,   Gasoline   53%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd591bac8-3d86-4263-ab38-ec2b483fc0c2', N'M48', N'Methanol  48%,   Gasoline   52%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'78c9548d-b959-4721-b5bb-3cc57b6d8384', N'M49', N'Methanol  49%,   Gasoline   51%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'79c593cc-4162-4bc4-b651-23de486ab9d9', N'M50', N'Methanol  50%,   Gasoline   50%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e6c02c42-1366-439d-81a1-4bc07a28f974', N'M51', N'Methanol  51%,   Gasoline   49%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'bc4b8754-3a0d-444c-b6c4-1085ea38b91d', N'M52', N'Methanol  52%,   Gasoline   48%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5c7e0fa3-ea33-4f9b-bd8c-493197be4df1', N'M53', N'Methanol  53%,   Gasoline   47%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ee52f38b-6a8b-4ab4-aab1-437a25daae26', N'M54', N'Methanol  54%,   Gasoline   46%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'0c499587-d8b0-409b-8fb1-817df5cd6be5', N'M55', N'Methanol  55%,   Gasoline   45%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7aaca300-431e-478f-a7ad-3e076774ee5f', N'M56', N'Methanol  56%,   Gasoline   44%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9ab9b218-bbfd-4f7e-8352-a88044d677b2', N'M57', N'Methanol  57%,   Gasoline   43%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ee02521b-9e48-435c-87dc-7d6e0ffe47d9', N'M58', N'Methanol  58%,   Gasoline   42%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'aeac0e21-cbd1-430a-b063-9c54bfcd2d23', N'M59', N'Methanol  59%,   Gasoline   41%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4c82ef33-d38e-4113-99c6-79cd2a677170', N'M60', N'Methanol  60%,   Gasoline   40%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4931d392-3385-44f1-9575-3159fa73ef37', N'M61', N'Methanol  61%,   Gasoline   39%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a2da1d5c-a84d-4d76-9914-01969980938e', N'M62', N'Methanol  62%,   Gasoline   38%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'836af5bb-1df9-45dd-bc7f-14e755f737a8', N'M63', N'Methanol  63%,   Gasoline   37%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3889760d-9597-4f6b-9616-c3b1cae79c0e', N'M64', N'Methanol  64%,   Gasoline   36%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9e89a6b0-0140-423d-9c39-b314bc6bb498', N'M65', N'Methanol  65%,   Gasoline   35%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'734b7aea-e980-43b4-8dda-70c52d37f194', N'M66', N'Methanol  66%,   Gasoline   34%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'248173b0-79a1-451b-96ce-966b42d85810', N'M67', N'Methanol  67%,   Gasoline   33%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6a6a7b1b-5d96-441a-adf4-c3926a1e484a', N'M68', N'Methanol  68%,   Gasoline   32%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'811c83b2-420c-4ded-a742-09df05aefba2', N'M69', N'Methanol  69%,   Gasoline   31%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'2e80a65a-c754-40b6-b2ae-59c4c72bb6a5', N'M70', N'Methanol  70%,   Gasoline   30%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5b9dc1cc-8c5a-4c69-a142-7426ea62d4dc', N'M71', N'Methanol  71%,   Gasoline   29%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9726fc80-9d35-44bc-b16c-31867e05f4bf', N'M72', N'Methanol  72%,   Gasoline   28%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'072d2e0b-03f6-4b4e-b356-c0f59f64c2b3', N'M73', N'Methanol  73%,   Gasoline   27%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'95f6f2fb-0253-46ca-b71e-dce3580d51f9', N'M74', N'Methanol  74%,   Gasoline   26%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a8351049-3302-4488-953a-b5f2c94326e5', N'M75', N'Methanol  75%,   Gasoline   25%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'df43b473-b0a3-4aa0-becf-4ef486891458', N'M76', N'Methanol  76%,   Gasoline   24%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7de8a96b-5b72-42cc-9fa1-479f6db797e3', N'M77', N'Methanol  77%,   Gasoline   23%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4587b31e-2da4-44d3-a72b-d330a04465ef', N'M78', N'Methanol  78%,   Gasoline   22%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'66131d93-d6bc-410a-bf28-bc4ed3ac5b8a', N'M79', N'Methanol  79%,   Gasoline   21%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'e60adbc4-4977-42c2-a0b0-efa5e5d46125', N'M80', N'Methanol  80%,   Gasoline   20%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ee2e94ca-9058-4d3b-a2d1-be57226d4140', N'M81', N'Methanol  81%,   Gasoline   19%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ddaa02b1-1d92-4fe7-9152-8dc9d85b4213', N'M82', N'Methanol  82%,   Gasoline   18%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd95abc9a-9c3b-4742-b5b0-7fd189c43eed', N'M83', N'Methanol  83%,   Gasoline   17%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5de73738-d852-4b99-8eb3-5de33d382153', N'M84', N'Methanol  84%,   Gasoline   16%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'93a5c45c-c322-465a-9b5c-1a51c8c6579d', N'M85', N'Methanol  85%,   Gasoline   15%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'52bf2735-ad68-4247-8ec1-a32898ac1d8c', N'M86', N'Methanol  86%,   Gasoline   14%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'7fa5d762-2ce5-494d-80a3-8b536792d9dc', N'M87', N'Methanol  87%,   Gasoline   13%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6b8d725d-5b51-4df9-ae6f-cd710151c105', N'M88', N'Methanol  88%,   Gasoline   12%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'644a6319-b00f-4885-9f43-0e1c44cad98d', N'M89', N'Methanol  89%,   Gasoline   11%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'456afeb8-3602-42b0-b063-1c47cc47e146', N'M90', N'Methanol  90%,   Gasoline   10%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'f9065afb-ab8d-4190-98df-44e33c5ac2d3', N'M91', N'Methanol  91%,   Gasoline   9%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9bc567ea-8a54-462e-b6ff-5f6355b65209', N'M92', N'Methanol  92%,   Gasoline   8%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'08ee89c1-53f1-4df8-b8bc-b21d56505d30', N'M93', N'Methanol  93%,   Gasoline   7%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'66eddb39-80c0-4c52-8a19-5e9ae63d40bf', N'M94', N'Methanol  94%,   Gasoline   6%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a1e613c0-ee61-469a-bbe2-e6421e3ec717', N'M95', N'Methanol  95%,   Gasoline   5%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'857bf3a9-51d1-4d52-a558-312541a48efa', N'M96', N'Methanol  96%,   Gasoline   4%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'99c2e195-5030-4b09-91da-55d9e0466063', N'M97', N'Methanol  97%,   Gasoline   3%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4bd77454-3294-467c-9e77-c16491a11dcc', N'M98', N'Methanol  98%,   Gasoline   2%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'95aa3fde-48ba-4d37-af82-6895a4ab660b', N'M99', N'Methanol  99%,   Gasoline   1%', 1, 0, 1, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'91cfa627-f980-490d-b644-303ca115eff1', N'S00', N'Substitute Ethanol', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6e421b4d-31de-4d1c-9292-bd436586c840', N'S01', N'Substitute Crude (any)', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'efaf93cf-6c4f-4a8c-9a5b-eba3d8691406', N'S21', N'Substitute TAME', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'4e8670c9-bbab-4a6d-861a-0a0747d2c21c', N'S23', N'Substitute Raffinates', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'84adcc18-e1d3-4d4f-8eac-5033a77de808', N'S25', N'Substitute Aviation Gasoline', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'6017c8d7-c44b-41ef-bae0-5f3101a249e2', N'S26', N'Substitute Liquified Natural Gas', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1b28a1b5-b936-4518-aee2-b18dd0784576', N'S48', N'Substitute Benzene', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'c770c789-bc58-4e1d-83ce-9c60e1a8072d', N'S49', N'Substitute Condensate (not Crude)', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'3f2d297a-13fd-46f8-a4db-70a4ffefad9d', N'S50', N'Substitute ETBE', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'1ed8e276-59e2-4ad5-a542-b28fb666d7cb', N'S52', N'Substitute Ethane', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5d0baf53-91ab-4fd7-b4a1-1e0c862e845b', N'S54', N'Substitute Propane', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ff2864b6-1455-453e-8183-69da0ca515d6', N'S65', N'Substitute Methane', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'9cc4ffe8-d3da-4391-ad0a-7ae857a41a13', N'S75', N'Substitute Propylene', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd0dab1bf-7041-4ef3-a18e-ee6b9d67626a', N'S76', N'Substitute Xylene', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'a38c181c-3ae9-4414-a740-3a802f192a22', N'S77', N'Substitute Excluded Liquid (Mineral Oil)', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'ba867c9f-05e3-43cb-80fc-20c2a19bb2f4', N'S79', N'Substitute Marine Diesel Oil', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'960fd260-2df5-420a-9177-f37545395800', N'S80', N'Substitute Marine Gas Oil', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'eb3e6cd2-d94d-4102-a37c-ec5d6ac9f3c3', N'S81', N'Substitute Mineral Oils', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'd26c4592-84ad-4db3-8067-727cbd9750a9', N'S88', N'Substitute Asphalt', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'53381b2c-4f1a-4c62-ac24-92dabc185e6e', N'S90', N'Substitute Additive – Miscellaneous', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b51c3c46-e386-45c4-9f9f-6a2966ae760e', N'S92', N'Substitute Undefined (Other) Product', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'cc00e6bc-a66a-4ef4-8ca6-0d91e501392d', N'S93', N'Substitute MTBE', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'b6d116ed-c78a-4603-83c5-42bce92adeaf', N'S96', N'Substitute Ethylene', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'59617479-b4a6-4b20-aba8-f11c3c9ad5af', N'S98', N'Substitute Butylene', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[product_irs_codes] ([_key], [prd_irs_code], [prd_name], [prd_ba_allowed], [prd_ce_allowed], [prd_require_pos_holder], [prd_tx_tor], [active]) VALUES (N'5a933d81-ecff-4135-ba83-a01def257d98', N'S99', N'Substitute Toluene', 1, 0, 0, N'XXX', 1)
GO
INSERT [dbo].[rpt_report_entity] ([_key], [re_name], [re_additional_name], [re_fein], [re_tx_taxpayer_number], [re_tcn], [re_637_registration], [re_physical_address1], [re_physical_address2], [re_physical_city], [re_physical_state], [re_physical_zip], [re_physical_country], [re_mailing_address1], [re_mailing_address2], [re_mailing_city], [re_mailing_state], [re_mailing_zip], [re_mailing_country], [re_general_contact_name], [re_general_contact_phone], [re_general_contact_fax], [re_general_contact_email], [re_edi_contact_name], [re_edi_contact_phone], [re_edi_contact_fax], [re_edi_contact_email], [re_fed_isa02], [re_fed_isa04], [re_fed_isa05], [re_fed_isa06], [re_fed_gs02], [re_is_group]) VALUES (N'1a796fdd-b065-4b49-80db-67f3ba5f1c65', N'Hartree Channelview LLC', NULL, N'831146678', N'32067863624', N'T76TX2850', N'2018-000576-S-X', N'16514 DeZavala Rd', NULL, N'Channelview', N'TX', N'77530', N'USA', N'1185 Avenue of the Americas', NULL, N'New York', N'NY', N'10036', N'USA', N'Louis Bernardo', N'2125368660', N'9173428800', N'Lbernardo@hplp.com', N'Candice Iago', N'5204441275', N'5204441275', N'ciago@manganinc.com', N'I999999999', N'2125368660', N'ZZ', N'831146678--CHAN', N'831146678', 1)
GO
INSERT [dbo].[rpt_report_entity_group_members] ([rptg_key], [rptg_report_entity_key], [rptg_name], [rptg_terminal], [rptg_carrier], [rptg_facility_number], [rptg_637_registration], [rptg_physical_address1], [rptg_city], [rptg_state], [rptg_zip], [rptg_country], [rptg_delete_flg], [rptg_number]) VALUES (N'fa88ad3c-6ab1-48b2-9d7a-8d50979ddbe7', N'1a796fdd-b065-4b49-80db-67f3ba5f1c65', N'Hartree Channelview LLC', 1, 0, N'T76TX2850', N'2018-000576-S-X', N'16514 DeZavala Rd', N'Channelview', N'TX', N'77530', N'USA', 0, 1)
GO
INSERT [dbo].[rpt_report_entity_group_members] ([rptg_key], [rptg_report_entity_key], [rptg_name], [rptg_terminal], [rptg_carrier], [rptg_facility_number], [rptg_637_registration], [rptg_physical_address1], [rptg_city], [rptg_state], [rptg_zip], [rptg_country], [rptg_delete_flg], [rptg_number]) VALUES (N'fb3db379-f4d6-46fc-aeaa-7dd7b59ebcc9', N'1a796fdd-b065-4b49-80db-67f3ba5f1c65', N'Hartree Channelview LLC', 0, 1, NULL, N'2018-000576-S-X', N'16514 DeZavala Rd', N'Channelview', N'TX', N'77530', N'USA', 0, 2)
GO
INSERT [dbo].[rpt_report_versions] ([_key], [rpt_key], [rpt_version], [rpt_version_description], [rpt_is_original], [rpt_is_correction], [rpt_is_replacement], [rpt_is_supplemental]) VALUES (N'bba0de85-4e6e-4702-8c8c-ba1a41084158', N'0de727bc-045f-49a9-98b4-ee62975c614a', N'O', N'Original', 1, 0, 0, 0)
GO
INSERT [dbo].[rpt_report_versions] ([_key], [rpt_key], [rpt_version], [rpt_version_description], [rpt_is_original], [rpt_is_correction], [rpt_is_replacement], [rpt_is_supplemental]) VALUES (N'68e9898a-6253-45e3-8ac0-93c04258ec06', N'0de727bc-045f-49a9-98b4-ee62975c614a', N'CO', N'Corrected', 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_reports] ([_key], [rpt_name], [rpt_type], [rpt_description], [rpt_is_federal], [rpt_is_state], [rpt_active]) VALUES (N'30edadd7-4ad0-4395-82ae-1c7f71921bcb', N'720-TO', N'TOR', N'Federal ExSTARS 720 Terminal Operator Report', 1, 0, 1)
GO
INSERT [dbo].[rpt_reports] ([_key], [rpt_name], [rpt_type], [rpt_description], [rpt_is_federal], [rpt_is_state], [rpt_active]) VALUES (N'21b71df7-10c9-4081-b75f-258862da5489', N'720-TO/CS', N'GRP', N'Federal Combined TOR/CCR', 1, 0, 1)
GO
INSERT [dbo].[rpt_reports] ([_key], [rpt_name], [rpt_type], [rpt_description], [rpt_is_federal], [rpt_is_state], [rpt_active]) VALUES (N'26690e4c-6e8f-4d72-90c4-d9378f81247b', N'720-CS', N'CCR', N'Federal ExSTARS Common Carrier Report', 1, 0, 1)
GO
INSERT [dbo].[rpt_reports] ([_key], [rpt_name], [rpt_type], [rpt_description], [rpt_is_federal], [rpt_is_state], [rpt_active]) VALUES (N'0de727bc-045f-49a9-98b4-ee62975c614a', N'TX-TOR', N'TXTOR', N'Texas Terminal Operator Report', 0, 1, 1)
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'c91826b8-e102-4a1a-b389-b8feaaa31f46', N'AK', N'Alaska', N'5bc9fc90-7c60-4afa-89ed-ee53e607b9b2')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'59a7bce1-8c90-4d72-b53e-462abc7c9229', N'AL', N'Alabama', N'a2584f38-e78e-4f5b-aeed-87ad9f2ecbb5')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'cd2e3701-38dc-4375-bdd9-26c7df9dbe8b', N'AR', N'Arkansas', N'f813acc7-448d-41c6-bdbe-f4fb8dcc60a9')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'29f5f84e-c15e-4a6e-abe6-3519e453cf88', N'AZ', N'Arizona', N'38a18a15-7462-4b58-9b75-7a0fc1a3a323')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'7fe1450e-c60a-4500-86b3-1024697e36dd', N'CA', N'California', N'949ffa78-33e7-4b83-9a2e-89bfb918572f')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'06682493-0614-43ce-ae7a-03084bbab690', N'CO', N'Colorado', N'6f5fe9a2-a201-491e-8b9c-ba5eae25e2e5')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'5e24fcc3-badb-4d28-9c6d-ecec1eda8eec', N'CT', N'Connecticut', N'b2774d2e-16a2-4568-87f7-d921f92fce2c')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'954af175-c4a3-48d2-aa27-fd8748946761', N'DC', N'District of Columbia', N'1dbd8bde-fcbc-4cbb-94f2-f0c2e44f431c')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'ee8970ee-a995-41de-8050-e3f5395607dc', N'DE', N'Delaware', N'6ddf6d2a-5367-4788-a9d2-dc6acd486cc5')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'232cbcee-aae2-4c94-8aab-fb4fdf5a2a8a', N'FL', N'Florida', N'cb8f27ce-9921-402e-8137-ea33a9616f9d')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'f5283224-4b1b-4724-9f40-1895085b914f', N'GA', N'Georgia', N'6a7ff147-a30a-41a6-a0dd-cb5886c3150d')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'393cbe4b-3f88-43de-b2fa-43a08e9a9488', N'HI', N'Hawaii', N'f55faa2a-5118-4c24-ae24-37ee41ca3c46')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'aa1aa978-c159-462f-969c-7190cd20e260', N'IA', N'Iowa', N'9b73e296-77b1-478b-a1b3-d5ca2c62bca6')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'ae98ecd0-b7b0-48f1-8575-b40f57e8914b', N'ID', N'Idaho', N'ecf95132-c38b-41d1-9dc7-94d46b56680a')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'9e8bfe47-108b-4d62-b2fb-a09fc51f4fba', N'IL', N'Illinois', N'daa53753-dcd1-41d9-b1f7-231ad4100ec4')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'0ec570be-4fdc-4d49-89a5-0f770c40f9b4', N'IN', N'Indiana', N'8cb64c02-f700-4e7a-b9fe-5e40bae89707')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'de8a7681-bfd6-4739-bd44-cf441ea33f20', N'KS', N'Kansas', N'3f55b108-39ab-4645-a71b-3d992943707c')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'd54bc56c-2cf5-4feb-9c1e-7699a1d0b588', N'KY', N'Kentucky', N'462e43ff-cbec-4848-a497-76d1e9037451')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'789e52a6-7794-41c2-bc2f-9a139224ec61', N'LA', N'Louisiana', N'5e3a4e9f-19a4-4bd4-b6a4-6cebd7e41075')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'55e460a5-786c-4dae-a181-b8bc99674611', N'MA', N'Massachusetts', N'71db99b6-e4c5-42a2-8257-88b305dc49c6')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'38cfb4bf-0059-411d-978b-61ceec821b6a', N'MD', N'Maryland', N'd80c892a-7704-4bbf-bbd8-c299c7416cb2')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'd2f3e475-248e-4160-8300-27bbb134c5c1', N'ME', N'Maine', N'64c8a75c-6aa5-44f0-9339-f674a991872c')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'70d9261d-f6bc-444b-8b24-0234fffdb1cb', N'MI', N'Michigan', N'd7eb3d44-cd15-4bc2-8126-34ebb386e251')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'0de3f0d2-4840-411c-9a0d-7f1b6939e510', N'MN', N'Minnesota', N'ce3182d7-690a-49c6-afae-8dda9098a0a2')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'8017d886-4166-4b2a-acee-e4d086cd8d2b', N'MO', N'Missouri', N'755d90b7-d5e7-46c3-a919-3992e05f869c')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'44e4d404-6137-46a4-969b-30961408786c', N'MS', N'Mississippi', N'7574d2b0-7184-488e-a503-b6662af72b95')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'd5865121-d527-42b6-95d1-04d69432ac12', N'MT', N'Montana', N'e54ab2eb-733d-4c59-b8d0-ba88c8181d40')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'cc8265f8-6415-4d96-b1f5-69b5adcad5cc', N'NC', N'North Carolina', N'5e94d402-6324-4d34-a470-89666fd21ca4')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'3e37bca0-1af7-4197-a520-02b0395b7830', N'ND', N'North Dakota', N'735d78a2-ffcd-4e64-8c60-f75593018c4b')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'adf3ba47-1733-42e2-8739-32a9974801b1', N'NE', N'Nebraska', N'1b6b0e74-1c06-42d0-98a7-2bc341250133')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'7cc50216-5c3b-4cfd-8ed4-f115fbe87993', N'NH', N'New Hampshire', N'89cf39e8-620c-4647-871b-6672e0115804')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'fd240889-c4a1-4a7c-bea9-89bc3a92a30b', N'NJ', N'New Jersey', N'1510f3e7-d0ae-4019-b5c5-12877e5324bc')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'398584d1-8acf-4ce4-b8c3-7320cde7af24', N'NM', N'New Mexico', N'600b09cd-a6aa-40b8-be24-5a13934e4970')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'3d97c447-dc95-43ce-852e-f13c3f0868d2', N'NV', N'Nevada', N'ad05ebed-b388-4e97-bd01-ff592f9bbfac')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'94b6386a-3c0d-4c90-91cf-26a65f96878b', N'NY', N'New York', N'4f7ae123-48b6-4f25-b823-4b6978d7138b')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'23dc81b1-0dea-449d-a527-682b5e8b4023', N'OH', N'Ohio', N'264243be-ed03-4186-90ac-621691ab034f')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'aa17eb97-6f73-4baa-8d84-886e1b95f230', N'OK', N'Oklahoma', N'ba6fa23c-bcee-4a98-a156-dd47f39a5b26')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'5637d959-6026-4033-b9ce-1a496562833a', N'OR', N'Oregon', N'edf1af75-5469-416f-af9d-a166d7236474')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'ffc60097-854c-4b13-adaf-dd35b6731102', N'PA', N'Pennsylvania', N'9fadab0f-a95a-40de-8129-fcd213d3531c')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'6b84fa87-37cb-44f6-ad5e-e97f9f1689d4', N'RI', N'Rhode Island', N'c4f7c667-a8d4-40f6-9576-fc7bce58fb96')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'4150020d-4958-4d9d-83ab-27f5110a4d8a', N'SC', N'South Carolina', N'8a0f31d4-5d33-417e-9492-0d158b7e8e32')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'7f016695-d67e-486a-8150-fd309f94db86', N'SD', N'South Dakota', N'660d9ad7-fbfb-4249-a104-26aa93876538')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'2853075f-8d60-48da-aebc-a230970901d9', N'TN', N'Tennessee', N'6d738b7b-3d18-405d-9812-c3b2258232e7')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'5a2e2836-7414-4d0a-8361-b5628015619a', N'TX', N'Texas', N'aaed528f-2c7d-4523-a60c-6a1a8c902454')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'b6562699-e18a-4c1c-8ea1-800b7ec0ab5c', N'UT', N'Utah', N'6b488212-fe63-4f45-8598-e4a11d5cf020')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'6f090546-aa52-4468-b45b-7810e7b32d0c', N'VA', N'Virginia', N'bfe09155-e433-41a0-9991-4e4f4627689d')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'6c13f923-f104-4ef4-ad53-8ecc56e4d4a7', N'VT', N'Vermont', N'446f7958-e60e-4c08-837f-e6626e650c64')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'b1dd6304-774c-4d68-870e-387629b7d6a6', N'WA', N'Washington', N'0faba1f1-f1f8-444d-acb5-5e0807b84897')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'f218aa89-6f94-4dce-992c-de927aa6a11f', N'WI', N'Wisconsin', N'b8ecb83b-8052-4500-be56-d71bf57de852')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'd910197e-0292-4d85-bf94-89e60b78e361', N'WV', N'West Virginia', N'fb868224-eb93-4ad2-aff6-2c5f6249032b')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'f4506054-76d4-4a03-8234-8f4f06864745', N'WY', N'Wyoming', N'bcbfa1e0-2de1-42cd-808c-563d207cee53')
GO
INSERT [dbo].[rpt_state_codes] ([_key], [st_abbreviation], [st_description], [st_external_id_key]) VALUES (N'828d092d-662a-4930-998e-f7e909af828f', N'ZZ', N'Out of Country', NULL)
GO
SET IDENTITY_INSERT [dbo].[rpt_ticket_bol] ON 
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'46ef069c-dd9c-4f32-9241-032e582a2c60', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 25167, 0, N'Tug Fueling', CAST(N'2021-02-12' AS Date), N'', N'', 500000226)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'd1ea6a84-f5bf-447e-96e2-0c18e22dc59d', N'6966ad88-7dbc-44c9-a552-203f11d5e576', N'32f71106-2405-4d16-8317-bac93dae0334', 16917, 0, N'Tug Fueling', CAST(N'2021-03-03' AS Date), N'', N'', 500000251)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'fcf334bc-4985-486f-b8ac-139494bc55d8', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 16544, 0, N'Tug Fueling', CAST(N'2021-02-05' AS Date), N'', N'', 500000219)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'0e8ffd99-8192-4a9d-960d-15dd7f68c9f4', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 12198, 0, N'Tug Fueling', CAST(N'2021-02-21' AS Date), N'', N'', 500000232)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'3ebe06e2-ca4d-492d-9ba4-16d612863efe', N'9858425f-f63c-4f89-b0a7-3d26fbf1e8c6', N'32f71106-2405-4d16-8317-bac93dae0334', 21087, 0, N'Tug Fueling', CAST(N'2021-03-20' AS Date), N'', N'', 500000267)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'd31597ca-bb1c-431d-b7e5-18866ec4241e', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 12775, 0, N'Tug Fueling', CAST(N'2021-02-20' AS Date), N'', N'', 500000231)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'83555aba-ccb3-4f9c-b090-20399cfa6191', N'1fb3f8e1-61a7-4339-8230-30060b36fda0', N'32f71106-2405-4d16-8317-bac93dae0334', 23084, 0, N'Tug Fueling', CAST(N'2021-03-31' AS Date), N'', N'', 500000277)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'45fedda1-5cf3-468d-85c9-21200109ad2e', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 28137, 0, N'Tug Fueling', CAST(N'2021-02-06' AS Date), N'', N'', 500000220)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'7d4ae07a-c2d0-4790-aa68-2602cf34b1d3', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 20427, 0, N'Tug Fueling', CAST(N'2021-02-19' AS Date), N'', N'', 500000230)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'43273ea3-8fc4-4060-bba0-2699ef14a5a6', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 19228, 0, N'Tug Fueling', CAST(N'2021-02-08' AS Date), N'', N'', 500000222)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'96540f07-6dc9-4f73-9f68-2f9f139ec536', N'1fb3f8e1-61a7-4339-8230-30060b36fda0', N'32f71106-2405-4d16-8317-bac93dae0334', 10880, 0, N'Tug Fueling', CAST(N'2021-03-30' AS Date), N'', N'', 500000276)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'21670a9e-8394-4530-8f3b-318212943818', N'59ce934f-f131-48cc-b450-371eba0d0774', N'32f71106-2405-4d16-8317-bac93dae0334', 31861, 0, N'Tug Fueling', CAST(N'2021-03-14' AS Date), N'', N'', 500000261)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'b26a902c-9018-4017-b84f-35942c655519', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 28963, 0, N'Tug Fueling', CAST(N'2021-02-03' AS Date), N'', N'', 500000217)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'0a3cad17-c6a2-472d-8a23-35cddc98621e', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 1029, 0, N'Tug Fueling', CAST(N'2021-02-02' AS Date), N'', N'', 500000216)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'bc63d289-e4bb-4d96-8732-387c434348e2', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 10911, 0, N'Tug Fueling', CAST(N'2021-02-04' AS Date), N'', N'', 500000218)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'10b85b91-3b7c-4a08-975a-41b1e598a3aa', N'8ea569cf-691a-41bd-8282-009bf6afff49', N'32f71106-2405-4d16-8317-bac93dae0334', 12267, 0, N'Tug Fueling', CAST(N'2021-03-24' AS Date), N'', N'', 500000270)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'f001663f-dd2d-48ee-8616-45aac059a6a7', N'73abe3f9-828e-4f5d-b839-546b26cd78b4', N'32f71106-2405-4d16-8317-bac93dae0334', 29748, 0, N'Tug Fueling', CAST(N'2021-03-19' AS Date), N'', N'', 500000266)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'0d25ba33-6372-4278-acba-4bc0527a649c', N'17e349ec-a608-42ee-be0e-1f586531d2c7', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 574043, 0, N'Barge CBR-2026', CAST(N'2021-02-07' AS Date), N'', N'', 500000239)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'30909f10-7a73-4733-a89f-4d528a0ac549', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 45743, 0, N'Tug Fueling', CAST(N'2021-02-13' AS Date), N'', N'', 500000227)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'2bf36db3-0408-46cd-aa65-561d9dc36f5f', N'd5623733-77dd-43a1-8916-420b17c5766a', N'32f71106-2405-4d16-8317-bac93dae0334', 19275, 0, N'Tug Fueling', CAST(N'2021-03-01' AS Date), N'', N'', 500000249)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'5e37df76-99a9-4e67-acae-56eb1ebd84a8', N'01f23b17-a139-49e0-83ab-cd4ffc8fe5ce', N'32f71106-2405-4d16-8317-bac93dae0334', 9965, 0, N'Tug Fueling', CAST(N'2021-03-06' AS Date), N'', N'', 500000254)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'ec3d39db-1fc3-4aff-a6cf-57c326470272', N'ea16acff-8359-413b-a481-30e591cedaf1', N'32f71106-2405-4d16-8317-bac93dae0334', 222935, 0, N'RECLASS', CAST(N'2021-03-31' AS Date), N'', N'', 500000246)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'1c893a21-006b-4cd4-b86c-594f0fa3449d', N'8b6548a9-6293-4716-9ffa-165373dcf59e', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 476321, 0, N'Barge CBR-2026', CAST(N'2021-02-07' AS Date), N'', N'', 500000240)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'449816e8-d2a3-4697-8096-5af9a805dc4f', N'64c19e55-2254-4756-8e09-922948fb2df5', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 280874, 0, N'Barge JAM-440', CAST(N'2021-02-25' AS Date), N'', N'', 500000210)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'10922357-732f-41a2-850b-6338f7c21e8b', N'531483fb-6994-4940-8531-49460eeb8ebc', N'32f71106-2405-4d16-8317-bac93dae0334', 5343, 0, N'Tug Fueling', CAST(N'2021-03-25' AS Date), N'', N'', 500000271)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'1f12dde4-d466-455a-8dd4-649e61a68b90', N'e626ce2e-31f1-477d-8802-a6b6e85742d1', N'32f71106-2405-4d16-8317-bac93dae0334', 26600, 0, N'Tug Fueling', CAST(N'2021-03-05' AS Date), N'', N'', 500000253)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'7491a55f-dff1-47d9-9c77-6d0a23ec6d5b', N'14781e73-a67f-4728-a1ee-94c25d117205', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 104712, 0, N'Barge Jam-440', CAST(N'2021-02-25' AS Date), N'', N'', 500000213)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'ae9b5a87-c91e-4f32-9bb5-6ffc3115eac2', N'04ec0871-7157-4db2-ab12-50e1c95f6ed6', N'32f71106-2405-4d16-8317-bac93dae0334', 275, 0, N'Tug Fueling', CAST(N'2021-03-08' AS Date), N'', N'', 500000255)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'9ae3a7c7-9940-4dd5-a631-735e226d4ca2', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 29618, 0, N'Tug Fueling', CAST(N'2021-02-24' AS Date), N'', N'', 500000234)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'f826c85d-3fa8-4d6f-9e36-737ecc12c732', N'08087730-64d1-4994-a15c-08879ec2e25f', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 587717, 0, N'RECLASS', CAST(N'2021-02-28' AS Date), N'', N'', 500000241)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'e2f6d42e-701d-4812-93b3-763e1a4f1f80', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 4314, 0, N'Tug Fueling', CAST(N'2021-02-25' AS Date), N'', N'', 500000235)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'f27b9198-4eea-4909-bdce-7997e8ec2022', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 22405, 0, N'Tug Fueling', CAST(N'2021-02-14' AS Date), N'', N'', 500000228)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'8b9c49e9-4910-4e30-9db3-7a063a0f30ab', N'38ad3929-1db9-4d8a-b962-6b99e21cc003', N'32f71106-2405-4d16-8317-bac93dae0334', 20058, 0, N'Tug Fueling', CAST(N'2021-03-21' AS Date), N'', N'', 500000278)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'1889dd4c-537e-4744-8fb5-7fb2f7f802be', N'1fb3f8e1-61a7-4339-8230-30060b36fda0', N'32f71106-2405-4d16-8317-bac93dae0334', 25774, 0, N'Tug Fueling', CAST(N'2021-03-27' AS Date), N'', N'', 500000273)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'f24ee764-517c-4807-b156-81463c0a811a', N'77ededf5-6c89-4400-aff6-55ecf5be1067', N'32f71106-2405-4d16-8317-bac93dae0334', 4003, 0, N'Tug Fueling', CAST(N'2021-03-12' AS Date), N'', N'', 500000259)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'b23e60eb-c1be-41af-8585-900d5cd02c83', N'cb7aefba-409b-4c87-8f28-6951334c85b9', N'32f71106-2405-4d16-8317-bac93dae0334', 576525, 0, N'Barge OMS-460', CAST(N'2021-03-07' AS Date), N'', N'', 500000243)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'b8d88fe1-5117-4a99-8016-9039dd07c738', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 11630, 0, N'Tug Fueling', CAST(N'2021-02-11' AS Date), N'', N'', 500000225)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'1cf2d0cb-fe20-4072-aa7d-91465c667f0c', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 36021, 0, N'Tug Fueling', CAST(N'2021-02-01' AS Date), N'', N'', 500000214)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'7a78eb40-3866-49fc-b35e-950fd8a2fe28', N'33724cef-9658-4d39-a40e-edc1093a47df', N'32f71106-2405-4d16-8317-bac93dae0334', 5736, 0, N'Tug Fueling', CAST(N'2021-03-16' AS Date), N'', N'', 500000263)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'd5f6ca8b-6c7d-4bd7-8e6d-988eb32e805a', N'9318cd84-c092-4689-bb0d-2f4ac8d5b602', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 587717, 0, N'RECLASS', CAST(N'2021-02-28' AS Date), N'', N'', 500000242)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'fa14d71b-a3eb-4b33-a5c4-997cce1f3a71', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 33247, 0, N'Tug Fueling', CAST(N'2021-02-22' AS Date), N'', N'', 500000233)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'f0ff2b68-07b7-4a24-a141-9a745abbbfd0', N'cdf984c1-9294-49e6-b948-e9fb4e2f79c0', N'32f71106-2405-4d16-8317-bac93dae0334', 33350, 0, N'Tug Fueling', CAST(N'2021-03-13' AS Date), N'', N'', 500000260)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'5663fa52-fb6d-49c8-9eaf-9b3a631a7db6', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 4398, 0, N'Tug Fueling', CAST(N'2021-02-26' AS Date), N'', N'', 500000236)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'cfc06df6-3b59-4643-8a5e-9f305be4c296', N'64c19e55-2254-4756-8e09-922948fb2df5', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 281004, 0, N'Barge JAM-440', CAST(N'2021-02-02' AS Date), N'', N'', 500000209)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'43b19984-b7ca-44f9-8a06-9f34b9d25420', N'd39e784b-f82b-4b8b-8006-edca13b8a1b4', N'32f71106-2405-4d16-8317-bac93dae0334', 14267, 0, N'Tug Fueling', CAST(N'2021-03-17' AS Date), N'', N'', 500000264)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'8c802358-aa46-42ec-abb9-a05dfcb32dc0', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 16662, 0, N'Tug Fueling', CAST(N'2021-02-02' AS Date), N'', N'', 500000215)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'f91af2bd-2b52-4c53-ac5a-a57742252bf3', N'1fb3f8e1-61a7-4339-8230-30060b36fda0', N'32f71106-2405-4d16-8317-bac93dae0334', 32120, 0, N'Tug Fueling', CAST(N'2021-03-26' AS Date), N'', N'', 500000272)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'0e2207b9-789d-4d37-8498-ad4fe336b8df', N'e34bb0f6-efc0-4d81-a630-11b977d95b06', N'32f71106-2405-4d16-8317-bac93dae0334', 352602, 0, N'Barge JAM-440', CAST(N'2021-03-16' AS Date), N'', N'', 500000247)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'ec1dfa5c-e534-48ad-b30f-b172fbfce51b', N'dd58c793-d148-415e-8196-efb7d1591f96', N'32f71106-2405-4d16-8317-bac93dae0334', 21267, 0, N'Tug Fueling', CAST(N'2021-03-04' AS Date), N'', N'', 500000252)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'97c3b749-aec5-426d-a2c8-b389291b28d2', N'e15b82bb-4795-4b83-bdac-e2e264a7b0b1', N'32f71106-2405-4d16-8317-bac93dae0334', 37471, 0, N'Tug Fueling', CAST(N'2021-03-02' AS Date), N'', N'', 500000250)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'13292a63-51dc-4388-9755-b3b467e3f461', N'39f28ec7-d275-41f8-8b44-b1bbf98562b4', N'32f71106-2405-4d16-8317-bac93dae0334', 5211, 0, N'Tug Fueling', CAST(N'2021-03-09' AS Date), N'', N'', 500000256)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'4c22d2d7-4199-475d-bebf-b4e03f087a7b', N'294f7116-6aff-4720-ab2c-6db65ab52a68', N'32f71106-2405-4d16-8317-bac93dae0334', 22897, 0, N'Tug Fueling', CAST(N'2021-03-18' AS Date), N'', N'', 500000265)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'01e54d99-0668-40af-a676-b8341acbd020', N'14781e73-a67f-4728-a1ee-94c25d117205', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 224620, 0, N'Barge Jam-440', CAST(N'2021-02-06' AS Date), N'', N'', 500000212)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'669eaf83-450b-47df-b78c-bbfcd9af8a2b', N'954e1215-b882-472f-a66a-25a6fbd9e62b', N'32f71106-2405-4d16-8317-bac93dae0334', 24596, 0, N'Tug Fueling', CAST(N'2021-03-11' AS Date), N'', N'', 500000258)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'56376706-0105-41e9-8365-c0091f088bed', N'0134f03c-1c47-4984-ab0c-e16edce9f3a3', N'32f71106-2405-4d16-8317-bac93dae0334', 15309, 0, N'Tug Fueling', CAST(N'2021-03-10' AS Date), N'', N'', 500000257)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'c233224f-5567-4789-a884-c6180567fb89', N'af56b09f-57b5-46a8-a256-64896ea47c85', N'32f71106-2405-4d16-8317-bac93dae0334', 23206, 0, N'Barge OMS-460', CAST(N'2021-03-07' AS Date), N'', N'', 500000245)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'0b4f49f8-c182-4c27-abe7-c7d84db065e3', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 17006, 0, N'Tug Fueling', CAST(N'2021-02-09' AS Date), N'', N'', 500000223)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'e3b6ff31-d650-40f6-b464-c867a9869408', N'2058464c-f89f-4a03-aacb-128fde65599b', N'32f71106-2405-4d16-8317-bac93dae0334', 222935, 0, N'RECLASS', CAST(N'2021-03-31' AS Date), N'', N'', 500000248)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'9694a8b2-3014-47bd-b719-d75cb034ee78', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 9074, 0, N'Tug Fueling', CAST(N'2021-02-07' AS Date), N'', N'', 500000221)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'd73a3876-2bcd-47ff-89a5-dea56c28534d', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 8181, 0, N'Tug Fueling', CAST(N'2021-02-18' AS Date), N'', N'', 500000229)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'6c656a26-571b-4405-96df-e02841dba46e', N'1fb3f8e1-61a7-4339-8230-30060b36fda0', N'32f71106-2405-4d16-8317-bac93dae0334', 35239, 0, N'Tug Fueling', CAST(N'2021-03-29' AS Date), N'', N'', 500000275)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'8d2858c5-693e-407f-a65c-e0d11cd8d41e', N'c332279d-5d91-4fc4-92e6-ceeebf07b108', N'32f71106-2405-4d16-8317-bac93dae0334', 249613, 0, N'Barge OMS-460', CAST(N'2021-03-07' AS Date), N'', N'', 500000244)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'c5e3cb2d-877e-4823-8160-e74fb71b964c', N'2f41c713-276a-4ab5-9d5a-087c034ec51a', N'32f71106-2405-4d16-8317-bac93dae0334', 26374, 0, N'Tug Fueling', CAST(N'2021-03-22' AS Date), N'', N'', 500000268)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'902f82d2-3c61-47f5-bc2c-e88dff0d68b3', N'14781e73-a67f-4728-a1ee-94c25d117205', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 333899, 0, N'Barge Jam-450', CAST(N'2021-02-05' AS Date), N'', N'', 500000211)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'243808b1-f791-4452-9d23-edea9fcbda61', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 17052, 0, N'Tug Fueling', CAST(N'2021-02-10' AS Date), N'', N'', 500000224)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'6473cbaa-1a70-47da-b08a-efa330ce4173', N'eba8a1e1-e1de-4160-b2cd-0c5843573bc1', N'32f71106-2405-4d16-8317-bac93dae0334', 3296, 0, N'Tug Fueling', CAST(N'2021-03-23' AS Date), N'', N'', 500000269)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'd50ebfbd-5675-4543-9cfc-f8bbd39b520a', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 4447, 0, N'Tug Fueling', CAST(N'2021-02-27' AS Date), N'', N'', 500000237)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'c4998c51-7fce-4d63-bc09-fb644e770920', N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', 32742, 0, N'Tug Fueling', CAST(N'2021-02-28' AS Date), N'', N'', 500000238)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'897a4c36-b877-42dd-a014-fbb47ef3b8e7', N'1fb3f8e1-61a7-4339-8230-30060b36fda0', N'32f71106-2405-4d16-8317-bac93dae0334', 21111, 0, N'Tug Fueling', CAST(N'2021-03-28' AS Date), N'', N'', 500000274)
GO
INSERT [dbo].[rpt_ticket_bol] ([_key], [tb_ticket_header_key], [tb_document_key], [tb_net_gallons], [tb_gross_gallons], [tb_bol], [tb_document_date], [tb_vessel_name], [tb_von_imo], [tb_ref_55]) VALUES (N'b6c550fa-d709-486d-9d02-fd8b98a2815d', N'61bd8dff-b7b0-450e-8cf5-9b89e3afea38', N'32f71106-2405-4d16-8317-bac93dae0334', 10007, 0, N'Tug Fueling', CAST(N'2021-03-15' AS Date), N'', N'', 500000262)
GO
SET IDENTITY_INSERT [dbo].[rpt_ticket_bol] OFF
GO
SET IDENTITY_INSERT [dbo].[rpt_ticket_header] ON 
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'8ea569cf-691a-41bd-8282-009bf6afff49', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643651', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000102)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'2f41c713-276a-4ab5-9d5a-087c034ec51a', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643648', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000100)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'08087730-64d1-4994-a15c-08879ec2e25f', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', N'TR_BA', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'', N'', N'', N'', N'T76TX2850', N'', N'', N'', N'', N'', 400000077)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'eba8a1e1-e1de-4160-b2cd-0c5843573bc1', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643650', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000101)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'e34bb0f6-efc0-4d81-a630-11b977d95b06', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_B_', N'226', N'228', N'J.A.M. DISTRIBUTING COMPANY', N'741998773', N'17419987734', N'O''ROURKE MARINE SERVICES, LLC', N'562643627', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000078)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'2058464c-f89f-4a03-aacb-128fde65599b', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_BA', N'226', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643626', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000079)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'8b6548a9-6293-4716-9ffa-165373dcf59e', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', N'TR_B_', N'227', N'228', N'O''ROURKE MARINE SERVICES, LLC', N'562643626', N'15626436263', N'', N'', N'', N'', N'T76TX2850', N'', N'', N'', N'', N'', 400000076)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'17e349ec-a608-42ee-be0e-1f586531d2c7', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', N'TR_B_', N'226', N'228', N'O''ROURKE MARINE SERVICES, LLC', N'562643626', N'15626436263', N'', N'', N'', N'', N'T76TX2850', N'', N'', N'', N'', N'', 400000075)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'6966ad88-7dbc-44c9-a552-203f11d5e576', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643628', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000082)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'954e1215-b882-472f-a66a-25a6fbd9e62b', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643636', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000089)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'9318cd84-c092-4689-bb0d-2f4ac8d5b602', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', N'TD_BA', N'226', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643626', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000073)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'1fb3f8e1-61a7-4339-8230-30060b36fda0', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643653', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000104)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'ea16acff-8359-413b-a481-30e591cedaf1', N'32f71106-2405-4d16-8317-bac93dae0334', N'TR_BA', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'', N'', N'', N'', N'T76TX2850', N'', N'', N'', N'', N'', 400000108)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'59ce934f-f131-48cc-b450-371eba0d0774', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643639', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000092)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'9858425f-f63c-4f89-b0a7-3d26fbf1e8c6', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643645', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000098)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'd5623733-77dd-43a1-8916-420b17c5766a', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643626', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000080)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'531483fb-6994-4940-8531-49460eeb8ebc', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643652', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000103)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'04ec0871-7157-4db2-ab12-50e1c95f6ed6', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643633', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000086)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'73abe3f9-828e-4f5d-b839-546b26cd78b4', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643644', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000097)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'77ededf5-6c89-4400-aff6-55ecf5be1067', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643637', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000090)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'af56b09f-57b5-46a8-a256-64896ea47c85', N'32f71106-2405-4d16-8317-bac93dae0334', N'TR_B_', N'227', N'228', N'J.A.M. DISTRIBUTING COMPANY', N'741998773', N'17419987734', N'', N'', N'', N'', N'T76TX2850', N'', N'', N'', N'', N'', 400000107)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'cb7aefba-409b-4c87-8f28-6951334c85b9', N'32f71106-2405-4d16-8317-bac93dae0334', N'TR_B_', N'226', N'228', N'J.A.M. DISTRIBUTING COMPANY', N'741998773', N'17419987734', N'', N'', N'', N'', N'T76TX2850', N'', N'', N'', N'', N'', 400000105)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'38ad3929-1db9-4d8a-b962-6b99e21cc003', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643646', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000099)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'294f7116-6aff-4720-ab2c-6db65ab52a68', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643643', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000096)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'64c19e55-2254-4756-8e09-922948fb2df5', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', N'TD_B_', N'226', N'228', N'J.A.M. DISTRIBUTING COMPANY', N'741998773', N'17419987734', N'O''ROURKE MARINE SERVICES, LLC', N'562643626', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000071)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'14781e73-a67f-4728-a1ee-94c25d117205', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', N'TD_B_', N'227', N'228', N'J.A.M. DISTRIBUTING COMPANY', N'741998773', N'17419987734', N'O''ROURKE MARINE SERVICES, LLC', N'562643626', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000072)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'61bd8dff-b7b0-450e-8cf5-9b89e3afea38', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643640', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000093)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'e626ce2e-31f1-477d-8802-a6b6e85742d1', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643630', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000084)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'39f28ec7-d275-41f8-8b44-b1bbf98562b4', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643634', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000087)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'b8129277-3a54-41b2-a241-b379c56e28a9', N'1b649df2-c7c3-406a-99b1-e4b98b2fb27c', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643626', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000074)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'01f23b17-a139-49e0-83ab-cd4ffc8fe5ce', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643631', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000085)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'c332279d-5d91-4fc4-92e6-ceeebf07b108', N'32f71106-2405-4d16-8317-bac93dae0334', N'TR_B_', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'', N'', N'', N'', N'T76TX2850', N'', N'', N'', N'', N'', 400000106)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'0134f03c-1c47-4984-ab0c-e16edce9f3a3', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643635', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000088)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'e15b82bb-4795-4b83-bdac-e2e264a7b0b1', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643627', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000081)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'cdf984c1-9294-49e6-b948-e9fb4e2f79c0', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643638', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000091)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'33724cef-9658-4d39-a40e-edc1093a47df', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643641', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000094)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'd39e784b-f82b-4b8b-8006-edca13b8a1b4', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643642', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000095)
GO
INSERT [dbo].[rpt_ticket_header] ([_key], [th_document_key], [th_tranaction_type_code], [th_product_code], [th_tx_product_code], [th_carrier_name], [th_carrier_fein], [th_carrier_tx_taxpayer], [th_position_holder_name], [th_position_holder_fein], [th_position_holder_tx_taxpayer], [th_origin_facility], [th_destination_facility], [th_destination_state], [th_consignor_name], [th_consignor_fein], [th_exchanger_name], [th_exchanger_fein], [th_ref_55]) VALUES (N'dd58c793-d148-415e-8196-efb7d1591f96', N'32f71106-2405-4d16-8317-bac93dae0334', N'TD_ST', N'227', N'228', N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643629', N'15626436263', N'T76TX2850', N'', N'TX', N'', N'', N'', N'', 400000083)
GO
SET IDENTITY_INSERT [dbo].[rpt_ticket_header] OFF
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'IN', N'IN_BI', N'147', N'142', N'BI', 249472, 0, N'', N'', N'', N'', N'', N'', N'', CAST(N'2021-03-31T00:00:00.000' AS DateTime), N'', N'', N'', N'', N'', N'', N'', N'', N'', N'b1eefbef-63b4-4c47-835a-4995c2a2930f', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'IN', N'IN_BI', N'226', N'228', N'BI', 16830, 0, N'', N'', N'', N'', N'', N'', N'', CAST(N'2021-03-31T00:00:00.000' AS DateTime), N'', N'', N'', N'', N'', N'', N'', N'', N'', N'4f285139-caa9-46fe-b5b3-9f64c8481bc5', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'IN', N'IN_BI', N'227', N'228', N'BI', 666932, 0, N'', N'', N'', N'', N'', N'', N'', CAST(N'2021-03-31T00:00:00.000' AS DateTime), N'', N'', N'', N'', N'', N'', N'', N'', N'', N'cd9fdf7b-140a-4b3b-abf2-4a12bc61d625', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'IN', N'IN_EI', N'147', N'142', N'EI', 250404, 0, N'', N'', N'', N'', N'', N'', N'', CAST(N'2021-03-31T00:00:00.000' AS DateTime), N'', N'', N'', N'', N'', N'', N'', N'', N'', N'1649b3df-f188-48e6-b5e9-ff9fb3a63095', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'IN', N'IN_EI', N'226', N'228', N'EI', 17818, 0, N'', N'', N'', N'', N'', N'', N'', CAST(N'2021-03-31T00:00:00.000' AS DateTime), N'', N'', N'', N'', N'', N'', N'', N'', N'', N'a0537537-49b8-421e-a339-a56948e415ce', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'IN', N'IN_EI', N'227', N'228', N'EI', 604409, 0, N'', N'', N'', N'', N'', N'', N'', CAST(N'2021-03-31T00:00:00.000' AS DateTime), N'', N'', N'', N'', N'', N'', N'', N'', N'', N'af28c7ab-9b4d-4c62-aa90-6917355a4603', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TR', N'TR_B_', N'226', N'228', N'B_', 576525, 0, N'J.A.M. DISTRIBUTING COMPANY', N'741998773', N'17419987734', N'', N'', N'', N'Barge OMS-460', CAST(N'2021-03-07T00:00:00.000' AS DateTime), N'', N'T76TX2850', N'', N'', N'', N'', N'', N'', N'', N'90e3a8fa-92cf-480b-8022-b9e8b0dd3491', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TR', N'TR_B_', N'227', N'228', N'B_', 249613, 0, N'HARTREE', N'831146678', N'32067863624', N'', N'', N'', N'Barge OMS-460', CAST(N'2021-03-07T00:00:00.000' AS DateTime), N'', N'T76TX2850', N'', N'', N'', N'', N'', N'', N'', N'5b30926e-789d-4c26-9650-5bcb0eaa3776', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TR', N'TR_B_', N'227', N'228', N'B_', 23206, 0, N'J.A.M. DISTRIBUTING COMPANY', N'741998773', N'17419987734', N'', N'', N'', N'Barge OMS-460', CAST(N'2021-03-07T00:00:00.000' AS DateTime), N'', N'T76TX2850', N'', N'', N'', N'', N'', N'', N'', N'f4dcbde2-c74b-4ebb-b049-10301d254f20', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TR', N'TR_BA', N'227', N'228', N'BA', 222935, 0, N'HARTREE', N'831146678', N'32067863624', N'', N'', N'', N'RECLASS', CAST(N'2021-03-31T00:00:00.000' AS DateTime), N'', N'T76TX2850', N'', N'', N'', N'', N'', N'', N'', N'f654036e-44a4-43dd-b7f1-631aa1cd5dba', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_B_', N'226', N'228', N'B_', 352602, 0, N'J.A.M. DISTRIBUTING COMPANY', N'741998773', N'17419987734', N'O''ROURKE MARINE SERVICES, LLC', N'562643627', N'15626436263', N'Barge JAM-440', CAST(N'2021-03-16T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'8622f4ce-90ed-4859-a859-e344a745c387', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_BA', N'226', N'228', N'BA', 222935, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643626', N'15626436263', N'RECLASS', CAST(N'2021-03-31T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'4ce0569f-6778-4dc3-9d75-6033d8bb3634', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 19275, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643626', N'15626436263', N'Tug Fueling', CAST(N'2021-03-01T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'e97e1b49-0953-4716-bff0-0694eccdaad2', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 37471, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643627', N'15626436263', N'Tug Fueling', CAST(N'2021-03-02T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'255dcd00-9712-4ec5-ab30-b081cd946250', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 16917, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643628', N'15626436263', N'Tug Fueling', CAST(N'2021-03-03T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'dfcb5e5b-45c7-47d2-9f96-f3ee805c65e3', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 21267, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643629', N'15626436263', N'Tug Fueling', CAST(N'2021-03-04T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'23c90da2-0519-45aa-9355-b7cec921f543', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 26600, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643630', N'15626436263', N'Tug Fueling', CAST(N'2021-03-05T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'd6029f75-dbbb-4f71-91e9-105fa34dc48d', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 9965, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643631', N'15626436263', N'Tug Fueling', CAST(N'2021-03-06T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'c3f0e66a-9a28-47af-b817-9830fffc780b', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 275, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643633', N'15626436263', N'Tug Fueling', CAST(N'2021-03-08T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'0d844592-d7ae-4d19-8415-2ad586c70345', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 5211, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643634', N'15626436263', N'Tug Fueling', CAST(N'2021-03-09T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'3458269a-b0fc-4274-9d06-6769951586ba', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 15309, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643635', N'15626436263', N'Tug Fueling', CAST(N'2021-03-10T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'05988fee-a282-47bd-8a99-4734a26cfa5f', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 24596, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643636', N'15626436263', N'Tug Fueling', CAST(N'2021-03-11T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'277f6bef-96f9-4072-913a-d24c7f7a40a0', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 4003, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643637', N'15626436263', N'Tug Fueling', CAST(N'2021-03-12T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'51f116ac-f9b3-494d-b398-f06c139d72c3', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 33350, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643638', N'15626436263', N'Tug Fueling', CAST(N'2021-03-13T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'0c1bb545-7fe9-40cd-975d-cd89b795c2c3', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 31861, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643639', N'15626436263', N'Tug Fueling', CAST(N'2021-03-14T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'95f3dc43-5697-491a-aa17-9ca3a6598c88', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 10007, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643640', N'15626436263', N'Tug Fueling', CAST(N'2021-03-15T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'a3a468f4-8437-4a13-b52d-adb57c554a7e', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 5736, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643641', N'15626436263', N'Tug Fueling', CAST(N'2021-03-16T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'92a44966-916c-43d7-aead-115deb09b92a', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 14267, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643642', N'15626436263', N'Tug Fueling', CAST(N'2021-03-17T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'49801f1b-dbcd-4f23-a1cf-be6843406512', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 22897, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643643', N'15626436263', N'Tug Fueling', CAST(N'2021-03-18T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'816dc8b2-07b6-4365-ac39-a90722a07c37', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 29748, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643644', N'15626436263', N'Tug Fueling', CAST(N'2021-03-19T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'147681c9-bcf4-433f-9fc3-0e80ce43f4ac', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 21087, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643645', N'15626436263', N'Tug Fueling', CAST(N'2021-03-20T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'27e96cac-f068-4f9a-978f-63444fbe4124', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 26374, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643648', N'15626436263', N'Tug Fueling', CAST(N'2021-03-22T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'cacbed76-3f56-4e93-b30d-a2d4ace1e8cd', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 3296, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643650', N'15626436263', N'Tug Fueling', CAST(N'2021-03-23T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'5e591b24-1260-4e84-90a3-2c4be2ffe931', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 12267, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643651', N'15626436263', N'Tug Fueling', CAST(N'2021-03-24T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'74f903d5-1787-4179-9e8d-bd91442a678e', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 5343, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643652', N'15626436263', N'Tug Fueling', CAST(N'2021-03-25T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'11ef4da0-ed8c-451c-a685-93c6a32e0bad', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 32120, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643653', N'15626436263', N'Tug Fueling', CAST(N'2021-03-26T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'7ffada7c-5ada-42ef-a326-b56d284727fb', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 25774, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643653', N'15626436263', N'Tug Fueling', CAST(N'2021-03-27T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'489d41ac-67b0-4134-a882-21d5a66f5bd7', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 21111, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643653', N'15626436263', N'Tug Fueling', CAST(N'2021-03-28T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'6c9da035-ab23-4081-88f6-d5325d5e1965', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 35239, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643653', N'15626436263', N'Tug Fueling', CAST(N'2021-03-29T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'f9724152-9702-4704-8b39-df3016b26435', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 10880, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643653', N'15626436263', N'Tug Fueling', CAST(N'2021-03-30T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'ceff32bb-4feb-44ff-ae19-07c0b35ac251', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 23084, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643653', N'15626436263', N'Tug Fueling', CAST(N'2021-03-31T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'2193f01f-dadf-4b67-8486-05c830f98936', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_tmp_import_data] ([transaction_type], [transaction_type_code], [product_code], [tx_product_code], [transaction_mode], [net_gallons], [gross_gallons], [carrier_name], [carrier_fein], [carrier_tx_taxpayer], [position_holder_name], [position_holder_fein], [position_holder_tx_taxpayer], [bol], [document_date], [origin_facility], [destintion_facility], [destination_state], [vessel_name], [von_imo], [consignor_name], [consignor_fein], [exchanger_name], [exchanger_fein], [_key], [document_key]) VALUES (N'TD', N'TD_ST', N'227', N'228', N'ST', 20058, 0, N'HARTREE', N'831146678', N'32067863624', N'O''ROURKE MARINE SERVICES, LLC', N'562643646', N'15626436263', N'Tug Fueling', CAST(N'2021-03-21T00:00:00.000' AS DateTime), N'T76TX2850', N'', N'TX', N'', N'', N'', N'', N'', N'', N'2ce39240-c3aa-4f53-9bab-918d03c89ee3', N'32f71106-2405-4d16-8317-bac93dae0334')
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CD_B ', N'CD', N'B ', N'CCR', N'14E', NULL, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CD_EB', N'CD', N'EB', N'CCR', N'14E', NULL, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CD_EP', N'CD', N'EP', N'CCR', N'14E', NULL, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CD_ES', N'CD', N'ES', N'CCR', N'14E', NULL, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CD_PL', N'CD', N'PL', N'CCR', N'14E', NULL, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CD_S ', N'CD', N'S ', N'CCR', N'14E', NULL, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CR_B ', N'CR', N'B ', N'CCR', N'14D', NULL, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CR_IB', N'CR', N'IB', N'CCR', N'14D', NULL, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CR_IP', N'CR', N'IP', N'CCR', N'14D', NULL, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CR_IS', N'CR', N'IS', N'CCR', N'14D', NULL, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CR_PL', N'CR', N'PL', N'CCR', N'14D', NULL, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CR_S ', N'CR', N'S ', N'CCR', N'14D', NULL, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CD_B ', N'CD', N'B ', N'GRP', N'14E', NULL, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CD_EB', N'CD', N'EB', N'GRP', N'14E', NULL, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CD_EP', N'CD', N'EP', N'GRP', N'14E', NULL, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CD_ES', N'CD', N'ES', N'GRP', N'14E', NULL, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CD_PL', N'CD', N'PL', N'GRP', N'14E', NULL, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CD_S ', N'CD', N'S ', N'GRP', N'14E', NULL, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CR_B ', N'CR', N'B ', N'GRP', N'14D', NULL, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CR_IB', N'CR', N'IB', N'GRP', N'14D', NULL, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CR_IP', N'CR', N'IP', N'GRP', N'14D', NULL, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CR_IS', N'CR', N'IS', N'GRP', N'14D', NULL, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CR_PL', N'CR', N'PL', N'GRP', N'14D', NULL, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'CR_S ', N'CR', N'S ', N'GRP', N'14D', NULL, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'IN_BI', N'IN', N'BI', N'GRP', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, NULL, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'IN_EI', N'IN', N'EI', N'GRP', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, NULL, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'IN_TG', N'IN', N'TG', N'GRP', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, NULL, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_AH', N'TD', N'AH', N'GRP', N'15B', NULL, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, NULL, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_AJ', N'TD', N'AJ', N'GRP', N'15B', NULL, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, NULL, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_B ', N'TD', N'B ', N'GRP', N'15B', NULL, 1, 0, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_BA', N'TD', N'BA', N'GRP', N'15B', NULL, 0, 0, 0, 0, 0, 0, NULL, 1, 0, NULL, NULL, NULL, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_CE', N'TD', N'CE', N'GRP', N'15B', NULL, 1, 1, 0, 0, 0, 0, 1, 1, 1, NULL, NULL, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_EB', N'TD', N'EB', N'GRP', N'15B', NULL, 1, 0, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_EJ', N'TD', N'EJ', N'GRP', N'15B', NULL, 1, 0, 1, 0, 0, 0, 1, 1, 1, NULL, 1, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_EP', N'TD', N'EP', N'GRP', N'15B', NULL, 1, 0, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_ER', N'TD', N'ER', N'GRP', N'15B', NULL, 1, 0, 1, 0, 0, 0, 1, 1, 1, NULL, 1, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_ES', N'TD', N'ES', N'GRP', N'15B', NULL, 1, 0, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_J ', N'TD', N'J ', N'GRP', N'15B', NULL, 1, 0, 1, 0, 0, 0, 1, 1, 1, NULL, 1, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_PL', N'TD', N'PL', N'GRP', N'15B', NULL, 1, 0, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_R ', N'TD', N'R ', N'GRP', N'15B', NULL, 1, 0, 1, 0, 0, 0, 1, 1, 1, NULL, 1, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_RF', N'TD', N'RF', N'GRP', N'15B', NULL, 1, 1, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_RR', N'TD', N'RR', N'GRP', N'15B', NULL, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, NULL, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_RS', N'TD', N'RS', N'GRP', N'15B', NULL, 1, 1, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_S ', N'TD', N'S ', N'GRP', N'15B', NULL, 1, 0, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_B ', N'TR', N'B ', N'GRP', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_BA', N'TR', N'BA', N'GRP', N'15A', NULL, 0, 0, 0, 0, 0, 1, NULL, 0, 0, 0, NULL, NULL, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_CE', N'TR', N'CE', N'GRP', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, NULL, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_IB', N'TR', N'IB', N'GRP', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_IJ', N'TR', N'IJ', N'GRP', N'15A', NULL, 1, 0, 1, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_IP', N'TR', N'IP', N'GRP', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_IR', N'TR', N'IR', N'GRP', N'15A', NULL, 1, 0, 1, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_IS', N'TR', N'IS', N'GRP', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_J ', N'TR', N'J ', N'GRP', N'15A', NULL, 1, 0, 1, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_PL', N'TR', N'PL', N'GRP', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_R ', N'TR', N'R ', N'GRP', N'15A', NULL, 1, 0, 1, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_S ', N'TR', N'S ', N'GRP', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'IN_BI', N'IN', N'BI', N'TOR', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, NULL, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'IN_EI', N'IN', N'EI', N'TOR', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, NULL, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'IN_TG', N'IN', N'TG', N'TOR', NULL, NULL, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, NULL, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_AH', N'TD', N'AH', N'TOR', N'15B', NULL, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, NULL, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_AJ', N'TD', N'AJ', N'TOR', N'15B', NULL, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, NULL, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_B ', N'TD', N'B ', N'TOR', N'15B', NULL, 1, 0, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_BA', N'TD', N'BA', N'TOR', N'15B', NULL, 0, 0, 0, 0, 0, 0, NULL, 1, 0, NULL, NULL, NULL, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_CE', N'TD', N'CE', N'TOR', N'15B', NULL, 1, 1, 0, 0, 0, 0, 1, 1, 1, NULL, NULL, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_EB', N'TD', N'EB', N'TOR', N'15B', NULL, 1, 0, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_EJ', N'TD', N'EJ', N'TOR', N'15B', NULL, 1, 0, 1, 0, 0, 0, 1, 1, 1, NULL, 1, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_EP', N'TD', N'EP', N'TOR', N'15B', NULL, 1, 0, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_ER', N'TD', N'ER', N'TOR', N'15B', NULL, 1, 0, 1, 0, 0, 0, 1, 1, 1, NULL, 1, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_ES', N'TD', N'ES', N'TOR', N'15B', NULL, 1, 0, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_J ', N'TD', N'J ', N'TOR', N'15B', NULL, 1, 0, 1, 0, 0, 0, 1, 1, 1, NULL, 1, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_PL', N'TD', N'PL', N'TOR', N'15B', NULL, 1, 0, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_R ', N'TD', N'R ', N'TOR', N'15B', NULL, 1, 0, 1, 0, 0, 0, 1, 1, 1, NULL, 1, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_RF', N'TD', N'RF', N'TOR', N'15B', NULL, 1, 1, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_RR', N'TD', N'RR', N'TOR', N'15B', NULL, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, NULL, 1, 0, 1, 1, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_RS', N'TD', N'RS', N'TOR', N'15B', NULL, 1, 1, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_S ', N'TD', N'S ', N'TOR', N'15B', NULL, 1, 0, 0, 0, 0, 0, 1, 1, NULL, NULL, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_B ', N'TR', N'B ', N'TOR', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_BA', N'TR', N'BA', N'TOR', N'15A', NULL, 0, 0, 0, 0, 0, 1, NULL, 0, 0, 0, NULL, NULL, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_CE', N'TR', N'CE', N'TOR', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, NULL, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_IB', N'TR', N'IB', N'TOR', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_IJ', N'TR', N'IJ', N'TOR', N'15A', NULL, 1, 0, 1, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_IP', N'TR', N'IP', N'TOR', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_IR', N'TR', N'IR', N'TOR', N'15A', NULL, 1, 0, 1, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_IS', N'TR', N'IS', N'TOR', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_J ', N'TR', N'J ', N'TOR', N'15A', NULL, 1, 0, 1, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_PL', N'TR', N'PL', N'TOR', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 0, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_R ', N'TR', N'R ', N'TOR', N'15A', NULL, 1, 0, 1, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 0, 1, NULL, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_S ', N'TR', N'S ', N'TOR', N'15A', NULL, 1, 0, 0, 0, 0, 1, NULL, 0, 0, 0, 1, 1, 1, 1, NULL, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_J_', N'TR', N'J_', N'TXTOR', N'05121', N'Receipt- Truck', 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_R_', N'TR', N'R_', N'TXTOR', N'05121', N'Receipt- Rail', 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_B_', N'TR', N'B_', N'TXTOR', N'05121', N'Receipt- Barge', 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_S_', N'TR', N'S_', N'TXTOR', N'05121', N'Receipt- Ship', 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_PL', N'TR', N'PL', N'TXTOR', N'05121', N'Receipt- Pipeline', 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_BA', N'TR', N'BA', N'TXTOR', N'05121', N'Receipt- Book Transfer', 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_ST', N'TR', N'ST', N'TXTOR', N'05121', N'Receipt- Stationary Transfer', 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TR_RT', N'TR', N'RT', N'TXTOR', N'05121', N'Receipt- Rail Terminal', 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_J_', N'TD', N'J_', N'TXTOR', N'05125', N'Disbursement- Truck', 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_R_', N'TD', N'R_', N'TXTOR', N'05125', N'Disbursement- Rail', 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_B_', N'TD', N'B_', N'TXTOR', N'05125', N'Disbursement- Barge', 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_S_', N'TD', N'S_', N'TXTOR', N'05125', N'Disbursement- Ship', 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_PL', N'TD', N'PL', N'TXTOR', N'05125', N'Disbursement- Pipeline', 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_BA', N'TD', N'BA', N'TXTOR', N'05125', N'Disbursement- Book Transfer', 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_ST', N'TD', N'ST', N'TXTOR', N'05125', N'Disbursement- Stationary Transfer', 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'TD_RT', N'TD', N'RT', N'TXTOR', N'05125', N'Disbursement- Rail Terminal', 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'IN_BI', N'IN', N'BI', N'TXTOR', NULL, N'Invenotry- Beginning', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'IN_EI', N'IN', N'EI', N'TXTOR', NULL, N'Invenotry- Ending', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0)
GO
INSERT [dbo].[rpt_transaction_types] ([tt_tranaction_type_code], [tt_transaction_type], [tt_transaction_mode], [tt_report_type], [tt_schedule_code], [tt_description], [tt_require_carrier], [tt_allow_carrier_override], [tt_allow_carrier_9s], [tt_allow_carrier_4s], [tt_require_consignor], [tt_require_destination], [tt_require_position_holder], [tt_require_origin], [tt_require_ship_to], [tt_require_exchanger], [tt_require_bol], [tt_require_document_date], [tt_require_vessel], [tt_require_net_gallon], [tt_require_gross_gallon], [tt_is_bulk]) VALUES (N'IN_TG', N'IN', N'TG', N'TXTOR', NULL, N'Invenotry- Target Gain Loss', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0)
GO
/****** Object:  Index [pk_documents]    Script Date: 4/23/2021 10:11:08 AM ******/
ALTER TABLE [dbo].[rpt_documents] ADD  CONSTRAINT [pk_documents] PRIMARY KEY NONCLUSTERED 
(
	[_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [pk_errorcodes]    Script Date: 4/23/2021 10:11:08 AM ******/
ALTER TABLE [dbo].[rpt_error_codes] ADD  CONSTRAINT [pk_errorcodes] PRIMARY KEY NONCLUSTERED 
(
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [pk_errors]    Script Date: 4/23/2021 10:11:08 AM ******/
ALTER TABLE [dbo].[rpt_errors] ADD  CONSTRAINT [pk_errors] PRIMARY KEY NONCLUSTERED 
(
	[error_code] ASC,
	[tmp_import_row_key] ASC,
	[error_field] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [pk_prd_irs_code]    Script Date: 4/23/2021 10:11:08 AM ******/
ALTER TABLE [dbo].[product_irs_codes] ADD  CONSTRAINT [pk_prd_irs_code] PRIMARY KEY NONCLUSTERED 
(
	[prd_irs_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [pk_filingentities]    Script Date: 4/23/2021 10:11:08 AM ******/
ALTER TABLE [dbo].[rpt_report_entity] ADD  CONSTRAINT [pk_filingentities] PRIMARY KEY NONCLUSTERED 
(
	[_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[rpt_balance_sheet] ADD  DEFAULT (newid()) FOR [_key]
GO
ALTER TABLE [dbo].[rpt_document_status] ADD  DEFAULT (newid()) FOR [_key]
GO
ALTER TABLE [dbo].[rpt_documents] ADD  CONSTRAINT [df_production]  DEFAULT ((1)) FOR [doc_production]
GO
ALTER TABLE [dbo].[rpt_documents] ADD  CONSTRAINT [df_terminalsoldaquired]  DEFAULT ((0)) FOR [doc_terminal_sold_aquired]
GO
ALTER TABLE [dbo].[rpt_documents] ADD  CONSTRAINT [df_nobusinessactivity]  DEFAULT ((0)) FOR [doc_no_business_activity]
GO
ALTER TABLE [dbo].[rpt_documents] ADD  CONSTRAINT [df_reportbi]  DEFAULT ((0)) FOR [doc_report_bi]
GO
ALTER TABLE [dbo].[rpt_documents_group_reports] ADD  DEFAULT (newid()) FOR [_key]
GO
ALTER TABLE [dbo].[rpt_documents_group_reports] ADD  DEFAULT ((0)) FOR [gdoc_no_business_activity]
GO
ALTER TABLE [dbo].[product_irs_codes] ADD  DEFAULT (newid()) FOR [_key]
GO
ALTER TABLE [dbo].[product_irs_codes] ADD  DEFAULT ((0)) FOR [prd_ba_allowed]
GO
ALTER TABLE [dbo].[product_irs_codes] ADD  DEFAULT ((0)) FOR [prd_ce_allowed]
GO
ALTER TABLE [dbo].[product_irs_codes] ADD  DEFAULT ((0)) FOR [prd_require_pos_holder]
GO
ALTER TABLE [dbo].[product_irs_codes] ADD  DEFAULT ((1)) FOR [active]
GO
ALTER TABLE [dbo].[rpt_report_entity] ADD  CONSTRAINT [DF_report_entity__key]  DEFAULT (newid()) FOR [_key]
GO
ALTER TABLE [dbo].[rpt_report_entity] ADD  CONSTRAINT [DF_report_entity_re_is_group]  DEFAULT ((0)) FOR [re_is_group]
GO
ALTER TABLE [dbo].[rpt_report_entity_group_members] ADD  DEFAULT (newid()) FOR [rptg_key]
GO
ALTER TABLE [dbo].[rpt_report_entity_group_members] ADD  DEFAULT ((1)) FOR [rptg_terminal]
GO
ALTER TABLE [dbo].[rpt_report_entity_group_members] ADD  DEFAULT ((0)) FOR [rptg_carrier]
GO
ALTER TABLE [dbo].[rpt_report_entity_group_members] ADD  DEFAULT ((0)) FOR [rptg_delete_flg]
GO
ALTER TABLE [dbo].[rpt_report_versions] ADD  CONSTRAINT [DF__report_ver___key__34C8D9D1]  DEFAULT (newid()) FOR [_key]
GO
ALTER TABLE [dbo].[rpt_report_versions] ADD  CONSTRAINT [DF_report_versions_rpt_is_original]  DEFAULT ((0)) FOR [rpt_is_original]
GO
ALTER TABLE [dbo].[rpt_report_versions] ADD  CONSTRAINT [DF_report_versions_rpt_is_correction]  DEFAULT ((0)) FOR [rpt_is_correction]
GO
ALTER TABLE [dbo].[rpt_report_versions] ADD  CONSTRAINT [DF_report_versions_rpt_is_replacement]  DEFAULT ((0)) FOR [rpt_is_replacement]
GO
ALTER TABLE [dbo].[rpt_report_versions] ADD  CONSTRAINT [DF_report_versions_rpt_is_supplemental]  DEFAULT ((0)) FOR [rpt_is_supplemental]
GO
ALTER TABLE [dbo].[rpt_reports] ADD  DEFAULT (newid()) FOR [_key]
GO
ALTER TABLE [dbo].[rpt_reports] ADD  DEFAULT ((1)) FOR [rpt_is_federal]
GO
ALTER TABLE [dbo].[rpt_reports] ADD  DEFAULT ((0)) FOR [rpt_is_state]
GO
ALTER TABLE [dbo].[rpt_reports] ADD  DEFAULT ((1)) FOR [rpt_active]
GO
ALTER TABLE [dbo].[rpt_state_codes] ADD  DEFAULT (newid()) FOR [_key]
GO
ALTER TABLE [dbo].[rpt_ticket_bol] ADD  CONSTRAINT [df_ticket_bol_key]  DEFAULT (newid()) FOR [_key]
GO
ALTER TABLE [dbo].[rpt_ticket_header] ADD  CONSTRAINT [df_ticket_header_key]  DEFAULT (newid()) FOR [_key]
GO
ALTER TABLE [dbo].[rpt_tmp_import_data] ADD  CONSTRAINT [DF_tmp_import_data__key]  DEFAULT (newid()) FOR [_key]
GO
ALTER TABLE [dbo].[rpt_ticket_bol]  WITH CHECK ADD  CONSTRAINT [ck_ref551_bol] CHECK  (([tb_ref_55]<(100000000000000000000.)))
GO
ALTER TABLE [dbo].[rpt_ticket_bol] CHECK CONSTRAINT [ck_ref551_bol]
GO
ALTER TABLE [dbo].[rpt_ticket_header]  WITH CHECK ADD  CONSTRAINT [ck_ref551] CHECK  (([th_ref_55]<(100000000000000000000.)))
GO
ALTER TABLE [dbo].[rpt_ticket_header] CHECK CONSTRAINT [ck_ref551]
GO
/****** Object:  StoredProcedure [dbo].[mgn_ex_report_document_drop_down]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 -- ===================================================
 -- Program Copyright Mangan Inc
 --  Author: Candice Iago
 --  Creation Date 4/21/21
 -- ===================================================
 -- EXEC mgn_ex_report_document_drop_down
 --
 --- Creates the stored procedure that checks the dbo.RawTrans for rpt_errors
 ---- Puts the code into system as a stored procedure

CREATE   PROCEDURE  [dbo].[mgn_ex_report_document_drop_down]

--WITH   ENCRYPTION 
AS
 

 SELECT d._key,  CONVERT(varchar(30), d.doc_irs_end_date, 101)  + '-' +   r.rpt_type  + '-' + re.re_name
 FROM rpt_documents d INNER JOIN 
	  rpt_reports r  ON r._key = d.doc_report_type_key INNER JOIN 
	  rpt_report_entity re ON re._key = d.doc_report_entity_key
	ORDER BY d.doc_irs_end_date desc
GO
/****** Object:  StoredProcedure [dbo].[mgn_ex_report_tor_ccr]    Script Date: 4/23/2021 10:11:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 -- ===================================================
 -- Program Copyright Mangan Inc
 --  Author: Candice Iago
 --  Creation Date 4/21/21
 -- ===================================================
 -- EXEC mgn_ex_report_tor_ccr
 --
 --- Creates the stored procedure that checks the dbo.RawTrans for rpt_errors
 ---- Puts the code into system as a stored procedure

CREATE   PROCEDURE  [dbo].[mgn_ex_report_tor_ccr]
--DECLARE	
@document_key uniqueidentifier = '32f71106-2405-4d16-8317-bac93dae0334'

--WITH   ENCRYPTION 
AS
 
DECLARE @docType varchar(3) = (SELECT LEFT(r.rpt_type, 3) 
							   FROM rpt_reports r INNER JOIN 
								    rpt_documents d ON d.doc_report_type_key = r._key
							   WHERE d._key = @document_key)


SET NOCOUNT ON

		SELECT rpt_balance_sheet.bs_product_code AS ProductCode, 
		rpt_balance_sheet.bs_irs_end_date as IRSEndDate, 
		rpt_balance_sheet.bs_beginning_inventory AS BeginningInv, 
		(ISNULL(rpt_balance_sheet.bs_receipts,0) + ISNULL(rpt_balance_sheet.bs_book_receipts,0)) AS TotalReceipts,
		(ISNULL(rpt_balance_sheet.bs_receipts,0) + ISNULL(rpt_balance_sheet.bs_book_receipts,0) + ISNULL(rpt_balance_sheet.bs_beginning_inventory,0)) AS TotalAvailable,
		(ISNULL(rpt_balance_sheet.bs_book_disbursements,0) + ISNULL(rpt_balance_sheet.bs_disbursements,0)) AS TotalDisbursements,
		((ISNULL(rpt_balance_sheet.bs_receipts,0) + ISNULL(rpt_balance_sheet.bs_book_receipts,0) + ISNULL(rpt_balance_sheet.bs_beginning_inventory,0)) - 
			(ISNULL(rpt_balance_sheet.bs_book_disbursements,0) + ISNULL(rpt_balance_sheet.bs_disbursements,0))) AS BookInventory,               
		rpt_balance_sheet.bs_ending_inventory AS EndingInv, 
		--rpt_balance_sheet.bs_gain_loss, 
		rpt_balance_sheet.bs_gain_loss AS CalculatedGL, 
		rpt_report_entity.re_name AS [Name],
		rpt_report_entity.re_fein AS FEIN, 
		ISNULL(rpt_balance_sheet.bs_tcn, 'Carrier') AS FacilityControlNumber,               
		rpt_report_entity.re_637_registration  AS Registration_637,
		--CASE WHEN @docType = 'GRP' THEN (SELECT ISNULL(ent_physical_address1, '') + ' ' + ISNULL(ent_city, '') + ' ' + ISNULL(ent_state, '') + ' ' + ISNULL(ent_zip, '') FROM filing_entity_group_members WHERE ent_key = rpt_balance_sheet.GroupID )
		--	ELSE 
			(ISNULL(rpt_report_entity.re_physical_address1,'') +' '+ ISNULL(rpt_report_entity.re_physical_address2,'') + ' ' + 
		ISNULL(rpt_report_entity.re_physical_city,'') +' '+ ISNULL(rpt_report_entity.re_physical_state,'') + ' ' +  
		ISNULL(rpt_report_entity.re_physical_zip,''))   AS TerminalLocation,
		(rpt_report_entity.re_mailing_address1+' '+ ISNULL(rpt_report_entity.re_mailing_address2,'')   ) AS MailingAddress,
		(rpt_report_entity.re_mailing_city +' '+ rpt_report_entity.re_mailing_state + ' ' +  rpt_report_entity.re_mailing_zip) AS MailingCityStateZip,
		rpt_report_entity.re_general_contact_name AS ContactPerson,
		rpt_report_entity.re_general_contact_phone AS DaytimePhone,
		rpt_report_entity.re_general_contact_fax AS FaxNumber,
		rpt_report_entity.re_general_contact_email AS Email, 
		rpt_reports.rpt_type AS DocumentType, 
		rv.rpt_version_description  as ReportType, 
		d._key, 
		d.doc_creation_datetime
		FROM            rpt_balance_sheet INNER JOIN
		rpt_documents d ON rpt_balance_sheet.bs_document_key = d._key INNER JOIN
		rpt_report_entity ON d.doc_report_entity_key = rpt_report_entity._key INNER JOIN
		rpt_reports ON d.doc_report_type_key = rpt_reports._key INNER JOIN 
		rpt_report_versions rv On d.doc_report_version_key = rv._key
		WHERE    d._key = @document_key
		ORDER BY rpt_balance_sheet.bs_tcn

--SET NOCOUNT OFF
 -- <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
GO
