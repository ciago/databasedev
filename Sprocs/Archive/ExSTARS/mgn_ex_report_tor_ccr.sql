
/****** Object:  StoredProcedure [dbo].[mgn_ex_report_tor_ccr]    Script Date: 4/21/2021 7:32:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 -- ===================================================
 -- Program Copyright Mangan Inc
 --  Author: Candice Iago
 --  Creation Date 4/21/21
 -- ===================================================
 -- EXEC mgn_ex_report_tor_ccr
 --
 --- Creates the stored procedure that checks the dbo.RawTrans for errors
 ---- Puts the code into system as a stored procedure

CREATE OR ALTER PROCEDURE  [dbo].mgn_ex_report_tor_ccr
--DECLARE	
@document_key uniqueidentifier = '32f71106-2405-4d16-8317-bac93dae0334'

--WITH   ENCRYPTION 
AS
 
DECLARE @docType varchar(3) = (SELECT LEFT(r.rpt_type, 3) 
							   FROM rpt_reports r INNER JOIN 
								    rpt_documents d ON d.doc_report_type_key = r._key
							   WHERE d._key = @document_key)


SET NOCOUNT ON

		SELECT bs.bs_product_code AS ProductCode, 
		bs.bs_irs_end_date as IRSEndDate, 
		bs.bs_beginning_inventory AS BeginningInv, 
		(ISNULL(bs.bs_receipts,0) + ISNULL(bs.bs_book_receipts,0)) AS TotalReceipts,
		(ISNULL(bs.bs_receipts,0) + ISNULL(bs.bs_book_receipts,0) + ISNULL(bs.bs_beginning_inventory,0)) AS TotalAvailable,
		(ISNULL(bs.bs_book_disbursements,0) + ISNULL(bs.bs_disbursements,0)) AS TotalDisbursements,
		((ISNULL(bs.bs_receipts,0) + ISNULL(bs.bs_book_receipts,0) + ISNULL(bs.bs_beginning_inventory,0)) - 
			(ISNULL(bs.bs_book_disbursements,0) + ISNULL(bs.bs_disbursements,0))) AS BookInventory,               
		bs.bs_ending_inventory AS EndingInv, 
		bs.bs_gain_loss AS CalculatedGL, 
		re.re_name AS [Name],
		re.re_fein AS FEIN, 
		ISNULL(bs.bs_tcn, 'Carrier') AS FacilityControlNumber,               
		re.re_637_registration  AS Registration_637,
		--CASE WHEN @docType = 'GRP' THEN (SELECT ISNULL(ent_physical_address1, '') + ' ' + ISNULL(ent_city, '') + ' ' + ISNULL(ent_state, '') + ' ' + ISNULL(ent_zip, '') FROM filing_entity_group_members WHERE ent_key = balance_sheet.GroupID )
		--	ELSE 
			(ISNULL(re.re_physical_address1,'') +' '+ ISNULL(re.re_physical_address2,'') + ' ' + 
		ISNULL(re.re_physical_city,'') +' '+ ISNULL(re.re_physical_state,'') + ' ' +  
		ISNULL(re.re_physical_zip,''))   AS TerminalLocation,
		(re.re_mailing_address1+' '+ ISNULL(re.re_mailing_address2,'')   ) AS MailingAddress,
		(re.re_mailing_city +' '+ re.re_mailing_state + ' ' +  re.re_mailing_zip) AS MailingCityStateZip,
		re.re_general_contact_name AS ContactPerson,
		re.re_general_contact_phone AS DaytimePhone,
		re.re_general_contact_fax AS FaxNumber,
		re.re_general_contact_email AS Email, 
		rr.rpt_type AS DocumentType, 
		rv.rpt_version_description  as ReportType, 
		d._key, 
		d.doc_creation_datetime
		FROM  rpt_balance_sheet  bs INNER JOIN
		rpt_documents d ON bs.bs_document_key = d._key INNER JOIN
		rpt_report_entity re ON d.doc_report_entity_key = re._key INNER JOIN
		rpt_reports rr ON d.doc_report_type_key = rr._key INNER JOIN 
		rpt_report_versions rv On d.doc_report_version_key = rv._key
		WHERE    d._key = @document_key
		ORDER BY bs.bs_tcn

--SET NOCOUNT OFF
 -- <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
