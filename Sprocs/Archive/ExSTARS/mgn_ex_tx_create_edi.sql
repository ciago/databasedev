
--CREATE OR ALTER PROCEDURE mgn_ex_tx_create_edi

 DECLARE  @document_key uniqueidentifier = '6599E51C-922F-469A-A376-7863E5DB15C6'
--WITH ENCRYPTION 
--AS
 
--SET NOCOUNT ON



DECLARE @production bit = (SELECT doc_production FROM rpt_documents WHERE _key = @document_key)
DECLARE @orig bit = (SELECT rv.rpt_is_original FROM rpt_documents d inner join rpt_report_versions rv ON d.doc_report_version_key = rv._key WHERE d._key = @document_key)
DECLARE @correction bit =  (SELECT rv.rpt_is_correction FROM rpt_documents d inner join rpt_report_versions rv ON d.doc_report_version_key = rv._key  WHERE d._key = @document_key)
DECLARE @report_type varchar(10) = (SELECT r.rpt_type FROM rpt_documents d INNER JOIN rpt_reports r ON d.doc_report_type_key = r._key WHERE d._key = @document_key)





DECLARE @rpt TABLE (segment varchar(max), row_order int IDENTITY(1,1))


--******************** ticket table setup ***********************************************
DECLARE @tickets TABLE (row_key uniqueidentifier, bol_row_key uniqueidentifier, row_order int IDENTITY(1,1))
DECLARE @current_bol_key uniqueidentifier

INSERT INTO @tickets(row_key, bol_row_key)
SELECT th._key , tb._key
FROM rpt_ticket_header th INNER JOIN 
	 rpt_ticket_bol tb ON th._key = tb.tb_ticket_header_key
WHERE th_document_key = @document_key

DECLARE @tickets_count int = (SELECT COUNT(row_key) FROM @tickets);
DECLARE @tickets_counter int = 0;

--******************** balance sheet setup ***********************************************
DECLARE @inv TABLE (row_key uniqueidentifier, product varchar(10), beginning int, ending int, gain_loss int, receipts int, disbursements int, row_order int IDENTITY(1,1))


INSERT INTO @inv(row_key, product, beginning , ending , gain_loss, receipts, disbursements)
SELECT bs._key , bs.bs_product_code, bs.bs_beginning_inventory, bs.bs_ending_inventory, bs.bs_gain_loss, bs.bs_book_receipts + bs.bs_receipts, bs.bs_book_disbursements + bs.bs_disbursements
FROM rpt_balance_sheet bs 
WHERE bs_document_key = @document_key

DECLARE @inv_count int = (SELECT COUNT(row_key) FROM @inv);
DECLARE @inv_counter int = 0;



--******************** edi segments  ***********************************************
--ISA 
INSERT INTO @rpt (segment)
SELECT 	'ISA~03~' + 
	    CASE WHEN @production = 1 THEN 'TX813050RP' ELSE 'TX813050RT' END + 
		'~00~          ~ZZ~' + re.re_tx_taxpayer_number + '    ~ZZ~TEX COMPTROLLER~' + 
		CONVERT( VARCHAR, d.doc_creation_datetime, 12) + '~' + 
		REPLACE(LEFT(CONVERT( VARCHAR,d.doc_creation_datetime, 108), 5), ':', '') + 
		'~U~00403~' + CONVERT(VARCHAR(20), d.doc_isa13_iea02) + '~0~'+
		CASE WHEN @production = 1 THEN 'P' ELSE 'T' END + '~^\'
FROM rpt_documents d INNER JOIN 
	 rpt_report_entity re ON d.doc_report_entity_key = re._key
WHERE d._key = @document_key
		

INSERT INTO @rpt (segment)
SELECT 	'GS~TF~' + re.re_tx_taxpayer_number + '~TEX COMPTROLLER~'
		+ CONVERT( VARCHAR, d.doc_creation_datetime, 112) + '~' + REPLACE(CONVERT( VARCHAR,d.doc_creation_datetime, 108), ':', '') 
		+ '00~' + CONVERT(VARCHAR(20), d.doc_gs06_gee02) + '~X~004030\'
FROM rpt_documents d INNER JOIN 
	 rpt_report_entity re ON d.doc_report_entity_key = re._key
WHERE d._key = @document_key


INSERT INTO @rpt (segment)
SELECT 	'ST~813~' + CONVERT(varchar,d.doc_st02_se02) + '\'
FROM rpt_documents d
WHERE d._key = @document_key

INSERT INTO @rpt (segment)
SELECT 	'BTI~T6~050~47~TX~' + CONVERT( VARCHAR, d.doc_creation_datetime, 112) + '~~94~50002~49~' + re.re_tx_taxpayer_number + '~SV~'
		+ CASE WHEN @orig = 1 THEN '~00\' ELSE '~~CO\' END
FROM rpt_documents d INNER JOIN 
	 rpt_report_entity re ON d.doc_report_entity_key = re._key
WHERE d._key = @document_key

INSERT INTO @rpt (segment)
SELECT 	'DTM~194~' + CONVERT( VARCHAR, d.doc_irs_end_date, 112) + '\'
FROM rpt_documents d
WHERE d._key = @document_key

INSERT INTO @rpt (segment)
SELECT 	'N1~L9~' + re.re_name + '\'
FROM rpt_documents d INNER JOIN 
	 rpt_report_entity re ON d.doc_report_entity_key = re._key
WHERE d._key = @document_key

INSERT INTO @rpt (segment)
SELECT 	CASE WHEN re.re_mailing_address1 IS NOT NULL THEN ('N3~' + ISNULL(re.re_mailing_address1,'')  +  '\' )  ELSE NULL END
FROM rpt_documents d INNER JOIN 
	 rpt_report_entity re ON d.doc_report_entity_key = re._key
WHERE d._key = @document_key

INSERT INTO @rpt (segment)
SELECT 	CASE WHEN re.re_mailing_address1 IS NOT NULL THEN 'N4~' + ISNULL(re.re_mailing_city,'') + 
	     '~' + ISNULL(re.re_mailing_state,'') + '~' + ISNULL(re.re_mailing_zip,'') + '~' + 
		 ISNULL(LEFT(re.re_mailing_country,2),'') + '\'ELSE NULL END
FROM rpt_documents d INNER JOIN 
	 rpt_report_entity re ON d.doc_report_entity_key = re._key
WHERE d._key = @document_key

INSERT INTO @rpt (segment)
SELECT 	CASE WHEN re.re_general_contact_name IS NOT NULL THEN 'PER~CN~'+ISNULL(re.re_general_contact_name,'')+
		'~TE~'+ISNULL(re.re_general_contact_phone,'')+'~FX~'+ISNULL(re.re_general_contact_fax,'')+'~EM~'+
		ISNULL(re.re_general_contact_email,'')+'\' ELSE NULL END
FROM rpt_documents d INNER JOIN 
	 rpt_report_entity re ON d.doc_report_entity_key = re._key
WHERE d._key = @document_key


INSERT INTO @rpt (segment)
SELECT 	'TFS~T2~' + (CASE WHEN @orig = 1 THEN '05100' ELSE '05110' END) + '~~~TC~' + re.re_tcn + '\'
FROM @inv i INNER JOIN
		rpt_balance_sheet bs ON i.row_key = bs._key INNER JOIN
		rpt_documents d ON bs.bs_document_key = d._key INNER JOIN 
		rpt_report_entity re ON re._key = d.doc_report_entity_key
WHERE row_order = @inv_counter + 1

WHILE (@inv_counter < @inv_count)
BEGIN


	INSERT INTO @rpt (segment)
	SELECT 	'FGS~SU~PG~'+ product +'\'
	FROM @inv
	WHERE row_order = @inv_counter + 1

	INSERT INTO @rpt (segment)
	SELECT 	'TIA~5000~~~'+CONVERT(varchar(20),beginning) +'~GA\'
	FROM @inv
	WHERE row_order = @inv_counter + 1

	INSERT INTO @rpt (segment)
	SELECT 	'TIA~5001~~~'+CONVERT(varchar(20),receipts) +'~GA\'
	FROM @inv
	WHERE row_order = @inv_counter + 1

	INSERT INTO @rpt (segment)
	SELECT 	 'TIA~5004~~~'+CONVERT(varchar(20),disbursements) +'~GA\'
	FROM @inv
	WHERE row_order = @inv_counter + 1

	INSERT INTO @rpt (segment)
	SELECT 	 'TIA~' + (CASE WHEN gain_loss >= 0 THEN '5006' ELSE '5007' END) +'~~~'+ 
		     CONVERT(varchar(20),(CASE WHEN gain_loss>=0 THEN gain_loss ELSE (gain_loss * -1) END)) +'~GA\'
	FROM @inv
	WHERE row_order = @inv_counter + 1

	INSERT INTO @rpt (segment)
	SELECT 	 'TIA~5008~~~'+CONVERT(varchar(20),ending) +'~GA\'
	FROM @inv
	WHERE row_order = @inv_counter + 1



	SET @inv_counter = @inv_counter + 1
END


WHILE (@tickets_counter < @tickets_count)
BEGIN

	SET @current_bol_key = (SELECT bol_row_key FROM @tickets WHERE row_order = @tickets_counter + 1)

	--SELECT *
	--FROM @tickets t INNER JOIN 
	--	  ticket_header th ON t.row_key = th._key INNER JOIN 
	--	  ticket_bol tb ON t.bol_row_key = tb._key INNER JOIN 
	--	  documents d ON th.th_document_key = d._key
	--WHERE tb._key = @current_bol_key

	INSERT INTO @rpt (segment)
	SELECT 	'TFS~T3~' + (CASE WHEN @orig = 1 THEN tt.tt_schedule_code ELSE '0' + 
			 CONVERT(varchar, (CONVERT(int, tt.tt_schedule_code) + 2)) END) + '~PG~' + 
			 th.th_tx_product_code + '~94~' + tt.tt_transaction_mode + '\'
	FROM @tickets t INNER JOIN 
		  rpt_ticket_header th ON t.row_key = th._key INNER JOIN 
		  rpt_ticket_bol tb ON t.bol_row_key = tb._key INNER JOIN 
		  rpt_documents d ON th.th_document_key = d._key INNER JOIN 
		  rpt_transaction_types tt on th.th_tranaction_type_code = tt.tt_tranaction_type_code 
	WHERE tb._key = @current_bol_key AND tt.tt_report_type = @report_type


	INSERT INTO @rpt (segment)
	SELECT 	'REF~FJ~'+ RIGHT(CONVERT(varchar(20),tb.tb_ref_55), 7) +'\'
	FROM @tickets t INNER JOIN 
		  rpt_ticket_header th ON t.row_key = th._key INNER JOIN 
		  rpt_ticket_bol tb ON t.bol_row_key = tb._key INNER JOIN 
		  rpt_documents d ON th.th_document_key = d._key
	WHERE tb._key = @current_bol_key


	INSERT INTO @rpt (segment)
	SELECT 	CASE WHEN th.th_origin_facility <> '' THEN 'N1~OT~' + re.re_name + '~TC~' + th.th_origin_facility + '\' ELSE NULL END
	FROM @tickets t INNER JOIN 
		  rpt_ticket_header th ON t.row_key = th._key INNER JOIN 
		  rpt_ticket_bol tb ON t.bol_row_key = tb._key INNER JOIN 
		  rpt_documents d ON th.th_document_key = d._key INNER JOIN 
		  rpt_report_entity re ON d.doc_report_entity_key = re._key
	WHERE tb._key = @current_bol_key AND th.th_origin_facility <> ''


	INSERT INTO @rpt (segment)
	SELECT 	CASE WHEN th.th_destination_facility <> '' THEN 'N1~DT~' + re.re_name + '~TC~' + th.th_destination_facility + '\'ELSE NULL END
	FROM @tickets t INNER JOIN 
		  rpt_ticket_header th ON t.row_key = th._key INNER JOIN 
		  rpt_ticket_bol tb ON t.bol_row_key = tb._key INNER JOIN 
		  rpt_documents d ON th.th_document_key = d._key INNER JOIN 
		  rpt_report_entity re ON d.doc_report_entity_key = re._key
	WHERE tb._key = @current_bol_key AND th.th_destination_facility <> ''


	INSERT INTO @rpt (segment)
	SELECT 	CASE WHEN th.th_destination_state <> '' THEN 'N1~ST~' + th.th_destination_state + '\'ELSE NULL END
	FROM @tickets t INNER JOIN 
		  rpt_ticket_header th ON t.row_key = th._key INNER JOIN 
		  rpt_ticket_bol tb ON t.bol_row_key = tb._key INNER JOIN 
		  rpt_documents d ON th.th_document_key = d._key
	WHERE tb._key = @current_bol_key AND th.th_destination_state <> '' 


	INSERT INTO @rpt (segment)
	SELECT CASE WHEN th.th_position_holder_name <> '' THEN 'N1~CI~' + LEFT(th.th_position_holder_name,35) + '~49~' + 
		   CASE WHEN th.th_position_holder_tx_taxpayer = '' THEN (th.th_position_holder_fein + '  ') ELSE th.th_position_holder_tx_taxpayer END +'\' ELSE NULL END
	FROM @tickets t INNER JOIN 
		  rpt_ticket_header th ON t.row_key = th._key INNER JOIN 
		  rpt_ticket_bol tb ON t.bol_row_key = tb._key INNER JOIN 
		  rpt_documents d ON th.th_document_key = d._key
	WHERE tb._key = @current_bol_key AND  th.th_position_holder_name <> ''


	INSERT INTO @rpt (segment)
	SELECT CASE WHEN th.th_carrier_name <> '' THEN 'N1~CA~' + LEFT(th.th_carrier_name,35) + '~49~' + 
		   CASE WHEN th.th_carrier_tx_taxpayer = '' THEN (th.th_carrier_fein + '  ') ELSE th.th_carrier_tx_taxpayer END +'\' ELSE NULL END
	--+ th.th_carrier_fein + '  \' ELSE NULL END
	FROM @tickets t INNER JOIN 
		  rpt_ticket_header th ON t.row_key = th._key INNER JOIN 
		  rpt_ticket_bol tb ON t.bol_row_key = tb._key INNER JOIN 
		  rpt_documents d ON th.th_document_key = d._key
	WHERE tb._key = @current_bol_key AND th.th_carrier_name <> ''


	INSERT INTO @rpt (segment)
	SELECT 'FGS~D~BM~'+ RIGHT(tb.tb_bol,9) +'\'
	FROM @tickets t INNER JOIN 
		  rpt_ticket_header th ON t.row_key = th._key INNER JOIN 
		  rpt_ticket_bol tb ON t.bol_row_key = tb._key INNER JOIN 
		  rpt_documents d ON th.th_document_key = d._key
	WHERE tb._key = @current_bol_key


	INSERT INTO @rpt (segment)
	SELECT 'DTM~095~'+CONVERT(varchar,tb.tb_document_date,112 )+'\'
	FROM @tickets t INNER JOIN 
		  rpt_ticket_header th ON t.row_key = th._key INNER JOIN 
		  rpt_ticket_bol tb ON t.bol_row_key = tb._key INNER JOIN 
		  rpt_documents d ON th.th_document_key = d._key
	WHERE tb._key = @current_bol_key


	INSERT INTO @rpt (segment)
	SELECT 'TIA~5005~~~'+CONVERT( varchar(20), tb.tb_net_gallons)+'~GA\'
	FROM @tickets t INNER JOIN 
		  rpt_ticket_header th ON t.row_key = th._key INNER JOIN 
		  rpt_ticket_bol tb ON t.bol_row_key = tb._key INNER JOIN 
		  rpt_documents d ON th.th_document_key = d._key
	WHERE tb._key = @current_bol_key

	SET @tickets_counter = @tickets_counter + 1
END


-- get segment count 
DECLARE @segment_count int 
SET @segment_count = (SELECT COUNT(segment) FROM @rpt) - 1 ;

INSERT INTO @rpt (segment)
SELECT 'SE~'+ CONVERT(varchar,@segment_count) + '~' + CONVERT(varchar,d.doc_st02_se02) + '\'
FROM rpt_documents d INNER JOIN 
	 rpt_report_entity re ON d.doc_report_entity_key = re._key
WHERE d._key = @document_key

INSERT INTO @rpt (segment)
SELECT 'GE~000001~' + CONVERT(varchar,d.doc_gs06_gee02) + '\'
FROM rpt_documents d INNER JOIN 
	 rpt_report_entity re ON d.doc_report_entity_key = re._key
WHERE d._key = @document_key

INSERT INTO @rpt (segment)
SELECT 'IEA~00001~' + CONVERT(varchar,d.doc_isa13_iea02) + '\'
FROM rpt_documents d INNER JOIN 
	 rpt_report_entity re ON d.doc_report_entity_key = re._key
WHERE d._key = @document_key
								   
select * from @rpt



  
SET NOCOUNT OFF
 -- <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
