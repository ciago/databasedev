
--/****** Object:  StoredProcedure [dbo].[mgn_ex_tx_balance_sheet]    Script Date: 3/22/2021 10:01:08 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

-- -- ===================================================
-- -- Program Copyright Mangan Inc.
-- --  Author: Candice Iago 
-- --  Creation Date 12/13/13
-- -- ===================================================
-- -- EXEC mgn_ex_tx_balance_sheet
-- --


--CREATE OR ALTER PROCEDURE  [dbo].[mgn_ex_tx_balance_sheet]
 declare @document_key uniqueidentifier  = '6599E51C-922F-469A-A376-7863E5DB15C6'
--WITH ENCRYPTION 
--AS
 
--SET NOCOUNT ON



IF	
((SELECT TOP 1 rpt_errors.error_code 
	FROM rpt_errors INNER JOIN   rpt_tmp_import_data ON rpt_errors.tmp_import_row_key = rpt_tmp_import_data._key 
	WHERE (rpt_errors.error_code = '4') AND (rpt_tmp_import_data.document_key = @document_key)) IS NULL)

		BEGIN
		
			--Delete the current document 
			DELETE 
			FROM            rpt_balance_sheet			
			WHERE			(rpt_balance_sheet.bs_document_key = @document_key)
			
			--DECLARE @document_key UNIQUEIDENTIFIER = 'df92a5b0-576d-4c23-9d69-174c07718f88'
			--   insert a row for each product code in the file
			--DECLARE @document_key uniqueidentifier  = 'd4fd7fc9-49a7-4ae9-a820-314bbe2b1c0f'
			INSERT INTO rpt_balance_sheet
									 (bs_product_code, bs_irs_end_date, bs_document_key, bs_tcn)
			SELECT DISTINCT tmp.tx_product_code, doc.doc_irs_end_date, tmp.document_key, re_tcn
			FROM            rpt_tmp_import_data AS tmp INNER JOIN
									 rpt_documents AS doc ON tmp.document_key = doc._key INNER JOIN 
									 rpt_report_entity re ON re._key = doc.doc_report_entity_key--INNER JOIN
									 --TransTypesState AS tt ON tmp.TransTypeId = tt.TransTypeID
			WHERE tmp.document_key = @document_key


			DECLARE @IRSDate date
			SET @IRSDate = (SELECT doc_irs_end_date FROM rpt_documents WHERE _key = @document_key)	

		

			--Ending Inventory, Beginning, GainLoss and Book Fields only apply to the TOR report and not the carrier reports
			
			--***************************** Ending Inventory
			--Grab any ending inventories from the file
			 --DECLARE @document_key uniqueidentifier  = 'd4fd7fc9-49a7-4ae9-a820-314bbe2b1c0f'
			UPDATE dbo.rpt_balance_sheet           
			SET      bs_ending_inventory =  ISNULL(tmp.net_gallons, 0)
			FROM            rpt_balance_sheet AS bs INNER JOIN
			(SELECT document_key,tx_product_code, SUM(net_gallons) as net_gallons 
			 FROM rpt_tmp_import_data 
			 WHERE transaction_type_code = 'IN_EI' 
			 GROUP BY tx_product_code, document_key)AS tmp ON bs.bs_document_key = tmp.document_key AND bs.bs_product_code = tmp.tx_product_code
			WHERE  bs.bs_document_key = @document_key
			

			-- Set any product code ending inventories to 0 if there was no transaction in the raw data			
			UPDATE dbo.rpt_balance_sheet           
			SET      bs_ending_inventory =  0
			FROM            rpt_balance_sheet 
			WHERE        (bs_ending_inventory IS NULL)  AND (bs_document_key = @document_key)


			-- ***************************Beginning Inventory
			--Update Beginning Inv and set to ending inv from month before if it exists

			

			DECLARE @PFilingEntity uniqueidentifier = (SELECT doc_report_entity_key FROM rpt_documents WHERE _key = @document_key)
			DECLARE @bs_irs_end_date DATE = (SELECT doc_irs_end_date FROM dbo.rpt_documents  WHERE (dbo.rpt_documents._key = @document_key))
			DECLARE @PrevIRSEndDate DATE = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@bs_irs_end_date),0))  --DATEADD(mm, -1, @bs_irs_end_date)
			DECLARE @PrevDocument uniqueidentifier = (SELECT TOP (1) _key
														FROM     rpt_documents
														WHERE    (doc_report_entity_key = @PFilingEntity) AND (doc_irs_end_date = @PrevIRSEndDate) --AND ( DocumentTypeID <> 'TOR_6S')
														ORDER BY doc_creation_datetime DESC)
					
			CREATE TABLE #BeginningInvTemp( bs_product_code varchar(3), BeginningInv bigint)


					
			INSERT INTO #BeginningInvTemp (bs_product_code, BeginningInv)
				(SELECT  bs_product_code, bs_ending_inventory
					FROM dbo.rpt_balance_sheet 
					WHERE (rpt_balance_sheet.bs_irs_end_date = @PrevIRSEndDate) AND  rpt_balance_sheet.bs_document_key = @PrevDocument
					)

					

			UPDATE dbo.rpt_balance_sheet                         
			SET      bs_beginning_inventory =  #BeginningInvTemp.BeginningInv
			FROM            rpt_balance_sheet AS bs LEFT OUTER JOIN
				#BeginningInvTemp ON  #BeginningInvTemp.bs_product_code = bs.bs_product_code
			



			drop table #BeginningInvTemp





			-- Update beginning inv with existing beginning INV from the raw trans IF it has not been filled by the prior query
			
			UPDATE dbo.rpt_balance_sheet           
			SET      bs_beginning_inventory =  ISNULL(tmp.net_gallons, 0)
			FROM            rpt_balance_sheet AS bs INNER JOIN
			(SELECT document_key,tx_product_code, SUM(net_gallons) as net_gallons 
			 FROM rpt_tmp_import_data 
			 WHERE transaction_type_code = 'IN_BI' 
			 GROUP BY tx_product_code, document_key)AS tmp ON bs.bs_document_key = tmp.document_key AND bs.bs_product_code = tmp.tx_product_code
			WHERE  bs.bs_document_key = @document_key

			-- Set any product code beginning inventories to 0 if there was no transaction in the raw data and nothing from the prior month

			UPDATE dbo.rpt_balance_sheet           
			SET      bs_beginning_inventory =  0
			FROM            rpt_balance_sheet 
			WHERE        (bs_beginning_inventory IS NULL)  AND (bs_document_key = @document_key)



			--************************Book Adjustments Receipts
			
			UPDATE dbo.rpt_balance_sheet           
			SET      bs_book_receipts =  ISNULL(tmp.net_gallons, 0)
			FROM            rpt_balance_sheet AS bs INNER JOIN
			(SELECT document_key,tx_product_code, SUM(net_gallons) as net_gallons 
			 FROM rpt_tmp_import_data 
			 WHERE transaction_type_code = 'TR_BA' 
			 GROUP BY tx_product_code, document_key)AS tmp ON bs.bs_document_key = tmp.document_key AND bs.bs_product_code = tmp.tx_product_code
			WHERE  bs.bs_document_key = @document_key
			
			UPDATE dbo.rpt_balance_sheet           
			SET      bs_book_receipts =  0
			FROM            rpt_balance_sheet 
			WHERE        (bs_book_receipts IS NULL)  AND (bs_document_key = @document_key)

			
			--************************Book Adjustments Disbursements
				
			UPDATE dbo.rpt_balance_sheet           
			SET      bs_book_disbursements =  ISNULL(tmp.net_gallons, 0)
			FROM            rpt_balance_sheet AS bs INNER JOIN
			(SELECT document_key,tx_product_code, SUM(net_gallons) as net_gallons 
			 FROM rpt_tmp_import_data 
			 WHERE transaction_type_code = 'TD_BA' 
			 GROUP BY tx_product_code, document_key)AS tmp ON bs.bs_document_key = tmp.document_key AND bs.bs_product_code = tmp.tx_product_code
			WHERE  bs.bs_document_key = @document_key
				
			UPDATE dbo.rpt_balance_sheet           
			SET      bs_book_disbursements =  0
			FROM            rpt_balance_sheet 
			WHERE        (bs_book_disbursements IS NULL)  AND (bs_document_key = @document_key)



				--Receipts and Disbursements are done for Carrier and Terminal
			--************************Receipts
				
			UPDATE dbo.rpt_balance_sheet           
			SET      bs_receipts =  ISNULL(tmp.net_gallons, 0)
			FROM            rpt_balance_sheet AS bs INNER JOIN
			(SELECT document_key,tx_product_code, SUM(net_gallons) as net_gallons 
			 FROM rpt_tmp_import_data 
			 WHERE transaction_type_code <> 'TR_BA' AND transaction_type = 'TR'
			 GROUP BY tx_product_code, document_key)AS tmp ON bs.bs_document_key = tmp.document_key AND bs.bs_product_code = tmp.tx_product_code
			WHERE  bs.bs_document_key = @document_key
				
			UPDATE dbo.rpt_balance_sheet           
			SET      bs_receipts =  0
			FROM            rpt_balance_sheet 
			WHERE        (bs_receipts IS NULL)  AND (bs_document_key = @document_key)


			--************************Disbursements
				
			UPDATE dbo.rpt_balance_sheet           
			SET      bs_disbursements =  ISNULL(tmp.net_gallons, 0)
			FROM            rpt_balance_sheet AS bs INNER JOIN
			(SELECT document_key,tx_product_code, SUM(net_gallons) as net_gallons 
			 FROM rpt_tmp_import_data 
			 WHERE transaction_type_code <> 'TD_BA' AND transaction_type = 'TD'
			 GROUP BY tx_product_code, document_key)AS tmp ON bs.bs_document_key = tmp.document_key AND bs.bs_product_code = tmp.tx_product_code
			WHERE  bs.bs_document_key = @document_key
			
			UPDATE dbo.rpt_balance_sheet           
			SET      bs_disbursements =  0
			FROM            rpt_balance_sheet 
			WHERE        (bs_disbursements IS NULL)  AND (bs_document_key = @document_key)


			--************************Calculated GainLoss

			UPDATE    rpt_balance_sheet
			SET       bs_gain_loss = bs_ending_inventory - 
			(bs_beginning_inventory + bs_receipts + bs_book_receipts 
			- bs_book_disbursements - bs_disbursements)
			FROM      dbo.rpt_balance_sheet
			WHERE   (bs_document_key = @document_key)



	END  --Ends the IF clause that checks for numeric



SET NOCOUNT OFF
 -- <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
