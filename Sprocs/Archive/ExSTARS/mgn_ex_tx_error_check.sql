
--/****** Object:  StoredProcedure [dbo].[mgn_ex_tx_error_check]    Script Date: 3/22/2021 8:10:09 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
 
-- -- ===================================================
-- -- Program Copyright Mangan Inc
-- --  Author: Candice Iago 
-- --  Creation Date 12/5/13

-- -- ===================================================
-- -- EXEC mgn_ex_tx_error_check @document_key='32f71106-2405-4d16-8317-bac93dae0334'
-- --


--CREATE OR ALTER PROCEDURE  [dbo].[mgn_ex_tx_error_check]
 declare @document_key uniqueidentifier =   '6599E51C-922F-469A-A376-7863E5DB15C6'
--WITH   ENCRYPTION 
--AS
 
--SET NOCOUNT ON
 
 --******TAKE OUT WHEN REALLY RUNNING THIS FROM THE APP *****************
 UPDATE rpt_tmp_import_data SET document_key = '6599E51C-922F-469A-A376-7863E5DB15C6'


DECLARE @nullguid uniqueidentifier = '00000000-0000-0000-0000-000000000000'
DECLARE @BookAdjustmentCheck bit = 0

DELETE FROM rpt_errors --CLEAR TABLE BEFORE BEGINNING

DECLARE @report_type varchar(50) = (SELECT rpt_type 
									 FROM rpt_reports r INNER JOIN 
										  rpt_documents d ON r._key = d.doc_report_type_key 
									 WHERE d._key = @document_key)



	
UPDATE	dbo.rpt_tmp_import_data
SET transaction_mode = ''
WHERE transaction_mode IS NULL
	
UPDATE	dbo.rpt_tmp_import_data
SET transaction_type = ''
WHERE transaction_type IS NULL	
	
	
UPDATE	dbo.rpt_tmp_import_data
SET product_code = ''
WHERE product_code IS NULL
	
UPDATE	dbo.rpt_tmp_import_data
SET carrier_name = ''
WHERE carrier_name IS NULL
	
UPDATE	dbo.rpt_tmp_import_data
SET carrier_fein = ''
WHERE carrier_fein IS NULL
	
UPDATE	dbo.rpt_tmp_import_data
SET carrier_tx_taxpayer = ''
WHERE carrier_tx_taxpayer IS NULL
	
UPDATE	dbo.rpt_tmp_import_data
SET consignor_name = ''
WHERE consignor_name IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET consignor_fein = ''
WHERE consignor_fein IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET destintion_facility = ''
WHERE destintion_facility IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET position_holder_name = ''
WHERE position_holder_name IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET position_holder_fein = ''
WHERE position_holder_fein IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET position_holder_tx_taxpayer = ''
WHERE position_holder_tx_taxpayer IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET origin_facility = ''
WHERE origin_facility IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET destination_state = ''
WHERE destination_state IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET exchanger_name = ''
WHERE exchanger_name IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET exchanger_fein = ''
WHERE exchanger_fein IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET BOL = ''
WHERE BOL IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET vessel_name = ''
WHERE vessel_name IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET von_imo = ''
WHERE von_imo IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET net_gallons = ''
WHERE net_gallons IS NULL	

UPDATE	dbo.rpt_tmp_import_data
SET gross_gallons = ''
WHERE gross_gallons IS NULL	



	/* CLEANUP transaction_type FIELD */
UPDATE dbo.rpt_tmp_import_data
SET dbo.rpt_tmp_import_data.transaction_type = UPPER(dbo.rpt_tmp_import_data.transaction_type)


	/* CLEANUP product_code FIELD */
UPDATE dbo.rpt_tmp_import_data
SET dbo.rpt_tmp_import_data.product_code = UPPER(dbo.rpt_tmp_import_data.product_code)

	/* CLEANUP transaction_mode FIELD */
UPDATE dbo.rpt_tmp_import_data
SET dbo.rpt_tmp_import_data.transaction_mode = UPPER(dbo.rpt_tmp_import_data.transaction_mode)

	/* CLEANUP carrier_nameFIELD */
UPDATE dbo.rpt_tmp_import_data
SET dbo.rpt_tmp_import_data.carrier_name = UPPER(dbo.rpt_tmp_import_data.carrier_name)

	/* CLEANUP consignor_name FIELD */
UPDATE dbo.rpt_tmp_import_data
SET dbo.rpt_tmp_import_data.consignor_name = UPPER(dbo.rpt_tmp_import_data.consignor_name)

	/* CLEANUP destintion_facility FIELD */
UPDATE dbo.rpt_tmp_import_data
SET dbo.rpt_tmp_import_data.destintion_facility = UPPER(dbo.rpt_tmp_import_data.destintion_facility)

	/* CLEANUP position_holder_name FIELD */
UPDATE dbo.rpt_tmp_import_data
SET dbo.rpt_tmp_import_data.position_holder_name = UPPER(dbo.rpt_tmp_import_data.position_holder_name)

	/* CLEANUP origin_facility FIELD */
UPDATE dbo.rpt_tmp_import_data
SET dbo.rpt_tmp_import_data.origin_facility = UPPER(dbo.rpt_tmp_import_data.origin_facility)

	/* CLEANUP destination_state FIELD */
UPDATE dbo.rpt_tmp_import_data
SET dbo.rpt_tmp_import_data.destination_state = UPPER(dbo.rpt_tmp_import_data.destination_state)

	/* CLEANUP ExchName FIELD */
UPDATE dbo.rpt_tmp_import_data
SET dbo.rpt_tmp_import_data.exchanger_name = UPPER(dbo.rpt_tmp_import_data.exchanger_name)

	/* CLEANUP vessel_name FIELD */
UPDATE dbo.rpt_tmp_import_data
SET dbo.rpt_tmp_import_data.vessel_name = UPPER(dbo.rpt_tmp_import_data.vessel_name)

	/* CLEANUP von_imo FIELD */
UPDATE dbo.rpt_tmp_import_data
SET dbo.rpt_tmp_import_data.von_imo = UPPER(dbo.rpt_tmp_import_data.von_imo)

	/* CLEANUP DOC DATE */
UPDATE rpt_tmp_import_data
SET document_date = CONVERT(varchar, document_date, 101)
		
	
	-- remove any special characters

	/* CLEANUP transaction_type FIELD */
	UPDATE rpt_tmp_import_data
	SET transaction_type = dbo.fn_replace_non_alpha_numeric(transaction_type)


	/* CLEANUP transaction_mode FIELD */
	UPDATE rpt_tmp_import_data
	SET transaction_mode = dbo.fn_replace_non_alpha_numeric(transaction_mode)
	/* NULL ALL EMPTY STRING */





--TEXAS SPECIFIC CHECKS*****************************************************************************************************************************

-- change all federal product codes to the state defined product code

--******************************product_code- if the length is less than 3, then pad the front of the code with 0's to make it 3 

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.product_code = RIGHT(('00' + rpt_tmp_import_data.product_code), 3)
	WHERE (LEN(rpt_tmp_import_data.product_code) < 3 AND product_code<>'')

	UPDATE tmp
	SET tx_product_code = p.prd_tx_tor
	FROM rpt_tmp_import_data tmp INNER JOIN 
		 product_irs_codes p ON tmp.product_code = p.prd_irs_code

--transaction_mode- if the transaction_mode is only one letter, then add a space onto the end of it.

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.transaction_mode = LEFT(dbo.rpt_tmp_import_data.transaction_mode + ' ' ,2)

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.transaction_mode = 'J_'
	WHERE ((dbo.rpt_tmp_import_data.transaction_mode = 'J ') OR (dbo.rpt_tmp_import_data.transaction_mode = 'EJ') OR (dbo.rpt_tmp_import_data.transaction_mode = 'IJ'))

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.transaction_mode = 'R_'
	WHERE ((dbo.rpt_tmp_import_data.transaction_mode = 'R ') OR (dbo.rpt_tmp_import_data.transaction_mode = 'ER') OR (dbo.rpt_tmp_import_data.transaction_mode = 'IR'))	
	
	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.transaction_mode = 'B_'
	WHERE ((dbo.rpt_tmp_import_data.transaction_mode = 'B ') OR (dbo.rpt_tmp_import_data.transaction_mode = 'EB') OR (dbo.rpt_tmp_import_data.transaction_mode = 'IB'))	
	
	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.transaction_mode = 'S_'
	WHERE ((dbo.rpt_tmp_import_data.transaction_mode = 'S ') OR (dbo.rpt_tmp_import_data.transaction_mode = 'ES') OR (dbo.rpt_tmp_import_data.transaction_mode = 'IS'))
		
	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.transaction_mode = 'PL'
	WHERE ((dbo.rpt_tmp_import_data.transaction_mode = 'IP') OR (dbo.rpt_tmp_import_data.transaction_mode = 'EP'))
	
	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.transaction_mode = 'ST'
	WHERE ((dbo.rpt_tmp_import_data.transaction_mode = 'AH') OR (dbo.rpt_tmp_import_data.transaction_mode = 'AJ') OR (dbo.rpt_tmp_import_data.transaction_mode = 'CE') 
	OR (dbo.rpt_tmp_import_data.transaction_mode = 'RF') OR (dbo.rpt_tmp_import_data.transaction_mode = 'RR') OR (dbo.rpt_tmp_import_data.transaction_mode = 'RS'))


-- Update the ship to to TX if empty

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.destination_state = 'TX'
	WHERE ((dbo.rpt_tmp_import_data.transaction_type = 'TD') AND (dbo.rpt_tmp_import_data.destination_state = ''))



--*********************Check transaction_mode, transaction_type, Fill in transaction_typeID ****************************************************************************


--transaction_type- Make sure that the transaction_type is valid and exists in the rpt_transaction_types table

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT 12, 'transaction_type',rpt_tmp_import_data.transaction_type, rpt_tmp_import_data._key  --, rpt_transaction_types.transaction_type, RawTrans.transaction_type
	FROM     rpt_tmp_import_data
	WHERE (rpt_tmp_import_data.transaction_type NOT IN (SELECT rpt_transaction_types.tt_transaction_type FROM dbo.rpt_transaction_types WHERE tt_report_type = @report_type )) OR rpt_tmp_import_data.transaction_type = ''

----transaction_mode- if the transaction_mode is only one letter, then add a space onto the end of it.

--	UPDATE dbo.rpt_tmp_import_data
--	SET dbo.rpt_tmp_import_data.transaction_mode = LEFT(dbo.rpt_tmp_import_data.transaction_mode + ' ' ,2)



--transaction_mode- Make sure it is valid and exists in the rpt_transaction_types table

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT 13, 'transaction_mode', rpt_tmp_import_data.transaction_mode, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (rpt_tmp_import_data.transaction_mode NOT IN (SELECT tt_transaction_mode FROM rpt_transaction_types WHERE tt_report_type = @report_type )) OR rpt_tmp_import_data.transaction_mode = ''


--^^^^^^^^^^^^^^^^    transaction_type ID  ^^^^^^^^^^^^^^^^^^^


--Set the transaction_typeID

	UPDATE tmp
	SET transaction_type_code = tt.tt_tranaction_type_code
	FROM dbo.rpt_tmp_import_data tmp INNER JOIN 
		 rpt_transaction_types tt ON tmp.transaction_mode = tt.tt_transaction_mode 
		 AND tmp.transaction_type = tt.tt_transaction_type
		 AND tt.tt_report_type = @report_type



--transaction_typeID- compare to the table rpt_transaction_types table, if the combo does not exist, throw error	


	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  37, 'transaction_mode and/or transaction_type' ,  (rpt_tmp_import_data.transaction_mode + '-' + rpt_tmp_import_data.transaction_type) ,rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE  transaction_type_code IS NULL

--*********************   Fill in the default values 	****************************************************************************


--carrier_name- if the tt_allow_carrier_override is true for the transaction_type then make the carrier_name equal to the filing entity name   
--carrier_fein- if the tt_allow_carrier_override is true for the transaction_typeID then make the carrier_fein equal to the FilingEntityFEIN

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT    	21, 'Carrier Name and FEIN', rpt_tmp_import_data.carrier_name + '-' + rpt_tmp_import_data.carrier_fein, rpt_tmp_import_data._key
	FROM        	rpt_tmp_import_data INNER JOIN
					rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE    	(rpt_transaction_types.tt_allow_carrier_override = 1) AND rpt_transaction_types.tt_report_type <> 'CCR' AND (rpt_transaction_types.tt_report_type = @report_type)

	UPDATE   	rpt_tmp_import_data
	SET            	carrier_name = rpt_report_entity.re_name, carrier_fein = rpt_report_entity.re_fein, carrier_tx_taxpayer = re_tx_taxpayer_number
	FROM        	rpt_documents INNER JOIN
                         	rpt_report_entity ON rpt_documents.doc_report_entity_key = rpt_report_entity._key INNER JOIN
                         	rpt_tmp_import_data INNER JOIN
                         	rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code ON rpt_documents._key = rpt_tmp_import_data.document_key
	WHERE    	(rpt_transaction_types.tt_allow_carrier_override = 1) AND (rpt_transaction_types.tt_report_type = @report_type)



--BOL- If the transaction_mode is CE,AH,AJ, or RR make the BOL = 'Summary"

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  29, 'BOL' ,rpt_tmp_import_data.BOL, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE ((dbo.rpt_tmp_import_data.transaction_mode = 'CE')
		OR (dbo.rpt_tmp_import_data.transaction_mode = 'AH')
		OR (dbo.rpt_tmp_import_data.transaction_mode = 'AJ')
		OR (dbo.rpt_tmp_import_data.transaction_mode = 'RR'))
 
	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.BOL = 'Summary'
	WHERE ((dbo.rpt_tmp_import_data.transaction_mode = 'CE')
		OR (dbo.rpt_tmp_import_data.transaction_mode = 'AH')
		OR (dbo.rpt_tmp_import_data.transaction_mode = 'AJ')
		OR (dbo.rpt_tmp_import_data.transaction_mode = 'RR'))



--BOL- If the transaction_mode is BA make the BOL = 'RECLASS"

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  30, 'BOL' ,rpt_tmp_import_data.BOL, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (dbo.rpt_tmp_import_data.transaction_mode = 'BA')
 
	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.BOL = 'RECLASS'
	WHERE (dbo.rpt_tmp_import_data.transaction_mode = 'BA')


--document_date- If the transaction_type is 'IN' make the document_date the doc_irs_end_date

	UPDATE       rpt_tmp_import_data
	SET                document_date = Convert(varchar, rpt_documents.doc_irs_end_date, 101)
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_documents ON rpt_tmp_import_data.document_key = rpt_documents._key
	WHERE        (rpt_tmp_import_data.transaction_type = 'IN')


--document_date- If the transaction_mode is 'BA' make the document_date the doc_irs_end_date

	UPDATE       rpt_tmp_import_data
	SET           document_date = Convert(varchar,rpt_documents.doc_irs_end_date,101)
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_documents ON rpt_tmp_import_data.document_key = rpt_documents._key
	WHERE        (rpt_tmp_import_data.transaction_mode = 'BA')


--origin_facility- If transaction_type is 'TD' then set it to the FilingEntityFacility	

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT TOP(1)  11, 'origin_facility','', @nullguid 
	FROM         rpt_tmp_import_data INNER JOIN
					rpt_documents ON rpt_tmp_import_data.document_key = rpt_documents._key INNER JOIN
					rpt_report_entity ON rpt_documents.doc_report_entity_key = rpt_report_entity._key 
	WHERE		 ((rpt_tmp_import_data.origin_facility <> rpt_report_entity.re_tcn) AND (rpt_tmp_import_data.transaction_type = 'TD'))

	UPDATE       rpt_tmp_import_data
	SET          origin_facility = rpt_report_entity.re_tcn
	FROM         rpt_tmp_import_data INNER JOIN
					rpt_documents ON rpt_tmp_import_data.document_key = rpt_documents._key INNER JOIN
					rpt_report_entity ON rpt_documents.doc_report_entity_key = rpt_report_entity._key 
	WHERE		 ((rpt_tmp_import_data.origin_facility <> rpt_report_entity.re_tcn) AND (rpt_tmp_import_data.transaction_type = 'TD'))




--destintion_facility- If transaction_type is 'TR' then set it to the FilingEntityFacility

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT TOP(1)  11, 'destintion_facility' , '', @nullguid
	FROM         rpt_tmp_import_data INNER JOIN
					rpt_documents ON rpt_tmp_import_data.document_key = rpt_documents._key INNER JOIN
					rpt_report_entity ON rpt_documents.doc_report_entity_key = rpt_report_entity._key 
	WHERE		 ((rpt_tmp_import_data.destintion_facility <> rpt_report_entity.re_tcn) AND (rpt_tmp_import_data.transaction_type = 'TR'))

	UPDATE       rpt_tmp_import_data
	SET          destintion_facility = rpt_report_entity.re_tcn
	FROM         rpt_tmp_import_data INNER JOIN
					rpt_documents ON rpt_tmp_import_data.document_key = rpt_documents._key INNER JOIN
					rpt_report_entity ON rpt_documents.doc_report_entity_key = rpt_report_entity._key 
	WHERE		 ((rpt_tmp_import_data.destintion_facility <> rpt_report_entity.re_tcn) AND (rpt_tmp_import_data.transaction_type = 'TR'))



--*********************REMOVE UNNECESSARY DATA****************************************************************************
--*********************************************************************************************************************************************



--carrier_name and carrier_fein- If tt_require_carrier is False on the rpt_tmp_import_data.transaction_type_code, then make both Fields null

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  1, 'Carrier Data' , rpt_tmp_import_data.carrier_name , rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_transaction_types.tt_require_carrier = 0) 
					AND (rpt_transaction_types.tt_report_type = @report_type)
					AND ((rpt_tmp_import_data.carrier_name <> '') OR (rpt_tmp_import_data.carrier_fein <> '') OR (rpt_tmp_import_data.carrier_tx_taxpayer <> ''))

	UPDATE       rpt_tmp_import_data
	SET                carrier_name = '', carrier_fein = '', carrier_tx_taxpayer = ''
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_transaction_types.tt_require_carrier = 0) 
					AND (rpt_transaction_types.tt_report_type = @report_type)
					AND ((rpt_tmp_import_data.carrier_name <> '') OR (rpt_tmp_import_data.carrier_fein <> '')OR (rpt_tmp_import_data.carrier_tx_taxpayer <> ''))



--consignor_name and consignor_fein- If tt_require_consignor is False on the rpt_tmp_import_data.transaction_type_code, then make both Fields null 

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  1, 'Consignor Data' , rpt_tmp_import_data.consignor_name, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE         (rpt_transaction_types.tt_require_consignor = 0) AND (rpt_transaction_types.tt_report_type = @report_type) AND
					((rpt_tmp_import_data.consignor_fein <> '') OR (rpt_tmp_import_data.consignor_name <> '') )

	UPDATE       rpt_tmp_import_data
	SET                consignor_name = '', consignor_fein = ''
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_transaction_types.tt_require_consignor = 0)  AND (rpt_transaction_types.tt_report_type = @report_type) AND 
								((rpt_tmp_import_data.consignor_fein <> '') OR (rpt_tmp_import_data.consignor_name <> '') )



--position_holder_name and position_holder_fein- If tt_require_position_holder is False on the rpt_tmp_import_data.transaction_type_code, then make both Fields null 

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  1, 'PositionHolder Data' , rpt_tmp_import_data.position_holder_name, rpt_tmp_import_data._key
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE      (rpt_transaction_types.tt_require_position_holder = 0) 
					AND (rpt_transaction_types.tt_report_type = @report_type)
					AND ( (rpt_tmp_import_data.position_holder_name <> '') OR (rpt_tmp_import_data.position_holder_fein <> '')  OR (rpt_tmp_import_data.position_holder_tx_taxpayer <> '') ) 


	UPDATE       rpt_tmp_import_data
	SET                position_holder_name = '', position_holder_fein = '', position_holder_tx_taxpayer = ''
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE       (rpt_transaction_types.tt_require_position_holder = 0) 
					AND (rpt_transaction_types.tt_report_type = @report_type)
					AND ((rpt_tmp_import_data.position_holder_name <> '') OR (rpt_tmp_import_data.position_holder_fein <> '')  OR (rpt_tmp_import_data.position_holder_tx_taxpayer <> '')) 




--origin_facility- If Requireorigin_facility is False on the rpt_tmp_import_data.transaction_type_code, then make field null 


	UPDATE       rpt_tmp_import_data
	SET                origin_facility = ''
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_origin = 0) 
					AND (rpt_transaction_types.tt_report_type = @report_type)
					AND ((rpt_tmp_import_data.origin_facility <> '') )	
					


--Destination- If RequireDestinationOption is False on the rpt_tmp_import_data.transaction_type_code, then make field null 



	UPDATE       rpt_tmp_import_data
	SET                destintion_facility = ''
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_destination = 0) 
					AND (rpt_transaction_types.tt_report_type = @report_type)
					AND ((rpt_tmp_import_data.destintion_facility <> '') )	


--Destination- If RequireDestinationOption is False on the rpt_tmp_import_data.transaction_type_code, then make field null 



	UPDATE       rpt_tmp_import_data
	SET                destination_state = ''
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_ship_to = 0) 
					AND (rpt_transaction_types.tt_report_type = @report_type)
					AND ((rpt_tmp_import_data.destination_state <> '') )	




--ExchName and ExchFEIN- If tt_require_exchanger is False on the rpt_tmp_import_data.transaction_type_code, then make both Fields null 

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  1, 'Exchanger Data' , rpt_tmp_import_data.exchanger_name, rpt_tmp_import_data._key
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE         (rpt_transaction_types.tt_require_exchanger = 0) AND (rpt_transaction_types.tt_report_type = @report_type) AND
								((rpt_tmp_import_data.exchanger_fein <> '') OR (rpt_tmp_import_data.exchanger_name <> '') ) 


	UPDATE       rpt_tmp_import_data
	SET                exchanger_name = '', exchanger_fein = ''
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_exchanger = 0) AND (rpt_transaction_types.tt_report_type = @report_type) AND
								((rpt_tmp_import_data.exchanger_fein <> '') OR (rpt_tmp_import_data.exchanger_name <> '') ) 



--BOL- If RequireBOL is False on the rpt_tmp_import_data.transaction_type_code, then make field null 

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  1, 'BOL' ,rpt_tmp_import_data.BOL, rpt_tmp_import_data._key
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_bol = 0) AND (rpt_tmp_import_data.BOL <> '')  AND (rpt_transaction_types.tt_report_type = @report_type)


	UPDATE       rpt_tmp_import_data
	SET                BOL = ''
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_bol = 0) AND (rpt_tmp_import_data.BOL <> '')  AND (rpt_transaction_types.tt_report_type = @report_type)


--document_date- If RequireDocDate is False on the rpt_tmp_import_data.transaction_type_code, then make field null 

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  1, 'document_date' ,rpt_tmp_import_data.document_date, rpt_tmp_import_data._key
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_document_date = 0) AND (rpt_tmp_import_data.document_date <> '')  AND (rpt_transaction_types.tt_report_type = @report_type)


	UPDATE       rpt_tmp_import_data
	SET                document_date = ''
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_document_date = 0) AND (rpt_tmp_import_data.document_date <> '') AND (rpt_transaction_types.tt_report_type = @report_type) 


--vessel_name and von_imo- If tt_require_vessel is False on the rpt_tmp_import_data.transaction_type_code, then make both Fields null 

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  1, 'Vessel Name and/or von_imo' , rpt_tmp_import_data.vessel_name + '-' + rpt_tmp_import_data.von_imo, rpt_tmp_import_data._key
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_vessel = 0) AND (rpt_transaction_types.tt_report_type = @report_type) AND 
								((rpt_tmp_import_data.von_imo <> '') OR (rpt_tmp_import_data.vessel_name <> ''))

	UPDATE       rpt_tmp_import_data
	SET                vessel_name = '', von_imo = ''
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_vessel = 0) AND (rpt_transaction_types.tt_report_type = @report_type) AND 
								((rpt_tmp_import_data.von_imo <> '') OR (rpt_tmp_import_data.vessel_name <> ''))



--Requirenet_gallons- If RequireRequirenet_gallons is False on the rpt_tmp_import_data.transaction_type_code, then make field null 

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  1, 'net_gallons' ,rpt_tmp_import_data.net_gallons, rpt_tmp_import_data._key
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_net_gallon = 0) AND (rpt_tmp_import_data.net_gallons <> '')  AND (rpt_transaction_types.tt_report_type = @report_type)


	UPDATE       rpt_tmp_import_data
	SET                net_gallons = ''
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_net_gallon = 0) AND (rpt_tmp_import_data.net_gallons <> '') AND (rpt_transaction_types.tt_report_type = @report_type)



--Requiregross_gallons- If RequireRequiregross_gallons is False on the rpt_tmp_import_data.transaction_type_code, then make field null 

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  1, 'gross_gallons' ,rpt_tmp_import_data.gross_gallons, rpt_tmp_import_data._key
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_gross_gallon = 0) AND (rpt_tmp_import_data.gross_gallons <> '') AND (rpt_transaction_types.tt_report_type = @report_type) 


	UPDATE       rpt_tmp_import_data
	SET                gross_gallons = ''
	FROM            rpt_transaction_types INNER JOIN
								rpt_tmp_import_data ON rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code
	WHERE        (rpt_transaction_types.tt_require_gross_gallon = 0) AND (rpt_tmp_import_data.gross_gallons <> '')  AND (rpt_transaction_types.tt_report_type = @report_type)	

	
--*********************CHECK FOR MISSING DATA****************************************************************************
--*********************************************************************************************************************************************


--carrier_name and carrier_fein- If tt_require_carrier is true on the rpt_tmp_import_data.transaction_type_code, and field = '', throw error



	-- Check for Entity Name 
		INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
		SELECT  2, 'carrier_name' ,rpt_tmp_import_data.carrier_name, rpt_tmp_import_data._key
		FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
		WHERE        (rpt_transaction_types.tt_require_carrier = 1) AND (rpt_tmp_import_data.carrier_name = '') AND (rpt_transaction_types.tt_report_type = @report_type)

	-- Check for FEIN or ID missing
		INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
		SELECT  2, 'Carrier FEIN/ID' , '' , rpt_tmp_import_data._key
		FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
		WHERE        (rpt_transaction_types.tt_require_carrier = 1) AND ((rpt_tmp_import_data.carrier_fein = '') )AND (rpt_tmp_import_data.carrier_tx_taxpayer = '') AND (rpt_transaction_types.tt_report_type = @report_type)


--consignor_name and consignor_fein- If tt_require_consignor is true on the rpt_tmp_import_data.transaction_type_code, and field = '', throw error


	-- Check for Entity Name 
		INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
		SELECT  2, 'consignor_name' ,rpt_tmp_import_data.consignor_name, rpt_tmp_import_data._key
		FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
		WHERE        (rpt_transaction_types.tt_require_consignor = 1) AND (rpt_tmp_import_data.consignor_name = '') AND (rpt_transaction_types.tt_report_type = @report_type)

	-- Check for FEIN or ID missing
		INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
		SELECT  2, 'Consignor FEIN/ID' , '' , rpt_tmp_import_data._key
		FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
		WHERE        (rpt_transaction_types.tt_require_consignor = 1) AND ((rpt_tmp_import_data.consignor_fein = '') ) AND (rpt_transaction_types.tt_report_type = @report_type)

--position_holder_name and position_holder_fein- If tt_require_position_holder is true on the rpt_tmp_import_data.transaction_type_code, and field = '', throw error


	-- Check for Entity Name 
		INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
		SELECT  2, 'position_holder_name' ,rpt_tmp_import_data.position_holder_name, rpt_tmp_import_data._key
		FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
		WHERE        (rpt_transaction_types.tt_require_position_holder = 1) AND (rpt_tmp_import_data.position_holder_name = '')  AND (rpt_transaction_types.tt_report_type = @report_type)

	-- Check for FEIN or ID missing
		INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
		SELECT  2, 'PositionHolder FEIN/ID' , '' , rpt_tmp_import_data._key
		FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
		WHERE        (rpt_transaction_types.tt_require_position_holder = 1) AND ((rpt_tmp_import_data.position_holder_fein = '') ) AND (rpt_tmp_import_data.position_holder_tx_taxpayer = '') AND (rpt_transaction_types.tt_report_type = @report_type)


--exchanger_name and exchanger_fein- If tt_require_exchanger is true on the rpt_tmp_import_data.transaction_type_code, and field = '', throw error



	-- Check for Entity Name 
		INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
		SELECT  2, 'exchanger_name' ,rpt_tmp_import_data.exchanger_name, rpt_tmp_import_data._key
		FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
		WHERE        (rpt_transaction_types.tt_require_exchanger = 1) AND (rpt_tmp_import_data.exchanger_name = '') AND (rpt_transaction_types.tt_report_type = @report_type)

	-- Check for FEIN or ID missing
		INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
		SELECT  2, 'Exchanger FEIN/ID' , '' , rpt_tmp_import_data._key
		FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
		WHERE        (rpt_transaction_types.tt_require_exchanger = 1) AND ((rpt_tmp_import_data.exchanger_fein = '')) AND (rpt_transaction_types.tt_report_type = @report_type)


--BOL- If RequireBOL is true on the rpt_tmp_import_data.transaction_type_code, and field = '', throw error 

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  2, 'BOL' , rpt_tmp_import_data.BOL, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_transaction_types.tt_require_bol = 1) AND (rpt_tmp_import_data.BOL = '')  AND (rpt_transaction_types.tt_report_type = @report_type)


--document_date- If RequireDocDate is true on the rpt_tmp_import_data.transaction_type_code, and field = '', throw error 

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  2, 'document_date' , rpt_tmp_import_data.document_date, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_transaction_types.tt_require_document_date = 1) AND (rpt_tmp_import_data.document_date = '')  AND (rpt_transaction_types.tt_report_type = @report_type)



--vessel_name and von_imo- If tt_require_vessel is true on the rpt_tmp_import_data.transaction_type_code, and field = '', throw error  

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  2, 'vessel_name' , rpt_tmp_import_data.vessel_name, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_transaction_types.tt_require_vessel = 1) AND (rpt_tmp_import_data.vessel_name = '')  AND (rpt_transaction_types.tt_report_type = @report_type)

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  2, 'von_imo' , rpt_tmp_import_data.von_imo, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_transaction_types.tt_require_vessel = 1) AND (rpt_tmp_import_data.von_imo = '') AND (rpt_transaction_types.tt_report_type = @report_type)


--Requirenet_gallons- If RequireRequirenet_gallons is true on the rpt_tmp_import_data.transaction_type_code, and field = '', throw error 

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  2, 'net_gallons' , rpt_tmp_import_data.net_gallons, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_transaction_types.tt_require_net_gallon = 1) AND (rpt_tmp_import_data.net_gallons = '') AND (rpt_transaction_types.tt_report_type = @report_type) 


--Requiregross_gallons- If RequireRequiregross_gallons is true on the rpt_tmp_import_data.transaction_type_code, and field = '', throw error 

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  2, 'gross_gallons' , rpt_tmp_import_data.gross_gallons, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_transaction_types.tt_require_gross_gallon = 1) AND (rpt_tmp_import_data.gross_gallons = '') AND (rpt_transaction_types.tt_report_type = @report_type) 



----destintion_facility- If Requiredestintion_facility is true on the rpt_tmp_import_data.transaction_type_code, and field = '', throw error 

		-- throw error if it is required and all fields are empty
			INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
			SELECT  2, 'Destination Facility or AltType' , '', rpt_tmp_import_data._key
			FROM            rpt_tmp_import_data INNER JOIN
										rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
			WHERE        (rpt_transaction_types.tt_require_destination = 1) AND (rpt_tmp_import_data.destintion_facility = '') AND (destination_state = '')   AND (rpt_transaction_types.tt_report_type = @report_type)
						
			--WHERE        (rpt_transaction_types.RequireDestinationOption = 1) AND (rpt_tmp_import_data.destintion_facility = '')  AND (DestinationAddress = '') AND (DestinationAltID = '') AND (DestinationAltIDType = '') 
			--				AND (DestinationCity = '') AND (DestinationCountry =  '') AND (DestinationCounty = '') AND (DestinationZIP = '') AND (destination_state = '')


----Origin Facility- If tt_require_origin is true on the rpt_tmp_import_data.transaction_type_code check for rpt_errors 

		-- throw error if it is required and all fields are empty
			INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
			SELECT  2, 'Origin' , '', rpt_tmp_import_data._key
			FROM            rpt_tmp_import_data INNER JOIN
										rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
			WHERE        (rpt_transaction_types.tt_require_origin = 1) AND (rpt_tmp_import_data.origin_facility = '') AND (rpt_transaction_types.tt_report_type = @report_type)
						
			--WHERE        (rpt_transaction_types.tt_require_origin = 1) AND (rpt_tmp_import_data.origin_facility = '')  AND (OriginAddress = '') AND (OriginAltID = '') AND (OriginAltIDType = '') 
			--				AND (OriginCity = '') AND (OriginCountry =  '') AND (OriginCounty = '') AND (OriginZIP = '') AND (OriginState = '')





--*********************REMOVE SPECIAL CHARACTERS FROM FIELDS CURSORS****************************************************************************
--*********************************************************************************************************************************************


--*********************product_code Remove Special Characters

	UPDATE dbo.rpt_tmp_import_data
	SET product_code = dbo.fn_replace_non_alpha_numeric(product_code)
	WHERE product_code not like replicate('[a-z|0-9]',len(product_code))


--*********************carrier_fein Remove Special Characters

	UPDATE dbo.rpt_tmp_import_data
	SET carrier_fein = dbo.fn_replace_non_alpha_numeric(carrier_fein)
	WHERE carrier_fein not like replicate('[a-z|0-9]',len(carrier_fein))


--*********************carrier_tx_taxpayer Remove Special Characters

	UPDATE dbo.rpt_tmp_import_data
	SET carrier_tx_taxpayer = dbo.fn_replace_non_alpha_numeric(carrier_tx_taxpayer)
	WHERE carrier_tx_taxpayer not like replicate('[a-z|0-9]',len(carrier_tx_taxpayer))


--*********************consignor_fein Remove Special Characters

	UPDATE dbo.rpt_tmp_import_data
	SET [dbo].[rpt_tmp_import_data].[consignor_fein] = dbo.fn_replace_non_alpha_numeric(consignor_fein)
	WHERE consignor_fein not like replicate('[a-z|0-9]',len(consignor_fein))



--*********************destintion_facility Remove Special Characters

	UPDATE dbo.rpt_tmp_import_data
	SET [dbo].[rpt_tmp_import_data].[destintion_facility] =  dbo.fn_replace_non_alpha_numeric(destintion_facility)
	WHERE destintion_facility not like replicate('[a-z|0-9]',len(destintion_facility))


--*********************position_holder_fein Remove Special Characters

	UPDATE dbo.rpt_tmp_import_data
	SET [dbo].[rpt_tmp_import_data].position_holder_fein =  dbo.fn_replace_non_alpha_numeric(position_holder_fein)
	WHERE position_holder_fein not like replicate('[a-z|0-9]',len(position_holder_fein))


--*********************position_holder_tx_taxpayer Remove Special Characters

	UPDATE dbo.rpt_tmp_import_data
	SET [dbo].[rpt_tmp_import_data].position_holder_tx_taxpayer =  dbo.fn_replace_non_alpha_numeric(position_holder_tx_taxpayer)
	WHERE position_holder_tx_taxpayer not like replicate('[a-z|0-9]',len(position_holder_tx_taxpayer))



--*********************origin_facility Remove Special Characters


	UPDATE dbo.rpt_tmp_import_data
	SET [dbo].[rpt_tmp_import_data].[origin_facility] = dbo.fn_replace_non_alpha_numeric(origin_facility)
	WHERE origin_facility not like replicate('[a-z|0-9]',len(origin_facility))



--*********************destination_state Remove Special Characters

	UPDATE dbo.rpt_tmp_import_data
	SET [dbo].[rpt_tmp_import_data].[destination_state] = dbo.fn_replace_non_alpha_numeric(destination_state)
	WHERE destination_state not like replicate('[a-z|0-9]',len(destination_state))


--*********************ExchFEIN Remove Special Characters
 
	UPDATE dbo.rpt_tmp_import_data
	SET [dbo].[rpt_tmp_import_data].exchanger_fein = dbo.fn_replace_non_alpha_numeric(exchanger_fein)
	WHERE exchanger_fein not like replicate('[a-z|0-9]',len(exchanger_fein))


--********************* von_imo Remove Special Characters

	UPDATE dbo.rpt_tmp_import_data
	SET [dbo].[rpt_tmp_import_data].[von_imo] = dbo.fn_replace_non_alpha_numeric(von_imo)
	WHERE von_imo not like replicate('[a-z|0-9]',len(von_imo))



--*********************   Check for Numeric Gallons  ****************************************************************************
--*********************************************************************************************************************************************



--******************************Numeric net_gallons
	PRINT 'Check ISNUMERIC net_gallons'
	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  4, 'net_gallons' , rpt_tmp_import_data.net_gallons, rpt_tmp_import_data._key --, rpt_tmp_import_data.DocumentRow
	FROM     rpt_tmp_import_data 
	WHERE ((ISNUMERIC(dbo.rpt_tmp_import_data.net_gallons) = 0) AND (dbo.rpt_tmp_import_data.net_gallons <> '')) 
	AND ((rpt_tmp_import_data.transaction_type <> 'IN'))
 

--******************************Numeric gross_gallons
	PRINT 'Check ISNUMERIC gross_gallons'
	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  4, 'gross_gallons' , rpt_tmp_import_data.gross_gallons, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE ((ISNUMERIC(dbo.rpt_tmp_import_data.gross_gallons) = 0) AND (dbo.rpt_tmp_import_data.gross_gallons <> '') 
	AND rpt_tmp_import_data.transaction_type <> 'IN')
 


--******************************Numeric carrier_fein
	PRINT 'Check ISNUMERIC carrier_fein'
	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  4, 'carrier_fein' , rpt_tmp_import_data.carrier_fein, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE ((ISNUMERIC(dbo.rpt_tmp_import_data.carrier_fein) = 0) AND (dbo.rpt_tmp_import_data.carrier_fein <> ''))
	AND rpt_tmp_import_data.transaction_type <> 'IN'
  

--******************************Numeric consignor_fein
	PRINT 'Check ISNUMERIC consignor_fein'
	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  4, 'consignor_fein' , rpt_tmp_import_data.consignor_fein, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE ((ISNUMERIC(dbo.rpt_tmp_import_data.consignor_fein) = 0) AND (dbo.rpt_tmp_import_data.consignor_fein <> ''))
	AND rpt_tmp_import_data.transaction_type <> 'IN'
 

--******************************Numeric position_holder_fein
	PRINT 'Check ISNUMERIC position_holder_fein'
	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  4, 'position_holder_fein' , rpt_tmp_import_data.position_holder_fein, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE ((ISNUMERIC(dbo.rpt_tmp_import_data.position_holder_fein) = 0) AND (dbo.rpt_tmp_import_data.position_holder_fein <> ''))
	AND rpt_tmp_import_data.transaction_type <> 'IN'
 

--******************************Numeric ExchFEIN
	PRINT 'Check ISNUMERIC ExchFEIN'
	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  4, 'ExchFEIN' , rpt_tmp_import_data.exchanger_fein, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE ((ISNUMERIC(dbo.rpt_tmp_import_data.exchanger_fein) = 0) AND (dbo.rpt_tmp_import_data.exchanger_fein <> '')	
	AND rpt_tmp_import_data.transaction_type <> 'IN')
 

 
--*********************   Round Gallons  ****************************************************************************
--*******************************************************************************************************************




--******************************Round net_gallons
	PRINT 'ROUND net_gallons'
 	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.net_gallons = REPLACE(STR(ROUND(CONVERT(float, dbo.rpt_tmp_import_data.net_gallons) , 0), 20, 0), ' ', '')
	WHERE (ISNUMERIC(dbo.rpt_tmp_import_data.net_gallons) = 1)


--******************************Round gross_gallons
	PRINT 'ROUND gross_gallons'
 	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.gross_gallons = REPLACE(STR(ROUND(CONVERT(float, dbo.rpt_tmp_import_data.gross_gallons) , 0), 20, 0), ' ', '')
	WHERE (ISNUMERIC(dbo.rpt_tmp_import_data.gross_gallons) = 1)



--********************  Check length of Name Fields to make sure 35 or less	*****************************************
--*******************************************************************************************************************


--******************************carrier_name

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  6, 'carrier_name' ,rpt_tmp_import_data.carrier_name, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.carrier_name) > 35 )

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.carrier_name = LEFT(rpt_tmp_import_data.carrier_name, 35)
	WHERE (LEN(rpt_tmp_import_data.carrier_name) > 35 )
	

--******************************consignor_name

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  6, 'consignor_name' , rpt_tmp_import_data.consignor_name, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.consignor_name) > 35 )

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.consignor_name = LEFT(rpt_tmp_import_data.consignor_name, 35)
	WHERE (LEN(rpt_tmp_import_data.consignor_name) > 35 )


--******************************position_holder_name

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  6, 'position_holder_name' ,rpt_tmp_import_data.position_holder_name, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.position_holder_name) > 35 )

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.position_holder_name = LEFT(rpt_tmp_import_data.position_holder_name, 35)
	WHERE (LEN(rpt_tmp_import_data.position_holder_name) > 35 )


--******************************ExchName

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  6, 'ExchName' , rpt_tmp_import_data.exchanger_name, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.exchanger_name) > 35 )

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.exchanger_name = LEFT(rpt_tmp_import_data.exchanger_name, 35)
	WHERE (LEN(rpt_tmp_import_data.exchanger_name) > 35 )
	

--******************************vessel_name

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  6, 'vessel_name' , rpt_tmp_import_data.vessel_name, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.vessel_name) > 35 )

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.vessel_name = LEFT(rpt_tmp_import_data.vessel_name, 35)
	WHERE (LEN(rpt_tmp_import_data.vessel_name) > 35 )
	

--******************** If EIN field is less than 9 digits, add 0's to the front of the EIN to make it 9 digits		*****************************************
--*******************************************************************************************************************


--******************************carrier_fein

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  8, 'carrier_fein' , rpt_tmp_import_data.carrier_fein, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.carrier_fein) < 9 AND carrier_fein<>'' )

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.carrier_fein = RIGHT(('000000000' + rpt_tmp_import_data.carrier_fein), 9)
	WHERE (LEN(rpt_tmp_import_data.carrier_fein) < 9 AND  rpt_tmp_import_data.carrier_fein<>'')


--******************************carrier_tx_taxpayer

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  8, 'carrier_tx_taxpayer' , rpt_tmp_import_data.carrier_tx_taxpayer, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.carrier_tx_taxpayer) < 11 AND carrier_tx_taxpayer<>'' )

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.carrier_tx_taxpayer = RIGHT(('00000000000' + rpt_tmp_import_data.carrier_tx_taxpayer), 11)
	WHERE (LEN(rpt_tmp_import_data.carrier_tx_taxpayer) < 11 AND  rpt_tmp_import_data.carrier_tx_taxpayer<>'')


--******************************consignor_fein

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  8, 'consignor_fein' , rpt_tmp_import_data.consignor_fein, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.consignor_fein) < 9 AND consignor_fein<>'')

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.consignor_fein = RIGHT(('000000000' + rpt_tmp_import_data.consignor_fein), 9)
	WHERE (LEN(rpt_tmp_import_data.consignor_fein) < 9 AND consignor_fein<>'' )


--******************************position_holder_fein

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  8, 'position_holder_fein' , rpt_tmp_import_data.position_holder_fein, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.position_holder_fein) < 9 AND position_holder_fein<>'')

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.position_holder_fein = RIGHT(('000000000' + rpt_tmp_import_data.position_holder_fein), 9)
	WHERE (LEN(rpt_tmp_import_data.position_holder_fein) < 9 AND rpt_tmp_import_data.position_holder_fein<>'')


--******************************position_holder_tx_taxpayer

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  8, 'position_holder_tx_taxpayer' , rpt_tmp_import_data.position_holder_tx_taxpayer, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.position_holder_tx_taxpayer) < 11 AND position_holder_tx_taxpayer<>'')

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.position_holder_tx_taxpayer = RIGHT(('00000000000' + rpt_tmp_import_data.position_holder_tx_taxpayer), 11)
	WHERE (LEN(rpt_tmp_import_data.position_holder_tx_taxpayer) < 11 AND rpt_tmp_import_data.position_holder_tx_taxpayer<>'')


--******************************ExchFEIN

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  8, 'ExchFEIN' , rpt_tmp_import_data.exchanger_fein, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.exchanger_fein) < 9 AND rpt_tmp_import_data.exchanger_fein<>'')

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.exchanger_fein = RIGHT(('000000000' + rpt_tmp_import_data.exchanger_fein), 9)
	WHERE (LEN(rpt_tmp_import_data.exchanger_fein) < 9 AND rpt_tmp_import_data.exchanger_fein<>'')



--********************Check EIN and facility fields to make sure they are no more than 9 digits	****************************************
--*******************************************************************************************************************


--******************************carrier_fein

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  7, 'carrier_fein' , rpt_tmp_import_data.carrier_fein, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.carrier_fein) > 9 )

	
--******************************carrier_tx_taxpayer

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  7, 'carrier_tx_taxpayer' , rpt_tmp_import_data.carrier_tx_taxpayer, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.carrier_tx_taxpayer) > 11 )




--******************************consignor_fein

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  7, 'consignor_fein' , rpt_tmp_import_data.consignor_fein, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.consignor_fein) > 9 )



--******************************position_holder_fein

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  7, 'position_holder_fein' , rpt_tmp_import_data.position_holder_fein, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.position_holder_fein) > 9 )

	
--******************************position_holder_tx_taxpayer

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  7, 'position_holder_tx_taxpayer' , rpt_tmp_import_data.position_holder_tx_taxpayer, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.position_holder_tx_taxpayer) > 11 )




--******************************ExchFEIN

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  7, 'ExchFEIN' , rpt_tmp_import_data.exchanger_fein, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.exchanger_fein) > 9 )






--******************************von_imo

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  33, 'von_imo' , rpt_tmp_import_data.von_imo, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.von_imo) > 9 )

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  38, 'von_imo' , rpt_tmp_import_data.von_imo, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (rpt_tmp_import_data.von_imo) = '0' 


--******************************destintion_facility

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  9, 'destintion_facility' , rpt_tmp_import_data.destintion_facility, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.destintion_facility) > 9 )


--******************************destination_state

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  9, 'destination_state' , rpt_tmp_import_data.destination_state, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.destination_state) > 2 )



--******************************origin_facility

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  9, 'origin_facility' , rpt_tmp_import_data.origin_facility, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.origin_facility) > 9 )



--*******************Check facility for correct format.  Must be Alpha, Numeric, Numeric, Alpha, Alpha, Numeric, Numeric, Numeric, Numeric		****************************************
--*******************************************************************************************************************


--******************************origin_facility

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  10, 'origin_facility' , rpt_tmp_import_data.origin_facility, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (( ((LEFT(rpt_tmp_import_data.origin_facility,1) <>  'T') AND  (LEFT(rpt_tmp_import_data.origin_facility,1) <>  'R') AND (LEFT(rpt_tmp_import_data.origin_facility,1) <>  'B')  
	AND (LEFT(rpt_tmp_import_data.origin_facility,1) <>  'E')  )
			OR (ISNUMERIC(RIGHT(LEFT(rpt_tmp_import_data.origin_facility,3),2)) = 0)
			OR (RIGHT(LEFT(rpt_tmp_import_data.origin_facility,5),2) NOT IN (SELECT st_abbreviation FROM rpt_state_codes ))
			OR (ISNUMERIC(RIGHT(rpt_tmp_import_data.origin_facility,4)) = 0)
			) AND (rpt_tmp_import_data.origin_facility <> ''))
			-- MAKE FUNCTION??


--******************************destintion_facility

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  10, 'destintion_facility' , rpt_tmp_import_data.destintion_facility, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (( ((LEFT(rpt_tmp_import_data.destintion_facility,1) <>  'T') AND  (LEFT(rpt_tmp_import_data.destintion_facility,1) <>  'R') AND (LEFT(rpt_tmp_import_data.destintion_facility,1) <>  'B')  
	AND (LEFT(rpt_tmp_import_data.destintion_facility,1) <>  'E')  )
			OR (ISNUMERIC(RIGHT(LEFT(rpt_tmp_import_data.destintion_facility,3),2)) = 0)
			OR (RIGHT(LEFT(rpt_tmp_import_data.destintion_facility,5),2) NOT IN (SELECT st_abbreviation FROM rpt_state_codes))
			OR (ISNUMERIC(RIGHT(rpt_tmp_import_data.destintion_facility,4)) = 0)
			) AND (rpt_tmp_import_data.destintion_facility <> ''))



--*******************          product_code rules			****************************************
--*******************************************************************************************************************


----******************************product_code- if the length is less than 3, then pad the front of the code with 0's to make it 3 

--	UPDATE dbo.rpt_tmp_import_data
--	SET dbo.rpt_tmp_import_data.product_code = RIGHT(('00' + rpt_tmp_import_data.product_code), 3)
--	WHERE (LEN(rpt_tmp_import_data.product_code) < 3 AND product_code<>'')


--******************************product_code- make sure it is in the list and is a valid product code

	--INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	--SELECT  15, 'product_code' , rpt_tmp_import_data.product_code, rpt_tmp_import_data._key
	--FROM     rpt_tmp_import_data --INNER JOIN Products ON rpt_tmp_import_data.product_code = 
	--WHERE (rpt_tmp_import_data.product_code NOT IN (select Products.product_code from Products)) OR (rpt_tmp_import_data.product_code IS NULL)
 

----******************************product_code- If the field BAAllowed is false, then the transaction_typeID can not be "TR_BA"
 
-- 	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT  16, 'product_code' , rpt_tmp_import_data.product_code, rpt_tmp_import_data._key
--	FROM            rpt_tmp_import_data INNER JOIN
--								Products ON rpt_tmp_import_data.product_code = Products.product_code
--	WHERE        (Products.BAAllowed = 0) AND (rpt_tmp_import_data.transaction_type_code = 'TR_BA')
 

----******************************  product_code- If the field CEAllowed is false, then transaction_mode can not be "CE"
 
-- 	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT  17, 'product_code' , rpt_tmp_import_data.product_code, rpt_tmp_import_data._key
--	FROM            rpt_tmp_import_data INNER JOIN
--								Products ON rpt_tmp_import_data.product_code = Products.product_code
--	WHERE        (Products.CEAllowed = 0) AND (rpt_tmp_import_data.transaction_mode = 'CE') 


--*******************     Check Valid States and Countries	****************************************
--*******************************************************************************************************************


--******************************destination_state- check the PostalAbb table to make sure it is valid
	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  28, 'destination_state' , rpt_tmp_import_data.destination_state, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data LEFT OUTER JOIN
								rpt_state_codes ON rpt_tmp_import_data.destination_state = st_abbreviation
	WHERE    rpt_tmp_import_data.destination_state <> '' AND   rpt_tmp_import_data.destination_state NOT IN (SELECT st_abbreviation FROM rpt_state_codes)
											  


						

--********************  Check length of BOL to make sure 15 or less	*****************************************
--*******************************************************************************************************************


--******************************BOL

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  6, 'BOL' , rpt_tmp_import_data.BOL, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (LEN(rpt_tmp_import_data.BOL) > 15 )

	UPDATE dbo.rpt_tmp_import_data
	SET dbo.rpt_tmp_import_data.BOL = LEFT(rpt_tmp_import_data.BOL, 15)
	WHERE (LEN(rpt_tmp_import_data.BOL) > 15 )


--********************  Check for 0 gallon transactions		*****************************************
--*******************************************************************************************************************

	
--******************************	net_gallons- if the net_gallons amount is 0, then remove that transaction (not allowed to report 0 gallon transactions)

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  34, 'net_gallons' , rpt_tmp_import_data.net_gallons, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE ((rpt_tmp_import_data.net_gallons = '0' ) or (rpt_tmp_import_data.net_gallons = 0 )) AND (rpt_tmp_import_data.transaction_type <> 'IN')


	DELETE FROM dbo.rpt_tmp_import_data 
	WHERE (rpt_tmp_import_data.net_gallons = '0' or rpt_tmp_import_data.net_gallons = 0) AND (rpt_tmp_import_data.transaction_type <> 'IN')
	--UPDATE dbo.rpt_tmp_import_data
	--SET dbo.rpt_tmp_import_data.net_gallons = null
	--WHERE (rpt_tmp_import_data.net_gallons = '0' )


--******************************gross_gallonss- if the Gross Gallon <> '', then it can not be 0 gallons

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  35, 'gross_gallons' , rpt_tmp_import_data.gross_gallons, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (rpt_tmp_import_data.gross_gallons = '0' )



--******************************document_date- check to make sure it is a valid date	

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  31, 'document_date' , rpt_tmp_import_data.document_date, rpt_tmp_import_data._key
	FROM     rpt_tmp_import_data 
	WHERE (ISDATE(rpt_tmp_import_data.document_date) = 0 ) AND rpt_tmp_import_data.document_date <> ''


--******************************document_date- The month in the doc date must be the same or one month prior than the doc_irs_end_date for the Document

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  32, 'document_date' , rpt_tmp_import_data.document_date, rpt_tmp_import_data._key --, document_date, doc_irs_end_date
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_documents ON rpt_tmp_import_data.document_key = rpt_documents._key
	WHERE  ((DATEPART(month, (rpt_documents.doc_irs_end_date)) <> DATEPART(month,rpt_tmp_import_data.document_date ))
			AND  (DATEPART(month,(DATEADD(mm, -1,  rpt_documents.doc_irs_end_date))) <> DATEPART(month,rpt_tmp_import_data.document_date )))
			AND  (rpt_tmp_import_data.document_date <> '') OR (DATEPART(year, (rpt_documents.doc_irs_end_date)) <> DATEPART(year,rpt_tmp_import_data.document_date ))
	
	
	
			--(rpt_tmp_import_data.document_date NOT BETWEEN DATEADD(month, -1, rpt_documents.doc_irs_end_date)AND  rpt_documents.doc_irs_end_date)  

--******************** PositionHolder and product_code rule	*****************************************
--*******************************************************************************************************************


--******************************position_holder_name and position_holder_fein- If there is a position_holder_name there must be a position_holder_fein, if there is a position_holder_fein there must be a position_holder_name
--PRINT 'position_holder_name and position_holder_fein'
--	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT  27, 'position_holder_name' , rpt_tmp_import_data.position_holder_name, rpt_tmp_import_data._key
--	FROM            rpt_tmp_import_data INNER JOIN
--								Products ON rpt_tmp_import_data.product_code = Products.product_code
--	WHERE        (rpt_tmp_import_data.position_holder_name = '') AND (Products.RequirePosHolder = 1) 
--	AND ((rpt_tmp_import_data.transaction_type <> 'IN') AND (rpt_tmp_import_data.transaction_type <> 'CR') AND (rpt_tmp_import_data.transaction_type <> 'CD'))

--	UPDATE       rpt_tmp_import_data
--	SET          position_holder_name = ''
--	FROM            rpt_tmp_import_data INNER JOIN
--								Products ON rpt_tmp_import_data.product_code = Products.product_code
--	WHERE        (rpt_tmp_import_data.position_holder_name = '') AND (Products.RequirePosHolder = 1) 
--	AND ((rpt_tmp_import_data.transaction_type <> 'IN') AND (rpt_tmp_import_data.transaction_type <> 'CR') AND (rpt_tmp_import_data.transaction_type <> 'CD'))

--	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT  27, 'position_holder_fein' , rpt_tmp_import_data.position_holder_fein, rpt_tmp_import_data._key
--	FROM            rpt_tmp_import_data INNER JOIN
--								Products ON rpt_tmp_import_data.product_code = Products.product_code
--	WHERE        (Products.RequirePosHolder = 1) AND (rpt_tmp_import_data.position_holder_fein = '') 
--	AND ((rpt_tmp_import_data.transaction_type <> 'IN') AND (rpt_tmp_import_data.transaction_type <> 'CR') AND (rpt_tmp_import_data.transaction_type <> 'CD'))

--	UPDATE       rpt_tmp_import_data
--	SET                position_holder_fein = ''
--	FROM            rpt_tmp_import_data INNER JOIN
--								Products ON rpt_tmp_import_data.product_code = Products.product_code
--	WHERE        (Products.RequirePosHolder = 1) AND (rpt_tmp_import_data.position_holder_fein = '') 
--	AND ((rpt_tmp_import_data.transaction_type <> 'IN') AND (rpt_tmp_import_data.transaction_type <> 'CR') AND (rpt_tmp_import_data.transaction_type <> 'CD'))

--********************  Make sure if there is a name, there is an EIN and vice versa		*****************************************
--*******************************************************************************************************************


--******************************carrier_name and Carrier ID- If there is a carrier_name there must be a Carrier ID, if there is a Carrier ID there must be a carrier_name
PRINT 'carrier_name and Carrier ID'
	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  22, 'carrier_name' , rpt_tmp_import_data.carrier_name, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_tmp_import_data.carrier_name = '') AND ((rpt_tmp_import_data.carrier_fein <> '') OR (rpt_tmp_import_data.carrier_tx_taxpayer <> ''))AND (rpt_transaction_types.tt_require_carrier  IS NULL) AND (rpt_transaction_types.tt_report_type = @report_type)


	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  22, 'Carrier FEIN/ID' , rpt_tmp_import_data.carrier_fein, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_tmp_import_data.carrier_name <> '') AND (rpt_tmp_import_data.carrier_fein = '')AND (rpt_tmp_import_data.carrier_tx_taxpayer = '')  AND (rpt_transaction_types.tt_require_carrier  IS NULL) AND (rpt_transaction_types.tt_report_type = @report_type)



--******************************consignor_name and Consignor ID- If there is a consignor_name there must be a Consignor ID, if there is a Consignor ID there must be a consignor_name
PRINT 'consignor_name and Consignor ID'
	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  22, 'consignor_name' , rpt_tmp_import_data.consignor_name, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_tmp_import_data.consignor_name = '') AND ((rpt_tmp_import_data.consignor_fein <> ''))AND (rpt_transaction_types.tt_require_consignor  IS NULL) AND (rpt_transaction_types.tt_report_type = @report_type)


	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  22, 'Consignor FEIN/ID' , rpt_tmp_import_data.consignor_fein, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_tmp_import_data.consignor_name <> '') AND (rpt_tmp_import_data.consignor_fein = '') AND (rpt_transaction_types.tt_require_consignor  IS NULL) AND (rpt_transaction_types.tt_report_type = @report_type)


--******************************position_holder_name and PositionHolder ID- If there is a position_holder_name there must be a PositionHolder ID, if there is a PositionHolder ID there must be a position_holder_name
PRINT 'position_holder_name and PositionHolder ID'
	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  22, 'position_holder_name' , rpt_tmp_import_data.position_holder_name, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_tmp_import_data.position_holder_name = '') AND ((rpt_tmp_import_data.position_holder_fein <> '')OR(rpt_tmp_import_data.position_holder_tx_taxpayer <> '')  )AND (rpt_transaction_types.tt_require_position_holder  IS NULL) AND (rpt_transaction_types.tt_report_type = @report_type)


	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  22, 'PositionHolder FEIN/ID' , rpt_tmp_import_data.position_holder_fein, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_tmp_import_data.position_holder_name <> '') AND (rpt_tmp_import_data.position_holder_fein = '')AND (rpt_tmp_import_data.position_holder_tx_taxpayer = '') AND (rpt_transaction_types.tt_require_position_holder  IS NULL) AND (rpt_transaction_types.tt_report_type = @report_type)



--******************************ExchName and Exchanger ID- If there is a ExchName there must be a Exchanger ID, if there is a Exchanger ID there must be a ExchName
PRINT 'Exchanger Name and Exchanger ID'
	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  22, 'exchanger_name' , rpt_tmp_import_data.exchanger_name, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_tmp_import_data.exchanger_name = '') AND ((rpt_tmp_import_data.exchanger_fein <> '') )AND (rpt_transaction_types.tt_require_exchanger  IS NULL) AND (rpt_transaction_types.tt_report_type = @report_type)


	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  22, 'Exchanger FEIN/ID' , rpt_tmp_import_data.exchanger_fein, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_tmp_import_data.exchanger_name <> '') AND (rpt_tmp_import_data.exchanger_fein = '')  AND (rpt_transaction_types.tt_require_exchanger  IS NULL) AND (rpt_transaction_types.tt_report_type = @report_type)



--******************************vessel_name and von_imo- If there is a vessel_name there must be a von_imo, if there is a von_imo there must be a vessel_name
PRINT 'vessel_name and von_imo'
	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  22, 'vessel_name' , rpt_tmp_import_data.vessel_name, rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_transaction_types.tt_require_vessel  IS NULL) AND (rpt_tmp_import_data.vessel_name = '') AND (rpt_tmp_import_data.von_imo <> '') AND (rpt_transaction_types.tt_report_type = @report_type)

	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
	SELECT  22, 'von_imo' , rpt_tmp_import_data.von_imo,  rpt_tmp_import_data._key
	FROM            rpt_tmp_import_data INNER JOIN
								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
	WHERE        (rpt_transaction_types.tt_require_vessel  IS NULL) AND (rpt_tmp_import_data.von_imo = '') AND (rpt_tmp_import_data.vessel_name <> '') AND (rpt_transaction_types.tt_report_type = @report_type)


--********************             carrier_fein values	                    *****************************************
--*******************************************************************************************************************

	-- ErrorCheckingSproc speadsheet line 159
--******************************carrier_fein- If it is '777777777' or '888888888' it is invalid
--PRINT 'carrier_fein- If it is 777777777 or 888888888 it is invalid'
--	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT  24, 'carrier_fein' , rpt_tmp_import_data.carrier_fein, rpt_tmp_import_data._key
--	FROM     rpt_tmp_import_data 
--	WHERE ((rpt_tmp_import_data.carrier_fein = '777777777') OR (rpt_tmp_import_data.carrier_fein = '888888888'))

	-- ErrorCheckingSproc speadsheet line 160
--******************************carrier_fein- If IsBulk from the transaction_type table is false and the EIN is '444444444' then EIN is not valid
--PRINT 'carrier_fein- If IsBulk from the transaction_type table is false and the EIN is 444444444 then EIN is not valid'
--	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT  25, 'carrier_fein' , rpt_tmp_import_data.carrier_fein, rpt_tmp_import_data._key
--	FROM            rpt_tmp_import_data INNER JOIN
--								rpt_transaction_types ON rpt_tmp_import_data.transaction_type_code = rpt_transaction_types.tt_tranaction_type_code
--	WHERE        (rpt_transaction_types.IsBulk = 0) AND (rpt_tmp_import_data.carrier_fein = '444444444')

	-- ErrorCheckingSproc speadsheet line 161
--******************************carrier_fein- If Carrier9s from the rpt_transaction_types table is false and the EIN is '999999999' then EIN is not valid
--PRINT 'carrier_fein- If Carrier9s from the rpt_transaction_types table is false and the EIN is 99999999 then EIN is not valid'
--	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT  26, 'carrier_fein' , rpt_tmp_import_data.carrier_fein, rpt_tmp_import_data._key
--	FROM     rpt_tmp_import_data 
--	WHERE (((SELECT Carrier9s FROM dbo.rpt_transaction_types WHERE (rpt_transaction_types.tt_tranaction_type_code = rpt_tmp_import_data.transaction_type_code)) = 0 ) AND (rpt_tmp_import_data.carrier_fein = '999999999' ))



--********************             position_holder_fein values	                    *****************************************
--*******************************************************************************************************************

--******************************position_holder_fein- If the EIN is '999999999' then EIN is not valid
--PRINT 'position_holder_fein- If EIN is 99999999 then EIN is not valid'
--	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT  26, 'position_holder_fein' , rpt_tmp_import_data.position_holder_fein, rpt_tmp_import_data._key
--	FROM     rpt_tmp_import_data 
--	WHERE (rpt_tmp_import_data.position_holder_fein = '999999999' )


--********************            InventoryTransactions	                    *****************************************
----*******************************************************************************************************************

--	-- ErrorCheckingSproc speadsheet guide line 164
----******************************BeginningInv- If there is a BeginningInv, check to make sure that it matches the EndingInv from the prior month (if it exists)

--PRINT 'Check BeginningInv line 164'

--DECLARE @PFilingEntity uniqueidentifier = (SELECT FilingEntityID FROM rpt_documents WHERE DocumentID = @document_key)
--DECLARE @doc_irs_end_date DATE = (SELECT doc_irs_end_date FROM dbo.rpt_documents  WHERE (dbo.rpt_documents.DocumentID = @document_key))
--DECLARE @PrevIRSEndDate DATE = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@doc_irs_end_date),0))  --DATEADD(mm, -1, @doc_irs_end_date)
--DECLARE @PrevDocument uniqueidentifier = (SELECT TOP (1) DocumentID
--											FROM     rpt_documents
--											WHERE    (FilingEntityID = @PFilingEntity) AND (doc_irs_end_date = @PrevIRSEndDate)
--											ORDER BY CreationDateTime DESC)
---- Log the error if there is one
--CREATE TABLE #BeginningInvTemp( product_code varchar(3), BeginningInv bigint)


----WITH BeginInv_CTE (product_code, BeginningInv)
----AS
--INSERT INTO #BeginningInvTemp (product_code, BeginningInv)
--	(SELECT  product_code, EndingInv
--		FROM dbo.BalanceSheetState 
--		WHERE (BalanceSheetState.doc_irs_end_date = @PrevIRSEndDate) AND  BalanceSheetState.DocumentID = @PrevDocument
--		)

--	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT	18, 'BeginningInv', rpt_tmp_import_data.BeginningInv, rpt_tmp_import_data._key
--	FROM dbo.rpt_tmp_import_data LEFT OUTER JOIN
--						#BeginningInvTemp ON  #BeginningInvTemp.product_code = rpt_tmp_import_data.product_code
--	WHERE  (rpt_tmp_import_data.BeginningInv <>  #BeginningInvTemp.BeginningInv)
--			AND (#BeginningInvTemp.BeginningInv <> '' )
--			AND (rpt_tmp_import_data.transaction_type_code = 'IN_BI')

---- Change the Beginning INv in the balance sheet
----WITH BeginInv_CTE (product_code, BeginningInv)
----AS
----	(SELECT  product_code, EndingInv
----		FROM dbo.BalanceSheet 
----		WHERE (BalanceSheet.doc_irs_end_date = @PrevIRSEndDate) AND  BalanceSheet.DocumentID = @PrevDocument
----		)
--	UPDATE dbo.rpt_tmp_import_data
--	SET  rpt_tmp_import_data.BeginningInv = #BeginningInvTemp.BeginningInv
--	FROM dbo.rpt_tmp_import_data LEFT OUTER JOIN
--			#BeginningInvTemp ON  #BeginningInvTemp.product_code = rpt_tmp_import_data.product_code
--	WHERE  (rpt_tmp_import_data.BeginningInv <>  #BeginningInvTemp.BeginningInv)
--			AND (#BeginningInvTemp.BeginningInv <> '' )
--			AND (rpt_tmp_import_data.transaction_type_code = 'IN_BI')

--drop table #BeginningInvTemp

--	-- ErrorCheckingSproc speadsheet line 165
----******************************BeginningInv and EndingInv- Can not be less than 0
--PRINT 'Check Beginning Inv and Ending Inv greater than 0'
--PRINT 'Check BeginningInv'

--	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT  19, 'Beginning Inv' , rpt_tmp_import_data.BeginningInv, rpt_tmp_import_data._key
--	FROM     rpt_tmp_import_data 
--	WHERE (CONVERT(FLOAT,(rpt_tmp_import_data.BeginningInv)) < 0 ) AND (ISNUMERIC(rpt_tmp_import_data.BeginningInv) = 1)

--PRINT 'Check Ending Inv'
--	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT  19, 'Ending Inv' , rpt_tmp_import_data.EndingInv, rpt_tmp_import_data._key
--	FROM     rpt_tmp_import_data 
--	WHERE (CONVERT(FLOAT,(rpt_tmp_import_data.EndingInv)) < 0 ) AND (ISNUMERIC(rpt_tmp_import_data.EndingInv) = 1);

--	-- ErrorCheckingSproc speadsheet line 166
----******************************BeginningInv, EndingInv, and TargetGL- there can not be more than one transaction per product code in the file 
--PRINT 'Check BeginningInv, EndingInv, TargetGL :  only one transaction per product code';

---- Beginning Inv
--	WITH product_codeBI_CTE (product_code, NumberTrans)
--	AS
--	(
--		SELECT product_code, COUNT(DISTINCT tmp_import_row_key) AS NumberTrans
--		FROM dbo.rpt_tmp_import_data 
--		WHERE (rpt_tmp_import_data.BeginningInv <> '') and rpt_tmp_import_data.transaction_type = 'IN' AND rpt_tmp_import_data.transaction_mode = 'BI' and rpt_tmp_import_data.product_code <> ''
--		GROUP BY product_code
--	)

--	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT	20, 'BeginningInv', '', _key --(SELECT TOP 1 _key FROM dbo.rpt_tmp_import_data WHERE ((rpt_tmp_import_data.product_code = product_codeBI_CTE.product_code) AND (rpt_tmp_import_data.BeginningInv <> '')) )
--	FROM product_codeBI_CTE INNER JOIN rpt_tmp_import_data ON rpt_tmp_import_data.product_code = product_codeBI_CTE.product_code
--	WHERE (product_codeBI_CTE.NumberTrans >1);

----EndingInv
--	WITH product_codeEI_CTE (product_code, NumberTrans)
--	AS
--	(
--		SELECT product_code, COUNT(DISTINCT tmp_import_row_key) AS NumberTrans
--		FROM dbo.rpt_tmp_import_data 
--		WHERE (rpt_tmp_import_data.EndingInv <> '') and rpt_tmp_import_data.transaction_type = 'IN' AND rpt_tmp_import_data.transaction_mode = 'EI' and rpt_tmp_import_data.product_code <> ''
--		GROUP BY product_code
--	)

--	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
--	SELECT	20, 'EndingInv', '', _key -- (SELECT TOP 1 _key FROM dbo.rpt_tmp_import_data WHERE ((rpt_tmp_import_data.product_code = product_codeEI_CTE.product_code) AND (rpt_tmp_import_data.EndingInv <> '')) )
--	FROM product_codeEI_CTE INNER JOIN rpt_tmp_import_data ON rpt_tmp_import_data.product_code = product_codeEI_CTE.product_code
--	WHERE (product_codeEI_CTE.NumberTrans >1);

------Target GL
----	WITH product_codeGL_CTE (product_code, NumberTrans)
----	AS
----	(
----		SELECT product_code, COUNT(DISTINCT tmp_import_row_key) AS NumberTrans
----		FROM dbo.rpt_tmp_import_data 
----		WHERE (rpt_tmp_import_data.TargetGL <> '')  and rpt_tmp_import_data.transaction_type = 'IN' AND rpt_tmp_import_data.transaction_mode = 'TG'
----		GROUP BY product_code
----	)

----	INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
----	SELECT 20, 'TargetGL', '', _key  --(SELECT TOP 1 _key FROM dbo.rpt_tmp_import_data WHERE ((rpt_tmp_import_data.product_code = product_codeGL_CTE.product_code) AND (rpt_tmp_import_data.TargetGL <> '')) )
----	FROM product_codeGL_CTE INNER JOIN rpt_tmp_import_data ON rpt_tmp_import_data.product_code = product_codeGL_CTE.product_code
----	WHERE (product_codeGL_CTE.NumberTrans >1)


--********************           Book Adjustments	                   *****************************************
--*******************************************************************************************************************

	-- ErrorCheckingSproc speadsheet line 169
--******************************	The total of all TD_BA transaction_typeID's must equal the total of all TR_BA transaction_typeID's

IF	((SELECT TOP 1 rpt_errors.error_code 
	FROM rpt_errors INNER JOIN   rpt_tmp_import_data ON rpt_errors.tmp_import_row_key = rpt_tmp_import_data._key 
	WHERE (rpt_errors.error_code = '4') AND (rpt_tmp_import_data.document_key = @document_key)) IS NULL) 
	BEGIN
		
		DECLARE @BAReceipts FLOAT = CASE WHEN (((SELECT SUM(CONVERT(FLOAT,rpt_tmp_import_data.net_gallons)) FROM rpt_tmp_import_data WHERE  transaction_type_code = 'TR_BA')) IS NOT NULL)
						THEN ((SELECT SUM(CONVERT(FLOAT,rpt_tmp_import_data.net_gallons)) 
						FROM rpt_tmp_import_data WHERE  transaction_type_code = 'TR_BA')) ELSE 0 END

						print @BAReceipts

		DECLARE @BADisbursements FLOAT = CASE WHEN (((SELECT SUM(CONVERT(FLOAT,rpt_tmp_import_data.net_gallons)) FROM rpt_tmp_import_data WHERE  transaction_type_code = 'TD_BA')) IS NOT NULL)
						THEN ((SELECT SUM(CONVERT(FLOAT,rpt_tmp_import_data.net_gallons)) 
						FROM rpt_tmp_import_data WHERE  transaction_type_code = 'TD_BA')) ELSE 0 END

						print @BADisbursements

		IF ( CONVERT(varchar,@BAReceipts) <> CONVERT(varchar,@BADisbursements) ) and @BookAdjustmentCheck = 0
		  BEGIN
			INSERT INTO dbo.rpt_errors (error_code, error_field, error_data, tmp_import_row_key)
			SELECT	36, 'BookAdjustments', 'BookDisb (' + CONVERT(varchar, @BADisbursements) + ') BookReceipt (' + CONVERT(varchar, @BAReceipts) + ')', (SELECT TOP 1 _key FROM rpt_tmp_import_data WHERE transaction_mode = 'BA')--@nullguid 
			
			SET @BookAdjustmentCheck = 1
			print 'book adjustment check done'
			END


DELETE FROM rpt_tmp_import_data WHERE tx_product_code = 'XXX' and document_key = @document_key


	END








SET NOCOUNT OFF
 -- <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
