
-- mgn_ex_rpt_documents_update

DECLARE 

@report_entity_key uniqueidentifier = '1a796fdd-b065-4b49-80db-67f3ba5f1c65' , 
@irs_end_date date = '4/30/2021', 
@report_type_key uniqueidentifier = '0de727bc-045f-49a9-98b4-ee62975c614a', 
@report_version_key uniqueidentifier = 'bba0de85-4e6e-4702-8c8c-ba1a41084158',
@production bit  = 1, 
@no_business_activity bit = 0, 
@document_to_replace uniqueidentifier = NULL,
@report_bi bit = 0


DECLARE @nullguid uniqueidentifier = '00000000-0000-0000-0000-000000000000'
	   ,@new_key uniqueidentifier



DECLARE @is_orig bit, @is_correction bit, @is_replace bit, @is_supplemental bit

SELECT @is_orig  = rpt_is_original, 
	   @is_correction = rpt_is_correction, 
	   @is_replace = rpt_is_replacement, 
	   @is_supplemental = rpt_is_supplemental
FROM rpt_report_versions
WHERE _key = @report_version_key

--if original file, see if there is already a document created 
IF @is_orig = 1
BEGIN 
	SET @new_key = (SELECT _key 
					FROM rpt_documents 
					WHERE doc_report_type_key = @report_type_key 
						  AND doc_report_version_key = @report_version_key
						  AND doc_irs_end_date = @irs_end_date
						  AND doc_report_entity_key = @report_entity_key)
END


IF @new_key IS NULL -- no existing document 
BEGIN 

	SET @new_key = newid();

	INSERT INTO rpt_documents(_key, 
				          doc_report_entity_key, 
						  doc_irs_end_date, 
						  doc_report_type_key, 
						  doc_report_version_key, 
						  doc_creation_datetime, 
						  doc_production, 
						  doc_no_business_activity, 
						  doc_report_bi,
						  doc_status) 
	VALUES (@new_key,
		    @report_entity_key,
			@irs_end_date,
			@report_type_key,
			@report_version_key,
			getdate(),
			@production,
			@no_business_activity,
			@report_bi,
			1)


	UPDATE rpt_documents
	SET doc_gs06_gee02 = doc_isa13_iea02,
	    doc_st02_se02 = doc_isa13_iea02
	WHERE _key = @new_key

					--IF (LEFT(@pDocumentTypeID, 3) = 'GRP')
		--BEGIN
		---- if a group then add groups to the table 
		--	SET @docStSe = (SELECT CONVERT(varchar, ST02_SE02) FROM Documents WHERE DocumentID = @newDocId)

		--	INSERT INTO dbo.documents_group_reports
		--			(gdoc_key
		--			,gdoc_document_id
		--			,gdoc_filing_group_id
		--			,gdoc_tia_5001_5004
		--			,gdoc_st_se_number)
		
		--	( SELECT newid()
		--			,@newDocId
		--			,grp.ent_key
		--			,NULL
		--			, '1' + RIGHT('00' + CONVERT(varchar,grp.ent_number) ,2) + RIGHT(@docStSe,6)
		--	  FROM filing_entity_group_members as grp INNER JOIN 
		--		   FilingEntities as fe ON grp.ent_filing_entity_key = fe.FilingEntityID
		--	  WHERE grp.ent_filing_entity_key = @pFilingEntityID
		--			)

			
		--   INSERT INTO @OldST(facility, sest)
		--   (SELECT gdoc_filing_group_id, gdoc_st_se_number FROM documents_group_reports WHERE gdoc_document_id = @pDocumentToReplace and gdoc_filing_group_id = gdoc_filing_group_id)

		--	UPDATE documents_group_reports
		--	SET gdoc_ref_fj = sest
		--	FROM @OldST 
		--	WHERE gdoc_document_id = @newDocId and gdoc_filing_group_id = facility

		--END


END
ELSE --document already exists 
BEGIN 
	UPDATE rpt_documents
	SET doc_production = @production, 
		doc_no_business_activity = @no_business_activity, 
		doc_report_bi = @report_bi,
		doc_status = 1
	WHERE _key = @new_key

		
		--IF (LEFT(@pDocumentTypeID, 3) = 'GRP')
		--BEGIN
		---- remove the old docs
		--	delete from documents_group_reports WHERE gdoc_document_id = @newDocId
		---- add new docs
		
		---- if a group then add groups to the table 
		--	SET @docStSe = (SELECT CONVERT(varchar, ST02_SE02) FROM Documents WHERE DocumentID = @newDocId)

		--	INSERT INTO dbo.documents_group_reports
		--			(gdoc_key
		--			,gdoc_document_id
		--			,gdoc_filing_group_id
		--			,gdoc_tia_5001_5004
		--			,gdoc_st_se_number)
		
		--	( SELECT newid()
		--			,@newDocId
		--			,grp.ent_key
		--			,NULL
		--			, '1' + RIGHT('00' + CONVERT(varchar,grp.ent_number) ,2) + RIGHT(@docStSe,6)
		--	  FROM filing_entity_group_members as grp INNER JOIN 
		--		   FilingEntities as fe ON grp.ent_filing_entity_key = fe.FilingEntityID
		--	  WHERE grp.ent_filing_entity_key = @pFilingEntityID
		--			)

		--   INSERT INTO @OldST(facility, sest)
		--   (SELECT gdoc_filing_group_id, gdoc_st_se_number FROM documents_group_reports WHERE gdoc_document_id = @pDocumentToReplace and gdoc_filing_group_id = gdoc_filing_group_id)

		--	UPDATE documents_group_reports
		--	SET gdoc_ref_fj = sest
		--	FROM @OldST 
		--	WHERE gdoc_document_id = @newDocId and gdoc_filing_group_id = facility
		--END

END


		SELECT @new_key

