
/****** Object:  UserDefinedFunction [dbo].[fn_replace_non_alpha_numeric]    Script Date: 4/26/2021 7:00:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER FUNCTION [dbo].[fn_replace_non_alpha_numeric] 
(
	@p1 varchar(50)
)
RETURNS varchar(50)
AS
BEGIN

	DECLARE @Result varchar(50)

	SET @Result = @p1

	While PatIndex('%[^a-z0-9]%', @Result) > 0
        Set @Result = Stuff(@Result, PatIndex('%[^a-z0-9]%', @Result), 1, '')


	RETURN @Result

END
GO


