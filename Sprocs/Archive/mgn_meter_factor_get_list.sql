USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_meter_factor_get_list]    Script Date: 3/19/2021 3:25:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/9/2021
-- Description:		Gets a list of meter factors from a selected site and meter.

/*EXEC mgn_meter_factor_get_list  
	@site_key = '00000000-0000-0000-0000-000000000000',
	@meter_key = '00000000-0000-0000-0000-000000000000'

    */	
-- =============================================
ALTER        PROCEDURE [dbo].[mgn_meter_factor_get_list]

	@site_key uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@meter_key uniqueidentifier = '00000000-0000-0000-0000-000000000000'
	  
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from meter_factor

SELECT			mf._key,
				mf.meter_key as [meter_key],
				m.skid_name + ' ' + m.meter_name as [meter_name],
				mf.meter_factor as [meter_factor],
				mf.mtr_start_datetime as [mtr_start_datetime],
				mf.mtr_end_datetime as [mtr_end_datetime],
				ISNULL(mf.external_row_id, 0) as [external_row_id]

FROM			meter_factor AS mf INNER JOIN
				meters AS m ON m._key = mf.meter_key		

WHERE			(mf.site_key = @site_key OR @site_key = '00000000-0000-0000-0000-000000000000')
				AND	(mf.meter_key = @meter_key OR @meter_key = '00000000-0000-0000-0000-000000000000')
				AND (mf.deleted = 0)
				and ( m.active = 1)

ORDER by meter_name
	  
END
