USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_meters_get_row]    Script Date: 4/20/2021 3:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/3/2021
-- Description:		Obtains a single row of meters data from a specified guid.

/*EXEC mgn_meters_get_row
	@row  = '9f3409df-db6b-4cab-877a-b9ccaec402bd'


	@row  = '9417AD6E-452A-4D5C-9EAF-2FF4D2FC5371'
	39e3b653-1cf8-4408-a643-cd975bb03af5

	*/
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_meters_get_row]
	@row uniqueidentifier = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from meters
--WHere _key = @row
   
SELECT			m._key,
				isnull(m.meter_name				,'') as [meter_name],
				isnull(m.meter_name_2			,'') as [meter_description],
				isnull(m.locations_key			,'00000000-0000-0000-0000-000000000000') as [locations_key],
				isnull(l.ln_location_name		,'') as [location_name],
				isnull(m.max_allowable_flow		,0) as[max_allowable_flow],
				isnull(m.max_allowable_pressure ,0) as[max_allowable_pressure],
				isnull(mf.meter_factor			,0) as [meter_factor],
				m.site_key							as [site_key],
				m.active							as [active]

FROM			meters AS m left outer JOIN
				locations AS l ON l._key = m.locations_key left outer join
				(select * from meter_factor where mtr_start_datetime <= getdate() and (mtr_end_datetime >= getdate() or mtr_end_datetime is null)) as mf on mf.meter_key = m._key

WHERE			@row = m._key --and
				--mf.mtr_start_datetime <= getdate() and
				--(mf.mtr_end_datetime >= getdate() or mf.mtr_end_datetime is null)
	  
END
