USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[daily_valve_activity_update]    Script Date: 3/8/2021 10:26:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER   PROCEDURE [dbo].[daily_valve_activity_update]
	@eod datetime
AS
/* 
EXEC daily_valve_activity_update 
	
*/
BEGIN


--DECLARE @eod datetime = '3/2/2021 07:00'

--truncate table dbo.valve_activity
--truncate table dbo.berth_activity_test
/*
1.  Bring in All valve data from historian 
2.  Create valve activity records 
3.  Create berth activity records from the valve activity 
	(min start time on all valves going to that berth = start time)
	(max stop time OR 7 am if loading not done on all valves going to that berth = stop time)


*/
	
-- EMAIL VARIABLES
 DECLARE @profile nvarchar(100),
		 @recipients nvarchar(max), 
		 @subject nvarchar(255),
		 @body_text nvarchar(max),
		 @database nvarchar(20) 

DECLARE @lastDateTime datetime = DATEADD(DAY, -1, @eod)--(SELECT MAX(valve_stop) FROM valve_activity),
DECLARE @error_message varchar(200)

 -- NEW VALVES NEED TO BE SEEDED IN THE VALVE ACTIVITY TABLE WITH A BEGINNING ENTRY 
truncate table tmp_activity_switch

DELETE
FROM valve_activity 
WHERE valve_eod_datetime = @eod 
--BEGIN TRAN

 -- Check for any valves that have not had any activity and insert a starting row
 INSERT INTO valve_activity (valve_key, valve_start, valve_stop, valve_skid_batch_id, valve_meter_check)
 SELECT DISTINCT _key, DATEADD(year, -1, GETDATE()), DATEADD(year, -1, GETDATE()), 'seed row', 1
 FROM valves 
 WHERE _key NOT IN (SELECT valve_key FROM valve_activity)
 --IF @@ERROR <> 0 GOTO TransactionError

 -- if the valve is open at 7 am, add 2 records, to close and open so it will split the batches for 7 am 
INSERT INTO valve_state (valve_key, [timestamp], [state])
SELECT DISTINCT vs.valve_key, DATETIMEFROMPARTS( DATEPART(YEAR, [timestamp]), DATEPART(MONTH, [timestamp]), DATEPART(DAY, [timestamp]), 7, 0, 2, 1) , 1
FROM valve_state vs  LEFT OUTER JOIN 
	 valve_activity va ON va.valve_key = vs.valve_key
WHERE [state] = 1 AND 
	  DATEPART(HOUR, [timestamp]) = 7 AND
	  DATEPART(MINUTE, [timestamp]) = 0 AND
	  DATEPART(SECOND, [timestamp]) = 0 
GROUP BY  vs.valve_key, vs.state, vs.timestamp
HAVING (vs.[timestamp] >= MAX(va.valve_stop))
 --IF @@ERROR <> 0 GOTO TransactionError

INSERT INTO valve_state (valve_key, [timestamp], [state])
SELECT DISTINCT vs.valve_key, DATETIMEFROMPARTS( DATEPART(YEAR, [timestamp]), DATEPART(MONTH, [timestamp]), DATEPART(DAY, [timestamp]), 7, 0, 1, 1) , 0
FROM valve_state vs  LEFT OUTER JOIN 
	 valve_activity va ON va.valve_key = vs.valve_key
WHERE [state] = 1 AND 
	  DATEPART(HOUR, [timestamp]) = 7 AND
	  DATEPART(MINUTE, [timestamp]) = 0 AND
	  DATEPART(SECOND, [timestamp]) = 0 
GROUP BY  vs.valve_key, vs.state, vs.timestamp
HAVING (vs.[timestamp] >= MAX(va.valve_stop))
-- IF @@ERROR <> 0 GOTO TransactionError



 -- Grab all valve activity since the last activity was logged for that valve
--DECLARE tmp_activity_switch Table ([row] int, valve_key uniqueidentifier, [state] bit, [timestamp] datetime)
INSERT INTO tmp_activity_switch([row] , valve_key , [state] , [timestamp])
(SELECT ROW_NUMBER() OVER (ORDER BY vs.valve_key, vs.[timestamp]), 
	    vs.valve_key,
		vs.[state], 
		vs.[timestamp]
FROM valve_state vs  LEFT OUTER JOIN 
	 valve_activity va ON va.valve_key = vs.valve_key
	 WHERE vs.[timestamp] >  @lastDateTime AND vs.[timestamp] <= DATEADD(SECOND, 1, @eod)
GROUP BY  vs.valve_key, vs.state, vs.timestamp
)
 --IF @@ERROR <> 0 GOTO TransactionError



-- delete the rows where the valve state did not change from the record before it 
 DELETE FROM tmp_activity_switch
 WHERE [row]  IN ( SELECT CASE WHEN [state] = LAG([state]) OVER (ORDER BY [row]) THEN [row] END r FROM tmp_activity_switch);
 --IF @@ERROR <> 0 GOTO TransactionError



 DECLARE @remove_switch TABLE(off_key uniqueidentifier, on_key uniqueidentifier)
 -- find rows to remove where there is less than 10 min between the valve start and stop state
;WITH ValveActivityRowNum (RowNumber, _key, valve_key, [state], [timestamp])
AS
(SELECT ROW_NUMBER() OVER (ORDER BY valve_key, [timestamp]), 
_key,
	    valve_key,
		[state], 
		[timestamp]
 FROM tmp_activity_switch)
 INSERT INTO @remove_switch(off_key, on_key)
 SELECT --v_on.valve_key, v_on.[timestamp] as start_datetime,v_on.[timestamp] as end_datetime , 
	    v_off._key as off_key, v_on._key as on_key --, DATEDIFF(minute, v_on.[timestamp], v_off.[timestamp])
 FROM tmp_activity_switch tmp LEFT OUTER JOIN 
	  (SELECT * FROM ValveActivityRowNum WHERE [state] = 1) v_on  ON v_on._key = tmp._key LEFT OUTER JOIN
	  (SELECT * FROM ValveActivityRowNum WHERE [state] = 0) v_off ON v_off.RowNumber + 1 = v_on.RowNumber
																	 AND v_off.valve_key = v_on.valve_key
WHERE DATEDIFF(MINUTE, v_on.[timestamp] ,v_on.[timestamp]) < 10
AND v_off._key IS NOT NULL AND v_on._key  IS NOT NULL
--AND tmp.valve_key = 'E54EB55F-7528-49D2-9FFA-5EF67A3932DE'--'AD980D5F-C146-4769-AE1B-2EAAD9E24247'

DELETE FROM tmp_activity_switch
WHERE _key IN (SELECT off_key FROM @remove_switch)

DELETE FROM tmp_activity_switch
WHERE _key IN (SELECT on_key FROM @remove_switch)


 -- insert a new activity record for each valve's start and stop time 
;WITH ValveActivityRowNum (RowNumber, valve_key, [state], [timestamp])
AS
(SELECT ROW_NUMBER() OVER (ORDER BY valve_key, [timestamp]), 
	    valve_key,
		[state], 
		[timestamp]
 FROM tmp_activity_switch)
 INSERT INTO valve_activity(valve_key, valve_start, valve_stop, valve_eod_datetime)
 SELECT v_on.valve_key, v_on.[timestamp] as start_datetime, v_off.[timestamp] as end_datetime, dbo.get_eod_datetime(v_on.[timestamp])  --, DATEDIFF(minute, v_on.[timestamp], v_off.[timestamp])
 FROM (SELECT * FROM ValveActivityRowNum WHERE [state] = 1) v_on LEFT OUTER JOIN
	  (SELECT * FROM ValveActivityRowNum WHERE [state] = 0) v_off ON v_off.RowNumber = v_on.RowNumber + 1
																	 AND v_off.valve_key = v_on.valve_key
 --IF @@ERROR <> 0 GOTO TransactionError



-- UPDATE THE BERTH 

UPDATE va
SET valve_berth_key = vb.berth_key
FROM valve_activity va INNER JOIN 
	 valve_to_berth vb ON va.valve_key = vb.valve_key
WHERE va.valve_berth_key IS NULL AND va.valve_eod_datetime = @eod
 --IF @@ERROR <> 0 GOTO TransactionError



-- UPDATE THE SKID 

UPDATE va
SET valve_skid_key = v.valve_skid_key
FROM valve_activity va INNER JOIN 
	 valves v ON va.valve_key = v._key
WHERE va.valve_skid_key IS NULL AND va.valve_eod_datetime = @eod
-- IF @@ERROR <> 0 GOTO TransactionError


-- reset those times we altered to create an EOD record
UPDATE va
SET valve_start = DATETIMEFROMPARTS( DATEPART(YEAR, valve_start), DATEPART(MONTH, valve_start), DATEPART(DAY, valve_start), 7, 0, 0, 0)
FROM valve_activity va
WHERE DATEPART(s, valve_start) = 02  AND va.valve_eod_datetime = @eod
 --IF @@ERROR <> 0 GOTO TransactionError


-- reset those times we altered to create an EOD record
UPDATE va
SET valve_stop = DATETIMEFROMPARTS( DATEPART(YEAR, valve_stop), DATEPART(MONTH, valve_stop), DATEPART(DAY, valve_stop), 7, 0, 0, 0)
FROM valve_activity va
WHERE DATEPART(s, valve_stop) = 01 AND va.valve_eod_datetime = @eod
-- IF @@ERROR <> 0 GOTO TransactionError

 -- print 'here2'







-- CHECK FOR NULL OPEN TOTALIZER 
IF EXISTS(	 select va._key,  SUM(mr.meter_totalizer) as start_totalizer --, m.meter_name, va.valve_key, va.valve_start, va.valve_stop
		 from valve_activity va  INNER JOIN 
			  valves v ON va.valve_key = v._key INNER JOIN 
			  meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
			  meter_readings mr ON mr.meter_key = m._key
		 WHERE --va._key = 'F2018298-A7F2-4022-8015-FAB65C9774A4'	AND	   
		    mr.meter_timestamp = va.valve_start AND mr.meter_totalizer IS NULL  AND mr.meter_timestamp > = @lastDateTime  AND mr.meter_timestamp <= @eod
		 GROUP BY va._key) 
BEGIN 
 ---- 
 -- select va._key,  mr.meter_totalizer as start_totalizer , m.meter_name, m._key as meter ,  va.valve_key, va.valve_start, va.valve_stop
	--	 from valve_activity va  INNER JOIN 
	--		  valves v ON va.valve_key = v._key INNER JOIN 
	--		  meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
	--		  meter_readings mr ON mr.meter_key = m._key
	--	 WHERE --va._key = 'F2018298-A7F2-4022-8015-FAB65C9774A4'	AND	   
	--	    mr.meter_timestamp = va.valve_start AND mr.meter_totalizer IS NULL  AND mr.meter_timestamp > = @lastDateTime

UPDATE va
SET valve_error = 1 , 
	valve_error_message = 'null open totalizer'
	from valve_activity va  INNER JOIN 
			  valves v ON va.valve_key = v._key INNER JOIN 
			  meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
			  meter_readings mr ON mr.meter_key = m._key
		 WHERE --va._key = 'F2018298-A7F2-4022-8015-FAB65C9774A4'	AND	   
		    mr.meter_timestamp = va.valve_start AND mr.meter_totalizer IS NULL  AND mr.meter_timestamp > = @lastDateTime	  AND mr.meter_timestamp <= @eod

	SET @error_message = 'nullOpenTotalizer'

		 SELECT @profile  = [profile_name], 
				@recipients  = [recipients],
				@subject  = [subject],
				@body_text =[body_text] ,
				@database = database_query
		FROM email_notifications 
		WHERE [type] = 'nullOpenTotalizer'


		EXEC msdb.dbo.sp_send_dbmail  
			@profile_name = @profile,  
			@recipients = @recipients,  
			@body = @body_text,  
			@subject = @subject;
	
		 
		--GOTO TransactionError

END


-- CHECK FOR NULL CLOSE TOTALIZER 
IF EXISTS(	 select va._key,  SUM(mr.meter_totalizer) as start_totalizer --, m.meter_name, va.valve_key, va.valve_start, va.valve_stop
		 from valve_activity va  INNER JOIN 
			  valves v ON va.valve_key = v._key INNER JOIN 
			  meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
			  meter_readings mr ON mr.meter_key = m._key
		 WHERE --va._key = 'F2018298-A7F2-4022-8015-FAB65C9774A4'	AND	   
		    mr.meter_timestamp = va.valve_stop AND mr.meter_totalizer IS NULL  AND mr.meter_timestamp > = @lastDateTime AND mr.meter_timestamp <= @eod
		 GROUP BY va._key) 
BEGIN 

		UPDATE va
		SET valve_error = 1 , 
			valve_error_message = 'null close totalizer'
		from valve_activity va  INNER JOIN 
			  valves v ON va.valve_key = v._key INNER JOIN 
			  meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
			  meter_readings mr ON mr.meter_key = m._key
		 WHERE --va._key = 'F2018298-A7F2-4022-8015-FAB65C9774A4'	AND	   
		    mr.meter_timestamp = va.valve_stop AND mr.meter_totalizer IS NULL  AND mr.meter_timestamp > = @lastDateTime AND mr.meter_timestamp <= @eod

 -- send mail 
	SET @error_message = 'nullCloseTotalizer'

		 SELECT @profile  = [profile_name], 
				@recipients  = [recipients],
				@subject  = [subject],
				@body_text =[body_text] ,
				@database = database_query
		FROM email_notifications 
		WHERE [type] = 'nullCloseTotalizer'


		EXEC msdb.dbo.sp_send_dbmail  
			@profile_name = @profile,  
			@recipients = @recipients,  
			@body = @body_text,  
			@subject = @subject;
	
		 
			--GOTO TransactionError

END


INSERT INTO valve_activity_meter (_key
           ,valve_activity_key
           ,meter_key
           ,skid_key
           ,valve_start
           ,valve_stop
           ,eod_datetime
           ,open_totalizer
           ,close_totalizer
           ,meter_error)
select newid() , 
	   st.valve_activity_key, 
	   st.meter_key, 
	   st.skid_key, 
	   st.valve_start, 
	   st.valve_stop, 
	   st.valve_eod_datetime, 
	   st.start_totalizer, 
	   en.stop_totalizer, 
	   CASE WHEN stop_totalizer <start_totalizer THEN 1 ELSE 0 END
	FROM (	 select va._key as valve_activity_key,  
					mr.meter_totalizer as start_totalizer, 
					m._key as meter_key, 
					va.valve_eod_datetime, ---eod datetime
					va.valve_start,
					va.valve_stop,
					m.skid_key as skid_key, 
					va.site_key  as site_key 
		 from valve_activity va  INNER JOIN 
			  valves v ON va.valve_key = v._key INNER JOIN 
			  meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
			  meter_readings mr ON mr.meter_key = m._key
		 WHERE mr.meter_timestamp = va.valve_start
		 ) st  INNER JOIN 
	(	 select va._key as valve_activity_key,  
				mr.meter_totalizer as stop_totalizer,
				m._key as meter_key--, m.meter_name, va.valve_key, va.valve_start, va.valve_stop
	from valve_activity va  INNER JOIN 
		valves v ON va.valve_key = v._key INNER JOIN 
		meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
		meter_readings mr ON mr.meter_key = m._key
	WHERE  mr.meter_timestamp = va.valve_stop
	) en ON en.valve_activity_key = st.valve_activity_key AND en.meter_key = st.meter_key  INNER JOIN 
	valve_activity va ON va._key = en.valve_activity_key
WHERE va.valve_bbl IS NULL 
	  AND va.valve_eod_datetime = @eod 






-- UPDATE THE GSV for the skid
UPDATE va
SET valve_bbl = ISNULL(en.stop_totalizer,0) - ISNULL(st.start_totalizer, 0)
FROM (	 select va._key,  SUM(mr.meter_totalizer) as start_totalizer --, m.meter_name, va.valve_key, va.valve_start, va.valve_stop
		 from valve_activity va  INNER JOIN 
			  valves v ON va.valve_key = v._key INNER JOIN 
			  meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
			  meter_readings mr ON mr.meter_key = m._key
		 WHERE --va._key = 'F2018298-A7F2-4022-8015-FAB65C9774A4'	AND	   
		    mr.meter_timestamp = va.valve_start
		 GROUP BY va._key) st  INNER JOIN 
	(	 select va._key,  SUM(mr.meter_totalizer) as stop_totalizer --, m.meter_name, va.valve_key, va.valve_start, va.valve_stop
	from valve_activity va  INNER JOIN 
		valves v ON va.valve_key = v._key INNER JOIN 
		meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
		meter_readings mr ON mr.meter_key = m._key
	WHERE --va._key = 'F2018298-A7F2-4022-8015-FAB65C9774A4'	AND	   
	 mr.meter_timestamp = va.valve_stop
	GROUP BY va._key) en ON en._key = st._key  INNER JOIN 
	valve_activity va ON va._key = en._key
	WHERE va.valve_bbl IS NULL AND va.valve_error <> 1 
	  AND va.valve_eod_datetime = @eod 





--- keep only records with greater than 0 bbls
DELETE from valve_activity 
WHERE valve_bbl    = 0   AND valve_eod_datetime = @eod
-- IF @@ERROR <> 0 GOTO TransactionError


--- delete records with null bbls (seed rows)
DELETE from valve_activity 
WHERE valve_bbl    IS NULL  AND valve_skid_batch_id = 'seed row'
-- IF @@ERROR <> 0 GOTO TransactionError



-- Store the orig valve open and close times 
UPDATE valve_activity
SET valve_start_orig = valve_start, 
	valve_stop_orig = valve_stop
WHERE valve_eod_datetime = @eod
-- IF @@ERROR <> 0 GOTO TransactionError


-- ****************  check the totalizers for the skids and update start and stop times to match when actual meter readings changed ********************
DECLARE 
    @va_row_key uniqueidentifier,
	@valve_start datetime, 
	@valve_stop datetime, 
	@valve_skid uniqueidentifier,
	@new_start datetime , 
	@new_stop datetime, 
	@row_start int, 
	@row_end int,
	@time_start TIME

DECLARE @compare TABLE ([row_number] int, totalizer float, prev_totalizer float) 
DECLARE @debug bit = 0


 
DECLARE va_cursor CURSOR
FOR SELECT _key, valve_start, valve_stop, valve_skid_key
    FROM valve_activity
	WHERE valve_meter_check = 0 AND valve_error = 0 AND valve_eod_datetime = @eod;
 
OPEN va_cursor;
 
FETCH NEXT FROM va_cursor INTO 
   @va_row_key ,
	@valve_start , 
	@valve_stop , 
	@valve_skid
 
WHILE @@FETCH_STATUS = 0
    BEGIN

	-- reset all values 
	DELETE FROM @compare
	DELETE FROM  tmp_skid_totalizer_switch
	SET @new_start = NULL
	SET @new_stop = NULL
	SET @row_start = NULL
	SET @row_end = NULL
	SET @time_start = NULL
  --create new rows from a previous day 

		  -- grab all meter totalizer sums for that valve activity record 
		INSERT INTO tmp_skid_totalizer_switch([row] , skid_key , totalizer , [timestamp])
		SELECT ROW_NUMBER() OVER (ORDER BY s._key , mr.meter_timestamp),
			   s._key, SUM(mr.meter_totalizer) , mr.meter_timestamp
		FROM meter_readings mr INNER JOIN 
			 meters m ON mr.meter_key = m._key INNER JOIN 
			 skids s ON m.skid_key = s._key
		WHERE mr.meter_timestamp BETWEEN  DATEADD(MINUTE, -1, @valve_start) AND @valve_stop AND s._key = @valve_skid
		GROUP BY s.skid_description , mr.meter_timestamp,  s._key
		--select * from tmp_skid_totalizer_switch
		
		
		INSERT INTO @compare ([row_number], totalizer, prev_totalizer)
		SELECT [row], totalizer, LAG(totalizer, 1) OVER (ORDER BY [row]) as prev_totalizer
		FROM tmp_skid_totalizer_switch
		
		
		SET @time_start = CAST(@valve_start as time)
		--select @time_start
		IF (@time_start = '07:00')
		BEGIN
			SET @row_start = (select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number])
			--PRINT @row_start
			IF @debug = 1
				select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number]
		END
		ELSE
		BEGIN
			SET @row_start = (select top(1) [row_number] -1 FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number])
			--PRINT @row_start
			IF @debug = 1
				select top(1) [row_number] -1 FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number]
		END
		--SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_start
		
		SET @row_end = (select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number] desc)
		--PRINT @row_end
		IF @debug = 1
			select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number] desc
		--SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_end
		--SELECT @va_row_key

		SET @new_start =  (SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_start)
		IF @debug = 1
			SELECT @new_start
		
		SET @new_stop = (SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_end)
		IF @debug = 1
			SELECT @new_stop
		IF @debug = 1
			SELECT @va_row_key

		UPDATE valve_activity
		SET valve_start = @new_start
		WHERE _key = @va_row_key

		UPDATE valve_activity
		SET valve_stop = @new_stop
		WHERE _key = @va_row_key
		
		UPDATE valve_activity
		SET valve_meter_check = 1
		WHERE _key = @va_row_key



        FETCH NEXT FROM va_cursor INTO 
             @va_row_key ,
			 @valve_start , 
			 @valve_stop , 
			 @valve_skid;
    END;
 
CLOSE va_cursor;
 
DEALLOCATE va_cursor;





--Update the batch ID *************************************************************************
UPDATE va
SET valve_skid_batch_id = sb.skid_batch_id
FROM valve_activity va LEFT OUTER JOIN 
	 skid_batch_ids sb ON va.valve_skid_key = sb.skid_key --AND va.valve_stop = sb.[timestamp] 
WHERE sb.timestamp >= va.valve_start AND sb.timestamp <= va.valve_stop
	  AND sb.skid_batch_id <> '' AND sb.skid_batch_id <> 'NULL' 
	  AND valve_skid_batch_id IS NULL
	  AND va.valve_eod_datetime = @eod 
 --IF @@ERROR <> 0 GOTO TransactionError

 UPDATE valve_activity
 SET valve_skid_batch_id = ''
 WHERE valve_skid_batch_id IS NULL
	  AND valve_eod_datetime = @eod 

END