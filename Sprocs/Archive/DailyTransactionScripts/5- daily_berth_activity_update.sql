-- ================================================
-- Mangan Inc
-- Created By:  Candice Iago
-- Date: 4/6/2020
-- Purpose
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.daily_berth_activity_update
	
AS
/* 
EXEC daily_berth_activity_update 
	
*/
BEGIN


 -- Check for any berths that have not had any activity and insert a starting row
 --INSERT INTO berth_activity(berth_key, berth_start, berth_stop)
 --SELECT DISTINCT _key, DATEADD(day, -1, GETDATE()), DATEADD(day, -1, GETDATE())
 --FROM berths 
 --WHERE _key NOT IN (SELECT berth_key FROM berth_activity)

 DECLARE @debug bit = 0

 -- find the last time of the berth stop per berth 
 DECLARE @LastBerthStop TABLE (berth uniqueidentifier, stop_datetime datetime)

 INSERT INTO @LastBerthStop (berth, stop_datetime)
 SELECT berth_key, MAX(berth_stop)
 FROM berth_activity 
 GROUP BY berth_key

 --DECLARE @RecentBatchIds TABLE (berth_key uniqueidentifier, berth_start datetime, berth_stop datetime, batch_id nvarchar(200))

 --select * from @LastBerthStop
 DECLARE @valve_activiy TABLE (rowNumber int, _key uniqueidentifier, valve_key uniqueidentifier, 
								valve_start datetime , valve_stop datetime , valve_berth_key uniqueidentifier, 
								valve_batch_id nvarchar(200), eod_day datetime, nsv float)
INSERT INTO @valve_activiy (rowNumber, _key, valve_key, valve_start, valve_stop, valve_berth_key, valve_batch_id, eod_day, nsv)
(SELECT ROW_NUMBER() OVER (ORDER BY va.valve_berth_key, va.valve_start),  va._key, va.valve_key, 
		va.valve_start, va.valve_stop, va.valve_berth_key, va.valve_skid_batch_id, 
		CASE WHEN cast( valve_start as time) >= cast('07:00 AM' as TIME) 
	        THEN DATEADD(day, 1, SMALLDATETIMEFROMPARTS(YEAR(valve_start), MONTH(valve_start), DAY(valve_start), 07, 00)) 
			ELSE SMALLDATETIMEFROMPARTS(YEAR(valve_start), MONTH(valve_start), DAY(valve_start), 07, 00) END, 
			valve_bbl
FROM valve_activity va  LEFT OUTER JOIN 
	 @LastBerthStop bs ON va.valve_berth_key = bs.berth
WHERE (va.valve_start >= bs.stop_datetime OR bs.stop_datetime IS NULL)
	  AND (va.valve_skid_batch_id <> 'seed row') 
	  AND va.valve_ignore = 0-- exclude the seeding rows 
)
--WHERE 
IF @debug = 1 
BEGIN
		SELECT * from @valve_activiy

		-- starts 

		SELECT valve_berth_key, valve_batch_id, MIN(valve_start) as batch_start , eod_day,  SUM(nsv)
			  FROM @valve_activiy 
			  GROUP BY valve_berth_key, valve_batch_id, eod_day
		-- stops
		SELECT valve_berth_key, valve_batch_id, MAX(valve_stop) as batch_stop , eod_day,  SUM(nsv)
			  FROM @valve_activiy 
			  GROUP BY valve_berth_key, valve_batch_id, eod_day
			  -- all
		SELECT v_starts.valve_berth_key, v_starts.valve_batch_id, v_starts.batch_start, v_stop.batch_stop, v_starts.eod_day, v_starts.nsv
		FROM (SELECT valve_berth_key, valve_batch_id, MIN(valve_start) as batch_start , eod_day,  SUM(nsv) as nsv
			  FROM @valve_activiy 
			  GROUP BY valve_berth_key, valve_batch_id, eod_day)v_starts INNER JOIN 
			  (SELECT valve_berth_key, valve_batch_id, MAX(valve_stop) as batch_stop , eod_day,  SUM(nsv) as nsv
			  FROM @valve_activiy 
			  GROUP BY valve_berth_key, valve_batch_id, eod_day) v_stop 
					ON v_starts.valve_berth_key = v_stop.valve_berth_key AND v_starts.valve_batch_id = v_stop.valve_batch_id
						AND v_starts.eod_day = v_stop.eod_day
END

INSERT INTO berth_activity (berth_key, berth_batch_id, berth_start, berth_stop, eod_datetime, berth_meter_bbls)
SELECT v_starts.valve_berth_key, v_starts.valve_batch_id, v_starts.batch_start, v_stop.batch_stop, v_starts.eod_day, v_starts.nsv
		FROM (SELECT valve_berth_key, valve_batch_id, MIN(valve_start) as batch_start , eod_day,  SUM(nsv) as nsv
			  FROM @valve_activiy 
			  GROUP BY valve_berth_key, valve_batch_id, eod_day)v_starts INNER JOIN 
			  (SELECT valve_berth_key, valve_batch_id, MAX(valve_stop) as batch_stop , eod_day,  SUM(nsv) as nsv
			  FROM @valve_activiy 
			  GROUP BY valve_berth_key, valve_batch_id, eod_day) v_stop 
					ON v_starts.valve_berth_key = v_stop.valve_berth_key AND v_starts.valve_batch_id = v_stop.valve_batch_id
						AND v_starts.eod_day = v_stop.eod_day







END