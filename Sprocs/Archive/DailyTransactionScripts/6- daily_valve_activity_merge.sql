CREATE OR ALTER PROCEDURE daily_valve_activity_merge
AS
BEGIN

--DECLARE @date datetime = '2/14/2021'

DECLARE @eod datetime  
--SET @eod  = DATEADD(DAY, 1, SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date) , 07, 00))
SET @eod  =  SMALLDATETIMEFROMPARTS(YEAR(getdate()), MONTH(getdate()), DAY(getdate()) , 07, 00)

DECLARE @records TABLE 
(   [_key] [uniqueidentifier] DEFAULT newid(),
	[valve_key] [uniqueidentifier] NOT NULL,
	[valve_start_orig] [datetime] NULL,
	[valve_stop_orig] [datetime] NULL,
	[valve_start] [datetime] NULL,
	[valve_stop] [datetime] NULL,
	[valve_eod_datetime] [datetime] NULL,
	[valve_berth_key] [uniqueidentifier] NULL,
	[valve_skid_key] [uniqueidentifier] NULL,
	[valve_skid_batch_id] [nvarchar](200) NULL,
	[valve_bbl] [float] NULL,
	[valve_meter_check] [bit] NULL,
	[valve_ignore] [bit] NOT NULL,
	[valve_error] [bit] NOT NULL,
	[valve_error_message] [nvarchar](300) NULL,
	[site_key] [uniqueidentifier] NOT NULL )

INSERT INTO @records([valve_key], [valve_eod_datetime], [valve_berth_key], [valve_skid_key], [valve_skid_batch_id] , [valve_meter_check],[valve_ignore], [valve_error],[valve_error_message], [site_key])
SELECT DISTINCT [valve_key], [valve_eod_datetime], [valve_berth_key], [valve_skid_key], [valve_skid_batch_id] , [valve_meter_check],[valve_ignore], [valve_error],[valve_error_message], [site_key]
FROM valve_activity
WHERE valve_eod_datetime = @eod


UPDATE r
SET valve_start_orig = va.valve_start_orig,
	valve_stop_orig = va.valve_stop_orig,
	valve_start = va.valve_start,
	valve_stop = va.valve_stop,
	valve_bbl = va.valve_bbl
FROM @records r INNER JOIN 
(SELECT  MIN(va.valve_start_orig ) as valve_start_orig, MAX(valve_stop_orig) as valve_stop_orig , Min(valve_start) as valve_start, MAX(valve_stop) as valve_stop, SUM(ISNULL(valve_bbl,0)) as valve_bbl,
		va.[valve_key], va.[valve_eod_datetime], va.[valve_berth_key], va.[valve_skid_key], va.[valve_skid_batch_id] , va.[valve_meter_check],va.[valve_ignore], va.[valve_error],va.[valve_error_message], va.[site_key]
FROM valve_activity  va
WHERE  va.valve_eod_datetime = @eod
GROUP BY va.[valve_key], va.[valve_eod_datetime], va.[valve_berth_key], va.[valve_skid_key], va.[valve_skid_batch_id] , va.[valve_meter_check],va.[valve_ignore], va.[valve_error],va.[valve_error_message], va.[site_key]) va 
ON r.[valve_key] = va.[valve_key]
		      AND r.[valve_eod_datetime] = va.[valve_eod_datetime]
		      AND  r.[valve_berth_key] = va.[valve_berth_key]
		      AND  r.[valve_skid_key] = va.[valve_skid_key]
		      AND  r.[valve_skid_batch_id] = va.[valve_skid_batch_id] 
		      AND  r.[valve_meter_check] = va.[valve_meter_check]
		      AND r.[valve_ignore] = va.[valve_ignore]
		      AND  r.[valve_error] = va.[valve_error]
		      AND ISNULL(r.[valve_error_message], '') = ISNULL(va.[valve_error_message], '')
		      AND  r.[site_key] = va.[site_key] 
WHERE  va.valve_eod_datetime = @eod

UPDATE @records
SET _key = newid()


DELETE FROM valve_activity WHERE  valve_eod_datetime = @eod


INSERT INTO [dbo].[valve_activity]
           ([_key]
           ,[valve_key]
           ,[valve_start_orig]
           ,[valve_stop_orig]
           ,[valve_start]
           ,[valve_stop]
           ,[valve_eod_datetime]
           ,[valve_berth_key]
           ,[valve_skid_key]
           ,[valve_skid_batch_id]
           ,[valve_bbl]
           ,[valve_meter_check]
           ,[valve_ignore]
           ,[valve_error]
           ,[valve_error_message]
           ,[site_key])

           (SELECT [_key]
           ,[valve_key]
           ,[valve_start_orig]
           ,[valve_stop_orig]
           ,[valve_start]
           ,[valve_stop]
           ,[valve_eod_datetime]
           ,[valve_berth_key]
           ,[valve_skid_key]
           ,[valve_skid_batch_id]
           ,[valve_bbl]
           ,[valve_meter_check]
           ,[valve_ignore]
           ,[valve_error]
           ,[valve_error_message]
           ,[site_key]
		   FROM @records)



--select * from @records
--select sum(ISNULL(valve_bbl,0)), valve_berth_key, valve_skid_key
--from valve_activity va 
--where valve_eod_datetime = '2/13/2021 07:00'
--group by valve_berth_key, valve_skid_key


END