-- ================================================
-- Mangan Inc
-- Created By:  Candice Iago
-- Date: 4/6/2020
-- Purpose
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.daily_tank_actuals_update
	
AS
/* 
EXEC daily_tank_actuals_update 
	
*/
BEGIN

DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)

DECLARE @StartDate datetime = DATEADD(day, 1, (SELECT MAX(eod_datetime) FROM tank_actuals))
       ,@EndDate   datetime = DATEADD(day, 1, getdate())
;

WITH theDates AS
     (SELECT @StartDate as theDate
      UNION ALL
      SELECT DATEADD(day, 1, theDate)
        FROM theDates
       WHERE DATEADD(day, 1, theDate) <= @EndDate
     )

INSERT INTO tank_actuals
(_key, tank_key, eod_datetime)
SELECT newid(), t._key, DATETIMEFROMPARTS(DATEPART(YEAR, d.theDate), DATEPART(MONTH, d.theDate), DATEPART(DAY, d.theDate), 7,0,0,0)
 --, p.description,  
FROM tanks t CROSS JOIN 
	 theDates d 
ORDER BY d.theDate
OPTION (MAXRECURSION 0)
;



UPDATE ta
SET [open_gsv] = ISNULL(openTank.OpenTank,0), 
	[close_gsv] = ISNULL(closeTank.CloseTank,0),
	[tank_gsv_change] = ISNULL(closeTank.CloseTank,0)-ISNULL(openTank.OpenTank,0)
FROM 
 (SELECT  t.tank_name, t._key, tank_gsv as OpenTank --, MAX(tank_nsv)
		 FROM tank_readings tr INNER JOIN 
		 tanks t on t._key = tr.tank_key
		 WHERE tank_timestamp = DATEADD(day, -1,@eod) AND t.active = 1) openTank
	INNER JOIN 
	(SELECT  t.tank_name, t._key,  tank_gsv as CloseTank --, MAX(tank_nsv)
		 FROM tank_readings tr INNER JOIN 
		 tanks t on t._key = tr.tank_key
		 WHERE tank_timestamp = @eod AND t.active = 1) closeTank ON openTank._key = closeTank._key INNER JOIN 
tank_actuals ta ON ta.eod_datetime = @eod AND ta.tank_key = openTank._key


UPDATE ta
SET [open_tov] = ISNULL(openTank.OpenTank,0), 
	[close_tov] = ISNULL(closeTank.CloseTank,0),
	[tank_tov_change] = ISNULL(closeTank.CloseTank,0)-ISNULL(openTank.OpenTank,0)
FROM 
 (SELECT  t.tank_name, t._key, tank_tov as OpenTank --, MAX(tank_tov)
		 FROM tank_readings tr INNER JOIN 
		 tanks t on t._key = tr.tank_key
		 WHERE tank_timestamp = DATEADD(day, -1,@eod) AND t.active = 1) openTank
	INNER JOIN 
	(SELECT  t.tank_name, t._key,  tank_tov as CloseTank --, MAX(tank_tov)
		 FROM tank_readings tr INNER JOIN 
		 tanks t on t._key = tr.tank_key
		 WHERE tank_timestamp = @eod AND t.active = 1) closeTank ON openTank._key = closeTank._key INNER JOIN 
tank_actuals ta ON ta.eod_datetime = @eod AND ta.tank_key = openTank._key


END