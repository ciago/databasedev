

--delete FROM pipeline_actuals
DECLARE @StartDate datetime = DATEADD(day, 1, (SELECT MAX(movement_date) FROM pipeline_actuals))
       ,@EndDate   datetime = DATEADD(day, 1, getdate())
;

--PRINT @StartDate
--PRINT @EndDate
;

WITH theDates AS
     (SELECT @StartDate as theDate
      UNION ALL
      SELECT DATEADD(day, 1, theDate)
        FROM theDates
       WHERE DATEADD(day, 1, theDate) <= @EndDate
     )
--SELECT theDate, 1 as theValue
--  FROM theDates
--OPTION (MAXRECURSION 0)
--;

INSERT INTO pipeline_actuals 
(_key, movement_date, pipeline_key)
SELECT newid(), DATETIMEFROMPARTS(DATEPART(YEAR, d.theDate), DATEPART(MONTH, d.theDate), DATEPART(DAY, d.theDate), 7,0,0,0)
, p._key --, p.description,  
FROM pipeline p CROSS JOIN 
	 theDates d 
ORDER BY d.theDate
OPTION (MAXRECURSION 0)
;

 DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)

--select openTot.skid, openTot.pipeline, openTot.eod, 
--	   openTot.OpenTotalizer, closeTot.CloseTotalizer, closeTot.CloseTotalizer - openTot.OpenTotalizer as NetDayBBLS, 
--	   p.description, tp._key
--FROM  
--(SELECT  s._key as skid, p._key as pipeline, pa.movement_date as eod , SUM( ISNULL(mr.meter_totalizer,0)) as CloseTotalizer
--FROM meters m INNER JOIN 
--	 pipeline p on m.pipeline_key = p._key INNER JOIN 
--	 skids s ON s._key = m.skid_key INNER JOIN 
--	 pipeline_actuals pa ON pa.pipeline_key = p._key
--	 LEFT OUTER JOIN 
--	 meter_readings mr ON mr.meter_key = m._key AND mr.meter_timestamp = pa.movement_date
--WHERE pa.movement_date = @eod
--GROUP BY s._key, p._key, pa.movement_date )
--closeTot INNER JOIN	 

--(SELECT  s._key as skid, p._key as pipeline, pa.movement_date as eod , SUM( ISNULL(mr.meter_totalizer,0)) as OpenTotalizer
--FROM meters m INNER JOIN 
--	 pipeline p on m.pipeline_key = p._key INNER JOIN 
--	 skids s ON s._key = m.skid_key INNER JOIN 
--	 pipeline_actuals pa ON pa.pipeline_key = p._key
--	 LEFT OUTER JOIN 
--	 meter_readings mr ON mr.meter_key = m._key AND mr.meter_timestamp = DATEADD(day, -1, pa.movement_date)
--WHERE pa.movement_date = @eod
--GROUP BY s._key, p._key, pa.movement_date) 
--openTot ON openTot.skid = closeTot.skid AND openTot.pipeline = closeTot.pipeline AND openTot.eod = closeTot.eod INNER JOIN 
--pipeline p ON openTot.pipeline = p._key INNER JOIN 
--pipeline_actuals tp ON tp.movement_date = openTot.eod AND tp.pipeline_key = openTot.pipeline


UPDATE tp
SET gsv_bbls = closeTot.CloseTotalizer - openTot.OpenTotalizer
	, open_totalizer = openTot.OpenTotalizer
	, close_totalizer =  closeTot.CloseTotalizer
FROM  
(SELECT  s._key as skid, p._key as pipeline, pa.movement_date as eod , SUM( ISNULL(mr.meter_totalizer,0)) as CloseTotalizer
FROM meters m INNER JOIN 
	 pipeline p on m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key INNER JOIN 
	 pipeline_actuals pa ON pa.pipeline_key = p._key
	 LEFT OUTER JOIN 
	 meter_readings mr ON mr.meter_key = m._key AND mr.meter_timestamp = pa.movement_date
WHERE pa.movement_date  = @eod
GROUP BY s._key, p._key, pa.movement_date )
closeTot INNER JOIN	 

(SELECT  s._key as skid, p._key as pipeline, pa.movement_date as eod , SUM( ISNULL(mr.meter_totalizer,0)) as OpenTotalizer
FROM meters m INNER JOIN 
	 pipeline p on m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key INNER JOIN 
	 pipeline_actuals pa ON pa.pipeline_key = p._key
	 LEFT OUTER JOIN 
	 meter_readings mr ON mr.meter_key = m._key AND mr.meter_timestamp = DATEADD(day, -1, pa.movement_date)
WHERE pa.movement_date = @eod
GROUP BY s._key, p._key, pa.movement_date) 
openTot ON openTot.skid = closeTot.skid AND openTot.pipeline = closeTot.pipeline AND openTot.eod = closeTot.eod INNER JOIN 
pipeline p ON openTot.pipeline = p._key INNER JOIN 
pipeline_actuals tp ON tp.movement_date = openTot.eod AND tp.pipeline_key = openTot.pipeline
WHERE gsv_bbls IS NULL

