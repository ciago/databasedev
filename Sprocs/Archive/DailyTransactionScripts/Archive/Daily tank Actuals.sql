 DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)
--delete FROM tmp_pipeline_actuals
DECLARE @StartDate datetime = DATEADD(day, 1, (SELECT MAX(eod_datetime) FROM tank_actuals))
       ,@EndDate   datetime = DATEADD(day, 1, getdate())
;

--PRINT @StartDate
--PRINT @EndDate
;

WITH theDates AS
     (SELECT @StartDate as theDate
      UNION ALL
      SELECT DATEADD(day, 1, theDate)
        FROM theDates
       WHERE DATEADD(day, 1, theDate) <= @EndDate
     )
--SELECT theDate, 1 as theValue
--  FROM theDates
--OPTION (MAXRECURSION 0)
--;

INSERT INTO tank_actuals
(_key, tank_key, eod_datetime)
SELECT newid(), t._key, DATETIMEFROMPARTS(DATEPART(YEAR, d.theDate), DATEPART(MONTH, d.theDate), DATEPART(DAY, d.theDate), 7,0,0,0)
 --, p.description,  
FROM tanks t CROSS JOIN 
	 theDates d 
ORDER BY d.theDate
OPTION (MAXRECURSION 0)
;


-- TANK open and close balances 
--= '2020-04-15 07:00:00.000'
--SELECT openTank._key, openTank.tank_name, openTank.OpenTank, closeTank.CloseTank, closeTank.CloseTank-openTank.OpenTank, @eod
--FROM 
-- (SELECT  t.tank_name, t._key, tank_nsv as OpenTank --, MAX(tank_nsv)
--		 FROM tank_readings tr INNER JOIN 
--		 tanks t on t._key = tr.tank_key
--		 WHERE tank_timestamp = DATEADD(day, -1,@eod)) openTank
--	INNER JOIN 
--	(SELECT  t.tank_name, t._key,  tank_nsv as CloseTank --, MAX(tank_nsv)
--		 FROM tank_readings tr INNER JOIN 
--		 tanks t on t._key = tr.tank_key
--		 WHERE tank_timestamp = @eod) closeTank ON openTank._key = closeTank._key INNER JOIN 
--tank_actuals ta ON ta.eod_datetime = @eod AND ta.tank_key = openTank._key

-- add a job that does this daily?


UPDATE ta
SET [open_nsv] = openTank.OpenTank, 
	[close_nsv] = closeTank.CloseTank,
	[tank_nsv_change] = closeTank.CloseTank-openTank.OpenTank
FROM 
 (SELECT  t.tank_name, t._key, tank_nsv as OpenTank --, MAX(tank_nsv)
		 FROM tank_readings tr INNER JOIN 
		 tanks t on t._key = tr.tank_key
		 WHERE tank_timestamp = DATEADD(day, -1,@eod)) openTank
	INNER JOIN 
	(SELECT  t.tank_name, t._key,  tank_nsv as CloseTank --, MAX(tank_nsv)
		 FROM tank_readings tr INNER JOIN 
		 tanks t on t._key = tr.tank_key
		 WHERE tank_timestamp = @eod) closeTank ON openTank._key = closeTank._key INNER JOIN 
tank_actuals ta ON ta.eod_datetime = @eod AND ta.tank_key = openTank._key
