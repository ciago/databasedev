
DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)
--UPDATE tk
--SET total_meter_diff = pa.tov_bbls
SELECT tk.total_tank_diff_nsv, pa.tov_bbls
FROM pipeline_to_tank_tickets tk INNER JOIN 	 
	(SELECT SUM(gsv_bbls) as tov_bbls, eod_datetime	 , pipeline_key
	FROM pipeline_actuals 
	WHERE eod_datetime = @eod
	GROUP BY eod_datetime, pipeline_key) pa ON tk.eod_datetime = pa.eod_datetime AND tk.pipeline_key = pa.pipeline_key
WHERE tk.eod_datetime = @eod


SELECT SUM(gsv_bbls) as tov_bbls, eod_datetime	 , pipeline_key
	FROM pipeline_actuals 
	WHERE eod_datetime = @eod AND pipeline_key = 'EDF94AB4-9845-4C15-8873-096D6AFD92A3'
	GROUP BY eod_datetime, pipeline_key


	-- CLOSE TOALIZER 
	SELECT  s._key as skid, p._key as pipeline, pa.eod_datetime as eod , mr.meter_totalizer as CloseTotalizer
FROM meters m INNER JOIN 
	 pipeline p on m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key INNER JOIN 
	 pipeline_actuals pa ON pa.pipeline_key = p._key
	 LEFT OUTER JOIN 
	 meter_readings mr ON mr.meter_key = m._key AND mr.meter_timestamp = pa.eod_datetime
WHERE pa.eod_datetime  = @eod and p._key = 'EDF94AB4-9845-4C15-8873-096D6AFD92A3'
--GROUP BY s._key, p._key, pa.eod_datetime


SELECT  s._key as skid, p._key as pipeline, pa.eod_datetime as eod , mr.meter_totalizer as OpenTotalizer
FROM meters m INNER JOIN 
	 pipeline p on m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key INNER JOIN 
	 pipeline_actuals pa ON pa.pipeline_key = p._key
	 LEFT OUTER JOIN 
	 meter_readings mr ON mr.meter_key = m._key AND mr.meter_timestamp = DATEADD(day, -1, pa.eod_datetime)
WHERE pa.eod_datetime = @eod and p._key = 'EDF94AB4-9845-4C15-8873-096D6AFD92A3'
--GROUP BY s._key, p._key, pa.eod_datetime


-- get the totalizer totals by skid for a time period 

--SELECT s.skid_description , mr.meter_timestamp, mr.meter_totalizer, m.meter_name, m._key
--FROM meter_readings mr INNER JOIN 
--	 meters m ON mr.meter_key = m._key INNER JOIN 
--	 skids s ON m.skid_key = s._key
--WHERE mr.meter_timestamp > '2020-04-22 08:50' AND s._key = 'E7D929C9-BCA9-4B8E-BF2E-46EDD9437045'
----GROUP BY s.skid_description , mr.meter_timestamp
--ORDER BY mr.meter_timestamp
