
-- ****************  check the totalizers for the skids and update start and stop times to match when actual meter readings changed ********************
DECLARE 
    @va_row_key uniqueidentifier,
	@valve_start datetime, 
	@valve_stop datetime, 
	@valve_skid uniqueidentifier,
	@new_start datetime , 
	@new_stop datetime, 
	@row_start int, 
	@row_end int,
	@time_start TIME

DECLARE @compare TABLE ([row_number] int, totalizer float, prev_totalizer float) 
DECLARE @debug bit = 1


 
DECLARE va_cursor CURSOR
FOR SELECT _key, valve_start, valve_stop, valve_skid_key
    FROM valve_activity
	WHERE valve_meter_check = 0;
 
OPEN va_cursor;
 
FETCH NEXT FROM va_cursor INTO 
   @va_row_key ,
	@valve_start , 
	@valve_stop , 
	@valve_skid
 
WHILE @@FETCH_STATUS = 0
    BEGIN

	-- reset all values 
	DELETE FROM @compare
	DELETE FROM  tmp_skid_totalizer_switch
	SET @new_start = NULL
	SET @new_stop = NULL
	SET @row_start = NULL
	SET @row_end = NULL
	SET @time_start = NULL
  --create new rows from a previous day 

		  -- grab all meter totalizer sums for that valve activity record 
		INSERT INTO tmp_skid_totalizer_switch([row] , skid_key , totalizer , [timestamp])
		SELECT ROW_NUMBER() OVER (ORDER BY s._key , mr.meter_timestamp),
			   s._key, SUM(mr.meter_totalizer) , mr.meter_timestamp
		FROM meter_readings mr INNER JOIN 
			 meters m ON mr.meter_key = m._key INNER JOIN 
			 skids s ON m.skid_key = s._key
		WHERE mr.meter_timestamp BETWEEN  DATEADD(MINUTE, -1, @valve_start) AND @valve_stop AND s._key = @valve_skid
		GROUP BY s.skid_description , mr.meter_timestamp,  s._key
		--select * from tmp_skid_totalizer_switch
		
		
		INSERT INTO @compare ([row_number], totalizer, prev_totalizer)
		SELECT [row], totalizer, LAG(totalizer, 1) OVER (ORDER BY [row]) as prev_totalizer
		FROM tmp_skid_totalizer_switch
		
		
		SET @time_start = CAST(@valve_start as time)
		--select @time_start
		IF (@time_start = '07:00')
		BEGIN
			SET @row_start = (select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number])
			--PRINT @row_start
			IF @debug = 1
				select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number]
		END
		ELSE
		BEGIN
			SET @row_start = (select top(1) [row_number] -1 FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number])
			--PRINT @row_start
			IF @debug = 1
				select top(1) [row_number] -1 FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number]
		END
		--SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_start
		
		SET @row_end = (select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number] desc)
		--PRINT @row_end
		IF @debug = 1
			select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number] desc
		--SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_end
		--SELECT @va_row_key

		SET @new_start =  (SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_start)
		IF @debug = 1
			SELECT @new_start
		
		SET @new_stop = (SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_end)
		IF @debug = 1
			SELECT @new_stop
		IF @debug = 1
			SELECT @va_row_key

		UPDATE valve_activity
		SET valve_start = @new_start
		WHERE _key = @va_row_key

		UPDATE valve_activity
		SET valve_stop = @new_stop
		WHERE _key = @va_row_key
		
		UPDATE valve_activity
		SET valve_meter_check = 1
		WHERE _key = @va_row_key



        FETCH NEXT FROM va_cursor INTO 
             @va_row_key ,
			 @valve_start , 
			 @valve_stop , 
			 @valve_skid;
    END;
 
CLOSE va_cursor;
 
DEALLOCATE va_cursor;
