

--truncate table dbo.valve_activity
--truncate table dbo.berth_activity
/*
1.  Bring in All valve data from historian 
2.  Create valve activity records 
3.  Create berth activity records from the valve activity 
	(min start time on all valves going to that berth = start time)
	(max stop time OR 7 am if loading not done on all valves going to that berth = stop time)


*/
	

 -- NEW VALVES NEED TO BE SEEDED IN THE VALVE ACTIVITY TABLE WITH A BEGINNING ENTRY 
truncate table tmp_activity_switch
 -- Check for any valves that have not had any activity and insert a starting row
 INSERT INTO valve_activity (valve_key, valve_start, valve_stop, valve_skid_batch_id, valve_meter_check)
 SELECT DISTINCT _key, DATEADD(year, -1, GETDATE()), DATEADD(year, -1, GETDATE()), 'seed row', 1
 FROM valves 
 WHERE _key NOT IN (SELECT valve_key FROM valve_activity)

 -- if the valve is open at 7 am, add 2 records, to close and open so it will split the batches for 7 am 
INSERT INTO valve_state (valve_key, [timestamp], [state])
SELECT DISTINCT vs.valve_key, DATETIMEFROMPARTS( DATEPART(YEAR, [timestamp]), DATEPART(MONTH, [timestamp]), DATEPART(DAY, [timestamp]), 7, 0, 2, 1) , 1
FROM valve_state vs  LEFT OUTER JOIN 
	 valve_activity va ON va.valve_key = vs.valve_key
WHERE [state] = 1 AND 
	  DATEPART(HOUR, [timestamp]) = 7 AND
	  DATEPART(MINUTE, [timestamp]) = 0 AND
	  DATEPART(SECOND, [timestamp]) = 0 
GROUP BY  vs.valve_key, vs.state, vs.timestamp
HAVING (vs.[timestamp] >= MAX(va.valve_stop))

INSERT INTO valve_state (valve_key, [timestamp], [state])
SELECT DISTINCT vs.valve_key, DATETIMEFROMPARTS( DATEPART(YEAR, [timestamp]), DATEPART(MONTH, [timestamp]), DATEPART(DAY, [timestamp]), 7, 0, 1, 1) , 0
FROM valve_state vs  LEFT OUTER JOIN 
	 valve_activity va ON va.valve_key = vs.valve_key
WHERE [state] = 1 AND 
	  DATEPART(HOUR, [timestamp]) = 7 AND
	  DATEPART(MINUTE, [timestamp]) = 0 AND
	  DATEPART(SECOND, [timestamp]) = 0 
GROUP BY  vs.valve_key, vs.state, vs.timestamp
HAVING (vs.[timestamp] >= MAX(va.valve_stop))


 -- Grab all valve activity since the last activity was logged for that valve
--DECLARE tmp_activity_switch Table ([row] int, valve_key uniqueidentifier, [state] bit, [timestamp] datetime)
INSERT INTO tmp_activity_switch([row] , valve_key , [state] , [timestamp])
(SELECT ROW_NUMBER() OVER (ORDER BY vs.valve_key, vs.[timestamp]), 
	    vs.valve_key,
		vs.[state], 
		vs.[timestamp]
FROM valve_state vs  LEFT OUTER JOIN 
	 valve_activity va ON va.valve_key = vs.valve_key
GROUP BY  vs.valve_key, vs.state, vs.timestamp
HAVING (vs.[timestamp] >= MAX(va.valve_stop)) --or (vs.[timestamp] IS NULL)
)


--DELETE FROM tmp_activity_switch t1
--WHERE EXISTS (SELECT 1 from tmp_activity_switch t2
--			  WHERE t1.[state] = t2.[state]
--			  AND t1.ROW_NUMBER()

-- delete the rows where the valve state did not change from the record before it 
 DELETE FROM tmp_activity_switch
 WHERE [row]  IN ( SELECT CASE WHEN [state] = LAG([state]) OVER (ORDER BY [row]) THEN [row] END r FROM tmp_activity_switch);
 --= (select [state] from tmp_activity_switch WHERE [row] = [row]-1)
 --select * from tmp_activity_switch

 -- insert a new activity record for each valve's start and stop time 
;WITH ValveActivityRowNum (RowNumber, valve_key, [state], [timestamp])
AS
(SELECT ROW_NUMBER() OVER (ORDER BY valve_key, [timestamp]), 
	    valve_key,
		[state], 
		[timestamp]
 FROM tmp_activity_switch)
 INSERT INTO valve_activity(valve_key, valve_start, valve_stop)
 SELECT v_on.valve_key, v_on.[timestamp] as start_datetime, v_off.[timestamp] as end_datetime  --, DATEDIFF(minute, v_on.[timestamp], v_off.[timestamp])
 FROM (SELECT * FROM ValveActivityRowNum WHERE [state] = 1) v_on LEFT OUTER JOIN
	  (SELECT * FROM ValveActivityRowNum WHERE [state] = 0) v_off ON v_off.RowNumber = v_on.RowNumber + 1
																	 AND v_off.valve_key = v_on.valve_key


-- delete from valve activity where the start is 7 am today or later-- 
--SELECT *
DELETE
FROM valve_activity 
WHERE valve_start >= DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)


---- IF THERE IS NO STOP TIME YET, END IT AT 6:59 AM 
--UPDATE valve_activity
--SET valve_stop = CASE WHEN cast( valve_start as time) >= cast('07:00 AM' as TIME) 
--	        THEN DATEADD(day, 1, SMALLDATETIMEFROMPARTS(YEAR(valve_start), MONTH(valve_start), DAY(valve_start), 07, 00)) 
--			ELSE SMALLDATETIMEFROMPARTS(YEAR(valve_start), MONTH(valve_start), DAY(valve_start), 07, 00) END
--WHERE valve_stop IS NULL 

-- UPDATE THE BERTH 

UPDATE va
SET valve_berth_key = vb.berth_key
FROM valve_activity va INNER JOIN 
	 valve_to_berth vb ON va.valve_key = vb.valve_key
WHERE va.valve_berth_key IS NULL


-- UPDATE THE SKID 

UPDATE va
SET valve_skid_key = v.valve_skid_key
FROM valve_activity va INNER JOIN 
	 valves v ON va.valve_key = v._key
WHERE va.valve_skid_key IS NULL

-- reset those times we altered to create an EOD record
UPDATE va
SET valve_start = DATETIMEFROMPARTS( DATEPART(YEAR, valve_start), DATEPART(MONTH, valve_start), DATEPART(DAY, valve_start), 7, 0, 0, 0)
FROM valve_activity va
WHERE DATEPART(s, valve_start) = 02

-- reset those times we altered to create an EOD record
UPDATE va
SET valve_stop = DATETIMEFROMPARTS( DATEPART(YEAR, valve_stop), DATEPART(MONTH, valve_stop), DATEPART(DAY, valve_stop), 7, 0, 0, 0)
FROM valve_activity va
WHERE DATEPART(s, valve_stop) = 01


-- UPDATE THE GSV for the skid
UPDATE va
SET valve_bbl = ISNULL(en.stop_totalizer,0) - ISNULL(st.start_totalizer, 0)
FROM (	 select va._key,  SUM(mr.meter_totalizer) as start_totalizer --, m.meter_name, va.valve_key, va.valve_start, va.valve_stop
		 from valve_activity va  INNER JOIN 
			  valves v ON va.valve_key = v._key INNER JOIN 
			  meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
			  meter_readings mr ON mr.meter_key = m._key
		 WHERE --va._key = 'F2018298-A7F2-4022-8015-FAB65C9774A4'	AND	   
		    mr.meter_timestamp = va.valve_start
		 GROUP BY va._key) st  INNER JOIN 
	(	 select va._key,  SUM(mr.meter_totalizer) as stop_totalizer --, m.meter_name, va.valve_key, va.valve_start, va.valve_stop
	from valve_activity va  INNER JOIN 
		valves v ON va.valve_key = v._key INNER JOIN 
		meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
		meter_readings mr ON mr.meter_key = m._key
	WHERE --va._key = 'F2018298-A7F2-4022-8015-FAB65C9774A4'	AND	   
	 mr.meter_timestamp = va.valve_stop
	GROUP BY va._key) en ON en._key = st._key  INNER JOIN 
	valve_activity va ON va._key = en._key
	WHERE va.valve_bbl IS NULL

-- Get the batch ID for the skid
-- update the batch is's for the valve activities

--Update the batch ID *************************************************************************
UPDATE va
SET valve_skid_batch_id = sb.skid_batch_id
FROM valve_activity va LEFT OUTER JOIN 
	 skid_batch_ids sb ON va.valve_skid_key = sb.skid_key
WHERE va.valve_stop = sb.[timestamp] AND valve_skid_batch_id IS NULL

-- create a unique id if there is none 
UPDATE va
SET valve_skid_batch_id = CONVERT(varchar(11), va.valve_start) + b.berth_short_name
FROM valve_activity va INNER JOIN 
	 berths b ON va.valve_berth_key = b._key
WHERE valve_skid_batch_id = ''
	
-- delete records where start time is today at 7 am 
--DELETE from valve_activity 
--WHERE valve_start =  DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)


--- keep only records with greater than 0 bbls
DELETE from valve_activity 
WHERE valve_bbl   < 200

-- UPDATE THE start and stop times by totalizer change 


-- ****************  check the totalizers for the skids and update start and stop times to match when actual meter readings changed ********************
DECLARE 
    @va_row_key uniqueidentifier,
	@valve_start datetime, 
	@valve_stop datetime, 
	@valve_skid uniqueidentifier,
	@new_start datetime , 
	@new_stop datetime, 
	@row_start int, 
	@row_end int,
	@time_start TIME

DECLARE @compare TABLE ([row_number] int, totalizer float, prev_totalizer float) 
DECLARE @debug bit = 0


 
DECLARE va_cursor CURSOR
FOR SELECT _key, valve_start, valve_stop, valve_skid_key
    FROM valve_activity
	WHERE valve_meter_check = 0;
 
OPEN va_cursor;
 
FETCH NEXT FROM va_cursor INTO 
   @va_row_key ,
	@valve_start , 
	@valve_stop , 
	@valve_skid
 
WHILE @@FETCH_STATUS = 0
    BEGIN

	-- reset all values 
	DELETE FROM @compare
	DELETE FROM  tmp_skid_totalizer_switch
	SET @new_start = NULL
	SET @new_stop = NULL
	SET @row_start = NULL
	SET @row_end = NULL
	SET @time_start = NULL
  --create new rows from a previous day 

		  -- grab all meter totalizer sums for that valve activity record 
		INSERT INTO tmp_skid_totalizer_switch([row] , skid_key , totalizer , [timestamp])
		SELECT ROW_NUMBER() OVER (ORDER BY s._key , mr.meter_timestamp),
			   s._key, SUM(mr.meter_totalizer) , mr.meter_timestamp
		FROM meter_readings mr INNER JOIN 
			 meters m ON mr.meter_key = m._key INNER JOIN 
			 skids s ON m.skid_key = s._key
		WHERE mr.meter_timestamp BETWEEN  DATEADD(MINUTE, -1, @valve_start) AND @valve_stop AND s._key = @valve_skid
		GROUP BY s.skid_description , mr.meter_timestamp,  s._key
		--select * from tmp_skid_totalizer_switch
		
		
		INSERT INTO @compare ([row_number], totalizer, prev_totalizer)
		SELECT [row], totalizer, LAG(totalizer, 1) OVER (ORDER BY [row]) as prev_totalizer
		FROM tmp_skid_totalizer_switch
		
		
		SET @time_start = CAST(@valve_start as time)
		--select @time_start
		IF (@time_start = '07:00')
		BEGIN
			SET @row_start = (select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number])
			--PRINT @row_start
			IF @debug = 1
				select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number]
		END
		ELSE
		BEGIN
			SET @row_start = (select top(1) [row_number] -1 FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number])
			--PRINT @row_start
			IF @debug = 1
				select top(1) [row_number] -1 FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number]
		END
		--SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_start
		
		SET @row_end = (select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number] desc)
		--PRINT @row_end
		IF @debug = 1
			select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number] desc
		--SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_end
		--SELECT @va_row_key

		SET @new_start =  (SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_start)
		IF @debug = 1
			SELECT @new_start
		
		SET @new_stop = (SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_end)
		IF @debug = 1
			SELECT @new_stop
		IF @debug = 1
			SELECT @va_row_key

		UPDATE valve_activity
		SET valve_start = @new_start
		WHERE _key = @va_row_key

		UPDATE valve_activity
		SET valve_stop = @new_stop
		WHERE _key = @va_row_key
		
		UPDATE valve_activity
		SET valve_meter_check = 1
		WHERE _key = @va_row_key



        FETCH NEXT FROM va_cursor INTO 
             @va_row_key ,
			 @valve_start , 
			 @valve_stop , 
			 @valve_skid;
    END;
 
CLOSE va_cursor;
 
DEALLOCATE va_cursor;