--DECLARE @check TABLE (_va_row_key uniqueidentifier, valve_start datetime, valve_stop datetime, valve_skid uniqueidentifier) 


-- ****************  check the totalizers for the skids and update start and stop times to match when actual meter readings changed ********************
DECLARE 
    @va_row_key uniqueidentifier,
	@valve_start datetime, 
	@valve_stop datetime, 
	@valve_skid uniqueidentifier
 
DECLARE va_cursor CURSOR
FOR SELECT _key, valve_start, valve_stop, valve_skid_key
    FROM valve_activity
	WHERE valve_meter_check = 0;
 
OPEN va_cursor;
 
FETCH NEXT FROM va_cursor INTO 
   @va_row_key ,
	@valve_start , 
	@valve_stop , 
	@valve_skid
 
WHILE @@FETCH_STATUS = 0
    BEGIN

	TRUNCATE TABLE tmp_skid_totalizer_switch
  --create new rows from a previous day 

		  -- grab all meter totalizer sums for that valve activity record 
		INSERT INTO tmp_skid_totalizer_switch([row] , skid_key , totalizer , [timestamp])
		SELECT ROW_NUMBER() OVER (ORDER BY s._key , mr.meter_timestamp),
			   s._key, SUM(mr.meter_totalizer) , mr.meter_timestamp
		FROM meter_readings mr INNER JOIN 
			 meters m ON mr.meter_key = m._key INNER JOIN 
			 skids s ON m.skid_key = s._key
		WHERE mr.meter_timestamp BETWEEN  DATEADD(MINUTE, -1, @valve_start) AND @valve_stop AND s._key = @valve_skid
		GROUP BY s.skid_description , mr.meter_timestamp,  s._key
		select * from tmp_skid_totalizer_switch
		
		DECLARE @compare TABLE ([row_number] int, totalizer float, prev_totalizer float) 
		INSERT INTO @compare ([row_number], totalizer, prev_totalizer)
		SELECT [row], totalizer, LAG(totalizer, 1) OVER (ORDER BY [row]) as prev_totalizer
		FROM tmp_skid_totalizer_switch

		DECLARE @row_start int, @row_end int

		SET @row_start = (select top(1) [row_number] -1 FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number])
		PRINT @row_start

		
		SET @row_end = (select top(1) [row_number]  FROM @compare WHERE totalizer <> prev_totalizer ORDER BY [row_number] desc)
		PRINT @row_end

		UPDATE valve_activity
		SET valve_start = (SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_start)
		WHERE _key = @va_row_key
		
		UPDATE valve_activity
		SET valve_stop = (SELECT [timestamp] FROM tmp_skid_totalizer_switch WHERE [row] = @row_end)
		WHERE _key = @va_row_key
		
		UPDATE valve_activity
		SET valve_meter_check = 1
		WHERE _key = @va_row_key



        FETCH NEXT FROM va_cursor INTO 
             @va_row_key ,
			 @valve_start , 
			 @valve_stop , 
			 @valve_skid;
    END;
 
CLOSE va_cursor;
 
DEALLOCATE va_cursor;

--SELECT * FROM valve_activity
--WHERE valve_meter_check = 0
---- get the total totalizer from meter reading by skid 

SELECT s.skid_description , mr.meter_timestamp, SUM(mr.meter_totalizer)
FROM meter_readings mr INNER JOIN 
	 meters m ON mr.meter_key = m._key INNER JOIN 
	 skids s ON m.skid_key = s._key
WHERE mr.meter_timestamp > '4/14/2020' AND s.skid_description = 'SK-1340A'
GROUP BY s.skid_description , mr.meter_timestamp
ORDER BY mr.meter_timestamp


--SELECT ROW_NUMBER() OVER (ORDER BY s._key , mr.meter_timestamp),
--	   s.skid_description , mr.meter_timestamp, SUM(mr.meter_totalizer)
--FROM meter_readings mr INNER JOIN 
--	 meters m ON mr.meter_key = m._key INNER JOIN 
--	 skids s ON m.skid_key = s._key
--WHERE mr.meter_timestamp > '4/14/2020' AND s.skid_description = 'SK-1340A'
--GROUP BY s.skid_description , mr.meter_timestamp,  s._key


--INSERT INTO tmp_skid_totalizer_switch([row] , skid_key , totalizer , [timestamp])
--SELECT ROW_NUMBER() OVER (ORDER BY s._key , mr.meter_timestamp),
--	   s.skid_description , mr.meter_timestamp, SUM(mr.meter_totalizer)
--FROM meter_readings mr INNER JOIN 
--	 meters m ON mr.meter_key = m._key INNER JOIN 
--	 skids s ON m.skid_key = s._key
--WHERE mr.meter_timestamp > '4/14/2020' AND s.skid_description = 'SK-1340A'
--GROUP BY s.skid_description , mr.meter_timestamp,  s._key





 -- Grab all valve activity since the last activity was logged for that valve
--DECLARE tmp_activity_switch Table ([row] int, valve_key uniqueidentifier, [state] bit, [timestamp] datetime)
--INSERT INTO tmp_skid_totalizer_switch([row] , skid_key , totalizer , [timestamp])
--(SELECT ROW_NUMBER() OVER (ORDER BY vs.skid, vs.[timestamp]), 
--	    vs.valve_key,
--		vs.[state], 
--		vs.[timestamp]
--FROM valve_state vs  LEFT OUTER JOIN 
--	 valve_activity va ON va.valve_key = vs.valve_key
--GROUP BY  vs.valve_key, vs.state, vs.timestamp
--HAVING (vs.[timestamp] >= MAX(va.valve_stop)) --or (vs.[timestamp] IS NULL)
--)


----DELETE FROM tmp_activity_switch t1
----WHERE EXISTS (SELECT 1 from tmp_activity_switch t2
----			  WHERE t1.[state] = t2.[state]
----			  AND t1.ROW_NUMBER()

---- delete the rows where the valve state did not change from the record before it 
-- DELETE FROM tmp_activity_switch
-- WHERE [row]  IN ( SELECT CASE WHEN [state] = LAG([state]) OVER (ORDER BY [row]) THEN [row] END r FROM tmp_activity_switch);
-- --= (select [state] from tmp_activity_switch WHERE [row] = [row]-1)
-- select * from tmp_activity_switch

-- -- insert a new activity record for each valve's start and stop time 
--;WITH ValveActivityRowNum (RowNumber, valve_key, [state], [timestamp])
--AS
--(SELECT ROW_NUMBER() OVER (ORDER BY valve_key, [timestamp]), 
--	    valve_key,
--		[state], 
--		[timestamp]
-- FROM tmp_activity_switch)
-- INSERT INTO valve_activity(valve_key, valve_start, valve_stop)
-- SELECT v_on.valve_key, v_on.[timestamp] as start_datetime, v_off.[timestamp] as end_datetime  --, DATEDIFF(minute, v_on.[timestamp], v_off.[timestamp])
-- FROM (SELECT * FROM ValveActivityRowNum WHERE [state] = 1) v_on LEFT OUTER JOIN
--	  (SELECT * FROM ValveActivityRowNum WHERE [state] = 0) v_off ON v_off.RowNumber = v_on.RowNumber + 1
	