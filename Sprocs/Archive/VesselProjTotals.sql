declare 	@month int = 6,
	@year int = 2020 
	,@product_key uniqueidentifier =  '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0'-- WTI
	,@entity_key uniqueidentifier = 'd25051f3-4888-4bfb-89fd-4e1449ac0fba' -- vitol
	,@site_key uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'


--UPDATE r
--SET total_berth_nominated = projected_volume
--FROM @results r INNER JOIN 
---- qty, eod, (grouping on eod ) 
--(
select SUM(projected_volume), eod
FROM 
(select vp.projected_volume as projected_volume,  dbo.get_eod_datetime( DATEADD(day, vp.day_number - 1, st.syn_vessel_eta)) as eod --DATEADD(day, vp.day_number - 1, st.syn_vessel_eta) as eta,
FROM vessel_projected_volumes vp INNER JOIN 
	synthesis_tickets st ON vp.synthesis_key = st._key INNER JOIN 
	 entities e ON st.syn_shipper_id = e.external_id INNER JOIN 
	 products p ON p.product_external_id = st.syn_product_code
WHERE syn_vessel_lay_status <> 'REJECTED' 
	  and (e._key= @entity_key OR @entity_key IS NULL)
	  AND (p._key = @product_key OR @product_key IS NULL)
--GROUP BY dbo.get_eod_datetime(syn_vessel_eta)
) daily
GROUP BY eod
order by eod

--) syn ON r.eod_datetime = syn.eod