USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_hartree_manual_tank_gauge_update]    Script Date: 4/20/2021 11:48:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/18/2021
-- Description:		Update selected business entities record with new user input details.

/*EXEC mgn_business_entities_update 
	@row = '00284D50-7227-4FA8-8DB7-0DFA462CDE50',
	@name = 'TestEntityAdd',
	@short_name = 'Test123',
	@type = 0,
	@active = 1,
	@transportation_mode_key = 'a9dcf071-7216-4e54-91e0-369257843c34',
	@city = 'Miami',
	@state_key = 'cb8f27ce-9921-402e-8137-ea33a9616f9d',
	@external_code = '',
	@fein = '',
	@user = 'candice'
	
	// 
	*/
-- =============================================
ALTER           PROCEDURE [dbo].[mgn_hartree_manual_tank_gauge_update]

	@day int, --convert to datetime
	@month int,
	@year int,
	@hour int,
	@min int,
	@sec int,
	@tank varchar(100), --compare to pi_name column in tanks table
	@auto_level float,
	@manual_level float
	--@difference float
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from entities
--Where _key = @row

declare @date datetime = Datetimefromparts(@year,@month,@day,@hour,@min,@sec,0)


INSERT INTO [dbo].[tank_gauge_manual]
           ([tgm_tank_key]
		   ,[tgm_gauge_date]
		   ,[tgm_auto_level]
		   ,[tgm_manual_level]
		   ,[tgm_difference])
VALUES	   
           ((select _key from tanks where tank_pi_name = @tank)
		   ,@date
		   ,@auto_level
		   ,@manual_level
		   ,@auto_level - @manual_level)

END

