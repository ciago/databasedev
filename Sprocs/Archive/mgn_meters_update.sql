USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_meters_update]    Script Date: 4/20/2021 3:44:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/3/2021
-- Description:		Update selected meters record with new user input details.

/*EXEC mgn_meters_update 
	@row = '39E3B653-1CF8-4408-A643-CD975BB03AF5',
	@meter_name = 'TestInsert123',
	@meter_description = 'TestInsert',
	@locations_key = '8F7C16A0-D4CC-43F2-A966-247217A6E928',
	@max_allowable_flow = '123',
	@max_allowable_pressure = '123',	
	@meter_factor = '123',
	@active = 1,
	@user = 'candice'

	// 
	*/
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_meters_update]

	@row uniqueidentifier,
	@meter_name nvarchar(50),
	@meter_description nvarchar(50),
	@locations_key uniqueidentifier,
	@max_allowable_flow float,
	@max_allowable_pressure float,
	@active bit,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from meters
--Where _key = @row

IF(@row IN (SELECT _key from meters))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM meters WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE meters

SET		meter_name = @meter_name,
		meter_name_2 = @meter_description,
		locations_key = @locations_key,
		max_allowable_flow = @max_allowable_flow,
		max_allowable_pressure = @max_allowable_pressure,
		active = @active,
		record_info = @json

WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[meters]
           ([_key]
			,[meter_name]
			,[meter_name_2]
			,[locations_key]
			,[max_allowable_flow]
			,[max_allowable_pressure]
			,[active]
			,[record_info])
     VALUES
           (@row
		   	,@meter_name
			,@meter_description
			,@locations_key
			,@max_allowable_flow
			,@max_allowable_pressure
			,@active
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
