--SELECT i.product_grade, p._key
UPDATE i
SET product_key =  p._key
FROM import_schedule i INNER JOIN products p ON i.product_grade = p.product_code
WHERE i.product_key IS NULL