USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[import_synthesis_vessel_orders]    Script Date: 3/25/2021 3:09:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER   PROCEDURE [dbo].[import_synthesis_vessel_orders]

	
AS
/* 
EXEC daily_berth_activity_update  @eod = DATETIMEFROMPARTS( 2020, 05, 29, 7, 0, 0, 0)
	
*/
BEGIN

INSERT INTO [dbo].[synthesis_tickets]
           ([_key]
           ,[syn_vessel_movement_id]
           ,[syn_ticket_id]
           ,[syn_syn_customer_order]
           ,[syn_vessel_trip_id]
           ,[syn_customer_ref]
           ,[syn_vessel_eta]
           ,[syn_shipper_short_name]
           ,[syn_shipper_id]
           ,[syn_nominated_quantity]
           ,[syn_vessel_name]
           ,[syn_vessel_id]
           ,[syn_vessel_loa]
           ,[syn_vessel_type_description]
           ,[syn_vessel_description]
           ,[syn_product_code]
           ,[syn_product_id]
           ,[syn_vessel_lay_status]
           ,[syn_dock_id]
           ,[syn_dock_name]
           ,[syn_total_synthesis_calculated_volume]
           ,[syn_total_synthesis_net_volume]
           ,[syn_ticket_start_datetime]
           ,[syn_ticket_end_datetime]
           ,[syn_ticket_type]
           ,[syn_ticket_last_update_date]
           ,[syn_movement_last_update_date]
		   ,syn_agent_id)
-- ticket status , 1 = open, 2=closed 3=deleted
 -- Vessel orders
 SELECT newid(), 
	mov.[VESSEL_MOVEMENT_ID],
	tk.[SA_TICKET_ID],
    mov.CUSTOMER_ORDER_NUMBER,
    vt.VESSEL_TRIP_ID,
	mov.CUSTOMER_REF,
    lay.EST_ARRIVAL_TIME AS [ETA],
	be.SHORT_NAME,
    CASE WHEN mov.CUSTOMER_ID IS NOT NULL THEN mov.CUSTOMER_ID ELSE tah.STOCKHOLDER_ID END AS [SHIPPER_ID],
    mov.NOMINATED_QTY AS [TOTAL_QUANTITY],
    v.DISPLAY_VALUE AS [VESSEL_NAME],
	v.[VESSEL_ID] AS [VESSEL_ID],
	v.LOA,
	vty.[VESSEL_TYPE_DESC],
	v.[VESSEL_DESC],
	prod.[PRODUCT_CODE] as MovementProduct,
	mov.[MATERIAL_ID] as MovementProductID,
    lk.ITEM_VALUE as [status],
    lay.MARINE_DOCK_ID,
    lay.MARINE_DOCK_NAME, 
    tk.TOTAL_CALCULATED_VOLUME, 
    tk.NET_VOL, 
    tk.SHIPMENT_START_DATE, 
    tk.SHIPMENT_DATE, 
    tk.TICKET_TYPE,
    tk.[LAST_UPDATED_DATE],
    mov.[LAST_UPDATED_DATE], 
	lay.AGENCY_ID
FROM EMERSON.Synthesis_Test_02242020.dbo.VESSEL_TRIP vt 
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL v ON V.VESSEL_ID = VT.VESSEL_ID
    JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_LAY lay ON lay.VESSEL_TRIP_ID = vt.VESSEL_TRIP_ID
    JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_MOVEMENT mov ON mov.VESSEL_LAY_ID = lay.VESSEL_LAY_ID  
    JOIN EMERSON.Synthesis_Test_02242020.dbo.TERMINAL_ACTIVITY_HEADER tah ON (tah.TERMINAL_ACTIVITY_HEADER_ID = VT.HEADER_ID OR tah.TERMINAL_ACTIVITY_HEADER_ID = mov.HEADER_ID)
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.CONTRACT_CHARGE_SCHEDULE cch ON cch.CONTRACT_CHARGE_SCHEDULE_ID = tah.CONTRACT_CHARGE_SCHEDULE_ID
	LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.MASTER_BUSINESS_ENTITY be ON be.MASTER_BE_ID = tah.STOCKHOLDER_ID 
	LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_TYPE  vty ON v.[VESSEL_TYPE_ID] = vty.[VESSEL_TYPE_ID] 
	LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.[MASTER_PRODUCT] prod ON prod.[MASTER_PRODUCT_ID] = mov.[MATERIAL_ID] 
	LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.[VESSEL_MOVEMENT_STATUS]  movstat ON movstat.[VESSEL_MOVEMENT_STATUS_ID]  = mov.[VESSEL_MOVEMENT_STATUS] 
    LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.[VESSEL_TRIP_STATUS] vstat ON vstat.[VESSEL_TRIP_STATUS_ID] = vt.[VESSEL_TRIP_STATUS]
    LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.[TERMINAL_ACTIVITY_STATUS] tahstat ON tahstat.[ACTIVITY_STATUS_ID] = tah.[TERMINAL_ACTIVITY_HEADER_STATUS] 
    LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.OPENSEAL_LKUP_ITEM lk ON lk.ITEM_KEY = mov.VESSEL_MOVEMENT_STATUS 
    LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.TICKET tk ON tk.MOVEMENT_ID = vt.VESSEL_TRIP_ID  
    LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.TICKET_STATUS tkstat ON tkstat.TICKET_STATUS_ID = tk.TICKET_STATUS 
WHERE lk.LKUP_ID = 1029 
	  AND mov.[VESSEL_MOVEMENT_ID] NOT IN (SELECT [syn_vessel_movement_id] FROM [dbo].[synthesis_tickets])
	  AND lay.EST_ARRIVAL_TIME > DATEADD(MONTH, -2, GETDATE())


-- lookup item 1029, item key macthes status in vessel order 

-- ticket status , 1 = open, 2=closed 3=deleted



UPDATE s
   SET  syn_ticket_id = tk.[SA_TICKET_ID]
	  ,syn_syn_customer_order = mov.CUSTOMER_ORDER_NUMBER
      ,[syn_customer_ref] = mov.CUSTOMER_REF
      ,[syn_vessel_eta] = lay.EST_ARRIVAL_TIME
      ,[syn_shipper_short_name] = be.SHORT_NAME
      ,[syn_shipper_id] =  CASE WHEN mov.CUSTOMER_ID IS NOT NULL THEN mov.CUSTOMER_ID ELSE tah.STOCKHOLDER_ID END
      ,[syn_nominated_quantity] = mov.NOMINATED_QTY
      ,[syn_vessel_name] = v.DISPLAY_VALUE
      ,[syn_vessel_id] = v.[VESSEL_ID]
      ,[syn_vessel_loa] = v.LOA
      ,[syn_vessel_type_description] = vty.[VESSEL_TYPE_DESC]
      ,[syn_vessel_description] = v.[VESSEL_DESC]
      ,[syn_product_code] = prod.[PRODUCT_CODE]
      ,[syn_product_id] = mov.[MATERIAL_ID]
      ,[syn_vessel_lay_status] = lk.ITEM_VALUE
      ,[syn_dock_id] = lay.MARINE_DOCK_ID
      ,[syn_dock_name] = lay.MARINE_DOCK_NAME
      ,[syn_total_synthesis_calculated_volume] = tk.TOTAL_CALCULATED_VOLUME
      ,[syn_total_synthesis_net_volume] = tk.NET_VOL
      ,[syn_ticket_start_datetime] =  tk.SHIPMENT_START_DATE
      ,[syn_ticket_end_datetime] = tk.SHIPMENT_DATE
      ,[syn_ticket_type] = tk.TICKET_TYPE
      ,[syn_ticket_last_update_date] = tk.[LAST_UPDATED_DATE]
      ,[syn_movement_last_update_date] = mov.[LAST_UPDATED_DATE]
	  , syn_agent_id = lay.AGENCY_ID
FROM synthesis_tickets s 
	INNER JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_TRIP vt ON vt.VESSEL_TRIP_ID = s.syn_vessel_trip_id
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL v ON V.VESSEL_ID = VT.VESSEL_ID
    JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_LAY lay ON lay.VESSEL_TRIP_ID = vt.VESSEL_TRIP_ID
    JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_MOVEMENT mov ON mov.VESSEL_LAY_ID = lay.VESSEL_LAY_ID  
    JOIN EMERSON.Synthesis_Test_02242020.dbo.TERMINAL_ACTIVITY_HEADER tah ON (tah.TERMINAL_ACTIVITY_HEADER_ID = VT.HEADER_ID OR tah.TERMINAL_ACTIVITY_HEADER_ID = mov.HEADER_ID)
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.CONTRACT_CHARGE_SCHEDULE cch ON cch.CONTRACT_CHARGE_SCHEDULE_ID = tah.CONTRACT_CHARGE_SCHEDULE_ID
	LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.MASTER_BUSINESS_ENTITY be ON be.MASTER_BE_ID = tah.STOCKHOLDER_ID 
	LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_TYPE  vty ON v.[VESSEL_TYPE_ID] = vty.[VESSEL_TYPE_ID] 
	LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.[MASTER_PRODUCT] prod ON prod.[MASTER_PRODUCT_ID] = mov.[MATERIAL_ID] 
	LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.[VESSEL_MOVEMENT_STATUS]  movstat ON movstat.[VESSEL_MOVEMENT_STATUS_ID]  = mov.[VESSEL_MOVEMENT_STATUS] 
    LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.[VESSEL_TRIP_STATUS] vstat ON vstat.[VESSEL_TRIP_STATUS_ID] = vt.[VESSEL_TRIP_STATUS]
    LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.[TERMINAL_ACTIVITY_STATUS] tahstat ON tahstat.[ACTIVITY_STATUS_ID] = tah.[TERMINAL_ACTIVITY_HEADER_STATUS] 
    LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.OPENSEAL_LKUP_ITEM lk ON lk.ITEM_KEY = mov.VESSEL_MOVEMENT_STATUS 
    LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.TICKET tk ON tk.MOVEMENT_ID = vt.VESSEL_TRIP_ID  
    LEFT OUTER JOIN EMERSON.Synthesis_Test_02242020.dbo.TICKET_STATUS tkstat ON tkstat.TICKET_STATUS_ID = tk.TICKET_STATUS 
WHERE lk.LKUP_ID = 1029 
	  AND mov.[VESSEL_MOVEMENT_ID] =  [syn_vessel_movement_id] 
	  AND lay.EST_ARRIVAL_TIME > DATEADD(MONTH, -2, GETDATE())
	  --AND (tk.[LAST_UPDATED_DATE] <> s.syn_ticket_last_update_date 
			--OR mov.[LAST_UPDATED_DATE] <> s.syn_movement_last_update_date
			--OR s.syn_ticket_last_update_date  IS NULL
			--OR s.syn_movement_last_update_date IS NULL)











INSERT INTO [dbo].[synthesis_tank_assign]
           ([_key]
           ,[syn_vessel_movement_id]
           ,[syn_ticket_id]
           ,[syn_port_movement_id]
		   ,syn_vessel_trip_id
           ,[syn_tank_location_id]
           ,[syn_port_movement_last_update])
 SELECT  newid(),
    mov.[VESSEL_MOVEMENT_ID],
	tk.[SA_TICKET_ID],
	pmda.[PORT_MOVEMENT_DEVICE_ASSIGN_ID],
	 vt.VESSEL_TRIP_ID,
	loc.LOCATION_CODE as Tank_location_id,
	pmda.[LAST_UPDATED_DATE]
FROM
	
    EMERSON.Synthesis_Test_02242020.dbo.VESSEL_TRIP vt  
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL v ON V.VESSEL_ID = VT.VESSEL_ID
    JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_LAY lay ON lay.VESSEL_TRIP_ID = vt.VESSEL_TRIP_ID
    JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_MOVEMENT mov ON mov.VESSEL_LAY_ID = lay.VESSEL_LAY_ID  
    JOIN EMERSON.Synthesis_Test_02242020.dbo.TERMINAL_ACTIVITY_HEADER tah ON (tah.TERMINAL_ACTIVITY_HEADER_ID = VT.HEADER_ID OR tah.TERMINAL_ACTIVITY_HEADER_ID = mov.HEADER_ID)
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.CONTRACT_CHARGE_SCHEDULE cch ON cch.CONTRACT_CHARGE_SCHEDULE_ID = tah.CONTRACT_CHARGE_SCHEDULE_ID
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.PORT_MOVEMENT_DEVICE_ASSIGN pmda ON pmda.VESSEL_MOVEMENT_ID = mov.VESSEL_MOVEMENT_ID
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.MASTER_DEVICE md ON md.MASTER_DEVICE_ID = pmda.DEVICE_ID AND md.MASTER_DEVICE_TYPE_ID = 4
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.TANK t ON t.MASTER_DEVICE_ID = md.MASTER_DEVICE_ID
	LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.MASTER_BUSINESS_ENTITY be ON be.MASTER_BE_ID = tah.STOCKHOLDER_ID 
	LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_TYPE  vty ON v.[VESSEL_TYPE_ID] = vty.[VESSEL_TYPE_ID]
	LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.MASTER_BUSINESS_ENTITY tankbe ON tankbe.MASTER_BE_ID = t.CURR_STOCKHOLDER_ID  
	LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.[MASTER_PRODUCT] prod ON prod.[MASTER_PRODUCT_ID] = mov.[MATERIAL_ID] 
	LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.[MASTER_PRODUCT] tankprod ON tankprod.[MASTER_PRODUCT_ID] = t.MASTER_PRODUCT_ID 
	LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.[VESSEL_MOVEMENT_STATUS]  movstat ON movstat.[VESSEL_MOVEMENT_STATUS_ID]  = mov.[VESSEL_MOVEMENT_STATUS]
    LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.[VESSEL_TRIP_STATUS] vstat ON vstat.[VESSEL_TRIP_STATUS_ID] = vt.[VESSEL_TRIP_STATUS]
    LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.[TERMINAL_ACTIVITY_STATUS] tahstat ON tahstat.[ACTIVITY_STATUS_ID] = tah.[TERMINAL_ACTIVITY_HEADER_STATUS] 
    LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.OPENSEAL_LKUP_ITEM lk ON lk.ITEM_KEY = mov.VESSEL_MOVEMENT_STATUS
    LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.TICKET tk ON tk.MOVEMENT_ID = vt.VESSEL_TRIP_ID  
    LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.TICKET_STATUS tkstat ON tkstat.TICKET_STATUS_ID = tk.TICKET_STATUS 
	LEFT OUTER JOIN 
	(SELECT * FROM OPENQUERY(EMERSON, 'select * FROM Synthesis_Test_02242020.dbo.MASTER_LOCATION')) loc ON md.MASTER_LOCATION_ID = loc.MASTER_LOCATION_ID
WHERE lk.LKUP_ID = 1029 
AND pmda.[PORT_MOVEMENT_DEVICE_ASSIGN_ID] NOT IN (SELECT syn_port_movement_id FROM [dbo].synthesis_tank_assign WHERE syn_port_movement_id IS NOT NULL)
--order by pmda.[PORT_MOVEMENT_DEVICE_ASSIGN_ID]








UPDATE [dbo].[synthesis_tank_assign]
   SET [syn_vessel_movement_id] = mov.[VESSEL_MOVEMENT_ID]
      ,[syn_ticket_id] =tk.[SA_TICKET_ID]
      ,[syn_vessel_trip_id] =  vt.VESSEL_TRIP_ID
      ,[syn_tank_location_id] = loc.LOCATION_CODE
      ,[syn_port_movement_last_update] = pmda.[LAST_UPDATED_DATE]

	 FROM
	synthesis_tank_assign s inner join 
    EMERSON.Synthesis_Test_02242020.dbo.VESSEL_TRIP vt  ON vt.VESSEL_TRIP_ID = s.syn_vessel_trip_id
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL v ON V.VESSEL_ID = VT.VESSEL_ID
    JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_LAY lay ON lay.VESSEL_TRIP_ID = vt.VESSEL_TRIP_ID
    JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_MOVEMENT mov ON mov.VESSEL_LAY_ID = lay.VESSEL_LAY_ID  
    JOIN EMERSON.Synthesis_Test_02242020.dbo.TERMINAL_ACTIVITY_HEADER tah ON (tah.TERMINAL_ACTIVITY_HEADER_ID = VT.HEADER_ID OR tah.TERMINAL_ACTIVITY_HEADER_ID = mov.HEADER_ID)
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.CONTRACT_CHARGE_SCHEDULE cch ON cch.CONTRACT_CHARGE_SCHEDULE_ID = tah.CONTRACT_CHARGE_SCHEDULE_ID
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.PORT_MOVEMENT_DEVICE_ASSIGN pmda ON pmda.VESSEL_MOVEMENT_ID = mov.VESSEL_MOVEMENT_ID
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.MASTER_DEVICE md ON md.MASTER_DEVICE_ID = pmda.DEVICE_ID AND md.MASTER_DEVICE_TYPE_ID = 4
    LEFT JOIN EMERSON.Synthesis_Test_02242020.dbo.TANK t ON t.MASTER_DEVICE_ID = md.MASTER_DEVICE_ID
	LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.MASTER_BUSINESS_ENTITY be ON be.MASTER_BE_ID = tah.STOCKHOLDER_ID 
	LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.VESSEL_TYPE  vty ON v.[VESSEL_TYPE_ID] = vty.[VESSEL_TYPE_ID]
	LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.MASTER_BUSINESS_ENTITY tankbe ON tankbe.MASTER_BE_ID = t.CURR_STOCKHOLDER_ID  
	LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.[MASTER_PRODUCT] prod ON prod.[MASTER_PRODUCT_ID] = mov.[MATERIAL_ID] 
	LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.[MASTER_PRODUCT] tankprod ON tankprod.[MASTER_PRODUCT_ID] = t.MASTER_PRODUCT_ID 
	LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.[VESSEL_MOVEMENT_STATUS]  movstat ON movstat.[VESSEL_MOVEMENT_STATUS_ID]  = mov.[VESSEL_MOVEMENT_STATUS]
    LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.[VESSEL_TRIP_STATUS] vstat ON vstat.[VESSEL_TRIP_STATUS_ID] = vt.[VESSEL_TRIP_STATUS]
    LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.[TERMINAL_ACTIVITY_STATUS] tahstat ON tahstat.[ACTIVITY_STATUS_ID] = tah.[TERMINAL_ACTIVITY_HEADER_STATUS] 
    LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.OPENSEAL_LKUP_ITEM lk ON lk.ITEM_KEY = mov.VESSEL_MOVEMENT_STATUS
    LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.TICKET tk ON tk.MOVEMENT_ID = vt.VESSEL_TRIP_ID  
    LEFT OUTER  JOIN EMERSON.Synthesis_Test_02242020.dbo.TICKET_STATUS tkstat ON tkstat.TICKET_STATUS_ID = tk.TICKET_STATUS 
	LEFT OUTER JOIN 
	(SELECT * FROM OPENQUERY(EMERSON, 'select * FROM Synthesis_Test_02242020.dbo.MASTER_LOCATION')) loc ON md.MASTER_LOCATION_ID = loc.MASTER_LOCATION_ID
WHERE lk.LKUP_ID = 1029 
AND pmda.[PORT_MOVEMENT_DEVICE_ASSIGN_ID] = s.syn_port_movement_id 
	
declare @newVessels TABLE ( syn_key uniqueidentifier, volume float)

INSERT INTO @newVessels (syn_key, volume)
select _key, syn_nominated_quantity from synthesis_tickets 
WHERE _key NOT IN (SELECT synthesis_key from vessel_projected_volumes)
	AND syn_vessel_eta > DATEADD(day, -45, getdate())
	AND syn_vessel_lay_status <> 'REJECTED'

-- put all volume in day 1, the team will edit as necessary
INSERT INTO vessel_projected_volumes (_key, synthesis_key, day_number, projected_volume)
SELECT newid(), syn_key, 1, volume
FROM @newVessels

INSERT INTO vessel_projected_volumes (_key, synthesis_key, day_number, projected_volume)
SELECT newid(), syn_key, 2, 0
FROM @newVessels

INSERT INTO vessel_projected_volumes (_key, synthesis_key, day_number, projected_volume)
SELECT newid(), syn_key, 3, 0
FROM @newVessels


UPDATE synthesis_tickets
SET syn_vessel_eta = DATETIMEFROMPARTS(YEAR(syn_vessel_eta), MONTH(syn_vessel_eta), DAY(syn_vessel_eta), 7,1,0,0)
WHERE CONVERT(TIME, syn_vessel_eta) = '00:00'


--- update synthesis received 

UPDATE mt
SET synthesis_received = emt.synthesis_received
FROM EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets] emt INNER JOIN 
	Moda_Final_Tickets mt ON emt._key = mt._key
WHERE mt.synthesis_received IS NULL

UPDATE tickets
SET tk_status = 5
FROM tickets t INNER JOIN 
	 Moda_Final_Tickets mt ON t._key = mt._key
WHERE tk_status < 5 AND mt.synthesis_received IS NOT NULL
	  AND t.tk_eod_datetime > DATEADD(MONTH, -2, getdate())


-- add a record for vessel invoicing 



INSERT INTO [dbo].[vessel_invoice]
           ([_key]
           ,[vi_synthesis_ticket_key]
           ,[vi_shipper_key]
           ,[vi_marine_agent_key]
           ,[vi_marine_agent])
SELECT newid(),
    st._key -- ticket key 
    ,shipper_e._key--shipper key 
    ,agent_e._key-- agent key
    ,agent_e.name
FROM synthesis_tickets st  INNER JOIN 
     entities shipper_e ON st.syn_shipper_id = shipper_e.external_id LEFT OUTER JOIN
     entities agent_e ON st.syn_agent_id = agent_e.external_id 
WHERE syn_vessel_lay_status <> 'REJECTED' 
AND st._key NOT IN (SELECT vi_synthesis_ticket_key FROM vessel_invoice)
AND st.syn_vessel_eta > DATEADD(MONTH, -2, getdate())

--PENDING
--SCHEDULED
--REJECTED
--EXECUTING
--TRANSFER COMPLETED
--EXECUTED
--CLOSED
--SELECT vi.vessel_invoice_status, st.syn_vessel_lay_status,
UPDATE vi
SET	vi_vessel_invoice_status_key = 
		CASE WHEN st.syn_vessel_lay_status = 'PENDING' THEN '4fa6e5bf-e593-4714-8240-c51dea7fedcb'--1
	        WHEN st.syn_vessel_lay_status = 'SCHEDULED' THEN '4fa6e5bf-e593-4714-8240-c51dea7fedcb' --1
			WHEN st.syn_vessel_lay_status = 'REJECTED' THEN '2af4b6d5-b7e6-434c-9eae-4c01c29e8b4e' --5
			WHEN st.syn_vessel_lay_status = 'EXECUTING' THEN 'b792e04d-a8fa-45c2-b8b0-f55dffc212ed' --2
			WHEN st.syn_vessel_lay_status = 'TRANSFER COMPLETED' THEN 'b792e04d-a8fa-45c2-b8b0-f55dffc212ed' --2
			WHEN st.syn_vessel_lay_status = 'EXECUTED' THEN 'b41c7137-8f60-4730-8f3f-4028420c60d2' --3
			WHEN st.syn_vessel_lay_status = 'CLOSED' THEN 'b41c7137-8f60-4730-8f3f-4028420c60d2' --3 
			END 
FROM vessel_invoice vi INNER JOIN 
	 synthesis_tickets st ON vi.vi_synthesis_ticket_key = st._key
WHERE st.syn_vessel_eta > DATEADD(MONTH, -2, getdate())



-- update batch id in the transfer table if it changed in moda system 
--SELECT * 
UPDATE mft
SET mft.batch_id = t.tk_batch_id
FROM Moda_Final_Tickets mft INNER JOIN 
	 tickets t ON mft._key = t._key
WHERE mft.batch_id <> t.tk_batch_id
AND mft.synthesis_received IS NULL


--SELECT * 
UPDATE mft
SET mft.batch_id = t.tk_batch_id
FROM  EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets]  mft INNER JOIN 
	 tickets t ON mft._key = t._key
WHERE mft.batch_id <> t.tk_batch_id
AND mft.synthesis_received IS NULL


-- UPDATE dates for ones that did not send properly 
UPDATE tickets
SET tk_start_datetime = DATEADD(DAY, -1, tk_eod_datetime)
WHERE tk_start_datetime = '2001-01-01 00:00'

--SELECT * 
--FROM Moda_Final_Tickets mft 
UPDATE Moda_Final_Tickets
SET start_datetime = DATEADD(DAY, -1, eod_datetime)
WHERE start_datetime = '2001-01-01 00:00'



--SELECT * 
----UPDATE mft
----SET mft.batch_id = t.tk_batch_id
--FROM  EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets]  mft 
UPDATE EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets]
SET start_datetime = DATEADD(DAY, -1, eod_datetime)
WHERE  start_datetime = '2001-01-01 00:00'


UPDATE tickets
SET tk_end_datetime =  tk_eod_datetime
WHERE tk_end_datetime = '2001-01-01 00:00'

--SELECT * 
--FROM Moda_Final_Tickets mft 
UPDATE Moda_Final_Tickets
SET stop_datetime = eod_datetime
WHERE stop_datetime = '2001-01-01 00:00'



--SELECT * 
----UPDATE mft
----SET mft.batch_id = t.tk_batch_id
--FROM  EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets]  mft 
UPDATE EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets]
SET stop_datetime = eod_datetime
WHERE  stop_datetime = '2001-01-01 00:00'


END
