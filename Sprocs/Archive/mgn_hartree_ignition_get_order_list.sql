--USE [Hartree_TMS]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_hartree_ignition_get_order_list]    Script Date: 5/4/2021 7:38:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/18/2021
-- Description:		Update selected business entities record with new user input details.

/*EXEC [mgn_hartree_ignition_get_order_list] 
	@location = tl1
	
	// 
	*/
-- =============================================
ALTER             PROCEDURE [dbo].[mgn_hartree_ignition_get_order_list]

	@location nvarchar(100) = 'tl1'
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from entities
--Where _key = @row
--declare @location nvarchar(100) = 3
declare @location_type int = (select lt.lt_location_type
							  from   locations as l left outer join
							  	     location_types as lt on l.ln_location_type = lt.lt_location_type
							  where  l.ln_external_id = @location)

							  print @location_type

declare @threshold int = (select min_nom_unload_limit
						  from site_configuration as sc
						  where site_key = [dbo].[get_default_site]())
print @threshold	

if (@location_type = 1) --Truck Lane
BEGIN
	SELECT ' Select Ticket' as [name]
	UNION
	SELECT Cast(n.nomination_number as varchar) + ' - '+ isnull(e1.short_name,'') + ' - '+ isnull(e2.short_name,'')+ ' - '+ isnull(e3.short_name,'')

	FROM (select * from locations where ln_location_type = @location_type AND ln_external_id = @location) as l left outer join
		 nominations_general as n on n.entity1_key = l.ln_e1_key and n.entity2_key = l.ln_e2_key left outer join
		 entities as e1 on n.entity1_key = e1._key left outer join
		 entities as e2 on n.entity2_key = e2._key left outer join
		 entities as e3 on n.entity3_key = e3._key 

	WHERE n.deleted = 0 and
		  (n.nom_volume - [dbo].[get_gen_nom_total](n.nomination_number)) > @threshold
		  AND (l.ln_e1_key = n.entity1_key OR l.ln_e1_key IS NULL)
		  AND (l.ln_e2_key = n.entity2_key OR l.ln_e2_key IS NULL)
		  AND (l.ln_e3_key = n.entity3_key OR l.ln_e3_key IS NULL)

	Group By n.nomination_number,
			 e1.short_name,
			 e2.short_name,
			 e3.short_name

	ORDER BY name
		  
END

if(@location_type = 3) --Dock
BEGIN
	SELECT ' Select Ticket' as [name]
	UNION
	SELECT   CAST(t.tk_ticket_id as varchar)  + ' - ' + isnull(v.vessel_name,'NO VESSEL NAME') + ' - '+ isnull(e1.short_name,'NO AGENT') + ' - ' + isnull(p.product_code, 'NO PRODUCT') as name
			  
	FROM	 (select * from ticket_type where type_is_vessel = 1) as tt left outer join
			 tickets as t on t.tk_type = tt.typ_type left outer join
			 entities as e1 on t.tk_entity1_key = e1._key left outer join
			 products as p on t.tk_product = p._key left outer join
			 vessels as v on t.tk_vessel_key = v._key
			 
	WHERE	 t.tk_deleted = 0 and
			 t.tk_status = 1 --Scheduled

    Group By t.tk_ticket_id,
			 v.vessel_name,
			 e1.short_name,
			 p.product_code

	ORDER BY name

END
END

