--TRUNCATE TABLE valve_state

--UPDATE valves
--SET valve_position_tag = REPLACE(valve_position_tag, '.CV', '')

DECLARE @lastTime datetime = (SELECT MAX([meter_timestamp]) FROM meter_readings)
DECLARE  @EndTime DATETIME = getdate()

SET @lastTime = DATEADD(MINUTE, 1, @lastTime)
--PRINT @lastTime

EXEC pi_meter_values_insert 
	@start = @lastTime,
	@end = @EndTime,
	@timestep = '1m'