


 DECLARE @valve TABLE (
        tag_name nVARCHAR(100),
        [timestamp] DATETIME,
        current_value NVARCHAR(100))

DECLARE 	@start datetime = GETDATE(), 
	@end datetime = DATEADD(day, -1, getdate()), 
	@timestep nvarchar(20) = '1m',
	@start2 nvarchar(20) = '4/5/2020 07:00',
	@end2 nvarchar(20) = '4/5/2020 07:00'
	

DECLARE @query nvarchar(max) 
DECLARE @name nvarchar(10) = 'Name'

SET @query = '
SELECT * FROM OPENQUERY([MODAPI], ''SELECT e.Name, s.TimeStamp, s.CV
FROM
(
	SELECT TOP 100 ID, Name, Template
	FROM [Master].[Element].[Element]
) e
CROSS APPLY [PiToSql].[PiToSql].[Raw_String_GetSampledValues]
(
	e.ID,  --Element ID
	''''' + @start2+''''',  --Start Time
	'''''+@end2+''''',  --End Time
	'''''+@timestep+'''''  --Time Step
) s
WHERE e.Template = N''''Raw_String''''
'')'


print @query
 select * from @valve

INSERT INTO @valve(tag_name, [timestamp],current_value)
 EXEC sp_executeSql  @query



 SELECT * FROM @valve