--TRUNCATE TABLE valve_state

--UPDATE valves
--SET valve_position_tag = REPLACE(valve_position_tag, '.CV', '')

DECLARE @lastTime datetime = (SELECT MAX([tank_timestamp]) FROM tank_readings)
DECLARE  @EndTime DATETIME = getdate()

SET @lastTime = DATEADD(MINUTE, 1, @lastTime)
--PRINT @lastTime

EXEC pi_tank_values_insert 
	@start = @lastTime,
	@end = @EndTime,
	@timestep = '1m'