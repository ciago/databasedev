-- ================================================
-- Mangan Inc
-- Created By:  Candice Iago
-- Date: 4/6/2020
-- Purpose
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.pi_valve_states_insert
	@start datetime, 
	@end datetime, 
	@timestep nvarchar(20) = '1m'
AS
/* 
EXEC pi_valve_states_insert 
	@start = '4/5/2020 08:03',
	@end = '4/7/2020 08:03',
	@timestep = '1m'
*/
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--EXECUTE AS USER = 'MODAINGLESIDE\Candice.Iago'

-- convert dates to nvarchar
DECLARE @start2 nvarchar(20) = CONVERT(nvarchar(20), @start, 120),
	    @end2 nvarchar(20) = CONVERT(nvarchar(20), @end, 120)

-- sample insert table(this will be the final real table)
DECLARE @valve_state TABLE (
        tag_name nVARCHAR(100),
        [timestamp] DATETIME,
        current_value NVARCHAR(100))

DECLARE @query nvarchar(max) 
DECLARE @name nvarchar(10) = 'Name'

SET @query = 'SELECT * FROM OPENQUERY([MODAPI],''
SELECT e.Name, sv.TimeStamp, sv.CV
FROM Master.Element.Element e
CROSS APPLY Master.Element.GetSampledValues
<
    ''''Raw_String'''', --Template - element template name
    {
        ''''|CV'''', -- AttributeTemplatePath - Required
        ''''CV'''', -- ValueColumnName - Required
        ''''CV_UnitOfMeasure'''', -- UnitOfMeasureColumnName - Optional
        ''''CV_Error'''', -- ErrorColumnName - Optional
        NULL -- UnitOfMeasure - Optional
    } -- AttributeMetadata - defines which columns to expose for a particular attribute template
-- ,{
--  } -- AttributeMetadata
>(e.ID,
	''''' + @start2+''''',  --Start Time
	'''''+@end2+''''',  --End Time
	'''''+@timestep+'''''  --Time Step
	) sv
WHERE e.Template = ''''Raw_String''''
'')'


--print @query
-- select * from @valve_state

INSERT INTO @valve_state(tag_name, [timestamp],current_value)
 EXEC sp_executeSql  @query


INSERT INTO [dbo].[valve_state]
           ([_key]
           ,[valve_key]
           ,[state]
           ,[timestamp])
SELECT newid(), 
	   v._key, 
	   CASE WHEN vs.current_value = 'OPEN' THEN 1 ELSE 0 END, 
	   vs.timestamp
FROM @valve_state vs INNER JOIN 
	 valves v ON vs.tag_name = v.valve_position_tag 




 --SELECT * FROM @valve_state
END
GO
