-- ================================================
-- Mangan Inc
-- Created By:  Candice Iago
-- Date: 4/6/2020
-- Purpose
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.pi_skid_batch_ids_insert
	@start datetime, 
	@end datetime, 
	@timestep nvarchar(20) = '1m'
AS
/* 
EXEC pi_skid_batch_ids_insert
	@start = '4/5/2020 08:03',
	@end = '4/7/2020 08:03',
	@timestep = '1m'
*/
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--EXECUTE AS USER = 'MODAINGLESIDE\Candice.Iago'

-- convert dates to nvarchar
DECLARE @start2 nvarchar(20) = CONVERT(nvarchar(20), @start, 120),
	    @end2 nvarchar(20) = CONVERT(nvarchar(20), @end, 120)

-- sample insert table(this will be the final real table)
DECLARE @skid_values TABLE (
        skid_name nVARCHAR(100),
        [timestamp] DATETIME,
        batch_id nvarchar(100))

DECLARE @query nvarchar(max) 
DECLARE @name nvarchar(10) = 'Name'

SET @query = 'SELECT * FROM OPENQUERY([MODAPI],''
SELECT e.Name, sv.TimeStamp,  sv.[BATCH-ID]
FROM Master.Element.Element e
CROSS APPLY Master.Element.GetSampledValues
<
    ''''Meter_Skid'''', --Template - element template name
    {
        ''''|BATCH-ID'''', -- AttributeTemplatePath - Required
        ''''BATCH-ID'''', -- ValueColumnName - Required
        ''''BATCH-ID_UnitOfMeasure'''', -- UnitOfMeasureColumnName - Optional
        ''''BATCH-ID_Error'''', -- ErrorColumnName - Optional
        NULL -- UnitOfMeasure - Optional
    } -- AttributeMetadata - defines which columns to expose for a particular attribute template
 
>(e.ID,
	''''' + @start2+''''',  --Start Time
	'''''+@end2+''''',  --End Time
	'''''+@timestep+'''''  --Time Step
	) sv
WHERE e.Template = ''''Meter_Skid''''
'')'

--print @query
-- select * from @skid_values

INSERT INTO @skid_values(skid_name, [timestamp],batch_id)
EXEC sp_executeSql  @query
--select * from @skid_values


INSERT INTO [dbo].skid_batch_ids
           ([_key]
           ,skid_key
           ,[timestamp]
		   ,skid_batch_id)
SELECT newid(), 
	   s._key,
	   sv.timestamp, 
	   ISNULL(sv.batch_id, '')
FROM @skid_values sv INNER JOIN 
	 skids s ON s.skid_description = sv.skid_name 




 --SELECT * FROM @skid_values
END
GO