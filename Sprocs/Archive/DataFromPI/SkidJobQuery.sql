--TRUNCATE TABLE valve_state

--UPDATE valves
--SET valve_position_tag = REPLACE(valve_position_tag, '.CV', '')

DECLARE @lastTime datetime = (SELECT MAX([timestamp]) FROM skid_batch_ids)
DECLARE  @EndTime DATETIME = getdate()

SET @lastTime = DATEADD(MINUTE, 1, @lastTime)
--PRINT @lastTime

EXEC pi_skid_batch_ids_insert 
	@start = @lastTime,
	@end = @EndTime,
	@timestep = '1m'