--delete from EMERSON.Moda_Interface.[dbo].[tank_readings]

INSERT INTO EMERSON.Moda_Interface.[dbo].[tank_readings]
           ([_key]
           ,tank_key
           ,[tank_timestamp]
		   ,tank_level
		   ,tank_nsv
		   ,tank_gsv
		   ,tank_tov
		   ,tank_external_id)
SELECT tv._key, 
	   tv.tank_key,
	   tv.tank_timestamp, 
	   tv.tank_level, 
	   tv.tank_nsv, 
	   tv.tank_gsv,
	   tv.tank_tov,
	   tv.tank_external_id
FROM tank_readings tv --LEFT OUTER JOIN 
	 --tanks t ON t.tank_pi_name = tv.tank_name 
WHERE CONVERT(time, tv.tank_timestamp) = '07:00'

