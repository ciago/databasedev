USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[virtual_tank_values_insert]    Script Date: 10/27/2020 1:08:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER     PROCEDURE [dbo].[virtual_tank_values_insert]
	--@start datetime, 
	--@end datetime, 
	--@timestep nvarchar(20) = '1m'
AS
/* 
EXEC pi_tank_values_insert 
	@start = '08/30/2020 07:00',
	@end = '9/14/2020 08:03',
	@timestep = '1m'
*/
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



declare  @date datetime = getdate()
-- Virtual Tanks 

DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, @date), DATEPART(MONTH, @date), DATEPART(DAY, @date), 7, 0, 0, 0)

DECLARE @StartDate datetime = (SELECT MAX([tank_timestamp]) FROM tank_readings WHERE tank_external_id LIKE '%virtual%')
       ,@EndDate   datetime;

SET @EndDate =dbo.get_eod_datetime(@date)
print @StartDate
PRINT @EndDate
;

--select @StartDate
WITH theDates AS
     (SELECT @StartDate as theDate
      UNION ALL
      SELECT DATEADD(day, 1, theDate)
        FROM theDates
       WHERE DATEADD(day, 1, theDate) < @EndDate
     )

INSERT INTO [dbo].tank_readings
           ([_key]
           ,tank_key
           ,[tank_timestamp]
		   ,tank_level
		   ,tank_nsv
		   ,tank_gsv
		   ,tank_tov
		   ,tank_external_id)
SELECT newid(), 
	   t._key, 
	   DATETIMEFROMPARTS(DATEPART(YEAR, d.theDate), DATEPART(MONTH, d.theDate), DATEPART(DAY, d.theDate), 7,0,0,0), 
	   0, 
	   0, 
	   0,
	   0,
	   t.tank_external_id_inventory
 --, p.description,  
FROM tanks t CROSS JOIN 
	 theDates d 
WHERE t.tank_name LIKE '%Virtual%' 
	  AND t._key NOT IN (SELECT tank_key FROM tank_readings WHERE tank_timestamp = DATETIMEFROMPARTS(DATEPART(YEAR, d.theDate), DATEPART(MONTH, d.theDate), DATEPART(DAY, d.theDate), 7,0,0,0) AND tank_key =  t._key )
ORDER BY d.theDate
OPTION (MAXRECURSION 0)
;






--;

--select @StartDate
WITH theDates AS
     (SELECT @StartDate as theDate
      UNION ALL
      SELECT DATEADD(day, 1, theDate)
        FROM theDates
       WHERE DATEADD(day, 1, theDate) < @EndDate
     )

INSERT INTO EMERSON.Moda_Interface.[dbo].[tank_readings]
           ([_key]
           ,tank_key
           ,[tank_timestamp]
		   ,tank_level
		   ,tank_nsv
		   ,tank_gsv
		   ,tank_tov
		   ,tank_external_id
		   ,site_code)
SELECT newid(), 
	   t._key, 
	   DATETIMEFROMPARTS(DATEPART(YEAR, d.theDate), DATEPART(MONTH, d.theDate), DATEPART(DAY, d.theDate), 7,0,0,0), 
	   0, 
	   0, 
	   0,
	   0,
	   t.tank_external_id_inventory, 
	   100
 --, p.description,  
FROM tanks t CROSS JOIN 
	 theDates d 
WHERE t.tank_name LIKE '%Virtual%'
	  AND t._key NOT IN (SELECT tank_key FROM EMERSON.Moda_Interface.[dbo].[tank_readings] WHERE tank_timestamp = DATETIMEFROMPARTS(DATEPART(YEAR, d.theDate), DATEPART(MONTH, d.theDate), DATEPART(DAY, d.theDate), 7,0,0,0) AND tank_key =  t._key )

ORDER BY d.theDate
OPTION (MAXRECURSION 0)
;


END