-- ================================================
-- Mangan Inc
-- Created By:  Candice Iago
-- Date: 4/6/2020
-- Purpose
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE dbo.pi_meter_values_insert
	@start datetime, 
	@end datetime, 
	@timestep nvarchar(20) = '1m'
AS
/* 
EXEC pi_meter_values_insert 
	@start = '4/5/2020 08:03',
	@end = '4/7/2020 08:03',
	@timestep = '1m'
*/
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--EXECUTE AS USER = 'MODAINGLESIDE\Candice.Iago'

-- convert dates to nvarchar
DECLARE @start2 nvarchar(20) = CONVERT(nvarchar(20), @start, 120),
	    @end2 nvarchar(20) = CONVERT(nvarchar(20), @end, 120)

-- sample insert table(this will be the final real table)
DECLARE @meter_values TABLE (
        meter_name nVARCHAR(100),
        [timestamp] DATETIME,
        totalizer float,
		tag_path nvarchar(300))

DECLARE @query nvarchar(max) 
DECLARE @name nvarchar(10) = 'Name'

SET @query = 'SELECT * FROM OPENQUERY([MODAPI],''
SELECT e.Name, sv.TimeStamp, Double(sv.Totalizer) as Totalizer,  e.PrimaryPath
FROM Master.Element.Element e
CROSS APPLY Master.Element.GetSampledValues
<
    ''''Meter'''', --Template - element template name
    {
        ''''|Totalizer'''', -- AttributeTemplatePath - Required
        ''''Totalizer'''', -- ValueColumnName - Required
        ''''Totalizer_UnitOfMeasure'''', -- UnitOfMeasureColumnName - Optional
        ''''Totalizer_Error'''', -- ErrorColumnName - Optional
        NULL -- UnitOfMeasure - Optional
    } -- AttributeMetadata - defines which columns to expose for a particular attribute template

>(e.ID,
	''''' + @start2+''''',  --Start Time
	'''''+@end2+''''',  --End Time
	'''''+@timestep+'''''  --Time Step
	) sv
WHERE e.Template = ''''Meter''''
'')'

--print @query
-- select * from @meter_values

INSERT INTO @meter_values(meter_name, [timestamp],totalizer,  tag_path)
EXEC sp_executeSql  @query
--select * from @meter_values

--\Ingleside\Meters\SK-1050\

UPDATE @meter_values
SET tag_path = RTRIM(REPLACE(REPLACE(tag_path, '\Ingleside\Meters\', '' ), '\', '' )),
	meter_name = RTRIM(meter_name)
--select * from @meter_values

INSERT INTO [dbo].meter_readings
           ([_key]
           ,meter_key
           ,[meter_timestamp]
		   ,meter_totalizer)
SELECT newid(), 
	   m._key,
	   mv.timestamp, 
	   mv.totalizer
FROM @meter_values mv INNER JOIN 
	 meters m ON m.meter_name = mv.meter_name AND tag_path = skid_name




 --SELECT * FROM @meter_values
END
GO