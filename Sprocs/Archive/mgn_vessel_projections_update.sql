USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_vessel_projections_update]    Script Date: 4/22/2021 10:57:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected vessel projections with new user input details.

/*EXEC mgn_vessel_projections_update 
	@row = '49c99307-d0da-47ef-8e64-dfa9ef43ebc3',
	@projected_volume = 462375
	   	
		462375

	// 
	*/
-- =============================================
ALTER           PROCEDURE [dbo].[mgn_vessel_projections_update]

	@row uniqueidentifier,
    @site_key uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 
	@projected_volume float
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from vessel_projections
--Where _key = @row

UPDATE vessel_projected_volumes

	SET projected_volume = @projected_volume
		
WHERE _key = @row

END
