USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_user_group_permissions_get_row]    Script Date: 5/5/2021 6:55:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 4/29/21
-- Description:	

/*

EXEC mgn_user_group_permissions_get_row  @row  = '207D4768-2D6E-4BC8-AEF3-B5C9D906601D'

*/
-- =============================================
ALTER         PROCEDURE [dbo].[mgn_user_group_permissions_get_row]
	@row uniqueidentifier = NULL
AS
BEGIN

SET NOCOUNT ON;


SELECT ugp._key as _key,
	   ug.user_group_name as group_name,
	   mi.menu_label_name as menu_name, 
	   mi.menu_is_parent,
	   mi.menu_is_child,
	   ugp.gp_view_permission, 
	   ugp.gp_update_permission,
	   ugp.gp_delete_inactivate_permisssion,
	   ugp.gp_special_permission, 
	   mi.menu_parent_number_1,
	   ISNULL(mi.menu_special_permission_description, '') as menu_special_permission_description
FROM user_groups_permissions ugp INNER JOIN  
	 menu_items mi  ON mi._key = ugp.gp_menu_item_key  INNER JOIN
	 user_groups ug ON ug._key = ugp.gp_user_group_key  INNER JOIN 
	 menu_items parent ON parent.menu_number = mi.menu_parent_number_1
WHERE ugp._key = @row
	
END
