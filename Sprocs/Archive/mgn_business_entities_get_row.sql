USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_business_entities_get_row]    Script Date: 3/25/2021 4:38:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/18/2021
-- Description:		Obtains a single row of business entities data from a specified guid.

/*EXEC mgn_business_entities_get_row
	@row  = '86c0cacb-b542-49c8-b352-8be35f276541'

	*/
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_business_entities_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from entities
--WHere _key = @row
   
SELECT			e._key,
				e.name as [name],
				e.short_name as [short_name],
				et.type_description as [entity_type_desc],
				e.type as [type],
				e.external_code as [external_code],
				ISNULL(e.fein, '') as [fein],
				ISNULL(e.texas_taxpayer_number, '') as [texas_taxpayer_number],
				e.active as [active],
				ISNULL(e.transportation_mode_key, '00000000-0000-0000-0000-000000000000') as [transportation_mode_key],
				ISNULL(e.city, '') as [city],
				ISNULL(e.state_key, '00000000-0000-0000-0000-000000000000') as [state_key],
				isnull(e.payment_terms,1000) as [payment_terms]
			  
FROM			entities AS e INNER JOIN
				entity_types AS et ON et.type_code = e.type

WHERE			@row = e._key
	  
END
