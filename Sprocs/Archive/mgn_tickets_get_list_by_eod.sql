USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_tickets_get_list_by_eod]    Script Date: 4/5/2021 4:28:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Candice Iago    
-- Create date: 6/26/2020
-- Description:    gets a list of vessel activity records for the selected date





/*EXEC mgn_tickets_get_list_by_eod
    @date  = '4/18/2020', @site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', @entity_filter_1 = '86c0cacb-b542-49c8-b352-8be35f276541'
    */
-- =============================================
ALTER        PROCEDURE [dbo].[mgn_tickets_get_list_by_eod]
    @date datetime = null,
    @site uniqueidentifier = null, 
    @user varchar(50) = 'default', 
    @machine varchar(50) = 'default', 
    @entity_filter_1 uniqueidentifier = null,
    @pipeline uniqueidentifier = null,
    @ticket_type int = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row





DECLARE @eod datetime = DATEADD(DAY, 1, SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), (DAY(@date)), 07, 00))





-- set site if null
IF(@site IS NULL) 
BEGIN 
    SET @site = dbo.get_default_site();
END






SELECT t._key,
       CASE WHEN tt.typ_type = 1 THEN 'pipeline' WHEN tt.typ_type = 2 THEN 'vessel' WHEN tt.typ_type = 3 THEN 'transfer' END as type_description, -- type 
       ts.stat_description, -- status
       t.tk_ticket_id,
       ISNULL(t.tk_nomination_number,0) as tk_nomination_number,
       e.short_name  as [entity_name], -- shipper
       p.product_code, -- product
       ISNULL(t.tk_batch_id, '') as [tk_batch_id], -- batch id
       ISNULL(t.tk_scheduled_start_datetime, DATEADD(DAY, -1, t.tk_eod_datetime)) as tk_sched_start_datetime,  -- start
       ISNULL(t.tk_scheduled_end_datetime, t.tk_eod_datetime) as tk_sched_end_datetime,-- end
       ISNULL(t.tk_start_datetime, DATEADD(DAY, -1, t.tk_eod_datetime)) as tk_start_datetime,  -- start
       ISNULL(t.tk_end_datetime, t.tk_eod_datetime) as tk_end_datetime,-- end
       ROUND( ISNULL(t.tk_final_gsv,0), 2) as tk_final_gsv, -- volume 
       t.tk_eod_datetime, 
       t.tk_scheduled_volume as tk_scheduled_volume,
       ISNULL(uom.uom_abbreviation, '') as uom_abbreviation,
       e1.name as entity1_name, 
       CASE WHEN t.[tk_source_tank_key] IS NULL THEN COALESCE(b.berth_description, pl.[description], '') ELSE ISNULL(from_t.tank_name,'') END as from_location , 
       CASE WHEN t.tk_destination_tank_key IS NULL THEN COALESCE(b.berth_description, pl.[description], '') ELSE ISNULL(to_t.tank_name,'') END as to_location 


FROM tickets t INNER JOIN 
     ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
     ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN 
     entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN -- entity1 is always going to be the site default entity type
     products p On t.tk_product = p._key LEFT OUTER JOIN 
     ticket_type_fields ttf on ttf.type_fie_ticket_type = tt._key left outer join
     uom on uom.uom_uom = t.tk_volume_uom LEFT OUTER JOIN 
     entities e1 ON e1._key = t.tk_entity1_key LEFT OUTER JOIN
     tanks from_t ON from_t._key = tk_source_tank_key LEFT OUTER JOIN 
     pipeline pl ON pl._key = t.tk_pipeline_key LEFT OUTER JOIN 
     berths b ON b._key = t.tk_berth_key LEFT OUTER JOIN 
     tanks to_t ON to_t._key = tk_destination_tank_key 





WHERE tk_eod_datetime = @eod AND t.tk_site_key = @site
      AND (@entity_filter_1 = t.tk_entity1_key OR @entity_filter_1 IS NULL)
      AND (@pipeline = t.tk_pipeline_key OR @pipeline IS NULL)
      AND (@ticket_type = tt.typ_type OR @ticket_type IS NULL)
      AND (t.tk_deleted = 0)





ORDER BY typ_type
    
END
 



