declare @eod datetime = '6/21/2020 7:00'
		,@entity_key uniqueidentifier 



SELECT st._key, st.syn_customer_ref, st.syn_vessel_eta, DATEADD(DAY, vv.day_number -1, st.syn_vessel_eta), vv.projected_volume  , dbo.get_eod_datetime(DATEADD(DAY, vv.day_number -1, st.syn_vessel_eta)) as eod
FROM vessel_projected_volumes vv INNER JOIN 
	 synthesis_tickets st ON vv.synthesis_key = st._key





