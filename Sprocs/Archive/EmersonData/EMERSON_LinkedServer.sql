USE [master]
GO

--/****** Object:  LinkedServer [EMERSON]    Script Date: 1/16/2014 3:37:16 PM ******/
--EXEC master.dbo.sp_dropserver @server=N'EMERSON', @droplogins='droplogins'
--GO


--*********************CHANGE DATASOURCE NAME ****************************************************************************
/****** Object:  LinkedServer [EMERSON]    Script Date: 1/16/2014 3:37:16 PM ******/
EXEC master.dbo.sp_addlinkedserver @server = N'EMERSON', @srvproduct=N'SQLSERVER', @provider=N'SQLNCLI', @datasrc=N'MIECTSDB02B', @catalog=N'Moda_Interface'
 /* For security reasons the linked server remote logins password is changed with ######## */
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'EMERSON',@useself=N'False',@locallogin=NULL,@rmtuser=N'LinkedServerUser',@rmtpassword='2AAbXXXu'

GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'collation compatible', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'data access', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'dist', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'pub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'rpc', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'rpc out', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'sub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'connect timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'collation name', @optvalue=null
GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'lazy schema validation', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'query timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'use remote collation', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'EMERSON', @optname=N'remote proc transaction promotion', @optvalue=N'true'
GO


