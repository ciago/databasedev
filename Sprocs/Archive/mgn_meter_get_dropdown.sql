USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_meter_get_dropdown]    Script Date: 3/19/2021 2:29:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/5/2021
-- Description:		Select list of meters for dropdown usage.


/*EXEC mgn_meter_get_dropdown

	*/
-- =============================================
ALTER       PROCEDURE [dbo].[mgn_meter_get_dropdown] 
    @site_key uniqueidentifier = null, 
	@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();
	SELECT '00000000-0000-0000-0000-000000000000', 'Select Meter' as [name]
	UNION
    SELECT m._key, COALESCE(skid_name, ln_location_name, '') + ' ' + meter_name

    FROM meters as m LEFT OUTER JOIN
		 locations as l on l._key = m.locations_key

	WHERE m.site_key = @site_key and
		  m.active = 1

ORDER by name
    
END
