USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_product_recipes_get_list]    Script Date: 3/19/2021 3:40:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/26/2021
-- Description:		Gets a list of product recipes from a selected site, product class, and active status.

/*EXEC mgn_product_recipes_get_list  
	@site_key = '00000000-0000-0000-0000-000000000000',
	@product_class_key = '00000000-0000-0000-0000-000000000000',
	@active = 1
	
    */	
-- =============================================
ALTER        PROCEDURE [dbo].[mgn_product_recipes_get_list]

	@site_key uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@product_class_key uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@active bit = 1
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from prodcuts

SELECT			p._key,
				p.product_name as [product_name],
				isnull(p.product_class_key, '00000000-0000-0000-0000-000000000000') as [product_class_key],
				isnull(pc.product_class,'') as [product_class],
				isnull(p.product_api,0) as [product_api],
				isnull(p.safe_operating_temperature,0) as [safe_operating_temp],
				isnull(p.product_external_id,'') as[product_external_id],
				isnull(p.product_code,'') as [product_code]

FROM			products AS p left outer join
				product_classes AS pc ON pc._key = p.product_class_key

WHERE			(p.site_key = @site_key OR @site_key = '00000000-0000-0000-0000-000000000000')
				AND (p.product_class_key = @product_class_key OR @product_class_key = '00000000-0000-0000-0000-000000000000')
				AND	(p.active = @active)
	  
END
