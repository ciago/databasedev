USE [Moda]
GO
/****** Object:  UserDefinedFunction [dbo].[get_invoice_charge_total]    Script Date: 3/26/2021 11:31:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER        FUNCTION [dbo].[get_invoice_charge_total]
( @vessel_invoice_key uniqueidentifier
)
RETURNS float
AS
BEGIN
declare @amount float

SELECT /*t.* , st.syn_customer_ref, st.syn_vessel_description, st.syn_vessel_name*/
	   @amount = sum(isnull(ic.ic_charge_total,0))
FROM invoice_charge ic 
WHERE ic.ic_vessel_invoice_key = @vessel_invoice_key

RETURN @amount 

END
