USE [Moda]
GO
/****** Object:  UserDefinedFunction [dbo].[get_default_uom_temperature]    Script Date: 3/11/2021 9:19:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER   FUNCTION [dbo].[get_default_uom_temperature]
(
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @site_key uniqueidentifier, 
	@uom int

	-- Add the T-SQL statements to compute the return value here
	SET @site_key = dbo.get_default_site()
	SET @uom = (SELECT uom_default_temperature FROM site_configuration WHERE site_key = @site_key)
	-- Return the result of the function
	RETURN @uom

END
