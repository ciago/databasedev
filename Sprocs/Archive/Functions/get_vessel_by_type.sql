USE [Moda]
GO
/****** Object:  UserDefinedFunction [dbo].[get_vessel_by_type]    Script Date: 3/16/2021 12:57:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER    FUNCTION [dbo].[get_vessel_by_type]
( @shipper uniqueidentifier,@vessel_type_key uniqueidentifier, @month int, @year int
)
RETURNS float
AS
BEGIN
declare @amount int
declare @month_start datetime = DATETIMEFROMPARTS(@year,@month,1,7,0,0,0)
declare @month_end datetime = DATEADD(month,1,@month_start)

SELECT /*t.* , st.syn_customer_ref, st.syn_vessel_description, st.syn_vessel_name*/
	   @amount = Count(t._key)
FROM tickets t LEFT OUTER JOIN
	 synthesis_tickets st ON t.tk_batch_id = st.syn_customer_ref LEFT OUTER JOIN
	 vessel_types vt on vt.vessel_type = RTRIM(LTRIM(st.syn_vessel_description))
WHERE t.tk_type = 2	and 
	  t.tk_entity1_key = @shipper and 
	  vt._key = @vessel_type_key and 
	  t.tk_start_datetime >= @month_start and 
	  t.tk_start_datetime < @month_end

RETURN @amount 

END
