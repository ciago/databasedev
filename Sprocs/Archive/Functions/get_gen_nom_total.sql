-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date, ,>
-- Description:    <Description, ,>
-- =============================================
CREATE  or alter      FUNCTION [dbo].[get_gen_nom_total]
( @nomination_number bigint
)
RETURNS float
AS
BEGIN
declare @amount float
--declare @month_start datetime = DATETIMEFROMPARTS(@year,@month,1,7,0,0,0)
--declare @month_end datetime = DATEADD(month,1,@month_start)

 

SELECT /*t.* , st.syn_customer_ref, st.syn_vessel_description, st.syn_vessel_name*/
       @amount = sum(isnull(t.tk_final_gsv,0))
FROM nominations_general n left join
     tickets t on n.nomination_number = t.tk_nomination_number -- LEFT OUTER JOIN
     --ticket_type tt on t.tk_type = tt.typ_type
WHERE n.nomination_number = @nomination_number and
      t.tk_status <> 7 and
      t.tk_deleted = 0

 

RETURN @amount 

 

END
GO