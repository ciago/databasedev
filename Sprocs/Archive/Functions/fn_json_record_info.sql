/****** Object:  UserDefinedFunction [dbo].[dsi_v5_json_record_info]    Script Date: 7/15/2019 5:01:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER FUNCTION [dbo].[fn_json_record_info](
  @data AS VARCHAR(MAX),
  @key AS VARCHAR(20),
  @user AS VARCHAR(50),
  @machine AS VARCHAR(255),
  @date AS VARCHAR(255))
  RETURNS VARCHAR(MAX) AS
BEGIN
  
  DECLARE @json_length int = LEN(dbo.default_record_info());
  IF (LEN(@data) < @json_length)
	SET @data = dbo.default_record_info();

  -- validate the data first
  IF @data IS NULL OR @data = ''
    SET @data = dbo.default_record_info();




  IF @key = 'created' OR @key = 'inserted' OR @key = 'ins'
  BEGIN
    SET @data = JSON_MODIFY(@data, '$.created_by', @user);
	  SET @data = JSON_MODIFY(@data, '$.created_machine', @machine);
	  SET @data = JSON_MODIFY(@data, '$.created_date', @date);
  END
  ELSE IF @key ='modified' OR @key = 'updated' OR @key = 'upd'
  BEGIN
    SET @data = JSON_MODIFY(@data, '$.modified_by', @user);
	  SET @data = JSON_MODIFY(@data, '$.modified_machine', @machine);
	  SET @data = JSON_MODIFY(@data, '$.modified_date', @date);
  END
  ELSE IF @key ='deleted' OR @key = 'del'
  BEGIN
    SET @data = JSON_MODIFY(@data, '$.deleted_by', @user);
	  SET @data = JSON_MODIFY(@data, '$.deleted_machine', @machine);
	  SET @data = JSON_MODIFY(@data, '$.deleted_date', @date);
  END
  ELSE IF @key ='archived' OR @key = 'arch'
  BEGIN
    SET @data = JSON_MODIFY(@data, '$.archived_by', @user);
	  SET @data = JSON_MODIFY(@data, '$.archived_machine', @machine);
	  SET @data = JSON_MODIFY(@data, '$.archived_date', @date);
  END
  ELSE IF @key ='restored' OR @key = 'rest'
  BEGIN
    SET @data = JSON_MODIFY(@data, '$.restored_by', @user);
	  SET @data = JSON_MODIFY(@data, '$.restored_machine', @machine);
	  SET @data = JSON_MODIFY(@data, '$.restored_date', @date);
  END

  RETURN @data;
END
