USE [Moda]
GO
/****** Object:  UserDefinedFunction [dbo].[get_tank_capacity]    Script Date: 2/5/2021 10:47:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER  FUNCTION [dbo].[get_tank_capacity]
( @shipper uniqueidentifier, @month int, @year int
)
RETURNS float
AS
BEGIN
declare @amount float
declare @effective_date datetime = DATETIMEFROMPARTS(@year,@month,1,7,0,0,0)

SELECT  
	  @amount =  sum(tow.capacity_for_billing)
FROM tank_owner tow  INNER JOIN 
     tanks mt ON tow.tank_key = mt._key LEFT OUTER JOIN  
	 entities e ON tow.tank_owner_key =  e._key
WHERE tow.tk_start_datetime <= @effective_date AND (tow.tk_end_datetime IS NULL OR tow.tk_end_datetime >= @effective_date)
	  AND (tow.tank_owner_key IS NOT NULL AND tow.tank_owner_key = @shipper OR @shipper IS NULL)
	  AND mt.tank_name NOT LIKE '%virtual%'
	  ORDER BY e.name, 
	   mt.tank_name
	  
RETURN @amount 

END
