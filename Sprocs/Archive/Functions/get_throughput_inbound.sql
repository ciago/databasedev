USE [Moda]
GO
/****** Object:  UserDefinedFunction [dbo].[get_vessel_by_type]    Script Date: 2/11/2021 2:15:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create or ALTER    FUNCTION [dbo].[get_throughput_inbound]
( @shipper uniqueidentifier, @month int, @year int
)
RETURNS float
AS
BEGIN
declare @amount int
declare @month_start datetime = DATETIMEFROMPARTS(@year,@month,1,7,0,0,0)
declare @month_end datetime = DATEADD(month,1,@month_start)

SELECT /*t.* , st.syn_customer_ref, st.syn_vessel_description, st.syn_vessel_name*/
	   @amount = sum(isnull(t.tk_final_gsv,0))
FROM tickets t LEFT OUTER JOIN
	 ticket_type tt on t.tk_type = tt.typ_type
WHERE tt.type_is_receipt = 1 and 
	  t.tk_entity1_key = @shipper and 
	  t.tk_start_datetime >= @month_start and 
	  t.tk_start_datetime < @month_end

RETURN @amount 

END
