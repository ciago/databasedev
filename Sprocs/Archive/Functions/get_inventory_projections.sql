/*
SELECT * FROM get_inventory_projections('6/18/2020 07:00')

*/


CREATE OR ALTER FUNCTION get_inventory_projections(
					@eod datetime
					)
RETURNS @inventory_projections TABLE (eod_datetime datetime, tank_key uniqueidentifier, beginning float, receipts float, 
									  shipments float, transfers float, ending_book float) 
AS
BEGIN
--declare 
--	@eod  datetime = '6/18/2020 07:00'

--DECLARE @inventory_projections TABLE (eod_datetime datetime, tank_key uniqueidentifier, beginning float, receipts float, 
--									  shipments float, transfers float, ending_book float) 


DECLARE @StartDate datetime = DATEADD(day, 1, (SELECT MAX(eod_datetime) FROM tank_actuals WHERE open_gsv IS NOT NULL))
       ,@EndDate   datetime = DATEADD(day, 7, @eod)
;

WITH theDates AS
     (SELECT @StartDate as theDate
      UNION ALL
      SELECT DATEADD(day, 1, theDate)
        FROM theDates
       WHERE DATEADD(day, 1, theDate) <= @EndDate
     )

INSERT INTO @inventory_projections
(eod_datetime, tank_key, beginning , receipts , shipments , transfers , ending_book)
SELECT DATETIMEFROMPARTS(DATEPART(YEAR, d.theDate), DATEPART(MONTH, d.theDate), DATEPART(DAY, d.theDate), 7,0,0,0), t._key, 0, 0, 0,0,0
 --, p.description,  
FROM tanks t CROSS JOIN 
	 theDates d 
WHERE t.active = 1 
ORDER BY d.theDate
OPTION (MAXRECURSION 0)
;

DECLARE @first_day bit = 1
DECLARE @current_date datetime

DECLARE db_cursor CURSOR FOR 
SELECT DISTINCT eod_datetime 
FROM @inventory_projections
ORDER BY eod_datetime

OPEN db_cursor  
FETCH NEXT FROM db_cursor INTO @current_date  

WHILE @@FETCH_STATUS = 0  
BEGIN  
      --PRINT @current_date 
	  
	-- beginning 
	  -- if it is the first day, get the beginning from the actual close inventory 
	  IF (@first_day = 1) 
	  BEGIN
		  UPDATE iv
		  SET beginning = ISNULL(ta.close_gsv,0)
		  FROM @inventory_projections iv INNER JOIN 
			   tank_actuals ta ON DATEADD(DAY, 1, ta.eod_datetime) = iv.eod_datetime AND ta.tank_key = iv.tank_key
		  WHERE iv.eod_datetime = @current_date

		SET @first_day = 0;
	  END
	  ELSE 
	  BEGIN 
		  UPDATE iv
		  SET beginning = iv_prev.ending_book
		  FROM @inventory_projections iv INNER JOIN 
			   @inventory_projections iv_prev ON DATEADD(DAY, 1, iv_prev.eod_datetime) = iv.eod_datetime AND iv_prev.tank_key = iv.tank_key
		  WHERE iv.eod_datetime = @current_date
	  END

	  UPDATE iv
	SET receipts = t.volume 
	FROM (SELECT SUM(t.tk_scheduled_volume) as volume, tk_eod_datetime, tk_destination_tank_key
	FROM tickets t INNER JOIN 
		 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
		 ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN 
		 entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN -- entity1 is always going to be the site default entity type
		 products p On t.tk_product = p._key LEFT OUTER JOIN 
		 ticket_type_fields ttf on ttf.type_fie_ticket_type = tt._key left outer join
		 uom on uom.uom_uom = t.tk_volume_uom LEFT OUTER JOIN 
		 entities e1 ON e1._key = t.tk_entity1_key LEFT OUTER JOIN
		 tanks from_t ON from_t._key = tk_source_tank_key LEFT OUTER JOIN 
		 pipeline pl ON pl._key = t.tk_pipeline_key LEFT OUTER JOIN 
		 berths b ON b._key = t.tk_berth_key LEFT OUTER JOIN 
		 tanks to_t ON to_t._key = tk_destination_tank_key  
	WHERE tk_eod_datetime = @current_date 
		  AND (t.tk_deleted = 0)
		  AND t.tk_status <> 7
		  AND tt.type_is_receipt = 1
    GROUP BY  tk_eod_datetime, tk_destination_tank_key )t  INNER JOIN 
		 @inventory_projections iv ON iv.eod_datetime = t.tk_eod_datetime AND iv.tank_key = t.tk_destination_tank_key

		 
	  UPDATE iv
	SET shipments = t.volume 
	FROM (SELECT SUM(t.tk_scheduled_volume) as volume, tk_eod_datetime, tk_source_tank_key
	FROM tickets t INNER JOIN 
		 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
		 ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN 
		 entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN -- entity1 is always going to be the site default entity type
		 products p On t.tk_product = p._key LEFT OUTER JOIN 
		 ticket_type_fields ttf on ttf.type_fie_ticket_type = tt._key left outer join
		 uom on uom.uom_uom = t.tk_volume_uom LEFT OUTER JOIN 
		 entities e1 ON e1._key = t.tk_entity1_key LEFT OUTER JOIN
		 tanks from_t ON from_t._key = tk_source_tank_key LEFT OUTER JOIN 
		 pipeline pl ON pl._key = t.tk_pipeline_key LEFT OUTER JOIN 
		 berths b ON b._key = t.tk_berth_key LEFT OUTER JOIN 
		 tanks to_t ON to_t._key = tk_destination_tank_key  
	WHERE tk_eod_datetime = @current_date 
		  AND (t.tk_deleted = 0)
		  AND t.tk_status <> 7
		  AND tt.type_is_shipment = 1
    GROUP BY  tk_eod_datetime, tk_source_tank_key )t  INNER JOIN 
		 @inventory_projections iv ON iv.eod_datetime = t.tk_eod_datetime AND iv.tank_key = t.tk_source_tank_key




		 
	  UPDATE iv
	SET transfers = transfers + t.volume 
	FROM (
SELECT SUM((t.tk_scheduled_volume * -1)) as volume, tk_eod_datetime, tk_source_tank_key
FROM tickets t INNER JOIN 
     ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
     ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN 
     entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN -- entity1 is always going to be the site default entity type
     products p On t.tk_product = p._key LEFT OUTER JOIN 
     ticket_type_fields ttf on ttf.type_fie_ticket_type = tt._key left outer join
     uom on uom.uom_uom = t.tk_volume_uom LEFT OUTER JOIN 
     entities e1 ON e1._key = t.tk_entity1_key LEFT OUTER JOIN
     pipeline pl ON pl._key = t.tk_pipeline_key LEFT OUTER JOIN 
     berths b ON b._key = t.tk_berth_key LEFT OUTER JOIN 
     tanks from_t ON from_t._key = tk_source_tank_key
WHERE tk_eod_datetime = @current_date 
      AND (t.tk_deleted = 0)
	  AND t.tk_status <> 7 -- not voided
	  AND tt.type_is_transfer = 1 
    GROUP BY  tk_eod_datetime, tk_source_tank_key )t  INNER JOIN 
		 @inventory_projections iv ON iv.eod_datetime = t.tk_eod_datetime AND iv.tank_key = t.tk_source_tank_key
		 
	  UPDATE iv
	SET transfers = transfers + t.volume 
	FROM (
SELECT SUM(t.tk_scheduled_volume ) as volume, tk_eod_datetime, tk_destination_tank_key
FROM tickets t INNER JOIN 
     ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
     ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN 
     entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN -- entity1 is always going to be the site default entity type
     products p On t.tk_product = p._key LEFT OUTER JOIN 
     ticket_type_fields ttf on ttf.type_fie_ticket_type = tt._key left outer join
     uom on uom.uom_uom = t.tk_volume_uom LEFT OUTER JOIN 
     entities e1 ON e1._key = t.tk_entity1_key LEFT OUTER JOIN
     pipeline pl ON pl._key = t.tk_pipeline_key LEFT OUTER JOIN 
     berths b ON b._key = t.tk_berth_key LEFT OUTER JOIN 
     tanks to_t ON to_t._key = tk_destination_tank_key
WHERE tk_eod_datetime = @current_date 
      AND (t.tk_deleted = 0)
	  AND t.tk_status <> 7 -- not voided
	  AND tt.type_is_transfer = 1 
    GROUP BY  tk_eod_datetime, tk_destination_tank_key )t  INNER JOIN 
		 @inventory_projections iv ON iv.eod_datetime = t.tk_eod_datetime AND iv.tank_key = t.tk_destination_tank_key

	-- ending 
	UPDATE @inventory_projections
	SET ending_book = beginning + receipts - shipments + transfers
	WHERE eod_datetime = @current_date



      FETCH NEXT FROM db_cursor INTO @current_date 
END 

CLOSE db_cursor  
DEALLOCATE db_cursor 


RETURN 
END