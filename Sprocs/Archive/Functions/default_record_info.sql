SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER   FUNCTION [dbo].[default_record_info]()
RETURNS VARCHAR(MAX)
AS
BEGIN
	RETURN 
  '{ 
    "created_by":"", 
    "created_date":"", 
    "created_machine":"", 
    "modified_by":"", 
    "modified_date":"", 
    "modified_machine":"", 
    "deleted_by":"", 
    "deleted_date":"", 
    "deleted_machine":"", 
    "archived_by":"", 
    "archived_date":"", 
    "archived_machine":"", 
    "restored_by":"", 
    "restored_date":"", 
    "restored_machine":"" 
  }';
END
