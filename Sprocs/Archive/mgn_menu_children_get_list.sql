USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_volume_types_get_list]    Script Date: 4/6/2021 11:03:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table

/*EXEC [mgn_menu_children_get_list]
	@parent_number  = 12
	*/
-- =============================================
Create or ALTER       PROCEDURE [dbo].[mgn_menu_children_get_list]
	@parent_number int,
	@user nvarchar(50) = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT menu_label_name as label_text, 
       menu_action_result_name as action_result, 
	   menu_controller as controller,
	   @parent_number as parent_number
FROM menu_items 
WHERE menu_active = 1 
      AND (menu_parent_number_1 = @parent_number or menu_parent_number_2 = @parent_number or menu_parent_number_3 = @parent_number or menu_parent_number_4 = @parent_number)
      AND menu_is_child = 1 
ORDER BY --try to make the first links be those whose first digits match those of the parent_number variable
		 menu_sort
	
END
