USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_marine_agent_generate_invoice]    Script Date: 3/25/2021 3:29:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		10/21/2020
-- Description:		Obtains a list of marine agent charges from a selected invoice record.

/*EXEC mgn_marine_agent_generate_invoice
	@row  = '17f0e167-1e91-4a92-bc2f-3d1ad86234a4'

    */	
-- =============================================
ALTER  		     PROCEDURE [dbo].[mgn_marine_agent_generate_invoice]

	--@row uniqueidentifier = null
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

--Select * from 
--WHere _key = @row 

		-- charge description, charge boolean, unit amount, unit description,
		-- unite rate, charge total, charge override

Update [dbo].[vessel_invoice]
SET	   [vi_invoice_total] = dbo.get_invoice_charge_total(vi._key),
	   [vi_accounting_date] = [syn_ticket_start_datetime],
	   [vi_invoice_date] = getdate(),
	   [vi_payment_date] = DATEADD(day,e.payment_terms,getdate()),
	   [vi_time_in_berth] = DATEDIFF(second,st.syn_ticket_start_datetime,st.syn_ticket_end_datetime)/3600.0,
	   [vi_vessel_invoice_status_key] = vis2._key
		
FROM   vessel_invoice as vi left outer join
	   vessel_invoice_status as vis on vi.vi_vessel_invoice_status_key = vis._key left outer join
	   synthesis_tickets as st on vi.vi_synthesis_ticket_key = st._key left outer join
	   entities as e on vi.vi_marine_agent_key = e._key left outer join
	   vessel_invoice_status as vis2 on vis2.stat_type = 6 -- status int associated with "Invoice Generated"

WHERE  vis.stat_type = 4 -- status int associated with "Ready to Invoice"

END
