select distinct [TABLE] = o.Name, [Found In] = sp.Name, sp.type_desc
from sys.objects o inner join
     sys.sql_expression_dependencies  sd on o.object_id = sd.referenced_id
     inner join sys.objects sp on sd.referencing_id = sp.object_id and sp.type in ('P', 'FN')
  where o.name = 'TABLE'
  order by sp.Name