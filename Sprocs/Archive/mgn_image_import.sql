--USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_charge_details_get]    Script Date: 4/12/2021 10:04:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_image_import
@ImageFolderPath = 'C:\Users\ciago\OneDrive - MANGAN INC\Client Installs\Hartree\Logos',
@Filename = 'Hartree-Logo-Rectangle-300x102.png'
 
	*/
Create or ALTER         PROCEDURE [dbo].[mgn_image_import] 
	--@app_name_key uniqueidentifier,
	@Filename nvarchar(50),
	@ImageFolderPath nvarchar(MAX)

AS
BEGIN
   DECLARE @Path2OutFile NVARCHAR (2000);
   DECLARE @tsql NVARCHAR (2000);
   SET NOCOUNT ON
   SET @Path2OutFile = CONCAT (
         @ImageFolderPath
         ,'\'
         , @Filename
         );
   SET @tsql = 'insert into app_logo (al_image_name, al_logo_data) ' +
               ' SELECT ' + '''' + @Filename + '''' + ', * ' + 
               'FROM Openrowset( Bulk ' + '''' + @Path2OutFile + '''' + ', Single_Blob) as img'
   EXEC (@tsql)
	  
END
