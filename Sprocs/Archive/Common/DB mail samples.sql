----Msdb.dbo.sysmail_help_queue_sp: This system stored procedure returns the status of the Database Mail queue:
--IF EXISTS (
--    SELECT 1 FROM sys.configurations 
--    WHERE NAME = 'Database Mail XPs' AND VALUE = 0)
--BEGIN
--  PRINT 'Enabling Database Mail XPs'
--  EXEC sp_configure 'show advanced options', 1;  
--  RECONFIGURE
--  EXEC sp_configure 'Database Mail XPs', 1;  
--  RECONFIGURE  
--END


--1
--EXEC msdb.dbo.sysmail_help_queue_sp @queue_type = 'Mail';
--EXEC msdb.dbo.sysmail_help_configure_sp;
--EXEC msdb.dbo.sysmail_help_account_sp;
--EXEC msdb.dbo.sysmail_help_profile_sp;
--EXEC msdb.dbo.sysmail_help_profileaccount_sp;
--EXEC msdb.dbo.sysmail_help_principalprofile_sp;



EXEC msdb.dbo.sp_send_dbmail  
    @profile_name = 'Test mail',  
    @recipients = 'candicemiago@gmail.com',  
    @body = 'This is a  test.',  
    @subject = 'This is a test',
	@query = 'select va._key,  SUM(mr.meter_totalizer) as start_totalizer --, m.meter_name, va.valve_key, va.valve_start, va.valve_stop
		 from valve_activity va  INNER JOIN 
			  valves v ON va.valve_key = v._key INNER JOIN 
			  meters m ON v.valve_skid_key = m.skid_key INNER JOIN 
			  meter_readings mr ON mr.meter_key = m._key
		 WHERE mr.meter_timestamp = va.valve_start
		 GROUP BY va._key',
	@execute_query_database = 'Moda',
	@exclude_query_output = 0;


 --   @profile_name =  'profile_name'   
 --   @recipients =  'recipients									--varchar(max)
 --   @copy_recipients =  'copy_recipient							--varchar(max)
 --   @blind_copy_recipients =  'blind_copy_recipient				--varchar(max)
 --   @from_address =  'from_address'								--varchar(max)
 --   @reply_to =  'reply_to'										--varchar(max)
 --   @subject =  'subject'											--nvarchar(255). 
 --   @body =  'body'												--nvarchar(max),
 --   @body_format =  'body_format'									--varchar(20)  OPTIONS:  TEXT or HTML, DEFAULT IS TEXT
 --   @importance =  'importance'									--varchar(6)  OPTIONS:  Low, Normal, High
 --   @sensitivity =  'sensitivity'									--varchar(12) OPTIONS: Normal, Personal, Private, Confidential
 --   @file_attachments =  'attachment								--nvarchar(max) Database Mail limits file attachments to 1 MB per file
 --   @query =  'query'												--nvarchar(max)
 --   @execute_query_database =  'execute_query_database'			--sysname, with a default of the current database
 --   @attach_query_result_as_file =  attach_query_result_as_file   --bit, with a default of 0
 --   @query_attachment_filename =  query_attachment_filename		--nvarchar(255)   Specifies the file name to use for the result set of the query attachment
 --   @query_result_header =  query_result_header					--bit   Specifies whether the query results include column headers.  When the value is 1, query results contain column headers
 --   @query_result_width =  query_result_width						--int, with a default of 256   Is the line width, in characters, to use for formatting the results of the query
 --   @query_result_separator =  'query_result_separator'			--char(1). Defaults to ' ' (space) character used to separate columns in the query
 --   @exclude_query_output =  exclude_query_output					--bit, with a default of 0. pecifies whether to return the output of the query execution 
 --   @append_query_error =  append_query_error						--bit, with a default of 0.Specifies whether to send the e-mail when an error returns 
 --   @query_no_truncate =  query_no_truncate						--bit, (when not specified, truncates to 256 chars,  Specifies whether to execute the query with the option that avoids truncation of large variable length data types
 --   @query_result_no_padding =  @query_result_no_padding			--bit. The default is 0. When you set to 1, the query results are not padded, possibly reducing the file size.If you set @query_result_no_padding to 1 and you set the @query_result_width parameter, the @query_result_no_padding parameter overwrites the @query_result_width parameter.
 --   @mailitem_id =  mailitem_id  [ OUTPUT							--Optional output parameter returns the mailitem_id of the message. The mailitem_id is of type int.