--SELECT * 
--FROM tickets  t 

--UPDATE tickets 
--SET tk_entity1_key = tk_shipper

---- vessel tickets 
--UPDATE tickets 
--SET tk_source_tank_key = tk_from_key
--WHERE tk_type = 2

--UPDATE tickets 
--SET tk_berth_key = tk_to_key
--WHERE tk_type = 2

---- pipeline tickets 
--UPDATE tickets 
--SET tk_pipeline_key = tk_from_key
--WHERE tk_type = 1

--UPDATE tickets 
--SET tk_destination_tank_key = tk_to_key
--WHERE tk_type = 1


---- tank to tank tickets 
--UPDATE tickets 
--SET tk_source_tank_key = tk_from_key
--WHERE tk_type = 3

--UPDATE tickets 
--SET tk_destination_tank_key = tk_to_key
--WHERE tk_type = 3

INSERT INTO [dbo].[tickets_meter]
	([_key], 
	 [tk_ticket_key],
	 [tk_meter_key],
	 [tk_meter_id], 
[tk_meter_batch_start_date],
[tk_meter_batch_start_time],
[tk_meter_batch_end_date],
[tk_meter_batch_end_time],
[tk_meter_product_name],
[tk_meter_opening_gross],
[tk_meter_opening_net],
[tk_meter_opening_nsv],
[tk_meter_closing_gross],
[tk_meter_closing_net],
[tk_meter_closing_nsv],
[tk_meter_volume_uom],
[tk_meter_mass_uom],
[tk_meter_temperature_uom],
[tk_meter_pressure_uom],
[tk_meter_density_uom],
[site_key],
[deleted])

select newid(),
	   t._key,
	   tmo.meter_key, 
	   m.meter_name, 
	   CONVERT(varchar, CONVERT(DATE, t.[tk_scheduled_start_datetime])),--[tk_meter_batch_start_date],
       CONVERT(varchar, CONVERT(TIME, t.[tk_scheduled_start_datetime])), --[tk_meter_batch_start_time],
       CONVERT(varchar, CONVERT(DATE, t.[tk_end_datetime]))  ,   --[tk_meter_batch_end_date],
       CONVERT(varchar, CONVERT(TIME, t.[tk_end_datetime])),     --[tk_meter_batch_end_time],
       p.product_name    ,      --[tk_meter_product_name],
       tmo.[open_totalizer] , --[tk_meter_opening_gross],
       tmo.[open_totalizer] ,  --[tk_meter_opening_net],
       tmo.[open_totalizer] ,  --[tk_meter_opening_nsv],
       tmo.[close_totalizer] ,  --[tk_meter_closing_gross],
       tmo.[close_totalizer] ,  --[tk_meter_closing_net],
       tmo.[close_totalizer] ,  --[tk_meter_closing_nsv],
       dbo.get_default_uom_volume(),  --[tk_meter_volume_uom],
       dbo.get_default_uom_mass(),  --[tk_meter_mass_uom],
       dbo.get_default_uom_temperature(),  --[tk_meter_temperature_uom],
       dbo.get_default_uom_pressure(),  --[tk_meter_pressure_uom],
       dbo.get_default_uom_density(),  --[tk_meter_density_uom],
       dbo.get_default_site(),  --[site_key],
       0  --[deleted]
from tickets t inner join 
    tickets_meter_old  tmo on t._key = tmo.ticket_key LEFT OUTER JOIN 
	meters m On tmo.meter_key = m._key left outer join 
	products p ON p._key = t.tk_product
	GROUP BY t._key,
	   tmo.meter_key, 
	   m.meter_name, t.[tk_scheduled_start_datetime], t.[tk_end_datetime],
	   p.product_name    ,      --[tk_meter_product_name],
       tmo.[open_totalizer] , --[tk_meter_opening_gross],
       tmo.[open_totalizer] ,  --[tk_meter_opening_net],
       tmo.[open_totalizer] ,  --[tk_meter_opening_nsv],
       tmo.[close_totalizer] ,  --[tk_meter_closing_gross],
       tmo.[close_totalizer] ,  --[tk_meter_closing_net],
       tmo.[close_totalizer]
	order by t.[tk_end_datetime] desc




INSERT INTO [dbo].[tickets_tank_gauge]
           ([_key]
           ,[tk_ticket_key]
           ,[tk_tank_source_key]
           ,[tk_tank_destination_key]
           ,[tk_tank_batch_start_date]
           ,[tk_tank_batch_start_time]
           ,[tk_tank_batch_end_date]
           ,[tk_tank_batch_end_time]
           ,[tk_tank_product_name]
           ,[tk_tank_opening_level_source]
           ,[tk_tank_opening_gross_source]
           ,[tk_tank_opening_net_source]
           ,[tk_tank_closing_timestamp_source]
           ,[tk_tank_closing_level_source]
           ,[tk_tank_closing_gross_source]
           ,[tk_tank_closing_net_source]
           ,[tk_tank_opening_level_destination]
           ,[tk_tank_opening_gross_destination]
           ,[tk_tank_opening_net_destination]
           ,[tk_tank_closing_timestamp_destination]
           ,[tk_tank_closing_level_destination]
           ,[tk_tank_closing_gross_destination]
           ,[tk_tank_closing_net_destination]
           ,[tk_tank_volume_uom]
           ,[tk_tank_mass_uom]
           ,[tk_tank_temperature_uom]
           ,[tk_tank_pressure_uom]
           ,[tk_tank_density_uom]
           ,[tk_tank_length_uom]
           ,[site_key]
           ,[deleted])
SELECT 

       newid(),    --(<_key, uniqueidentifier,>
       t._key,    --,<tk_ticket_key, uniqueidentifier,>
       t.tk_source_tank_key,    --,<tk_tank_source_key, uniqueidentifier,>
       t.tk_destination_tank_key,    --,<tk_tank_destination_key, uniqueidentifier,>
       CONVERT(varchar, CONVERT(DATE, t.[tk_scheduled_start_datetime])),--[tk_meter_batch_start_date],
       CONVERT(varchar, CONVERT(TIME, t.[tk_scheduled_start_datetime])), --[tk_meter_batch_start_time],
       CONVERT(varchar, CONVERT(DATE, t.[tk_end_datetime]))  ,   --[tk_meter_batch_end_date],
       CONVERT(varchar, CONVERT(TIME, t.[tk_end_datetime])),     --[tk_meter_batch_end_time],
       p.product_name,     --,<tk_tank_product_name, varchar(50),>
       CASE WHEN t.tk_type <> 1 THEN  [tk_tank_open_level] ELSE NULL END,     --,<tk_tank_opening_level_source, float,>
       CASE WHEN t.tk_type <> 1 THEN  tk_tank_open_gsv ELSE NULL END,        --,<tk_tank_opening_gross_source, float,>
       CASE WHEN t.tk_type <> 1 THEN  tk_tank_open_gsv ELSE NULL END,         --,<tk_tank_opening_net_source, float,>
       CASE WHEN t.tk_type <> 1 THEN [tk_tank_close_timestamp] ELSE NULL END,    --,<tk_tank_closing_timestamp_source, datetime,>
       CASE WHEN t.tk_type <> 1 THEN  [tk_tank_close_level] ELSE NULL END,     --,<tk_tank_closing_level_source, float,>
       CASE WHEN t.tk_type <> 1 THEN  [tk_tank_close_gsv] ELSE NULL END,       --,<tk_tank_closing_gross_source, float,>
       CASE WHEN t.tk_type <> 1 THEN  [tk_tank_close_gsv] ELSE NULL END,       --,<tk_tank_closing_net_source, float,>
       CASE WHEN t.tk_type = 1 THEN  [tk_tank_open_level] WHEN  t.tk_type = 3 THEN  [tk_transfer_to_tank_open_level]    ELSE NULL END,        --,<tk_tank_opening_level_destination, float,>
       CASE WHEN t.tk_type  = 1 THEN  tk_tank_open_gsv   WHEN  t.tk_type = 3 THEN    tk_transfer_to_tank_open_gsv      ELSE NULL END,            --,<tk_tank_opening_gross_destination, float,>
       CASE WHEN t.tk_type  = 1 THEN  tk_tank_open_gsv   WHEN  t.tk_type = 3 THEN   tk_transfer_to_tank_open_gsv       ELSE NULL END,            --,<tk_tank_opening_net_destination, float,>
       CASE WHEN t.tk_type  = 1 THEN [tk_tank_close_timestamp]  WHEN  t.tk_type = 3 THEN [tk_tank_close_timestamp]   ELSE NULL END,    --,<tk_tank_closing_timestamp_destination, datetime,>
       CASE WHEN t.tk_type  = 1 THEN  [tk_tank_close_level]   WHEN  t.tk_type = 3 THEN  [tk_transfer_to_tank_close_level]   ELSE NULL END,       --,<tk_tank_closing_level_destination, float,>
       CASE WHEN t.tk_type  = 1 THEN  [tk_tank_close_gsv]   WHEN  t.tk_type = 3 THEN  [tk_transfer_to_tank_close_gsv]     ELSE NULL END,         --,<tk_tank_closing_gross_destination, float,>
       CASE WHEN t.tk_type  = 1 THEN  [tk_tank_close_gsv]   WHEN  t.tk_type = 3 THEN  [tk_transfer_to_tank_close_gsv]     ELSE NULL END,         --,<tk_tank_closing_net_destination, float,>
         dbo.get_default_uom_volume(),  --[tk_meter_volume_uom],
       dbo.get_default_uom_mass(),  --[tk_meter_mass_uom],
       dbo.get_default_uom_temperature(),  --[tk_meter_temperature_uom],
       dbo.get_default_uom_pressure(),  --[tk_meter_pressure_uom],
       dbo.get_default_uom_density(),  --[tk_meter_density_uom],
	   
       dbo.get_default_uom_length(),    --,<tk_tank_length_uom, float,>
       dbo.get_default_site(),  --[site_key],
       0  --[deleted]
         


from tickets t LEFT OUTER JOIN  
    tanks ts  ON ts._key = t.tk_source_tank_key LEFT OUTER JOIN 
	tanks td ON td._key = t.tk_destination_tank_key LEFT OUTER JOIN 
	products p ON p._key = t.tk_product
GROUP BY t._key, tk_type, tk_status, tk_batch_id, tk_ticket_id, tk_nomination_number, tk_external_ticket_id, tk_scheduled_start_datetime, tk_scheduled_end_datetime, tk_start_datetime, tk_end_datetime, tk_eod_datetime, tk_product, tk_day_sequence_number, tk_volume_uom, tk_final_gsv, tk_final_nsv, tk_scheduled_volume, tk_source_tank_key, tk_destination_tank_key, tk_berth_key, tk_pipeline_key, tk_vehicle_key, tk_vessel_key, tk_truck_lane_key, tk_entity1_key, tk_entity2_key, tk_entity3_key, tk_entity4_key, tk_entity5_key, tk_entity6_key, tk_entity7_key,  tk_tank_open_gsv, tk_tank_open_level, tk_tank_close_gsv, tk_tank_close_level, tk_tank_change_gsv, tk_group_total_tank_diff, tk_percent_tank_diff, tk_total_meter_diff, tk_meter_volume, tk_meter_error, tk_transfer_to_tank_open_gsv, tk_transfer_to_tank_open_level, tk_transfer_to_tank_close_gsv, tk_transfer_to_tank_close_level, tk_from_key, tk_to_key, tk_shipper, tk_tank_close_timestamp,
        p.product_name


		
INSERT INTO [dbo].[tickets_tank_meter_tieback]
           ([_key]
           ,[tk_ticket_key]
           ,[tk_tmt_tank_source_key]
           ,[tk_tmt_tank_destination_key]
           ,[tk_tmt_tank_change_gsv]
           ,[tk_tmt_group_total_tank_diff]
           ,[tk_tmt_percent_tank_diff]
           ,[tk_tmt_total_meter_diff]
           ,[tk_tmt_meter_volume]
           ,[tk_tmt_tank_volume_uom]
           ,[site_key]
           ,[deleted])

SELECT 

       newid(),    --(<_key, uniqueidentifier,>
       t._key,    --,<tk_ticket_key, uniqueidentifier,>
       t.tk_source_tank_key,    --,<tk_tank_source_key, uniqueidentifier,>
       t.tk_destination_tank_key,
       t.[tk_tank_change_gsv],    --,<tk_tmt_tank_change_gsv, float,>
       t.[tk_group_total_tank_diff],  --,<tk_tmt_group_total_tank_diff, float,>
       t.[tk_percent_tank_diff],   --,<tk_tmt_percent_tank_diff, float,>
       t.[tk_total_meter_diff] ,  --,<tk_tmt_total_meter_diff, float,>
       t.[tk_meter_volume],   --,<tk_tmt_meter_volume, float,>
       dbo.get_default_uom_volume(),   --,<tk_tmt_tank_volume_uom, float,>
       dbo.get_default_site(),  --,<site_key, uniqueidentifier,>
       0 --   ,<deleted, bit,>
from tickets t 
	GROUP BY t._key, tk_type, tk_status, tk_batch_id, tk_ticket_id, tk_nomination_number, tk_external_ticket_id, tk_scheduled_start_datetime, tk_scheduled_end_datetime, tk_start_datetime, tk_end_datetime, tk_eod_datetime, tk_product, tk_day_sequence_number, tk_volume_uom, tk_final_gsv, tk_final_nsv, tk_scheduled_volume, tk_source_tank_key, tk_destination_tank_key, tk_berth_key, tk_pipeline_key, tk_vehicle_key, tk_vessel_key, tk_truck_lane_key, tk_entity1_key, tk_entity2_key, tk_entity3_key, tk_entity4_key, tk_entity5_key, tk_entity6_key, tk_entity7_key,  tk_tank_open_gsv, tk_tank_open_level, tk_tank_close_gsv, tk_tank_close_level, tk_tank_change_gsv, tk_group_total_tank_diff, tk_percent_tank_diff, tk_total_meter_diff, tk_meter_volume, tk_meter_error, tk_transfer_to_tank_open_gsv, tk_transfer_to_tank_open_level, tk_transfer_to_tank_close_gsv, tk_transfer_to_tank_close_level, tk_from_key, tk_to_key, tk_shipper, tk_tank_close_timestamp



