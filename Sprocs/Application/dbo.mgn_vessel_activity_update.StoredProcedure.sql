--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_vessel_activity_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Updates one row of the vessel acitvity table 

/*EXEC mgn_vessel_activity_update
	@row  = '4A602809-B566-4B00-BF88-CA6993FEEF75',
	@valve_skid_key = 'dbc368c9-5405-4450-b4d6-29408ecc12b9',	
	@start_date = '2020-07-20 10:01',
	@end_date = '2020-07-20 10:02',
	@amount = 1234,
	@batch_id = 'Test1234',
	@berth_key = '67502856-5746-45b9-96c7-dab439768624',
	@ignore = 0,
	@user = 'test'

	*/
-- =============================================
CREATE  OR ALTER   PROCEDURE [dbo].[mgn_vessel_activity_update]

	@row uniqueidentifier = null,
	@valve_skid_key uniqueidentifier,	
	@start_date datetime = null, 
	@end_date datetime = null, 
	@amount float = null, 
	@ignore bit = 0, 
	@batch_id nvarchar(200), 
	@berth_key uniqueidentifier,
	@user varchar(50) = '',
	@site uniqueidentifier = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DECLARE @nullguid uniqueidentifier = '00000000-0000-0000-0000-000000000000'

IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END

IF(@row IN (SELECT _key from valve_activity))
BEGIN
-- it exists already so update it 

--record info update 
--DECLARE @json varchar(max)
---- SET @json = (SELECT record_info FROM valve_activity WHERE _key = @row)
--SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())

UPDATE valve_activity 

	SET	valve_skid_key = @valve_skid_key,
		valve_start = @start_date, 
		valve_stop = @end_date, 
		valve_bbl = @amount, 
		valve_ignore = @ignore, 
		valve_skid_batch_id  = @batch_id, 
		valve_berth_key = @berth_key
--		record_info = @json

WHERE _key = @row
	
END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[valve_activity]
           ([_key]
		   ,[valve_skid_key]
		   ,[valve_start]
		   ,[valve_stop]
		   ,[valve_bbl]
		   ,[valve_ignore]
		   ,[valve_skid_batch_id]
		   ,[valve_berth_key],
		   [valve_key],
		   [valve_start_orig],
		   [valve_stop_orig],
		   [valve_eod_datetime],
		   [site_key])
--           ,[record_info])
     VALUES
           (@row
		   ,@valve_skid_key
		   ,@start_date
		   ,@end_date
		   ,@amount
		   ,@ignore
		   ,@batch_id
		   ,@berth_key
		   ,@nullguid
		   ,@start_date
		   ,@end_date
		   ,dbo.get_eod_datetime(@start_date)
		   ,@site
--           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
GO
