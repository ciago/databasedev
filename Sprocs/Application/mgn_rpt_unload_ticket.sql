
/****** Object:  StoredProcedure [dbo].[mgn_rpt_unload_ticket]    Script Date: 4/20/2021 7:59:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_unload_ticket_report_get
@row = '5d55d4da-d27e-4a0a-a9af-27c4ace3a141'
 
	*/

CREATE OR ALTER        PROCEDURE [dbo].mgn_rpt_unload_ticket 

@ticket_id int

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from tickets

SELECT        t._key as [_key],
			  t.tk_ticket_id as [ticket_number],--ticket number
			  ISNULL(tm.tm_lease, '') as [customer_ticket_number],--customer ticket number
			  tt.type_description as [ticket_type],--ticket type
			  t.tk_start_datetime as tk_start_datetime,--date submitted
			  t.tk_end_datetime as tk_end_datetime,
			  e1.name as CustomerName,
			  e2.name as CarrierName,
			  tm.tm_driver_name,
			  tm.tm_truck_name,
			  tm.tm_trailer_name,
			  --last updated
			  --shipper name w/ EPA ID # & address
			  --transporter name w/ us epa id number
			  --receiver name w/ us epa id number
			  --supplier name w/ us epa id number
			  --PU Date
			  --UNL Date
			  --Driver Name
			  --Truck#
			  --Unloader Name
			  ISNULL(p.product_name, 'No Product Assigned') as [product],--product
			  --container
			  --gross weight
			  --net weight
			  ROUND(ISNULL((t.tk_final_gsv), 0), 2) as [gross_barrels],--gross bbls
			  ROUND(ISNULL(t.tk_final_gsv * 42, 0), 2) as [gross_gallons],--gross gal
			  --bsw
			  ROUND(ISNULL(p.product_api, 0), 2) as [product_api],--api
			  --halogens (T/F)
			  --t.tk_source_tank_key,--tank number
			  --t.tk_destination_tank_key
			  CONVERT(varchar, getdate(), 0) as [current_datetime]--signature region (current datetime)
FROM		  tickets AS t INNER JOIN
			  ticket_type AS tt ON tt.typ_type = t.tk_type LEFT OUTER JOIN
			  products AS p ON p._key = t.tk_product LEFT OUTER JOIN 
			  tickets_misc tm ON t._key = tm.tm_ticket_key LEFT OUTER JOIN 
			  entities e1 ON e1._key = t.tk_entity1_key LEFT OUTER JOIN 
			  entities e2 ON e2._key = t.tk_entity2_key 
WHERE		  @ticket_id = t.tk_ticket_id
    
END