--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_marine_agent_generated_invoices_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Update selected berth fees record with new user input details.

/*EXEC mgn_accounting_vessel_types_update 
	@vt_key = '64b80ca9-adba-492f-8692-876c343c9c57',
	@vt_desc = 'Suez',
	@vt_vol = 1234,
	@site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@active = 1,
	@user = 'candice'
	
	// 
	*/
-- =============================================
create     OR ALTER        PROCEDURE [dbo].[mgn_marine_agent_generated_invoices_update]

	@vi_key uniqueidentifier,
    @comments varchar(max),
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT vi_record_info FROM vessel_invoice WHERE _key = @vi_key)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE vessel_invoice

SET		vi_comments = @comments,
		vi_record_info = @json

WHERE _key = @vi_key

END

GO
