--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_charge_unit_type_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>

/*EXEC mgn_charge_unit_type_get_list

	*/

-- =============================================
CREATE   OR ALTER     PROCEDURE [dbo].[mgn_charge_unit_type_get_list]

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

	SELECT '00000000-0000-0000-0000-000000000000', 'Select Unit Type'
	UNION
    SELECT _key, charge_unit_description
    FROM charge_unit_type
    
END
GO
