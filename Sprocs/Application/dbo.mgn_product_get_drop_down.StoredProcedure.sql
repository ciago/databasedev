--USE [Hartree_TMS]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_product_get_drop_down]    Script Date: 5/11/2021 7:32:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>


/*EXEC mgn_product_get_drop_down

	*/
-- =============================================
CREATE OR ALTER       PROCEDURE [dbo].[mgn_product_get_drop_down] 
    @site_key uniqueidentifier = null, 
	@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();
	SELECT '00000000-0000-0000-0000-000000000000' as _key, ' Select Product' as name
	UNION
    SELECT _key as _key, product_name as name
    FROM products
	WHERE site_key = @site_key
    
END
