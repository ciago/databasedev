--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_ticket_comments_ignition]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create OR ALTER              PROCEDURE [dbo].[mgn_ticket_comments_ignition]

	
	@ticket_id bigint = null,
	@comment nvarchar(max)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from entities
--Where _key = @row

declare @ticket_comment_key uniqueidentifier = null,
	@ticket_key uniqueidentifier = null,
	@delete bit = 0

		INSERT INTO [dbo].[ticket_comments]
				   (_key
				   ,[tc_ticket_key]
				   ,[tc_ticket_id]
				   ,[tc_comment]
				   ,[tc_date])
		VALUES	  ( 
				   newid()
				   ,isnull(@ticket_key, (select _key from tickets where tk_ticket_id = @ticket_id))
				   ,isnull(@ticket_id, (select tk_ticket_id from tickets where _key = @ticket_key))
				   ,@comment
				   ,getdate())

	END

GO
