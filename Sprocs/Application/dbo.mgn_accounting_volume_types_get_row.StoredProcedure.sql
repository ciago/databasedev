USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_volume_types_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Obtains a single row of volume accounts data by searching for a specified guid.

/*EXEC mgn_accounting_volume_types_get_row
	@row  = '2bb23464-5f62-4ad4-b57b-0718abb90cd1',
	@site  = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
	*/
-- =============================================
CREATE           PROCEDURE [dbo].[mgn_accounting_volume_types_get_row]
	@row uniqueidentifier = null,
	@site uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from 
--WHere _key = @row
   
SELECT avt._key,
	   avt.type_description as volume_type, 
	   avt.site_key,
	   sites.short_name,
	   avt.active,
	   avt.show_shipper,
	   avt.show_vessel_type
			  
FROM accounting_volume_types avt left join
	 sites ON avt.site_key = sites._key 

WHERE ((@row = avt._key) and (@site = avt.site_key) and (avt.[active] = 1))
GROUP BY avt._key, avt.type_description, avt.site_key, sites.short_name, avt.active, avt.show_shipper, avt.show_vessel_type
	  
END
GO
