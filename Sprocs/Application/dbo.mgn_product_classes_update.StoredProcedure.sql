--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_product_classes_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/1/2021
-- Description:		Update selected product classes record with new user input details.

/*EXEC mgn_product_classes_update 
	@row = '8216E75A-1ECE-41BD-A391-0644FF8E3457',
	@product_class = 'TestInsert123',
	@class_description = 'A quick brown fox jumps over the lazy dog. 123',
	@active = 1,
	@user = 'candice'

	// 
	*/
-- =============================================
CREATE  OR ALTER     PROCEDURE [dbo].[mgn_product_classes_update]

	@row uniqueidentifier,
	@product_class nvarchar(50),
	@class_description nvarchar(max),
	@active bit,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from product_classes
--Where _key = @row

IF(@row IN (SELECT _key from product_classes))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM product_classes WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE product_classes

SET		product_class = @product_class,		
		class_description = @class_description,
		active = @active,
		record_info = @json

WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[product_classes]
           ([_key]
		   ,[product_class]
		   ,[class_description]
		   ,[active]
           ,[record_info])
     VALUES
           (@row
		   ,@product_class
		   ,@class_description
		   ,@active
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
GO
