--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_meters_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER      PROCEDURE [dbo].[mgn_meters_get_list]

	@site_key uniqueidentifier = null,
	@locations_key uniqueidentifier = null,
	@active bit = 1
	  
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from meters

SELECT			m._key,
				ISNULL(ISNULL(m.skid_name, l.ln_location_name), '') + ' ' + m.meter_name as [meter_name],
				ISNULL(m.meter_name_2			,'') as [meter_description],
				ISNULL(m.locations_key			,'00000000-0000-0000-0000-000000000000') as [locations_key],
				ISNULL(l.ln_location_name		,'') as [location_name],
				ISNULL(m.max_allowable_flow		,0) as [max_allowable_flow],
				ISNULL(m.max_allowable_pressure ,0) as [max_allowable_pressure],
				ISNULL(m.meter_factor			,0) as [meter_factor]

FROM			meters AS m LEFT OUTER JOIN
				--meter_products AS mp ON mp.meter_key = m._key INNER JOIN
				--products AS p ON p._key = mp.product_key INNER JOIN
				locations AS l ON l._key = m.locations_key			

WHERE			(m.site_key = @site_key OR @site_key is null)
				AND	(m.locations_key = @locations_key OR @locations_key is null)
				and (m.active = @active)

ORDER by meter_name
	  
END
GO
