USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_volume_types_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Update selected berth fees record with new user input details.

/*EXEC mgn_accounting_volume_types_update 
	@avt_key = 'fd323b20-3399-4241-9433-c83b06fe6cd2',
	@avt_desc = 'General',
	@site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@user = 'candice'
	
	// 
	*/
-- =============================================
create           PROCEDURE [dbo].[mgn_accounting_volume_types_update]

	@avt_key uniqueidentifier,
    @avt_desc varchar(50),
	@site_key uniqueidentifier,
	@user varchar(50),
	@active bit
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

IF(@avt_key IN (SELECT _key from accounting_volume_types))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM accounting_volume_types WHERE _key = @avt_key)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE accounting_volume_types

SET		type_description = @avt_desc,
		site_key = @site_key,
		[user] = @user,
		active = @active,
		record_info = @json

WHERE _key = @avt_key

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].accounting_volume_types
           ([_key]
           ,type_description
           ,site_key
		   ,active
           ,[record_info])
     VALUES
           (@avt_key
           ,@avt_desc
           ,@site_key
		   ,@active
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
GO
