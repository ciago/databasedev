--USE [Hartree_TMS]
--GO
/****** Object:  UserDefinedFunction [dbo].[get_eod_datetime]    Script Date: 5/11/2021 10:51:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE OR ALTER   FUNCTION [dbo].[get_eod_datetime]
( @date datetime
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @eod datetime

	-- Add the T-SQL statements to compute the return value here

	SET @eod = DATEADD(day, 1, SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date), 00, 00)) 
			


	-- Return the result of the function
	RETURN @eod

END
