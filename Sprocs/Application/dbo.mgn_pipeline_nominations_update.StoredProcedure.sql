--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_nominations_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected nomination bpd value in nominations table.

/*EXEC mgn_pipeline_nominations_update
	@row = '76a42202-5f4c-4f45-a02a-4409caecbe27', @bbls_per_day = 2000

	*/
-- =============================================
CREATE   OR ALTER   PROCEDURE [dbo].[mgn_pipeline_nominations_update]
	@row uniqueidentifier = null,
	@bbls_per_day float = null, 
	@user varchar(50) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--record info update 
DECLARE @json varchar(max)
SET @json = (SELECT record_info FROM nominations WHERE _key = @row)
SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())


declare @date datetime = (select DATETIMEFROMPARTS([year], [month], 1, 7, 0,0,0) FROM nominations WHERE _key = @row)

DECLARE @num_days int = (select dbo.get_days_in_month(@date))



UPDATE nominations
	SET bbls_per_day = @bbls_per_day, 
		bbls_per_month = @bbls_per_day * @num_days, 
		record_info = @json
WHERE _key = @row

END
GO
