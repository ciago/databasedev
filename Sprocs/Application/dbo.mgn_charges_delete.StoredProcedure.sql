--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_charges_delete]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Deletes a single row of charges data based on the selected row/record.

/*EXEC mgn_charges_delete
	@row  = '4A990D99-427E-468E-BDE2-812F28D538F7'

	deleted row data reference -
	row										descrip     amount	type    type_key								start_date				end_date
	67EED59A-8BF5-4918-A1FD-9C47E9E0C31B	testdelete	1234	per day	CBDA2ACF-FA81-41BA-8038-C42E17917A5E	2020-01-01 00:00:00.000	2020-12-31 00:00:00.000
	*/
-- =============================================
CREATE   OR ALTER         PROCEDURE [dbo].[mgn_charges_delete]
	@row uniqueidentifier = null,
	@user varchar(50) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from charges
--WHere _key = @row

Delete From charges

Where _key = @row

END
GO
