--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_menu_parents_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table

/*
EXEC [mgn_menu_parents_get_list] @user = 'ciago'
EXEC [mgn_menu_parents_get_list] @user = null
	*/
-- =============================================
CREATE   OR ALTER  PROCEDURE [dbo].[mgn_menu_parents_get_list]
	@user nvarchar(50) = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC mgn_user_group_permissions_sync

Declare @company_name nvarchar(100) = (select an_company_name from [dbo].[app_name])
Declare @company_name_key uniqueidentifier = (select _key from [dbo].[app_name])

IF (@user IS NOT NULL)
BEGIN 
	SELECT mi.menu_label_name as label_text,
		   default_child.menu_action_result_name as action_result, 
		   default_child.menu_controller as controller,
		   mi.menu_number as parent_number,
		   @company_name as company_name,
		   @company_name_key as company_key
	FROM menu_items mi INNER JOIN 
		 (SELECT mi.* 
          FROM menu_items mi INNER JOIN 
          	(SELECT MIN(menu_sort) as menu_sort,  menu_parent_number_1
          	 FROM menu_items mi INNER JOIN 
          	 	 users u ON u.user_login_name = @user INNER JOIN
          	 	 user_groups ug ON ug.user_group_hierarchy = u.user_highest_group_number INNER JOIN 
          	 	 user_groups_permissions ugp ON ug._key = ugp.gp_user_group_key AND mi._key = ugp.gp_menu_item_key
          	 WHERE menu_active = 1
          	 	  AND menu_is_child = 1 
          	 	  AND ugp.gp_view_permission = 1
          	 GROUP BY menu_parent_number_1) as default_child ON mi.menu_sort = default_child.menu_sort) as default_child ON default_child.menu_parent_number_1 = mi.menu_parent_number_1  INNER JOIN 
		 users u ON u.user_login_name = @user INNER JOIN
		 user_groups ug ON ug.user_group_hierarchy = u.user_highest_group_number INNER JOIN 
		 user_groups_permissions ugp ON ug._key = ugp.gp_user_group_key AND mi._key = ugp.gp_menu_item_key
	WHERE mi.menu_active = 1 
		  AND mi.menu_is_parent = 1
		  AND ugp.gp_view_permission = 1
	ORDER BY mi.menu_sort
END	
ELSE 
BEGIN 
	SELECT mi.menu_label_name as label_text,
		   default_child.menu_action_result_name as action_result, 
		   default_child.menu_controller as controller,
		   mi.menu_number as parent_number,
		   @company_name as company_name,
		   @company_name_key as company_key
	FROM menu_items mi INNER JOIN 
		 menu_items default_child ON mi.menu_default_child_number = default_child.menu_number
	WHERE mi.menu_active = 1 
		  AND mi.menu_is_parent = 1
		  AND mi.menu_number = 1
	ORDER BY mi.menu_sort

END 

END
GO
