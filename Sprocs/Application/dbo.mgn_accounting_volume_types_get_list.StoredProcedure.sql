USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_volume_types_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table

/*EXEC [mgn_accounting_volume_types_get_list]
	@volume_type  = 'f315101e-b284-447f-976f-f88f6d06df63'
	@volume_type  = '8c73c675-5d76-40a9-a836-8c3ef8035083'
	*/
-- =============================================
CREATE       PROCEDURE [dbo].[mgn_accounting_volume_types_get_list]
	/*Defining the variable that I will eventually be filtering the table by*/
	@site uniqueidentifier = null,
	@user nvarchar(50) = 'candice'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--Where _key = @volume_type

--DECLARE @eod datetime = SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date), 07, 00)

Select avt._key, 
	   avt.type_description, 
	   avt.site_key,
	   sites.short_name,
	   avt.active
from accounting_volume_types as avt Inner JOIN 
	 sites on avt.site_key = sites._key

WHERE ((avt.site_key = @site) and (avt.active = 1))
GROUP BY avt._key, avt.type_description, avt.site_key, sites.short_name, avt.active
--ORDER BY berth_description, valve_start
	
END
GO
