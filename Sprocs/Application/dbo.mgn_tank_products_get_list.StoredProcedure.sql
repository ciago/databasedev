--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_products_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_tank_owners_get_list
	@site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@user = 'candice',
	@tank_key = '847b3f4e-aa0c-465d-946d-1ed49bf3714c'
	
	*/

Create  OR ALTER PROCEDURE [dbo].[mgn_tank_products_get_list] 
    @site_key uniqueidentifier, 
	@user varchar(50),
	@tank_key uniqueidentifier
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

Select  tp._key,
		tp.tank_key,
		t.tank_name,
		tp.product_key,
		e.product_name as [product_name],
		tp.tk_start_datetime,
		tp.tk_end_datetime,
		tp.external_row_id
from tank_products as tp left join
	 tanks as t on tp.tank_key = t._key left join
	 products as e on tp.product_key = e._key
where tp.site_key = @site_key
	  and (tp.tank_key = @tank_key)
	  and (tp.deleted = 0)

END
GO
