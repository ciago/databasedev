--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   OR ALTER  PROCEDURE [dbo].[mgn_pipeline_get_list] 
	--@terminal uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT '00000000-0000-0000-0000-000000000000' AS RowKey, 'Select Pipeline' AS description
	UNION
    SELECT _key as RowKey, description AS description
	FROM pipeline
	WHERE active = 1 

	ORDER BY RowKey

END
GO
