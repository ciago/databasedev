--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_accounting_vessel_types_get_row]    Script Date: 5/6/2021 3:28:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Obtains a single row of volume accounts data by searching for a specified guid.

/*EXEC mgn_accounting_vessel_types_get_row
	@row  = '4eec6d8d-8c3f-4c9f-896b-6ba19865131a'
	*/
-- =============================================
create or ALTER           PROCEDURE [dbo].[mgn_pipeline_product_test]
	@pipeline uniqueidentifier,
	@product uniqueidentifier,
	@month int,
	@year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*
	EXEC mgn_pipeline_product_test
	@pipeline = 'fc536541-5a92-466a-bcd1-db7fedcd15e7',
	@product = '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',
	@month = 4,
	@year = 2021
	*/

--Select * from 
--WHere _key = @row

DECLARE @return bit = 0
DECLARE @date datetime = DATETIMEFROMPARTS(@year,@month,1,0,0,0,0)
if EXISTS (SELECT * 
		   FROM pipeline_product_assignment
		   WHERE [pipeline_key] = @pipeline and
		   [product_key] = @product and
		   @date >= [start_datetime] and (@date <= [end_datetime] or [end_datetime] IS NULL))
	begin
		SET @return = 1
	End
		   
select @return

	  
END
