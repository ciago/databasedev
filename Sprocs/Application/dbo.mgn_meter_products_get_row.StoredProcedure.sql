--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_meter_products_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/9/2021
-- Description:		Obtains a single row of meter product data from a specified guid.

/*
EXEC mgn_meter_products_get_row
	@row  = 'fe0cb933-5f97-4848-a93f-4bd845f59954'

	*/
-- =============================================
CREATE  OR ALTER     PROCEDURE [dbo].[mgn_meter_products_get_row]
	@row uniqueidentifier = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from meters
--WHere _key = @row
   
SELECT			mp._key,
				mp.meter_key as [meter_key],
				ISNULL(m.skid_name,'') + ' ' + m.meter_name as [meter_name],
				mp.product_key as [product_key],
				ISNULL(p.product_name,'') as [product_name],
				mp.mtr_start_datetime as [mtr_start_datetime],
				mp.mtr_end_datetime as [mtr_end_datetime]

FROM			meter_products AS mp INNER JOIN
				meters AS m ON m._key = mp.meter_key INNER JOIN
				products AS p ON p._key = mp.product_key

WHERE			@row = mp._key
	  
END
GO
