--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_vessel_projections_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   OR ALTER PROCEDURE [dbo].[mgn_vessel_projections_get_list]

    @site uniqueidentifier = null, --'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@key uniqueidentifier = null --'97CC1221-F697-4B9B-9262-83254A0C4CCE'
		   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from 

IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END

SELECT		vv._key, 
			ISNULL(st.syn_customer_ref,'') as syn_customer_ref,	   
			st.syn_vessel_eta, 
			st.syn_vessel_name, 
			ISNULL(st.syn_vessel_description, '') as syn_vessel_description, 
			DATEADD(DAY, vv.day_number -1, st.syn_vessel_eta), 
			CONVERT(int, ROUND(vv.projected_volume, 0)) as  projected_volume , 
			dbo.get_eod_datetime(DATEADD(DAY, vv.day_number -1, st.syn_vessel_eta)) as eod, 
			vv.day_number as day_number

FROM		vessel_projected_volumes vv INNER JOIN
			synthesis_tickets st ON vv.synthesis_key = st._key

WHERE	st._key = @key

ORDER BY syn_customer_ref,
		 day_number

END
 

GO
