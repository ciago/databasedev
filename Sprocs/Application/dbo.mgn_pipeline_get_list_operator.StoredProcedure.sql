--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_get_list_operator]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE   OR ALTER  PROCEDURE [dbo].[mgn_pipeline_get_list_operator] 
	--@terminal uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT _key, [description], ISNULL([shutdown],0)   as shutdown_status, sort_order
	FROM pipeline
	WHERE sort_order IS NOT NULL 
	ORDER BY sort_order

END
GO
