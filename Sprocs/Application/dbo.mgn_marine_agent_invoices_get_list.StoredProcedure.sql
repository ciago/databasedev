--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_marine_agent_invoices_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Brandon Morey    
-- Create date: 5/4/2020
-- Description:    Gets a list of marine agent invoices from a selected day.

/*EXEC mgn_marine_agent_invoices_get_list  
	@date = '2/1/2021',
	@marine_agent_key = '62fbdb06-d325-4e53-b503-576aea97027f',
	@vessel_status = '00000000-0000-0000-0000-000000000000'
	
	EXEC mgn_marine_agent_invoices_get_list  
	@date = '11/1/2020'
    */
	
-- =============================================
CREATE	 OR ALTER     PROCEDURE [dbo].[mgn_marine_agent_invoices_get_list]

	@marine_agent_key uniqueidentifier = null,
	@vessel_status uniqueidentifier = null, 
	@date datetime
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

----Select * from synthesis_tickets

   -- ETA Date (descending order), Berth, Vessel Name, Agent, Arrived Date,
   -- Status, Volume, Type

SELECT        vi._key,--st._key,
			  st.syn_vessel_eta as [eta_date],
			  ISNULL(st.syn_dock_name, '') as [berth_name],
			  isnull(st.syn_vessel_name,'') as [vessel_name],
			  isnull(vi.vi_marine_agent_key,'00000000-0000-0000-0000-000000000000'),
			  isnull(e.name,'') as [marine_agent],
			  --st.syn_marine_agent as [marine_agent],
			  ISNULL(CONVERT(varchar, st.syn_ticket_start_datetime, 22), '') as [arrived_date],
			  ISNULL(CONVERT(varchar, st.syn_ticket_end_datetime, 22), '') as [departed_date],
			  isnull(vis.stat_description,'') as [vessel_status],
			  ISNULL(CONVERT(float, st.syn_total_synthesis_net_volume), 0) as [vessel_volume],
			  ISNULL(st.syn_vessel_description, '') as [vessel_type]
			  
FROM		  vessel_invoice vi left outer join
			  synthesis_tickets AS st ON vi.vi_synthesis_ticket_key = st._key left outer join
			  vessel_invoice_status AS vis ON vi.vi_vessel_invoice_status_key = vis._key left outer JOIN
			  entities AS e ON vi.vi_marine_agent_key = e._key

WHERE		  (vi.vi_marine_agent_key = @marine_agent_key OR @marine_agent_key is null)
			  AND vi.vi_vessel_invoice_status_key = @vessel_status -- allows the user to select whatever status they want (including voided)
			  OR (@vessel_status is null AND vis.stat_type <> 5) -- but does not automatically show voided invoices if the user wants to see all invoices
			  AND st.syn_vessel_eta >= @date
	  
END
GO
