--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_entity_type_get]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey
-- Create date:		3/2/2021
-- Description:		Obtains a single row of entity type info

/*EXEC mgn_entity_type_get
	@type  = 1
	*/
-- =============================================
CREATE   OR ALTER      PROCEDURE [dbo].[mgn_entity_type_get]
	@type int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from 
--WHere _key = @row
   
SELECT   et._key,
	     et.type_code, 
		 et.type_description, 
	  etf.show_shipper,
	  etf.show_marine_agent,
	  etf.show_carrier,
	  etf.show_destination,
	  etf.show_position_holder,
	  etf.show_marine_inspector,
	  etf.show_terminal
FROM entity_types et LEFT OUTER JOIN 
	 entity_type_fields etf on etf.entity_type_key = et._key
WHERE @type = et.type_code
GROUP BY et._key, 
	     et.type_code, 
		 et.type_description,
	  etf.show_shipper,
	  etf.show_marine_agent,
	  etf.show_carrier,
	  etf.show_destination,
	  etf.show_position_holder,
	  etf.show_marine_inspector,
	  etf.show_terminal
	  
END
GO
