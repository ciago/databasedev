--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_charges_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Brandon Morey	
-- Create date: 11/11/2020
-- Description:	Obtains a single row of marine charges data from a specified guid.

/*EXEC mgn_charges_get_row
	@row  = 'ba3c4789-c970-4ef0-9697-71ade21f36ee'
	*/
-- =============================================
CREATE    OR ALTER     PROCEDURE [dbo].[mgn_charges_get_row]

	@row uniqueidentifier = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from charges
--WHere _key = @row
   
SELECT        c._key,
			  isnull(c.charge_description,'') as [charge_description],
			  isnull(c.charge_amount,0) as [charge_amount],
			  isnull(ct.charge_unit_description,'') as [charge_unit_type],
			  isnull(c.charge_unit_key,'00000000-0000-0000-0000-000000000000') as [charge_unit_key],
			  c.start_date as [start_date],
			  c.end_date as [end_date]
			  
FROM		  charges AS c INNER JOIN
			  charge_unit_type AS ct ON ct._key = c.charge_unit_key

WHERE		  @row = c._key
	  
END
GO
