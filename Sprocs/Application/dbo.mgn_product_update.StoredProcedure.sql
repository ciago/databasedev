--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_product_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   OR ALTER  PROCEDURE [dbo].[mgn_product_update]

	@row uniqueidentifier,
	@product_name nvarchar(100),
	@product_class_key uniqueidentifier,
	@product_api float,
	@safe_operating_temperature float,
	@product_external_id nvarchar(50) = null,
	@product_code nvarchar(8),
	@product_irs_code nvarchar(3) = 'XXX',
	@site_key uniqueidentifier,
	@active bit,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from products
--Where _key = @row




IF(@row IN (SELECT _key from products))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM products WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE products

SET		product_name = @product_name,		
		product_class_key = @product_class_key,
		product_api = @product_api,
		safe_operating_temperature = @safe_operating_temperature,
		product_external_id = @product_external_id,
		product_code = @product_code,
		site_key = @site_key,
		active = @active,
		record_info = @json, 
		product_irs_code = @product_irs_code

WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[products]
           ([_key]
		   ,[product_name]
		   ,[product_class_key]
		   ,[product_api]
		   ,[safe_operating_temperature]
		   ,[product_external_id]
		   ,[product_code]
		   ,product_irs_code
		   ,[site_key]
		   ,[active]
           ,[record_info])
     VALUES
           (@row
		   ,@product_name
		   ,@product_class_key
		   ,@product_api
		   ,@safe_operating_temperature
		   ,@product_external_id
		   ,@product_code
		   ,@product_irs_code
		   ,@site_key
		   ,@active
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
GO
