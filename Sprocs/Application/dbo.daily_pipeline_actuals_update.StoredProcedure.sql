--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[daily_pipeline_actuals_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  OR ALTER PROCEDURE [dbo].[daily_pipeline_actuals_update]
	
AS
/* 
EXEC daily_pipeline_actuals_update 
	
*/
BEGIN
DECLARE @StartDate datetime = DATEADD(day, 1, (SELECT MAX(eod_datetime) FROM pipeline_actuals))
       ,@EndDate   datetime = DATEADD(day, 1, getdate())
;



WITH theDates AS
     (SELECT @StartDate as theDate
      UNION ALL
      SELECT DATEADD(day, 1, theDate)
        FROM theDates
       WHERE DATEADD(day, 1, theDate) <= @EndDate
     )


INSERT INTO pipeline_actuals 
(_key, eod_datetime, pipeline_key)
SELECT newid(), DATETIMEFROMPARTS(DATEPART(YEAR, d.theDate), DATEPART(MONTH, d.theDate), DATEPART(DAY, d.theDate), 7,0,0,0)
, p._key --, p.description,  
FROM pipeline p CROSS JOIN 
	 theDates d 
ORDER BY d.theDate
OPTION (MAXRECURSION 0)
;

 --DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)
 
DECLARE @eod datetime = dbo.get_eod_datetime(getdate())

 -- insert meter readings into reference table 
delete from pipeline_actuals_meter WHERE eod_datetime = @eod

INSERT INTO pipeline_actuals_meter(skid_key, pipeline_key, meter_key, eod_datetime, open_totalizer, close_totalizer, meter_error)
select Opentot.skid, Opentot.pipeline, Opentot.meter_key, Opentot.eod, Opentot.OpenTotalizer, Closetot.CloseTotalizer , 
	   CASE WHEN Closetot.CloseTotalizer - Opentot.OpenTotalizer < 0 THEN 1 ELSE 0 END 
FROM
(SELECT  s._key as skid, p._key as pipeline, pa.eod_datetime as eod , mr.meter_key, mr.meter_totalizer as CloseTotalizer
FROM meters m INNER JOIN 
	 pipeline p on m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key INNER JOIN 
	 pipeline_actuals pa ON pa.pipeline_key = p._key
	 LEFT OUTER JOIN 
	 meter_readings mr ON mr.meter_key = m._key AND mr.meter_timestamp = pa.eod_datetime
WHERE pa.eod_datetime  = @eod) Closetot inner join

(SELECT  s._key as skid, p._key as pipeline, pa.eod_datetime as eod ,  mr.meter_key, mr.meter_totalizer as OpenTotalizer
FROM meters m INNER JOIN 
	 pipeline p on m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key INNER JOIN 
	 pipeline_actuals pa ON pa.pipeline_key = p._key
	 LEFT OUTER JOIN 
	 meter_readings mr ON mr.meter_key = m._key AND mr.meter_timestamp = DATEADD(day, -1, pa.eod_datetime)
WHERE pa.eod_datetime = @eod) Opentot ON Closetot.skid = Opentot.skid AND Closetot.meter_key = Opentot.meter_key AND Closetot.eod = Opentot.eod AND Closetot.pipeline = Opentot.pipeline

 -- UPDATE pipeline actuals table 
UPDATE tp
SET gsv_bbls = closeTot.CloseTotalizer - openTot.OpenTotalizer
	, open_totalizer = openTot.OpenTotalizer
	, close_totalizer =  closeTot.CloseTotalizer
FROM  
(SELECT  s._key as skid, p._key as pipeline, pa.eod_datetime as eod , SUM( ISNULL(mr.meter_totalizer,0)) as CloseTotalizer
FROM meters m INNER JOIN 
	 pipeline p on m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key INNER JOIN 
	 pipeline_actuals pa ON pa.pipeline_key = p._key
	 LEFT OUTER JOIN 
	 meter_readings mr ON mr.meter_key = m._key AND mr.meter_timestamp = pa.eod_datetime
WHERE pa.eod_datetime  = @eod
GROUP BY s._key, p._key, pa.eod_datetime )
closeTot INNER JOIN	 

(SELECT  s._key as skid, p._key as pipeline, pa.eod_datetime as eod , SUM( ISNULL(mr.meter_totalizer,0)) as OpenTotalizer
FROM meters m INNER JOIN 
	 pipeline p on m.pipeline_key = p._key INNER JOIN 
	 skids s ON s._key = m.skid_key INNER JOIN 
	 pipeline_actuals pa ON pa.pipeline_key = p._key
	 LEFT OUTER JOIN 
	 meter_readings mr ON mr.meter_key = m._key AND mr.meter_timestamp = DATEADD(day, -1, pa.eod_datetime)
WHERE pa.eod_datetime = @eod
GROUP BY s._key, p._key, pa.eod_datetime) 
openTot ON openTot.skid = closeTot.skid AND openTot.pipeline = closeTot.pipeline AND openTot.eod = closeTot.eod INNER JOIN 
pipeline p ON openTot.pipeline = p._key INNER JOIN 
pipeline_actuals tp ON tp.eod_datetime = openTot.eod AND tp.pipeline_key = openTot.pipeline
WHERE eod_datetime = @eod


-- update the error message if there appears to be a totalizer data error
UPDATE pipeline_actuals 
SET pipeline_error = 1, 
	pipeline_error_message = 'close totalizer smaller than open, check meter data'
WHERE close_totalizer < open_totalizer AND eod_datetime = @eod

END
GO
