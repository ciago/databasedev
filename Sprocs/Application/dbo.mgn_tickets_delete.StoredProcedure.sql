--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tickets_delete]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 5/15/2020
-- Description:	gets a list of vessel activity records for the selected date

/*EXEC mgn_tickets_delete
	@row  = '00000000-0000-0000-0000-000000000000''0a5b820b-d68c-4087-b559-f869a678df08'
	*/
-- =============================================
CREATE    OR ALTER  PROCEDURE [dbo].[mgn_tickets_delete]
	@row uniqueidentifier = null
	--, @type (pipeline, vessel, tank to tank transfer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row

DELETE 
FROM tickets 
WHERE _key = @row
	

END








GO
