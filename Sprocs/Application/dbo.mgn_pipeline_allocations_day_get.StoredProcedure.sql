--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_allocations_day_get]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago
-- Create date: 2/26/2020
-- Description:	Gets the allocations for a single day on a single pipeline 

/*EXEC mgn_pipeline_allocations_day_get
	@pipeline_key = 'fc536541-5a92-466a-bcd1-db7fedcd15e7',
	@date  = '6/03/2020'
	*/
-- =============================================
CREATE OR ALTER    PROCEDURE [dbo].[mgn_pipeline_allocations_day_get]
 	@pipeline_key uniqueidentifier =null,
	@date date =  null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @datetime DATETIME = CONVERT(DATETIME, @date)

DECLARE @eod DATETIME  =  DATEADD(day, 1, SMALLDATETIMEFROMPARTS(YEAR(@datetime), MONTH(@datetime), DAY(@datetime), 07, 00)) 
DECLARE @month int =  (SELECT MONTH(@date)), 
	    @year int =  (SELECT YEAR(@date))
		print @month
		print @year
			--print @eod
--select @eod
	-- fill the tmp table with all pipeline schedules by product by hour
EXEC mgn_pipeline_schedule_fill_hourly
	@month = @month,
	@year  = @year


	-- fill the tmp table with all pipeline schedules by product by hour
EXEC mgn_pipeline_schedule_fill_daily
	@month = @month,
	@year  = @year

--SELECT sum(allocated_amount)
--FROM pipeline_allocations pa 
--WHERE eod_datetime = @eod AND pa.pipeline_key = @pipeline
--GROUP BY pipeline_key

Select p._key as row_key, 
	   pl.short_code as pipeline_description, 
	   e.name as [entity_name], 
	   p.allocated_amount as [amount], 
	   DATEADD(day, -1, p.eod_datetime) as [date], 
	   p.pipeline_key as pipeline_key, 
	   prod.product_code as product_name
	   --act.total_allocated as total_day_allocated, 
	   --d.volume as total_day_scheduled
from pipeline_allocations p INNER JOIN 
	 entities e ON p.entity_key = e._key LEFT OUTER JOIN 
	 pipeline as pl ON pl._key = p.pipeline_key LEFT OUTER JOIN 
	 products prod ON prod._key = p.product_key
	 --(SELECT CONVERT(Date, start_datetime) as [date], ISNULL(AVG(NULLIF(rate,0)),0) as rate, SUM(volume) as volume, pipeline_key, end_datetime
	 -- FROM tmp_pipeline_by_day d 
	 -- WHERE (pipeline_key = @pipeline)
	 -- GROUP BY start_datetime, pipeline_key, end_datetime)d ON d.pipeline_key = p.pipeline_key  and d.end_datetime = p.eod_datetime --LEFT OUTER JOIN 
--	  (
--SELECT sum(allocated_amount) as total_allocated, pipeline_key
--FROM pipeline_allocations pa 
--WHERE eod_datetime = @eod AND pa.pipeline_key = @pipeline
--GROUP BY pipeline_key
--)act ON act.pipeline_key = p.pipeline_key
WHERE p.pipeline_key = @pipeline_key AND   p.eod_datetime = @eod -- AND d.[date] = @date
ORDER BY product_code, e.name	
END

GO
