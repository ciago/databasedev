--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_berth_fees_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected berth fees record with new user input details.

/*EXEC mgn_berth_fees_update 
	@row = '52E1B719-D8F2-4C58-B06C-B5F731D87314',
	@site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@start_loa = '123',
	@end_loa = '1234',
	@loa_fee = '12.34',
	@user = 'candice'
	
	// 
	*/
-- =============================================
CREATE OR ALTER      PROCEDURE [dbo].[mgn_berth_fees_update]

	@row uniqueidentifier,
    @site_key uniqueidentifier,
	@start_loa int,
	@end_loa int,
	@loa_fee float,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

IF(@row IN (SELECT _key from berth_fees))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM berth_fees WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE berth_fees

SET		start_loa = @start_loa,
		end_loa = @end_loa,
		loa_fee = @loa_fee,
		record_info = @json

WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[berth_fees]
           ([_key]
           ,[site_key]
           ,[start_loa]
           ,[end_loa]
           ,[loa_fee]
           ,[record_info])
     VALUES
           (@row
           ,@site_key
           ,@start_loa
           ,@end_loa
           ,@loa_fee
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
GO
