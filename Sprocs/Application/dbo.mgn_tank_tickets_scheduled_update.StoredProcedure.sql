--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_tickets_scheduled_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected tank ticket with new user input details.

/*EXEC mgn_tank_tickets_scheduled_update 
	@row = 'c75bb10a-30ec-4a4d-8295-c3b86aa3d4a4',
	@ticket_type = 1,
	@tk_shipper = 'dd85132f-4039-4fda-8fcc-bbd2dd9b7656',
	@tk_product = '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',
	@tk_batch_id = '',
	@tk_scheduled_start_datetime = '1/3/2021',
	@tk_end_datetime = '1/4/2021',
	@tk_scheduled_volume = 60307
			
	// 
	*/
-- =============================================
CREATE  OR ALTER   PROCEDURE [dbo].[mgn_tank_tickets_scheduled_update]

	@row uniqueidentifier,
    @site_key uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 
	@tk_type int,
	@tk_shipper uniqueidentifier,
	@tk_product uniqueidentifier,
	@tk_batch_id nvarchar(200),
	--@tk_to_key uniqueidentifier,
	--@tk_from_key uniqueidentifier,
	@tk_source_tank_key uniqueidentifier = null,
	@tk_destination_tank_key uniqueidentifier = null,
	@tk_berth_key uniqueidentifier = null,
	@tk_pipeline_key uniqueidentifier = null,
	@tk_start_datetime datetime,
	@tk_end_datetime datetime,
	@tk_scheduled_volume float, 
	@user nvarchar(50) = ''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from tickets
--Where _key = @row

IF(@row IN (SELECT _key from tickets))
BEGIN
-- it exists already so update it 

--record info update 
DECLARE @json varchar(max)
SET @json = (SELECT tk_record_info FROM tickets WHERE _key = @row)
SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())

UPDATE tickets

	SET tk_type = @tk_type,
		tk_entity1_key = @tk_shipper,
		tk_product = @tk_product,
		tk_batch_id = @tk_batch_id,
		tk_scheduled_start_datetime = @tk_start_datetime,
		tk_start_datetime = @tk_start_datetime,
		tk_scheduled_end_datetime = @tk_end_datetime,
		tk_end_datetime = @tk_end_datetime,
		--tk_to_key = @tk_to_key,
		--tk_from_key = @tk_from_key,
		tk_source_tank_key = @tk_source_tank_key,
		tk_destination_tank_key = @tk_destination_tank_key,
		tk_berth_key = @tk_berth_key,
		tk_pipeline_key = @tk_pipeline_key,
		tk_scheduled_volume = @tk_scheduled_volume,
		tk_eod_datetime = dbo.get_eod_datetime(@tk_start_datetime)
		
WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

INSERT INTO [dbo].[tickets]
           ([_key]
           ,[tk_type]
           ,[tk_status]
           ,[tk_entity1_key]
           ,[tk_product]
           ,[tk_batch_id]
           ,[tk_eod_datetime]
           --,[tk_from_key]
           --,[tk_to_key]
		   ,[tk_source_tank_key]
		   ,[tk_destination_tank_key]
		   ,[tk_berth_key]
		   ,[tk_pipeline_key]
           ,[tk_scheduled_start_datetime]
           ,[tk_scheduled_end_datetime]
           ,[tk_start_datetime]
           ,[tk_end_datetime]
           ,[tk_volume_uom]
           ,[tk_scheduled_volume]
           ,[tk_record_info])
     VALUES
           (@row
           ,@tk_type
           ,1 -- scheduled status 
           ,@tk_shipper
           ,@tk_product
           ,@tk_batch_id
           ,dbo.get_eod_datetime(@tk_start_datetime)
           --,@tk_from_key
           --,@tk_to_key
		   ,@tk_source_tank_key
		   ,@tk_destination_tank_key
		   ,@tk_berth_key
		   ,@tk_pipeline_key
           ,@tk_start_datetime
           ,@tk_end_datetime
           ,@tk_start_datetime
           ,@tk_end_datetime
           ,1
           ,@tk_scheduled_volume
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
GO
