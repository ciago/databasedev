--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_marine_agent_invoices_add_agent]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Add an additional row of the selected agent invoices record with an empty marine agent guid.

/*EXEC mgn_marine_agent_invoices_add_agent
	@row = 'f7d2cbad-d34c-49ce-8147-3a1f86252849'

	// 
	*/
-- =============================================
CREATE	 OR ALTER PROCEDURE [dbo].[mgn_marine_agent_invoices_add_agent]
	@row uniqueidentifier,
	@user nvarchar(50) = 'candice'
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from vessel_invoice
--Where _key = @row

-- declaring the new_id rather than having the table automatically generate one, so that we can make sure the new vessel_invoice is linked to the invoice_charge table
declare @new_id uniqueidentifier = newid()

	INSERT INTO [dbo].[vessel_invoice]
			([_key]
			,vi_synthesis_ticket_key
			,vi_shipper_key
			,vi_marine_agent_key
			,vi_vessel_invoice_status_key
			,[vi_vessel_status]
			,[vi_invoice_total]
			,[vi_invoice_sent_bool]
			,[vi_accounting_date]
			,[vi_invoice_date]
			,[vi_payment_date]
			,[vi_time_in_berth])
     
			(SELECT @new_id
			,vi_synthesis_ticket_key
			,vi_shipper_key
			,null
			,vi_vessel_invoice_status_key
			,[vi_vessel_status]
			,[vi_invoice_total]
			,[vi_invoice_sent_bool]
			,[vi_accounting_date]
			,[vi_invoice_date]
			,[vi_payment_date]
			,[vi_time_in_berth]
			FROM vessel_invoice 
			WHERE _key = @row)

declare @charge_date datetime = (select syn.[syn_ticket_start_datetime]
								 from vessel_invoice vi left outer join
									  synthesis_tickets as syn on vi.vi_synthesis_ticket_key = syn._key
								 where vi._key = newid())
	
	Insert Into [dbo].[invoice_charge]
		   ([_key]
		   ,[ic_vessel_invoice_key]
		   ,[ic_charges_key]
		   ,[ic_charge_date]
		   ,[ic_charge_bool]
		   ,[ic_unit_amount]
		   ,[ic_charge_total]
		   ,[ic_charge_override]
		   ,[ic_record_info])

		   (Select newid()
		   ,@new_id
		   ,[_key]
		   ,@charge_date
		   ,0
		   ,0
		   ,0
		   ,0
		   ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   from charges as c
		   where (@charge_date >= c.[start_date] or c.[start_date] is null) and
				 (@charge_date <= c.[end_date]) or c.[end_date] is null)
		   

END


GO
