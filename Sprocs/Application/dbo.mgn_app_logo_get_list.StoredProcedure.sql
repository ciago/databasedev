USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_app_logo_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table

/*EXEC [mgn_accounting_vessel_types_get_list]
	@site  = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
	@user  = 'candice'
	*/
-- =============================================
Create         PROCEDURE [dbo].[mgn_app_logo_get_list]
	/*Defining the variable that I will eventually be filtering the table by*/
	@company_key uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--Where _key = @volume_type

--DECLARE @eod datetime = SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date), 07, 00)

Select al.al_logo_data, 
	   al.al_image_name

from app_logo as al

WHERE al.al_associated_app = @company_key
--ORDER BY berth_description, valve_start
	
END
GO
