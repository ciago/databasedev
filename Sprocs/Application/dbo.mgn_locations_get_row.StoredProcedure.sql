--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_locations_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Candice Iago    
-- Create date: 5/15/2020
-- Description:    gets a list of vessel activity records for the selected date

 

/*EXEC mgn_locations_get_row
    @row  = '4FEF705E-F2F1-4278-89A9-0DB064149B33'
    */
-- =============================================
create   OR ALTER  PROCEDURE [dbo].[mgn_locations_get_row]
    @row uniqueidentifier = null
    --, @type (pipeline, vessel, tank to tank transfer
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row

 

SELECT ln._key,
	   ln.ln_location_type as [location_type],
	   ln.ln_location_name as [location_name],
	   ISNULL(ln.ln_external_id,'') as [external_id],
	   ISNULL(ln.ln_e1_key,'00000000-0000-0000-0000-000000000000') as [e1_key],
	   ISNULL(et1.type_description,'') as [entity_type1],
	   ISNULL(ln.ln_e2_key,'00000000-0000-0000-0000-000000000000') as [e2_key],
	   ISNULL(et2.type_description,'') as [entity_type2],
	   ISNULL(ln.ln_e3_key,'00000000-0000-0000-0000-000000000000') as [e3_key],
	   ISNULL(et3.type_description,'') as [entity_type3],
	   ln.ln_offload as [offload],
	   ln.ln_load as [load],
	   ln.ln_site_key as [site],
	   ln.ln_active as [active],
	   ISNULL(p.product_name, '') as product_name,
	   ISNULL(ln.ln_product_key,'00000000-0000-0000-0000-000000000000') as ln_product_key
      
FROM locations as ln left outer join
	 location_types as lt on ln.ln_location_type = lt.lt_location_type left outer join
	 entities e1 ON e1._key = ln.ln_e1_key LEFT OUTER JOIN
	 entities e2 ON e2._key = ln.ln_e2_key LEFT OUTER JOIN
	 entities e3 ON e3._key = ln.ln_e3_key left outer join
	 entity_types et1 on lt.lt_e1_type = et1.type_code left outer join
	 entity_types et2 on lt.lt_e2_type = et2.type_code left outer join
	 entity_types et3 on lt.lt_e3_type = et3.type_code LEFT OUTER JOIN 
	 products p ON ln.ln_product_key = p._key

WHERE ln._key = @row
    

 

END
 
GO
