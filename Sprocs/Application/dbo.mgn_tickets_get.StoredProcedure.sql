--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tickets_get]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Candice Iago    
-- Create date: 5/15/2020
-- Description:    gets a list of vessel activity records for the selected date





/*EXEC mgn_tickets_get
    @row  = 'c75ba23e-f036-416f-bfea-013ebf7e8fa5'
    */
-- =============================================
CREATE  OR ALTER     PROCEDURE [dbo].[mgn_tickets_get]
    @row uniqueidentifier = null
    --, @type (pipeline, vessel, tank to tank transfer
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row





SELECT t._key, 
      t.tk_type, -- type
      ts.stat_type as stat_type,
      t.tk_product as product_key,
      t.tk_ticket_id,
      isnull(t.tk_nomination_number,0) as tk_nomination_number,
      ISNULL(t.tk_batch_id, '') as [tk_batch_id], -- batch id
      ISNULL(t.tk_scheduled_start_datetime, DATEADD(DAY, -1, t.tk_eod_datetime)) as tk_sched_start_datetime,  -- start
      ISNULL(t.tk_scheduled_end_datetime, t.tk_eod_datetime) as tk_sched_end_datetime,-- end
      ISNULL(t.tk_start_datetime, DATEADD(DAY, -1, t.tk_eod_datetime)) as tk_start_datetime,  -- start
      ISNULL(t.tk_end_datetime, t.tk_eod_datetime) as tk_end_datetime,-- end
      ROUND( ISNULL(t.tk_final_gsv,0), 2) as tk_final_gsv, -- volume 
      ROUND( ISNULL(t.tk_final_nsv,0), 2) as tk_final_nsv, -- volume 
      t.tk_eod_datetime, 
      t.tk_scheduled_volume,
      ISNULL(t.tk_volume_uom,0) as tk_volume_uom,
      ISNULL(t.tk_source_tank_key,'00000000-0000-0000-0000-000000000000') as source_tank_key,
      ISNULL(t.tk_destination_tank_key,'00000000-0000-0000-0000-000000000000') as destination_tank_key,
      ISNULL(t.tk_berth_key,'00000000-0000-0000-0000-000000000000') as berth_key,
      ISNULL(t.tk_pipeline_key,'00000000-0000-0000-0000-000000000000') as pipeline_key,
      ISNULL(t.tk_vehicle_key,'00000000-0000-0000-0000-000000000000') as vehicle_key,
      ISNULL(t.tk_vessel_key,'00000000-0000-0000-0000-000000000000') as vessel_key,
      ISNULL(t.tk_truck_lane_key,'00000000-0000-0000-0000-000000000000') as truck_lane_key,
      ISNULL(t.tk_entity1_key,'00000000-0000-0000-0000-000000000000') as entity1_key,
      ISNULL(t.tk_entity2_key,'00000000-0000-0000-0000-000000000000') as entity2_key,
      ISNULL(t.tk_entity3_key,'00000000-0000-0000-0000-000000000000') as entity3_key,
      ISNULL(t.tk_entity4_key,'00000000-0000-0000-0000-000000000000') as entity4_key,
      ISNULL(t.tk_entity5_key,'00000000-0000-0000-0000-000000000000') as entity5_key,
      ISNULL(t.tk_entity6_key,'00000000-0000-0000-0000-000000000000') as entity6_key,
      ISNULL(t.tk_entity7_key,'00000000-0000-0000-0000-000000000000') as entity7_key,
      t.tk_site_key,
     -- CASE WHEN t.[tk_source_tank_key] IS NULL THEN COALESCE(b.berth_description, pl.[description]) ELSE from_t.tank_name END as from_location , 
      --CASE WHEN t.tk_destination_tank_key IS NULL THEN COALESCE(b.berth_description, pl.[description]) ELSE to_t.tank_name END as to_location ,
      e1.name as entity1_name, 
	  tt.type_description as type_description, 
	  ts.stat_description as stat_description, 
	  p.product_code as product_code,
	  ISNULL(tm.tm_driver_name,'') as tm_driver_name,
	  ISNULL(tm.tm_lease,'') as tm_lease,
	  ISNULL(tm.tm_trailer_name,'') as tm_trailer_name,
	  ISNULL(tm.tm_truck_name,'') as tm_truck_name
      
FROM tickets t INNER JOIN 
     ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
     ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN 
     --entities e ON e._key = t.tk_shipper LEFT OUTER JOIN 
     products p On t.tk_product = p._key LEFT OUTER JOIN 
     --tanks from_t ON from_t._key = tk_from_key LEFT OUTER JOIN 
     --pipeline from_p ON from_p._key = t.tk_from_key LEFT OUTER JOIN 
     --berths from_b ON from_b._key = t.tk_from_key LEFT OUTER JOIN 
     --tanks to_t ON to_t._key = tk_to_key LEFT OUTER JOIN 
     --pipeline to_p ON to_p._key = t.tk_to_key LEFT OUTER JOIN 
     --berths to_b ON to_b._key = t.tk_to_key LEFT OUTER JOIN 
     ticket_type_fields ttf on ttf.type_fie_ticket_type = tt._key LEFT OUTER JOIN
     tanks from_t ON from_t._key = tk_source_tank_key LEFT OUTER JOIN 
     pipeline pl ON pl._key = t.tk_pipeline_key LEFT OUTER JOIN 
     berths b ON b._key = t.tk_berth_key LEFT OUTER JOIN 
     tanks to_t ON to_t._key = tk_destination_tank_key  LEFT OUTER JOIN 
     entities e1 ON e1._key = t.tk_entity1_key LEFT OUTER JOIN 
	 tickets_misc tm ON tm.tm_ticket_key = t._key
WHERE t._key = @row
    





END
 


GO
