--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_berth_get_list_operator]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create   OR ALTER    PROCEDURE [dbo].[mgn_berth_get_list_operator] 
	--@terminal uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT _key, berth_description
	FROM berths
	WHERE berth_external_id is NOT NULL
	ORDER BY berth_external_id

END
GO
