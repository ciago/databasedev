--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tickets_approve]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   OR ALTER  PROCEDURE [dbo].[mgn_tickets_approve]
 @eod datetime 
	
AS	

/* 
declare @date datetime = DATETIMEFROMPARTS(2020, 05, 29, 7, 0, 0, 0)
EXEC mgn_approve_tickets  @eod = @date  
								
	
*/
BEGIN


--SET @eod  = DATEADD(DAY, 1, SMALLDATETIMEFROMPARTS(YEAR(@eod), MONTH(@eod), DAY(@eod) , 07, 00))
DECLARE @eod_time int = (SELECT TOP(1) eod_time FROM site_configuration)
SET @eod = DATEADD(HOUR, @eod_time, @eod) 
SET @eod  = dbo.get_eod_datetime(@eod)

UPDATE tickets
SET tk_start_datetime = DATEADD(DAY, -1, @eod)
WHERE tk_start_datetime IS NULL AND tk_eod_datetime = @eod

UPDATE tickets
SET tk_end_datetime = @eod
WHERE tk_end_datetime IS NULL AND tk_eod_datetime = @eod


--DECLARE @eod datetime  = DATETIMEFROMPARTS( DATEPART(YEAR, getdate()), DATEPART(MONTH, getdate()), DATEPART(DAY, getdate()), 7, 0, 0, 0)
--DECLARE @eod datetime  = DATETIMEFROMPARTS( 2020, 05, 28, 7, 0, 0, 0)

-- update vessel tickets to match what is in the customer order 
UPDATE t
SET t.tk_product =  syn_prod._key
FROM tickets t INNER JOIN 
products p ON t.tk_product = p._key LEFT OUTER  JOIN
	synthesis_tickets st ON t.tk_batch_id = st.syn_customer_ref LEFT OUTER JOIN 
	products syn_prod ON syn_prod.product_external_id = st.syn_product_code
	--[Moda_Final_Tickets] ft ON t._key = ft._key
WHERE tk_type = 2 AND st.syn_product_code <>  p.product_external_id
	  AND tk_eod_datetime = @eod AND syn_prod._key IS NOT NULL





--delete from Moda_Final_Tickets WHERE eod_datetime = @eod 


declare @all TABLE (batchid nvarchar(200), open_gsv float, close_gsv float, open_date datetime, close_date datetime, tank_key uniqueidentifier, gsv float, gsv_calculated float)

declare @current TABLE (batchid nvarchar(200), open_gsv float, close_gsv float, tank_key uniqueidentifier, 
						gsv float, gsv_calculated float, eod_datetime datetime, 
						row_key uniqueidentifier, prev_open float, new_close float, gsv2 float)


INSERT INTO @current (batchid , open_gsv , close_gsv , 
					  tank_key , gsv , gsv_calculated , 
					  eod_datetime , row_key)
SELECT  tk_batch_id, ISNULL(tk_tank_opening_gross_source, 450000), ISNULL(tk_tank_closing_gross_source, 450000 - tk_final_gsv) , 
	   tk_source_tank_key, tk_final_gsv, NULL, 
	   tk_eod_datetime,  t._key
FROM tickets t INNER JOIN 
	 tickets_tank_gauge tt ON t._key = tt.tk_ticket_key AND tt.tk_tank_source_key = t.tk_source_tank_key
WHERE tk_eod_datetime = @eod
	  AND tk_type = 2


INSERT INTO @all (batchid , open_gsv , close_gsv , open_date , close_date , tank_key, gsv, gsv_calculated)
select mt.batch_id, mt.tank_open_gsv, mt.tank_close_gsv, mt.start_datetime, mt.stop_datetime , t.tk_source_tank_key, t.tk_final_gsv,  mt.tank_open_gsv - mt.tank_close_gsv as calc_gsv
FROM Moda_Final_Tickets mt INNER JOIN 
	 tickets t on mt._key = t._key
WHERE mt.batch_id IN (SELECT batchid FROM @current) 
	  AND tk_type = 2
	  AND eod_datetime < @eod


INSERT INTO @all (batchid , open_gsv , close_gsv , open_date , close_date , tank_key, gsv, gsv_calculated)
SELECT tk_batch_id, 
	   tt.tk_tank_opening_gross_source, 
	   tt.tk_tank_closing_gross_source, 
	   tk_start_datetime,
	   tk_end_datetime,
	   t.tk_source_tank_key, 
	   tk_final_gsv, 
	   tk_final_gsv
FROM tickets t INNER JOIN 
	 tickets_tank_gauge tt ON t._key = tt.tk_ticket_key AND tt.tk_tank_source_key = t.tk_source_tank_key
WHERE tk_eod_datetime = @eod
	  AND tk_type = 2



UPDATE c
SET gsv_calculated =  a.gsv,
	prev_open = a.open_gsv
FROM 
	(SELECT SUM(a.gsv) as gsv, a.tank_key,  a.batchid, MAX(a.open_gsv) as open_gsv
	FROM @all a INNER JOIN 
	 @current c ON a.batchid = c.batchid AND a.tank_key = c.tank_key
GROUP BY a.tank_key, a.batchid) a INNER JOIN 
@current c ON  a.batchid = c.batchid AND a.tank_key = c.tank_key 

UPDATE @current
SET new_close = prev_open - gsv_calculated
--WHERE prev_open <> open_gsv


UPDATE @current
SET gsv2 = open_gsv - new_close 

UPDATE @current
SET open_gsv = new_close + gsv
WHERE gsv <> gsv2

--select * from @all
--SELECT * FROM @current



INSERT INTO [dbo].[Moda_Final_Tickets]
           ([_key]
           ,[movement_type]
           ,[batch_id]
		   , ticket_id
           ,[from_location_id]
           ,[to_location_id]
           ,[product_id]
           ,[gsv]
           ,[start_datetime]
           ,[stop_datetime]
           ,[eod_datetime]
           ,[tank_open_gsv]
           ,[tank_open_level]
           ,[tank_close_gsv]
           ,[tank_close_level]
		   
        )
select row_key, 
	   tt.type_external_code as [type],
	   t.batchid, 
	   tic.tk_ticket_id,
	  ISNULL(ISNULL(out_pipe.external_id, out_tank.tank_external_id_location), out_berth.berth_external_code) as from_location_code,
	  ISNULL(ISNULL(in_pipe.external_id, in_tank.tank_external_id_location), in_berth.berth_external_code) as to_location_code,
	   p.product_external_id as product, 
	   t.gsv, 
	   ISNULL(tic.tk_start_datetime, DATEADD(DAY, -1, tic.tk_eod_datetime)), 
	   ISNULL(tic.tk_end_datetime, tic.tk_eod_datetime), 
	   tic.tk_eod_datetime, 
	   t.open_gsv, 
	   t2.tk_tank_opening_level_source, 
	   ISNULL(t.new_close,t.open_gsv - t.gsv) , 
	   t2.tk_tank_closing_level_source
	   --,  t.open_gsv - t.new_close
from @current t INNER JOIN 
	tickets tic ON tic._key = t.row_key INNER JOIN 
	tickets_tank_gauge t2 ON tic._key = t2.tk_ticket_key INNER JOIN 
	 ticket_type tt ON tic.tk_type = tt.typ_type INNER JOIN 
	 products p ON p._key = tic.tk_product LEFT OUTER JOIN 
	 pipeline in_pipe ON in_pipe._key  = tic.tk_pipeline_key LEFT OUTER JOIN 
	 pipeline out_pipe ON out_pipe._key = tic.tk_pipeline_key LEFT OUTER JOIN 
	 tanks in_tank ON in_tank._key = tic.tk_destination_tank_key LEFT OUTER JOIN 
	 tanks out_tank ON out_tank._key = tic.tk_source_tank_key LEFT OUTER JOIN 
	 berths in_berth ON in_berth._key = tic.tk_berth_key LEFT OUTER JOIN 
     berths out_berth ON out_berth._key = tic.tk_berth_key
WHERE tic.tk_type = 2 AND tk_eod_datetime  = @eod 
	  AND row_key NOT IN (SELECT _key FROM Moda_Final_Tickets WHERE eod_datetime = @eod)




-- PIPELINE
UPDATE tickets
SET tk_batch_id = tk_ticket_id
WHERE tk_eod_datetime = @eod AND tk_type = 1 

INSERT INTO [dbo].[Moda_Final_Tickets]
           ([_key]
           ,[movement_type]
           ,[batch_id]
		   ,ticket_id
           ,[from_location_id]
           ,[to_location_id]
           ,[product_id]
           ,[gsv]
           ,[start_datetime]
           ,[stop_datetime]
           ,[eod_datetime]
           ,[tank_open_gsv]
           ,[tank_open_level]
           ,[tank_close_gsv]
           ,[tank_close_level]
		   
        )

SELECT  t._key,
	   tt.type_external_code as [type],
	   t.tk_batch_id, 
	   t.tk_ticket_id,
	  ISNULL(ISNULL(out_pipe.external_id, out_tank.tank_external_id_location), out_berth.berth_external_code) as from_location_code,
	   in_tank.tank_external_id_location as to_location_code,
	   p.product_external_id as product, 
	   t.tk_final_gsv, 
	   t.tk_start_datetime, 
	   t.tk_end_datetime, 
	   t.tk_eod_datetime, 
	    t2.tk_tank_closing_gross_destination - t.tk_final_gsv,-- t.tk_tank_open_gsv, 
	   t2.tk_tank_opening_level_destination, 
	   t2.tk_tank_closing_gross_destination,  --t.tk_tank_close_gsv, 
	   t2.tk_tank_closing_level_destination
FROM tickets t  INNER JOIN 
	 tickets_tank_gauge t2 ON t._key = t2.tk_ticket_key INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 products p ON p._key = t.tk_product LEFT OUTER JOIN 
	 pipeline in_pipe ON in_pipe._key  = t.tk_pipeline_key LEFT OUTER JOIN 
	 pipeline out_pipe ON out_pipe._key = t.tk_pipeline_key LEFT OUTER JOIN 
	 tanks in_tank ON in_tank._key = t.tk_destination_tank_key LEFT OUTER JOIN 
	 tanks out_tank ON out_tank._key = t.tk_source_tank_key LEFT OUTER JOIN 
	 berths in_berth ON in_berth._key = t.tk_berth_key LEFT OUTER JOIN 
     berths out_berth ON out_berth._key = t.tk_berth_key
WHERE t.tk_type = 1 AND tk_eod_datetime  = @eod
	  AND t._key NOT IN (SELECT _key FROM Moda_Final_Tickets WHERE eod_datetime = @eod)

--UPDATE Moda_Final_Tickets 
--SET batch_id = ticket_id 
--WHERE eod_datetime = @eod AND movement_type = 'PipelineToTank'


--**************************Transfers**********************************
INSERT INTO [dbo].[Moda_Final_Tickets]
           ([_key]
           ,[movement_type]
           ,[batch_id]
		   , ticket_id
           ,[from_location_id]
           ,[to_location_id]
           ,[product_id]
           ,[gsv]
           ,[start_datetime]
           ,[stop_datetime]
           ,[eod_datetime]
           ,[tank_open_gsv]
           ,[tank_open_level]
           ,[tank_close_gsv]
           ,[tank_close_level]
        )
		
SELECT  t._key,
	   tt.type_external_code as [type],
	   t.tk_batch_id, 
	   t.tk_ticket_id,
	   out_tank.tank_external_id_location as from_location_code,
	   NULL as to_location_code,
	   p.product_external_id as product, 
	   ISNULL(t.tk_final_gsv,0) as tk_final_gsv, 
	   t.tk_start_datetime, 
	   t.tk_end_datetime, 
	   t.tk_eod_datetime, 
	   ISNULL(t2.tk_tank_opening_gross_source,t.tk_final_gsv) as tk_tank_opening_gross_source, 
	   ISNULL(t2.tk_tank_opening_level_source,0) as tk_tank_opening_level_source, 
	   ISNULL(t2.tk_tank_closing_gross_source,0) as tk_tank_closing_gross_source, 
	   ISNULL(t2.tk_tank_closing_level_source,0) as tk_tank_closing_level_source
FROM tickets t INNER JOIN 
	 tickets_tank_gauge t2 ON t._key = t2.tk_ticket_key INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 products p ON p._key = t.tk_product LEFT OUTER JOIN 
	 tanks out_tank ON out_tank._key = t.tk_source_tank_key
WHERE t.tk_type  = 3 AND tk_eod_datetime  = @eod
	  AND t._key NOT IN (SELECT _key FROM Moda_Final_Tickets WHERE eod_datetime = @eod)


INSERT INTO [dbo].[Moda_Final_Tickets]
           ([_key]
           ,[movement_type]
           ,[batch_id]
		   ,ticket_id
           ,[from_location_id]
           ,[to_location_id]
           ,[product_id]
           ,[gsv]
           ,[start_datetime]
           ,[stop_datetime]
           ,[eod_datetime]
           ,[tank_open_gsv]
           ,[tank_open_level]
           ,[tank_close_gsv]
           ,[tank_close_level]
        )
SELECT  t._key,
	   tt.type_external_code as [type],
	   t.tk_batch_id, 
	   t.tk_ticket_id,
	   NULL as from_location_code,
	   in_tank.tank_external_id_location as to_location_code,
	   p.product_external_id as product, 
	   ISNULL(t.tk_final_gsv,0) as tk_final_gsv, 
	   t.tk_start_datetime, 
	   t.tk_end_datetime, 
	   t.tk_eod_datetime, 
	   ISNULL(t2.tk_tank_opening_gross_destination,0) as tk_tank_opening_gross_destination, 
	   ISNULL(t2.tk_tank_opening_level_destination,0) as tk_tank_opening_level_destination, 
	   ISNULL(t2.tk_tank_closing_gross_destination,t.tk_final_gsv) as tk_tank_closing_gross_destination, 
	   ISNULL(t2.tk_tank_closing_level_destination,0) as tk_tank_closing_level_destination
FROM tickets t INNER JOIN 
	 tickets_tank_gauge t2 ON t._key = t2.tk_ticket_key INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 products p ON p._key = t.tk_product LEFT OUTER JOIN 
	 tanks in_tank ON in_tank._key = t.tk_destination_tank_key
WHERE t.tk_type  = 3 AND tk_eod_datetime  = @eod
		AND (SELECT COUNT(_key) FROM Moda_Final_Tickets WHERE batch_id = t.tk_batch_id) < 2



--INSERT INTO EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets]
--           ([_key]
--           ,[movement_type]
--           ,[batch_id]
--		   ,ticket_id
--           ,[from_location_id]
--           ,[to_location_id]
--           ,[product_id]
--           ,[gsv]
--           ,[start_datetime]
--           ,[stop_datetime]
--           ,[eod_datetime]
--           ,[tank_open_gsv]
--           ,[tank_open_level]
--           ,[tank_close_gsv]
--           ,[tank_close_level]
--		   ,site_code)
--   SELECT[_key]
--           ,[movement_type]
--           ,[batch_id]
--		   ,ticket_id
--           ,[from_location_id]
--           ,[to_location_id]
--           ,[product_id]
--           ,[gsv]
--           ,[start_datetime]
--           ,[stop_datetime]
--           ,[eod_datetime]
--           ,ISNULL([tank_open_gsv],0)
--           ,ISNULL([tank_open_level],0)
--           ,ISNULL([tank_close_gsv],0)
--           ,ISNULL([tank_close_level],0)
--		   ,ISNULL(site_code,100)
--	FROM [Moda_Final_Tickets]
--	WHERE eod_datetime = @eod AND _key NOT IN (SELECT _key FROM EMERSON.Moda_Interface.[dbo].[Moda_Final_Tickets] WHERE eod_datetime = @eod)
	


	
Update tickets
SET tk_status = 4
Where tk_eod_datetime = @eod

--UPDATE tickets 
--SET tk_ticket_id = mt.ticket_id
--FROM tickets t INNER JOIN 
--	 Moda_Final_Tickets mt ON t._key = mt._key
--Where tk_eod_datetime = @eod

--UPDATE tickets 
--SET tk_batch_id = mt.batch_id
--FROM tickets t INNER JOIN 
--	 Moda_Final_Tickets mt ON t._key = mt._key
--Where tk_eod_datetime = @eod AND t.tk_batch_id = ''



-- send the tank inventories 
--INSERT INTO EMERSON.Moda_Interface.[dbo].[tank_readings]
--           ([_key]
--           ,tank_key
--           ,[tank_timestamp]
--		   ,tank_level
--		   ,tank_nsv
--		   ,tank_gsv
--		   ,tank_tov
--		   ,tank_external_id
--		   ,site_code)
--SELECT ta._key, 
--	   t._key,
--	   @eod, 
--	   0, 
--	   ISNULL(ta.close_gsv,0), 
--	   ISNULL(ta.close_gsv,0),
--	   ISNULL(ta.close_tov,0),
--	   t.tank_external_id_inventory,
--	   100
--FROM tank_actuals ta LEFT OUTER JOIN 
--	 tanks t ON ta.tank_key = t._key
--WHERE ta.eod_datetime = @eod AND t.tank_virtual = 0 AND t.active = 1
--      AND ta._key NOT IN (SELECT [_key] FROM EMERSON.Moda_Interface.[dbo].[tank_readings])
--	  AND ta.eod_datetime > '2021-02-08 07:00' -- the date we switched from auto sending to having brandi team approve this first







	END
GO
