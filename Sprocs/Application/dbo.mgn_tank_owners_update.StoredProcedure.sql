--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_owners_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Update selected berth fees record with new user input details.


-- =============================================
CREATE    OR ALTER   PROCEDURE [dbo].[mgn_tank_owners_update]

	@key uniqueidentifier,
    @tank_key uniqueidentifier,
	@owner_key uniqueidentifier,
	@start_date datetime,
	@end_date datetime = null,
	@bill_cap bigint,
	@site_key uniqueidentifier,
	@deleted bit,
	@user nvarchar(50) = null
	
	/*
	EXEC mgn_tank_owners_update 
	@key = '9f0b321f-75ba-40fc-8cbe-59b720e9dd5a',
    @tank_key = '847b3f4e-aa0c-465d-946d-1ed49bf3714c',
	@owner_key = 'dd85132f-4039-4fda-8fcc-bbd2dd9b7656',
	@start_date = '01-02-2021',
	@bill_cap = 10101,
	@ext_row_id = 10101,
	@site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@deleted = 0,
	@user = 'candice'
	*/

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

IF(@key IN (SELECT _key from tank_owner))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM tank_owner WHERE _key = @key)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE tank_owner

SET		[_key] = @key,
		[tank_key] = @tank_key,
		[tank_owner_key] = @owner_key,
		[tk_start_datetime] = @start_date,
		[tk_end_datetime] = @end_date,
		[capacity_for_billing] = @bill_cap,
		[site_key] = @site_key,
		[deleted] = @deleted

WHERE _key = @key

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[tank_owner]
           ([_key]
           ,[tank_key]
           ,[tank_owner_key]
           ,[tk_start_datetime]
           ,[tk_end_datetime]
		   ,[capacity_for_billing]
           ,[site_key]
		   ,[deleted]
		   ,[record_info])
     VALUES
           (@key
		   ,@tank_key
           ,@owner_key
           ,@start_date
           ,@end_date
           ,@bill_cap
		   ,@site_key
		   ,@deleted
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE()))

END

END
GO
