--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_schedule_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected schedule with new user input details.

/*EXEC mgn_schedule_update @row = '69CC92F5-2277-4B90-B07F-654EA6D99610',
						   @startDatetime = '2020-05-01T07:00:00.000',
						   @stopDatetime = '2020-05-02T04:27:00.000',
						   @evtVolume = 123456,
						   @rateBPH = 9876,
						   @pipeline_key = 'fc536541-5a92-466a-bcd1-db7fedcd15e7',
                           @product_key = '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',
						   @shipper_key = 'd25051f3-4888-4bfb-89fd-4e1449ac0fba',
						   @user = 'Candice'
	// 514942
	*/
-- =============================================
CREATE     OR ALTER PROCEDURE [dbo].[mgn_schedule_update]
	@row uniqueidentifier,
	@startDatetime datetime,
	@stopDatetime datetime,
	@evtVolume float,
	@rateBPH float,
	@pipeline_key uniqueidentifier,
	@product_key uniqueidentifier,
	@shipper_key uniqueidentifier = null,
	@user varchar(50) = ''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from schedule
--Where _key = @row

IF(@row IN (SELECT _key from import_schedule))
BEGIN
-- it exists already so update it 

--record info update 
DECLARE @json varchar(max)
SET @json = (SELECT record_info FROM import_schedule WHERE _key = @row)
SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())

UPDATE import_schedule

	SET start_datetime = @startDatetime,
		stop_datetime = @stopDatetime,
		evt_volume = @evtVolume,
		rate_bbls_hour = @rateBPH,
		pipeline_key = @pipeline_key,
		product_key = @product_key,
		shipper_key = @shipper_key,
		record_info = @json

WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[import_schedule]
           ([_key]
		   ,[start_datetime]
		   ,[stop_datetime]
		   ,[evt_volume]
		   ,[rate_bbls_hour]
		   ,[pipeline_key]
		   ,[product_key]
		   ,[shipper_key]
           ,[record_info])
     VALUES
           (@row
		   ,@startDatetime
		   ,@stopDatetime
		   ,@evtVolume
		   ,@rateBPH
		   ,@pipeline_key
		   ,@product_key
		   ,@shipper_key
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
GO
