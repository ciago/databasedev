USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_contracts_update]    Script Date: 5/13/2021 9:13:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER         PROCEDURE [dbo].[mgn_contracts_update]

	@row uniqueidentifier,
	@entity_key uniqueidentifier,
	@description nvarchar(50),
	@status int,
	@type uniqueidentifier,
	@start_date datetime,
	@end_date datetime = null,
	@comment nvarchar(MAX) = null,
	@site uniqueidentifier,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from contacts
--Where _key = @row

IF(@row IN (SELECT _key from contracts))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json_upd varchar(max)
	SET @json_upd = (SELECT record_info FROM contacts WHERE _key = @row)
	SET @json_upd = dbo.fn_json_record_info(@json_upd, 'updated', @user, HOST_NAME(), GETDATE())

	DECLARE @json_del varchar(max)
	SET @json_del = (SELECT record_info FROM contacts WHERE _key = @row)
	SET @json_del = dbo.fn_json_record_info(@json_del, 'deleted', @user, HOST_NAME(), GETDATE())

	DECLARE @json_arc varchar(max)
	SET @json_arc = (SELECT record_info FROM contacts WHERE _key = @row)
	SET @json_arc = dbo.fn_json_record_info(@json_arc, 'archived', @user, HOST_NAME(), GETDATE())

	DECLARE @json_res varchar(max)
	SET @json_res = (SELECT record_info FROM contacts WHERE _key = @row)
	SET @json_res = dbo.fn_json_record_info(@json_res, 'restored', @user, HOST_NAME(), GETDATE())
	
	
	UPDATE contracts

	SET		[ctr_contract_status] = @status,
			[ctr_contract_type] = @type,
			[ctr_entity] = @entity_key,
			[ctr_start_date] = @start_date,
			[ctr_end_date] = @end_date,
			[ctr_description] = @description,
			[ctr_comments] = @comment,
			ctr_record_info = case when @status = 3 /*deleted*/ then @json_del else @json_upd end,
			[ctr_site] = @site

	WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[contracts]
           ([_key]
		   ,[ctr_contract_status]
		   ,[ctr_contract_type]
		   ,[ctr_entity]
		   ,[ctr_start_date]
		   ,[ctr_end_date]
		   ,[ctr_description]
		   ,[ctr_comments]
		   ,[ctr_record_info]
		   ,[ctr_site])
     VALUES
           (@row
		   ,@status
		   ,@type
		   ,@entity_key
		   ,@start_date
		   ,@end_date
		   ,@description
		   ,@comment
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   ,@site)

END

END
