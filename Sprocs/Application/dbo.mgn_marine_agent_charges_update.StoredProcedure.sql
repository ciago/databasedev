--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_marine_agent_charges_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE	 OR ALTER    PROCEDURE [dbo].[mgn_marine_agent_charges_update]

	@row uniqueidentifier,
	@charge_bool bit,
	@unit_amount float,
	@charge_total decimal(18,5),
	@charge_override bit,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from invoice_charge
--Where _key = @row

IF(@row IN (SELECT _key from invoice_charge))
BEGIN
-- it exists already so update it 

DECLARE @unit_rate float

SET @unit_rate = (SELECT charge_amount FROM invoice_charge as ic INNER JOIN
						 charges as c ON ic.ic_charges_key = c._key
				   WHERE ic._key = @row)
print @unit_rate

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT ic_record_info FROM invoice_charge WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())	

SET			@charge_total = (@unit_amount*@unit_rate)
print @charge_total

UPDATE invoice_charge

SET			ic_charge_bool = @charge_bool,
			ic_unit_amount = @unit_amount,
			ic_charge_total = @charge_total,
			ic_charge_override = @charge_override,
			ic_record_info = @json

WHERE _key = @row

END

END
GO
