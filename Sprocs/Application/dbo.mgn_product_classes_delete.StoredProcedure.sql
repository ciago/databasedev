--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_product_classes_delete]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/1/2021
-- Description:		Deletes a single row of product classes data based on the selected row/record.

/*EXEC mgn_product_classes_delete
	@row  = '8216e75a-1ece-41bd-a391-0644ff8e3457'

	*/
-- =============================================
CREATE  OR ALTER PROCEDURE [dbo].[mgn_product_classes_delete]
	@row uniqueidentifier = null,
	@user varchar(50) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from product_classes
--WHere _key = @row
--Delete From product_classes
UPDATE product_classes
SET active = 0
Where _key = @row

END
GO
