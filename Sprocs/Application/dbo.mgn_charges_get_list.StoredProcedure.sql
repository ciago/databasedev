--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_charges_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Brandon Morey    
-- Create date: 11/11/2020
-- Description:    Gets a list of marine charges from an effective date.

/*EXEC mgn_charges_get_list  
	@date = '2020-11-11 00:00:00.000'
	
    */
	
-- =============================================
CREATE    OR ALTER     PROCEDURE [dbo].[mgn_charges_get_list]

	@date datetime = null
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from charges

SELECT        c._key,
			  c.charge_description as [charge_description],
			  ISNULL(c.charge_amount, 0) as [charge_amount],
			  ct.charge_unit_description as [charge_unit_type],
			  c.charge_unit_key as [charge_unit_key],
			  c.start_date as [start_date],
			  c.end_date as [end_date]
			  
FROM		  charges AS c INNER JOIN
			  charge_unit_type AS ct ON ct._key = c.charge_unit_key

WHERE		  (c.start_date <= @date) AND (c.end_date >= @date OR c.end_date IS NULL)
	  
END
GO
