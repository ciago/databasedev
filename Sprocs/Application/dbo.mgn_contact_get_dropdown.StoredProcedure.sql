--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_contact_get_dropdown]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/2/2021
-- Description:		Select list of contacts for dropdown usage.

/*EXEC mgn_contact_get_dropdown

	*/

-- =============================================
CREATE   OR ALTER     PROCEDURE [dbo].[mgn_contact_get_dropdown]

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

	SELECT '00000000-0000-0000-0000-000000000000', ' Select Contact' as [name]
	UNION
    SELECT _key, first_name + ' ' + last_name
    FROM contacts

	ORDER BY name
    
END
GO
