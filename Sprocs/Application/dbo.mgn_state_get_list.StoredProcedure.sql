--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_state_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/25/2021
-- Description:		Select list of states for dropdown usage.

/*EXEC mgn_state_get_list

	*/

-- =============================================
CREATE      OR ALTER   PROCEDURE [dbo].[mgn_state_get_list]

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

	SELECT '00000000-0000-0000-0000-000000000000', ' Select State' as [name]
	UNION
    SELECT _key, state
    FROM states

	ORDER BY name
    
END
GO
