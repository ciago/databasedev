--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_product_class_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/01/2021
-- Description:		Select list of product classes for dropdown usage.

/*EXEC mgn_product_class_get_list

	*/
-- =============================================

CREATE   OR ALTER      PROCEDURE [dbo].[mgn_product_class_get_list] 
		@site_key uniqueidentifier = null,
		@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();

SELECT '00000000-0000-0000-0000-000000000000' as [key], ' Select Product Class' as [name]
UNION
SELECT			pc._key as [key],
				pc.product_class
FROM			product_classes pc
WHERE			pc.site_key = @site_key
ORDER BY name
    
END
	
GO
