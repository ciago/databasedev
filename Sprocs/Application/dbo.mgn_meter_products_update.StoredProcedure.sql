--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_meter_products_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/9/2021
-- Description:		Update selected meter product record with new user input details.

/*EXEC mgn_meter_products_update 
	@row = '1FB2BE99-A42D-4148-8FEE-0A0438DA471E',
    @meter_key = 'dc6794e1-ee3d-478f-bdd5-13352fc58512',
	@product_key = 'aaf2e572-0a5a-483a-8bb0-41573e832ab7',
	@mtr_start_datetime = '2020-01-01 10:01',
	@mtr_end_datetime = '2020-01-02 10:02',
	@external_row_id = 123456,
	@deleted = 0,
	@user = 'candice'

	// 
	*/
-- =============================================
CREATE OR ALTER    PROCEDURE [dbo].[mgn_meter_products_update]

	@row uniqueidentifier,	
	@meter_key uniqueidentifier,
	@product_key uniqueidentifier,
	@mtr_start_datetime datetime,
	@mtr_end_datetime datetime = NULL,
	@deleted bit,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from meter_products
--Where _key = @row

IF @row = '00000000-0000-0000-0000-000000000000'
BEGIN
	SET @row = newid()
END


IF(@row IN (SELECT _key from meter_products))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM meter_products WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE meter_products

SET		meter_key = @meter_key,
		product_key = @product_key,
		mtr_start_datetime = @mtr_start_datetime,
		mtr_end_datetime = @mtr_end_datetime,
		deleted = @deleted,
		record_info = @json

WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[meter_products]
           ([_key]
			,[meter_key]
			,[product_key]
			,[mtr_start_datetime]
			,[mtr_end_datetime]
			,[deleted]
			,[record_info])
     VALUES
           (@row
		   	,@meter_key
			,@product_key
			,@mtr_start_datetime
			,@mtr_end_datetime
			,@deleted
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
GO
