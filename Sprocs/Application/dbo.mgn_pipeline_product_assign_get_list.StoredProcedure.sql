--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_product_assign_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Brandon Morey    
-- Create date: 5/4/2020
-- Description:    Gets a list of pipeline product assignments from a selected day.

/*EXEC mgn_pipeline_product_assign_get_list  
	@pipeline = 'fc536541-5a92-466a-bcd1-db7fedcd15e7'
	
    */
	
-- =============================================
CREATE    OR ALTER   PROCEDURE [dbo].[mgn_pipeline_product_assign_get_list]

    @pipeline uniqueidentifier = null,
	@site uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from pipeline_product_assignment



IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END

SELECT        ppa._key,
			  ppa.pipeline_key as [pipeline_key],
			  ppa.product_key as [product_key],
			  ppa.start_datetime as [start_datetime],
			  ppa.end_datetime as [end_datetime],
			  pl.description as [pipeline_desc],
			  ISNULL(p.product_name, '') as [product_name]
			  
FROM pipeline_product_assignment AS ppa INNER JOIN
              pipeline AS pl ON ppa.pipeline_key = pl._key LEFT OUTER JOIN
			  products AS p ON p._key = ppa.product_key

WHERE (end_datetime IS NULL OR end_datetime > getdate())
	AND (ppa.pipeline_key = @pipeline or @pipeline is null)
	  
END
GO
