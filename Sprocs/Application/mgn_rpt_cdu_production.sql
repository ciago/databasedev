
/****** Object:  StoredProcedure [dbo].[mgn_rpt_cdu_production]    Script Date: 4/20/2021 7:59:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_cdu_production_report_get
@row = '5d55d4da-d27e-4a0a-a9af-27c4ace3a141'
 
	*/

CREATE         PROCEDURE [dbo].[mgn_rpt_cdu_production] 

@date datetime

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from tanks

SELECT       -- t._key as [_key]
			  @date as [Date]
			  --Date
			  --Crude Feed
			  --Transmix
			  --Off Spec LPG
			  --Total Feed

--Volumetric(Net Barrels):
			  --LPG Light Naphtha
			  --Heavy Naphtha
			  --Kerosene
			  --Diesel
			  --Resid Oil
			  --Total Product

			  --Gain/Loss Volume
			  --%Volume

--Mass (kPounds):
			  --Feed
			  --Product
			  --Gain/Loss Mass
			  --% Mass

--Barges:
			  --Naphtha
			  --Kerosene/Diesel
			  --Crude/Resid

			  
--FROM		  tanks AS t

--WHERE		  @row = t._key
    
END
GO