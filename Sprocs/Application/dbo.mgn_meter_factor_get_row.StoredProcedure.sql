--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_meter_factor_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/9/2021
-- Description:		Obtains a single row of meter factor data from a specified guid.

/*EXEC mgn_meter_factor_get_row
	@row  = '8500ea19-34a4-4d3e-ab4a-0bac3a9e7c1b'

	*/
-- =============================================
CREATE OR ALTER     PROCEDURE [dbo].[mgn_meter_factor_get_row]
	@row uniqueidentifier = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from meter_factor
--WHere _key = @row
   
SELECT			mf._key,
				mf.meter_key as [meter_key],
				ISNULL(m.skid_name,'') + ' ' + m.meter_name as [meter_name],
				mf.meter_factor as [meter_factor],
				mf.mtr_start_datetime as [mtr_start_datetime],
				mf.mtr_end_datetime as [mtr_end_datetime]

FROM			meter_factor AS mf INNER JOIN
				meters AS m ON m._key = mf.meter_key

WHERE			@row = mf._key
	  
END
GO
