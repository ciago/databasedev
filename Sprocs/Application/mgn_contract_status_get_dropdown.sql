
/****** Object:  StoredProcedure [dbo].[mgn_accounting_vessel_types_get_dropdown]    Script Date: 5/11/2021 2:51:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
Create or ALTER             PROCEDURE [dbo].[mgn_contract_status_get_dropdown] 
	@site uniqueidentifier = null, 
	@user varchar(50) = 'default'

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


SELECT 0 AS RowKey, 'Select Status' As description
UNION
SELECT [cs_status] as RowKey, [cs_description] as description
FROM [dbo].[contract_status]
WHERE cs_active = 1
ORDER BY RowKey

END


-- EXEC [mgn_accounting_volume_types_get_list]





