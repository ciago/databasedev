--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_entity_assign_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected pipeline entity assignment with new user input details.

/*EXEC mgn_pipeline_entity_assign_update 
	@row = 'd5ac78b1-6677-45b0-8412-b84881591f08',
	@pipeline_key = 'fc536541-5a92-466a-bcd1-db7fedcd15e7',
	@entity_key = '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',
	@start_datetime = '1/1/2020 10:00',
	@end_datetime = '1/7/2020 10:00',
	@user = 'candice'
	
	// 
	*/
-- =============================================
CREATE    OR ALTER  PROCEDURE [dbo].[mgn_pipeline_entity_assign_update]

	@row uniqueidentifier,
    @pipeline_key uniqueidentifier ,
	@entity_key uniqueidentifier,
	@start_datetime datetime,
	@end_datetime datetime =  NULL,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_actuals
--Where _key = @row

IF(@row IN (SELECT _key from pipeline_entity_assignment))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM nominations WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE pipeline_entity_assignment

SET end_datetime = @end_datetime, 
	record_info = @json

WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[pipeline_entity_assignment]
           ([_key]
           ,[pipeline_key]
           ,[entity_key]
           ,[start_datetime]
           ,[end_datetime]
           ,[record_info])
     VALUES
           (@row
           ,@pipeline_key
           ,@entity_key
           ,@start_datetime
           ,@end_datetime
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
GO
