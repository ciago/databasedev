--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_marine_agent_generated_invoices_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Brandon Morey    
-- Create date: 5/4/2020
-- Description:    Gets a list of marine agent invoices from a selected day.

/*EXEC mgn_marine_agent_invoices_get_list  
	@date = '2/1/2021',
	@marine_agent_key = '62fbdb06-d325-4e53-b503-576aea97027f',
	@vessel_status = '00000000-0000-0000-0000-000000000000'
	
	EXEC mgn_marine_agent_generated_invoices_get_list
	@vessel_status = '94A7A511-C925-4957-94C6-96D5BC08A175'
    */
	
-- =============================================
Create   OR ALTER     PROCEDURE [dbo].[mgn_marine_agent_generated_invoices_get_list]

	@marine_agent_key uniqueidentifier = null,
	@vessel_status uniqueidentifier = null
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

----Select * from synthesis_tickets

   -- ETA Date (descending order), Berth, Vessel Name, Agent, Arrived Date,
   -- Status, Volume, Type

SELECT        vi._key,--st._key,
			  vi.vi_invoice_number as [invoice_number],
			  isnull(st.syn_vessel_name,'') as [vessel_name],
			  ISNULL(st.syn_vessel_description, '') as [vessel_type],
			  isnull(e.name,'') as [marine_agent],
			  ISNULL(CONVERT(varchar, vi.vi_accounting_date, 22), '') as [arrived_date],
			  ISNULL(CONVERT(varchar, st.syn_ticket_end_datetime, 22), '') as [departed_date],
			  isnull(vis.stat_description,'') as [invoice_status],
			  ISNULL(CONVERT(varchar, vi.vi_invoice_date, 22), '') as [invoice_date],
			  ISNULL(CONVERT(varchar, vi.vi_payment_date, 22), '') as [payment_date],
			  ISNULL(CONVERT(float, vi.vi_invoice_total), 0) as [invoice_total]
			  
FROM		  vessel_invoice vi left outer join
			  synthesis_tickets AS st ON vi.vi_synthesis_ticket_key = st._key left outer join
			  vessel_invoice_status AS vis ON vi.vi_vessel_invoice_status_key = vis._key left outer JOIN
			  entities AS e ON vi.vi_marine_agent_key = e._key

WHERE		  (vi.vi_marine_agent_key = @marine_agent_key OR @marine_agent_key is null)
			  AND vi.vi_vessel_invoice_status_key = @vessel_status -- allows the user to select whatever status they want (including voided)
			  OR (@vessel_status is null AND (vis.stat_type = 4 or vis.stat_type = 6 or vis.stat_type = 7)) -- but does not automatically show voided invoices if the user wants to see all invoices
	  
END
GO
