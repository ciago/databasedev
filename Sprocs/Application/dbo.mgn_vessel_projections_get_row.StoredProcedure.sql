--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_vessel_projections_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Obtains a single row of vessel projections data from a specified guid.

/*EXEC mgn_vessel_projections_get_row
	@row  = '831d42fa-e4ba-42ee-908c-a484cc0f264f'
	*/
-- =============================================
CREATE    OR ALTER  PROCEDURE [dbo].[mgn_vessel_projections_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from 
--WHere _key = @row

--	   select one of those vessels and then a grid loads with the days info 
--declare @key uniqueidentifier = '97CC1221-F697-4B9B-9262-83254A0C4CCE'

SELECT vv._key,
	   vv.synthesis_key as [synthesis_tickets_key], 
	   st.syn_customer_ref, 
	   st.syn_vessel_eta, 
	   st.syn_vessel_name, 
	   st.syn_vessel_description, 
	   DATEADD(DAY, vv.day_number -1, st.syn_vessel_eta) as date_day_number, 
	   CONVERT(int,vv.projected_volume) as projected_volume, 
	   dbo.get_eod_datetime(DATEADD(DAY, vv.day_number -1, st.syn_vessel_eta)) as eod, 
	   vv.day_number as day_number
FROM   vessel_projected_volumes vv INNER JOIN
       synthesis_tickets st ON vv.synthesis_key = st._key

WHERE  vv._key = @row

END
GO
