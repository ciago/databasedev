USE [Moda]
GO
/****** Object:  StoredProcedure [dbo].[mgn_agent_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_agent_get_list
 
	*/

CREATE         PROCEDURE [dbo].[mgn_agent_get_list] 
    @site_key uniqueidentifier = null, 
	@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();

	SELECT '00000000-0000-0000-0000-000000000000' as [key], ' Select Agent' as [name]
	UNION
    SELECT _key as [key],
			name as [name]
    FROM entities
	WHERE site_key = @site_key AND type = 2

	ORDER BY name
    
END
GO
