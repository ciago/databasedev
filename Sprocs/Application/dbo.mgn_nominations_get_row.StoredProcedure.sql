--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_nominations_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Obtains a single row of volume accounts data by searching for a specified guid.


-- =============================================
CREATE  OR ALTER   PROCEDURE [dbo].[mgn_nominations_get_row]
	@row uniqueidentifier = null

	/*EXEC [mgn_nominations_get_row]
	@row  = '32f4bc65-5dc2-4f23-9170-59bd7bdb9f86'
	*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from 
--WHere _key = @row
   
SELECT n._key,
		n.nomination_number,
		n.[start_date],
		n.end_date,
		n.[order_type],
		n.[product_key],
		isnull(n.[entity1_key],'00000000-0000-0000-0000-000000000000') as [entity1_key],
		isnull(n.[entity2_key],'00000000-0000-0000-0000-000000000000') as [entity2_key],
		isnull(n.[entity3_key],'00000000-0000-0000-0000-000000000000') as [entity3_key],
		isnull(n.[entity4_key],'00000000-0000-0000-0000-000000000000') as [entity4_key],
		isnull(n.[entity5_key],'00000000-0000-0000-0000-000000000000') as [entity5_key],
		isnull(n.[entity6_key],'00000000-0000-0000-0000-000000000000') as [entity6_key],
		isnull(n.[entity7_key],'00000000-0000-0000-0000-000000000000') as [entity7_key],
		isnull(n.[nom_volume],0) as [nom_volume],
		isnull(dbo.[get_gen_nom_total] (nomination_number),0) as [used_volume],
		isnull(n.nom_volume - dbo.[get_gen_nom_total](nomination_number),n.[nom_volume]) as [remaining_volume],
		n.[site]

from nominations_general as n left join
	 entities as e on e._key = n.entity1_key left join
	 products as p on p._key = n.product_key 

WHERE @row = n._key and n.deleted = 0
	  
END
GO
