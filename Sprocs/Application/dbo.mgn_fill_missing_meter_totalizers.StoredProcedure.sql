--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_fill_missing_meter_totalizers]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE OR ALTER  PROCEDURE [dbo].[mgn_fill_missing_meter_totalizers] 
AS 
BEGIN

DECLARE @update bit = 1
DECLARE @start_datetime datetime = dateadd(day, -1, getdate())--CONVERT(DATETIME, '2020-11-03 22:27', 102)

Declare @meter_key uniqueidentifier, 
		@totalizer_before_null float, 
		--@timestamp_before_null datetime,
		@totalizer_after_null float, 
		--@timestamp_after_null datetime, 
		@diff float,
		@update_to_lastest bit = 1

DECLARE meter_cursor CURSOR 
FOR SELECT DISTINCT meter_key 
	FROM meter_readings 
	WHERE meter_timestamp > @start_datetime AND meter_totalizer IS NULL;

OPEN meter_cursor;

FETCH NEXT FROM meter_cursor INTO @meter_key;

WHILE @@FETCH_STATUS = 0
BEGIN

	

		SELECT @totalizer_before_null = t1.lastTotalizer, @totalizer_after_null = t2.nextTotalizer, @diff =  t2.nextTotalizer - t1.lastTotalizer 
		FROM 
		(
		-- last totalizer before null
		Select meter_totalizer as lastTotalizer, mr.meter_key
		from meter_readings mr INNER JOIN 
		(Select MIN(meter_timestamp) as firstNull , meter_key
		from meter_readings 
		where meter_totalizer is null AND (meter_timestamp > @start_datetime) AND meter_key = @meter_key
		GROUP BY meter_key) minNULL ON mr.meter_key = minNULL.meter_key
		where mr.meter_timestamp = DATEADD(MINUTE, -1, minNULL.firstNull )  AND mr.meter_key = @meter_key
		) t1 INNER JOIN 
		--totalizer after null 
		(
		Select meter_totalizer as nextTotalizer, mr.meter_key
		from meter_readings mr INNER JOIN 
		(Select MAX(meter_timestamp) as lastNull , meter_key
		from meter_readings 
		where meter_totalizer is null AND (meter_timestamp > @start_datetime)  AND meter_key = @meter_key 
		GROUP BY meter_key) maxNULL ON mr.meter_key = maxNULL.meter_key
		where mr.meter_timestamp = DATEADD(MINUTE, 1, maxNULL.lastNull)  AND mr.meter_key = @meter_key
		) t2 ON t1.meter_key = t2.meter_key

--SELECT @totalizer_before_null ,  @totalizer_after_null


		

			IF(@totalizer_after_null <> NULL) --if there is a newer totalizer value at this point, 
			BEGIN
				IF (@update_to_lastest = 0) -- update the readinggs to be the totalizer value before we lost it 
				BEGIN
					PRINT 'Updating to totalizer before null'
						Select @totalizer_before_null
					If (@update= 1)
					BEGIN
						UPDATE meter_readings
						SET meter_totalizer = @totalizer_before_null,
								meter_auto_updated = 1 
						--FROM            meter_readings
						WHERE        (meter_key = @meter_key) AND (meter_timestamp > @start_datetime) AND meter_totalizer IS NULL
					END
				END 
				ELSE
				BEGIN-- update the readinggs to be the totalizer value AFTER we got it back
					PRINT 'Updating to totalizer after null'
						Select @totalizer_after_null
					If (@update= 1)
					BEGIN
						UPDATE meter_readings
						SET meter_totalizer = @totalizer_after_null,
								meter_auto_updated = 1 
						--FROM            meter_readings
						WHERE        (meter_key = @meter_key) AND (meter_timestamp > @start_datetime) AND meter_totalizer IS NULL
					END
				END 

			END
			ELSE -- There is no new value yet 
			BEGIN
				--DECLARE 
				declare @lastNullTimestamp datetime
				declare @lastTotalizer float

				Select @lastNullTimestamp = MAX(meter_timestamp) 
				from meter_readings 
				where meter_totalizer is null AND (meter_timestamp > @start_datetime)  AND meter_key = @meter_key 
				GROUP BY meter_key

				--Select MAX(meter_timestamp) as lastNull , meter_key
				--from meter_readings 
				--where meter_totalizer is null AND (meter_timestamp > @start_datetime)  AND meter_key = @meter_key 
				--GROUP BY meter_key

				IF (@lastNullTimestamp = (SELECT MAX(meter_timestamp) FROM meter_readings WHERE meter_key = @meter_key))
				BEGIN
					  Select @lastTotalizer = meter_totalizer 
						from meter_readings mr INNER JOIN 
						(Select MIN(meter_timestamp) as firstNull , meter_key
						from meter_readings 
						where meter_totalizer is null AND (meter_timestamp > @start_datetime) AND meter_key = @meter_key
						GROUP BY meter_key) minNULL ON mr.meter_key = minNULL.meter_key
						where mr.meter_timestamp = DATEADD(MINUTE, -1, minNULL.firstNull )  AND mr.meter_key = @meter_key

						PRINT 'update totalizers to last good one ' 
						Select @lastTotalizer, @meter_key

						
						If (@update= 1)
						BEGIN
							UPDATE meter_readings
							SET meter_totalizer = @lastTotalizer,
								meter_auto_updated = 1 
							--FROM            meter_readings
							WHERE        (meter_key = @meter_key) AND (meter_timestamp > @start_datetime) AND meter_totalizer IS NULL
						END

				END



			END

		

	

		-- reset the diff 
		SET @diff = null;



        FETCH NEXT FROM meter_cursor INTO 
             @meter_key ;
    END;
 
CLOSE meter_cursor;
 
DEALLOCATE meter_cursor;



END
GO
