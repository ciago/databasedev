--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_entity_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  OR ALTER  PROCEDURE [dbo].[mgn_entity_get_list] 
	@type int = null
	--@terminal uniqueidentifier = null

/*
execute [mgn_entity_get_list] @type = 1
*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @default_option varchar(50)

	if(@type is null)
	Begin
		Set @default_option = ' Select entity'
	end
	else
	begin
		Set @default_option = ' Select ' + (select type_description from entity_types where type_code = @type)
	end

	SELECT '00000000-0000-0000-0000-000000000000' as _key, @default_option as name
	UNION
    SELECT _key, name
	FROM entities
	WHERE (type = @type or @type is null)
	AND active = 1
	ORDER BY name
	
END

GO
