--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_marine_agent_invoices_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Obtains a single row of marine agent invoices data from a specified guid.

/*EXEC mgn_marine_agent_invoices_get_row
	@row  = '1B584A30-50F3-4B5F-94A6-7351D0CBB68A'
	*/
-- =============================================
CREATE	 OR ALTER   PROCEDURE [dbo].[mgn_marine_agent_invoices_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from 
--WHere _key = @row   

   -- ETA Date (descending order), Berth, Vessel Name, Agent, Arrived Date,
   -- Status, Volume, Type

SELECT        vi._key,--st._key,
			  st.syn_vessel_eta as [eta_date],
			  ISNULL(st.syn_dock_name, '') as [berth_name],
			  isnull(st.syn_vessel_name,'') as [vessel_name],
			  isnull(vi.vi_marine_agent_key,'00000000-0000-0000-0000-000000000000') as [marine_agent_key],
			  --st.syn_marine_agent as [marine_agent],
			  ISNULL(CONVERT(varchar, st.syn_ticket_start_datetime, 22), '') as [arrived_date],
			  ISNULL(CONVERT(varchar, st.syn_ticket_end_datetime, 22), '') as [departed_date],
			  isnull(vi.vi_vessel_invoice_status_key,'00000000-0000-0000-0000-000000000000') as [vessel_invoice_status_key],
			  isnull(vis.stat_description,'') as [vessel_status],
			  ISNULL(CONVERT(float, st.syn_total_synthesis_net_volume), 0) as [vessel_volume],
			  ISNULL(st.syn_vessel_description, '') as [vessel_type]
			  
FROM		  vessel_invoice vi left outer JOIN 
			  synthesis_tickets AS st ON vi.vi_synthesis_ticket_key = st._key left outer JOIN
			  vessel_invoice_status AS vis ON vi.vi_vessel_invoice_status_key = vis._key 

WHERE @row = vi._key
	  
END
GO
