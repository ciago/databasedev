USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_contracts_update]    Script Date: 5/13/2021 11:18:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create or ALTER         PROCEDURE [dbo].[mgn_charges_general_update]

	@row uniqueidentifier,
	@charge_description nvarchar(50) = null,
	@charge_amount decimal(18,5) = null,
	@unit_type uniqueidentifier = null,
	@charge_category uniqueidentifier = null,
	@start_date datetime = null,
	@end_date datetime = null,
	@charge_type int = null,
	@charge_site uniqueidentifier = null,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from contacts
--Where _key = @row

IF(@row IN (SELECT _key from contracts))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json_upd varchar(max)
	SET @json_upd = (SELECT record_info FROM contacts WHERE _key = @row)
	SET @json_upd = dbo.fn_json_record_info(@json_upd, 'updated', @user, HOST_NAME(), GETDATE())

	DECLARE @json_del varchar(max)
	SET @json_del = (SELECT record_info FROM contacts WHERE _key = @row)
	SET @json_del = dbo.fn_json_record_info(@json_del, 'deleted', @user, HOST_NAME(), GETDATE())

	DECLARE @json_arc varchar(max)
	SET @json_arc = (SELECT record_info FROM contacts WHERE _key = @row)
	SET @json_arc = dbo.fn_json_record_info(@json_arc, 'archived', @user, HOST_NAME(), GETDATE())

	DECLARE @json_res varchar(max)
	SET @json_res = (SELECT record_info FROM contacts WHERE _key = @row)
	SET @json_res = dbo.fn_json_record_info(@json_res, 'restored', @user, HOST_NAME(), GETDATE())
	
	
	UPDATE charges_general

	SET		[cg_charge_description] = @charge_description,
			[cg_charge_amount] = @charge_amount,
			[cg_charge_unit_key] = @unit_type,
			[cgc_category] = @charge_category,
			[cg_start_date] = @start_date,
			[cg_end_date] = @end_date,
			[cg_charge_type] = @charge_type,
			[cg_record_info] = @json_upd,
			[cg_site] = @charge_site

	WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[charges_general]
           ([_key]
		   ,[cg_charge_description]
		   ,[cg_charge_amount]
		   ,[cg_charge_unit_key]
		   ,[cgc_category]
		   ,[cg_start_date]
		   ,[cg_end_date]
		   ,[cg_charge_type]
		   ,[cg_record_info]
		   ,[cg_site])
     VALUES
           (@row
		   ,@charge_description
		   ,@charge_amount
		   ,@unit_type
		   ,@charge_category
		   ,@start_date
		   ,@end_date
		   ,@charge_type
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   ,@charge_site)

END

END
