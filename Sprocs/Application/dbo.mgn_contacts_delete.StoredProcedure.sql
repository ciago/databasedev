--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_contacts_delete]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/24/2021
-- Description:		Deletes a single row of contacts data based on the selected row/record.

/*EXEC mgn_contacts_delete
	@row  = '8650AB82-B903-4C55-B507-CD94B1677987'

	*/
-- =============================================
CREATE    OR ALTER         PROCEDURE [dbo].[mgn_contacts_delete]
	@row uniqueidentifier = null,
	@user varchar(50) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from contacts
--WHere _key = @row
Delete From contacts

Where _key = @row

END
GO
