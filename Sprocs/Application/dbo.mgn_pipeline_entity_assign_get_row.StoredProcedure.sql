--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_entity_assign_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Obtains a single row of pipeline entity assignment data from a specified guid.

/*EXEC mgn_pipeline_entity_assign_get_row
	@row  = '0f4489ba-192d-4636-bba6-893a44058fed'
	*/
-- =============================================
CREATE     OR ALTER    PROCEDURE [dbo].[mgn_pipeline_entity_assign_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from 
--WHere _key = @row
   
SELECT        pea._key,
			  pea.pipeline_key as [pipeline_key],
			  pea.entity_key as [entity_key],
			  pea.start_datetime as [start_datetime],
			  pea.end_datetime as [end_datetime]
			  
FROM pipeline_entity_assignment AS pea

WHERE @row = pea._key
	  
END
GO
