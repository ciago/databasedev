--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_owner_report]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   OR ALTER PROCEDURE [dbo].[mgn_tank_owner_report]
 @effective_date DATETIME
, @shipper uniqueidentifier = null
	
AS
/* 
EXEC mgn_tank_owner_report @effective_date = '1/22/2021', @shipper= 'd25051f3-4888-4bfb-89fd-4e1449ac0fba'
	
*/
BEGIN


IF @shipper = '00000000-0000-0000-0000-000000000000'
BEGIN
SET @shipper = null
END

--DECLARE @effective_date DATETIME = '10/30/2020'
--	  , @shipper uniqueidentifier = null


SELECT e.name, 
	   mt.tank_name, 
	   tow.capacity_for_billing
FROM tank_owner tow  INNER JOIN 
     tanks mt ON tow.tank_key = mt._key LEFT OUTER JOIN  
	 entities e ON tow.tank_owner_key =  e._key
WHERE tow.tk_start_datetime <= @effective_date AND (tow.tk_end_datetime IS NULL OR tow.tk_end_datetime >= @effective_date)
	  AND (tow.tank_owner_key IS NOT NULL AND tow.tank_owner_key = @shipper OR @shipper IS NULL)
	  AND mt.tank_name NOT LIKE '%virtual%'
	  ORDER BY e.name, 
	   mt.tank_name


	   
END
GO
