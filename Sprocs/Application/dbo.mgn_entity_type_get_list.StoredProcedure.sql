--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_entity_type_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/18/2021
-- Description:		Select list of entity types for dropdown usage.

/*EXEC mgn_entity_type_get_list

	*/
-- =============================================

CREATE  OR ALTER       PROCEDURE [dbo].[mgn_entity_type_get_list] 
		@site_key uniqueidentifier = null,
		@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();   -- Needs to change to Hartree site GUID

SELECT '0' as [type_code], ' Select Entity Type' as [name]
UNION

SELECT			et.type_code as [type_code],
				et.type_description
			  
FROM			entity_types et

WHERE			et.site_key = @site_key
				AND active = 1

ORDER BY name
    
END
	
GO
