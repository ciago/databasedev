USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_rpt_nominations_get]    Script Date: 5/24/21 12:19:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================



ALTER    PROCEDURE [dbo].[mgn_rpt_nominations_get] 
   -- @site uniqueidentifier = null, 
	@start_date datetime = null,
	@product uniqueidentifier = null,
	@entity1 uniqueidentifier = null,
	@order_type int = null

	/*EXEC mgn_nominations_get
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@start_date = '2021-03-03'
	@product = '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0',
	@customer = '86c0cacb-b542-49c8-b352-8be35f276541',

	@order_type = 1
	
	*/
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
DECLARE @site uniqueidentifier 
IF @site IS NULL 
	Begin
		SET @site = dbo.get_default_site();
	End





Select  n._key,
		n.nomination_number,
		n.[start_date],
		n.end_date,
		n.[order_type],
		tt.type_description,
		n.[product_key],
		p.product_name,
		isnull(n.[entity1_key],'00000000-0000-0000-0000-000000000000') as [entity1_key],
		isnull(e1.short_name,'') as [entity1_name],
		n.[nom_volume],
		isnull(dbo.[get_gen_nom_total] (nomination_number),0) as [used_volume],
		nom_volume - isnull(dbo.[get_gen_nom_total](nomination_number),0) as [remaining_volume],
		n.[site],
		e2.name as entity2_name

from nominations_general as n left join
	 entities as e1 on e1._key = n.entity1_key left join
	 products as p on p._key = n.product_key left join
	 ticket_type as tt on n.order_type = tt.typ_type LEFT OUTER JOIN 
	 entities e2 ON n.entity2_key = e2._key

where n.[site] = @site
	  and (e1._key = @entity1 or @entity1 = '00000000-0000-0000-0000-000000000000')
	  and (p._key = @product or @product = '00000000-0000-0000-0000-000000000000')
	  and (n.[start_date] <= @start_date )
	  and (n.[end_date] >= @start_date or n.[end_date] IS NULL)
	  and (n.order_type = @order_type or @order_type = 0)
	  and (n.deleted = 0)

order by [nomination_number]

END
