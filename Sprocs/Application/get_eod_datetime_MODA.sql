--USE [Moda]
--GO
/****** Object:  UserDefinedFunction [dbo].[get_eod_datetime]    Script Date: 5/11/2021 12:08:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE OR ALTER   FUNCTION [dbo].[get_eod_datetime]
( @date datetime
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @eod datetime

	-- Add the T-SQL statements to compute the return value here

	SET @eod = CASE WHEN cast( @date as time) >= cast('07:00 AM' as TIME) 
	        THEN DATEADD(day, 1, SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date), 07, 00)) 
			ELSE SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date), 07, 00) END


	-- Return the result of the function
	RETURN @eod

END
