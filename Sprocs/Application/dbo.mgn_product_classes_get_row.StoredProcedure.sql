--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_product_classes_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/1/2021
-- Description:		Obtains a single row of product classes data from a specified guid.

/*EXEC mgn_product_classes_get_row
	@row  = '3bdcb8c4-b681-4f6f-b2f2-1bc2e6e9cbdc'

	*/
-- =============================================
CREATE  OR ALTER     PROCEDURE [dbo].[mgn_product_classes_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from product_classes
--WHere _key = @row
   
SELECT			pc._key,
				ISNULL(pc.product_class, '') as [product_class],
				ISNULL(pc.class_description, '') as [class_description],
				pc.active as [active]

FROM			product_classes AS pc

WHERE			@row = pc._key
	  
END
GO
