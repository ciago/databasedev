USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_contracts_get_list]    Script Date: 5/12/2021 5:00:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER          PROCEDURE [dbo].[mgn_contracts_get_list]

	@contract_status int = null,
	@entity_key uniqueidentifier = null,
	@effective_date datetime = null,
	@site uniqueidentifier = null
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from contacts

/*
Exec [mgn_contracts_get_list]
@effective_date = '2021-05-12 08:00:00.000'
*/

SELECT			c._key as [_key],
				c.ctr_contract_number as [contract_number],
				e.short_name as [entity],
				isnull(c.ctr_description,'') as [description],
				cs.cs_description as [status],
				ct.ct_description as [type],
				isnull(convert(nvarchar(50),c.ctr_start_date,101),'') as [start_date],
				isnull(convert(nvarchar(50),c.ctr_end_date,101),'') as [end_date],
				isnull(c.ctr_comments,'') as [comments],
				s.short_name as [site]

FROM			contracts AS c INNER JOIN
				entities AS e ON e._key = c.ctr_entity LEFT OUTER JOIN
				contract_types AS ct ON c.ctr_contract_type = ct._key LEFT OUTER JOIN
				contract_status AS cs ON c.ctr_contract_status = cs.cs_status left outer join
				sites as s on c.ctr_site = s._key

WHERE			(c.ctr_contract_status = @contract_status or @contract_status is null)
				AND (c.ctr_entity = @entity_key OR @entity_key IS NULL)
				AND	(c.ctr_start_date <= @effective_date)
				AND	(c.ctr_end_date >= @effective_date or c.ctr_end_date is null)
				AND (c.ctr_site = @site or @site is null)

ORDER BY		[start_date]
	  
END
