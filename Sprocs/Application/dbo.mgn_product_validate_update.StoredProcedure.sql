
/****** Object:  StoredProcedure [dbo].[mgn_product_validate_update]    Script Date: 5/11/2021 12:55:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create or ALTER                   PROCEDURE [dbo].[mgn_product_validate_update]

	@row uniqueidentifier,
	@product_name nvarchar(100),
	@product_class_key uniqueidentifier = null,
	@product_api float,
	@safe_operating_temperature float,
	@product_external_id nvarchar(50) = null,
	@product_code nvarchar(8),
	@product_irs_code nvarchar(3) = 'XXX',
	@site_key uniqueidentifier,
	@active bit,
	@user varchar(50)
	
	
	/*EXEC [mgn_product_class_validate_update] 
	@row ='dd85132f-4039-4fda-8fcc-bbd2dd9b7656' ,
	@product_name ='test' ,
	@product_class_key = 'dd85132f-4039-4fda-8fcc-bbd2dd9b7656',
	@product_api = 0,
	@safe_operating_temperature =0,
	@product_external_id  = null,
	@product_code ='test',
	@product_irs_code  = 'XXX',
	@site_key = 'dd85132f-4039-4fda-8fcc-bbd2dd9b7656',
	@active =1,
	@user ='test'
	
	// 
	*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row


--declare @product_class_key uniqueidentifier= null --'dd85132f-4039-4fda-8fcc-bbd2dd9b7656'
declare @message nvarchar(400)
declare @product_class_message nvarchar(50) = ''
-- Check for stuff

IF(@product_class_key is null)
Begin
	Set @product_class_message = 'Product Class must be selected. ' 
End

--print @product_class_message

set @message = @product_class_message

--print @message

SELECT @message
END
