--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_rpt_ticket_listing]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 6/26/2020
-- Description:	gets a list of vessel activity records for the selected date

/*EXEC mgn_rpt_ticket_listing
	@start_date  = '5/4/2020', @shipper = 'dd85132f-4039-4fda-8fcc-bbd2dd9b7656'
	*/
-- =============================================
CREATE  OR ALTER     PROCEDURE [dbo].mgn_rpt_ticket_listing
	@start_date datetime = null,
	@end_date datetime = null,
	@customer uniqueidentifier = null
	--, @type (pipeline, vessel, tank to tank transfer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row

--DECLARE @eod datetime = DATEADD(DAY, 1, SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), (DAY(@date)), 07, 00))
declare @site uniqueidentifier

-- set site if null
IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END

IF(@customer = '00000000-0000-0000-0000-000000000000')
BEGIN
	SET @customer = NULL;
END

SELECT --t._key,
	  tt.type_description as type_description, -- type 
	  ts.stat_description, -- status
	  e.short_name  as customer_name, -- shipper
	  p.product_code, -- product
	  t.tk_batch_id, -- batch id
	  ISNULL(t.tk_start_datetime, DATEADD(DAY, -1, t.tk_eod_datetime)) as tk_start_datetime,  -- start
	   ISNULL(t.tk_end_datetime, t.tk_eod_datetime) as tk_end_datetime,-- end
	 COALESCE(from_t.tank_pi_name, from_p.short_code , from_b.berth_short_name, from_l.[ln_location_name]) as [from_location],-- from, pipeine receipt - from a pipeline, going to a tank   vessel delivery from  tank to berth 	  
	 COALESCE( to_t.tank_pi_name,  to_b.berth_short_name, to_l.[ln_location_name]) as to_location, -- to
	 ROUND( ISNULL(t.tk_final_gsv,0), 2) as tk_final_gsv, -- volume 
	 ROUND( ISNULL(t.tk_final_nsv,0), 2) as tk_final_nsv, -- volume 
	 t.tk_eod_datetime, 
	 t.tk_scheduled_volume as tk_scheduled_volume,
	 e2.name as Carrier_Name,
	 t.tk_nomination_number
FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN 
	 ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN 
	 entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN 
	 products p On t.tk_product = p._key LEFT OUTER JOIN 
	 tanks from_t ON from_t._key = tk_source_tank_key LEFT OUTER JOIN 
	 pipeline from_p ON from_p._key = t.tk_pipeline_key LEFT OUTER JOIN 
	 berths from_b ON from_b._key = t.tk_berth_key LEFT OUTER JOIN 
	 tanks to_t ON to_t._key = tk_destination_tank_key LEFT OUTER JOIN 
	 berths to_b ON to_b._key = t.tk_berth_key LEFT OUTER JOIN 
	 locations from_l ON t.tk_truck_lane_key = from_l._key LEFT OUTER JOIN 
	 locations to_l ON t.tk_truck_lane_key = to_l._key LEFT OUTER JOIN 
	 entities e2 ON e2._key = t.tk_entity2_key
WHERE --tk_eod_datetime = @eod AND t.site_key = @site
	  tk_end_datetime >= @start_date AND (tk_start_datetime <= @end_date OR @end_date IS NULL)
	  AND (@customer = t.tk_entity1_key OR @customer IS NULL)
ORDER BY typ_type, [from_location]
	
END
GO
