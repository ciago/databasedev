--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_contacts_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   OR ALTER      PROCEDURE [dbo].[mgn_contacts_update]

	@row uniqueidentifier,
	@entity_key uniqueidentifier,
	@contact_type_key uniqueidentifier,
	@first_name nvarchar(50) = '',
	@last_name nvarchar(50) = '',
	@company_name nvarchar(100) = '',
	@address_1 nvarchar(100) = '',
	@address_2 nvarchar(100) = '',
	@city nvarchar(50) ='',
	@state_key uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@country_key uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@zip_code nvarchar(10) = '',
	@phone_1 nvarchar(20) = '',
	@phone_2 nvarchar(20) = '',
	@email_address nvarchar(100) = '',
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from contacts
--Where _key = @row

IF @row = '00000000-0000-0000-0000-000000000000'
BEGIN
	SET @row = newid()
END



IF(@row IN (SELECT _key from contacts))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM contacts WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE contacts

SET		entity_key = @entity_key,
		contact_type_key = @contact_type_key,
		first_name = @first_name,
		last_name = @last_name,
		company_name = @company_name,
		address_1 = @address_1,
		address_2 = @address_2,
		city = @city,
		state_key = @state_key,
		country_key = @country_key,
		zip_code = @zip_code,
		phone_1 = @phone_1,
		phone_2 = @phone_2,
		email_address = @email_address,
		record_info = @json

WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[contacts]
           ([_key]
		   ,[entity_key]
		   ,[contact_type_key]
		   ,[first_name]
		   ,[last_name]
		   ,[company_name]
		   ,[address_1]
		   ,[address_2]
		   ,[city]
		   ,[state_key]
		   ,[country_key]
		   ,[zip_code]
		   ,[phone_1]
		   ,[phone_2]
		   ,[email_address]
           ,[record_info])
     VALUES
           (@row
		   ,@entity_key
		   ,@contact_type_key
		   ,@first_name
		   ,@last_name
		   ,@company_name
		   ,@address_1
		   ,@address_2
		   ,@city
		   ,@state_key
		   ,@country_key
		   ,@zip_code
		   ,@phone_1
		   ,@phone_2
		   ,@email_address
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
GO
