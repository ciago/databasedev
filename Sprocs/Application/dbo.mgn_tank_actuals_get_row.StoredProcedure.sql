--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_actuals_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Obtains a single row of tank actuals data from a specified guid.

/*EXEC mgn_tank_actuals_get_row
	@row  = 'F6DA37F0-689D-47A9-84AD-B4EAC7CC7889'
	*/
-- =============================================
CREATE     OR ALTER   PROCEDURE [dbo].[mgn_tank_actuals_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from tank_actuals
--WHere _key = @row

declare @eod datetime = (SELECT eod_datetime FROM tank_actuals WHERE _key = @row)

SELECT        ta._key,
			  ta.tank_key as [tank_key],
			  t.tank_name as [tank_name],
              DATEADD(DAY, -1, ta.eod_datetime) as [date_datetime],
              ROUND(ISNULL(ta.tank_gsv_change,0), 2) as [tank_gsv_change],
              ROUND(ISNULL(ta.tank_tov_change,0), 2) as [tank_tov_change],
			  ROUND(ISNULL(ta.open_gsv, 0), 2) as [open_gsv],
			  ROUND(ISNULL(ta.close_gsv,0), 2) as [close_gsv],
			  ta.site_key as [site_key],
              s.short_name as [site_name],
			   ISNULL(p.product_name, 'No Product Assigned') as [tank_product],
			  ISNULL(ta.tank_api, 0) as [tank_api],
			  ISNULL(ta.tank_temp, 0) as [tank_temp]

FROM          tank_actuals AS ta INNER JOIN
              sites AS s ON s._key = ta.site_key INNER JOIN
              tanks AS t ON ta.tank_key = t._key LEFT OUTER JOIN
			  (SELECT * FROM tank_products WHERE (tk_start_datetime <= @eod)
			 AND (tk_end_datetime >= @eod or tk_end_datetime IS null)) AS tp ON ta.tank_key = tp.tank_key LEFT OUTER JOIN
			  products AS p ON tp.product_key = p._key

WHERE  ta._key = @row			
	
END
GO
