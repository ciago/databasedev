--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_berth_fees_delete]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Deletes a single row of berth fees data based on the selected row/record.

/*EXEC mgn_berth_fees_delete
	@row  = '5dbdc516-110e-4f20-ab7a-b768078125dd'
	*/
-- =============================================
CREATE  OR ALTER         PROCEDURE [dbo].[mgn_berth_fees_delete]
	@row uniqueidentifier = null,
	@user varchar(50) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from schedule
--WHere _key = @row
Delete From berth_fees

Where _key = @row

END
GO
