--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_inbound_dashboard_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


   

 --delete from pipeline_allocations
--USE [Moda]
--GO
--/****** Object:  StoredProcedure [dbo].[mgn_tank_overview_get]    Script Date: 5/1/2020 7:35:46 AM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		Candice Iago	
---- Create date: 6/26/20
---- Description:	Gets all of the tank overview records for the month

/*EXEC mgn_inbound_dashboard_get_list
	@date  = '3/29/2021', @pipeline = '263486ba-8550-49e2-81d2-055bbb61b0ae'

	'8/29/2020'
	*/
---- =============================================

CREATE   OR ALTER     PROCEDURE [dbo].[mgn_inbound_dashboard_get_list]
-- 	@pipeline_key uniqueidentifier , 
--DECLARE 
	@date datetime = null,
	@site uniqueidentifier = null, 
	@user nvarchar(50) = 'default', 
	@machine nvarchar(50) = 'default', 
	@shipper uniqueidentifier = null,
	@pipeline uniqueidentifier = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row

DECLARE @eod_int int = 1
DECLARE @eod datetime = DATEADD(DAY, @eod_int, SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), (DAY(@date)), 07, 00))

-- set site if null
IF(@site IS NULL) 
	BEGIN 
		SET @site = dbo.get_default_site();
	END

declare @completed_key uniqueidentifier = (SELECT top(1) _key FROM ticket_status WHERE stat_is_complete = 1)

declare @results table (_key uniqueidentifier,
						type_description nvarchar(100),
						stat_desc_key uniqueidentifier,
						stat_description nvarchar(100),
						shipper_name nvarchar(20),
						product_code nvarchar(8),
						tk_batch_id nvarchar(200),
						tk_start_datetime datetime,
						tk_end_datetime datetime,
						from_location nvarchar(16),
						to_location nvarchar(16),
						tk_final_gsv float,
						tk_eod_datetime datetime,
						tk_scheduled_volume float,
						sequence_number int,
						shutdown_status bit)

-- fills the @results table with data from the day the user selected
insert into @results  (_key,
					   type_description,
					   stat_desc_key,
					   stat_description,
					   shipper_name,
					   product_code,
					   tk_batch_id,
					   tk_start_datetime,
					   tk_end_datetime,
					   from_location,
					   to_location,
					   tk_final_gsv,
					   tk_eod_datetime,
					   tk_scheduled_volume,
					   sequence_number,
					   shutdown_status)


SELECT	t._key,
		CASE WHEN tt.typ_type = 1 THEN 'pipeline' WHEN tt.typ_type = 2 THEN 'vessel' WHEN tt.typ_type = 3 THEN 'transfer' END as type_description, -- type 
		ts._key as stat_desc_key,
		ts.stat_description, -- status
		e.short_name  as shipper_name, -- shipper
		p.product_code, -- product
		t.tk_batch_id, -- batch id
		ISNULL(t.tk_start_datetime, DATEADD(DAY, -1, t.tk_eod_datetime)) as tk_start_datetime,  -- start
		ISNULL(t.tk_end_datetime, t.tk_eod_datetime) as tk_end_datetime,-- end
		ISNULL( ISNULL(from_t.tank_pi_name, from_p.short_code) , from_b.berth_short_name) as [from_location],-- from, pipeine receipt - from a pipeline, going to a tank   vessel delivery from  tank to berth
		ISNULL( ISNULL(to_t.tank_pi_name, to_p.short_code) , to_b.berth_short_name) as to_location, -- to
		ROUND( ISNULL(t.tk_final_gsv,0), 2) as tk_final_gsv, -- volume 
		t.tk_eod_datetime, 
		t.tk_scheduled_volume as tk_scheduled_volume,
		ISNULL(t.tk_day_sequence_number, 0) as sequence_number,
		ISNULL(from_p.[shutdown], 0) as [shutdown]

FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN
	 ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN
	 entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN
	 products p On t.tk_product = p._key LEFT OUTER JOIN
	 tanks from_t ON from_t._key = tk_source_tank_key LEFT OUTER JOIN
	 pipeline from_p ON from_p._key = t.tk_pipeline_key LEFT OUTER JOIN
	 berths from_b ON from_b._key = t.tk_berth_key LEFT OUTER JOIN
	 tanks to_t ON to_t._key = tk_destination_tank_key LEFT OUTER JOIN
	 pipeline to_p ON to_p._key = t.tk_pipeline_key LEFT OUTER JOIN
	 berths to_b ON to_b._key = t.tk_berth_key

WHERE (t.tk_eod_datetime = @eod) -- fills the @results table with data from the day the user selected
	   AND (t.tk_site_key = @site)
	   AND (@shipper = t.tk_entity1_key OR @shipper IS NULL)
	   AND (@pipeline = t.tk_pipeline_key OR @pipeline IS NULL)

ORDER BY tk_eod_datetime, sequence_number

/* If the stat_desc_key column of the @results table already contains the GUID associated with "completed" then we don't need to go back in time looking for it */
if Not Exists (Select stat_desc_key from @results where stat_desc_key = @completed_key)
	begin
		-- adds to the results table, the first row of data from beore the day the user selected that contains anything other than "Scheduled" or "Void", but only if the results table doesn't already contain a row with a status other than "Scheduled" or "Void"
		insert into @results  (_key,
							   type_description,
							   stat_desc_key,
							   stat_description,
							   shipper_name,
							   product_code,
							   tk_batch_id,
							   tk_start_datetime,
							   tk_end_datetime,
							   from_location,
							   to_location,
							   tk_final_gsv,
							   tk_eod_datetime,
							   tk_scheduled_volume,
							   sequence_number,
							   shutdown_status)

		SELECT top(1)	t._key,
						CASE WHEN tt.typ_type = 1 THEN 'pipeline' WHEN tt.typ_type = 2 THEN 'vessel' WHEN tt.typ_type = 3 THEN 'transfer' END as type_description, -- type 
						ts._key as stat_desc_key,
						ts.stat_description, -- status
						e.short_name  as shipper_name, -- shipper
						p.product_code, -- product
						t.tk_batch_id, -- batch id
						ISNULL(t.tk_start_datetime, DATEADD(DAY, -1, t.tk_eod_datetime)) as tk_start_datetime,  -- start
						ISNULL(t.tk_end_datetime, t.tk_eod_datetime) as tk_end_datetime,-- end
						ISNULL( ISNULL(from_t.tank_pi_name, from_p.short_code) , from_b.berth_short_name) as [from_location],-- from, pipeine receipt - from a pipeline, going to a tank   vessel delivery from  tank to berth
						ISNULL( ISNULL(to_t.tank_pi_name, to_p.short_code) , to_b.berth_short_name) as to_location, -- to
						ROUND( ISNULL(t.tk_final_gsv,0), 2) as tk_final_gsv, -- volume 
						t.tk_eod_datetime, 
						t.tk_scheduled_volume as tk_scheduled_volume,
						ISNULL(t.tk_day_sequence_number, 0) as sequence_number,
						ISNULL(from_p.[shutdown], 0) as [shutdown]

		FROM tickets t INNER JOIN 
			 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN
			 ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN
			 entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN
			 products p On t.tk_product = p._key LEFT OUTER JOIN
			 tanks from_t ON from_t._key = tk_source_tank_key LEFT OUTER JOIN
			 pipeline from_p ON from_p._key = t.tk_pipeline_key LEFT OUTER JOIN
			 berths from_b ON from_b._key = t.tk_berth_key LEFT OUTER JOIN
			 tanks to_t ON to_t._key = tk_destination_tank_key LEFT OUTER JOIN
			 pipeline to_p ON to_p._key = t.tk_pipeline_key LEFT OUTER JOIN
			 berths to_b ON to_b._key = t.tk_berth_key

		WHERE	(t.tk_eod_datetime < @eod) -- adds to the results table, the first row of data from beore the day the user selected that contains anything other than "Scheduled" or "Void", but only if the results table doesn't already contain a row with a status other than "Scheduled" or "Void"
				AND (t.tk_site_key = @site)
				AND (@shipper = t.tk_entity1_key OR @shipper IS NULL)
				AND (@pipeline = t.tk_pipeline_key OR @pipeline IS NULL)
				AND t.tk_status <> 1
				AND t.tk_status <> 7
	end /* if Not Exists... END */

-- adds to the results table, any rows of data from beore the day the user selected that contains the status type "Scheduled"
insert into @results  (_key,
					   type_description,
					   stat_desc_key,
					   stat_description,
					   shipper_name,
					   product_code,
					   tk_batch_id,
					   tk_start_datetime,
					   tk_end_datetime,
					   from_location,
					   to_location,
					   tk_final_gsv,
					   tk_eod_datetime,
					   tk_scheduled_volume,
					   sequence_number,
					   shutdown_status)

SELECT	t._key,
		CASE WHEN tt.typ_type = 1 THEN 'pipeline' WHEN tt.typ_type = 2 THEN 'vessel' WHEN tt.typ_type = 3 THEN 'transfer' END as type_description, -- type 
		ts._key as stat_desc_key,
		ts.stat_description, -- status
		e.short_name  as shipper_name, -- shipper
		p.product_code, -- product
		t.tk_batch_id, -- batch id
		ISNULL(t.tk_start_datetime, DATEADD(DAY, -1, t.tk_eod_datetime)) as tk_start_datetime,  -- start
		ISNULL(t.tk_end_datetime, t.tk_eod_datetime) as tk_end_datetime,-- end
		ISNULL( ISNULL(from_t.tank_pi_name, from_p.short_code) , from_b.berth_short_name) as [from_location],-- from, pipeine receipt - from a pipeline, going to a tank   vessel delivery from  tank to berth
		ISNULL( ISNULL(to_t.tank_pi_name, to_p.short_code) , to_b.berth_short_name) as to_location, -- to
		ROUND( ISNULL(t.tk_final_gsv,0), 2) as tk_final_gsv, -- volume 
		t.tk_eod_datetime, 
		t.tk_scheduled_volume as tk_scheduled_volume,
		ISNULL(t.tk_day_sequence_number, 0) as sequence_number,
		ISNULL(from_p.[shutdown], 0) as [shutdown]

FROM tickets t INNER JOIN 
	 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN
	 ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN
	 entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN
	 products p On t.tk_product = p._key LEFT OUTER JOIN
	 tanks from_t ON from_t._key = tk_source_tank_key LEFT OUTER JOIN
	 pipeline from_p ON from_p._key = t.tk_pipeline_key LEFT OUTER JOIN
	 berths from_b ON from_b._key = t.tk_berth_key LEFT OUTER JOIN
	 tanks to_t ON to_t._key = tk_destination_tank_key LEFT OUTER JOIN
	 pipeline to_p ON to_p._key = t.tk_pipeline_key LEFT OUTER JOIN
	 berths to_b ON to_b._key = t.tk_berth_key

WHERE	(t.tk_eod_datetime < @eod) -- adds to the results table, any rows of data from beore the day the user selected that contains the status type "Scheduled." There should not be many if any that meet this criteria.
		AND (t.tk_site_key = @site)
		AND (@shipper = t.tk_entity1_key OR @shipper IS NULL)
		AND (@pipeline = t.tk_pipeline_key OR @pipeline IS NULL)
		AND t.tk_status = 1

declare @row_count int = (select count(_key) from @results)
declare @row_count_max int = 7

if (@row_count < @row_count_max)
	Begin
		insert into @results  (_key,
							   type_description,
							   stat_desc_key,
							   stat_description,
							   shipper_name,
							   product_code,
							   tk_batch_id,
							   tk_start_datetime,
							   tk_end_datetime,
							   from_location,
							   to_location,
							   tk_final_gsv,
							   tk_eod_datetime,
							   tk_scheduled_volume,
							   sequence_number,
							   shutdown_status)

		SELECT top(@row_count_max - @row_count)	t._key,
												CASE WHEN tt.typ_type = 1 THEN 'pipeline' WHEN tt.typ_type = 2 THEN 'vessel' WHEN tt.typ_type = 3 THEN 'transfer' END as type_description, -- type 
												ts._key as stat_desc_key,
												ts.stat_description, -- status
												e.short_name  as shipper_name, -- shipper
												p.product_code, -- product
												t.tk_batch_id, -- batch id
												ISNULL(t.tk_start_datetime, DATEADD(DAY, -1, t.tk_eod_datetime)) as tk_start_datetime,  -- start
												ISNULL(t.tk_end_datetime, t.tk_eod_datetime) as tk_end_datetime,-- end
												ISNULL( ISNULL(from_t.tank_pi_name, from_p.short_code) , from_b.berth_short_name) as [from_location],-- from, pipeine receipt - from a pipeline, going to a tank   vessel delivery from  tank to berth
												ISNULL( ISNULL(to_t.tank_pi_name, to_p.short_code) , to_b.berth_short_name) as to_location, -- to
												ROUND( ISNULL(t.tk_final_gsv,0), 2) as tk_final_gsv, -- volume 
												t.tk_eod_datetime, 
												t.tk_scheduled_volume as tk_scheduled_volume,
												ISNULL(t.tk_day_sequence_number, 0) as sequence_number,
												ISNULL(from_p.[shutdown], 0) as [shutdown]

		FROM tickets t INNER JOIN 
			 ticket_type tt ON t.tk_type = tt.typ_type INNER JOIN
			 ticket_status ts ON t.tk_status = ts.stat_type  LEFT OUTER JOIN
	 entities e ON e._key = t.tk_entity1_key LEFT OUTER JOIN
	 products p On t.tk_product = p._key LEFT OUTER JOIN
	 tanks from_t ON from_t._key = tk_source_tank_key LEFT OUTER JOIN
	 pipeline from_p ON from_p._key = t.tk_pipeline_key LEFT OUTER JOIN
	 berths from_b ON from_b._key = t.tk_berth_key LEFT OUTER JOIN
	 tanks to_t ON to_t._key = tk_destination_tank_key LEFT OUTER JOIN
	 pipeline to_p ON to_p._key = t.tk_pipeline_key LEFT OUTER JOIN
	 berths to_b ON to_b._key = t.tk_berth_key

		WHERE	(t.tk_eod_datetime > @eod) -- adds to the results table, any rows of data from after the day the user selected that contains anything other than "Void"
				AND (t.tk_site_key = @site)
				AND (@shipper = t.tk_entity1_key OR @shipper IS NULL)
				AND (@pipeline = t.tk_pipeline_key OR @pipeline IS NULL)
				AND t.tk_status <> 7
	End /* if(@row_count... END */

select top (7) *
from @results
order by tk_eod_datetime asc, sequence_number asc
			
END
GO
