--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_location_types_get]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Candice Iago    
-- Create date: 5/15/2020
-- Description:    gets a list of vessel activity records for the selected date

 

/*EXEC mgn_location_types_get
    @row  = '95dba723-3c12-488d-8f39-1da005b72b74'
    */
-- =============================================
Create   OR ALTER  PROCEDURE [dbo].[mgn_location_types_get]
    @row uniqueidentifier = null
    --, @type (pipeline, vessel, tank to tank transfer
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row

 

SELECT lt._key,
	   lt.lt_location_type as [location_type],
	   isnull(lt.lt_e1_type,0) as [e1_type],
	   isnull(lt.lt_e2_type,0) as [e2_type],
	   isnull(lt.lt_e3_type,0) as [e3_type]
      
FROM location_types as lt

WHERE lt._key = @row
    

 

END
 
GO
