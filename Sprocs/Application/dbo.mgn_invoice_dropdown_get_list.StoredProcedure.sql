--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_invoice_dropdown_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_invoice_dropdown_get_list
 
	*/

CREATE     OR ALTER    PROCEDURE [dbo].[mgn_invoice_dropdown_get_list] 
		@site_key uniqueidentifier = null, 
		@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();

declare	@date datetime = DATEADD( MONTH, -1, getdate())

SELECT '00000000-0000-0000-0000-000000000000' as [key], ' Select Invoice' as [name]
UNION
SELECT        vi._key as [key],
			  st.syn_vessel_name  + ' - ' + isnull(e.name,'NO AGENT') + ' - ' + CONVERT(varchar(300), st.syn_vessel_eta, 101) as name
			  
FROM		  vessel_invoice vi left outer JOIN 
			  synthesis_tickets AS st ON vi.vi_synthesis_ticket_key = st._key left outer JOIN
			  vessel_invoice_status AS vis ON vi.vi_vessel_invoice_status_key = vis._key left outer join
			  entities as e on vi.vi_marine_agent_key = e._key

WHERE		   vis.stat_type <> 5
			   AND st.syn_vessel_eta >= @date

ORDER BY name
    
END
	
GO
