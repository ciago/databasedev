--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_inbound_dashboard_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 5/15/2020
-- Description:	gets a list of vessel activity records for the selected date

/*EXEC mgn_inbound_dashboard_update
	@row  = '59a5d250-3188-432b-ad27-6116293172c1', @volume =   106137.85458626457   
	*/
-- =============================================
Create    OR ALTER     PROCEDURE [dbo].[mgn_inbound_dashboard_update]
	@user varchar(50) = 'default', 
	@row uniqueidentifier = null, 
	@next_row uniqueidentifier = null, 
	@site uniqueidentifier = null
	--@machine varchar(50) = 'default'
	--, @type (pipeline, vessel, tank to tank transfer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Update record info 
declare @completed_stat_int int = (SELECT top(1) stat_type FROM ticket_status WHERE stat_is_complete = 1)

UPDATE tickets
SET tk_status = @completed_stat_int,
	tk_end_datetime = (Select GETDATE())
WHERE _key = @row

UPDATE tickets
SET tk_start_datetime = (Select GETDATE())
WHERE _key = @next_row

END
GO
