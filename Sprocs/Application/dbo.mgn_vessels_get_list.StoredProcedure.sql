--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_vessels_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table


-- =============================================
CREATE   OR ALTER  PROCEDURE [dbo].[mgn_vessels_get_list]
	/*Defining the variable that I will eventually be filtering the table by*/
	@vessel_type uniqueidentifier = null,
	@site uniqueidentifier,
	@active bit = 1

	/*EXEC [mgn_vessels_get_list]
	@vessel_type  = '4eec6d8d-8c3f-4c9f-896b-6ba19865131a',
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
  EXEC [mgn_vessels_get_list]
	@vessel_type  = '00000000-0000-0000-0000-000000000000',
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
  EXEC [mgn_vessels_get_list]
	@vessel_type  = '00000000-0000-0000-0000-000000000000',
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@active = 0
	*/

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--Where _key = @volume_type

DECLARE @empty_guid UNIQUEIDENTIFIER = 0x0
if (@vessel_type = @empty_guid)
	Begin
		Select v._key, 
			   v.vessel_name, 
			   v.vessel_type_key,
			   vt.vessel_type,
			   ISNULL(v.vessel_description,'') as [vessel_description],
			   ISNULL(v.vessel_external_id, 0) [external_id],
			   v.vessel_loa,
			   v.[von],
			   v.[site]
		from vessels as v INNER JOIN 
			 vessel_types vt ON v.vessel_type_key = vt._key 
		Where ((v.[site] = @site) and (v.active = @active))
		GROUP BY v._key, v.vessel_name, v.vessel_type_key, vt.vessel_type, v.vessel_description, v.vessel_external_id, v.vessel_loa, v.von,v.[site]
		ORDER BY v.vessel_name
	End
Else
	BEGIN
		Select v._key, 
			   v.vessel_name, 
			   v.vessel_type_key,
			   vt.vessel_type,
			   ISNULL(v.vessel_description,'') as [vessel_description],
			   ISNULL(v.vessel_external_id, 0) [external_id],
			   v.vessel_loa,
			   v.von,
			   v.[site]
		from vessels as v INNER JOIN 
			 vessel_types vt ON v.vessel_type_key = vt._key 
		Where ((v.[site] = @site) and (v.active = @active) and (v.vessel_type_key = @vessel_type))
		GROUP BY v._key, v.vessel_name, v.vessel_type_key, vt.vessel_type, v.vessel_description, v.vessel_external_id, v.vessel_loa, v.von,v.[site]
		ORDER BY v.vessel_name
	END
	
END
GO
