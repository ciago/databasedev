--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_payment_terms_get_dropdown]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
create  OR ALTER  PROCEDURE [dbo].[mgn_payment_terms_get_dropdown] 
	@site_key uniqueidentifier = null, 
	@user varchar(50) = 'default'

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


SELECT 1000 AS RowKey, ' Select Terms' As description
UNION
SELECT [pt_terms] as RowKey, [pt_description] as description
FROM [dbo].[payment_terms]
ORDER BY description

END


-- EXEC [mgn_accounting_volume_types_get_list]





GO
