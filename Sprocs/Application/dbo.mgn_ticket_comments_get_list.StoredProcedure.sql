--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_ticket_comments_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  OR ALTER PROCEDURE [dbo].[mgn_ticket_comments_get_list]
    @ticket_key uniqueidentifier
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row
/*
execute [mgn_ticket_comments_get_list] @ticket_key = '5d55d4da-d27e-4a0a-a9af-27c4ace3a141'
*/
 


--MODA DATABASE

SELECT	_key,
		tc.tc_ticket_key,
		tc.[tc_date],
		tc.[tc_comment]

FROM ticket_comments tc

WHERE tc.tc_ticket_key = @ticket_key and 
	  tc.tc_deleted = 0

ORDER BY [tc_date]

END
GO
