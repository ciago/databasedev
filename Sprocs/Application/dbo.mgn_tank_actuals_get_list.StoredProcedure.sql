--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_actuals_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        Brandon Morey    
-- Create date: 5/4/2020
-- Description:    Gets a list of tank actuals from a selected day.

/*EXEC mgn_tank_actuals_get_list  
	@date  = '5/2/2021'
	,@site = NULL

	@date  = '5/31/2020'
    */
	
-- =============================================
CREATE  OR ALTER      PROCEDURE [dbo].[mgn_tank_actuals_get_list]

    @site uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 
	@date datetime = null

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from tank_actuals
DECLARE @eod_time int = (SELECT TOP(1) eod_time FROM site_configuration)
SET @date = DATEADD(HOUR, @eod_time, @date) 
DECLARE @eod datetime = dbo.get_eod_datetime(@date)
--print @eod
IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END

SELECT        ta._key,
			  ta.tank_key as [tank_key],
			  t.tank_name as [tank_name],
              DATEADD(DAY, -1, ta.eod_datetime) as [date_datetime],
              ROUND(ISNULL(ta.tank_gsv_change,0), 2) as [tank_gsv_change],
              ROUND(ISNULL(ta.tank_tov_change,0), 2) as [tank_tov_change],
			  ROUND(ISNULL(ta.open_gsv, 0), 2) as [open_gsv],
			  ROUND(ISNULL(ta.close_gsv,0), 2) as [close_gsv],
			  ta.site_key as [site_key],
              s.short_name as [site_name],
			  ISNULL(p.product_name, 'No Product Assigned') as [tank_product],
			  ISNULL(ta.tank_api, 0) as [tank_api],
			  ISNULL(ta.tank_temp, 0) as [tank_temp],
			  ta.eod_datetime

FROM          tank_actuals AS ta INNER JOIN
              sites AS s ON s._key = ta.site_key INNER JOIN
              tanks AS t ON ta.tank_key = t._key LEFT OUTER JOIN
			  (SELECT * FROM tank_products WHERE (tk_start_datetime <= @eod)
			  AND (tk_end_datetime >= @eod or tk_end_datetime IS null)) AS tp ON ta.tank_key = tp.tank_key LEFT OUTER JOIN
			  products AS p ON tp.product_key = p._key
			  
WHERE		  s._key = @site
	          AND eod_datetime = @eod
	          AND t.active = 1

ORDER BY eod_datetime
		  
END
GO
