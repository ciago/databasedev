--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_allocations_month_get]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--delete from pipeline_allocations
--USE [Moda]
--GO
--/****** Object:  StoredProcedure [dbo].[mgn_pipeline_allocations_month_get]    Script Date: 5/1/2020 7:35:46 AM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- =============================================
---- Author:		Candice Iago	
---- Create date: 2/26/20
---- Description:	Gets all of the allocations for all days in a certain month for a certain pipeline

/*EXEC mgn_pipeline_allocations_month_get
	@pipeline_key  = 'fc536541-5a92-466a-bcd1-db7fedcd15e7', 
	@month = 6,
	@year = 2020 , @product_key= '86e1baa4-142e-4d36-9d2b-7e9e3a3530d2'
	*/
---- =============================================
CREATE   OR ALTER  PROCEDURE [dbo].[mgn_pipeline_allocations_month_get]
 	@pipeline_key uniqueidentifier , 
	@month int , 
	@year int , 
	@product_key uniqueidentifier = null--'87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @month_start_datetime DATETIME = DATETIMEFROMPARTS(@year, @month, 1, 7 , 0, 0, 0)
DECLARE @month_end_datetime DATETIME = DATETIMEFROMPARTS(@year, (@month + 1) , 1, 7 , 0, 0, 0)

	-- fill the tmp table with all pipeline schedules by product by hour
EXEC mgn_pipeline_schedule_fill_hourly
	@month = @month,
	@year  = @year


	-- fill the tmp table with all pipeline schedules by product by hour
EXEC mgn_pipeline_schedule_fill_daily
	@month = @month,
	@year  = @year
-- insert any new rows in pipeline allocation tabel for any new entities, pipelines dates during this period 
BEGIN
		DECLARE @prdates TABLE (start_datetime datetime, end_datetime datetime)
		DECLARE @possible_records TABLE (start_datetime datetime, end_datetime datetime, pipeline_key uniqueidentifier, entity_key uniqueidentifier, product_key uniqueidentifier)

		;
		WITH Dates_CTE
			 AS (SELECT @month_start_datetime AS Dates
				 UNION ALL
				 SELECT Dateadd(DD, 1, Dates)
				 FROM   Dates_CTE
				 WHERE  Dates < DATEADD(DAY, -1, @month_end_datetime))
		INSERT INTO @prdates (start_datetime, end_datetime)
		SELECT Dates as startdate, DATEADD(day, 1, Dates) as enddate
		FROM   Dates_CTE 
		OPTION (MAXRECURSION 0) ;
		--select * from @dates

		INSERT INTO @possible_records (start_datetime, end_datetime, pipeline_key, entity_key, product_key)
		SELECT x.start_datetime, x.end_datetime, x.pipeline_key, e.entity_key as entity_key, x.product_key
		FROM
				   (
				   SELECT d.start_datetime, d.end_datetime, pp.pipeline_key as pipeline_key, pp.product_key
					FROM   @prdates d CROSS JOIN 
						   (SELECT pipeline_key, product_key
							FROM pipeline_product_assignment 
							WHERE start_datetime < getdate() and (end_datetime >= @month_end_datetime or end_datetime IS NULL)) pp
			) x	 
			INNER JOIN 
		  (select * FROM pipeline_entity_assignment  WHERE start_datetime < getdate() AND (end_datetime >= @month_end_datetime or end_datetime IS NULL)
		  ) as e  ON x.pipeline_key = e.pipeline_key

		--select * from @possible_records

		INSERT INTO pipeline_allocations(_key, pipeline_key, eod_datetime, entity_key, allocated_amount, actual_bbls, product_key)
		SELECT newid(), pr.pipeline_key, pr.end_datetime, pr.entity_key, 0,0, pr.product_key
		FROM @possible_records pr LEFT OUTER JOIN 
			 pipeline_allocations pa ON pr.end_datetime = pa.eod_datetime AND pr.pipeline_key = pa.pipeline_key AND pr.entity_key = pa.entity_key AND pr.product_key = pa.product_key
		WHERE pa._key IS NULL
END


		DECLARE @oemi uniqueidentifier = 'dd85132f-4039-4fda-8fcc-bbd2dd9b7656', 
				@vitol uniqueidentifier = 'd25051f3-4888-4bfb-89fd-4e1449ac0fba', 
				@anadarko uniqueidentifier = '86c0cacb-b542-49c8-b352-8be35f276541', 
				@eog uniqueidentifier = '1597bb7c-302c-461a-8873-0f4ed0381149'




		DECLARE @results TABLE ( [eod_datetime] datetime, total int, oemi_amt int, vitol_amt int, anadarko_amt int, 
								 eog_amt int, pipeline_key uniqueidentifier, product_key uniqueidentifier)

		IF(@product_key IS NULL)
		BEGIN
			INSERT INTO @results ([eod_datetime], pipeline_key, product_key)
			SELECT DISTINCT  eod_datetime, @pipeline_key, NULL
			FROM pipeline_allocations 
			WHERE eod_datetime BETWEEN @month_start_datetime AND @month_end_datetime
				  --AND (product_key = @product_key OR @product_key IS NULL)
		END
		ELSE
		BEGIN
			INSERT INTO @results ([eod_datetime], pipeline_key, product_key)
			SELECT DISTINCT  eod_datetime, @pipeline_key, product_key
			FROM pipeline_allocations 
			WHERE eod_datetime BETWEEN @month_start_datetime AND @month_end_datetime
				  AND product_key = @product_key 
		END
			

		--UPDATE re 
		--SET oemi_amt = ISNULL(al.allocated_amount, 0)
		--FROM pipeline_allocations al INNER JOIN
		--	 @results re ON al.eod_datetime = re.eod_datetime
		--WHERE al.pipeline_key = @pipeline_key
		--	  AND al.entity_key = @oemi


--SELECT total.allocated_amount, re.eod_datetime
UPDATE re 
SET oemi_amt = ISNULL(total.allocated_amount, 0)
FROM 
(SELECT SUM(allocated_amount)as allocated_amount, entity_key, pipeline_key, eod_datetime
 FROM pipeline_allocations
 WHERE (product_key = @product_key OR @product_key IS NULL) AND pipeline_key = @pipeline_key AND entity_key = @oemi
 GROUP BY entity_key, pipeline_key, eod_datetime) total INNER JOIN 
 @results re ON re.eod_datetime = total.eod_datetime


UPDATE re 
SET vitol_amt = ISNULL(total.allocated_amount, 0)
FROM 
(SELECT SUM(allocated_amount)as allocated_amount, entity_key, pipeline_key, eod_datetime
 FROM pipeline_allocations
 WHERE (product_key = @product_key OR @product_key IS NULL) AND pipeline_key = @pipeline_key AND entity_key = @vitol
 GROUP BY entity_key, pipeline_key, eod_datetime) total INNER JOIN 
 @results re ON re.eod_datetime = total.eod_datetime


 
UPDATE re 
SET anadarko_amt = ISNULL(total.allocated_amount, 0)
FROM 
(SELECT SUM(allocated_amount)as allocated_amount, entity_key, pipeline_key, eod_datetime
 FROM pipeline_allocations
 WHERE (product_key = @product_key OR @product_key IS NULL) AND pipeline_key = @pipeline_key AND entity_key = @anadarko
 GROUP BY entity_key, pipeline_key, eod_datetime) total INNER JOIN 
 @results re ON re.eod_datetime = total.eod_datetime


 
UPDATE re 
SET eog_amt = ISNULL(total.allocated_amount, 0)
FROM 
(SELECT SUM(allocated_amount)as allocated_amount, entity_key, pipeline_key, eod_datetime
 FROM pipeline_allocations
 WHERE (product_key = @product_key OR @product_key IS NULL) AND pipeline_key = @pipeline_key AND entity_key = @eog
 GROUP BY entity_key, pipeline_key, eod_datetime) total INNER JOIN 
 @results re ON re.eod_datetime = total.eod_datetime
 
UPDATE @results
SET total  = 
		ISNULL(oemi_amt, 0) + 
		ISNULL(vitol_amt, 0) + 
		ISNULL(anadarko_amt, 0) + 
		ISNULL(eog_amt, 0) 

		-- return data 
SELECT d.[date] as [Date], 
		ROUND(d.volume,0) as [Scheduled Volume],
		ROUND(ISNULL(total, 0),0) as [Total Allocated],  
		ROUND(d.volume - total,0) as Diff,
		ROUND(ISNULL(oemi_amt, 0),0) as [OEMI] , 
		ROUND(ISNULL(vitol_amt, 0),0)  as [Vitol], 
		ROUND(ISNULL(anadarko_amt, 0),0) as [Anadarko], 
		ROUND(ISNULL(eog_amt, 0),0) as [EOG] ,
		d.pipeline_key as [Edit]
FROM (
	SELECT CONVERT(Date, start_datetime) as [date], ISNULL(AVG(NULLIF(rate,0)),0) as rate, SUM(volume) as volume, pipeline_key, end_datetime
	FROM tmp_pipeline_by_day d 
	WHERE (pipeline_key = @pipeline_key) AND (product_key = @product_key OR @product_key IS NULL)
	GROUP BY start_datetime, pipeline_key, end_datetime)d INNER JOIN 
	@results r ON d.end_datetime = r.eod_datetime AND d.pipeline_key = r.pipeline_key

END
GO
