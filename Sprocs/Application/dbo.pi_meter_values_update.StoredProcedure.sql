--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[pi_meter_values_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER  PROCEDURE [dbo].[pi_meter_values_update]
	--@start datetime, 
	--@end datetime, 
	--@timestep nvarchar(20) = '1m'
AS
/* 
EXEC pi_meter_values_update 
	
*/

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DECLARE	@start datetime =  DATEADD(day,-3 , getdate()), 
	@end datetime = getdate(), 
	@timestep nvarchar(20) = '1m'
	--EXECUTE AS USER = 'MODAINGLESIDE\Candice.Iago'

-- convert dates to nvarchar
DECLARE @start2 nvarchar(20) = CONVERT(nvarchar(20), @start, 120),
	    @end2 nvarchar(20) = CONVERT(nvarchar(20), @end, 120)

-- sample insert table(this will be the final real table)
DECLARE @meter_values TABLE (
        meter_name nVARCHAR(100),
        [timestamp] DATETIME,
        totalizer float, 
		tag_path nvarchar(300), 
		rounded_timestamp datetime)

DECLARE @query nvarchar(max) 
DECLARE @name nvarchar(10) = 'Name'

SET @query = 'SELECT * FROM OPENQUERY([MODAPI],''
SELECT e.Name, sv.TimeStamp, Double(sv.Totalizer) as Totalizer,  e.PrimaryPath
FROM Master.Element.Element e
CROSS APPLY Master.Element.GetSampledValues
<
    ''''Meter'''', --Template - element template name
    {
        ''''|Totalizer'''', -- AttributeTemplatePath - Required
        ''''Totalizer'''', -- ValueColumnName - Required
        ''''Totalizer_UnitOfMeasure'''', -- UnitOfMeasureColumnName - Optional
        ''''Totalizer_Error'''', -- ErrorColumnName - Optional
        NULL -- UnitOfMeasure - Optional
    } -- AttributeMetadata - defines which columns to expose for a particular attribute template

>(e.ID,
	''''' + @start2+''''',  --Start Time
	'''''+@end2+''''',  --End Time
	'''''+@timestep+'''''  --Time Step
	) sv
WHERE e.Template = ''''Meter''''
'')'

--print @query
-- select * from @meter_values

INSERT INTO @meter_values(meter_name, [timestamp],totalizer, tag_path)
EXEC sp_executeSql  @query
--select * from @meter_values


--\Ingleside\Meters\SK-1050\

UPDATE @meter_values
SET tag_path = RTRIM(REPLACE(REPLACE(tag_path, '\Ingleside\Meters\', '' ), '\', '' )),
	meter_name = RTRIM(meter_name)
--select * from @meter_values



UPDATE @meter_values
SET rounded_timestamp = dateadd(mi, datediff(mi, 0, [timestamp]), 0)



--SELECT * from @meter_values WHERE meter_reading_row_key IS NOT NULL

--INSERT INTO [dbo].meter_readings
--           ([_key]
--           ,meter_key
--           ,[meter_timestamp]
--		   ,meter_totalizer)
--SELECT --newid(), 
--	   m._key,
--	   mv.rounded_timestamp, 
--	   mv.totalizer,	
--	   mv.meter_name, 
--	   mv.tag_path,
--	   mr.meter_totalizer, mr._key
	   UPDATE mr
	   SET mr.meter_totalizer = mv.totalizer
FROM @meter_values mv LEFT OUTER JOIN 
	 meters m ON m.meter_name = mv.meter_name AND tag_path = skid_name LEFT OUTER JOIN 
	 meter_readings mr ON mr.meter_key = m._key AND mr.meter_timestamp = mv.rounded_timestamp
WHERE mr.meter_totalizer IS NULL and mv.totalizer IS NOT NULL  AND mr._key IS NOT NULL --AND m._key = '4956D4EB-5884-4158-8B69-245ACC1BC0C4'
--ORDER BY mv.rounded_timestamp










 --SELECT * FROM @meter_values
END


GO
