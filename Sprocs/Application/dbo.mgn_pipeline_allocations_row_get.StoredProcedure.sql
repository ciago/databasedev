--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_allocations_row_get]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets one row from the allocation for a single day for a single pipeline for a single entity

/*EXEC mgn_pipeline_allocations_row_get
	@row  = 'F04C98D8-A192-415D-8BCC-0C50DEC56838'
	*/
-- =============================================
CREATE    OR ALTER  PROCEDURE [dbo].[mgn_pipeline_allocations_row_get]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row

Select p._key as row_key, 
	   pl.description as pipeline_description, 
	   e.name as [entity_name], 
	   p.allocated_amount as [amount], 
	   DATEADD(day, -1, p.eod_datetime) as [date], 
	   p.pipeline_key as pipeline_key,
	   prod.product_code as product_name
from pipeline_allocations p INNER JOIN 
	 entities e ON p.entity_key = e._key LEFT OUTER JOIN 
	 pipeline as pl ON pl._key = p.pipeline_key LEFT OUTER JOIN 
	 products prod ON prod._key = p.product_key
WHere  @row = p._key
	
END
GO
