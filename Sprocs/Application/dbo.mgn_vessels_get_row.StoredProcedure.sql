--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_vessels_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Obtains a single row of volume accounts data by searching for a specified guid.


-- =============================================
CREATE     OR ALTER  PROCEDURE [dbo].[mgn_vessels_get_row]
	@row uniqueidentifier = null

	/*EXEC [mgn_vessels_get_row]
	@row  = '9f0b321f-72ba-40fc-8cbe-59b720e9dd5a'
	*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from 
--WHere _key = @row
   
SELECT v._key, 
	   v.[vessel_name], 
	   v.[vessel_type_key],
	   vt.vessel_type,
	   v.[vessel_description],
	   v.[vessel_external_id],
	   v.[vessel_loa],
	   v.[von],
	   v.[site],
	   v.active

			  
FROM vessels as v INNER JOIN 
	 vessel_types vt ON v.vessel_type_key = vt._key

WHERE @row = v._key
GROUP BY v._key, v.[vessel_name], v.[vessel_type_key], vt.vessel_type, v.[vessel_description], v.[vessel_external_id], v.[vessel_loa],v.[von],v.[site],v.active

	  
END
GO
