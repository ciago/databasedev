--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_business_entities_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE OR ALTER      PROCEDURE [dbo].[mgn_business_entities_update]

	@row uniqueidentifier,
	@name nvarchar(100),
	@short_name nvarchar(20),
	@type int,
	@active bit,
	@transportation_mode_key uniqueidentifier,
	@city nvarchar(50) = null,
	@state_key uniqueidentifier = '00000000-0000-0000-0000-000000000000',
	@external_code nvarchar(32) = '',
	@fein nvarchar(32) = '',
	@user varchar(50),
	@payment_terms int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from entities
--Where _key = @row

IF @row = '00000000-0000-0000-0000-000000000000'
BEGIN
	SET @row = newid()
END

IF(@row IN (SELECT _key from entities))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM entities WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE entities

SET		name = @name,
		short_name = @short_name,
		type = @type,
		active = @active,
		transportation_mode_key = @transportation_mode_key,
		city = @city,
		state_key = @state_key,
		external_code = @external_code,
		fein = @fein,
		record_info = @json,
		payment_terms = @payment_terms

WHERE _key = @row

END

ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[entities]
           ([_key]
		   ,[name]
		   ,[short_name]
		   ,[type]
		   ,[active]
		   ,[transportation_mode_key]
		   ,[city]
		   ,[state_key]
		   ,[external_code]
		   ,[fein]
           ,[record_info]
		   ,[payment_terms])
     VALUES
           (@row
		   ,@name
		   ,@short_name
		   ,@type
		   ,@active
		   ,@transportation_mode_key
		   ,@city
		   ,@state_key
		   ,@external_code
		   ,@fein
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   ,@payment_terms)

END

END
GO
