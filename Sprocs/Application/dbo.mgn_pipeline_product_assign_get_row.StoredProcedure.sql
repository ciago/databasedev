--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_product_assign_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Obtains a single row of pipeline product assignment data from a specified guid.

/*EXEC mgn_pipeline_product_assign_get_row
	@row  = 'd5ac78b1-6677-45b0-8412-b84881591f08'
	*/
-- =============================================
CREATE   OR ALTER  PROCEDURE [dbo].[mgn_pipeline_product_assign_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from tank_actuals
--WHere _key = @row
   
SELECT        ppa._key,
			  ppa.pipeline_key as [pipeline_key],
			  ppa.product_key as [product_key],
			  ppa.start_datetime as [start_datetime],
			  ppa.end_datetime as [end_datetime]
			  
FROM pipeline_product_assignment AS ppa

WHERE @row = ppa._key
	  
END
GO
