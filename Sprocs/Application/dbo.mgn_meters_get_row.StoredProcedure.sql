--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_meters_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER      PROCEDURE [dbo].[mgn_meters_get_row]
	@row uniqueidentifier = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from meters
--WHere _key = @row
   
SELECT			m._key,
				isnull(m.meter_name				,'') as [meter_name],
				isnull(m.meter_name_2			,'') as [meter_description],
				isnull(m.locations_key			,'00000000-0000-0000-0000-000000000000') as [locations_key],
				isnull(l.ln_location_name		,'') as [location_name],
				isnull(m.max_allowable_flow		,0) as[max_allowable_flow],
				isnull(m.max_allowable_pressure ,0) as[max_allowable_pressure],
				isnull(mf.meter_factor			,0) as [meter_factor],
				m.site_key							as [site_key],
				m.active							as [active]

FROM			meters AS m left outer JOIN
				locations AS l ON l._key = m.locations_key left outer join
				(select * from meter_factor where mtr_start_datetime <= getdate() and (mtr_end_datetime >= getdate() or mtr_end_datetime is null)) as mf on mf.meter_key = m._key

WHERE			@row = m._key --and
				--mf.mtr_start_datetime <= getdate() and
				--(mf.mtr_end_datetime >= getdate() or mf.mtr_end_datetime is null)
	  
END
GO
