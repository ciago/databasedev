--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_ticket_status_get_dropdown]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  OR ALTER   PROCEDURE [dbo].[mgn_ticket_status_get_dropdown] 
	@site_key uniqueidentifier = null, 
	@user varchar(50) = 'default'

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


SELECT 0 AS RowKey, 'Select Type' As description
UNION
SELECT stat_type as RowKey, [stat_description] as description
FROM [dbo].[ticket_status]
WHERE active = 1
ORDER BY RowKey

END

GO
