USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_contracts_get_list]    Script Date: 5/13/2021 11:18:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create or ALTER          PROCEDURE [dbo].[mgn_charges_general_get_list]

	@type int = null,
	@category uniqueidentifier = null,
	@site uniqueidentifier = null
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from contacts

/*
Exec [mgn_contracts_get_list]
@effective_date = '2021-05-12 08:00:00.000'
*/

SELECT			cg._key as [_key],
				cg.cg_charge_description as [description],
				cg.cg_charge_amount as [amount],
				cut.charge_unit_description as [unit_type],
				cc.chc_category as [category],
				isnull(convert(nvarchar(50),cg.cg_start_date,101),'N/A') as [start_date],
				isnull(convert(nvarchar(50),cg.cg_end_date,101),'N/A') as [end_date],
				ct.cht_description as [charge_type],
				s.short_name as [site]

FROM			charges_general AS cg INNER JOIN
				charge_unit_type AS cut ON cg.cg_charge_unit_key = cut._key LEFT OUTER JOIN
				charge_categories AS cc ON cg.cgc_category = cc._key LEFT OUTER JOIN
				charge_types AS ct ON cg.cg_charge_type = ct.cht_charge_type_int left outer join
				sites as s on cg.cg_site = s._key

WHERE			(cg.cg_charge_type = @type or @type is null)
				AND (cg.cgc_category = @category OR @category IS NULL)
				AND (cg.cg_site = @site or @site is null)

ORDER BY		[category], [unit_type]
	  
END
