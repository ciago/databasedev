--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_actuals_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Brandon Morey    
-- Create date: 5/4/2020
-- Description:    Gets a list of pipeline actuals from a selected day.

/*EXEC mgn_pipeline_actuals_get_list  
	@date  = '5/31/2020'
    */
	
-- =============================================
CREATE   OR ALTER    PROCEDURE [dbo].[mgn_pipeline_actuals_get_list]

    @site uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 
	@date datetime = null



AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from pipeline_actuals

--DECLARE @eod datetime = DATEADD(DAY, 1, SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date), 07, 00))

DECLARE @eod_time int = (SELECT TOP(1) eod_time FROM site_configuration)
SET @date = DATEADD(HOUR, @eod_time, @date) 
DECLARE @eod datetime = dbo.get_eod_datetime(@date)


IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END




SELECT        pa._key,
			  pa.pipeline_key as [pipeline_key],
			  pl.description as [pipeline_desc],
              DATEADD( day, -1, pa.eod_datetime) as [date_datetime],
              ISNULL(pa.gsv_bbls,0) as [gsv_bbls],
			  ISNULL(pa.open_totalizer, 0) as [open_totalizer],
			  ISNULL(pa.close_totalizer,0) as [close_totalizer],
			  pa.pipeline_error as [pipeline_error],
			  ISNULL(pa.pipeline_error_message,'') as [pipeline_error_message],
			  pa.site_key as [site_key],
              s.short_name as [site_name]

FROM          pipeline_actuals AS pa INNER JOIN
              sites AS s ON s._key = pa.site_key INNER JOIN
              pipeline AS pl ON pa.pipeline_key = pl._key
			  
WHERE     s._key = @site
	  AND eod_datetime = @eod

ORDER BY eod_datetime
		  
END
 
GO
