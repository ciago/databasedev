--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_product_verification]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>

/*EXEC mgn_product_get_list 
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@product = 'WTI'*/

-- =============================================
CREATE   OR ALTER   PROCEDURE [dbo].[mgn_product_verification] 
	@site uniqueidentifier = null,
	@product nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @prod_exist bit

if EXISTS 
	(
		SELECT _key
		 from products 
		 WHERE (@site =  [site_key]) AND
			   ([deleted] = 0) AND
			   ((@product = product_code) OR
				(@product = product_name))
	)
	BEGIN
		Set @prod_exist = 1
	END
Else
	Begin
		Set @prod_exist = 0
	End

Select @prod_exist

END
GO
