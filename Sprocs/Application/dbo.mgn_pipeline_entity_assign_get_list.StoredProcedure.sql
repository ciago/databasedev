--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_pipeline_entity_assign_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Brandon Morey    
-- Create date: 5/4/2020
-- Description:    Gets a list of pipeline entity assignments from a selected day.

/*EXEC mgn_pipeline_entity_assign_get_list  
	@pipeline = '263486ba-8550-49e2-81d2-055bbb61b0ae'
	
    */
	
-- =============================================
CREATE     OR ALTER   PROCEDURE [dbo].[mgn_pipeline_entity_assign_get_list]

    @pipeline uniqueidentifier = null,
	@site uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from pipeline_entity_assignment



IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END

SELECT        pea._key,
			  pea.pipeline_key as [pipeline_key],
			  pea.entity_key as [entity_key],
			  pea.start_datetime as [start_datetime],
			  pea.end_datetime as [end_datetime],
			  pl.description as [pipeline_desc],
			  ISNULL(e.short_name, '') as [entity_name]
			  
FROM		  pipeline_entity_assignment AS pea INNER JOIN
              pipeline AS pl ON pea.pipeline_key = pl._key LEFT OUTER JOIN
			  entities AS e ON e._key = pea.entity_key

WHERE		  (end_datetime IS NULL OR end_datetime > getdate())
	          AND (pea.pipeline_key = @pipeline or @pipeline is null)
	  
END
GO
