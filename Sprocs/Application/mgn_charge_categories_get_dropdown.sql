USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_charge_categories_get_dropdown]    Script Date: 5/13/2021 3:43:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>

/*EXEC mgn_charge_unit_type_get_list

	*/

-- =============================================
ALTER         PROCEDURE [dbo].[mgn_charge_categories_get_dropdown]
	@site_key uniqueidentifier = null, 
	@user varchar(50) = 'default'

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

	SELECT '00000000-0000-0000-0000-000000000000', 'Select Category'
	UNION
    SELECT _key, chc_category
    FROM charge_categories
	where chc_active = 1
    
END
