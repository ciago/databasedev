--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_country_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/26/2021
-- Description:		Select list of countries for dropdown usage.

/*EXEC mgn_country_get_list

	*/

-- =============================================
CREATE  OR ALTER     PROCEDURE [dbo].[mgn_country_get_list]

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

	SELECT '00000000-0000-0000-0000-000000000000', ' Select Country' as [name]
	UNION
    SELECT _key, country
    FROM countries

	ORDER BY name
    
END
GO
