--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_charges_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   OR ALTER   PROCEDURE [dbo].[mgn_charges_update]

	@row uniqueidentifier,
	@charge_description nvarchar(50),
	@charge_amount decimal(18,5),
	@charge_unit_key uniqueidentifier,
	@start_date datetime,
	@end_date datetime = NULL,
	@user varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from charges
--Where _key = @row

IF(@row IN (SELECT _key from charges))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT record_info FROM charges WHERE _key = @row)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE charges

SET		charge_description = @charge_description,
		charge_amount = @charge_amount,
		charge_unit_key = @charge_unit_key,
		start_date = @start_date,
		end_date = @end_date,
		record_info = @json

WHERE _key = @row

END

ELSE
BEGIN
-- add new charge record into invoice charge records that havent been completed
-- it does not exist so insert a new row 

INSERT INTO [dbo].[invoice_charge]
           ([_key]
           ,[ic_vessel_invoice_key]
		   ,[ic_charges_key]
           ,[ic_charge_date]
           ,[ic_charge_bool]
           ,[ic_unit_amount]
           ,[ic_charge_total]
           ,[ic_charge_override]
           ,[ic_record_info])

           (SELECT		newid(), vi._key, @row, st.syn_vessel_eta, 'False', 0, 0, 'False', dbo.fn_json_record_info(NULL, 'created', 'Brandon', HOST_NAME(), GETDATE())
			FROM		vessel_invoice vi INNER JOIN
						vessel_invoice_status vs ON vi.vi_vessel_invoice_status_key = vs._key INNER JOIN 
						synthesis_tickets AS st ON vi.vi_synthesis_ticket_key = st._key
			WHERE		vs.stat_type < 5)

INSERT INTO [dbo].[charges]
           ([_key]
           ,[charge_description]
		   ,[charge_amount]
		   ,[charge_unit_key]
		   ,[start_date]
		   ,[end_date]
           ,[record_info])
     VALUES
           (@row
		   ,@charge_description
		   ,@charge_amount
		   ,@charge_unit_key
		   ,@start_date
		   ,@end_date
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE())
		   )

END

END
GO
