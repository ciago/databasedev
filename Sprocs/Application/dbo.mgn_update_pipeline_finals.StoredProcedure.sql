--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_update_pipeline_finals]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER  PROCEDURE [dbo].[mgn_update_pipeline_finals] 
AS
BEGIN



-- update net AND TICKET ID on  PIPELINE tickets from synthesis 
UPDATE t 
SET tk_final_nsv = et.[NET_VOL],
	tk_status = 6
FROM tickets t INNER JOIN 
	 tanks ta ON t.tk_to_key = ta._key LEFT OUTER JOIN 
	 pipeline p ON p._key = t.tk_from_key LEFT OUTER JOIN 
	 Moda_Final_Tickets mft ON mft._key = t._key LEFT OUTER JOIN 
	 EMERSON.Synthesis_Test_02242020.dbo.TICKET et ON et.LS_TICKET_ID = mft.ticket_id INNER JOIN 
 (SELECT MAX(SA_TICKET_ID) as SA_TICKET_ID, LS_TICKET_ID
  FROM EMERSON.Synthesis_Test_02242020.dbo.TICKET
  GROUP BY LS_TICKET_ID) unique_tickets ON et.SA_TICKET_ID = unique_tickets.SA_TICKET_ID
 LEFT OUTER JOIN 
	 (SELECT et.SA_TICKET_ID,  MIN(tr.SA_TICKET_READING_ID) as open_ticket_id
		FROM EMERSON.Synthesis_Test_02242020.dbo.TICKET et INNER JOIN 
			 EMERSON.Synthesis_Test_02242020.dbo.TICKET_READING tr ON et.SA_TICKET_ID = tr.SA_TICKET_ID
		--WHERE et.SA_TICKET_ID = 15169
		GROUP BY et.SA_TICKET_ID) open_read ON et.SA_TICKET_ID = open_read.SA_TICKET_ID INNER JOIN 
		EMERSON.Synthesis_Test_02242020.dbo.TICKET_READING tr_open ON tr_open.SA_TICKET_READING_ID = open_read.open_ticket_id LEFT OUTER JOIN 
	 (SELECT et.SA_TICKET_ID,  MAX(tr.SA_TICKET_READING_ID) as close_ticket_id
		FROM EMERSON.Synthesis_Test_02242020.dbo.TICKET et INNER JOIN 
			 EMERSON.Synthesis_Test_02242020.dbo.TICKET_READING tr ON et.SA_TICKET_ID = tr.SA_TICKET_ID
		--WHERE et.SA_TICKET_ID = 15169
		GROUP BY et.SA_TICKET_ID) close_read ON et.SA_TICKET_ID = close_read.SA_TICKET_ID INNER JOIN 
		EMERSON.Synthesis_Test_02242020.dbo.TICKET_READING tr_close ON tr_close.SA_TICKET_READING_ID = close_read.close_ticket_id

WHERE et.MEASUREMENT_TICKET_TYPE_ID = 1002 
AND (ROUND( t.tk_final_nsv ,2) <> et.[NET_VOL] OR t.tk_final_nsv IS NULL)  AND ISNUMERIC(et.LS_TICKET_ID) = 1
AND t.tk_eod_datetime > DATEADD(MONTH, -2, getdate())


-- update GSV on PIPELINE tickets from synthesis 
UPDATE t 
SET tk_final_gsv = CASE WHEN et.[TICKET_GSV_OVERRIDE] = 1 THEN et.[TICKET_GSV_OVERRIDE_AMT] ELSE et.TICKET_GSV_AMT END
FROM tickets t INNER JOIN 
	 tanks ta ON t.tk_to_key = ta._key LEFT OUTER JOIN 
	 pipeline p ON p._key = t.tk_from_key LEFT OUTER JOIN 
	 Moda_Final_Tickets mft ON mft._key = t._key LEFT OUTER JOIN 
	 EMERSON.Synthesis_Test_02242020.dbo.TICKET et ON et.LS_TICKET_ID = mft.ticket_id INNER JOIN 
 (SELECT MAX(SA_TICKET_ID) as SA_TICKET_ID, LS_TICKET_ID
  FROM EMERSON.Synthesis_Test_02242020.dbo.TICKET
  GROUP BY LS_TICKET_ID) unique_tickets ON et.SA_TICKET_ID = unique_tickets.SA_TICKET_ID
 LEFT OUTER JOIN 
	 (SELECT et.SA_TICKET_ID,  MIN(tr.SA_TICKET_READING_ID) as open_ticket_id
		FROM EMERSON.Synthesis_Test_02242020.dbo.TICKET et INNER JOIN 
			 EMERSON.Synthesis_Test_02242020.dbo.TICKET_READING tr ON et.SA_TICKET_ID = tr.SA_TICKET_ID
		--WHERE et.SA_TICKET_ID = 15169
		GROUP BY et.SA_TICKET_ID) open_read ON et.SA_TICKET_ID = open_read.SA_TICKET_ID INNER JOIN 
		EMERSON.Synthesis_Test_02242020.dbo.TICKET_READING tr_open ON tr_open.SA_TICKET_READING_ID = open_read.open_ticket_id LEFT OUTER JOIN 
	 (SELECT et.SA_TICKET_ID,  MAX(tr.SA_TICKET_READING_ID) as close_ticket_id
		FROM EMERSON.Synthesis_Test_02242020.dbo.TICKET et INNER JOIN 
			 EMERSON.Synthesis_Test_02242020.dbo.TICKET_READING tr ON et.SA_TICKET_ID = tr.SA_TICKET_ID
		--WHERE et.SA_TICKET_ID = 15169
		GROUP BY et.SA_TICKET_ID) close_read ON et.SA_TICKET_ID = close_read.SA_TICKET_ID INNER JOIN 
		EMERSON.Synthesis_Test_02242020.dbo.TICKET_READING tr_close ON tr_close.SA_TICKET_READING_ID = close_read.close_ticket_id

WHERE et.MEASUREMENT_TICKET_TYPE_ID = 1002 
AND ROUND( t.tk_final_gsv ,2) <> et.TICKET_GSV_AMT 
AND ISNUMERIC(et.LS_TICKET_ID) = 1
AND t.tk_eod_datetime > DATEADD(MONTH, -2, getdate())




END
GO
