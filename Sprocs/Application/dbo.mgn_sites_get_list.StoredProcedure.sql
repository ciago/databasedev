--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_sites_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 5/15/2020
-- Description:	gets a list of vessel activity records for the selected date

/*EXEC mgn_sites_get_list
	
	*/
-- =============================================
CREATE   OR ALTER    PROCEDURE [dbo].[mgn_sites_get_list]
	@user varchar(50) = 'default'
	--, @type (pipeline, vessel, tank to tank transfer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT _key, name as site_name, short_name as site_code
FROM sites
	
END
GO
