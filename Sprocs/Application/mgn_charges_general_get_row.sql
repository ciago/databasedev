USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_contracts_get_row]    Script Date: 5/13/2021 11:18:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create or ALTER         PROCEDURE [dbo].[mgn_charges_general_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from contacts
--exec [mgn_charges_general_get_row] @row = 'EA6450BB-5B56-462B-A026-49AD1F1EE180'
   
SELECT			cg._key as [_key],
				cg.cg_charge_description as [description],
				cg.cg_charge_amount as [amount],
				cg.cg_charge_unit_key as [unit_type],
				cg.cgc_category as [category],
				cg.cg_start_date as [start_date],
				cg.cg_end_date as [end_date],
				cg.cg_charge_type as [charge_type],
				cg.cg_site as [site]

FROM			charges_general as cg

WHERE			@row = cg._key
	  
END
