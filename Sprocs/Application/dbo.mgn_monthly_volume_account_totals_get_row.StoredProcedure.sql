--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_monthly_volume_account_totals_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Obtains a single row of volume accounts data by searching for a specified guid.

/*EXEC mgn_monthly_volume_account_totals_get_row
	@row  = '92AC36D2-3914-44E0-9528-272ED95999A5'
	*/
-- =============================================
CREATE   OR ALTER   PROCEDURE [dbo].[mgn_monthly_volume_account_totals_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from 
--WHere _key = @row
   
Select		  mvat._key,
			  mvat.month,
			  mvat.year, 
			  mvat.volume_account_key, 
			  va.accounts, 
			  va.unit_description,
			  va.accounting_volume_types_key,
			  avt.type_description,
			  ISNULL(va.entity_key, '00000000-0000-0000-0000-000000000000') as [entity_key],
			  ISNULL(ent.short_name, '') as [short_name], 
			  ISNULL(va.vessel_types_key, '00000000-0000-0000-0000-000000000000') as [vessel_types_key],
			  ISNULL(vt.vessel_type, '') as [vessel_type],
			  ISNULL(va.gl_account, '') as [gl_account],
			   va.[start],
			   va.[stop],
			   avt.auto_data,
			   mvat.monthly_account_total


FROM          monthly_volume_account_totals AS mvat INNER JOIN
              volume_accounts AS va on mvat.volume_account_key = va._key left join
			  accounting_volume_types avt ON va.accounting_volume_types_key = avt._key LEFT JOIN
			  entities ent ON va.entity_key = ent._key LEFT JOIN 
			  vessel_types vt ON va.vessel_types_key = vt._key 

WHERE @row = mvat._key
GROUP BY mvat._key,
		 mvat.month,
		 mvat.year, 
		 mvat.volume_account_key, 
		 va.accounts, 
		 va.unit_description,
		 va.accounting_volume_types_key,
		 avt.type_description,
		 [entity_key],
		 [short_name], 
		 [vessel_types_key],
		 [vessel_type],
		 [gl_account],
		 va.[start],
		 va.[stop],
		 avt.auto_data,
		 mvat.monthly_account_total

	  
END
GO
