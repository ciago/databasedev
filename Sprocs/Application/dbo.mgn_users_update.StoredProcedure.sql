--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_users_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 4/28/21
-- Description:	updates the user's group assignment and last login date 

/*EXEC [mgn_users_update]
	*/
-- =============================================
CREATE OR ALTER  PROCEDURE [dbo].[mgn_users_update]
	@user nvarchar(50) = null, 
	@highest_group_number int 
	
AS
BEGIN

IF EXISTS(SELECT user_login_name FROM users WHERE user_login_name = @user)
BEGIN
	UPDATE users 
	SET user_last_login = getdate(),
		user_highest_group_number = @highest_group_number
	WHERE user_login_name = @user
END
ELSE
BEGIN
	INSERT INTO [dbo].[users]
           ([_key]
           ,[user_login_name]
           ,[user_last_login]
           ,[user_highest_group_number])
     VALUES
           (newid(),
		   @user,
		   getdate(),
		   @highest_group_number)

END
END
GO
