--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_users_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table

/*EXEC [mgn_users_get_list]*/
-- =============================================
Create  OR ALTER  PROCEDURE [dbo].[mgn_users_get_list]
	/*Defining the variable that I will eventually be filtering the table by*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from pipeline_allocations
--Where _key = @volume_type

--DECLARE @eod datetime = SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date), 07, 00)

Select u.userid, 
	   u.username, 
	   u.first_name, 
	   u.last_name, 
	   u.employee_id, 
	   u.[password], 
	   u.[security], 
	   us.[security_int], 
	   us.[_key] as security_key, 
	   u.[is_active]

from users as u Inner JOIN 
	 user_security us on u.[security] = us.[secuirty_level]

GROUP BY u.userid, u.username, u.first_name, u.last_name, u.employee_id, u.[password], u.[security], us.[security_int], us.[_key], u.[is_active]
--ORDER BY berth_description, valve_start

Order by u.[is_active] desc, us.[security_int], u.username
	
END
GO
