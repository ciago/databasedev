--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_ticket_comments_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  OR ALTER   PROCEDURE [dbo].[mgn_ticket_comments_get_row] 
    @rowKey uniqueidentifier
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

--MODA DATABASE

Select  _key,
		tc_date,
		tc_comment
from ticket_comments
where _key = @rowKey and
	  tc_deleted = 0

END
GO
