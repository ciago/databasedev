--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_hartree_ignition_start_dock_ticket]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/18/2021
-- Description:		Update selected business entities record with new user input details.

/*EXEC [mgn_ignition_start_dock_ticket] 
	@ticket = 1005
	
	// 
	*/
-- =============================================
CREATE  OR ALTER        PROCEDURE [dbo].[mgn_hartree_ignition_start_dock_ticket]

	@ticket bigint
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from entities
--Where _key = @row

Update tickets
set tk_status = 8
where tk_ticket_id = @ticket



SELECT t.tk_ticket_id,
	   p.product_code,
	   m.meter_id
FROM tickets t LEFT OUTER JOIN 
	 products p ON t.tk_product = p._key LEFT OUTER JOIN 
	 (SELECT * from meter_products mp WHERE mp.mtr_start_datetime <= getdate() and (mp.mtr_end_datetime >= getdate() OR mp.mtr_end_datetime IS NULL)) mp ON mp.product_key = t.tk_product LEFT  OUTER JOIN 
	 meters m ON m._key = mp.meter_key LEFT OUTER JOIN 
	 locations l ON m.locations_key = l._key INNER JOIN 
	 location_types lt ON l.ln_location_type = lt.lt_location_type
WHERE tk_ticket_id = @ticket 
	  AND lt.lt_location_type = 3 -- dock type 
	




----declare @ticket bigint = 1005
--Select t.tk_ticket_id,
--	   p.product_code,
--	   m.meter_name
--from tickets as t left outer join
--	 products as p on t.tk_product = p._key left outer join
--	 meter_products as mp on t.tk_product = mp.product_key left outer join
--	 meters as m on mp.meter_key = m._key
--where tk_ticket_id = @ticket

END

GO
