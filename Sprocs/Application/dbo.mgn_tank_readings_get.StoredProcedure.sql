--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_readings_get]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:
-- Create date:
-- Description:

/*EXEC mgn_tank_readings_get
	@date = '2021-01-25 12:16:00.000',
	@tank_key = 'aa2abc9f-19a5-432d-8147-0a0b8b43fd2f'

*/
-- =============================================
CREATE   OR ALTER  PROCEDURE [dbo].[mgn_tank_readings_get]

		@site uniqueidentifier = NULL, 
	    @tank_key uniqueidentifier, 
		@date datetime
		
AS	
BEGIN

--declare @eod datetime 
--SET @eod  = DATEADD(DAY, 1, SMALLDATETIMEFROMPARTS(YEAR(@date), MONTH(@date), DAY(@date) , 07, 00))
DECLARE @eod_time int = (SELECT TOP(1) eod_time FROM site_configuration)
SET @date = DATEADD(HOUR, @eod_time, @date) 
DECLARE @eod datetime = dbo.get_eod_datetime(@date)

IF @site IS NULL
BEGIN 
	SET @site = dbo.get_default_site();
END

SELECT		t.tank_name as [tank_name], 
			tr.tank_timestamp as [tank_timestamp],
			ISNULL( ROUND(tr.tank_gsv,2),0) as [tank_gsv], 
			ISNULL( ROUND(tr.tank_tov,2),0) as [tank_tov],
			ISNULL(p.product_name, 'No Product Assigned') as [tank_product]			

FROM		tank_readings tr INNER JOIN 
			tanks t ON tr.tank_key = t._key INNER JOIN 
			sites s ON t.site_key = s._key LEFT OUTER JOIN
			(SELECT * FROM tank_products WHERE (tk_start_datetime <= @eod)
			AND (tk_end_datetime >= @eod or tk_end_datetime IS null)) AS tp ON tr.tank_key = tp.tank_key LEFT OUTER JOIN
			products AS p ON tp.product_key = p._key

WHERE		t._key = @tank_key AND s._key = @site 
			AND tank_timestamp BETWEEN DATEADD(DAY, -1, @eod) AND @eod

ORDER BY tank_timestamp

END
GO
