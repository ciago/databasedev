USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_contracts_validate_update]    Script Date: 5/13/2021 11:18:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create or ALTER                       PROCEDURE [dbo].[mgn_charges_general_validate_update]

	@row uniqueidentifier,
	@charge_description nvarchar(50) = null,
	@charge_amount decimal(18,5) = null,
	@unit_type uniqueidentifier = null,
	@charge_category uniqueidentifier = null,
	@start_date datetime = null,
	@end_date datetime = null,
	@charge_type int = null,
	@charge_site uniqueidentifier = null,
	@site uniqueidentifier,
	@user varchar(50)
	
	
	/*EXEC [mgn_charges_general_validate_update]
	@row = '05a70c1d-2804-46a2-8fe3-7d2182ae8b26', -- 'c6d73637-edd5-4ff8-9f51-e33572e822f8','f935e668-6a5e-488b-a67d-7154449d1d7e'
	@charge_description = null,
	@charge_amount = 0,
	@unit_type = null,
	@charge_category = null ,
	@start_date = null,
	@charge_type = null ,
	@charge_site = null ,
	@site  = 'c6d73637-edd5-4ff8-9f51-e33572e822f8',
	@user = 'ciago'
	
	// 
	*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

declare @message nvarchar(400)
declare @description_message nvarchar(50) = ''
declare @amount_message nvarchar(50) = ''
declare @unit_type_message nvarchar(50) = ''
declare @category_message nvarchar(50) = ''
declare @charge_type_message nvarchar(50) = ''
declare @site_key_message nvarchar(50) = ''
declare @start_date_message nvarchar (50) = ''
-- Check for stuff

IF(@charge_description is null)
Begin
	Set @description_message = 'A Charge Description must be entered. ' 
End

IF (@charge_amount IS NULL)
Begin
	Set @amount_message = 'A Charge Amount must be entered. '
End

if (@unit_type is null)
begin
	Set @unit_type_message = 'A Unit Type must be selected. '
end

if (@charge_category IS null)
begin
	Set @category_message = 'A Category must be selected. '
end

if (@charge_type IS null)
begin
	Set @charge_type_message = 'A Charge Type must be selected. '
end

if (@charge_site IS null)
begin
	Set @site_key_message = 'A Site must be selected. '
end

declare @start_date_needed bit = (	select	chc.chc_charge_date_required 
									from	charges_general as cg left outer join
											charge_categories as chc on cg.cgc_category = chc._key
									where	cg._key = @row and
											chc.chc_active = 1)

if ((@start_date IS null) and (@start_date_needed = 1))
begin
	Set @start_date_message = 'A Start Date must be selected. '
end

set @message = @description_message + @amount_message + @unit_type_message + @category_message + @charge_type_message + @site_key_message + @start_date_message

--print @message

SELECT @message
END
