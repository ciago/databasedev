USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_contracts_get_row]    Script Date: 5/12/2021 5:47:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER         PROCEDURE [dbo].[mgn_contracts_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from contacts
--exec [mgn_contracts_get_row] @row = '159759df-a2aa-4a86-8785-de6b2f7ce35f'
   
SELECT			c._key as [_key],
				c.ctr_contract_number as [contract_number],
				c.ctr_entity as [entity],
				isnull(c.ctr_description,'') as [description],
				c.ctr_contract_status as [status],
				c.ctr_contract_type as [type],
				c.ctr_start_date as [start_date],
				c.ctr_end_date as [end_date],
				isnull(c.ctr_comments,'') as [comments],
				c.ctr_site as [site]

FROM			contracts as c

WHERE			@row = c._key
	  
END
