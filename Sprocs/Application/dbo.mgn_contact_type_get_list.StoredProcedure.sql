--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_contact_type_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/24/2021
-- Description:		Select list of contact types for dropdown usage.

/*EXEC mgn_contact_type_get_list

	*/
-- =============================================

CREATE OR ALTER        PROCEDURE [dbo].[mgn_contact_type_get_list] 
		@site_key uniqueidentifier = null, 
		@user varchar(50) = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

IF @site_key IS NULL 
	SET @site_key = dbo.get_default_site();   -- Needs to change to Hartree site GUID

SELECT '00000000-0000-0000-0000-000000000000' as [key], ' Select Contact Type' as [name]
UNION

SELECT			ct._key as [key],
				ct.contact_type_description
			  
FROM			contact_types ct

WHERE			ct.site_key = @site_key

ORDER BY name
    
END
	
GO
