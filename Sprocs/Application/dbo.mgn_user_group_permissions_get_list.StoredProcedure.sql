--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_user_group_permissions_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 4/29/21
-- Description:	

/*

EXEC mgn_user_group_permissions_get_list  @group_key  = '32b8bf8c-5a3a-4643-a2e5-03a85f961e5c'

*/
-- =============================================
CREATE    OR ALTER    PROCEDURE [dbo].[mgn_user_group_permissions_get_list]
	@group_key uniqueidentifier = NULL
AS
BEGIN

SET NOCOUNT ON;


SELECT ugp._key as _key,
	   ug.user_group_name as group_name,
	   mi.menu_label_name as menu_name, 
	   mi.menu_is_parent,
	   mi.menu_is_child,
	   ugp.gp_view_permission, 
	   ugp.gp_update_permission,
	   ugp.gp_delete_inactivate_permisssion,
	   ugp.gp_special_permission, 
	   mi.menu_parent_number_1
FROM user_groups_permissions ugp INNER JOIN  
	 menu_items mi  ON mi._key = ugp.gp_menu_item_key  INNER JOIN
	 user_groups ug ON ug._key = ugp.gp_user_group_key  INNER JOIN 
	 menu_items parent ON parent.menu_number = mi.menu_parent_number_1
WHERE mi.menu_active = 1 
      AND ug._key = @group_key
	  AND mi.menu_parent_number_1 <> 1
ORDER BY parent.menu_sort, mi.menu_sort 
	
END
GO
