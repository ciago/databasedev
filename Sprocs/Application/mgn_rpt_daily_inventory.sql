--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_rpt_daily_inventory]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:       Candice Iago   
-- Create date: 5/4/2020
-- Description:    Gets a list of tank actuals from a selected day.

/*EXEC mgn_rpt_daily_inventory  
	@date  = '5/2/2021'
	,@site = NULL

	@date  = '5/31/2020'
    */
	
-- =============================================
CREATE  OR ALTER      PROCEDURE [dbo].mgn_rpt_daily_inventory

    @site uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 
	@date datetime = null

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from tank_actuals
DECLARE @eod_time int = (SELECT TOP(1) eod_time FROM site_configuration)
SET @date = DATEADD(HOUR, @eod_time, @date) 
DECLARE @eod datetime = dbo.get_eod_datetime(@date)
--print @eod
IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END

SELECT        ta._key,
			  ta.tank_key as [tank_key],
			  t.tank_name as [tank_name],
              DATEADD(DAY, -1, ta.eod_datetime) as [date_datetime],
			  ROUND(ISNULL(ta.close_gsv,0), 2) as [close_gsv],
			  ta.close_nsv as close_nsv,
			  ISNULL(p.product_name, 'No Product Assigned') as [tank_product],
			  ISNULL(ta.tank_api, 0) as [tank_api],
			  ISNULL(ta.tank_temp, 0) as [tank_temp],
			  ta.eod_datetime, 
			  ta.tank_level, 
			  t.tank_capacity, 
			  CASE WHEN t.tank_capacity <> 0 THEN ROUND(ta.close_gsv / t.tank_capacity,4) ELSE 0 END as percent_full

FROM          tank_actuals AS ta INNER JOIN
              sites AS s ON s._key = ta.site_key INNER JOIN
              tanks AS t ON ta.tank_key = t._key LEFT OUTER JOIN
			  (SELECT * FROM tank_products WHERE (tk_start_datetime <= @eod)
			  AND (tk_end_datetime >= @eod or tk_end_datetime IS null)) AS tp ON ta.tank_key = tp.tank_key LEFT OUTER JOIN
			  products AS p ON tp.product_key = p._key
			  
WHERE		  s._key = @site
	          AND eod_datetime = @eod
	          AND t.active = 1

ORDER BY eod_datetime
		  
END
GO
