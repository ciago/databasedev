--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_volume_accounts_add_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Matthew Carlin
-- Create date: 12/11/2020
-- Description:	Obtains a single row of volume accounts data by searching for a specified guid.

/*EXEC mgn_volume_accounts_add_row
	@account_name = 'test',
	@account_units 'test',
	@volume_account_type = '6fdc5c66-6dcc-4aeb-b68c-f020e40da497',
	@entity = 'd25051f3-4888-4bfb-89fd-4e1449ac0fba',
	@vessel_type = '53d8a499-0a47-4d9e-b327-37de6d547af8',
	@gl_code = 'test',
	@site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@user = 'Candice'
	*/
-- =============================================
Create      OR ALTER   PROCEDURE [dbo].[mgn_volume_accounts_add_row]
	@account_name nvarchar(50),
	@account_units nvarchar(50),
	@volume_account_type uniqueidentifier,
	@entity uniqueidentifier,
	@vessel_type uniqueidentifier,
	@gl_code nvarchar(50),
	@site uniqueidentifier,
	@user nvarchar

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from 
--WHere _key = @row
 
Insert Into [volume_accounts] ([accounts],
							   [unit_description],
							   [accounting_volume_types_key],
							   [entity_key],
							   [vessel_types_key],
							   [gl_account],
							   [site],
							   [record_info])

Values (@account_name,
		@account_units,
		@volume_account_type,
		@entity,
		@vessel_type,
		@gl_code,
		@site,
		dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE()))
	  
END
GO
