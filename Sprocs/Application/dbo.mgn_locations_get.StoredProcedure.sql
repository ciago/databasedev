--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_locations_get]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  OR ALTER  PROCEDURE [dbo].[mgn_locations_get]
    @site uniqueidentifier = null, 
    @user varchar(50) = 'default', 
    @machine varchar(50) = 'default', 
    @location_type int,
    @active bit = null

AS
BEGIN
/* 
EXEC [mgn_locations_get]  @location_type = NULL, @active = 1 
*/
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
--Select * from pipeline_allocations
--WHere _key = @row

-- set site if null
IF(@site IS NULL) 
BEGIN 
    SET @site = dbo.get_default_site();
END

 

SELECT ln._key,
	   ln.ln_location_type as [location_type],
	   ln.ln_location_name as [location_name],
	   lt.[lt_type_desc] as [type_description],
	   ISNULL(e1.[short_name],'') as [entity1],
	   ISNULL(et1.type_description,'') as [entity_type1],
	   ISNULL(e2.[short_name],'') as [entity2],
	   ISNULL(et2.type_description,'') as [entity_type2],
	   ISNULL(e3.[short_name],'') as [entity3],
	   ISNULL(et3.type_description,'') as [entity_type3],
	   ISNULL(p.product_name, '') as product_name

FROM locations as ln left outer join
	 location_types as lt on ln.ln_location_type = lt.lt_location_type left outer join
	 entities e1 ON e1._key = ln.ln_e1_key LEFT OUTER JOIN
	 entities e2 ON e2._key = ln.ln_e2_key LEFT OUTER JOIN
	 entities e3 ON e3._key = ln.ln_e3_key left outer join
	 entity_types et1 on lt.lt_e1_type = et1.type_code left outer join
	 entity_types et2 on lt.lt_e2_type = et2.type_code left outer join
	 entity_types et3 on lt.lt_e3_type = et3.type_code LEFT OUTER JOIN 
	 products p ON ln.ln_product_key = p._key

WHERE ln.ln_site_key = @site and ln.ln_active = @active
		and (ln.ln_location_type = @location_type OR @location_type IS NULL)

ORDER BY ln.ln_location_name

END
GO
