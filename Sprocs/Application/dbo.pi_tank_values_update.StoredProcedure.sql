--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[pi_tank_values_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER    PROCEDURE [dbo].[pi_tank_values_update]

AS
/* 
EXEC pi_tank_values_update 
	@start = '4/5/2020 08:03',
	@end = '4/7/2020 08:03',
	@timestep = '1m'
*/
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--EXECUTE AS USER = 'MODAINGLESIDE\Candice.Iago'


DECLARE	@start datetime =  DATEADD(day,-3 , getdate()), 
	@end datetime = getdate(), 
	@timestep nvarchar(20) = '1m'
	--EXECUTE AS USER = 'MODAINGLESIDE\Candice.Iago'

-- convert dates to nvarchar
DECLARE @start2 nvarchar(20) = CONVERT(nvarchar(20), @start, 120),
	    @end2 nvarchar(20) = CONVERT(nvarchar(20), @end, 120)


-- sample insert table(this will be the final real table)
DECLARE @tank_values TABLE (
        tank_name nVARCHAR(100),
        [timestamp] DATETIME,
        tank_level float,
		tank_nsv float, 
		tank_gsv float,
		tank_tov float,
		_key uniqueidentifier, 
		rounded_timestamp datetime)

DECLARE @query nvarchar(max) 
DECLARE @name nvarchar(10) = 'Name'

SET @query = 'SELECT * FROM OPENQUERY([MODAPI],''
SELECT e.Name, sv.TimeStamp, sv.TM_Level, sv.NSV, sv.GSV, sv.TOV
FROM Master.Element.Element e
CROSS APPLY Master.Element.GetSampledValues
<
    ''''Tank'''', --Template - element template name
    {
        ''''|TM_Level'''', -- AttributeTemplatePath - Required
        ''''TM_Level'''', -- ValueColumnName - Required
        ''''TM_Level_UnitOfMeasure'''', -- UnitOfMeasureColumnName - Optional
        ''''TM_Level_Error'''', -- ErrorColumnName - Optional
        NULL -- UnitOfMeasure - Optional
    } -- AttributeMetadata - defines which columns to expose for a particular attribute template
 ,{
        ''''|NSV'''', -- AttributeTemplatePath - Required
        ''''NSV'''', -- ValueColumnName - Required
        ''''NSV_UnitOfMeasure'''', -- UnitOfMeasureColumnName - Optional
        ''''NSV_Error'''', -- ErrorColumnName - Optional
        NULL -- UnitOfMeasure - Optional
  } -- AttributeMetadata
 ,{
        ''''|GSV'''', -- AttributeTemplatePath - Required
        ''''GSV'''', -- ValueColumnName - Required
        ''''GSV_UnitOfMeasure'''', -- UnitOfMeasureColumnName - Optional
        ''''GSV_Error'''', -- ErrorColumnName - Optional
        NULL -- UnitOfMeasure - Optional
  } -- AttributeMetadata
 ,{
        ''''|TOV'''', -- AttributeTemplatePath - Required
        ''''TOV'''', -- ValueColumnName - Required
        ''''TOV_UnitOfMeasure'''', -- UnitOfMeasureColumnName - Optional
        ''''TOV_Error'''', -- ErrorColumnName - Optional
        NULL -- UnitOfMeasure - Optional
  } -- AttributeMetadata
>(e.ID,
	''''' + @start2+''''',  --Start Time
	'''''+@end2+''''',  --End Time
	'''''+@timestep+'''''  --Time Step
	) sv
WHERE e.Template = ''''Tank''''
'')'

--print @query
-- select * from @tank_values

INSERT INTO @tank_values(tank_name, [timestamp],tank_level, tank_nsv, tank_gsv, tank_tov)
EXEC sp_executeSql  @query
--select * from @tank_values

UPDATE @tank_values
SET _key = newid()


UPDATE @tank_values
SET rounded_timestamp = dateadd(mi, datediff(mi, 0, [timestamp]), 0)



	   UPDATE tr
	   SET tr.tank_level = tv.tank_level,
		   tr.tank_nsv = tv.tank_nsv, 
		   tr.tank_gsv = tv.tank_gsv,
		   tr.tank_tov = tv.tank_tov
		  --select *
FROM @tank_values tv LEFT OUTER JOIN 
	 tanks t ON t.tank_pi_name = tv.tank_name  LEFT OUTER JOIN 
	 tank_readings tr ON tr.tank_key = t._key AND tr.tank_timestamp = tv.rounded_timestamp
WHERE tr.tank_gsv IS NULL and tv.tank_gsv IS NOT NULL  AND tr._key IS NOT NULL --AND m._key = '4956D4EB-5884-4158-8B69-245ACC1BC0C4'

--INSERT INTO [dbo].tank_readings
--           ([_key]
--           ,tank_key
--           ,[tank_timestamp]
--		   ,tank_level
--		   ,tank_nsv
--		   ,tank_gsv
--		   ,tank_tov
--		   ,tank_external_id)
--SELECT tv._key, 
--	   t._key,
--	   tv.timestamp, 
--	   tv.tank_level, 
--	   tv.tank_nsv, 
--	   tv.tank_gsv,
--	   tv.tank_tov,
--	   t.tank_external_id_inventory
--FROM @tank_values tv LEFT OUTER JOIN 
--	 tanks t ON t.tank_pi_name = tv.tank_name 


--INSERT INTO EMERSON.Moda_Interface.[dbo].[tank_readings]
--           ([_key]
--           ,tank_key
--           ,[tank_timestamp]
--		   ,tank_level
--		   ,tank_nsv
--		   ,tank_gsv
--		   ,tank_tov
--		   ,tank_external_id
--		   ,site_code)
--SELECT tv._key, 
--	   t._key,
--	   tv.timestamp, 
--	   tv.tank_level, 
--	   tv.tank_nsv, 
--	   tv.tank_gsv,
--	   tv.tank_tov,
--	   t.tank_external_id_inventory,
--	   100
--FROM @tank_values tv LEFT OUTER JOIN 
--	 tanks t ON t.tank_pi_name = tv.tank_name 
--WHERE CONVERT(time, tv.timestamp) = '07:00'





 --SELECT * FROM @tank_values
END
GO
