--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_invoices_delete]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 11/23/2020
-- Description:	Deletes a single row of invoices data based on the selected row/record.

/*EXEC mgn_invoices_delete
	@row  = '12B7B9E3-E75B-4570-AD36-B4A876B5DB75'

	deleted row data reference -
	row	12B7B9E3-E75B-4570-AD36-B4A876B5DB75
	*/
-- =============================================
CREATE   OR ALTER    PROCEDURE [dbo].[mgn_invoices_delete]
	@row uniqueidentifier = null,
	@user varchar(50) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from invoice_charge
--WHere _key = @row

Delete From invoice_charge

Where _key = @row

END
GO
