
/****** Object:  StoredProcedure [dbo].[mgn_accounting_vessel_types_get_dropdown]    Script Date: 5/11/2021 2:53:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
create or ALTER             PROCEDURE [dbo].[mgn_contract_types_get_dropdown] 
	@site_key uniqueidentifier = null, 
	@user varchar(50) = 'default'

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


SELECT '00000000-0000-0000-0000-000000000000' AS RowKey, 'Select Type' As description
UNION
SELECT _key as RowKey, [ct_description] as description
FROM [dbo].[contract_types]
WHERE ct_active = 1
ORDER BY RowKey

END


-- EXEC [mgn_accounting_volume_types_get_list]





