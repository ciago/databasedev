--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tickets_general_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    OR ALTER   PROCEDURE [dbo].[mgn_tickets_general_update]

	@_key uniqueidentifier,
    @ticket_type int, 
	@status int = 1,
	@product uniqueidentifier,
	@batch_id nvarchar(50) = '',
	@sched_start_date datetime = null,
	@sched_end_date datetime = null,
	@start_date datetime = null,
	@end_date datetime = null,
	@nomination_number bigint = null,
	@scheduled_volume float = 0,
	@volume_uom int = null,
	@final_gsv float = null,
	@final_nsv float = null,
	@source_tank_key uniqueidentifier = null,
	@destination_tank_key uniqueidentifier = null,
	@berth_key uniqueidentifier = null,
	@pipeline_key uniqueidentifier = null,
	@vehicle_key uniqueidentifier = null,
	@vessel_key uniqueidentifier = null,
	@truck_lane_key uniqueidentifier = null,
	@entity1_key uniqueidentifier = null,
	@entity2_key uniqueidentifier = null,
	@entity3_key uniqueidentifier = null,
	@entity4_key uniqueidentifier = null,
	@entity5_key uniqueidentifier = null,
	@entity6_key uniqueidentifier = null,
	@entity7_key uniqueidentifier = null,
	@site_key uniqueidentifier,
	@deleted bit = 0,
	@user nvarchar(50),
	@final_update bit = 0,
	@void bit = 0,
	@driver nvarchar(100) = '', 
	@lease  nvarchar(100) = '', 
	@truck  nvarchar(100) = '', 
	@trailer  nvarchar(100) = ''
	
	
	/*EXEC [mgn_tickets_general_update] 
	@_key = '1f61fc3f-d394-408d-9124-930b30035d83',
	@ticket_type = 2,
	@product = '00000000-0000-0000-0000-000000000000',
	@user = 'candice',
	@site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@void = 1,
	@final_update = 1
	
	// 
	*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

if(@void = 1)
Begin
	Set @status = (Select stat_type from ticket_status where stat_description = 'Void')
end

DECLARE @ticket_misc_key uniqueidentifier = (SELECT _key from tickets_misc WHERE tm_ticket_key = @_key)
IF (@ticket_misc_key IS NULL) 
BEGIN 
	INSERT INTO [dbo].[tickets_misc]
           ([_key]
           ,[tm_ticket_key]
           ,[tm_truck_name]
           ,[tm_trailer_name]
           ,[tm_lease]
           ,[tm_driver_name])
     VALUES
           (newid()
           ,@_key
           ,@truck
           ,@trailer
           ,@lease
           ,@driver)

END
ELSE 
BEGIN 
	UPDATE [tickets_misc]
	SET [tm_truck_name] = @truck
           ,[tm_trailer_name] = @trailer
           ,[tm_lease] = @lease
           ,[tm_driver_name] = @driver
	WHERE _key = @ticket_misc_key
END 


IF(@_key IN (SELECT t._key from tickets as t))
BEGIN
-- it exists already so update it 

	--record info update 
	DECLARE @json varchar(max)
	SET @json = (SELECT t.tk_record_info FROM tickets as t WHERE _key = @_key)
	SET @json = dbo.fn_json_record_info(@json, 'updated', @user, HOST_NAME(), GETDATE())
	
UPDATE tickets

SET		[tk_type] = @ticket_type,		
		[tk_product] = @product,
		[tk_batch_id] = @batch_id,
		[tk_scheduled_start_datetime] = @sched_start_date,
		[tk_scheduled_end_datetime] = @sched_end_date,	
		[tk_start_datetime] = @sched_start_date,
	    [tk_end_datetime] = @sched_end_date,	
		[tk_scheduled_volume] = @scheduled_volume,
		[tk_volume_uom] = @volume_uom,
		[tk_source_tank_key] = @source_tank_key,
		[tk_destination_tank_key] = @destination_tank_key,
		[tk_berth_key] = @berth_key,
		[tk_pipeline_key] = @pipeline_key,
		[tk_vehicle_key] = @vehicle_key,
		[tk_vessel_key] = @vessel_key,
		[tk_truck_lane_key] = @truck_lane_key,
		[tk_entity1_key] = @entity1_key,
		[tk_entity2_key] = @entity2_key,
		[tk_entity3_key] = @entity3_key,
		[tk_entity4_key] = @entity4_key,
		[tk_entity5_key] = @entity5_key,
		[tk_entity6_key] = @entity6_key,
		[tk_entity7_key] = @entity7_key,
		[tk_eod_datetime] = dbo.get_eod_datetime(@sched_start_date),
		[tk_site_key] = @site_key,
		[tk_deleted] = @deleted,
		tk_record_info = @json

WHERE _key = @_key

	IF(@final_update <> 0)
	BEGIN

	Update tickets

	SET 	[tk_status] = @status,
			[tk_nomination_number] = @nomination_number,
			[tk_start_datetime] = @start_date,
			[tk_end_datetime] = @end_date,
			[tk_eod_datetime] = dbo.get_eod_datetime(@sched_start_date),
			[tk_final_gsv] = @final_gsv,
			[tk_final_nsv] = @final_nsv

	WHERE _key = @_key

	END
END
ELSE
BEGIN
-- it does not exist so insert a new row 

	INSERT INTO [dbo].[tickets]
           ([_key]
           ,[tk_type]
		   ,[tk_status]
		   ,[tk_product]
		   ,[tk_batch_id]
		   ,[tk_scheduled_start_datetime]
		   ,[tk_scheduled_end_datetime]
		   ,[tk_start_datetime]
		   ,[tk_end_datetime]
		   ,[tk_scheduled_volume]
		   ,[tk_volume_uom]
		   ,[tk_source_tank_key]
		   ,[tk_destination_tank_key]
		   ,[tk_berth_key]
		   ,[tk_pipeline_key]
		   ,[tk_vehicle_key]
		   ,[tk_vessel_key]
		   ,[tk_truck_lane_key]
		   ,[tk_entity1_key]
		   ,[tk_entity2_key]
		   ,[tk_entity3_key]
		   ,[tk_entity4_key]
		   ,[tk_entity5_key]
		   ,[tk_entity6_key]
		   ,[tk_entity7_key]
		   ,[tk_site_key]
		   ,[tk_deleted]
		   ,[tk_eod_datetime]
		   ,[tk_record_info])
     VALUES
           (@_key
		   ,@ticket_type
		   ,@status
		   ,@product
		   ,@batch_id
		   ,@sched_start_date
		   ,@sched_end_date
		   ,@sched_start_date
		   ,@sched_end_date
		   ,@scheduled_volume
		   ,@volume_uom
		   ,@source_tank_key
		   ,@destination_tank_key
		   ,@berth_key
		   ,@pipeline_key
		   ,@vehicle_key
		   ,@vessel_key
		   ,@truck_lane_key
		   ,@entity1_key
		   ,@entity2_key
		   ,@entity3_key
		   ,@entity4_key
		   ,@entity5_key
		   ,@entity6_key
		   ,@entity7_key
		   ,@site_key
		   ,@deleted
		   ,dbo.get_eod_datetime(@sched_start_date)
           ,dbo.fn_json_record_info(NULL, 'created', @user, HOST_NAME(), GETDATE()))

		   	IF(@final_update <> 0)
			BEGIN

			INSERT INTO [dbo].[tickets]
						([tk_status]
						,[tk_start_datetime]
						,[tk_end_datetime]
						,tk_eod_datetime
						,[tk_nomination_number]
						,[tk_final_gsv]
						,[tk_final_nsv])
			VALUES
						(@status
						,@start_date
						,@end_date
						,dbo.get_eod_datetime(@start_date)
						,@nomination_number
						,@final_gsv
						,@final_nsv)		

			END



END

END
GO
