--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_valve_activity_delete]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 2/12/2021
-- Description:	Deletes a single row of valve activity data based on the selected row/record.

/*EXEC mgn_valve_activity_delete
	@row  = '7c0a375d-fea9-460d-a046-a2a51ca98034'
	*/
-- =============================================
CREATE  OR ALTER  PROCEDURE [dbo].[mgn_valve_activity_delete]
	@row uniqueidentifier = null,
	@user varchar(50) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from valve_activity
--WHere _key = @row
Delete From valve_activity
Where _key = @row

END
GO
