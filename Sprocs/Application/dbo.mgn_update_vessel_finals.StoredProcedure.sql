--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_update_vessel_finals]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   OR ALTER  PROCEDURE [dbo].[mgn_update_vessel_finals] 
AS
BEGIN

DECLARE @EmersonVesselOrders TABLE(customer_ref nvarchar(50), tank_location_id nvarchar(30), gsv float, nsv float, ticket_id nvarchar(64))


INSERT INTO @EmersonVesselOrders (customer_ref , tank_location_id , gsv , nsv , ticket_id)
SELECT --mov.CUSTOMER_ORDER_NUMBER,
	mov.CUSTOMER_REF,	
--lk.ITEM_VALUE as [status], 
--tah.TERMINAL_ACTIVITY_HEADER_ID,
	loc.LOCATION_CODE as Tank_location_id, 
	et.TICKET_GSV_AMT, 
	et.NET_VOL , 
	et.LS_TICKET_ID
	--newtic.TICKET_REV_NUM
FROM 
    EMERSON.Synthesis_Test_02242020.dbo.VESSEL_TRIP vt LEFT JOIN 
    EMERSON.Synthesis_Test_02242020.dbo.VESSEL_LAY lay ON lay.VESSEL_TRIP_ID = vt.VESSEL_TRIP_ID JOIN 
    EMERSON.Synthesis_Test_02242020.dbo.VESSEL_MOVEMENT mov ON mov.VESSEL_LAY_ID = lay.VESSEL_LAY_ID  LEFT OUTER JOIN 
	EMERSON.Synthesis_Test_02242020.dbo.OPENSEAL_LKUP_ITEM lk ON lk.ITEM_KEY = mov.VESSEL_MOVEMENT_STATUS LEFT OUTER JOIN 
	EMERSON.Synthesis_Test_02242020.dbo.TERMINAL_ACTIVITY_HEADER tah ON tah.ORDER_ID = mov.CUSTOMER_ORDER_NUMBER LEFT OUTER JOIN 
	(SELECT LS_TICKET_ID,HEADER_ID,  MAX(TICKET_REV_NUM) as TICKET_REV_NUM FROM EMERSON.Synthesis_Test_02242020.dbo.TICKET WHERE MEASUREMENT_TICKET_TYPE_ID = 1003 GROUP BY LS_TICKET_ID, HEADER_ID) newtic ON newtic.HEADER_ID =  tah.TERMINAL_ACTIVITY_HEADER_ID LEFT OUTER JOIN 
	EMERSON.Synthesis_Test_02242020.dbo.TICKET et ON et.HEADER_ID = tah.TERMINAL_ACTIVITY_HEADER_ID AND newtic.TICKET_REV_NUM = et.TICKET_REV_NUM  LEFT OUTER JOIN 
	EMERSON.Synthesis_Test_02242020.dbo.PORT_MOVEMENT_DEVICE_ASSIGN pmda ON pmda.VESSEL_MOVEMENT_ID = mov.VESSEL_MOVEMENT_ID LEFT JOIN 
	EMERSON.Synthesis_Test_02242020.dbo.MASTER_DEVICE md ON md.MASTER_DEVICE_ID = pmda.DEVICE_ID AND md.MASTER_DEVICE_TYPE_ID = 4  LEFT JOIN 
    EMERSON.Synthesis_Test_02242020.dbo.TANK t ON t.MASTER_DEVICE_ID = md.MASTER_DEVICE_ID  LEFT OUTER  JOIN 
	(SELECT * FROM OPENQUERY(EMERSON, 'select * FROM Synthesis_Test_02242020.dbo.MASTER_LOCATION')) loc ON md.MASTER_LOCATION_ID = loc.MASTER_LOCATION_ID
	
WHERE lk.LKUP_ID = 1029
	--AND mov.CUSTOMER_REF = 'Corossol062020'
	AND  et.MASTER_DEVICE_ID = md.MASTER_DEVICE_ID
	AND lk.ITEM_VALUE = 'CLOSED'
GROUP BY mov.CUSTOMER_ORDER_NUMBER,
	mov.CUSTOMER_REF,	
	lk.ITEM_VALUE ,
	tah.TERMINAL_ACTIVITY_HEADER_ID,
	loc.LOCATION_CODE, 
	et.TICKET_GSV_AMT, 
	et.NET_VOL , 
	newtic.TICKET_REV_NUM, 
	et.LS_TICKET_ID

	--(SELECT LS_TICKET_ID, TICKET_REV_NUM FROM TICKET WHERE MEASUREMENT_TICKET_TYPE_ID = 1003 GROUP BY LS_TICKET_ID) newtic

	DECLARE @ManganTickets TABLE (_key uniqueidentifier, tank nvarchar(30), batch_id nvarchar(50), 
								  eod_datetime datetime, final_gsv float, final_nsv float, total_vessel_gsv float, 
								  total_vessel_nsv float, emerson_gsv float, emerson_nsv float, new_gsv float, 
								  new_nsv float, last_date datetime, previous_days_gsv float, previous_day_nsv float, closed bit, ticket_id nvarchar(64))
	INSERT INTO @ManganTickets (_key , tank , batch_id , eod_datetime , final_gsv , final_nsv, closed )
	SELECT t._key, ta.tank_external_id_location, t.tk_batch_id, t.tk_eod_datetime, t.tk_final_gsv, t.tk_final_nsv, 0
	FROM tickets t INNER JOIN 
	tanks ta ON t.tk_from_key = ta._key
	WHERE t.tk_type = 2 AND t.tk_status = 4

	
	UPDATE  mt 
	SET total_vessel_gsv = smt.final_gsv, 
		total_vessel_nsv =  smt.final_nsv
	FROM @ManganTickets mt INNER JOIN 
	(SELECT SUM(final_gsv) as final_gsv, SUM(final_nsv) as final_nsv, batch_id, tank FROM @ManganTickets  GROUP BY batch_id, tank)smt ON mt.tank = smt.tank AND mt.batch_id = smt.batch_id
	

	--SELECT mt.batch_id, mt.tank, final_gsv , final_nsv, e.gsv, e.nsv
	UPDATE mt
	SET emerson_gsv = e.gsv,
		emerson_nsv = e.nsv, 
		closed = 1,
		ticket_id = e.ticket_id
	FROM @ManganTickets mt INNER JOIN 
		 @EmersonVesselOrders e ON mt.batch_id = e.customer_ref AND mt.tank = e.tank_location_id
		 
 UPDATE  mt 
	SET last_date = smt.last_date
	FROM @ManganTickets mt INNER JOIN 
(SELECT MAX(eod_datetime) as last_date, batch_id, tank 
FROM @ManganTickets  
GROUP BY batch_id, tank)smt ON mt.tank = smt.tank AND mt.batch_id = smt.batch_id

UPDATE mt 
SET final_nsv = final_gsv
FROM @ManganTickets mt 
WHERE closed = 1


	UPDATE  mt 
	SET previous_days_gsv = smt.final_gsv, 
		previous_day_nsv =  smt.final_nsv
	FROM @ManganTickets mt INNER JOIN 
	(SELECT SUM(ISNULL(final_gsv,0)) as final_gsv, SUM(ISNULL(final_nsv,0)) as final_nsv, batch_id, tank 
	 FROM @ManganTickets 
	 WHERE eod_datetime <> last_date 
	 GROUP BY batch_id, tank)smt ON mt.tank = smt.tank AND mt.batch_id = smt.batch_id
	--WHERE eod_datetime <> last_date

	UPDATE mt 
	SET new_gsv = emerson_gsv - previous_days_gsv, 
		new_nsv = emerson_nsv - previous_day_nsv
	FROm @ManganTickets mt 
	WHERE eod_datetime = last_date

	
	--UPDATE mt 
	--SET new_nsv = ISNULL(total_vessel_nsv,0) - ISNULL(previous_day_nsv,0)
	--FROm @ManganTickets mt 
	--WHERE eod_datetime = last_date

	UPDATE t
	SET tk_final_gsv = new_gsv
	--,
		--tk_ticket_id = ticket_id
	--SELECT t._key, t.tk_batch_id ,tk_final_gsv, tk_final_nsv, mt.emerson_gsv, mt.emerson_nsv, mt.new_gsv, mt.new_nsv 
	FROM tickets t INNER JOIN 
		 @ManganTickets mt ON t._key = mt._key
	WHERE closed = 1 
	AND ROUND( tk_final_gsv ,2) <> new_gsv
	AND new_gsv IS NOT NULL
    AND eod_datetime > DATEADD(MONTH, -2, getdate())

	UPDATE t
	SET tk_final_nsv = new_nsv,
		tk_status = 6
	--SELECT t._key, t.tk_batch_id ,tk_final_gsv, tk_final_nsv, mt.emerson_gsv, mt.emerson_nsv, mt.new_gsv, mt.new_nsv 
	FROM tickets t INNER JOIN 
		 @ManganTickets mt ON t._key = mt._key
	WHERE closed = 1 
	AND (ROUND( tk_final_nsv ,2) <> new_nsv or tk_final_nsv IS NULL)
	AND new_nsv IS NOT NULL
    AND eod_datetime > DATEADD(MONTH, -2, getdate())


--AND eod_datetime > DATEADD(MONTH, -2, getdate())

--	SELECT * from @ManganTickets 
--	WHERE closed = 1 
--	AND ROUND( final_gsv ,2) <> new_gsv
--	AND new_gsv IS NOT NULL
----AND eod_datetime > DATEADD(MONTH, -2, getdate())
--		  --and emerson_nsv IS NULL
--	--WHERE batch_id = 'MINERVA ZOBIA'
--	ORDER BY batch_id

END
GO
