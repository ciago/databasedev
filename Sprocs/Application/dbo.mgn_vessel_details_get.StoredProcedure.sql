--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_vessel_details_get]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================

/*EXEC mgn_vessel_details_get
@row = 'f1d7731a-117d-4377-b06a-377f0284e671'
 
	*/

CREATE OR ALTER   PROCEDURE [dbo].[mgn_vessel_details_get] 

@row uniqueidentifier



AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from addresses

SELECT        vi._key as [_key],
			  st.syn_vessel_name as [vessel_name],
			  ISNULL(CONVERT(varchar, st.syn_ticket_start_datetime, 22), '') as [arrived_date],
			  ISNULL(CONVERT(varchar, st.syn_ticket_end_datetime, 22), '') as [departed_date],
			  CONVERT(decimal(18,2) ,(DATEDIFF(MINUTE, st.syn_ticket_end_datetime, st.syn_ticket_start_datetime)/60)) as [total_berth_time],
			  CONVERT(decimal(18,2) ,(DATEDIFF(MINUTE, st.syn_ticket_end_datetime, st.syn_ticket_start_datetime)/60/24)) as [total_days_at_berth],
			  DATEDIFF(MINUTE, st.syn_ticket_end_datetime, st.syn_ticket_start_datetime)/60/24 as [total_days_charged]
			  
			  
FROM		  vessel_invoice vi INNER JOIN 
			  synthesis_tickets AS st ON vi.vi_synthesis_ticket_key = st._key
			  
WHERE		  @row = vi._key
    
END
GO
