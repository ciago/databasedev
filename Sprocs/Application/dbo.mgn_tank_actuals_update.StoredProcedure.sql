--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tank_actuals_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Update selected tank_actuals with new user input details.

/*EXEC mgn_tank_actuals_update 
	@row = 'F6DA37F0-689D-47A9-84AD-B4EAC7CC7889',
	@tank_gsv_change = ,
	@open_gsv = 145797.515625,
	@close_gsv = 330912.84375,
	@tank_api = 12.3,
	@tank_temp = 123


	OUTDATED - 3/22/2021

	@gsv_bbls = 185115.328125,
	@open_totalizer = 145797.515625,
	@close_totalizer = 330912.84375

	185115.328125 
	145797.515625
	330912.84375
	
	// 
	*/
-- =============================================
CREATE    OR ALTER   PROCEDURE [dbo].[mgn_tank_actuals_update]

	@row uniqueidentifier,
    @site_key uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd', 
	@tank_gsv_change float,
	@open_gsv float,
	@close_gsv float,
	@tank_api float,
	@tank_temp float
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from tank_actuals
--Where _key = @row


UPDATE tank_actuals

	SET --tank_gsv_change = @tank_gsv_change,
		tank_gsv_change = @close_gsv - @open_gsv,
		open_gsv = @open_gsv,
		close_gsv = @close_gsv,
		tank_api = @tank_api,
		tank_temp = @tank_temp
		
WHERE _key = @row

--UPDATE tank_actuals
--SET tank_gsv_change = close_gsv - open_gsv
--WHERE _key = @row

END
GO
