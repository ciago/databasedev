--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_product_get_row]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/26/2021
-- Description:		Obtains a single row of product recipes data from a specified guid.

/*EXEC mgn_product_get_row
	@row  = '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0'

	*/
-- =============================================
CREATE    OR ALTER  PROCEDURE [dbo].[mgn_product_get_row]
	@row uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Select * from products
--WHere _key = @row
   
SELECT			p._key,
				ISNULL(p.product_name, '') as [product_name],
				ISNULL(p.product_class_key, '00000000-0000-0000-0000-000000000000') as [product_class_key],
				ISNULL(pc.product_class, '') as [product_class],
				ISNULL(p.product_api,0) as [product_api],
				ISNULL(p.safe_operating_temperature,0) as [safe_operating_temp],
				ISNULL(p.product_external_id, '') as[product_external_id],
				ISNULL(p.product_code, '') as [product_code],
				ISNULL(p.product_irs_code, 'XXX') as product_irs_code,
				p.site_key as [site_key],
				p.active as [active]
FROM			products AS p LEFT OUTER JOIN
				product_classes AS pc ON pc._key = p.product_class_key
WHERE			@row = p._key
	  
END
GO
