--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_user_group_permissions_sync]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Candice Iago	
-- Create date: 2/26/2020
-- Description:	Gets the accounting_volume_types table

/*EXEC [mgn_user_group_permissions_sync]
	*/
-- =============================================
CREATE     OR ALTER  PROCEDURE [dbo].[mgn_user_group_permissions_sync]
	
AS
BEGIN

-- UPDATE GROUP PERMISSIONS 

-- insert any new combination of records 
insert into user_groups_permissions(_key, 
									gp_user_group_key, 
									gp_menu_item_key, 
									gp_view_permission, 
									gp_update_permission, 
									gp_delete_inactivate_permisssion, 
									gp_special_permission)
SELECT newid(), 
	   ug._key as group_key, 
	   mi._key as menu_key, 
	   1,
	   1,
	   1,
	   1
	   --mi.menu_action_result_name, 
	   --mi.menu_is_parent, 
	   --mi.menu_is_child, 
	   --mi.menu_label_name
FROM menu_items mi CROSS JOIN 
     user_groups ug
WHERE NOT EXISTS
		(SELECT ugp.gp_user_group_key, ugp.gp_menu_item_key
		 FROM user_groups_permissions ugp
		 WHERE ugp.gp_user_group_key = ug._key
			   AND ugp.gp_menu_item_key = mi._key)


END 
GO
