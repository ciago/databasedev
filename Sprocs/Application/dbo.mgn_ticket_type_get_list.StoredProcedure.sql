--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_ticket_type_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   OR ALTER    PROCEDURE [dbo].[mgn_ticket_type_get_list] 
	@site uniqueidentifier = null, 
	@user varchar(50) = 'default'

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;


SELECT 0 as type , 'Select Ticket Type' as description
UNION
SELECT typ_type  as type , type_description  as description
FROM ticket_type
WHERE type_active = 1
    
END

GO
