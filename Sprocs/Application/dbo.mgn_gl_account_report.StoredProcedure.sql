--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_gl_account_report]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  OR ALTER   PROCEDURE [dbo].[mgn_gl_account_report]

	
AS
/* 
EXEC daily_berth_activity_update  @eod = DATETIMEFROMPARTS( 2020, 05, 29, 7, 0, 0, 0)
	
*/
BEGIN


SELECT ct.CHARGE_TYPE_ID, ct.DESCRIPTION, lg.ACCOUNT_CODE 
FROM EMERSON.Synthesis_Test_02242020.[dbo].CHARGE_TYPE ct left outer join 

EMERSON.Synthesis_Test_02242020.[dbo].LEDGER_GL_ACCOUNT lg ON ct.CHARGE_TYPE_ID = lg.CHARGE_TYPE_ID
where [ACTIVE] = 1
END
GO
