--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_product_delete]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/26/2021
-- Description:		Deletes a single row of product recipes data based on the selected row/record.

/*EXEC mgn_product_delete
	@row  = '713fff12-5dcd-46dc-83d9-fbad54498f61'

	*/
-- =============================================
CREATE    OR ALTER           PROCEDURE [dbo].[mgn_product_delete]
	@row uniqueidentifier = null,
	@user varchar(50) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from products
--WHere _key = @row

UPDATE products
SET active = 0
Where _key = @row

END
GO
