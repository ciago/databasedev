--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_product_classes_get_list]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		3/1/2021
-- Description:		Gets a list of product classes from a selected site and product.

/*EXEC mgn_product_classes_get_list  
	@site_key = '00000000-0000-0000-0000-000000000000'
	
    */	
-- =============================================
CREATE    OR ALTER    PROCEDURE [dbo].[mgn_product_classes_get_list]

	@site_key uniqueidentifier = '00000000-0000-0000-0000-000000000000'
	   
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from product_classes

SELECT			pc._key,
				ISNULL(pc.product_class, '') as [product_class],
				ISNULL(pc.class_description, '') as [class_description],
				pc.active as [active]

FROM			product_classes AS pc

WHERE			(pc.site_key = @site_key OR @site_key = '00000000-0000-0000-0000-000000000000')
	  
END
GO
