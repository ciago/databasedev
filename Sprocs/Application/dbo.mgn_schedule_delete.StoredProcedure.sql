--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_schedule_delete]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Morey	
-- Create date: 5/4/2020
-- Description:	Deletes a single row of schedule data based on the user clicking 'edit' within the browser application.

/*EXEC mgn_schedule_delete
	@row  = '28b4e85f-53ae-422b-bf39-57df57829b3a'
	*/
-- =============================================
CREATE   OR ALTER  PROCEDURE [dbo].[mgn_schedule_delete]
	@row uniqueidentifier = null,
	@user varchar(50) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from schedule
--WHere _key = @row
Delete From import_schedule
Where _key = @row



END
GO
