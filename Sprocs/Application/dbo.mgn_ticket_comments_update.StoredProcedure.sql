--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_ticket_comments_update]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create OR ALTER       PROCEDURE [dbo].[mgn_ticket_comments_update]

	@ticket_comment_key uniqueidentifier = null,
	@ticket_key uniqueidentifier = null,
	@ticket_id bigint = null,
	@comment nvarchar(max),
	@delete bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from entities
--Where _key = @row


--MODA DATABASE
if((@ticket_comment_key is null) and (@ticket_key is not null) and (@delete = 1))
	begin

		update [ticket_comments]
		set [tc_deleted] = @delete
		where tc_ticket_key = @ticket_key

	end
else IF(@ticket_comment_key IN (SELECT _key from ticket_comments))
	begin

		update [ticket_comments]
		set [tc_comment] = @comment,
			[tc_deleted] = @delete
		where _key = @ticket_comment_key

	end
else
	begin

		INSERT INTO [dbo].[ticket_comments]
				   (_key
				   ,[tc_ticket_key]
				   ,[tc_ticket_id]
				   ,[tc_comment]
				   ,[tc_date])
		VALUES	   
				   (isnull(@ticket_comment_key, newid())
				   ,isnull(@ticket_key, (select _key from tickets where tk_ticket_id = @ticket_id))
				   ,isnull(@ticket_id, (select tk_ticket_id from tickets where _key = @ticket_key))
				   ,@comment
				   ,getdate())

	END
end
GO
