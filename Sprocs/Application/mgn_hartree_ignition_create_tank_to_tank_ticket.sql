--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_hartree_ignition_create_tank_to_tank_ticket]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

/*EXEC [mgn_hartree_ignition_create_tank_to_tank_ticket] 
	@location = 'tl3',
	@nomination = 1001
	
	// 
	*/
-- =============================================
CREATE   OR ALTER   PROCEDURE [dbo].mgn_hartree_ignition_create_tank_to_tank_ticket

	@ticket_id bigint,
	@batch_start_date varchar(50),
	@batch_start_time varchar(50),
	@batch_end_date varchar(50),
	@batch_end_time varchar(50),
	@source_tank_id int,
	@source_level_open float,
	@source_level_close float,
	@source_gross_open float,
	@source_gross_close float,
	@source_net_open float,
	@source_net_close float,
	@source_percent_open float,
	@source_percent_close float,
	@source_product nvarchar(100) = null,
	@destination_tank_id int,
	@destination_level_open float,
	@destination_level_close float,
	@destination_gross_open float,
	@destination_gross_close float,
	@destination_net_open float,
	@destination_net_close float,
	@destination_percent_open float,
	@destination_percent_close float,
	@destination_product nvarchar(100) = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @volume_uom float = (select uom_default_volume from site_configuration where site_key = dbo.get_default_site())
declare @mass_uom float = (select uom_default_mass from site_configuration where site_key = dbo.get_default_site())
declare @temperature_uom float = (select uom_default_temperature from site_configuration where site_key = dbo.get_default_site())
declare @density_uom float = (select uom_default_density from site_configuration where site_key = dbo.get_default_site())
declare @pressure_uom float = (select uom_default_pressure from site_configuration where site_key = dbo.get_default_site())

INSERT INTO [dbo].[tmp_hartree_tickets_tank_to_tank]
           ([_key]
           ,[tk_ticket_key]
           ,[tk_tank_source_key]
           ,[tk_tank_destination_key]
           ,[tk_tank_ticket_id]
           ,[tk_tank_source_id]
           ,[tk_tank_destination_id]
           ,[tk_tank_batch_start_date]
           ,[tk_tank_batch_start_time]
           ,[tk_tank_batch_end_date]
           ,[tk_tank_batch_end_time]
           ,[tk_source_tank_product_name]
		   ,[tk_destination_tank_product_name]
           ,[tk_tank_batch_id]
           ,[tk_tank_opening_level_source]
           ,[tk_tank_opening_gross_source]
           ,[tk_tank_opening_net_source]
		   ,tk_tank_opening_percent_source
           ,[tk_tank_closing_timestamp_source]
           ,[tk_tank_closing_level_source]
           ,[tk_tank_closing_gross_source]
           ,[tk_tank_closing_net_source]
		   ,tk_tank_closing_percent_source
           ,[tk_tank_opening_level_destination]
           ,[tk_tank_opening_gross_destination]
           ,[tk_tank_opening_net_destination]
		   ,tk_tank_opening_percent_destination
           ,[tk_tank_closing_timestamp_destination]
           ,[tk_tank_closing_level_destination]
           ,[tk_tank_closing_gross_destination]
           ,[tk_tank_closing_net_destination]
		   ,tk_tank_closing_percent_destination
           ,[tk_tank_batch_report_number]
           ,[tk_tank_batch_average_temperature]
           ,[tk_tank_batch_average_pressure]
           ,[tk_tank_batch_average_api_60f]
           ,[tk_tank_volume_uom]
           ,[tk_tank_mass_uom]
           ,[tk_tank_temperature_uom]
           ,[tk_tank_pressure_uom]
           ,[tk_tank_density_uom]
           ,[tk_tank_length_uom]
           ,[site_key]
           ,[deleted]
           ,[record_info])
     VALUES
           (newid()
           ,'00000000-0000-0000-0000-000000000000'
           ,'00000000-0000-0000-0000-000000000000'
           ,'00000000-0000-0000-0000-000000000000'
           ,@ticket_id
           ,@source_tank_id
           ,@destination_tank_id
           ,@batch_start_date
           ,@batch_start_time     --<tk_tank_batch_start_time, varchar(50),>
           ,@batch_end_date     --<tk_tank_batch_end_date, varchar(50),>
           ,@batch_end_time     --<tk_tank_batch_end_time, varchar(50),>
           ,@source_product     --<tk_tank_product_name, varchar(50),>
		   ,@destination_product
           ,@ticket_id     --<tk_tank_batch_id, varchar(50),>
           ,@source_level_open     --<tk_tank_opening_level_source, float,>
           ,@source_gross_open     --<tk_tank_opening_gross_source, float,>
           ,@source_net_open     --<tk_tank_opening_net_source, float,>
		   ,@source_percent_open
           ,NULL     --<tk_tank_closing_timestamp_source, datetime,>
           ,@source_level_close     --<tk_tank_closing_level_source, float,>
           ,@source_gross_close     --<tk_tank_closing_gross_source, float,>
           ,@source_net_close     --<tk_tank_closing_net_source, float,>
		   ,@source_percent_close
           ,@destination_level_open     --<tk_tank_opening_level_destination, float,>
           ,@destination_gross_open     --<tk_tank_opening_gross_destination, float,>
           ,@destination_net_open     --<tk_tank_opening_net_destination, float,>
		   ,@destination_percent_open     -- tk_tank_opening_percent_destination
           ,NULL     --<tk_tank_closing_timestamp_destination, datetime,>
           ,@destination_level_close     --<tk_tank_closing_level_destination, float,>
           ,@destination_gross_close     --<tk_tank_closing_gross_destination, float,>
           ,@destination_net_close     --<tk_tank_closing_net_destination, float,>
		   ,@destination_percent_close     -- tk_tank_closing_percent_destination
           ,NULL     --<tk_tank_batch_report_number, int,>
           ,NULL     --<tk_tank_batch_average_temperature, float,>
           ,NULL     --<tk_tank_batch_average_pressure, float,>
           ,NULL     --<tk_tank_batch_average_api_60f, float,>
           ,@volume_uom     --<tk_tank_volume_uom, float,>
           ,@mass_uom     --<tk_tank_mass_uom, float,>
           ,@temperature_uom     --<tk_tank_temperature_uom, float,>
           ,@pressure_uom     --<tk_tank_pressure_uom, float,>
           ,@density_uom     --<tk_tank_density_uom, float,>
           ,dbo.get_default_uom_length()     --<tk_tank_length_uom, float,>
           ,dbo.get_default_site()     --<site_key, uniqueidentifier,>
           ,0     --<deleted, bit,>
           ,dbo.default_record_info()     --<record_info, varchar(max),>)
		   )

END