--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_ex_report_document_drop_down]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 -- ===================================================
 -- Program Copyright Mangan Inc
 --  Author: Candice Iago
 --  Creation Date 4/21/21
 -- ===================================================
 -- EXEC mgn_ex_report_document_drop_down
 --
 --- Creates the stored procedure that checks the dbo.RawTrans for errors
 ---- Puts the code into system as a stored procedure

CREATE  OR ALTER     PROCEDURE  [dbo].[mgn_ex_report_document_drop_down]

--WITH   ENCRYPTION 
AS
 

 SELECT d._key,  CONVERT(varchar(30), d.doc_irs_end_date, 101)  + '-' +   r.rpt_type  + '-' + re.re_name
 FROM rpt_documents d INNER JOIN 
	  rpt_reports r  ON r._key = d.doc_report_type_key INNER JOIN 
	  rpt_report_entity re ON re._key = d.doc_report_entity_key
	ORDER BY d.doc_irs_end_date desc
GO
