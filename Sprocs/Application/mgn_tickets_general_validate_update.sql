--USE [Hartree_TMS]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_tickets_general_validate_update]    Script Date: 5/10/2021 3:22:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER                 PROCEDURE [dbo].[mgn_tickets_general_validate_update]

	@_key uniqueidentifier,
    @ticket_type int, 
	@status int = 1,
	@product uniqueidentifier = null,
	@batch_id nvarchar(50) = '',
	@sched_start_date datetime = null,
	@sched_end_date datetime = null,
	@start_date datetime = null,
	@end_date datetime = null,
	@nomination_number bigint = null,
	@scheduled_volume float = 0,
	@volume_uom int = null,
	@final_gsv float = null,
	@final_nsv float = null,
	@source_tank_key uniqueidentifier = null,
	@destination_tank_key uniqueidentifier = null,
	@berth_key uniqueidentifier = null,
	@pipeline_key uniqueidentifier = null,
	@vehicle_key uniqueidentifier = null,
	@vessel_key uniqueidentifier = null,
	@truck_lane_key uniqueidentifier = null,
	@entity1_key uniqueidentifier = null,
	@entity2_key uniqueidentifier = null,
	@entity3_key uniqueidentifier = null,
	@entity4_key uniqueidentifier = null,
	@entity5_key uniqueidentifier = null,
	@entity6_key uniqueidentifier = null,
	@entity7_key uniqueidentifier = null,
	@ticket_selected_site uniqueidentifier = null,
	@site_key uniqueidentifier,
	@deleted bit = 0,
	@user nvarchar(50),
	@final_update bit = 0,
	@void bit = 0
	
	
	/*EXEC [mgn_tickets_general_validate_update] 
	@_key = '5f362bb7-2926-4db5-b8b2-0d62d13ad85a',
	@ticket_type = 3,
	@entity1_key = 'dd85132f-4039-4fda-8fcc-bbd2dd9b7656',
	@scheduled_volume = 200,
	@ticket_selected_site = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@site_key = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd',
	@product  = '90be1f05-e535-4693-be59-b216b35ef1ec',
	@sched_start_date = '2021-05-10 7:00:00.000',
	@user = 'ciago'
	
	// 
	*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

--declare @ticket_type int = 3
--declare @entity1_key uniqueidentifier = 'dd85132f-4039-4fda-8fcc-bbd2dd9b7656' --'dd85132f-4039-4fda-8fcc-bbd2dd9b7656'
--declare @scheduled_volume float = 1
--declare @ticket_selected_site uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd' --'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
--declare @site_key uniqueidentifier = 'b8ab8843-02d4-4e7d-96cb-e3648327c0dd'
--declare @product uniqueidentifier = '90be1f05-e535-4693-be59-b216b35ef1ec'--'90be1f05-e535-4693-be59-b216b35ef1ec'  '87b8ed5e-b2a6-4460-a201-3b9f53c5d4e0'
--declare @sched_start_date datetime= '2021-05-07 00:00:00.000'
declare @message nvarchar(400)
declare @place_holder_string nvarchar(600) = ''
declare @ticket_type_message nvarchar(50) = ''
declare @entity_type_message nvarchar(50) = ''
declare @scheduled_volume_message nvarchar(50) = ''
declare @site_key_message nvarchar(50) = ''
declare @product_dock_message nvarchar(100) = ''
declare @product_message nvarchar(50) = ''
-- Check for stuff

IF(@ticket_type = 0)
Begin
	Set @ticket_type_message = 'Ticket Type must be selected. ' 
End

IF (@entity1_key IS NULL)
Begin
	Set @entity_type_message = (select et.type_description from site_configuration sc left outer join entity_types et on sc.default_entity_type_1 = et.type_code where @site_key = sc.site_key) + ' must be selected. '
End

if (@scheduled_volume = 0)
begin
	Set @scheduled_volume_message = 'Scheduled Volume must be selected. '
end

if (@product IS null)
begin
	Set @product_message = 'Product must be selected. '
end

if (@ticket_selected_site IS NULL)
begin
	set @site_key_message = 'Site must be selected. '
end

--print @ticket_type_message
--print @entity_type_message
--print @scheduled_volume_message
--print @product_message
--print @site_key_message
--print @message

if not exists (
SELECT m.meter_id
FROM products p LEFT OUTER JOIN 
	 (SELECT * from meter_products mp WHERE mp.mtr_start_datetime <= @sched_start_date and (mp.mtr_end_datetime >= @sched_start_date OR mp.mtr_end_datetime IS NULL)) mp ON mp.product_key = p._key LEFT OUTER JOIN 
	 meters m ON m._key = mp.meter_key LEFT OUTER JOIN 
	 locations l ON m.locations_key = l._key INNER JOIN 
	 location_types lt ON l.ln_location_type = lt.lt_location_type
WHERE p._key = @product and
	  lt.lt_location_type = 3 /*dock type*/) AND @ticket_type = 2 /*tank to vessel*/ AND @product IS NOT NULL
begin
	set @product_dock_message = 'There is currently no ' + (select lt_type_desc from location_types where lt_location_type = 3) + ' where ' + (select product_name from products where _key = @product) + ' can be loaded.'
end


--print @product_dock_message

set @message = @ticket_type_message+ @entity_type_message +@scheduled_volume_message + @product_message + @site_key_message + @product_dock_message

--print @message

SELECT @message
END
