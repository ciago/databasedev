--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_ticket_type_get]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   OR ALTER     PROCEDURE [dbo].[mgn_ticket_type_get]
	@type int = null,
	@site uniqueidentifier = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

/*
execute [mgn_ticket_type_get] @type = 2
*/

--Select * from 
--WHere _key = @row
   
IF(@site IS NULL) 
BEGIN 
	SET @site = dbo.get_default_site();
END

SELECT   tt._key, 
	     tt.typ_type, 
		 tt.type_description, 
		 tt.type_is_receipt, 
		 tt.type_is_shipment, 
		 tt.type_is_transfer, 
		 tt.type_is_pipeline, 
		 tt.type_is_vessel, 
		 tt.type_is_truck, 
		 tt.type_is_rail,
		  ttf.type_fie_show_berth as show_berth,
		  ttf.type_fie_show_dest_tank	as show_dest_tank,
		  ttf.type_fie_show_pipeline	as show_pipeline,
		  ttf.type_fie_show_product		as show_product,
		  ttf.type_fie_show_source_tank	as show_source_tank,
		  ttf.type_fie_show_truck		as show_truck,
		  ttf.type_fie_show_vessel		as show_vessel,
		  ttf.type_fie_show_location as show_location,
		  ttf.type_fie_show_nom_num as show_nom_num,
		  ttf.type_fie_show_misc as show_misc,
		  isnull(e1.type_description,'') as entity_1_label,
		  isnull(e1.type_code,0) as entity_1_type,
		  isnull(e2.type_description,'') as entity_2_label,
		  isnull(e2.type_code,0) as entity_2_type,
		  isnull(e3.type_description,'') as entity_3_label,
		  isnull(e3.type_code,0) as entity_3_type,
		  isnull(e4.type_description,'') as entity_4_label,
		  isnull(e4.type_code,0) as entity_4_type,
		  isnull(e5.type_description,'') as entity_5_label,
		  isnull(e5.type_code,0) as entity_5_type,
		  isnull(e6.type_description,'') as entity_6_label,
		  isnull(e6.type_code,0) as entity_6_type,
		  isnull(e7.type_description,'') as entity_7_label,
		  isnull(e7.type_code,0) as entity_7_type
		  
FROM ticket_type tt LEFT OUTER JOIN 
	 ticket_type_fields ttf on ttf.type_fie_ticket_type = tt._key left outer join
	 (SELECT * FROM ticket_type_entities WHERE @site = type_ent_site) tte on tte.type_ent_ticket_type = tt.typ_type left outer join
	 entity_types e1 ON tte.type_ent_entity_1_type_code = e1.type_code LEFT OUTER JOIN
	 entity_types e2 ON tte.type_ent_entity_2_type_code = e2.type_code LEFT OUTER JOIN
	 entity_types e3 ON tte.type_ent_entity_3_type_code = e3.type_code LEFT OUTER JOIN
	 entity_types e4 ON tte.type_ent_entity_4_type_code = e4.type_code LEFT OUTER JOIN
	 entity_types e5 ON tte.type_ent_entity_5_type_code = e5.type_code LEFT OUTER JOIN
	 entity_types e6 ON tte.type_ent_entity_6_type_code = e6.type_code LEFT OUTER JOIN
	 entity_types e7 ON tte.type_ent_entity_7_type_code = e7.type_code

WHERE @type = tt.typ_type and
	  @site = tt.site_key and
	  @site = ttf.type_fie_site

END
GO
