--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_business_entities_delete]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Brandon Morey    
-- Create date:		2/18/2021
-- Description:		Deletes a single row of business entities data based on the selected row/record.

/*EXEC mgn_business_entities_delete
	@row  = '00284d50-7227-4fa8-8db7-0dfa462cde50'

	*/
-- =============================================
CREATE  OR ALTER         PROCEDURE [dbo].[mgn_business_entities_delete]
	@row uniqueidentifier = null,
	@user varchar(50) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from entities
--WHere _key = @row
Delete From entities

Where _key = @row

END
GO
