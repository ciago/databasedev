--USE [Moda]
--GO
/****** Object:  StoredProcedure [dbo].[mgn_charge_details_get]    Script Date: 5/5/2021 5:39:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   OR ALTER     PROCEDURE [dbo].[mgn_charge_details_get] 

@row uniqueidentifier

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
----Select * from addresses

/*
execute [mgn_charge_details_get] @row ='ae4a6d50-0844-40d0-9c49-e637da046202'
*/

SELECT        ic._key,
			  ic.ic_vessel_invoice_key as [vessel_invoice_key],
			  c.charge_description as [charge_descrip],
			  ISNULL(ic.ic_unit_amount, 0) as [unit_amount],
			  ISNULL(cut.charge_unit_description, '') as [unit_descrip],
			  CONVERT(decimal(18,5) ,ISNULL(c.charge_amount, 0)) as [unit_rate],
			  ISNULL((ic.ic_unit_amount)*(c.charge_amount), 0) as [charge_total]
			  
FROM          invoice_charge AS ic INNER JOIN
              charges AS c ON ic.ic_charges_key = c._key INNER JOIN
	          charge_unit_type AS cut ON c.charge_unit_key = cut._key

WHERE		  ic.ic_vessel_invoice_key = @row
	  
END
GO
