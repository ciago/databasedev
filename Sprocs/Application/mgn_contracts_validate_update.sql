USE [Hartree_TMS]
GO
/****** Object:  StoredProcedure [dbo].[mgn_contracts_validate_update]    Script Date: 5/12/2021 5:16:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create or ALTER                     PROCEDURE [dbo].[mgn_contracts_validate_update]

	@row uniqueidentifier,
	@entity_key uniqueidentifier = null,
	@status int = 0,
	@type uniqueidentifier = null,
	@start_date datetime = null,
	@end_date datetime = null,
	@contract_site uniqueidentifier,
	@site uniqueidentifier,
	@user varchar(50),
	@description nvarchar (50) = null
	
	
	/*EXEC [mgn_contracts_validate_update]
	@row = 'c6d73637-edd5-4ff8-9f51-e33572e822f8',
	@entity_key = null,
	@status = 0,
	@type = null,
	@start_date = null ,
	@end_date  = null,
	@contract_site = null ,
	@site  = 'c6d73637-edd5-4ff8-9f51-e33572e822f8',
	@user = 'ciago'
	
	// 
	*/
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Select * from berth_fees
--Where _key = @row

declare @message nvarchar(400)
declare @status_message nvarchar(50) = ''
declare @entity_message nvarchar(50) = ''
declare @type_message nvarchar(50) = ''
declare @site_key_message nvarchar(50) = ''
declare @start_date_message nvarchar (50) = ''
declare @description_message nvarchar(50) = ''
-- Check for stuff

IF(@status = 0)
Begin
	Set @status_message = 'A Contract Status must be selected. ' 
End

IF (@entity_key IS NULL)
Begin
	Set @entity_message = 'An Entity must be selected. '
End

if (@type is null)
begin
	Set @type_message = 'A Contract Type must be selected. '
end

if (@contract_site IS null)
begin
	Set @site_key_message = 'A Site must be selected. '
end

if (@start_date IS null)
begin
	Set @start_date_message = 'A Start Date must be selected. '
end

if (@description IS null)
begin
	Set @description_message = 'A Contract Description must be entered. '
end

--print @status_message
--print @entity_message
--print @type_message
--print @site_key_message
--print @message

set @message = @status_message + @entity_message + @type_message + @site_key_message + @start_date_message + @description_message

--print @message

SELECT @message
END
