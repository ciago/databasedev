truncate table berth_activity
GO

--truncate table [dbo].[entities]
--GO

--truncate table  [dbo].[tank_owner] GO
--truncate table  [dbo].[tank_products] GO

truncate table   [dbo].[import_schedule]
GO


truncate table  [dbo].[invoice_charge]
GO


--truncate table  [dbo].[locations]
--GO

truncate table    [dbo].[meter_products]
GO
truncate table    [dbo].[meter_factor]
GO

truncate table  [dbo].[meter_readings]
GO

truncate table  [dbo].[meter_to_berth_layout] 
GO
truncate table  [dbo].[meters] 
GO
truncate table  [dbo].[Moda_Final_Tickets] 
GO
truncate table  [dbo].[monthly_volume_account_totals] 
GO
truncate table  [dbo].[nominations] 
GO
truncate table  [dbo].[nominations_general] 
GO
truncate table  [dbo].[pipeline] 
GO
truncate table  [dbo].[pipeline_actuals] 
GO
truncate table  [dbo].[pipeline_actuals_meter] 
GO
truncate table  [dbo].[pipeline_allocations] 
GO
truncate table  [dbo].[pipeline_entity_assignment] 
GO
truncate table  [dbo].[pipeline_product_assignment] 
GO
--truncate table  [dbo].[products] GO
truncate table  [dbo].[skid_batch_ids] 
GO
truncate table  [dbo].[skids] 
GO
truncate table  [dbo].[synthesis_entity_types] 
GO
truncate table  [dbo].[synthesis_tank_assign] 
GO
truncate table  [dbo].[synthesis_tickets] 
GO
